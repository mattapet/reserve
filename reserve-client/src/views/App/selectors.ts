/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';
import { RootState } from '@reserve/client/reducers';
import { workspaceActions, userActions } from '@reserve/client/actions';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  isLoggedIn: state.auth.isLoggedIn,
  retrieved: state.view.user.getMe.retrieved,
});

export const mapDisaptchToProps = (dispatch: Dispatch) => ({
  onFetchAppMeta: () => {
    dispatch(userActions.getMe.request());
    dispatch(workspaceActions.fetchByName.request());
    dispatch(workspaceActions.fetchAnnouncement.request());
  },
});
