/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Spinner from '@reserve/client/components/Spinner';
import NotFound from '@reserve/client/views/NotFound';
const Overview = React.lazy(() => import('./screens/Overview'));
const Reservations = React.lazy(() => import('./screens/Reservations'));
//#endregion

export interface Props {}

const Router: React.FunctionComponent<Props> = () => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      <Redirect key="/" from="/" to="/overview" push={false} exact />
      <Redirect key="/login" from="/login" to="/overview" push={false} />
      <Redirect
        key="/register/confirm"
        from="/register/confirm"
        to="/profile"
      />
      <Route key="/overview" path="/overview" component={Overview} exact />
      <Route
        key="/reservations"
        path="/reservations"
        component={Reservations}
      />
      <Route component={NotFound} />
    </Switch>
  </React.Suspense>
);

export default Router;
