/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Moment } from 'moment';
import { Card, Skeleton, Form, Button, DatePicker, Select } from 'antd';
import { Resource } from '@reserve/client/lib/types/Resource';
import { TextArea } from '@reserve/client/components/Input';
//#endregion

export interface Props {
  readonly resources: Resource[];
  readonly meta: {
    readonly pending: boolean;
    readonly retrieved: boolean;
    readonly error?: Error;
  };
  readonly dateStart: Moment | null | undefined;
  readonly dateEnd: Moment | null | undefined;
  readonly selectedResources: number[];
  readonly notes: string;
  readonly onDateStartChange: (value: Moment | null | undefined) => any;
  readonly onDateEndChange: (value: Moment | null | undefined) => any;
  readonly onSelectedResourcesChange: (value: number[]) => any;
  readonly onNotesChange: (value: string) => any;
  readonly onReset: () => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
}

const View: React.FunctionComponent<Props> = props => (
  <Card>
    <Skeleton loading={!props.meta.retrieved} active>
      <Form onSubmit={props.onSubmit}>
        <Form.Item label="Date range">
          <DatePicker.RangePicker
            showTime={{ format: 'HH:mm', minuteStep: 15 }}
            format="YYYY-MM-DD HH:mm"
            value={[props.dateStart, props.dateEnd] as any}
            onChange={([dateStart, dateEnd]) => {
              props.onDateStartChange(dateStart);
              props.onDateEndChange(dateEnd);
            }}
            disabled={props.meta.pending}
          />
        </Form.Item>
        <Form.Item label="Selected resources">
          <Select
            mode="multiple"
            value={props.selectedResources}
            onChange={props.onSelectedResourcesChange}
            disabled={props.meta.pending}
          >
            {props.resources.map(resource => (
              <Select.Option key={`${resource.id}`} value={resource.id}>
                {resource.name}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="Additional notes">
          <TextArea
            rows={4}
            value={props.notes}
            onChange={props.onNotesChange}
          />
        </Form.Item>
        <Button onClick={props.onReset} disabled={props.meta.pending}>
          Clear
        </Button>
        <Button
          htmlType="submit"
          type="primary"
          loading={props.meta.pending}
          disabled={
            !props.dateStart ||
            !props.dateEnd ||
            !props.selectedResources.length
          }
        >
          Submit
        </Button>
      </Form>
    </Skeleton>
  </Card>
);

export default View;
