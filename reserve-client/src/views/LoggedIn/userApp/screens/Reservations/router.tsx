/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Spinner from '@reserve/client/components/Spinner';
import NotFound from '@reserve/client/views/NotFound';
const ReservationList = React.lazy(() => import('./screens/ReservationList'));
const CreateReservation = React.lazy(() =>
  import('./screens/CreateReservation')
);
const ReservationDetails = React.lazy(() =>
  import('./screens/ReservationDetails')
);
const EditReservation = React.lazy(() => import('./screens/EditReservation'));
//#endregion

export interface Props {}

const Router: React.FunctionComponent<Props> = () => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      <Route
        key="/reservations"
        path="/reservations"
        component={ReservationList}
        exact
      />
      <Route
        key="/reservations/:id/details"
        path="/reservations/:id/details"
        component={ReservationDetails}
        exact
      />
      <Route
        key="/reservations/:id/edit"
        path="/reservations/:id/edit"
        component={EditReservation}
        exact
      />
      <Route
        key="/reservations/create"
        path="/reservations/create"
        component={CreateReservation}
        exact
      />
      <Route component={NotFound} />
    </Switch>
  </React.Suspense>
);

export default Router;
