/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { SortOrder } from 'antd/lib/table/interface';
//#endregion

export interface SortInfo {
  readonly columnKey: keyof Reservation;
  readonly order: SortOrder;
}
