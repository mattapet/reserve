/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import * as R from 'ramda';
import { Table, PageHeader } from 'antd';
import { useUserReservations } from './services/userUserReservations';
import { SortInfo } from '@reserve/client/lib/types/SortInfo';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { useQueryControlledTable } from '@reserve/client/lib/hooks';
import { columns } from './columns';
//#endregion

//#region Component interfaces
export interface Props {
  readonly meta: {
    readonly pending: boolean;
    readonly error?: Error;
  };
  readonly onCancel: (reservationId: string) => any;
}
//#endregion

const ReservationsList: React.FunctionComponent<Props> = props => {
  const defaultSort: SortInfo<Reservation> = {
    columnKey: 'createdAt',
    order: 'descend',
  };
  const {
    filterInfo,
    sortInfo,
    pagination,
    handlePaginationChange,
    handleTableChange,
  } = useQueryControlledTable<Reservation>(defaultSort);
  const { current, pageSize } = pagination ?? {};

  const { reservations, meta } = useUserReservations();
  const loading = !meta.retrieved || props.meta.pending;

  return (
    <PageHeader title="My Reservations">
      <Table
        dataSource={reservations}
        columns={columns(sortInfo, filterInfo, props.onCancel)}
        loading={loading}
        rowKey={R.prop('id')}
        onChange={handleTableChange}
        pagination={{
          current,
          pageSize,
          onChange: handlePaginationChange,
        }}
      />
    </PageHeader>
  );
};

export default ReservationsList;
