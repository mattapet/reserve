/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Divider } from 'antd';
import { ColumnProps } from 'antd/lib/table/interface';

import { date } from '@reserve/client/lib/dateFormatter';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import ReservationStateTag from '@reserve/client/components/ReservationStateTag';
import { ReservationState } from '@reserve/client/lib/types/ReservationState';
import { FilterInfo } from '@reserve/client/lib/types/FilterInfo';
import { SortInfo } from '@reserve/client/lib/types/SortInfo';
import CancelReservationButton from './components/CancelReservationButton';
import ReservationDetailsLink from './components/ReservationDetailsLink';
import ResourcesColumn from './components/ResourcesColumn';
import * as sorters from './services/sorters';
import * as filters from './services/filters';
//#endregion

function shouldDisplayCancelButtonForState(state: ReservationState): boolean {
  return (
    state === ReservationState.requested || state === ReservationState.confirmed
  );
}

export const columns = (
  sortInfo: SortInfo<Reservation> | undefined,
  filterInfo: FilterInfo<Reservation> | undefined,
  onCancel: (id: string) => any
): ColumnProps<Reservation>[] => [
  {
    key: 'resources',
    title: 'Resources',
    render: (_, reservation) => <ResourcesColumn reservation={reservation} />,
  },
  {
    key: 'dateStart',
    title: 'Date Start',
    sortOrder: sortInfo?.columnKey === 'dateStart' && sortInfo.order,
    sorter: sorters.createdAtSorter,
    render: (_, { dateStart }) => date(dateStart),
  },
  {
    key: 'dateEnd',
    title: 'Date End',
    sortOrder: sortInfo?.columnKey === 'dateEnd' && sortInfo.order,
    sorter: sorters.dateEndSort,
    render: (_, { dateEnd }) => date(dateEnd),
  },
  {
    key: 'state',
    title: 'State',
    filters: [
      {
        text: 'Requested',
        value: ReservationState.requested,
      },
      {
        text: 'Confirmed',
        value: ReservationState.confirmed,
      },
      {
        text: 'Rejected',
        value: ReservationState.rejected,
      },
      {
        text: 'Canceled',
        value: ReservationState.canceled,
      },
      {
        text: 'Completed',
        value: ReservationState.completed,
      },
    ],
    filteredValue: filterInfo?.state ?? null,
    onFilter: filters.stateFilter,
    render: (_, { state }) => <ReservationStateTag state={state} />,
  },
  {
    key: 'createdAt',
    title: 'Created At',
    sortOrder: sortInfo?.columnKey === 'createdAt' && sortInfo.order,
    sorter: sorters.createdAtSorter,
    render: (_, { createdAt }) => date(createdAt),
  },
  {
    key: 'actions',
    title: 'Actions',
    render: (_, { id, state }) => (
      <React.Fragment>
        <ReservationDetailsLink id={id} />
        {shouldDisplayCancelButtonForState(state) ? (
          <React.Fragment>
            <Divider type="vertical" />
            <CancelReservationButton id={id} onCancel={onCancel} />
          </React.Fragment>
        ) : null}
      </React.Fragment>
    ),
  },
];
