/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Popconfirm } from 'antd';
//#endregion

export interface Props {
  readonly id: string;
  readonly onCancel: (reservationId: string) => any;
}

const CancelReservationButton: React.FunctionComponent<Props> = props => (
  <Popconfirm
    title="Are you sure?"
    okText="Yes"
    onConfirm={() => props.onCancel(props.id)}
  >
    {/* eslint-disable-next-line */}
    <a href="javascript:;">Cancel</a>
  </Popconfirm>
);

export default CancelReservationButton;
