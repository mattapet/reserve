/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { PageHeader, Skeleton } from 'antd';
import ReservationFormWrapper from './ReservationFormWraper';
import { useHistory } from 'react-router';
import { useParamReservation } from '@reserve/client/services/hooks/useParamReservation';
//#endregion

export interface Props {}

const CreateReservation: React.FunctionComponent<Props> = props => {
  const history = useHistory();
  const reservation = useParamReservation();

  return (
    <PageHeader
      title="My Reservations"
      subTitle="Create new reservation"
      onBack={() => history.goBack()}
    >
      <Skeleton loading={!reservation}>
        <ReservationFormWrapper
          defaultValue={reservation}
          onComplete={() => history.goBack()}
        />
      </Skeleton>
    </PageHeader>
  );
};

export default CreateReservation;
