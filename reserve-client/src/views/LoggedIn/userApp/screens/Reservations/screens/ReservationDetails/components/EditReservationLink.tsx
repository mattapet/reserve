/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Icon } from 'antd';
import { Link } from 'react-router-dom';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { ReservationState } from '@reserve/client/lib/types/ReservationState';
//#endregion

//#region Component interfaces
export interface Props {
  readonly id: string;
  readonly reservation?: Reservation;
}
//#endregion

const EditReservationLink: React.FunctionComponent<Props> = props =>
  props.reservation?.state === ReservationState.requested ||
  props.reservation?.state === ReservationState.confirmed ? (
    <Link to={`/reservations/${props.id}/edit`}>
      <Icon type="edit" key="edit" /> Edit
    </Link>
  ) : null;

export default EditReservationLink;
