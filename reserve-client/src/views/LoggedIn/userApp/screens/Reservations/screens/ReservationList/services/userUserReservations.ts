/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as R from 'ramda';
import { useSelector } from 'react-redux';
import { FetchMeta } from '@reserve/client/reducers/view/resource/fetch';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { userIdentitySelector } from '@reserve/client/services/authSelectors';
import { useReservations } from '@reserve/client/services/hooks/useReservations';
//#endregion

interface Result {
  readonly reservations: Reservation[];
  readonly meta: FetchMeta;
}

export function useUserReservations(): Result {
  const { reservations, meta } = useReservations();
  const identity = useSelector(userIdentitySelector);

  return {
    reservations: R.filter(
      ({ userId }) => userId === identity.id,
      reservations
    ),
    meta,
  };
}
