/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TimelineGroup } from 'react-calendar-timeline';
import { Resource } from '@reserve/client/lib/types/Resource';
//#endregion

export interface TimelineGroupExtraFields {
  readonly active: boolean;
}
export function transformResourceToTimelineGroup(
  resource: Resource
): TimelineGroup<TimelineGroupExtraFields> {
  return {
    id: `${resource.id}`,
    title: resource.name,
    rightTitle: resource.description,
    active: resource.active,
  };
}
