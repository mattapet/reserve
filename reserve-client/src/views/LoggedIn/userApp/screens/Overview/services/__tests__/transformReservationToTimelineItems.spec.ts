/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import moment, { Moment } from 'moment';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { ReservationState } from '@reserve/client/lib/types/ReservationState';
import { transformReservationToTimelineItems } from '../transformReservationToTimelineItems';
//#endregion

function createReservation(
  resourceIds: number[],
  dateStart: Date,
  dateEnd: Date
): Reservation {
  return {
    id: '12345',
    dateStart,
    dateEnd,
    resourceIds,
    allDay: false,
    userId: '12345',
    state: ReservationState.confirmed,
  };
}

function createTimelineItem(
  resourceId: number,
  dateStart: Date,
  dateEnd: Date,
  title: string
) {
  return {
    id: `${'12345'}:${resourceId}`,
    group: `${resourceId}`,
    title,
    start_time: moment(dateStart),
    end_time: moment(dateEnd),
  };
}

describe('transforming reservations to timeline items', () => {
  describe('reservation with a single resource', () => {
    it('should produce single item for a reservation with resource 1', () => {
      const dateStart = new Date('2020-10-01');
      const dateEnd = new Date('2020-10-05');
      const reservation = createReservation([1], dateStart, dateEnd);

      const result = transformReservationToTimelineItems(reservation);

      expect(result).toEqual([
        createTimelineItem(1, dateStart, dateEnd, '2020-10-01 - 2020-10-05'),
      ]);
    });

    it('should produce single item for a reservation with resource 2', () => {
      const dateStart = new Date('2020-11-01');
      const dateEnd = new Date('2020-11-05');
      const reservation = createReservation([2], dateStart, dateEnd);

      const result = transformReservationToTimelineItems(reservation);

      expect(result).toEqual([
        createTimelineItem(2, dateStart, dateEnd, '2020-11-01 - 2020-11-05'),
      ]);
    });
  });

  describe('reservation with multiple resources', () => {
    it('should produce two items for a reservation with resources 1, 2', () => {
      const dateStart = new Date('2020-11-01');
      const dateEnd = new Date('2020-11-05');
      const reservation = createReservation([1, 2], dateStart, dateEnd);

      const result = transformReservationToTimelineItems(reservation);

      expect(result).toEqual([
        createTimelineItem(1, dateStart, dateEnd, '2020-11-01 - 2020-11-05'),
        createTimelineItem(2, dateStart, dateEnd, '2020-11-01 - 2020-11-05'),
      ]);
    });

    it('should produce 3 items for a reservation with 3 resources', () => {
      const dateStart = new Date('2020-11-01');
      const dateEnd = new Date('2020-11-05');
      const reservation = createReservation([1, 2, 3], dateStart, dateEnd);

      const result = transformReservationToTimelineItems(reservation);

      expect(result).toEqual([
        createTimelineItem(1, dateStart, dateEnd, '2020-11-01 - 2020-11-05'),
        createTimelineItem(2, dateStart, dateEnd, '2020-11-01 - 2020-11-05'),
        createTimelineItem(3, dateStart, dateEnd, '2020-11-01 - 2020-11-05'),
      ]);
    });
  });
});
