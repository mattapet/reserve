/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import moment, { Moment } from 'moment';
import { TimelineItemBase } from 'react-calendar-timeline';
import { Reservation } from '@reserve/client/lib/types/Reservation';
//#endregion

function formatDate(date: Date): string {
  return moment(date).format('YYYY-MM-DD');
}

function createTitle(reservation: Reservation): string {
  const { dateStart, dateEnd } = reservation;
  return `${formatDate(dateStart)} - ${formatDate(dateEnd)}`;
}

export function transformReservationToTimelineItems(
  reservation: Reservation
): TimelineItemBase<Moment>[] {
  return reservation.resourceIds.map(resourceId => ({
    id: `${reservation.id}:${resourceId}`,
    group: `${resourceId}`,
    title: createTitle(reservation),
    start_time: moment(reservation.dateStart),
    end_time: moment(reservation.dateEnd),
  }));
}
