/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Resource } from '@reserve/client/lib/types/Resource';
import { transformResourceToTimelineGroup } from '../transformResourceToTimelineGroup';
//#endregion

describe('transforming resources to timeline groups', () => {
  describe('an active resource', () => {
    it('return a valid timeline group', () => {
      const resource: Resource = {
        id: 1,
        name: 'test resource',
        description: 'some description',
        active: true,
      };

      const result = transformResourceToTimelineGroup(resource);

      expect(result).toEqual({
        id: '1',
        title: 'test resource',
        rightTitle: 'some description',
        active: true,
      });
    });
  });
});
