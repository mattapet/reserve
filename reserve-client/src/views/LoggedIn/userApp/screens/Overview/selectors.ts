/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';
import { createSelector } from 'reselect';
import { reservationActions, resourceActions } from '@reserve/client/actions';
import {
  reservationMetaSelector,
  reservationListSelector,
} from '@reserve/client/services/reservationSelectors';
import {
  resourceMetaSelector,
  resourceListSelector,
} from '@reserve/client/services/resourceSelectors';
//#endregion

const retrievedSelector = createSelector(
  reservationMetaSelector,
  resourceMetaSelector,
  (reservation, resource) => reservation.retrieved && resource.retrieved
);

const pendingSelector = createSelector(
  reservationMetaSelector,
  resourceMetaSelector,
  (reservation, resource) => reservation.pending || resource.pending
);

export const mapStateToProps = createSelector(
  retrievedSelector,
  pendingSelector,
  resourceListSelector,
  reservationListSelector,
  (retrieved, pending, resources, reservations) => ({
    retrieved,
    pending,
    resources,
    reservations,
  })
);

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onFetch: () => {
    dispatch(reservationActions.fetch.request());
    dispatch(resourceActions.fetch.request());
  },
});
