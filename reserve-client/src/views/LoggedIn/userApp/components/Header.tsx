/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Button, Layout, Menu, Popover } from 'antd';
import {
  Link,
  NavLink,
  RouteComponentProps,
  withRouter,
} from 'react-router-dom';
import styled from 'styled-components';
import config from '@reserve/client/config';
//#endregion

const LayoutHeader = styled(Layout.Header)`
  display: flex;
  justify-content: space-between;
`;

const MenuContainer = styled.div`
  display: flex;
`;

const Title = styled.h1`
  font-size: 36px;
  color: whitesmoke;
  display: inline-block;
  cursor: pointer;
  margin-right: 0.5em;

  transition: all 0.3s;

  &:hover {
    color: #40a9ff;
  }
`;

const UserPopover = styled(Popover)`
  justify-self: center;
  align-self: center;
`;

const ProfileOptions: React.FunctionComponent<RouteComponentProps<
  any
>> = props => (
  <Menu mode="vertical" selectedKeys={[props.location.pathname]}>
    <Menu.Item key="/profile">
      <NavLink to="/profile">Profile</NavLink>
    </Menu.Item>
    <Menu.Item key="/profile/settings">
      <NavLink to="/profile/settings">Settings</NavLink>
    </Menu.Item>
    <Menu.Item key="/logout">
      <NavLink to="/logout">Logout</NavLink>
    </Menu.Item>
  </Menu>
);

const Header: React.FunctionComponent<RouteComponentProps<any>> = props => (
  <LayoutHeader>
    <MenuContainer>
      <Link to="/">
        <Title>{config.workspaceName}</Title>
      </Link>

      <Menu
        mode="horizontal"
        selectedKeys={[props.location.pathname]}
        theme="dark"
        style={{ lineHeight: '64px' }}
      >
        <Menu.Item key="/overview">
          <Link to="/overview">Overview</Link>
        </Menu.Item>
        <Menu.Item key="/reservations">
          <Link to="/reservations">My Reservations</Link>
        </Menu.Item>
        <Menu.Item key="/reservations/create">
          <Link to="/reservations/create">Create Reservation</Link>
        </Menu.Item>
      </Menu>
    </MenuContainer>

    <UserPopover content={<ProfileOptions {...props} />} trigger="click">
      <Button icon="user" ghost={true} shape="circle" />
    </UserPopover>
  </LayoutHeader>
);

export default withRouter(Header);
