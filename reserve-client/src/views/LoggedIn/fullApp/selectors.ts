/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createSelector } from 'reselect';
import {
  userRoleSelector,
  userIdentitySelector,
} from '@reserve/client/services/authSelectors';
//#endregion

export const mapStateToProps = createSelector(
  userRoleSelector,
  userIdentitySelector,
  (role, { isOwner }) => ({
    role,
    isOwner,
  })
);
