/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react';
import { Layout } from 'antd';
import styled from 'styled-components';
import { WorkspaceAnnouncement } from '@reserve/client/lib/types/WorkspaceAnnouncement';

export interface Props {
  readonly announcement: WorkspaceAnnouncement | null;
}

const LayoutHeader = styled(Layout.Header)`
  text-align: center;
  color: whitesmoke;
  background-color: #faad14;
`;

const Announcement: React.FunctionComponent<Props> = props =>
  props.announcement && <LayoutHeader>{props.announcement.text}</LayoutHeader>;

export default Announcement;
