/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect } from 'react';
import Timeline from 'react-calendar-timeline';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { Resource } from '@reserve/client/lib/types/Resource';
import { transformResourceToTimelineGroup } from './services/transformResourceToTimelineGroup';
import { transformReservationToTimelineItems } from './services/transformReservationToTimelineItems';
import Spinner from '@reserve/client/components/Spinner';
//#endregion

//#region Component interfaces
export interface Props {
  readonly pending: boolean;
  readonly retrieved: boolean;
  readonly resources: Resource[];
  readonly reservations: Reservation[];
  readonly onFetch: () => any;
}
export interface ResourceGroupProps {}
//#endregion

function getDefaultTimeStart(): Date {
  return new Date(Date.now());
}

function getDefaultTimeEnd(): Date {
  const FIVE_DAYS = 1000 * 60 * 60 * 5;
  return new Date(Date.now() + FIVE_DAYS);
}

const Overview: React.FunctionComponent<Props> = props => {
  const { retrieved, pending, onFetch } = props;
  const items = props.reservations
    .map(transformReservationToTimelineItems)
    .reduce((acc, next) => acc.concat(next), []);

  const groups = props.resources.map(transformResourceToTimelineGroup);

  useEffect(() => {
    if (!retrieved && !pending) {
      onFetch();
    }
  }, [retrieved, pending, onFetch]);

  if (!props.retrieved) {
    return <Spinner />;
  }

  return (
    <Timeline
      items={items}
      groups={groups}
      defaultTimeStart={getDefaultTimeStart()}
      defaultTimeEnd={getDefaultTimeEnd()}
    />
  );
};

export default Overview;
