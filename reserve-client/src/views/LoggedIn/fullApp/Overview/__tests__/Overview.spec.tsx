/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import Timeline, {
  TimelineGroup,
  TimelineItemBase,
} from 'react-calendar-timeline';
import { shallow, mount } from 'enzyme';
import Overview from '../Overview';
import { Resource } from '@reserve/client/lib/types/Resource';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { ReservationState } from '@reserve/client/lib/types/ReservationState';
import Spinner from '@reserve/client/components/Spinner';
//#endregion

//#region render helerps
const defaultProps = {
  retrieved: true,
  pending: false,
  resources: [],
  reservations: [],
  onFetch: jest.fn(),
};
function render_emptyOverview() {
  return <Overview {...defaultProps} />;
}

function render_overviewWithResources(resources: Resource[]) {
  return <Overview {...defaultProps} resources={resources} />;
}

function render_overviewWithReservations(reservations: Reservation[]) {
  return <Overview {...defaultProps} reservations={reservations} />;
}

function render_overviewWithRetrieved(retrieved: boolean) {
  return <Overview {...defaultProps} retrieved={retrieved} />;
}
function render_overviewWithRetrievedPendingAndOnFetch(
  retrieved: boolean,
  pending: boolean,
  onFetch: () => any
) {
  const props = { retrieved, pending, onFetch };
  return <Overview {...defaultProps} {...props} />;
}
//#endregion

//#region mock helpers
function mock_dateNowMethod(now: number) {
  jest.spyOn(Date, 'now').mockImplementation(() => now);
}
//#endregion

//#region creation helpers
function make_resource(
  id: number,
  name: string,
  description: string,
  active: boolean = false
): Resource {
  return { id, name, description, active };
}

function make_groupItem(
  id: string,
  title: string,
  rightTitle: string,
  active: boolean = false
): TimelineGroup<{ active: boolean }> {
  return { id, title, rightTitle, active };
}

function make_reservation(
  id: string,
  dateStart: Date,
  dateEnd: Date,
  resourceIds: number[]
): Reservation {
  return {
    id,
    dateStart,
    dateEnd,
    resourceIds,
    allDay: false,
    state: ReservationState.confirmed,
    userId: 'abcd',
  };
}

function make_items(
  id: string,
  dateStart: string,
  dateEnd: string,
  groupIds: string[]
): TimelineItemBase<Date>[] {
  return groupIds.map(group => ({
    id: `${id}:${group}`,
    group,
    title: `${dateStart} - ${dateEnd}`,
    start_time: new Date(dateStart),
    end_time: new Date(dateEnd),
  }));
}
//#endregion

describe('Overview component', () => {
  describe('rendering without any data', () => {
    it('should render a timeline component', () => {
      const wrapper = shallow(render_emptyOverview());

      expect(wrapper.find(Timeline).exists()).toBe(true);
    });

    it('should provide a default time start as of today', () => {
      const NOW = 100_000;
      mock_dateNowMethod(NOW);

      const wrapper = shallow(render_emptyOverview());

      expect(wrapper.find(Timeline).prop('defaultTimeStart')).toEqual(
        new Date(NOW)
      );
    });

    it('should provide a default time end as of today 5 days in future', () => {
      const NOW = 100_000;
      const FIVE_DAYS = 1000 * 60 * 60 * 5;
      mock_dateNowMethod(NOW);

      const wrapper = shallow(render_emptyOverview());

      expect(wrapper.find(Timeline).prop('defaultTimeEnd')).toEqual(
        new Date(NOW + FIVE_DAYS)
      );
    });
  });

  describe('rendering resources', () => {
    it('should render a timeline with a single group for one resource', () => {
      const resources: Resource[] = [
        make_resource(1, 'Test resource', 'some description', true),
      ];

      const result = shallow(render_overviewWithResources(resources));

      expect(result.find(Timeline).prop('groups')).toEqual([
        make_groupItem('1', 'Test resource', 'some description', true),
      ]);
    });

    it('should render a timeline with multiple groups', () => {
      const resources: Resource[] = [
        make_resource(1, 'Test resource', 'some description', true),
        make_resource(2, 'Another Test resource', 'Other description', false),
      ];

      const result = shallow(render_overviewWithResources(resources));

      expect(result.find(Timeline).prop('groups')).toEqual([
        make_groupItem('1', 'Test resource', 'some description', true),
        make_groupItem(
          '2',
          'Another Test resource',
          'Other description',
          false
        ),
      ]);
    });
  });

  describe('rendering reservations', () => {
    it('should be able to translate reservation to an item', () => {
      const dateStart = new Date('2020-01-10');
      const dateEnd = new Date('2020-01-15');
      const reservations: Reservation[] = [
        make_reservation('12345', dateStart, dateEnd, [1]),
      ];

      const result = shallow(render_overviewWithReservations(reservations));

      expect(result.find(Timeline).prop('items')).toEqual([
        ...make_items('12345', '2020-01-10', '2020-01-15', ['1']),
      ]);
    });

    it('should be able to translate reservation to mutltiple items', () => {
      const dateStart = new Date('2020-01-10');
      const dateEnd = new Date('2020-01-15');
      const reservations: Reservation[] = [
        make_reservation('12345', dateStart, dateEnd, [1, 2]),
      ];

      const result = shallow(render_overviewWithReservations(reservations));

      expect(result.find(Timeline).prop('items')).toEqual([
        ...make_items('12345', '2020-01-10', '2020-01-15', ['1', '2']),
      ]);
    });

    it('should be able to translate mutliple reservations', () => {
      const dateStart = new Date('2020-01-10');
      const dateEnd = new Date('2020-01-15');
      const reservations: Reservation[] = [
        make_reservation('12345', dateStart, dateEnd, [1, 2]),
        make_reservation('67890', dateStart, dateEnd, [2, 4]),
      ];

      const result = shallow(render_overviewWithReservations(reservations));

      expect(result.find(Timeline).prop('items')).toEqual([
        ...make_items('12345', '2020-01-10', '2020-01-15', ['1', '2']),
        ...make_items('67890', '2020-01-10', '2020-01-15', ['2', '4']),
      ]);
    });
  });

  describe('data not not retrieved', () => {
    it('should not render Timeline component', () => {
      const wrapper = shallow(render_overviewWithRetrieved(false));

      expect(wrapper.find(Timeline).exists()).toBe(false);
    });

    it('should render Spinner component', () => {
      const wrapper = shallow(render_overviewWithRetrieved(false));

      expect(wrapper.find(Spinner).exists()).toBe(true);
    });
  });

  describe('data not not retrieved and not pending', () => {
    it('should call onFetch callback', () => {
      const onFetch = jest.fn();

      mount(
        render_overviewWithRetrievedPendingAndOnFetch(false, false, onFetch)
      );

      expect(onFetch).toHaveBeenCalledTimes(1);
    });
  });

  describe('data not not retrieved and pending', () => {
    it('should call onFetch callback', () => {
      const onFetch = jest.fn();

      mount(
        render_overviewWithRetrievedPendingAndOnFetch(false, true, onFetch)
      );

      expect(onFetch).not.toHaveBeenCalled();
    });
  });
});
