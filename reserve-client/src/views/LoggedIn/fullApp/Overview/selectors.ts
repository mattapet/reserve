/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';
import { RootState } from '@reserve/client/reducers';
import { reservationActions, resourceActions } from '@reserve/client/actions';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  reservations: Object.values(state.reservation.data),
  resources: Object.values(state.resource.data),
  retrieved:
    state.view.reservation.fetch.retrieved &&
    state.view.resource.fetch.retrieved,
  pending:
    state.view.reservation.fetch.pending || state.view.resource.fetch.pending,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onFetch: () => {
    dispatch(reservationActions.fetch.request());
    dispatch(resourceActions.fetch.request());
  },
});
