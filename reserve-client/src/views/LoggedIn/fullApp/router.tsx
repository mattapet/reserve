/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { UserRole } from '@reserve/client/lib/types/UserRole';
import NotFound from '@reserve/client/views/NotFound';
import Spinner from '@reserve/client/components/Spinner';
const MyReservations = React.lazy(() => import('./MyReservations'));
const Overview = React.lazy(() => import('./Overview'));
const Reservations = React.lazy(() => import('./Reservations'));
const Resources = React.lazy(() => import('./Resources'));
const Users = React.lazy(() => import('./Users'));
const Announcements = React.lazy(() => import('./Settings/Announcements'));
const Webhooks = React.lazy(() => import('./Settings/Webhooks'));
const Workspace = React.lazy(() => import('./Settings/Workspace'));
const ReservationSettings = React.lazy(() => import('./Settings/Reservation'));
//#endregion

const maintainerRoutes = [
  <Redirect key="/" from="/" to="/overview" push={false} exact />,
  <Redirect key="/login" from="/login" to="/overview" push={false} />,
  <Redirect
    key="/register"
    from="/register"
    to="overview"
    push={false}
    exact
  />,
  <Route key="/overview" path="/overview" component={Overview} />,
  <Route
    key="/my_reservations"
    path="/my_reservations"
    component={MyReservations}
  />,

  <Route key="/resources" path="/resources" component={Resources} />,
  <Route key="/reservations" path="/reservations" component={Reservations} />,
  <Route key="/users" path="/users" component={Users} />,
];

const adminRoutes = [
  ...maintainerRoutes,

  <Redirect
    key="/settings"
    from="/settings"
    to="/settings/reservations"
    exact
  />,
  <Route
    key="/settings/reservations"
    path="/settings/reservations"
    component={ReservationSettings}
  />,
  <Route
    key="/settings/announcements"
    path="/settings/announcements"
    component={Announcements}
  />,
  <Route
    key="/settings/webhooks"
    path="/settings/webhooks"
    component={Webhooks}
  />,
  <Redirect
    key="/settings/workspace"
    from="/settings/workspace"
    to="/settings"
    push={false}
  />,
];

const ownerRoutes = [
  ...maintainerRoutes,

  <Redirect
    key="/settings"
    from="/settings"
    to="/settings/reservations"
    exact
  />,
  <Route
    key="/settings/reservations"
    path="/settings/reservations"
    component={ReservationSettings}
  />,
  <Route
    key="/settings/announcements"
    path="/settings/announcements"
    component={Announcements}
  />,
  <Route
    key="/settings/webhooks"
    path="/settings/webhooks"
    component={Webhooks}
  />,
  <Route
    key="/settings/workspace"
    path="/settings/workspace"
    component={Workspace}
  />,
];

export interface Props {
  readonly role: UserRole;
  readonly isOwner: boolean;
}

const Router: React.FunctionComponent<Props> = (props) => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      {props.isOwner
        ? ownerRoutes
        : props.role === UserRole.maintainer
        ? maintainerRoutes
        : adminRoutes}
      <Route component={NotFound} />
    </Switch>
  </React.Suspense>
);

export default Router;
