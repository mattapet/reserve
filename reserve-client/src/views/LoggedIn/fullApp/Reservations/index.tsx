/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import NotFound from '@reserve/client/views/NotFound';
import EditReservation from './scenes/EditReservation';
import CreateReservation from './scenes/CreateReservation';
import ReservationList from './scenes/ReservationsList';
import ReservationDetails from './scenes/ReservationDetails';
//#endregion

const Router: React.FunctionComponent<any> = () => (
  <Switch>
    <Route path="/reservations" component={ReservationList} exact />
    <Route path="/reservations/create" component={CreateReservation} exact />
    <Route path="/reservations/:id/edit" component={EditReservation} exact />
    <Route
      path="/reservations/:id/details"
      component={ReservationDetails}
      exact
    />
    <Route component={NotFound} />
  </Switch>
);

export default Router;
