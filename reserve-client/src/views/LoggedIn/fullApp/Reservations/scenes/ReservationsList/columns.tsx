/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Divider } from 'antd';
import { ColumnProps } from 'antd/lib/table/interface';

import { date } from '@reserve/client/lib/dateFormatter';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import ReservationStateTag from '@reserve/client/components/ReservationStateTag';
import { ReservationState } from '@reserve/client/lib/types/ReservationState';
import { FilterInfo } from '@reserve/client/lib/types/FilterInfo';
import { SortInfo } from '@reserve/client/lib/types/SortInfo';
import { Resource } from '@reserve/client/lib/types/Resource';
import ReservationActionSelect from './components/ReservationActionSelect';
import ReservationDetailsLink from './components/ReservationDetailsLink';
import ResourcesColumn from './components/ResourcesColumn';
import AssignedToLink from '../../components/AssignedToLink';
import * as sorters from './services/sorters';
import * as filters from './services/filters';
//#endregion

function shouldDispalyReservationActions(state: ReservationState): boolean {
  return (
    state === ReservationState.requested || state === ReservationState.confirmed
  );
}

export const columns = (
  resources: Resource[],
  sortInfo: SortInfo<Reservation> | undefined,
  filterInfo: FilterInfo<Reservation> | undefined,
  onCancel: (id: string) => any,
  onConfirm: (id: string) => any,
  onReject: (id: string, reason: string) => any
): ColumnProps<Reservation>[] => [
  {
    key: 'resourceIds',
    title: 'Resources',
    filters: resources.map(({ name, id }) => ({
      text: name,
      value: `${id}`,
    })),
    filteredValue: filterInfo?.resourceIds ?? null,
    onFilter: filters.resourceFilter,
    render: (_, reservation) => <ResourcesColumn reservation={reservation} />,
  },
  {
    key: 'dateStart',
    title: 'Date Start',
    sortOrder: sortInfo?.columnKey === 'dateStart' && sortInfo.order,
    sorter: sorters.createdAtSorter,
    render: (_, { dateStart }) => date(dateStart),
  },
  {
    key: 'dateEnd',
    title: 'Date End',
    sortOrder: sortInfo?.columnKey === 'dateEnd' && sortInfo.order,
    sorter: sorters.dateEndSort,
    render: (_, { dateEnd }) => date(dateEnd),
  },
  {
    key: 'state',
    title: 'State',
    filters: [
      {
        text: 'Requested',
        value: ReservationState.requested,
      },
      {
        text: 'Confirmed',
        value: ReservationState.confirmed,
      },
      {
        text: 'Rejected',
        value: ReservationState.rejected,
      },
      {
        text: 'Canceled',
        value: ReservationState.canceled,
      },
      {
        text: 'Completed',
        value: ReservationState.completed,
      },
    ],
    filteredValue: filterInfo?.state ?? null,
    onFilter: filters.stateFilter,
    render: (_, { state }) => <ReservationStateTag state={state} />,
  },
  {
    key: 'assignedTo',
    title: 'Assigned To',
    render: (_, { userId }) => <AssignedToLink userId={userId} />,
  },
  {
    key: 'createdAt',
    title: 'Created At',
    sortOrder: sortInfo?.columnKey === 'createdAt' && sortInfo.order,
    sorter: sorters.createdAtSorter,
    render: (_, { createdAt }) => date(createdAt),
  },
  {
    key: 'actions',
    title: 'Actions',
    render: (_, reservation) => (
      <React.Fragment>
        <ReservationDetailsLink id={reservation.id} />
        {shouldDispalyReservationActions(reservation.state) ? (
          <React.Fragment>
            <Divider type="vertical" />
            <ReservationActionSelect
              reservation={reservation}
              onCancel={onCancel}
              onConfirm={onConfirm}
              onReject={onReject}
            />
          </React.Fragment>
        ) : null}
      </React.Fragment>
    ),
  },
];
