/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region importrs
import * as R from 'ramda';
import { Reservation } from '@reserve/client/lib/types/Reservation';
//#endregion

function makePropertyComparator<K extends keyof Reservation>(prop: K) {
  return R.comparator(
    (lhs: Reservation, rhs: Reservation) =>
      R.prop(prop, lhs) < R.prop(prop, rhs)
  );
}

export const createdAtSorter = makePropertyComparator('createdAt');

export const dateStartSort = makePropertyComparator('dateStart');

export const dateEndSort = makePropertyComparator('dateEnd');
