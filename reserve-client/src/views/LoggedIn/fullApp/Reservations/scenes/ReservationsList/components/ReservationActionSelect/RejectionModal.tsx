/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import { Modal } from 'antd';
import { TextArea } from '@reserve/client/components/Input';
//#endregion

export interface Props {
  readonly visible: boolean;
  readonly onCancel: () => any;
  readonly onConfirm: (reason: string) => any;
}

const RejectionModal: React.FunctionComponent<Props> = props => {
  const [reason, setReason] = useState('');
  const { visible, onCancel, onConfirm } = props;

  function handleCancel() {
    onCancel();
    setReason('');
  }

  function handleConfirm() {
    onConfirm(reason);
    setReason('');
  }

  return (
    <Modal
      title="Please specify the reason for rejection"
      okText="Reject"
      okType="danger"
      visible={visible}
      onCancel={handleCancel}
      onOk={handleConfirm}
      okButtonProps={{ disabled: !reason.length }}
      cancelButtonProps={{ hidden: true }}
      centered
    >
      <React.Fragment>
        <p>Please specify the reason for the reservation rejection:</p>
        <TextArea value={reason} onChange={setReason} />
      </React.Fragment>
    </Modal>
  );
};

export default RejectionModal;
