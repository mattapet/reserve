/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import * as R from 'ramda';
import { Table, PageHeader } from 'antd';
import { SortInfo } from '@reserve/client/lib/types/SortInfo';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { useQueryControlledTable } from '@reserve/client/lib/hooks';
import { useReservations } from '@reserve/client/services/hooks/useReservations';
import { useResources } from '@reserve/client/services/hooks/useResources';

import CreateReservationLink from './components/CreateReservationLink';
import { columns } from './columns';
//#endregion

//#region Component interfaces
export interface Props {
  readonly meta: {
    readonly pending: boolean;
    readonly error?: Error;
  };
  readonly onCancel: (reservationId: string) => any;
  readonly onConfirm: (reservationId: string) => any;
  readonly onReject: (reservationId: string, reason: string) => any;
}
//#endregion

const ReservationsList: React.FunctionComponent<Props> = (props) => {
  const defaultSort: SortInfo<Reservation> = {
    columnKey: 'createdAt',
    order: 'descend',
  };
  const {
    filterInfo,
    sortInfo,
    pagination,
    handlePaginationChange,
    handleTableChange,
  } = useQueryControlledTable<Reservation>(defaultSort);
  const { current, pageSize } = pagination ?? {};

  const { reservations, meta } = useReservations();
  const { resources, meta: resourceMeta } = useResources();
  const loading =
    !meta.retrieved || !resourceMeta.retrieved || props.meta.pending;

  return (
    <PageHeader title="Reservations" extra={[<CreateReservationLink />]}>
      <Table
        dataSource={reservations}
        columns={columns(
          resources,
          sortInfo,
          filterInfo,
          props.onCancel,
          props.onConfirm,
          props.onReject
        )}
        loading={loading}
        rowKey={R.prop('id')}
        onChange={handleTableChange}
        pagination={{
          current,
          pageSize,
          onChange: handlePaginationChange,
        }}
      />
    </PageHeader>
  );
};

export default ReservationsList;
