/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region importrs
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { ReservationState } from '@reserve/client/lib/types/ReservationState';

import { createdAtSorter, dateStartSort, dateEndSort } from '../sorters';
//#endregion

describe('reservation sorters', () => {
  let id: number = 0;
  function make_defaultReservation(): Reservation {
    return {
      allDay: false,
      createdAt: new Date(0),
      dateEnd: new Date(100),
      dateStart: new Date(200),
      id: `${id++}`,
      resourceIds: [],
      state: ReservationState.requested,
      userId: 'test',
    };
  }

  function make_reservaitonWithCreatedAt(createdAt: Date): Reservation {
    return { ...make_defaultReservation(), createdAt };
  }

  function make_reservaitonDateStart(dateStart: Date): Reservation {
    return { ...make_defaultReservation(), dateStart };
  }

  function make_reservaitonDateEnd(dateEnd: Date): Reservation {
    return { ...make_defaultReservation(), dateEnd };
  }

  describe('createdAt sort', () => {
    it('should return positive number if reservations are in descending order', () => {
      const lhs = make_reservaitonWithCreatedAt(new Date(200));
      const rhs = make_reservaitonWithCreatedAt(new Date(0));

      const result = createdAtSorter(lhs, rhs);

      expect(result).toBeGreaterThan(0);
    });

    it('should return negative number if reservations are in ascending order', () => {
      const lhs = make_reservaitonWithCreatedAt(new Date(0));
      const rhs = make_reservaitonWithCreatedAt(new Date(200));

      const result = createdAtSorter(lhs, rhs);

      expect(result).toBeLessThan(0);
    });

    it('should return zero if reservations were created at the same time', () => {
      const lhs = make_reservaitonWithCreatedAt(new Date(200));
      const rhs = make_reservaitonWithCreatedAt(new Date(200));

      const result = createdAtSorter(lhs, rhs);

      expect(result).toBe(0);
    });
  });

  describe('dateStart sort', () => {
    it('should return positive number if reservations are in descending order', () => {
      const lhs = make_reservaitonDateStart(new Date(200));
      const rhs = make_reservaitonDateStart(new Date(0));

      const result = dateStartSort(lhs, rhs);

      expect(result).toBeGreaterThan(0);
    });

    it('should return negative number if reservations are in ascending order', () => {
      const lhs = make_reservaitonDateStart(new Date(0));
      const rhs = make_reservaitonDateStart(new Date(200));

      const result = dateStartSort(lhs, rhs);

      expect(result).toBeLessThan(0);
    });

    it('should return zero if reservations were created at the same time', () => {
      const lhs = make_reservaitonDateStart(new Date(200));
      const rhs = make_reservaitonDateStart(new Date(200));

      const result = dateStartSort(lhs, rhs);

      expect(result).toBe(0);
    });
  });

  describe('dateEnd sort', () => {
    it('should return positive number if reservations are in descending order', () => {
      const lhs = make_reservaitonDateEnd(new Date(200));
      const rhs = make_reservaitonDateEnd(new Date(0));

      const result = dateEndSort(lhs, rhs);

      expect(result).toBeGreaterThan(0);
    });

    it('should return negative number if reservations are in ascending order', () => {
      const lhs = make_reservaitonDateEnd(new Date(0));
      const rhs = make_reservaitonDateEnd(new Date(200));

      const result = dateEndSort(lhs, rhs);

      expect(result).toBeLessThan(0);
    });

    it('should return zero if reservations were created at the same time', () => {
      const lhs = make_reservaitonDateEnd(new Date(200));
      const rhs = make_reservaitonDateEnd(new Date(200));

      const result = dateEndSort(lhs, rhs);

      expect(result).toBe(0);
    });
  });
});
