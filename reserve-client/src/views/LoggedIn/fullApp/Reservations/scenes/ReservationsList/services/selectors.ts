/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch, bindActionCreators } from 'redux';
import { createSelector } from 'reselect';
import { reservationEditMetaSelector } from '@reserve/client/services/reservationSelectors';
import { reservationActions } from '@reserve/client/actions';
//#endregion

export const mapStateToProps = createSelector(
  reservationEditMetaSelector,
  meta => ({ meta })
);

export const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      onCancel: reservationActions.cancelById.request,
      onConfirm: reservationActions.confirmById.request,
      onReject: reservationActions.rejectById.request,
    },
    dispatch
  );
