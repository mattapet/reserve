/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import { Menu, Dropdown, Icon } from 'antd';
import { ReservationState } from '@reserve/client/lib/types/ReservationState';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import RejectionModal from './RejectionModal';
//#endregion

export interface Props {
  readonly reservation: Reservation;
  readonly onConfirm: (reservationId: string) => any;
  readonly onCancel: (reservationId: string) => any;
  readonly onReject: (reservationId: string, reason: string) => any;
}

interface ActionProps {
  readonly onConfirm: () => any;
  readonly onCancel: () => any;
  readonly onReject: () => any;
}

function getApplicableActions(
  reservation: Reservation,
  actions: ActionProps
): React.ReactNode[] {
  switch (reservation.state) {
    case ReservationState.requested:
      return [
        <Menu.Item key="confirm" onClick={actions.onConfirm}>
          Confirm
        </Menu.Item>,
        <Menu.Item key="reject" onClick={actions.onReject}>
          Reject
        </Menu.Item>,
      ];
    case ReservationState.confirmed:
      return [
        <Menu.Item key="cancel" onClick={actions.onCancel}>
          Cancel
        </Menu.Item>,
      ];
    case ReservationState.canceled:
    case ReservationState.rejected:
    case ReservationState.completed:
      return [];
  }
}

const ReservationActionSelect: React.FunctionComponent<Props> = props => {
  const [showRejectionModal, setShowRejectionModal] = useState(false);
  const { reservation, onConfirm, onCancel, onReject } = props;
  const applicableActions = getApplicableActions(reservation, {
    onCancel: () => onCancel(reservation.id),
    onConfirm: () => onConfirm(reservation.id),
    onReject: handleRejectionSelect,
  });

  function handleRejectionSelect() {
    setShowRejectionModal(true);
  }

  function handleRejectionCancel() {
    setShowRejectionModal(false);
  }

  function handleRejectionConfirm(reason: string) {
    setShowRejectionModal(false);
    onReject(reservation.id, reason);
  }

  return (
    <React.Fragment>
      <Dropdown
        overlay={<Menu selectedKeys={[]}>{applicableActions}</Menu>}
        trigger={['click']}
      >
        {/* eslint-disable-next-line */}
        <a href="javascript:;">
          State <Icon type="down" />
        </a>
      </Dropdown>
      <RejectionModal
        visible={showRejectionModal}
        onCancel={handleRejectionCancel}
        onConfirm={handleRejectionConfirm}
      />
    </React.Fragment>
  );
};

export default ReservationActionSelect;
