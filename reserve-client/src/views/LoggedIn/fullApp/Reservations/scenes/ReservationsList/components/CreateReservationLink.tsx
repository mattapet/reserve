/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Icon } from 'antd';
import { Link } from 'react-router-dom';
//#endregion

export interface Props {}

const CreateReservationLink: React.FunctionComponent<Props> = props => (
  <Link to="/reservations/create">
    <Icon type="form" /> Create Reservation
  </Link>
);

export default CreateReservationLink;
