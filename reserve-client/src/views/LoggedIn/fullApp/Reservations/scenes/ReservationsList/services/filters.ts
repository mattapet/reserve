/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region importrs
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { ReservationState } from '@reserve/client/lib/types/ReservationState';
//#endregion

export function stateFilter(
  state: ReservationState,
  reservation: Reservation
): boolean {
  return state === reservation.state;
}

export function resourceFilter(
  resourceId: string,
  reservation: Reservation
): boolean {
  return reservation.resourceIds.map(String).includes(resourceId);
}
