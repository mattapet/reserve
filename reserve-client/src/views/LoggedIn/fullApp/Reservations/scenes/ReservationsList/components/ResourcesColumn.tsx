/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import * as R from 'ramda';
import { useSelector } from 'react-redux';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { resourceDataSelector } from '@reserve/client/services/resourceSelectors';
//#endregion

export interface Props {
  readonly reservation: Reservation;
}

const ResourcesColumn: React.FunctionComponent<Props> = props => {
  const resources = useSelector(resourceDataSelector);
  const reservedResources = R.map(
    R.pipe(
      (resourceId: number) => resources[resourceId],
      resource => resource?.name ?? 'Loading...'
    ),
    props.reservation.resourceIds
  );

  return <>{reservedResources.join(', ')}</>;
};

export default ResourcesColumn;
