/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region importrs
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { ReservationState } from '@reserve/client/lib/types/ReservationState';

import { stateFilter } from '../filters';
//#endregion

describe('reservation filters', () => {
  let id: number = 0;
  function make_defaultReservation(): Reservation {
    return {
      allDay: false,
      createdAt: new Date(0),
      dateEnd: new Date(100),
      dateStart: new Date(200),
      id: `${id++}`,
      resourceIds: [],
      state: ReservationState.requested,
      userId: 'test',
    };
  }

  function make_reservaitonWithState(state: ReservationState): Reservation {
    return { ...make_defaultReservation(), state };
  }

  describe('reservation state filter', () => {
    it('should return true is state matches the filter', () => {
      const state = ReservationState.requested;
      const reservation = make_reservaitonWithState(state);

      const result = stateFilter(state, reservation);

      expect(result).toBe(true);
    });

    it('should return true is state matches the filter', () => {
      const state = ReservationState.completed;
      const reservation = make_reservaitonWithState(state);

      const result = stateFilter(state, reservation);

      expect(result).toBe(true);
    });

    it('should return false is state matches the filter', () => {
      const state = ReservationState.completed;
      const reservation = make_reservaitonWithState(state);

      const result = stateFilter(ReservationState.canceled, reservation);

      expect(result).toBe(false);
    });
  });
});
