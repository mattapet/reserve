/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Form, Comment, Button } from 'antd';

import TextArea from '@reserve/client/components/Input/TextArea';
//#endregion

//#region Component interfaces
export interface Props {
  readonly value: string;
  readonly pending: boolean;
  readonly onChange: (value: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLElement>) => any;
}
//#endregion

const CommentBox: React.FunctionComponent<Props> = props => (
  <Comment
    content={
      <div>
        <Form onSubmit={props.onSubmit}>
          <TextArea
            rows={4}
            value={props.value}
            onChange={props.onChange}
            placeholder="Add a comment..."
            disabled={props.pending}
          />
          <Button
            type="primary"
            htmlType="submit"
            loading={props.pending}
            disabled={!props.value.length}
          >
            Submit
          </Button>
        </Form>
      </div>
    }
  />
);

export default CommentBox;
