/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import moment from 'moment';
import { Card, Skeleton, Comment, List } from 'antd';

import AsyncUserNameLink from '@reserve/client/components/AsyncUserNameLink';
import { ReservationComment } from '@reserve/client/lib/types/ReservationComment';
import CommentBox from './CommentBox';
//#endregion

//#region Component interfaces
export interface Props {
  readonly loading: boolean;
  readonly pending: boolean;
  readonly comment: string;
  readonly comments: ReservationComment[];
  readonly onChange: (value: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLElement>) => any;
}
//#endregion

const View: React.FunctionComponent<Props> = props => (
  <Card>
    <Skeleton active loading={props.loading}>
      {props.comments.length > 0 && (
        <List
          dataSource={props.comments}
          rowKey={item => item.id}
          renderItem={item => (
            <Comment
              author={<AsyncUserNameLink userId={item.author} />}
              datetime={moment(item.timestamp).fromNow()}
              content={item.comment}
            />
          )}
        />
      )}
      <CommentBox
        value={props.comment}
        pending={props.pending}
        onChange={props.onChange}
        onSubmit={props.onSubmit}
      />
    </Skeleton>
  </Card>
);

export default View;
