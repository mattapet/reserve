/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import * as R from 'ramda';
import { PageHeader } from 'antd';
import { useParams, useHistory } from 'react-router';
import { useParamReservation } from '@reserve/client/services/hooks/useParamReservation';
import { useResources } from '@reserve/client/services/hooks/useResources';
import ReservationDetails from './components/ReservationDetails';
import EditReservationLink from './components/EditReservationLink';
//#endregion

//#region Component interfaces
export interface Props {}
//#endregion

const ReservationDetailsWrapper: React.FunctionComponent<Props> = props => {
  const { id } = useParams();
  const history = useHistory();
  const reservation = useParamReservation();
  const { resources } = useResources();
  const retrieved = resources.length !== 0 && reservation != null;

  const selectedResources = R.filter(
    resource => R.includes(resource.id, reservation?.resourceIds ?? []),
    resources
  );

  return (
    <PageHeader
      title="Reservations"
      subTitle="Reservation details"
      extra={[<EditReservationLink id={id!} reservation={reservation} />]}
      onBack={() => history.goBack()}
    >
      <ReservationDetails
        retrieved={retrieved}
        reservation={reservation}
        resources={selectedResources}
      />
    </PageHeader>
  );
};

export default ReservationDetailsWrapper;
