/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Card, Descriptions } from 'antd';
import ReservationStateTag from '@reserve/client/components/ReservationStateTag';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { Resource } from '@reserve/client/lib/types/Resource';
import { date } from '@reserve/client/lib/dateFormatter';
import AssignedToLink from '../../../components/AssignedToLink';
import CommentList from './CommentList';
//#endregion

//#region Component interfaces
export interface Props {
  readonly retrieved: boolean;
  readonly reservation?: Reservation;
  readonly resources: Resource[];
}
//#endregion

const ReservationDetails: React.FunctionComponent<Props> = props => (
  <React.Fragment>
    <Card loading={!props.retrieved}>
      <Descriptions>
        <Descriptions.Item label="Reservation start">
          {date(props.reservation?.dateStart ?? new Date(Date.now()))}
        </Descriptions.Item>
        <Descriptions.Item label="Reservation end">
          {date(props.reservation?.dateEnd ?? new Date(Date.now()))}
        </Descriptions.Item>
        <Descriptions.Item label="Reservation state">
          <ReservationStateTag state={props.reservation?.state!} />
        </Descriptions.Item>
        <Descriptions.Item label="Assigned to">
          <AssignedToLink userId={props.reservation?.userId!} />
        </Descriptions.Item>
        <Descriptions.Item label="Resources">
          {props.resources.map(resource => (
            <p>{resource.name}</p>
          ))}
        </Descriptions.Item>
        {props.reservation?.notes && (
          <Descriptions.Item label="Additional notes">
            <p>{props.reservation.notes}</p>
          </Descriptions.Item>
        )}
      </Descriptions>
    </Card>
    <CommentList />
  </React.Fragment>
);

export default ReservationDetails;
