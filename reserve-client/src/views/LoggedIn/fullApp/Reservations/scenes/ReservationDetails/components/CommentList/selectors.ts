/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from '@reserve/client/actions/reservationComments';
//#endregion

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  create: (comment: string, reservationId: string) =>
    dispatch(actions.create.request({ reservationId, comment })),
});
