/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { useReservationComments } from '@reserve/client/services/hooks/useReservationComments';
import { reservationCommentsEditMetaSelector } from '@reserve/client/services/reservationSelectors';

import View from './view';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly create: (comment: string, reservationId: string) => any;
}
//#endregion

const CommentList: React.FunctionComponent<Props> = props => {
  const [comment, setComment] = useState('');
  const reservationId = (props.match.params as any).id;

  const { comments, meta } = useReservationComments();
  const editMeta = useSelector(reservationCommentsEditMetaSelector);

  function handleSubmit(e: React.FormEvent<HTMLElement>) {
    e.preventDefault();
    props.create(comment, reservationId);
    setComment('');
  }

  return (
    <View
      loading={!meta.retrieved}
      pending={editMeta.pending}
      comment={comment}
      comments={comments}
      onChange={setComment}
      onSubmit={handleSubmit}
    />
  );
};

export default withRouter(CommentList);
