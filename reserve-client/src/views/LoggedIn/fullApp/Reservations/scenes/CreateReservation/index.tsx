/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { PageHeader, Skeleton } from 'antd';
import { useHistory } from 'react-router';
import ReservationFormWrapper from './ReservationFormWrapper';
import { useTemplate } from './services/useTemplate';
//#endregion

export interface Props {}

const CreateReservation: React.FunctionComponent<Props> = (props) => {
  const history = useHistory();
  const { template, retrieved } = useTemplate();

  return (
    <PageHeader
      title="Reservations"
      subTitle="Create new reservation"
      onBack={() => history.goBack()}
    >
      <Skeleton loading={!retrieved}>
        <ReservationFormWrapper
          onComplete={() => history.goBack()}
          template={template}
        />
      </Skeleton>
    </PageHeader>
  );
};

export default CreateReservation;
