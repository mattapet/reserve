/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  reservationTemplateSelector,
  reservationTemplateMetaSelector,
} from '@reserve/client/services/reservationSelectors';
import { templateActions } from '@reserve/client/actions';
//#endregion

export function useTemplate() {
  const dispatch = useDispatch();
  const template = useSelector(reservationTemplateSelector);
  const meta = useSelector(reservationTemplateMetaSelector);

  useEffect(() => {
    console.log(meta);
    if (!meta.retrieved && !meta.pending) {
      dispatch(templateActions.fetch.request());
    }
  }, [meta, dispatch]);

  return { template, ...meta };
}
