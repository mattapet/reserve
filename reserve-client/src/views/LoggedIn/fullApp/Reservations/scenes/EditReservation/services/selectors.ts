/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createSelector } from 'reselect';
import { Dispatch, bindActionCreators } from 'redux';
import { reservationActions } from '@reserve/client/actions';
import { userIdentitySelector } from '@reserve/client/services/authSelectors';
import { reservationEditMetaSelector } from '@reserve/client/services/reservationSelectors';
//#endregion

export const mapStateToProps = createSelector(
  userIdentitySelector,
  reservationEditMetaSelector,
  (identity, meta) => ({
    userId: identity.id,
    meta,
  })
);

export const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ onSubmit: reservationActions.edit.request }, dispatch);
