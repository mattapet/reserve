/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { PageHeader } from 'antd';
import { useHistory } from 'react-router';
import ReservationFormWrapper from './ReservationFormWrapper';
//#endregion

export interface Props {}

const CreateReservation: React.FunctionComponent<Props> = props => {
  const history = useHistory();

  return (
    <PageHeader
      title="Reservations"
      subTitle="Edit reservation"
      onBack={() => history.goBack()}
    >
      <ReservationFormWrapper onComplete={() => history.goBack()} />
    </PageHeader>
  );
};

export default CreateReservation;
