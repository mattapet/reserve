/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Skeleton } from 'antd';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './services/selectors';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { useParamReservation } from '@reserve/client/services/hooks/useParamReservation';
import ReservationForm, {
  Props as FormProps,
} from '../../components/ReservationForm';
//#endregion

export interface Props extends Omit<FormProps, 'onSubmit'> {
  readonly onSubmit: (reservation: Reservation) => any;
}

const ReservationFormWrapper: React.FunctionComponent<Props> = props => {
  const { onSubmit, ...formProps } = props;
  const reservation = useParamReservation();

  function handleSubmit(payload: {
    dateStart: Date;
    dateEnd: Date;
    resourceIds: number[];
    userId: string;
    notes: string;
  }) {
    onSubmit({ ...reservation!, ...payload });
  }

  return (
    <Skeleton loading={!reservation}>
      <ReservationForm
        defaultValue={reservation}
        {...formProps}
        onSubmit={handleSubmit}
      />
    </Skeleton>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReservationFormWrapper);
