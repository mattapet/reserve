/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Link } from 'react-router-dom';
import { useUserById } from '@reserve/client/services/hooks/useUserById';
//#endregion

export interface Props {
  readonly userId: string;
}

const AssignedToLink: React.FunctionComponent<Props> = props => {
  const user = useUserById(props.userId);

  if (!user) {
    return <i>Loading...</i>;
  } else {
    return (
      <Link to={`/users/${props.userId}/details`}>
        {`${user.profile!.firstName} ${user.profile!.lastName}`}
      </Link>
    );
  }
};

export default AssignedToLink;
