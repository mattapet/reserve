/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imoports
import React from 'react';
import { Select } from 'antd';
import { Resource } from '@reserve/client/lib/types/Resource';
//#endregion

export interface Props {
  readonly resources: Resource[];
  readonly disabled: boolean;
  readonly selectedResources: number[];
  readonly onSelectedResourcesChange: (value: number[]) => any;
}

const ResourceSelect: React.FunctionComponent<Props> = props => (
  <Select
    mode="multiple"
    value={props.selectedResources}
    onChange={props.onSelectedResourcesChange}
    disabled={props.disabled}
  >
    {props.resources.map(resource => (
      <Select.Option key={`${resource.id}`} value={resource.id}>
        {resource.name}
      </Select.Option>
    ))}
  </Select>
);

export default ResourceSelect;
