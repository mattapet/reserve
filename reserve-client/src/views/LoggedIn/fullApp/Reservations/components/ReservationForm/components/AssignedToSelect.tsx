/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imoports
import React from 'react';
import * as R from 'ramda';
import { Select } from 'antd';
import { User } from '@reserve/client/lib/types/User';
import * as filters from '@reserve/client/services/users/filters';
//#endregion

export interface Props {
  readonly users: User[];
  readonly disabled: boolean;
  readonly assignedTo: string;
  readonly onAssignedToChanged: (value: string) => any;
}

const ResourceSelect: React.FunctionComponent<Props> = props => (
  <Select
    value={props.assignedTo}
    onChange={props.onAssignedToChanged}
    disabled={props.disabled}
    filterOption={(query, item) =>
      R.pipe(
        (userId: string) => props.users[userId!],
        filters.searchFilter(query)
      )(item.props.value! as string)
    }
  >
    {props.users.map(user => (
      <Select.Option key={user.id} value={user.id}>
        {`${user.profile!.firstName} ${user.profile!.lastName}`}
      </Select.Option>
    ))}
  </Select>
);

export default ResourceSelect;
