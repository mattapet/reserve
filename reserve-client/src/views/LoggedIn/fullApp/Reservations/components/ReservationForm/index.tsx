/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState, useEffect } from 'react';
import moment, { Moment } from 'moment';
import { usePrevious } from '@reserve/client/lib/hooks';
import { useResources } from '@reserve/client/services/hooks/useResources';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { useUsers } from '@reserve/client/services/hooks/useUsers';
import View from './view';
//#endregion

//#region Component interfaces
export interface Props {
  readonly defaultValue?: Reservation;
  readonly template?: string;
  readonly userId: string;
  readonly meta: {
    readonly pending: boolean;
    readonly error?: Error;
  };
  readonly onSubmit: (payload: {
    dateStart: Date;
    dateEnd: Date;
    resourceIds: number[];
    userId: string;
    notes: string;
  }) => any;
  readonly onComplete: () => any;
}
//#endregion

const ReservationForm: React.FunctionComponent<Props> = (props) => {
  const { defaultValue, onComplete } = props;
  const [dateStart, setDateStart] = useState<Moment | null | undefined>(
    defaultValue ? moment(defaultValue.dateStart) : null
  );
  const [dateEnd, setDateEnd] = useState<Moment | null | undefined>(
    defaultValue ? moment(defaultValue.dateEnd) : null
  );
  const [selectedResources, setSelectedResources] = useState(
    defaultValue?.resourceIds ?? []
  );
  const [assignedTo, setAssignedTo] = useState(
    defaultValue?.userId ?? props.userId
  );
  const [additionalNotes, setAdditionalNotes] = useState(
    defaultValue?.notes ?? props.template ?? ''
  );

  const { resources, meta: resourcesMeta } = useResources();
  const { users, meta: usersMeta } = useUsers();

  const prevPending = usePrevious(props.meta.pending);

  function resetState() {
    setDateStart(defaultValue ? moment(defaultValue.dateStart) : null);
    setDateEnd(defaultValue ? moment(defaultValue.dateEnd) : null);
    setSelectedResources(defaultValue?.resourceIds ?? []);
    setAssignedTo(props.userId);
    setAdditionalNotes(defaultValue?.notes ?? '');
  }

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    props.onSubmit({
      dateStart: dateStart!.toDate(),
      dateEnd: dateEnd!.toDate(),
      resourceIds: selectedResources,
      userId: assignedTo,
      notes: additionalNotes,
    });
  }

  const { pending, error } = props.meta;
  useEffect(() => {
    if (prevPending && !pending && !error) {
      onComplete();
    }
  }, [prevPending, pending, error, onComplete]);

  return (
    <View
      meta={{
        retrieved: resourcesMeta.retrieved && usersMeta.retrieved,
        pending: props.meta.pending,
        error: props.meta.error ?? usersMeta.error ?? resourcesMeta.error,
      }}
      resources={resources}
      users={users}
      dateStart={dateStart}
      dateEnd={dateEnd}
      selectedResources={selectedResources}
      assignedTo={assignedTo}
      notes={additionalNotes}
      onDateStartChange={setDateStart}
      onDateEndChange={setDateEnd}
      onSelectedResourcesChange={setSelectedResources}
      onAssignedToChange={setAssignedTo}
      onNotesChange={setAdditionalNotes}
      onSubmit={handleSubmit}
      onReset={resetState}
    />
  );
};

export default ReservationForm;
