/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import * as R from 'ramda';
import { PageHeader, Table } from 'antd';
import { useDispatch } from 'react-redux';
import { useAnnouncements } from '@reserve/client/services/hooks/useAnnouncements';
import { workspaceActions } from '@reserve/client/actions';

import CreateAnnouncementLink from './components/CreateAnnouncementLink';
import { columns } from './columns';
//#endregion

const Announcements: React.FunctionComponent = () => {
  const { announcements, meta } = useAnnouncements();
  const dispatch = useDispatch();

  function handleToggle(name: string) {
    dispatch(workspaceActions.toggleAnnouncement.request(name));
  }

  return (
    <PageHeader title="Announcements" extra={[<CreateAnnouncementLink />]}>
      <Table
        dataSource={announcements}
        rowKey={R.prop('name')}
        loading={!meta.retrieved}
        columns={columns(handleToggle)}
      />
    </PageHeader>
  );
};

export default Announcements;
