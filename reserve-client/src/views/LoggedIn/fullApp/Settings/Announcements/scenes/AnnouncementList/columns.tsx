/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react';
import { ColumnProps } from 'antd/lib/table';
import { Badge, Button } from 'antd';
import { WorkspaceAnnouncement } from '@reserve/client/lib/types/WorkspaceAnnouncement';

export const columns = (
  onToggle: (name: string) => any
): ColumnProps<WorkspaceAnnouncement>[] => [
  {
    title: 'Name',
    dataIndex: 'name',
  },
  {
    title: 'Text',
    dataIndex: 'text',
  },
  {
    title: 'Active',
    render: (_, item) =>
      item.active ? (
        <Badge status="success" text="Yes" />
      ) : (
        <Badge status="error" text="No" />
      ),
  },
  {
    title: 'Actions',
    render: (_, item) =>
      item.active ? (
        <Button onClick={() => onToggle(item.name)}>Deactivate</Button>
      ) : (
        <Button onClick={() => onToggle(item.name)}>Activate</Button>
      ),
  },
];
