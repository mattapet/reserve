/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { PageHeader } from 'antd';
import { workspaceActions } from '@reserve/client/actions';
import { announcementCreateMeta } from '@reserve/client/services/announcementSelectors';

import View from './view';
//#endregion

const CreateAnnouncement: React.FunctionComponent = () => {
  const [name, setName] = useState('');
  const [text, setText] = useState('');
  const history = useHistory();
  const dispatch = useDispatch();
  const { pending } = useSelector(announcementCreateMeta);

  function handleSubmit(e: React.FormEvent) {
    e.preventDefault();
    dispatch(workspaceActions.createAnnouncement.request({ name, text }));
  }

  return (
    <PageHeader
      title="Announcements"
      subTitle="Create new announcement"
      onBack={() => history.goBack()}
    >
      <View
        pending={pending}
        name={name}
        onNameChange={setName}
        text={text}
        onTextChange={setText}
        onSubmit={handleSubmit}
      />
    </PageHeader>
  );
};

export default CreateAnnouncement;
