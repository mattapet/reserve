/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react';
import { Icon } from 'antd';
import { Link } from 'react-router-dom';

const CreateAnnouncementLink: React.FunctionComponent = () => (
  <Link to="/settings/announcements/create">
    <Icon type="form" /> Create Announcement
  </Link>
);

export default CreateAnnouncementLink;
