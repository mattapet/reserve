/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Form, Button, Card } from 'antd';
import Input, { TextArea } from '@reserve/client/components/Input';
//#endregion

export interface Props {
  readonly pending: boolean;
  readonly name: string;
  readonly onNameChange: (value: string) => any;
  readonly text: string;
  readonly onTextChange: (value: string) => any;
  readonly onSubmit: (e: React.FormEvent) => any;
}

const View: React.FunctionComponent<Props> = props => (
  <Card>
    <Form onSubmit={props.onSubmit}>
      <Form.Item label="Announcement name" required>
        <Input
          placeholder="Name"
          value={props.name}
          onChange={props.onNameChange}
          disabled={props.pending}
          required
        />
      </Form.Item>
      <Form.Item label="Announcement" required>
        <TextArea
          rows={4}
          value={props.text}
          onChange={props.onTextChange}
          disabled={props.pending}
          required
        />
      </Form.Item>
      <Button
        htmlType="submit"
        type="primary"
        loading={props.pending}
        disabled={!props.name.length || !props.text.length}
      >
        Submit
      </Button>
    </Form>
  </Card>
);

export default View;
