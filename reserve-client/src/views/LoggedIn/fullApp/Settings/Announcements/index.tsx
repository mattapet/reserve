/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import NotFound from '@reserve/client/views/NotFound';
import AnnouncementList from './scenes/AnnouncementList';
import CreateAnnouncement from './scenes/CreateAnnouncement';
//#endregion

const Router: React.FunctionComponent<any> = () => (
  <Switch>
    <Route path="/settings/announcements" component={AnnouncementList} exact />
    <Route
      path="/settings/announcements/create"
      component={CreateAnnouncement}
      exact
    />
    <Route component={NotFound} />
  </Switch>
);

export default Router;
