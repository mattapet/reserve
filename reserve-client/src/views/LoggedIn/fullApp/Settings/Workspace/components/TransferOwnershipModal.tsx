/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import { Modal, Select } from 'antd';
import styled from 'styled-components';
import Input from '@reserve/client/components/Input';
import { User } from '@reserve/client/lib/types/User';
import { useUsers } from '@reserve/client/services/hooks/useUsers';
//#endregion

//#region Component interface
export interface Props {
  readonly visible: boolean;
  readonly workspaceName: string;
  readonly confirmValue: string;
  readonly onConfirmValueChanged: (confirmValue: string) => any;
  readonly onSubmit: (userId: string) => any;
  readonly onCancel: () => any;
}

const UserSelect = styled(Select as any)`
  width: 100%;
`;
//#endregion

const TransferOwnershipModal: React.FunctionComponent<Props> = (props) => {
  const { users, meta } = useUsers();
  const [transferTo, setTransferTo] = useState<string | null>(null);

  function handlerCancel() {
    setTransferTo(null);
    props.onCancel();
  }

  return (
    <Modal
      title="Are you absolutely sure?"
      visible={props.visible}
      onOk={() => props.onSubmit(transferTo!)}
      onCancel={handlerCancel}
      okButtonProps={{
        style: { width: '100%' },
        disabled: props.workspaceName !== props.confirmValue || !transferTo,
      }}
      okText="I understand, transfer ownership of this workspace"
      okType="danger"
      cancelButtonProps={{ hidden: true }}
    >
      <React.Fragment>
        <p>
          This action <b>cannot</b> be undone. Continuing with this action will
          transfer ownership to the selected users. You will no longer be able
          to access any owner-specific aspects of the workspace{' '}
          <b>{props.workspaceName}</b>.
        </p>
        <p>
          Select the user that the ownership of the workspace should be
          transfered to
        </p>
        <UserSelect
          loading={meta.pending}
          value={transferTo}
          onChange={setTransferTo}
        >
          {users
            .filter((user: User) => !user.isOwner)
            .map((user: User) => (
              <Select.Option key={user.id} value={user.id}>
                {`${user.profile?.firstName} ${user.profile?.lastName}`}
              </Select.Option>
            ))}
        </UserSelect>
        <p>Please type in the name of the workspace to confirm.</p>
        <Input
          value={props.confirmValue}
          onChange={props.onConfirmValueChanged}
        />
      </React.Fragment>
    </Modal>
  );
};
export default TransferOwnershipModal;
