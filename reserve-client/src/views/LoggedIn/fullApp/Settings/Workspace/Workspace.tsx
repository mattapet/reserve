/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import config from '@reserve/client/config';

import View, { ModalType } from './view';
//#endregion

//#region Component interfaces
export interface Props {
  readonly onTransferOwnership: (payload: { to: string }) => any;
  readonly onDelete: () => any;
}
//#endregion

const WorkspaceSettings: React.FunctionComponent<Props> = (props) => {
  const [displayModal, setDisplayModal] = useState<ModalType | null>(null);
  const [confirmValue, setConfirmValue] = useState('');

  function handleDeleteConfirm() {
    props.onDelete();
  }

  function handleTransferOwnership(to: string) {
    props.onTransferOwnership({ to });
  }

  function handleCancel() {
    setConfirmValue('');
    setDisplayModal(null);
  }

  return (
    <View
      workspaceName={config.workspaceName}
      displayModal={displayModal}
      onDisplayModalChange={setDisplayModal}
      confirmValue={confirmValue}
      onConfirmValueChanged={setConfirmValue}
      onTransferConfirm={handleTransferOwnership}
      onDeleteConfirm={handleDeleteConfirm}
      onCancel={handleCancel}
    />
  );
};

export default WorkspaceSettings;
