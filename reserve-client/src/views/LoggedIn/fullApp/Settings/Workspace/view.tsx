/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Button, Modal } from 'antd';

import Input from '@reserve/client/components/Input';
import Entry from '../components/form/Entry';
import Section from '../components/form/Section';

import TransferOwnershipModal from './components/TransferOwnershipModal';
//#endregion

//#region Component interfaces
export interface Props {
  readonly workspaceName: string;
  readonly displayModal: ModalType | null;
  readonly onDisplayModalChange: (type: ModalType) => any;
  readonly confirmValue: string;
  readonly onConfirmValueChanged: (e: string) => any;
  readonly onDeleteConfirm: () => any;
  readonly onTransferConfirm: (userId: string) => any;
  readonly onCancel: () => any;
}

export enum ModalType {
  TRANSFER_OWNERSHIP = 'TRANSFER_OWNERSHIP',
  DELETE_WORKSPACE = 'DELETE_WORKSPACE',
}
//#endregion

const View: React.FunctionComponent<Props> = (props) => (
  <Section title="Danger zone">
    <Entry label="Transfer ownership">
      <Button
        size="large"
        type="danger"
        onClick={() => props.onDisplayModalChange(ModalType.TRANSFER_OWNERSHIP)}
      >
        <span>
          Transfer ownership of workspace <b>{props.workspaceName}</b>
        </span>
      </Button>
      <TransferOwnershipModal
        {...props}
        visible={props.displayModal === ModalType.TRANSFER_OWNERSHIP}
        onSubmit={props.onTransferConfirm}
      />
    </Entry>

    <Entry label="Delete workspace">
      <Button
        size="large"
        type="danger"
        onClick={() => props.onDisplayModalChange(ModalType.DELETE_WORKSPACE)}
      >
        <span>
          Delete workspace <b>{props.workspaceName}</b>
        </span>
      </Button>
      <Modal
        title="Are you absolutely sure?"
        visible={props.displayModal === ModalType.DELETE_WORKSPACE}
        onOk={props.onDeleteConfirm}
        onCancel={props.onCancel}
        okButtonProps={{
          style: { width: '100%' },
          disabled: props.workspaceName !== props.confirmValue,
        }}
        okText="I understand, delete this workspace"
        okType="danger"
        cancelButtonProps={{ hidden: true }}
      >
        <React.Fragment>
          <p>
            This action <b>cannot</b> be undone. This will permanently delete
            the <b>{props.workspaceName}</b> workspace with all the data that
            may be associated with it including resources, reservations and user
            information.
          </p>
          <p>Please type in the name of the workspace to confirm.</p>
          <Input
            value={props.confirmValue}
            onChange={props.onConfirmValueChanged}
          />
        </React.Fragment>
      </Modal>
    </Entry>
  </Section>
);

export default View;
