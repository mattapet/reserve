/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState, useEffect } from 'react';
import View from './view';
import { useEditTemplate } from './services/useEditTemplate';
import { usePrevious } from '@reserve/client/lib/hooks';
//#endregion

const Template: React.FunctionComponent = () => {
  const {
    pending,
    retrieved,
    template,
    deleteTemplate,
    setTemplate,
  } = useEditTemplate();
  const [editting, setEditting] = useState(false);
  const [value, setValue] = useState(template ?? '');

  function handleSubmit(e: React.FormEvent) {
    e.preventDefault();
    setTemplate(value);
  }

  function handleReset() {
    setValue(template ?? '');
    setEditting(false);
  }

  const prevPending = usePrevious(pending);
  useEffect(() => {
    if (prevPending && !pending) {
      setEditting(false);
    }
  }, [pending, prevPending]);

  return (
    <View
      isEditting={editting}
      pending={pending}
      retrieved={retrieved}
      template={template ?? ''}
      value={value}
      onChange={setValue}
      onSubmit={handleSubmit}
      onDelete={deleteTemplate}
      onCancel={handleReset}
      onEdit={() => setEditting(true)}
    />
  );
};

export default Template;
