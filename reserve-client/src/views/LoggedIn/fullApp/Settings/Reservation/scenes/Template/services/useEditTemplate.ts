/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  reservationTemplateSelector,
  reservationTemplateMetaSelector,
} from '@reserve/client/services/reservationSelectors';
import { templateEditMetaSelector } from './selectors';
import { templateActions } from '@reserve/client/actions';
//#endregion

export function useEditTemplate() {
  const dispatch = useDispatch();
  const template = useSelector(reservationTemplateSelector);
  const fetchMeta = useSelector(reservationTemplateMetaSelector);
  const editMeta = useSelector(templateEditMetaSelector);

  useEffect(() => {
    if (!fetchMeta.retrieved && !fetchMeta.pending) {
      dispatch(templateActions.fetch.request());
    }
  }, [dispatch, fetchMeta]);

  function setTemplate(template: string) {
    dispatch(templateActions.set.request(template));
  }

  function deleteTemplate() {
    dispatch(templateActions.delete.request());
  }

  return {
    retrieved: fetchMeta.retrieved,
    pending: editMeta.pending,
    error: fetchMeta.error ?? fetchMeta.error,
    template,
    setTemplate,
    deleteTemplate,
  };
}
