/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Card, Popconfirm, Button } from 'antd';
import Form from './components/Form';
//#endregion

export interface Props {
  readonly isEditting: boolean;
  readonly pending: boolean;
  readonly retrieved: boolean;
  readonly template: string;
  readonly value: string;
  readonly onChange: (template: string) => any;
  readonly onSubmit: (e: React.FormEvent) => any;
  readonly onCancel: () => any;
  readonly onDelete: () => any;
  readonly onEdit: () => any;
}

const View: React.FunctionComponent<Props> = (props) =>
  props.isEditting ? (
    <Form {...props} />
  ) : (
    <Card title="Reservation Notes Template" loading={!props.retrieved}>
      <p>{props.template.length ? props.template : <i>No template yet</i>}</p>
      <Button icon="edit" onClick={props.onEdit}>
        Edit
      </Button>
      <Popconfirm
        title="Are you sure delete this template?"
        onConfirm={props.onDelete}
        okText="Yes"
        cancelText="No"
        okType="danger"
      >
        <Button icon="delete" type="danger">
          Delete
        </Button>
      </Popconfirm>
    </Card>
  );

export default View;
