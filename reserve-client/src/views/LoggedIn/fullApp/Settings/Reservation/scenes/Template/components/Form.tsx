/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Card, Form, Button } from 'antd';
import { TextArea } from '@reserve/client/components/Input';
//#endregion

export interface Props {
  readonly template: string;
  readonly value: string;
  readonly pending: boolean;
  readonly onChange: (template: string) => any;
  readonly onSubmit: (e: React.FormEvent) => any;
  readonly onCancel: () => any;
}

const FormComponent: React.FunctionComponent<Props> = (props) => (
  <Card title="Reservation Notes Template">
    <Form onSubmit={props.onSubmit}>
      <TextArea
        placeholder="Example reservation notes..."
        rows={4}
        value={props.value}
        onChange={props.onChange}
        autoFocus
      />
      <Button
        type="primary"
        htmlType="submit"
        disabled={props.template === props.value || props.pending}
      >
        Save
      </Button>
      <Button onClick={props.onCancel} disabled={props.pending}>
        cancel
      </Button>
    </Form>
  </Card>
);

export default FormComponent;
