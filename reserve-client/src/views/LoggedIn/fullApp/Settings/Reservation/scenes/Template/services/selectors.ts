/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createSelector } from 'reselect';
import { RootState } from '@reserve/client/reducers';
//#endregion

const templateSetMetaSelector = (state: RootState) => state.view.template.set;

const templateDeleteMetaSelector = (state: RootState) =>
  state.view.template.delete;

export const templateEditMetaSelector = createSelector(
  templateSetMetaSelector,
  templateDeleteMetaSelector,
  (set, del) => ({
    pending: set.pending || del.pending,
    error: set.error ?? del.error,
  })
);
