/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from '@reserve/client/actions/webhook';
import { RootState } from '@reserve/client/reducers';
import { Webhook } from '@reserve/client/lib/types/Webhook';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  meta: state.view.webhook.edit,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  fetchById: (id: number) => dispatch(actions.fetchById.request(id)),
  onSubmit: (payload: Webhook) => dispatch(actions.edit.request(payload)),
  onCancel: () => dispatch(actions.edit.cancel()),
});
