/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import WebhookList from './scenes/WebhookList';
import WebhookCreate from './scenes/WebhookCreate';
import Edit from './edit';
import NotFound from '@reserve/client/views/NotFound';
//#endregion

const Router: React.FunctionComponent<any> = () => (
  <Switch>
    <Route path="/settings/webhooks" component={WebhookList} exact />
    <Route path="/settings/webhooks/create" component={WebhookCreate} exact />
    <Route path="/settings/webhooks/:id/edit" component={Edit} exact />
    <Route component={NotFound} />
  </Switch>
);

export default Router;
