/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Form, Select, Button } from 'antd';

import Input, { Password } from '@reserve/client/components/Input';
import { EncodingType } from '@reserve/client/lib/types/EncodingType';
import { EventType } from '@reserve/client/lib/types/EventType';
//#endregion

//#region Component interfaces
export interface Props {
  readonly payloadUrl: string;
  readonly encoding: EncodingType;
  readonly secret: string;
  readonly events: EventType[];
  readonly onPayloadUrlChange: (e: string) => any;
  readonly onEncodingChange: (encoding: EncodingType) => any;
  readonly onSecretChange: (e: string) => any;
  readonly onEventsChange: (eventTypes: EventType[]) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
  readonly onCancel: () => any;
}
//#endregion

const View: React.FunctionComponent<Props> = (props) => (
  <Form onSubmit={props.onSubmit}>
    <Input
      type="url"
      placeholder="Payload URL"
      value={props.payloadUrl}
      onChange={props.onPayloadUrlChange}
      autoFocus
      required
    />

    <Select
      placeholder="Content-Type"
      value={props.encoding}
      onChange={props.onEncodingChange}
    >
      {[EncodingType.urlencoded, EncodingType.json].map((encoding) => (
        <Select.Option key={encoding} value={encoding}>
          {encoding}
        </Select.Option>
      ))}
    </Select>

    <Password
      placeholder="Secret"
      value={props.secret}
      onChange={props.onSecretChange}
      required
    />

    <Select
      mode="multiple"
      value={props.events}
      onChange={props.onEventsChange}
      placeholder="Events"
    >
      {[
        EventType.reservation,
        EventType.reservationCanceled,
        EventType.reservationConfirmed,
        EventType.reservationCreated,
        EventType.reservationRejected,
      ].map((event) => (
        <Select.Option key={event} value={event}>
          {event}
        </Select.Option>
      ))}
    </Select>

    <Button onClick={props.onCancel}>Cancel</Button>
    <Button type="primary" htmlType="submit">
      Submit
    </Button>
  </Form>
);

export default View;
