/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';

import { Webhook } from '@reserve/client/lib/types/Webhook';
import { EncodingType } from '@reserve/client/lib/types/EncodingType';
import { EventType } from '@reserve/client/lib/types/EventType';

import View from './view';
//#endregion

//#region Component interfaces
export interface Props {
  readonly defaultValue?: Webhook;
  readonly onSubmit: (
    payloadUrl: string,
    encoding: EncodingType,
    secret: string,
    events: EventType[]
  ) => any;
  readonly onCancel: () => any;
}

//#endregion

const Form: React.FunctionComponent<Props> = (props) => {
  const { defaultValue } = props;
  const [payloadUrl, setPayloadUrl] = useState(defaultValue?.payloadUrl ?? '');
  const [encoding, setEncoding] = useState(
    defaultValue?.encoding ?? EncodingType.urlencoded
  );
  const [secret, setSecret] = useState(defaultValue?.secret ?? '');
  const [events, setEvents] = useState(defaultValue?.events ?? []);

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    props.onSubmit(payloadUrl, encoding, secret, events);
  }

  return (
    <View
      payloadUrl={payloadUrl}
      encoding={encoding}
      secret={secret}
      events={events}
      onCancel={props.onCancel}
      onSubmit={handleSubmit}
      onPayloadUrlChange={setPayloadUrl}
      onEncodingChange={setEncoding}
      onSecretChange={setSecret}
      onEventsChange={setEvents}
    />
  );
};

export default Form;
