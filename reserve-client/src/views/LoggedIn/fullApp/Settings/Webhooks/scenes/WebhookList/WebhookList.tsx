/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imporst
import React from 'react';
import { PageHeader, Table } from 'antd';
import { useSelector } from 'react-redux';
import { useWebhooks } from '@reserve/client/services/hooks/useWebhooks';

import { columns } from './columns';
import CreateWebhookLink from './components/CreateWebhookLink';
import { webhookPendingMetaSelector } from './services/selectors';
//#endregion

export interface Props {
  readonly onRemove: (webhookId: number) => any;
  readonly onToggleActive: (webhookId: number) => any;
}

const WebhookList: React.FunctionComponent<Props> = (props) => {
  const { webhooks, meta } = useWebhooks();
  const { pending } = useSelector(webhookPendingMetaSelector);

  return (
    <PageHeader title="Webhooks" extra={[<CreateWebhookLink />]}>
      <Table
        loading={!meta.retrieved || pending}
        dataSource={webhooks}
        columns={columns(props.onRemove, props.onToggleActive)}
      />
    </PageHeader>
  );
};

export default WebhookList;
