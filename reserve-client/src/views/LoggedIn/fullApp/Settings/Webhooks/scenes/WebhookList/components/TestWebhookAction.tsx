/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import { Modal } from 'antd';
import { TextArea } from '@reserve/client/components/Input';
import { webhookActions } from '@reserve/client/actions';
import { useTestWebhook } from '@reserve/client/services/hooks/useWebhooks';

import { useDispatch } from 'react-redux';
//#endregion

export interface Props {
  readonly id: number;
}

const TestWebhookAction: React.FunctionComponent<Props> = (props) => {
  const [opened, setOpened] = useState(false);
  const [content, setContent] = useState('');
  const dispatch = useDispatch();
  const { test, testResponse, meta } = useTestWebhook();

  function handleTest() {
    if (testResponse) {
      dispatch(webhookActions.test.reset());
      setOpened(false);
    } else {
      test(props.id, content);
    }
  }

  function handleCancel() {
    setOpened(false);
    setContent('');
    dispatch(webhookActions.test.reset());
  }

  return (
    <React.Fragment>
      {/*eslint-disable jsx-a11y/anchor-is-valid*/}
      {/*eslint-disable-next-line no-script-url*/}
      <a href="javascript:;" onClick={() => setOpened(true)}>
        Test
      </a>

      <Modal
        visible={opened}
        onOk={handleTest}
        onCancel={handleCancel}
        okButtonProps={{ disabled: meta.pending }}
      >
        {testResponse ? (
          <pre>{JSON.stringify(testResponse, null, 2)}</pre>
        ) : (
          <TextArea
            placeholder="plaintext content"
            value={content}
            onChange={setContent}
          />
        )}
      </Modal>
    </React.Fragment>
  );
};

export default TestWebhookAction;
