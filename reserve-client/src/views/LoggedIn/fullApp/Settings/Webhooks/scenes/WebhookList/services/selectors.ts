/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch, bindActionCreators } from 'redux';
import { createSelector } from 'reselect';
import { webhookActions } from '@reserve/client/actions';
import { RootState } from '@reserve/client/reducers';
//#endregion

export const webhookEditMetaSelector = (state: RootState) =>
  state.view.webhook.edit;

export const webhookRemoveMetaSelector = (state: RootState) =>
  state.view.webhook.remove;

export const webhookPendingMetaSelector = createSelector(
  webhookEditMetaSelector,
  webhookRemoveMetaSelector,
  (edit, remove) => ({
    pending: edit.pending || remove.pending,
    error: edit.error ?? remove.error,
  })
);

export const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      onToggleActive: webhookActions.toggleActive.request,
      onRemove: webhookActions.remove.request,
    },
    dispatch
  );
