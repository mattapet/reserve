/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Link } from 'react-router-dom';
import { Icon } from 'antd';
//#endregion

const CreateWebhookLink: React.FunctionComponent = () => (
  <Link to="/settings/webhooks/create">
    <Icon type="edit" /> Create webhook
  </Link>
);

export default CreateWebhookLink;
