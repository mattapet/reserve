/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect } from 'react';
import { PageHeader } from 'antd';
import { useHistory } from 'react-router';
import { EncodingType } from '@reserve/client/lib/types/EncodingType';
import { EventType } from '@reserve/client/lib/types/EventType';
import { usePrevious } from '@reserve/client/lib/hooks';

import Form from '../../components/form';
//#endregion

export interface Props {
  readonly meta: {
    readonly error?: Error;
    readonly pending: boolean;
    readonly retrieved?: boolean;
  };
  readonly onSubmit: (
    payloadUrl: string,
    encoding: EncodingType,
    secret: string,
    events: EventType[]
  ) => any;
  readonly onCancel: () => any;
}

const CreateWebhook: React.FunctionComponent<Props> = (props) => {
  const history = useHistory();
  const prevPending = usePrevious(props.meta.pending);

  function handleCancel(): void {
    history.replace('/settings/webhooks');
  }

  useEffect(() => {
    if (prevPending && !props.meta.pending && !props.meta.error) {
      history.replace('/settings/webhooks');
    }
  }, [props, history, prevPending]);

  return (
    <PageHeader title="Webhooks" subTitle="Create webhook">
      <Form onSubmit={props.onSubmit} onCancel={handleCancel} />
    </PageHeader>
  );
};

export default CreateWebhook;
