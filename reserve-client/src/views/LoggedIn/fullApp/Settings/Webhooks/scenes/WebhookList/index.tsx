/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { connect } from 'react-redux';
import { mapDispatchToProps } from './services/selectors';

import WebhookList from './WebhookList';

export default connect(null, mapDispatchToProps)(WebhookList);
