/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { ColumnProps } from 'antd/lib/table/interface';
import { Popconfirm, Divider, Badge } from 'antd';
import { Link } from 'react-router-dom';
import { Webhook } from '@reserve/client/lib/types/Webhook';

import TestWebhookAction from './components/TestWebhookAction';
//#endregion

export const columns = (
  onRemove: (id: number) => any,
  onToggle: (id: number) => any
) =>
  [
    {
      key: 'payloadUrl',
      title: 'Payload URL',
      dataIndex: 'payloadUrl',
    },
    {
      key: 'encoding',
      title: 'Encoding',
      dataIndex: 'encoding',
    },
    {
      key: 'status',
      title: 'Status',
      render: (_, { active }) =>
        active ? (
          <Badge status="success" text="Active" />
        ) : (
          <Badge status="error" text="Inactive" />
        ),
    },
    {
      key: 'actions',
      title: 'Actions',
      render: (_, { id, active }) => (
        <React.Fragment>
          <Link to={`/settings/webhooks/${id}/edit`}>Edit</Link>
          <Divider type="vertical" />
          <TestWebhookAction id={id} />
          <Divider type="vertical" />
          <Popconfirm title="Are you sure?" onConfirm={() => onToggle(id)}>
            {/* eslint-disable-next-line */}
            <a href="javascript:;">{active ? 'Deactive' : 'Activate'}</a>
          </Popconfirm>
          <Divider type="vertical" />
          <Popconfirm
            title="Are you sure?"
            onConfirm={() => onRemove(id)}
            okType="danger"
          >
            {/* eslint-disable-next-line */}
            <a href="javascript:;">Remove</a>
          </Popconfirm>
        </React.Fragment>
      ),
    },
  ] as ColumnProps<Webhook>[];
