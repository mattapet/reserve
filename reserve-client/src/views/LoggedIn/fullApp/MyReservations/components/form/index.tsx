/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';

import { resourceCollectionProvider } from '@reserve/client/providers';
import { WrappedProps as CollectionProviderProps } from '@reserve/client/providers/collectionProvider';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { Resource } from '@reserve/client/lib/types/Resource';
import View from './view';
//#endregion

//#region Component interfaces
export interface Props extends CollectionProviderProps<Resource> {
  readonly defaultValue?: Reservation;
  readonly onSubmit: (
    dateStart: Date,
    dateEnd: Date,
    resourceIds: number[],
    allDay: boolean,
    userId?: string
  ) => any;
  readonly onCancel: () => any;
}

interface State {
  readonly dateStart: Date;
  readonly dateEnd: Date;
  readonly resourceIds: { key: number; label: string }[];
  readonly userId?: string;
}
//#endregion

class FormController extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    const { defaultValue } = props;
    this.state = {
      dateStart: defaultValue?.dateStart ?? new Date(),
      dateEnd: defaultValue?.dateEnd ?? new Date(),
      resourceIds:
        defaultValue?.resourceIds.map(id => ({
          key: id,
          label: props.data[id].name,
        })) ?? [],
      userId: defaultValue?.userId,
    };
  }

  public render() {
    return (
      <View
        {...this.state}
        resources={Object.values(this.props.data)}
        onCancel={this.props.onCancel}
        onSubmit={this.handleSubmit}
        onDateRangeChange={this.handleDateRangeChange}
        onResourceIdsChange={this.handleResourceIdsChange}
      />
    );
  }

  private handleDateRangeChange = (dateRange: [string, string]) => {
    const [dateStart, dateEnd] = dateRange;
    this.setState({
      dateStart: new Date(dateStart),
      dateEnd: new Date(dateEnd),
    });
  };

  private handleResourceIdsChange = (
    resourceIds: { key: number; label: string }[]
  ) => {
    this.setState({ resourceIds });
  };

  private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { dateStart, dateEnd, resourceIds, userId } = this.state;
    this.props.onSubmit(
      dateStart,
      dateEnd,
      resourceIds.map(({ key }) => key),
      true,
      userId
    );
  };
}

export default resourceCollectionProvider(FormController as any) as any;
