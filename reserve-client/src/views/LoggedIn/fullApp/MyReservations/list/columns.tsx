/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Link } from 'react-router-dom';
import { ColumnProps } from 'antd/lib/table/interface';

import { Reservation } from '@reserve/client/lib/types/Reservation';
import ReservationStateTag from '@reserve/client/components/ReservationStateTag';
import { date } from '@reserve/client/lib/dateFormatter';
//#endregion

export const columns = () =>
  [
    {
      key: 'id',
      title: 'ID',
      render: (_, { id }) => (
        <Link to={`/reservations/${id}/details`}>{id}</Link>
      ),
    },
    {
      key: 'dateStart',
      title: 'Date Start',
      render: (_, item) => date(item.dateStart),
    },
    {
      key: 'dateEnd',
      title: 'Date End',
      render: (_, item) => date(item.dateEnd),
    },
    {
      key: 'state',
      title: 'State',
      render: (_, { state }) => <ReservationStateTag state={state} />,
    },
    {
      dataIndex: 'resourceIds',
      key: 'resourceIds',
      title: 'Resource Id',
    },
  ] as ColumnProps<Reservation>[];
