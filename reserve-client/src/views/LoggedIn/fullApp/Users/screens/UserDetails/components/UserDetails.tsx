/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Card, Descriptions } from 'antd';
import { User } from '@reserve/client/lib/types/User';
import UserRoleTag from '@reserve/client/components/UserRoleTag';
import { UserRole } from '@reserve/client/lib/types/UserRole';
//#endregion

export interface Props {
  readonly user?: User;
}

const UserDetails: React.FunctionComponent<Props> = props => (
  <Card loading={!props.user}>
    <Descriptions>
      <Descriptions.Item label="First Name">
        {props.user?.profile?.firstName}
      </Descriptions.Item>
      <Descriptions.Item label="First Name">
        {props.user?.profile?.firstName}
      </Descriptions.Item>
      <Descriptions.Item label="User Role">
        <UserRoleTag role={props.user?.role || UserRole.user} />
      </Descriptions.Item>
      <Descriptions.Item label="Email">
        <a href={`mailto:${props.user?.email}`}>{props.user?.email}</a>
      </Descriptions.Item>
    </Descriptions>
  </Card>
);

export default UserDetails;
