/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { PageHeader } from 'antd';
import { useHistory, useParams } from 'react-router';
import { useParamUser } from '@reserve/client/services/hooks/useParamUser';
import UserReservationsLink from './components/UserReservationsLink';
import View from './components/UserDetails';
//#endregion

export interface Props {}

const UserDetails: React.FunctionComponent<Props> = props => {
  const { id } = useParams();
  const history = useHistory();
  const user = useParamUser();

  return (
    <PageHeader
      title="Users"
      subTitle="User details"
      extra={[<UserReservationsLink id={id!} />]}
      onBack={() => history.goBack()}
    >
      <View user={user} />
    </PageHeader>
  );
};

export default UserDetails;
