/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Link } from 'react-router-dom';
import { Icon } from 'antd';
//#endregion

export interface Props {
  readonly id: string;
}

const UserReservationsLink: React.FunctionComponent<Props> = props => (
  <Link to={`/users/${props.id}/reservations`}>
    <Icon type="unordered-list" /> Reservations
  </Link>
);

export default UserReservationsLink;
