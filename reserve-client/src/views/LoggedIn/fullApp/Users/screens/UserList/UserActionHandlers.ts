/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from '@reserve/client/lib/types/UserRole';
//#endregion

export interface UserActionHandlers {
  readonly onUserRoleChage: (id: string, role: UserRole) => any;
  readonly onUserBanToggle: (id: string) => any;
}
