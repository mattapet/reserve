/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Input } from 'antd';
import { SearchProps } from 'antd/lib/input/Search';
//#endregion

export interface Props extends Omit<SearchProps, 'onChange'> {
  readonly onChange: (value: string) => void;
}

const SearchInput: React.FunctionComponent<Props> = props => {
  const { onChange, ...restProps } = props;
  return (
    <Input.Search {...restProps} onChange={e => onChange(e.target.value)} />
  );
};

export default SearchInput;
