/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Divider } from 'antd';
import { User } from '@reserve/client/lib/types/User';
import { UserRole } from '@reserve/client/lib/types/UserRole';
import { useUserIdentity } from '@reserve/client/services/hooks/useUserIdentity';
import UserDetailsLink from './UserDetailsLink';
import UserRoleSelect from './UserRoleSelect';
//#endregion

export interface Props {
  readonly user: User;
  readonly onUserRoleChage: (id: string, role: UserRole) => any;
}

const UserActionsColumn: React.FunctionComponent<Props> = (props) => {
  const identity = useUserIdentity();
  const { user, onUserRoleChage } = props;

  if (props.user.id === identity.id) {
    return null;
  }

  return (
    <React.Fragment>
      <UserDetailsLink user={user} />

      {!props.user.isOwner && (
        <React.Fragment>
          <Divider type="vertical" />
          <UserRoleSelect
            identity={identity}
            user={user}
            onRoleChange={onUserRoleChage}
          />
        </React.Fragment>
      )}
    </React.Fragment>
  );
};

export default UserActionsColumn;
