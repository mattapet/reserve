/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { ColumnProps } from 'antd/lib/table/interface';

import UserRoleTag from '@reserve/client/components/UserRoleTag';
import { User } from '@reserve/client/lib/types/User';
import { timestamp } from '@reserve/client/lib/dateFormatter';
import { UserRole } from '@reserve/client/lib/types/UserRole';
import { SortInfo } from '@reserve/client/lib/types/SortInfo';
import * as filters from '@reserve/client/services/users/filters';

import { FilterInfo } from '@reserve/client/lib/types/FilterInfo';
import UserActionsColumn from './components/UserActionsColumn';
import { UserActionHandlers } from './UserActionHandlers';
import UserBannedColumn from './components/UserBannedColumn';
import * as sorters from './services/sorters';
//#endregion

export const columns = (
  searchQuery: string | undefined,
  sortInfo: SortInfo<User> | undefined,
  filterInfo: FilterInfo<User> | undefined,
  { onUserRoleChage, onUserBanToggle }: UserActionHandlers
): ColumnProps<User>[] => [
  {
    key: 'email',
    title: 'Email',
    render: (_, { email }) => <a href={`mailto:${email}`}>{email}</a>,
  },
  {
    dataIndex: 'profile.firstName',
    key: 'firstName',
    title: 'First Name',
    filteredValue: searchQuery ? [searchQuery] : null,
  },
  {
    dataIndex: 'profile.lastName',
    key: 'lastName',
    title: 'Last Name',
  },
  {
    key: 'banned',
    title: 'Banned',
    filters: [
      {
        text: 'Banned',
        value: 'true',
      },
      {
        text: 'Not Banned',
        value: 'false',
      },
    ],
    filteredValue: filterInfo?.banned ?? null,
    onFilter: filters.bannedFilter,
    render: (_, item) => (
      <UserBannedColumn user={item} onBanToggle={onUserBanToggle} />
    ),
  },
  {
    key: 'role',
    title: 'Role',
    filters: [
      {
        text: 'User',
        value: UserRole.user,
      },
      {
        text: 'Maintainer',
        value: UserRole.maintainer,
      },
      {
        text: 'Admin',
        value: UserRole.admin,
      },
    ],
    onFilter: filters.roleFilter,
    filteredValue: filterInfo?.role ?? null,
    render: (_, item) => <UserRoleTag role={item.role} />,
  },
  {
    key: 'lastLogin',
    title: 'Last Login',
    sortOrder: sortInfo?.columnKey === 'lastLogin' && sortInfo.order,
    sorter: sorters.lastLoginSorter,
    render: (_, item) => (+item.lastLogin ? timestamp(item.lastLogin) : 'N/A'),
  },
  {
    key: 'actions',
    title: 'Actions',
    render: (_, item) => (
      <UserActionsColumn user={item} onUserRoleChage={onUserRoleChage} />
    ),
  },
];
