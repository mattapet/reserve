/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as R from 'ramda';
import { User } from '@reserve/client/lib/types/User';
//#endregion

export const lastLoginSorter = R.comparator(
  (lhs: User, rhs: User) =>
    +R.prop('lastLogin', lhs) < +R.prop('lastLogin', rhs)
);
