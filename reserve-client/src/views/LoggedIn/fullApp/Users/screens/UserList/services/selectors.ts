/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createSelector } from 'reselect';
import { Dispatch, bindActionCreators } from 'redux';
import { userEditSelector } from '@reserve/client/services/userSelectors';
import { userActions } from '@reserve/client/actions';
//#endregion

export const mapStateToProps = createSelector(userEditSelector, meta => ({
  meta,
}));

export const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      onUserRoleChage: userActions.updateRole.request,
      onUserBanToggle: userActions.toggleBan.request,
    },
    dispatch
  );
