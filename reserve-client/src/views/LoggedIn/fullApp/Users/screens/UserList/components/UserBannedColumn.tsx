/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Badge, Dropdown, Menu, Icon } from 'antd';
import { User } from '@reserve/client/lib/types/User';
import { Identity } from '@reserve/client/reducers/auth';
import { UserRole } from '@reserve/client/lib/types/UserRole';
import { useUserIdentity } from '@reserve/client/services/hooks/useUserIdentity';
//#endregion

export interface Props {
  readonly user: User;
  readonly onBanToggle: (id: string) => any;
}

function renderUserBadge(user: User) {
  if (!user.banned) {
    return <Badge status="success" text="No" />;
  } else {
    return <Badge status="error" text="Yes" />;
  }
}

function canToggleBan(identity: Identity, user: User): boolean {
  return user.role !== UserRole.admin && identity.role === UserRole.admin;
}

const UserBannedColumn: React.FunctionComponent<Props> = props => {
  const identity = useUserIdentity();
  const { user, onBanToggle } = props;

  if (!canToggleBan(identity, user)) {
    return renderUserBadge(props.user);
  }

  return (
    <Dropdown
      overlay={
        <Menu selectedKeys={[]}>
          {
            <Menu.Item key="toggleBan" onClick={() => onBanToggle(user.id)}>
              {props.user.banned ? 'Unban' : 'Ban'}
            </Menu.Item>
          }
        </Menu>
      }
      trigger={['click']}
    >
      {/* eslint-disable-next-line */}
      <a href="javascript:;">
        {renderUserBadge(props.user)} <Icon type="down" />
      </a>
    </Dropdown>
  );
};

export default UserBannedColumn;
