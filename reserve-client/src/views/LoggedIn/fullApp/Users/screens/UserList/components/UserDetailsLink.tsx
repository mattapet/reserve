/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Link } from 'react-router-dom';
import { User } from '@reserve/client/lib/types/User';
//#endregion

export interface Props {
  readonly user: User;
}

const UserDetailsLink: React.FunctionComponent<Props> = props => (
  <Link to={`/users/${props.user.id}/details`}>Details</Link>
);

export default UserDetailsLink;
