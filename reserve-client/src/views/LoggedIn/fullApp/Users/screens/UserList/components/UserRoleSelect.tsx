/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import * as R from 'ramda';
import { UserRole } from '@reserve/client/lib/types/UserRole';
import { User } from '@reserve/client/lib/types/User';
import { Identity } from '@reserve/client/reducers/auth';
import { Menu, Dropdown, Icon } from 'antd';
//#endregion

export interface Props {
  readonly identity: Identity;
  readonly user: User;
  readonly onRoleChange: (id: string, userRole: UserRole) => any;
}

function getAuthorizedUserRolesForIdentityRole(identity: Identity): UserRole[] {
  switch (identity.role) {
    case UserRole.maintainer:
      return [UserRole.user, UserRole.maintainer];
    case UserRole.admin:
      return [UserRole.user, UserRole.maintainer, UserRole.admin];
    default:
      throw new Error('Unreachable');
  }
}

const UserRoleSelect: React.FunctionComponent<Props> = props => {
  const { identity, user, onRoleChange } = props;
  const authorizedUserRoles = getAuthorizedUserRolesForIdentityRole(identity);
  const roleApplicableFilter = R.pipe(R.equals(user.role), R.not);

  const userRoleOptions = R.pipe<UserRole[], UserRole[], React.ReactNode[]>(
    R.filter(roleApplicableFilter),
    R.map(role => (
      <Menu.Item key={`${role}`} onClick={() => onRoleChange(user.id, role)}>
        {role.toUpperCase()}
      </Menu.Item>
    ))
  )(authorizedUserRoles);

  return (
    <Dropdown
      overlay={<Menu selectedKeys={[user.role]}>{userRoleOptions}</Menu>}
      trigger={['click']}
    >
      {/* eslint-disable-next-line */}
      <a href="javascript:;">
        Role <Icon type="down" />
      </a>
    </Dropdown>
  );
};

export default UserRoleSelect;
