/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import * as R from 'ramda';
import { PageHeader, Table } from 'antd';
import { useUsers } from '@reserve/client/services/hooks/useUsers';
import { UserRole } from '@reserve/client/lib/types/UserRole';
import {
  useQueryControlledTable,
  useQueryState,
} from '@reserve/client/lib/hooks';
import { User } from '@reserve/client/lib/types/User';
import { SortInfo } from '@reserve/client/lib/types/SortInfo';
import { searchFilter } from '@reserve/client/services/users/filters';

import SearchInput from './components/SearchInput';
import { columns } from './columns';
//#endregion

export interface Props {
  readonly meta: {
    readonly pending: boolean;
    readonly error?: Error;
  };
  readonly onUserRoleChage: (payload: {
    readonly id: string;
    readonly role: UserRole;
  }) => any;
  readonly onUserBanToggle: (id: string) => any;
}

const UserList: React.FunctionComponent<Props> = (props) => {
  const { users, meta } = useUsers();
  const { onUserBanToggle, onUserRoleChage } = props;
  const loading = !meta.retrieved || props.meta.pending;

  const defaultSort: SortInfo<User> = {
    columnKey: 'lastLogin',
    order: 'descend',
  };
  const [search, setSearch] = useQueryState('search', '');
  const {
    filterInfo,
    sortInfo,
    pagination,
    handlePaginationChange,
    handleTableChange,
  } = useQueryControlledTable<User>(defaultSort);
  const { current, pageSize } = pagination ?? {};

  const filteredUsers = R.filter(searchFilter(search), users);

  return (
    <PageHeader title="Users">
      <SearchInput
        placeholder="Search"
        value={search}
        onChange={setSearch}
        style={{ width: 200 }}
      />
      <Table
        dataSource={filteredUsers}
        loading={loading}
        columns={columns(search, sortInfo, filterInfo, {
          onUserBanToggle,
          onUserRoleChage: (id, role) => onUserRoleChage({ id, role }),
        })}
        rowKey={R.prop('id')}
        onChange={handleTableChange}
        pagination={{
          current,
          pageSize,
          onChange: handlePaginationChange,
        }}
      />
    </PageHeader>
  );
};

export default UserList;
