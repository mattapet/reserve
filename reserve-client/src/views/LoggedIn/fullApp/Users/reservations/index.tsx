/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { List } from '@reserve/client/components/Data';
import { reservationCollectionProvider as provider } from '@reserve/client/providers';
import { WrappedProps as CollectionProviderProps } from '@reserve/client/providers/collectionProvider';
import AsyncUserNameLink from '@reserve/client/components/AsyncUserNameLink';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import ResourceTag from '@reserve/client/components/ResourceTag';

import { mapDispatchToProps } from './selectors';
import { columns } from './columns';
//#endregion

//#region Component interfaces
export interface Props
  extends CollectionProviderProps<Reservation>,
    RouteComponentProps<any> {
  readonly onConfirm: (id: string) => any;
  readonly onCancel: (id: string) => any;
  readonly onReject: (id: string) => any;
}
//#endregion

const ReservationList: React.FunctionComponent<Props> = props => (
  <List<Reservation>
    title={
      <span>
        <AsyncUserNameLink userId={props.match.params.id} />
        's Reservations
      </span>
    }
    data={Object.values(props.data).filter(
      reservation => reservation.userId === props.match.params.id
    )}
    pending={props.meta.pending || !props.meta.retrieved}
    extractKey={(entity, _) => `${entity.id}`}
    columns={columns(props.onConfirm, props.onCancel, props.onReject)}
    expandedRowRender={reservation => (
      <React.Fragment>
        <b>Resources: </b>
        {reservation.resourceIds.map(id => (
          <ResourceTag key={`${id}`} id={id} />
        ))}
      </React.Fragment>
    )}
  />
);

export default connect(
  null,
  mapDispatchToProps
)(withRouter(provider(ReservationList as any) as any) as any);
