/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import NotFound from '@reserve/client/views/NotFound';

import UserList from './screens/UserList';
import UserDetails from './screens/UserDetails';
import Reservations from './reservations';
//#endregion

const Router: React.FunctionComponent<any> = () => (
  <Switch>
    <Route path="/users" component={UserList} exact />
    <Route path="/users/:id/details" component={UserDetails} exact />
    <Route path="/users/:id/reservations" component={Reservations} exact />
    <Route component={NotFound} />
  </Switch>
);

export default Router;
