/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { Resource } from '@reserve/client/lib/types/Resource';
import { resourceEntityProvider, idProvider } from '@reserve/client/providers';
import { WrappedProps as IdProviderProps } from '@reserve/client/providers/idProvider';
import { WrappedProps as EntityProviderProps } from '@reserve/client/providers/entityProvider';
import Spinner from '@reserve/client/components/Spinner';
import Details from '../components/details';
//#endregion

//#region Component interfaces
export interface Props
  extends IdProviderProps,
    EntityProviderProps<Resource>,
    RouteComponentProps {
  readonly onSubmit: (value: Resource) => any;
}
//#endregion

const EditResource: React.FunctionComponent<Props> = props =>
  props.entity ? <Details entity={props.entity} /> : <Spinner />;

export default idProvider(
  withRouter(resourceEntityProvider(EditResource as any) as any) as any
);
