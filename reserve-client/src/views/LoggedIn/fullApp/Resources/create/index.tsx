/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { Create } from '@reserve/client/components/Data';
import Form from '../components/form';
import { mapStateToProps, mapDispatchToProps } from './selectors';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly meta: {
    readonly error?: Error;
    readonly pending: boolean;
  };
  readonly onSubmit: (name: string, description: string) => any;
  readonly onCancel: () => any;
}
//#endregion

const CreateResource: React.FunctionComponent<Props> = props => (
  <Create
    meta={props.meta as any}
    title="Create Resource"
    onComplete={() => props.history.replace('/resources')}
  >
    <Form onSubmit={props.onSubmit} onCancel={() => props.history.goBack()} />
  </Create>
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CreateResource));
