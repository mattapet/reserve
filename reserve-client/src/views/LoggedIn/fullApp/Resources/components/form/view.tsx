/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Button, Input, Form } from 'antd';
//#endregion

//#region Component interface
export interface Props {
  readonly name: string;
  readonly description: string;
  readonly onNameChange: (e: React.FormEvent<HTMLInputElement>) => any;
  readonly onDescriptionChange: (e: React.FormEvent<HTMLInputElement>) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
  readonly onCancel: () => any;
}
//#endregion

const View: React.FunctionComponent<Props> = (props) => (
  <Form onSubmit={props.onSubmit}>
    <Input
      value={props.name}
      placeholder="Name"
      onChange={props.onNameChange}
      required
    />
    <Input
      value={props.description}
      placeholder="Description"
      onChange={props.onDescriptionChange}
    />
    <Button onClick={props.onCancel}>Cancel</Button>
    <Button type="primary" htmlType="submit">Submit</Button>
  </Form>
);

export default View;
