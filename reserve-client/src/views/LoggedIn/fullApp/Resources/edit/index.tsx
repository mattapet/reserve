/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import Form from '../components/form';
import { Resource } from '@reserve/client/lib/types/Resource';
import Spinner from '@reserve/client/components/Spinner';
import { resourceEntityProvider, idProvider } from '@reserve/client/providers';
import { WrappedProps as IdProviderProps } from '@reserve/client/providers/idProvider';
import { WrappedProps as EntityProviderProps } from '@reserve/client/providers/entityProvider';

import { mapDispatchToProps } from './selectors';
//#endregion

//#region Component interfaces
export interface Props
  extends IdProviderProps,
    EntityProviderProps<Resource>,
    RouteComponentProps {
  readonly onSubmit: (value: Resource) => any;
}
//#endregion

const EditResource: React.FunctionComponent<Props> = props =>
  props.entity ? (
    <Form
      defaultValue={props.entity}
      onSubmit={(name: string, description: string) =>
        props.onSubmit({
          ...props.entity!,
          name,
          description,
        })
      }
      onCancel={() => props.history.goBack()}
    />
  ) : (
    <Spinner />
  );

export default connect(
  null,
  mapDispatchToProps
)(
  idProvider(
    withRouter(resourceEntityProvider(EditResource as any) as any) as any
  )
);
