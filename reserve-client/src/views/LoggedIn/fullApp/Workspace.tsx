/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Layout } from 'antd';
import styled from 'styled-components';

import { UserRole } from '@reserve/client/lib/types/UserRole';
import Sidebar from './components/Sidebar';
import Header from './components/Header';
import Router from './router';
//#endregion

//#region Component interfaces
export interface Props {
  readonly isOwner: boolean;
  readonly role: UserRole;
}
//#endregion

//#region Styled
const Container = styled(Layout)`
  height: 100%;
`;

const Content = styled(Layout.Content)`
  height: 100%;
`;
//#endregion

const Workspace: React.FunctionComponent<Props> = (props) => (
  <Container>
    <Header />
    <Container>
      <Sidebar {...props} />
      <Content>
        <Router {...props} />
      </Content>
    </Container>
  </Container>
);

export default Workspace;
