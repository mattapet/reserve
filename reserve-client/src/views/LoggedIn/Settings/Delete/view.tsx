/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import { Modal, Layout, Card, Button } from 'antd';

import Input from '@reserve/client/components/Input';
//#endregion

//#region Component interfaces
export interface Props {
  readonly email: string;
  readonly onDeleteRequest: () => any;
  readonly displayDeleteConfirm: boolean;
  readonly deleteConfirmValue: string;
  readonly onDeleteConfirmChanged: (e: string) => any;
  readonly onDeleteConfirm: () => any;
  readonly onDeleteCancel: () => any;
}
//#endregion

//#region Styled
const Container = styled(Layout)`
  flex: 1;
  justify-content: center;
  align-items: center;
`;
//#endregion

const View: React.FunctionComponent<Props> = props => (
  <Container>
    <Card title="Profile">
      <Button size="large" type="danger" onClick={props.onDeleteRequest}>
        <span>
          Delete account <b>{props.email}</b>
        </span>
      </Button>

      <Modal
        title="Are you absolutely sure?"
        visible={props.displayDeleteConfirm}
        onOk={props.onDeleteConfirm}
        onCancel={props.onDeleteCancel}
        okButtonProps={{
          style: { width: '100%' },
          disabled: props.email !== props.deleteConfirmValue,
        }}
        okText="I understand, delete this account"
        okType="danger"
        cancelButtonProps={{ hidden: true }}
      >
        <React.Fragment>
          <p>
            This action <b>cannot</b> be undone. This will permanently delete
            your account with all the data that may be associated with it.
          </p>
          <p>Please type in the your email to confirm.</p>
          <Input
            value={props.deleteConfirmValue}
            onChange={props.onDeleteConfirmChanged}
          />
        </React.Fragment>
      </Modal>
    </Card>
  </Container>
);

export default View;
