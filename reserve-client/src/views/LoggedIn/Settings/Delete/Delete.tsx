/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';

import View from './view';
//#endregion

//#region Component interfaces
export interface Props {
  readonly userId: string;
  readonly email: string;
  readonly pending: boolean;
  readonly onDelete: () => any;
}
//#endregion

export const DeleteUser: React.FunctionComponent<Props> = (props) => {
  const [displayDeleteConfirm, setDisplayDeleteConfirm] = useState(false);
  const [deleteConfirmValue, setDeleteConfirmValue] = useState('');

  function handleDeleteConfirm() {
    setDisplayDeleteConfirm(false);
    props.onDelete();
  }

  return (
    <View
      displayDeleteConfirm={displayDeleteConfirm}
      deleteConfirmValue={deleteConfirmValue}
      email={props.email}
      onDeleteConfirmChanged={setDeleteConfirmValue}
      onDeleteRequest={() => setDisplayDeleteConfirm(true)}
      onDeleteConfirm={handleDeleteConfirm}
      onDeleteCancel={() => setDisplayDeleteConfirm(false)}
    />
  );
};

export default DeleteUser;
