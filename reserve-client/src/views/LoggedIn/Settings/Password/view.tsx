/*
 * Copyright (c) 2019-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import { Button, Form, Layout, Card } from 'antd';

import { Password } from '@reserve/client/components/Input';
//#endregion

//#region Component interfaces
export interface Props {
  readonly password: string;
  readonly passwordVerify: string;
  readonly pending: boolean;
  readonly onPasswordChange: (value: string) => any;
  readonly onPasswordVerifyChange: (value: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLElement>) => any;
}
//#endregion

//#region Styled
const Container = styled(Layout)`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const StyledForm = styled(Form)`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-content: center;

  * {
    margin: 5px;
  }
`;

const StyledButton = styled(Button)`
  width: 100%;
`;
//#endregion

const View: React.FunctionComponent<Props> = props => (
  <Container>
    <Card title="Change Password">
      <StyledForm onSubmit={props.onSubmit}>
        <Password
          placeholder="Password"
          value={props.password}
          onChange={props.onPasswordChange}
          autoCapitalize="words"
        />
        <Password
          placeholder="Verify password"
          value={props.passwordVerify}
          onChange={props.onPasswordVerifyChange}
          autoCapitalize="words"
        />
        <StyledButton
          type="primary"
          htmlType="submit"
          onClick={props.onSubmit}
          loading={props.pending}
        >
          Submit
        </StyledButton>
      </StyledForm>
    </Card>
  </Container>
);

export default View;
