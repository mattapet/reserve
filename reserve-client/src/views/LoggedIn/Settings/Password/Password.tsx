/*
 * Copyright (c) 2019-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';

import View from './view';
//#endregion

//#region Component interfaces
export interface Props {
  readonly pending: boolean;
  readonly onSubmit: (password: string) => any;
}
//#endregion

const Password: React.FunctionComponent<Props> = (props) => {
  const [password, setPassword] = useState('');
  const [passwordVerify, setPasswordVerify] = useState('');

  function handleSubmit(e: React.FormEvent<HTMLElement>) {
    e.preventDefault();

    props.onSubmit(password);
  }

  return (
    <View
      pending={props.pending}
      password={password}
      passwordVerify={passwordVerify}
      onPasswordChange={setPassword}
      onPasswordVerifyChange={setPasswordVerify}
      onSubmit={handleSubmit}
    />
  );
};

export default Password;
