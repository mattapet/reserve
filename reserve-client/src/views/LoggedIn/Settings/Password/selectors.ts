/*
 * Copyright (c) 2019-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import { RootState } from '@reserve/client/reducers';
import { authActions } from '@reserve/client/actions';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  pending: state.view.auth.setPassword.pending,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onSubmit: (password: string) =>
    dispatch(authActions.setPassword.request(password)),
});
