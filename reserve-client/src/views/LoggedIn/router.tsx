/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Spinner from '@reserve/client/components/Spinner';
import { UserRole } from '@reserve/client/lib/types/UserRole';
const Logout = React.lazy(() => import('./Logout'));
const Profile = React.lazy(() => import('./Profile'));
const Settings = React.lazy(() => import('./Settings'));
const fullApp = React.lazy(() => import('./fullApp'));
const userApp = React.lazy(() => import('./userApp'));
//#endregion

export interface Props {
  readonly userRole: UserRole;
}

const Router: React.FunctionComponent<Props> = props => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      <Redirect
        key="/auth/:authority/callback"
        from="/auth/:authority/callback"
        to="/"
      />
      <Redirect
        key="/register/confirm"
        from="/register/confirm"
        to="/profile"
      />
      <Route
        key="/"
        path="/"
        component={props.userRole === UserRole.user ? userApp : fullApp}
        exact
      />
      <Route key="/profile" path="/profile" component={Profile} exact />
      <Route
        key="/profile/settings"
        path="/profile/settings"
        component={Settings}
      />
      <Route key="/logout" path="/logout" component={Logout} />
      <Route component={props.userRole === UserRole.user ? userApp : fullApp} />
    </Switch>
  </React.Suspense>
);

export default Router;
