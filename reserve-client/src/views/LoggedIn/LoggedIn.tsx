/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Layout } from 'antd';
import styled from 'styled-components';

import Router from './router';
import { UserRole } from '@reserve/client/lib/types/UserRole';
import { WorkspaceAnnouncement } from '@reserve/client/lib/types/WorkspaceAnnouncement';
import Announcement from './fullApp/components/Announcement';
//#endregion

//#region Component interfaces
export interface Props {
  readonly role: UserRole;
  readonly announcement: WorkspaceAnnouncement | null;
}
//#endregion

//#region Styled
const Container = styled(Layout)`
  height: 100%;
`;
//#endregion

const LoggedIn: React.FunctionComponent<Props> = (props) => (
  <Container>
    <Announcement announcement={props.announcement} />
    <Router userRole={props.role} />
  </Container>
);

export default LoggedIn;
