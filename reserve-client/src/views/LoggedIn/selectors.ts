/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createSelector } from 'reselect';
import { userRoleSelector } from '@reserve/client/services/authSelectors';
import { activeAnnouncementSelector } from '../../services/announcementSelectors';
//#endregion

export const mapStateToProps = createSelector(
  userRoleSelector,
  activeAnnouncementSelector,
  (role, announcement) => ({
    role,
    announcement,
  })
);
