/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect } from 'react';

import Spinner from '@reserve/client/components/Spinner';
//#endregion

//#region Component interfaces
export interface Props {
  readonly refreshToken: string;
  readonly onLogout: (refreshToken: string) => any;
  readonly onLogoutCancel: () => any;
}
//#endregion

const Logout: React.FunctionComponent<Props> = (props) => {
  const { refreshToken, onLogout, onLogoutCancel } = props;

  useEffect(() => {
    onLogout(refreshToken);
    return () => onLogoutCancel();
  }, [onLogout, onLogoutCancel, refreshToken]);

  return <Spinner />;
};

export default Logout;
