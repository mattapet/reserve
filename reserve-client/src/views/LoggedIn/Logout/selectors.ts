/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createSelector } from 'reselect';
import { Dispatch, bindActionCreators } from 'redux';

import { authActions } from '@reserve/client/actions';
import { refreshTokenSelector } from '@reserve/client/services/authSelectors';
//#endregion

export const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      onLogout: authActions.logout.request,
      onLogoutCancel: authActions.logout.cancel,
    },
    dispatch
  );

export const mapStateToProps = createSelector(
  refreshTokenSelector,
  (refreshToken) => ({ refreshToken })
);
