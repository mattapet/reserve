/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import { RootState } from '@reserve/client/reducers';
import { Profile } from '@reserve/client/lib/types/Profile';
import { userActions } from '@reserve/client/actions';
//#endregion

export const mapStateToProps = (state: RootState) => {
  if (!state.auth.isLoggedIn) {
    throw new Error('User must be logged in!');
  }
  return {
    pending: state.view.user.edit.pending,
    profile: state.auth.identity.profile,
  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onSubmit: (profile: Profile) =>
    dispatch(userActions.updateProfile.request(profile)),
});
