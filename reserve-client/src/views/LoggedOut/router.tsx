/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Spinner from '@reserve/client/components/Spinner';
const Login = React.lazy(() => import('./Login'));
const Logout = React.lazy(() => import('./Logout'));
const Register = React.lazy(() => import('./Register'));
const Auth = React.lazy(() => import('./Auth'));
const ConfirmEmail = React.lazy(() => import('./ConfirmEmail'));
//#endregion

export interface Props {}

const Router: React.FunctionComponent<Props> = props => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      <Redirect path="/" to="/login" exact />
      <Route path="/login" component={Login} />
      <Route path="/logout" component={Logout} exact />
      <Route path="/register" component={Register} exact />
      <Route path="/register/confirm" component={ConfirmEmail} exact />
      <Route path="/auth/:authority/callback" component={Auth} exact />
      <Redirect to="/" />
    </Switch>
  </React.Suspense>
);

export default Router;
