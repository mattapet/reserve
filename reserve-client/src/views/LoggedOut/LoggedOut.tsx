/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import { Layout } from 'antd';

import Header from './components/Header';
import Router from './router';
//#endregion

//#region Component interfaces
export interface Props {
  readonly isLoggedIn: boolean;
}
//#endregion

const Container = styled(Layout)`
  height: 100%;
`;

const LoggedOut: React.FunctionComponent<Props> = props => (
  <Container>
    <Header />
    <Router />
  </Container>
);

export default LoggedOut;
