/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as authActions from '../../../actions/auth';
import { RootState } from '../../../reducers';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  meta: state.view.register,
  workspaceId: state.workspace!.id,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onRegister: (workspaceId: string, email: string) =>
    dispatch(authActions.register.request(workspaceId, email)),
  onRegisterCancel: () =>
    dispatch(authActions.register.cancel()),
});
