/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState, useEffect } from 'react';

import { RegisterMeta } from '@reserve/client/reducers/view/register';
import View from './view';
//#endregion

//#region Component interfaces
export interface Props {
  readonly workspaceId: string;
  readonly meta: RegisterMeta;
  readonly onRegister: (workspaceId: string, email: string) => any;
  readonly onRegisterCancel: () => any;
}
//#endregion

const Register: React.FunctionComponent<Props> = props => {
  const [email, setEmail] = useState('');

  useEffect(() => () => props.onRegisterCancel(), [
    props,
    props.onRegisterCancel,
  ]);

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    props.onRegister(props.workspaceId, email);
  }

  return (
    <View
      {...props.meta}
      email={email}
      onEmailChange={setEmail}
      onSubmit={handleSubmit}
    />
  );
};

export default Register;
