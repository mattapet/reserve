/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect } from 'react';
import * as querystring from 'querystring';
import { RouteComponentProps } from 'react-router-dom';
import Spinner from '@reserve/client/components/Spinner';

import { LoginMeta } from '@reserve/client/reducers/view/login';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly meta: LoginMeta;
  readonly onLoginAuthority: (
    workspaceId: string,
    name: string,
    code: string
  ) => any;
}
//#endregion

const Auth: React.FunctionComponent<Props> = props => {
  useEffect(() => {
    const authority = (props.match.params as any).authority;
    const query = props.location.search.slice(1);
    const { code, state } = querystring.parse(query);
    if (!code || !state) {
      return props.history.replace('/');
    }
    props.onLoginAuthority(
      decodeURIComponent(state as string),
      authority,
      code as string
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (props.meta.error) {
      props.history.replace('/');
    }
  }, [props.history, props.meta.error]);

  return <Spinner />;
};

export default Auth;
