/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect } from 'react';
import * as querystring from 'querystring';
import { RouteComponentProps } from 'react-router-dom';
import Spinner from '@reserve/client/components/Spinner';

import { LoginMeta } from '@reserve/client/reducers/view/login';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly workspaceId: string;
  readonly meta: LoginMeta;
  readonly onConfirmEmail: (
    workspaceId: string,
    email: string,
    expiry: string,
    signature: string
  ) => any;
}
//#endregion

const ConfirmEmail: React.FunctionComponent<Props> = props => {
  useEffect(() => {
    const query = props.location.search.slice(1); // Drop leading `?`
    const { workspaceId, email, expiry, signature } = querystring.parse(query);
    props.onConfirmEmail(
      workspaceId as string,
      email as string,
      expiry as string,
      signature as string
    );
  }, [props]);

  useEffect(() => {
    if (props.meta.error) {
      props.history.replace('/');
    }
  }, [props.history, props.meta.error]);

  return <Spinner />;
};

export default ConfirmEmail;
