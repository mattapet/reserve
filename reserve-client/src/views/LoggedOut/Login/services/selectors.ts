/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createSelector } from 'reselect';
import config from '@reserve/client/config';
import { workspaceSelector } from '@reserve/client/services/workspaceSelectors';
//#endregion

export const stateSelector = createSelector(workspaceSelector, ({ id }) =>
  JSON.stringify({ clientUrl: config.workspaceUrl, workspaceId: id })
);
