/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Link } from 'react-router-dom';

import config from '@reserve/client/config';
import { SubmitButton } from './styled';
import Container from './Container';
//#endregion

//#region Component interfaces
interface LoginViewProps {
  readonly state: string;
}
//#endregion

const View: React.FunctionComponent<LoginViewProps> = props => (
  <Container>
    <SubmitButton type="primary">
      <a
        href={`${
          config.apiUrl
        }api/v1/login/silicon_hill?state=${encodeURIComponent(props.state)}`}
      >
        Login with IS
      </a>
    </SubmitButton>
    <Link to="/login/password" replace>
      <SubmitButton htmlType="submit">Login with password</SubmitButton>
    </Link>
  </Container>
);

export default View;
