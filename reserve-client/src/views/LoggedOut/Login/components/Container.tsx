/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Card } from 'antd';

import { ContentWrapper } from './styled';
//#endregion

const Container: React.FunctionComponent = props => (
  <ContentWrapper>
    <Card title="Login">{props.children}</Card>
  </ContentWrapper>
);

export default Container;
