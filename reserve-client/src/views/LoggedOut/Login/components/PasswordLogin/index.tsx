/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import View from './view';
//#endregion

export interface Props {
  readonly onLogin: (payload: {
    readonly email: string;
    readonly password: string;
  }) => any;
  readonly pending: boolean;
}

const PasswordLogin: React.FunctionComponent<Props> = props => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();

    props.onLogin({ email, password });
  }

  return (
    <View
      email={email}
      password={password}
      onEmailChange={setEmail}
      onPasswordChange={setPassword}
      onSubmit={handleSubmit}
      pending={props.pending}
    />
  );
};

export default PasswordLogin;
