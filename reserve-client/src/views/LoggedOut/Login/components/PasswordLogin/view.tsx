/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Link } from 'react-router-dom';

import { FormContainer, Input, SubmitButton } from '../styled';
import Container from '../Container';
//#endregion

//#region Component interfaces
interface LoginViewProps {
  readonly pending: boolean;
  readonly email: string;
  readonly password: string;
  readonly onEmailChange: (e: string) => any;
  readonly onPasswordChange: (e: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
}
//#endregion

const View: React.FunctionComponent<LoginViewProps> = props => (
  <Container>
    <FormContainer onSubmit={props.onSubmit}>
      <Input
        type="email"
        placeholder="Email"
        value={props.email}
        onChange={props.onEmailChange}
        autoFocus
      />
      <Input
        type="password"
        placeholder="Password"
        value={props.password}
        onChange={props.onPasswordChange}
      />
      <SubmitButton type="primary" htmlType="submit" loading={props.pending}>
        Login
      </SubmitButton>
      <Link to="/login" replace>
        <SubmitButton>Cancel</SubmitButton>
      </Link>
    </FormContainer>
  </Container>
);

export default View;
