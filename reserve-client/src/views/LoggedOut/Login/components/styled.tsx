/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import styled from 'styled-components';
import { Button, Form, Layout } from 'antd';
import RSInput from '@reserve/client/components/Input';
//#endregion

export const Input = styled(RSInput)`
  margin: 5px;
`;

export const FormContainer = styled(Form)`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-content: center;
`;

export const ContentWrapper = styled(Layout)`
  height: 100%;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const SubmitButton = styled(Button)`
  width: 100%;
  margin: 5px;
`;
