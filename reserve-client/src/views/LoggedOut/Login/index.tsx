/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import LoginMenu from './screens/LoginMenu';
import PasswordLogin from './screens/PasswordLogin';

//#endregion

const Router: React.FunctionComponent = () => (
  <Switch>
    <Route path="/login" component={LoginMenu} exact />
    <Route path="/login/password" component={PasswordLogin} exact />
  </Switch>
);

export default Router;
