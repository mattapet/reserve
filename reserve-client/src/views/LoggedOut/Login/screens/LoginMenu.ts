/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import LoginMenu from '../components/LoginMenu';
import { stateSelector } from '../services/selectors';
//#endregion

const mapStateToProps = createSelector(stateSelector, state => ({
  state,
}));

export default connect(mapStateToProps, null)(LoginMenu);
