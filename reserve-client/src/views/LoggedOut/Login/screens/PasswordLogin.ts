/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Dispatch, bindActionCreators } from 'redux';
import { loginMetaSelector } from '@reserve/client/services/authSelectors';
import { authActions } from '@reserve/client/actions';

import PasswordLogin from '../components/PasswordLogin';
//#endregion

const mapStateToProps = createSelector(loginMetaSelector, ({ pending }) => ({
  pending,
}));

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ onLogin: authActions.login.request }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PasswordLogin);
