/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ProfileDTO } from '@reserve/client/lib/types/dto/Profile.dto';
import { Profile } from '@reserve/client/lib/types/Profile';
import { UserDTO } from '@reserve/client/lib/types/dto/User';
import { User } from '@reserve/client/lib/types/User';
//#endregion

export function decode(dto: UserDTO): User {
  if (dto.email && dto.profile) {
    return {
      id: dto.id,
      role: dto.role,
      workspace: dto.workspace,
      isOwner: dto.is_owner,
      lastLogin: new Date(dto.last_login ?? 0),
      email: dto.email,
      profile: {
        firstName: dto.profile.first_name,
        lastName: dto.profile.last_name,
        phone: dto.profile.phone,
      } as Profile,
      banned: dto.banned,
    } as User;
  } else {
    return {
      id: dto.id,
      workspace: dto.workspace,
      lastLogin: new Date(dto.last_login ?? 0),
    } as User;
  }
}

export function encode(user: User): UserDTO {
  if (user.email && user.profile) {
    return {
      id: user.id,
      role: user.role,
      workspace: user.workspace,
      last_login: user.lastLogin.toISOString(),
      is_owner: user.isOwner,
      email: user.email,
      profile: {
        first_name: user.profile.firstName,
        last_name: user.profile.lastName,
        phone: user.profile.phone,
      } as ProfileDTO,
      banned: user.banned,
    } as UserDTO;
  } else {
    return {
      id: user.id,
      workspace: user.workspace,
      last_login: user.lastLogin.toISOString(),
    } as UserDTO;
  }
}
