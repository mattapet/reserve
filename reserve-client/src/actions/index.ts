/*

* Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as authActions from './auth';
import * as requestActions from './request';
import * as resourceActions from './resources';
import * as reservationActions from './reservations';
import * as restrictionActions from './restrictions';
import * as userActions from './users';
import * as webhookActions from './webhook';
import * as workspaceActions from './workspace';
import * as reservationComments from './reservationComments';
import * as templateActions from './template';
import * as notificationActions from './notifications';
//#endregion

export type RootAction =
  | authActions.AuthAction
  | requestActions.RequestAction
  | resourceActions.ResourceAction
  | reservationActions.ReservationAction
  | restrictionActions.RestrictionAction
  | userActions.UserAction
  | webhookActions.WebhookAction
  | workspaceActions.WorkspaceAction
  | reservationComments.ReservationCommentAction
  | notificationActions.NotificationAction
  | templateActions.TemplateAction;

export {
  authActions,
  requestActions,
  resourceActions,
  reservationActions,
  restrictionActions,
  userActions,
  webhookActions,
  workspaceActions,
  reservationComments,
  notificationActions,
  templateActions,
};
