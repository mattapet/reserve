/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { REMOVE_RESOURCE } from './constants';
//#endregion

export const request = createStandardAction(REMOVE_RESOURCE.REQUEST).map(
  (id: number) => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: () => success(id),
      url: `api/v1/resource/${id}`,
    },
  }),
);

export const success =
  createStandardAction(REMOVE_RESOURCE.SUCCESS)<number>();

export const cancel = createAction(REMOVE_RESOURCE.CANCEL);

export const fail = createStandardAction(REMOVE_RESOURCE.FAIL)<Error>();
