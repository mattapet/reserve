/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { ResourceDTO } from '@reserve/client/lib/types/dto/Resource.dto';
import { Resource } from '@reserve/client/lib/types/Resource';
import { EDIT_RESOURCE } from './constants';
import * as mapper from './mapper';
//#endregion

export const request = createStandardAction(EDIT_RESOURCE.REQUEST).map(
  (resource: Resource) => ({
    payload: mapper.encode(resource),
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/resource/${resource.id}`,
    },
  })
);

export const success = createStandardAction(EDIT_RESOURCE.SUCCESS).map(
  (payload: ResourceDTO) => ({
    payload: mapper.decode(payload),
  })
);

export const cancel = createAction(EDIT_RESOURCE.CANCEL);

export const fail = createStandardAction(EDIT_RESOURCE.FAIL)<Error>();
