/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { CONFIRM_EMAIL } from './constants';
//#endregion

export interface RequestPayload {
  readonly workspaceId: string;
  readonly email: string;
  readonly expiry: string;
  readonly signature: string;
}

export const request = createStandardAction(CONFIRM_EMAIL.REQUEST)
  .map((payload: RequestPayload) => ({
      payload: {
        ...payload,
        workspace_id: payload.workspaceId,
      },
      meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: 'api/v1/email/confirm',
      }
    }),
  );

export const success = createStandardAction(CONFIRM_EMAIL.SUCCESS)
  .map((payload: any) => ({ payload }));

export const cancel = createAction(CONFIRM_EMAIL.CANCEL);

export const fail = createStandardAction(
  CONFIRM_EMAIL.FAIL
)<Error>();
