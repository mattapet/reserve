/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { LOGOUT } from './constants';
//#endregion

export const request = createStandardAction(LOGOUT.REQUEST).map(
  (refreshToken: string) => ({
    payload: { refresh_token: refreshToken },
    meta: {
      headers: null,
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/oauth2/token/revoke',
    },
  })
);

export const success = createAction(LOGOUT.SUCCESS);

export const cancel = createAction(LOGOUT.CANCEL);

export const fail = createStandardAction(LOGOUT.FAIL)<Error>();
