/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { ReservationTemplate } from '@reserve/client/lib/types/ReservationTemplate';
import { SET_TEMPLATE } from './constants';
//#endregion

export const request = createStandardAction(SET_TEMPLATE.REQUEST).map(
  (template: string) => ({
    payload: { template },
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/settings/reservation/template',
    },
  })
);

export const success = createStandardAction(SET_TEMPLATE.SUCCESS)<
  ReservationTemplate
>();

export const cancel = createAction(SET_TEMPLATE.CANCEL);

export const fail = createStandardAction(SET_TEMPLATE.FAIL)<Error>();
