/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export enum FETCH_TEMPLATE {
  REQUEST = 'FETCH_TEMPLATE_REQUEST',
  SUCCESS = 'FETCH_TEMPLATE_SUCCESS',
  CANCEL = 'FETCH_TEMPLATE_CANCEL',
  FAIL = 'FETCH_TEMPLATE_FAIL',
}

export enum SET_TEMPLATE {
  REQUEST = 'SET_TEMPLATE_REQUEST',
  SUCCESS = 'SET_TEMPLATE_SUCCESS',
  CANCEL = 'SET_TEMPLATE_CANCEL',
  FAIL = 'SET_TEMPLATE_FAIL',
}

export enum DELETE_TEMPLATE {
  REQUEST = 'DELETE_TEMPLATE_REQUEST',
  SUCCESS = 'DELETE_TEMPLATE_SUCCESS',
  CANCEL = 'DELETE_TEMPLATE_CANCEL',
  FAIL = 'DELETE_TEMPLATE_FAIL',
}
