/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { DELETE_TEMPLATE } from './constants';
//#endregion

export const request = createStandardAction(DELETE_TEMPLATE.REQUEST).map(
  () => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/settings/reservation/template',
    },
  })
);

export const success = createStandardAction(DELETE_TEMPLATE.SUCCESS)<void>();

export const cancel = createAction(DELETE_TEMPLATE.CANCEL);

export const fail = createStandardAction(DELETE_TEMPLATE.FAIL)<Error>();
