/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { ReservationTemplate } from '@reserve/client/lib/types/ReservationTemplate';
import { FETCH_TEMPLATE } from './constants';
//#endregion

export const request = createStandardAction(FETCH_TEMPLATE.REQUEST).map(() => ({
  meta: {
    onCancel: cancel,
    onFail: fail,
    onSuccess: success,
    url: 'api/v1/settings/reservation/template',
  },
}));

export const success = createStandardAction(FETCH_TEMPLATE.SUCCESS)<
  ReservationTemplate
>();

export const cancel = createAction(FETCH_TEMPLATE.CANCEL);

export const fail = createStandardAction(FETCH_TEMPLATE.FAIL)<Error>();
