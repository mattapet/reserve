/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import * as set from './set';
import * as fetch from './fetch';
import * as del from './delete';
import { ActionType } from 'typesafe-actions';

export { set, fetch, del as delete };

export type TemplateFetchAction =
  | ActionType<typeof fetch.request>
  | ActionType<typeof fetch.success>
  | ActionType<typeof fetch.cancel>
  | ActionType<typeof fetch.fail>;

export type TemplateSetAction =
  | ActionType<typeof set.request>
  | ActionType<typeof set.success>
  | ActionType<typeof set.cancel>
  | ActionType<typeof set.fail>;

export type TemplateDeleteAction =
  | ActionType<typeof del.request>
  | ActionType<typeof del.success>
  | ActionType<typeof del.cancel>
  | ActionType<typeof del.fail>;

export type TemplateAction =
  | TemplateFetchAction
  | TemplateSetAction
  | TemplateDeleteAction;
