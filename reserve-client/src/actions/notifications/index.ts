/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ActionType, createStandardAction } from 'typesafe-actions';
import * as constants from './constants';
//#endregion

export const success = createStandardAction(constants.NOTIFICATION_SUCCESS)<
  string
>();

export const info = createStandardAction(constants.NOTIFICATION_INFO)<string>();

export const warning = createStandardAction(constants.NOTIFICATION_WARNING)<
  string
>();

export const error = createStandardAction(constants.NOTIFICATION_ERROR)<
  string
>();

export type NotificationAction =
  | ActionType<typeof success>
  | ActionType<typeof info>
  | ActionType<typeof warning>
  | ActionType<typeof error>;
