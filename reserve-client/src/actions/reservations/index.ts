/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ActionType } from 'typesafe-actions';

import * as fetchById from './fetchReservationById';
import * as fetch from './fetchReservations';
import * as create from './createReservation';
import * as edit from './editReservation';
import * as confirmById from './confirmById';
import * as cancelById from './cancelById';
import * as rejectById from './rejectById';
//#endregion

export { fetchById, fetch, create, edit, confirmById, cancelById, rejectById };

//#region types
export type ReservationActionFetchById =
    ActionType<typeof fetchById.request>
  | ActionType<typeof fetchById.success>
  | ActionType<typeof fetchById.cancel>
  | ActionType<typeof fetchById.fail>;

export type ReservationActionFetch =
    ActionType<typeof fetch.request>
  | ActionType<typeof fetch.success>
  | ActionType<typeof fetch.cancel>
  | ActionType<typeof fetch.fail>;

export type ReservationActionCreate =
    ActionType<typeof create.request>
  | ActionType<typeof create.success>
  | ActionType<typeof create.cancel>
  | ActionType<typeof create.fail>;

export type ReservationActionEdit =
    ActionType<typeof edit.request>
  | ActionType<typeof edit.success>
  | ActionType<typeof edit.cancel>
  | ActionType<typeof edit.fail>;

export type ReservationActionConfirmById =
    ActionType<typeof confirmById.request>
  | ActionType<typeof confirmById.success>
  | ActionType<typeof confirmById.cancel>
  | ActionType<typeof confirmById.fail>;

export type ReservationActionCancelById =
    ActionType<typeof cancelById.request>
  | ActionType<typeof cancelById.success>
  | ActionType<typeof cancelById.cancel>
  | ActionType<typeof cancelById.fail>;

export type ReservationActionRejectById =
    ActionType<typeof rejectById.request>
  | ActionType<typeof rejectById.success>
  | ActionType<typeof rejectById.cancel>
  | ActionType<typeof rejectById.fail>;

export type ReservationAction =
    ReservationActionFetchById
  | ReservationActionFetch
  | ReservationActionCreate
  | ReservationActionEdit
  | ReservationActionConfirmById
  | ReservationActionCancelById
  | ReservationActionRejectById;
//#endregion
