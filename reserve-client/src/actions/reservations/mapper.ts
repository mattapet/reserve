/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationDTO } from '@reserve/client/lib/types/dto/Reservation.dto';
import { Reservation } from '@reserve/client/lib/types/Reservation';
//#endregion

export function decode(dto: ReservationDTO): Reservation {
  return {
    id: dto.id,
    dateStart: new Date(dto.date_start),
    dateEnd: new Date(dto.date_end),
    resourceIds: dto.resource_ids,
    notes: dto.notes,
    state: dto.state,
    createdAt: new Date(dto.created_at),
    userId: dto.user_id,
  } as Reservation;
}

export function encode(reservation: Reservation): ReservationDTO {
  return {
    id: reservation.id,
    date_start: reservation.dateStart.toISOString(),
    date_end: reservation.dateEnd.toISOString(),
    resource_ids: reservation.resourceIds,
    notes: reservation.notes,
    state: reservation.state,
    created_at: reservation.createdAt.toISOString(),
    user_id: reservation.userId,
  } as ReservationDTO;
}
