/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ActionType } from 'typesafe-actions';

import * as fetch from './fetch';
import * as create from './create';
import * as edit from './edit';
import * as remove from './remove';
//#endregion

export { fetch, create, edit, remove };

//#region types
export type ReservationCommentActionFetch =
    ActionType<typeof fetch.request>
  | ActionType<typeof fetch.success>
  | ActionType<typeof fetch.cancel>
  | ActionType<typeof fetch.fail>;

export type ReservationCommentActionCreate =
    ActionType<typeof create.request>
  | ActionType<typeof create.success>
  | ActionType<typeof create.cancel>
  | ActionType<typeof create.fail>;

export type ReservationCommentActionEdit =
    ActionType<typeof edit.request>
  | ActionType<typeof edit.success>
  | ActionType<typeof edit.cancel>
  | ActionType<typeof edit.fail>;

export type ReservationCommentActionRemove =
    ActionType<typeof remove.request>
  | ActionType<typeof remove.success>
  | ActionType<typeof remove.cancel>
  | ActionType<typeof remove.fail>;

export type ReservationCommentAction =
  | ReservationCommentActionFetch
  | ReservationCommentActionCreate
  | ReservationCommentActionEdit
  | ReservationCommentActionRemove;
//#endregion
