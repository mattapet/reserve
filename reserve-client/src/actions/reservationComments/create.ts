/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { ReservationCommentDTO } from '@reserve/client/lib/types/dto/ReservationComment.dto';
import { CREATE_RESERVATION_COMMENT } from './constants';
import * as mapper from './mapper';
//#endregion

export interface Payload {
  readonly reservationId: string;
  readonly comment: string;
}

export const request = createStandardAction(
  CREATE_RESERVATION_COMMENT.REQUEST
).map(({ reservationId, comment }: Payload) => ({
  payload: {
    comment,
  },
  meta: {
    onCancel: cancel,
    onFail: fail,
    onSuccess: success,
    url: `api/v1/reservation/${reservationId}/comment`,
  },
}));

export const success = createStandardAction(
  CREATE_RESERVATION_COMMENT.SUCCESS
).map((dto: ReservationCommentDTO) => ({
  payload: mapper.decode(dto),
}));

export const cancel = createAction(CREATE_RESERVATION_COMMENT.CANCEL);

export const fail = createStandardAction(CREATE_RESERVATION_COMMENT.FAIL)<
  Error
>();
