/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { REMOVE_RESERVATION_COMMENT } from './constants';
import { ReservationCommentDTO } from '@reserve/client/lib/types/dto/ReservationComment.dto';
import * as mapper from './mapper';
//#endregion

interface Payload {
  readonly reservationId: string;
  readonly commentId: string;
}

export const request = createStandardAction(
  REMOVE_RESERVATION_COMMENT.REQUEST
).map(({ reservationId, commentId }: Payload) => ({
  meta: {
    onCancel: cancel,
    onFail: fail,
    onSuccess: success,
    url: `api/v1/reservation/${reservationId}/comment/${commentId}`,
  },
}));

export const success = createStandardAction(
  REMOVE_RESERVATION_COMMENT.SUCCESS
).map((payload: ReservationCommentDTO) => ({
  payload: mapper.decode(payload),
}));

export const cancel = createAction(REMOVE_RESERVATION_COMMENT.CANCEL);

export const fail = createStandardAction(REMOVE_RESERVATION_COMMENT.FAIL)<
  Error
>();
