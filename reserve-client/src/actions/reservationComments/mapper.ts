/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationCommentDTO } from '@reserve/client/lib/types/dto/ReservationComment.dto';
import { ReservationComment } from '@reserve/client/lib/types/ReservationComment';
//#endregion

export function decode(dto: ReservationCommentDTO): ReservationComment {
  return {
    id: dto.id,
    reservationId: dto.reservation_id,
    timestamp: new Date(dto.timestamp),
    author: dto.author,
    comment: dto.comment,
    deleted: dto.deleted,
  } as ReservationComment;
}

export function encode(comment: ReservationComment): ReservationCommentDTO {
  return {
    id: comment.id,
    reservation_id: comment.reservationId,
    timestamp: comment.timestamp.toISOString(),
    author: comment.author,
    comment: comment.comment,
    deleted: comment.deleted,
  } as ReservationCommentDTO;
}
