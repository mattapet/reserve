/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { EDIT_RESERVATION_COMMENT } from './constants';
import * as mapper from './mapper';
import { ReservationCommentDTO } from '@reserve/client/lib/types/dto/ReservationComment.dto';
//#endregion

interface Payload {
  readonly comment: string;
  readonly reservationId: string;
  readonly commentId: string;
}

export const request = createStandardAction(
  EDIT_RESERVATION_COMMENT.REQUEST
).map(({ comment, reservationId, commentId }: Payload) => ({
  payload: {
    comment,
  },
  meta: {
    onCancel: cancel,
    onFail: fail,
    onSuccess: success,
    url: `api/v1/reservation/${reservationId}/comment/${commentId}`,
  },
}));

export const success = createStandardAction(
  EDIT_RESERVATION_COMMENT.SUCCESS
).map((dto: ReservationCommentDTO) => ({
  payload: mapper.decode(dto),
}));

export const cancel = createAction(EDIT_RESERVATION_COMMENT.CANCEL);

export const fail = createStandardAction(EDIT_RESERVATION_COMMENT.FAIL)<
  Error
>();
