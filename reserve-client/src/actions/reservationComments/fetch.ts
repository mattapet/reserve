/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { FETCH_RESERVATION_COMMENTS } from './constants';
import * as mapper from './mapper';
import { ReservationCommentDTO } from '@reserve/client/lib/types/dto/ReservationComment.dto';
//#endregion

export const request = createStandardAction(
  FETCH_RESERVATION_COMMENTS.REQUEST
).map((reservationId: string) => ({
  meta: {
    onCancel: cancel,
    onFail: fail,
    onSuccess: (comments: ReservationCommentDTO[]) =>
      success({ reservationId, comments }),
    url: `api/v1/reservation/${reservationId}/comment`,
  },
}));

export const success = createStandardAction(
  FETCH_RESERVATION_COMMENTS.SUCCESS
).map(
  (payload: {
    readonly comments: ReservationCommentDTO[];
    readonly reservationId: string;
  }) => ({
    payload: { ...payload, comments: payload.comments.map(mapper.decode) },
  })
);

export const cancel = createAction(FETCH_RESERVATION_COMMENTS.CANCEL);

export const fail = createStandardAction(FETCH_RESERVATION_COMMENTS.FAIL)<
  Error
>();
