/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { WebhookDTO } from '@reserve/client/lib/types/dto/Webhook.dto';
import { Webhook } from '@reserve/client/lib/types/Webhook';
import { EDIT_WEBHOOK } from './constants';
import * as mapper from './mapper';
//#endregion

export const request = createStandardAction(EDIT_WEBHOOK.REQUEST).map(
  (webhook: Webhook) => ({
    payload: mapper.encode(webhook),
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/webhook/${webhook.id}`,
    },
  })
);

export const success = createStandardAction(EDIT_WEBHOOK.SUCCESS).map(
  (payload: WebhookDTO) => ({
    payload: mapper.decode(payload),
  })
);

export const cancel = createAction(EDIT_WEBHOOK.CANCEL);

export const fail = createStandardAction(EDIT_WEBHOOK.FAIL)<Error>();
