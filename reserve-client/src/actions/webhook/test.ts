/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { TEST_WEBHOOK, RESET_WEBHOOK_TEST } from './constants';
import { WebhookTest } from '@reserve/client/lib/types/WebhookTest';
//#endregion

export const reset = createAction(RESET_WEBHOOK_TEST);

export interface Payload {
  readonly id: number;
  readonly content: string;
}
export const request = createStandardAction(TEST_WEBHOOK.REQUEST).map(
  ({ id, content }: Payload) => ({
    paylaod: content,
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/webhook/${id}/test`,
    },
  })
);

export const success = createStandardAction(TEST_WEBHOOK.SUCCESS)<
  WebhookTest
>();

export const cancel = createAction(TEST_WEBHOOK.CANCEL);

export const fail = createStandardAction(TEST_WEBHOOK.FAIL)<Error>();
