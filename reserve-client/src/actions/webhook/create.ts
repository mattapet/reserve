/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { CREATE_WEBHOOK } from './constants';
import { WebhookDTO } from '@reserve/client/lib/types/dto/Webhook.dto';
import { EncodingType } from '@reserve/client/lib/types/EncodingType';
import { EventType } from '@reserve/client/lib/types/EventType';
import * as mapper from './mapper';
//#endregion

export interface Payload {
  readonly payloadUrl: string;
  readonly secret: string;
  readonly encoding: EncodingType;
  readonly events: EventType[];
}

export const request = createStandardAction(CREATE_WEBHOOK.REQUEST).map(
  (payload: Payload) => ({
    payload: {
      payload_url: payload.payloadUrl,
      secret: payload.secret,
      encoding: payload.encoding,
      events: payload.events,
    },
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/webhook',
    },
  })
);

export const success = createStandardAction(CREATE_WEBHOOK.SUCCESS).map(
  (dto: WebhookDTO) => ({
    payload: mapper.decode(dto),
  })
);

export const cancel = createAction(CREATE_WEBHOOK.CANCEL);

export const fail = createStandardAction(CREATE_WEBHOOK.FAIL)<Error>();
