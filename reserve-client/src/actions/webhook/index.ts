/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ActionType } from 'typesafe-actions';

import * as fetchById from './fetchById';
import * as fetch from './fetch';
import * as create from './create';
import * as edit from './edit';
import * as remove from './remove';
import * as toggleActive from './toggleActive';
import * as test from './test';
//#endregion

export { fetchById, fetch, create, edit, remove, toggleActive, test };

//#region types
export type WebhookActionFetchById =
  | ActionType<typeof fetchById.request>
  | ActionType<typeof fetchById.success>
  | ActionType<typeof fetchById.cancel>
  | ActionType<typeof fetchById.fail>;

export type WebhookActionFetch =
  | ActionType<typeof fetch.request>
  | ActionType<typeof fetch.success>
  | ActionType<typeof fetch.cancel>
  | ActionType<typeof fetch.fail>;

export type WebhookActionCreate =
  | ActionType<typeof create.request>
  | ActionType<typeof create.success>
  | ActionType<typeof create.cancel>
  | ActionType<typeof create.fail>;

export type WebhookActionEdit =
  | ActionType<typeof edit.request>
  | ActionType<typeof edit.success>
  | ActionType<typeof edit.cancel>
  | ActionType<typeof edit.fail>;

export type WebhookActionRemove =
  | ActionType<typeof remove.request>
  | ActionType<typeof remove.success>
  | ActionType<typeof remove.cancel>
  | ActionType<typeof remove.fail>;

export type WebhookActionToggleActive =
  | ActionType<typeof toggleActive.request>
  | ActionType<typeof toggleActive.success>
  | ActionType<typeof toggleActive.cancel>
  | ActionType<typeof toggleActive.fail>;

export type WebhookTestAction =
  | ActionType<typeof test.request>
  | ActionType<typeof test.success>
  | ActionType<typeof test.cancel>
  | ActionType<typeof test.fail>
  | ActionType<typeof test.reset>;

export type WebhookAction =
  | WebhookActionFetchById
  | WebhookActionFetch
  | WebhookActionCreate
  | WebhookActionEdit
  | WebhookActionRemove
  | WebhookActionToggleActive
  | WebhookTestAction;
//#endregion
