/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { FETCH_ANNOUNCEMENT } from './constants';
import { WorkspaceAnnouncement } from '../../lib/types/WorkspaceAnnouncement';
//#endregion

export const request = createStandardAction(FETCH_ANNOUNCEMENT.REQUEST).map(
  () => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/workspace/announcement/active`,
    },
  })
);

export const success = createStandardAction(FETCH_ANNOUNCEMENT.SUCCESS)<
  WorkspaceAnnouncement | undefined
>();

export const cancel = createAction(FETCH_ANNOUNCEMENT.CANCEL);

export const fail = createStandardAction(FETCH_ANNOUNCEMENT.FAIL)<Error>();
