/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { DELETE_ANNOUNCEMENT } from './constants';
//#endregion

export const request = createStandardAction(DELETE_ANNOUNCEMENT.REQUEST).map(
  (name: string) => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: () => success(name),
      url: `api/v1/workspace/announcement/${name}`,
    },
  })
);

export const success = createStandardAction(DELETE_ANNOUNCEMENT.SUCCESS)<
  string
>();

export const cancel = createAction(DELETE_ANNOUNCEMENT.CANCEL);

export const fail = createStandardAction(DELETE_ANNOUNCEMENT.FAIL)<Error>();
