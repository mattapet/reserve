/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { CREATE_ANNOUNCEMENT } from './constants';
import { WorkspaceAnnouncement } from '../../lib/types/WorkspaceAnnouncement';
//#endregion

export const request = createStandardAction(CREATE_ANNOUNCEMENT.REQUEST).map(
  (payload: { name: string; text: string }) => ({
    payload,
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/workspace/announcement`,
    },
  })
);

export const success = createStandardAction(CREATE_ANNOUNCEMENT.SUCCESS)<
  WorkspaceAnnouncement
>();

export const cancel = createAction(CREATE_ANNOUNCEMENT.CANCEL);

export const fail = createStandardAction(CREATE_ANNOUNCEMENT.FAIL)<Error>();
