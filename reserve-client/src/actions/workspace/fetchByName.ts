/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import config from '@reserve/client/config';
import { createAction, createStandardAction } from 'typesafe-actions';
import { FETCH_BY_NAME } from './constants';
import { Workspace } from '@reserve/client/lib/types/Workspace';
//#endregion

export const request = createStandardAction(FETCH_BY_NAME.REQUEST).map(() => ({
  meta: {
    onCancel: cancel,
    onFail: fail,
    onSuccess: success,
    url: `api/v1/workspace?name=${encodeURIComponent(config.workspaceName)}`,
  },
}));

export const success = createStandardAction(FETCH_BY_NAME.SUCCESS)<Workspace>();

export const cancel = createAction(FETCH_BY_NAME.CANCEL);

export const fail = createStandardAction(FETCH_BY_NAME.FAIL)<Error>();
