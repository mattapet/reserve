/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { TOGGLE_ANNOUNCEMENT } from './constants';
import { WorkspaceAnnouncement } from '../../lib/types/WorkspaceAnnouncement';
//#endregion

export const request = createStandardAction(TOGGLE_ANNOUNCEMENT.REQUEST).map(
  (name: string) => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/workspace/announcement/${name}/toggle`,
    },
  })
);

export const success = createStandardAction(TOGGLE_ANNOUNCEMENT.SUCCESS)<
  WorkspaceAnnouncement
>();

export const cancel = createAction(TOGGLE_ANNOUNCEMENT.CANCEL);

export const fail = createStandardAction(TOGGLE_ANNOUNCEMENT.FAIL)<Error>();
