/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { FETCH_ANNOUNCEMENTS } from './constants';
import { WorkspaceAnnouncement } from '../../lib/types/WorkspaceAnnouncement';
//#endregion

export const request = createStandardAction(FETCH_ANNOUNCEMENTS.REQUEST).map(
  () => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/workspace/announcement`,
    },
  })
);

export const success = createStandardAction(FETCH_ANNOUNCEMENTS.SUCCESS)<
  WorkspaceAnnouncement[]
>();

export const cancel = createAction(FETCH_ANNOUNCEMENTS.CANCEL);

export const fail = createStandardAction(FETCH_ANNOUNCEMENTS.FAIL)<Error>();
