/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ActionType } from 'typesafe-actions';

import * as fetchByName from './fetchByName';
import * as remove from './remove';
import * as createAnnouncement from './createAnnouncement';
import * as deleteAnnouncement from './deleteAnnouncement';
import * as fetchAnnouncement from './fetchAnnouncement';
import * as fetchAnnouncements from './fetchAnnouncements';
import * as toggleAnnouncement from './toggleAnnouncement';
import * as transferOwnership from './transferOwnership';
//#endregion

export {
  fetchByName,
  remove,
  createAnnouncement,
  deleteAnnouncement,
  fetchAnnouncement,
  fetchAnnouncements,
  toggleAnnouncement,
  transferOwnership,
};

//#region types
export type WorkspaceFetchByName =
  | ActionType<typeof fetchByName.request>
  | ActionType<typeof fetchByName.success>
  | ActionType<typeof fetchByName.cancel>
  | ActionType<typeof fetchByName.fail>;

export type WorkspaceRemove =
  | ActionType<typeof remove.request>
  | ActionType<typeof remove.success>
  | ActionType<typeof remove.cancel>
  | ActionType<typeof remove.fail>;

export type WorkspaceAnnouncementCreate =
  | ActionType<typeof createAnnouncement.request>
  | ActionType<typeof createAnnouncement.success>
  | ActionType<typeof createAnnouncement.cancel>
  | ActionType<typeof createAnnouncement.fail>;

export type WorkspaceAnnouncementDelete =
  | ActionType<typeof deleteAnnouncement.request>
  | ActionType<typeof deleteAnnouncement.success>
  | ActionType<typeof deleteAnnouncement.cancel>
  | ActionType<typeof deleteAnnouncement.fail>;

export type WorkspaceAnnouncementFetch =
  | ActionType<typeof fetchAnnouncement.request>
  | ActionType<typeof fetchAnnouncement.success>
  | ActionType<typeof fetchAnnouncement.cancel>
  | ActionType<typeof fetchAnnouncement.fail>;

export type WorkspacAnnouncementsFetch =
  | ActionType<typeof fetchAnnouncements.request>
  | ActionType<typeof fetchAnnouncements.success>
  | ActionType<typeof fetchAnnouncements.cancel>
  | ActionType<typeof fetchAnnouncements.fail>;

export type WorkspaceAnnouncementToggle =
  | ActionType<typeof toggleAnnouncement.request>
  | ActionType<typeof toggleAnnouncement.success>
  | ActionType<typeof toggleAnnouncement.cancel>
  | ActionType<typeof toggleAnnouncement.fail>;

export type WorkspaceTransferOwnership =
  | ActionType<typeof transferOwnership.request>
  | ActionType<typeof transferOwnership.success>
  | ActionType<typeof transferOwnership.cancel>
  | ActionType<typeof transferOwnership.fail>;

export type WorkspaceAction =
  | WorkspaceFetchByName
  | WorkspaceRemove
  | WorkspaceAnnouncementCreate
  | WorkspaceAnnouncementDelete
  | WorkspaceAnnouncementFetch
  | WorkspacAnnouncementsFetch
  | WorkspaceTransferOwnership
  | WorkspaceAnnouncementToggle;
//#endregion
