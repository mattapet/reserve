/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { REMOVE_WORKSPACE } from './constants';
//#endregion

export const request = createStandardAction(REMOVE_WORKSPACE.REQUEST).map(
  () => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: () => success(),
      url: `api/v1/workspace`,
    },
  })
);

export const success = createStandardAction(REMOVE_WORKSPACE.SUCCESS)();

export const cancel = createAction(REMOVE_WORKSPACE.CANCEL);

export const fail = createStandardAction(REMOVE_WORKSPACE.FAIL)<Error>();
