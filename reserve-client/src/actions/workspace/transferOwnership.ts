/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { TRANSFER_OWNERSHIP } from './constants';
import { Workspace } from '@reserve/client/lib/types/Workspace';
//#endregion

export interface Payload {
  readonly to: string;
}

export const request = createStandardAction(TRANSFER_OWNERSHIP.REQUEST).map(
  (payload: Payload) => ({
    payload,
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/workspace/transfer',
    },
  })
);

export const success = createStandardAction(TRANSFER_OWNERSHIP.SUCCESS)<
  Workspace
>();

export const cancel = createAction(TRANSFER_OWNERSHIP.CANCEL);

export const fail = createStandardAction(TRANSFER_OWNERSHIP.FAIL)<Error>();
