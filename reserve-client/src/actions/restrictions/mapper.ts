/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { RestrictionDTO } from '@reserve/client/lib/types/dto/Restriction';
import { Restriction } from '@reserve/client/lib/types/Restriction';
//#endregion

export function decode(dto: RestrictionDTO): Restriction {
  return {
    id: dto.id,
    dateStart: new Date(dto.date_start),
    dateEnd: new Date(dto.date_end),
    resourceIds: dto.resource_ids,
    description: dto.description,
  } as Restriction;
}

export function encode(restriction: Restriction): RestrictionDTO {
  return {
    id: restriction.id,
    date_start: restriction.dateStart.toISOString(),
    date_end: restriction.dateEnd.toISOString(),
    resource_ids: restriction.resourceIds,
    description: restriction.description,
  } as RestrictionDTO;
}
