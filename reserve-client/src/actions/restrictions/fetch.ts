/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { FETCH_RESTRICTIONS } from './constants';
import * as mapper from './mapper';
import { RestrictionDTO } from '@reserve/client/lib/types/dto/Restriction';
//#endregion

export const request = createStandardAction(FETCH_RESTRICTIONS.REQUEST).map(
  () => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/restriction',
    },
  })
);

export const success = createStandardAction(FETCH_RESTRICTIONS.SUCCESS).map(
  (payload: RestrictionDTO[]) => ({
    payload: payload.map(mapper.decode),
  })
);

export const cancel = createAction(FETCH_RESTRICTIONS.CANCEL);

export const fail = createStandardAction(FETCH_RESTRICTIONS.FAIL)<Error>();
