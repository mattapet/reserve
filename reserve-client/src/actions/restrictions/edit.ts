/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { EDIT_RESTRICTION } from './constants';
import * as mapper from './mapper';
import { Restriction } from '@reserve/client/lib/types/Restriction';
import { RestrictionDTO } from '@reserve/client/lib/types/dto/Restriction';
//#endregion

export const request = createStandardAction(EDIT_RESTRICTION.REQUEST).map(
  (restriction: Restriction) => ({
    payload: mapper.encode(restriction),
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/restriction/${restriction.id}`,
    },
  })
);

export const success = createStandardAction(EDIT_RESTRICTION.SUCCESS).map(
  (dto: RestrictionDTO) => ({
    payload: mapper.decode(dto),
  })
);

export const cancel = createAction(EDIT_RESTRICTION.CANCEL);

export const fail = createStandardAction(EDIT_RESTRICTION.FAIL)<Error>();
