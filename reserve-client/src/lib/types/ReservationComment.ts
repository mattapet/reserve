/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export interface ReservationComment {
  readonly id: string;
  readonly reservationId: string;
  readonly timestamp: Date;
  readonly author: string;
  readonly comment: string;
  readonly deleted: boolean;
}
