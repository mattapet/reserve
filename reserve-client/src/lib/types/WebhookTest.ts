/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export interface SuccessfullWebhookResponse {
  readonly status: number;
  readonly headers?: { [key: string]: string };
  readonly body?: string;
}

export interface ErrorWebhookResponse {
  readonly message: string;
}

export type WebhookTest = SuccessfullWebhookResponse | ErrorWebhookResponse;
