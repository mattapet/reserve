/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ProfileMapDTO } from './ProfileMap.dto';
//#endregion

export interface AuthorityDTO {
  readonly name: string;
  readonly description: string;
  readonly authorize_url: string;
  readonly token_url: string;
  readonly profile_urls: string[];
  readonly profile_map: ProfileMapDTO;
  readonly redirect_uri: string;
  readonly client_id: string;
  readonly client_secret: string;
  readonly scope: string[];
}
