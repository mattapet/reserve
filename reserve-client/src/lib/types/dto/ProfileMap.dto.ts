/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export interface ProfileMapDTO {
  readonly first_name: string;
  readonly last_name: string;
  readonly email: string;
  readonly phone?: string;
}
