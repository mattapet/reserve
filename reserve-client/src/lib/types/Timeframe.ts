/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TimeUnit } from './TimeUnit';
//#endregion

export interface Timeframe {
  amount: number;
  units: TimeUnit;
}
