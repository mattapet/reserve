/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
//#endregion

interface Meta {
  readonly pending: boolean;
  readonly retrieved: boolean;
  readonly error?: Error;
}

export function useDataProvider(
  meta: Meta,
  action: () => any,
  cancelAction?: () => any
): void {
  const dispatch = useDispatch();

  useEffect(() => {
    if (!meta.retrieved && !meta.pending && !meta.error) {
      dispatch(action());
      return () => cancelAction && cancelAction();
    }
    return () => {};
  }, [meta, action, cancelAction, dispatch]);
}
