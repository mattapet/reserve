/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useState } from 'react';
import { useQueryParam, JsonParam } from 'use-query-params';
//#endregion

export function useQueryState<T>(
  queryParam: string,
  defaultValue?: T | undefined
): [T | undefined, (newValue: T) => void] {
  const [queryValue, setQueryValue] = useQueryParam(queryParam, JsonParam);
  const [value, setValue] = useState<T>(queryValue ?? defaultValue);

  function updateValue(newValue: T): void {
    setQueryValue(newValue);
    setValue(newValue);
  }

  return [value, updateValue];
}
