/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { FilterInfo } from '@reserve/client/lib/types/FilterInfo';
import { useQueryState } from '@reserve/client/lib/hooks';
//#endregion

export function useFilterInfo<Entity extends {}>(): [
  FilterInfo<Entity> | undefined,
  (sortInfo: FilterInfo<Entity>) => void
] {
  return useQueryState('filterInfo');
}
