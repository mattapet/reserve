/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { FilterInfo } from '../types/FilterInfo';
import {
  SorterResult,
  PaginationConfig,
  TableCurrentDataSource,
} from 'antd/lib/table';
import { useFilterInfo } from './useFilterInfo';
import { useSortInfo } from './useSortInfo';
import { usePagination } from './usePagination';
import { SortInfo } from '../types/SortInfo';
//#endregion

export interface Result<Entity> {
  readonly filterInfo: FilterInfo<Entity> | undefined;
  readonly sortInfo: SortInfo<Entity> | undefined;
  readonly pagination?: {
    readonly current: number;
    readonly pageSize?: number;
  };
  readonly handlePaginationChange: (current: number, pageSize?: number) => void;
  readonly handleTableChange: (
    pagination: PaginationConfig,
    filterInfo: FilterInfo<Entity>,
    sortInfo: SorterResult<Entity>,
    extra: TableCurrentDataSource<Entity>
  ) => void;
}

export function useQueryControlledTable<Entity>(
  defaultSort?: SortInfo<Entity> | undefined
): Result<Entity> {
  const [filterInfo, setFilterInfo] = useFilterInfo<Entity>();
  const [sortInfo, setSortInfo] = useSortInfo(defaultSort);
  const [pagination, setPagination] = usePagination();

  function handleTableChange(
    pagination: PaginationConfig,
    filterInfo: FilterInfo<Entity>,
    sortInfo: SorterResult<Entity>,
    extra: TableCurrentDataSource<Entity>
  ): void {
    setSortInfo({
      columnKey: sortInfo.columnKey,
      order: sortInfo.order,
    } as any);
    setFilterInfo(filterInfo);
  }

  function handlePaginationChange(current: number, pageSize?: number): void {
    setPagination({ current, pageSize: pageSize });
  }

  return {
    filterInfo,
    sortInfo,
    pagination,
    handlePaginationChange,
    handleTableChange,
  };
}
