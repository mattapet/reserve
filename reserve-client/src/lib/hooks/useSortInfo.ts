/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { SortInfo } from '@reserve/client/lib/types/SortInfo';
import { useQueryState } from '@reserve/client/lib/hooks';
//#endregion

export function useSortInfo<Entity extends {}>(
  defaultValue?: SortInfo<Entity> | undefined
): [SortInfo<Entity> | undefined, (sortInfo: SortInfo<Entity>) => void] {
  const [sortInfo, setSortInfo] = useQueryState('sortInfo', defaultValue);

  function updateSortInfo(newValue: SortInfo<Entity>): void {
    setSortInfo({ columnKey: newValue.columnKey, order: newValue.order });
  }

  return [sortInfo, updateSortInfo];
}
