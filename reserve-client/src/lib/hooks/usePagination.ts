/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useQueryState } from '@reserve/client/lib/hooks';
import { PaginationInfo } from '@reserve/client/lib/types/PaginationInfo';
//#endregion

export function usePagination(
  defaultValue?: PaginationInfo
): [PaginationInfo | undefined, (paginationInfo: PaginationInfo) => void] {
  return useQueryState('pagination', defaultValue);
}
