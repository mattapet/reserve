/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { searchFilter } from '../filters';
import { User } from '@reserve/client/lib/types/User';
import { UserRole } from '@reserve/client/lib/types/UserRole';
//#endregion

describe('user fitlers', () => {
  function make_defaultUser(): User {
    return {
      id: '99',
      workspace: 'test',
      email: 'test@test.com',
      lastLogin: new Date(0),
      role: UserRole.user,
    };
  }

  function make_userWitheEmal(email: string): User {
    return { ...make_defaultUser(), email };
  }

  function make_userWithFirstName(firstName: string): User {
    return { ...make_defaultUser(), profile: { firstName, lastName: '' } };
  }

  function make_userWithLastName(lastName: string): User {
    return { ...make_defaultUser(), profile: { lastName, firstName: '' } };
  }

  describe('search filter', () => {
    it('should return all rulests if query is undefined', () => {
      const users = [
        make_defaultUser(),
        make_defaultUser(),
        make_defaultUser(),
      ];

      const result = users.filter(searchFilter());

      expect(result).toEqual(users);
    });

    it('should return single matched user by email', () => {
      const userWithEmail = make_userWitheEmal('test@testable.com');
      const users = [make_defaultUser(), userWithEmail, make_defaultUser()];

      const result = users.filter(searchFilter('testable'));

      expect(result).toEqual([userWithEmail]);
    });

    it('should return single matched user its first name', () => {
      const userWithFirstName = make_userWithFirstName('Peter');
      const users = [make_defaultUser(), userWithFirstName, make_defaultUser()];

      const result = users.filter(searchFilter('pet'));

      expect(result).toEqual([userWithFirstName]);
    });

    it('should return single matched user its last name', () => {
      const userWithLastName = make_userWithLastName('Example');
      const users = [make_defaultUser(), userWithLastName, make_defaultUser()];

      const result = users.filter(searchFilter('mple'));

      expect(result).toEqual([userWithLastName]);
    });

    it('should return two matched user their first or last name', () => {
      const userWithFirstName = make_userWithFirstName('James');
      const userWithLastName = make_userWithLastName('Jameson');
      const users = [userWithFirstName, userWithLastName, make_defaultUser()];

      const result = users.filter(searchFilter('ame'));

      expect(result).toEqual([userWithFirstName, userWithLastName]);
    });
  });
});
