/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region importrs
import * as R from 'ramda';
import { User } from '@reserve/client/lib/types/User';
import { UserRole } from '@reserve/client/lib/types/UserRole';
//#endregion

export function roleFilter(role: UserRole, user: User): boolean {
  return role === user.role;
}

export function bannedFilter(banned: string, user: User): boolean {
  return (
    (banned === 'true' && (user.banned ?? false)) ||
    (banned === 'false' && !(user.banned ?? false))
  );
}

export function searchFilter(searchQuery?: string): (user: User) => boolean {
  const userSearchableProperties = (user: User) =>
    R.map(R.applyTo(user), [
      R.path<string | undefined>(['email']),
      R.path<string | undefined>(['profile', 'firstName']),
      R.path<string | undefined>(['profile', 'lastName']),
    ]);

  const propertyMatches = R.pipe(
    R.defaultTo(''),
    R.toString,
    R.toLower,
    R.includes(searchQuery?.toLowerCase() ?? '')
  );

  return user => R.any(propertyMatches, userSearchableProperties(user));
}
