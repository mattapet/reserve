/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createSelector } from 'reselect';
import * as R from 'ramda';
import { RootState } from '@reserve/client/reducers';
//#endregion

export const announcementDataSelector = (state: RootState) =>
  state.workspaceAnnouncement.data;

export const announcementListSelector = createSelector(
  announcementDataSelector,
  R.values
);

export const announcementMetaSelector = (state: RootState) =>
  state.view.workspaceAnnouncement.fetch;

export const announcementCreateMeta = (state: RootState) =>
  state.view.workspaceAnnouncement.create;

export const activeAnnouncementSelector = (state: RootState) =>
  state.workspaceAnnouncement.active;
