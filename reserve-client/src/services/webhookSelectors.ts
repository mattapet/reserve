/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { RootState } from '../reducers';
import * as R from 'ramda';
import { createSelector } from 'reselect';
//#endregion

export const webhookDataSelector = (state: RootState) => state.webhook.data;

export const webhookListSelector = createSelector(
  webhookDataSelector,
  R.values
);

export const webhookMetaSelector = (state: RootState) =>
  state.view.webhook.fetch;

export const webhookTestDataSelector = (state: RootState) => state.webhook.test;

export const webhookTestMetaSelector = (state: RootState) =>
  state.view.webhook.test;
