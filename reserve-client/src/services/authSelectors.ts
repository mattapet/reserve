/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { RootState } from '@reserve/client/reducers';
import { createSelector } from 'reselect';
//#endregion

export const loginMetaSelector = (state: RootState) => state.view.login;

export const authMetaSelector = (state: RootState) => ({
  workspace: state.auth.workspace,
  isLoggedIn: state.auth.isLoggedIn,
});

export const userIdentitySelector = (state: RootState) => {
  if (!state.auth.isLoggedIn) {
    throw new Error('User is not logged in');
  }
  return state.auth.identity;
};

export const userRoleSelector = createSelector(
  userIdentitySelector,
  ({ role }) => role
);

export const refreshTokenSelector = (state: RootState) => {
  if (!state.auth.isLoggedIn) {
    throw new Error('User is not logged in');
  }
  return state.auth.tokens.refreshToken;
};
