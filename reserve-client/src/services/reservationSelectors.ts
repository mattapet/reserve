/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as R from 'ramda';
import { createSelector } from 'reselect';
import { RootState } from '@reserve/client/reducers';
//#endregion

export const reservationDataSelector = (state: RootState) =>
  state.reservation.data;

export const reservationListSelector = createSelector(
  reservationDataSelector,
  R.values
);

export const reservationMetaSelector = (state: RootState) =>
  state.view.reservation.fetch;

export const reservationCreateMetaSelector = (state: RootState) =>
  state.view.reservation.create;

export const reservationEditMetaSelector = (state: RootState) =>
  state.view.reservation.edit;

export const reservationCommentDataSelector = (state: RootState) =>
  state.reservationComments.data;

export const reservationCommentListelector = createSelector(
  reservationCommentDataSelector,
  R.values
);

export const reservationCommentsByReservationSelector = (
  reservationId: string
) =>
  createSelector(
    reservationCommentListelector,
    R.filter((comment) => comment.reservationId === reservationId)
  );

export const reservationCommentsMetaSelector = (state: RootState) =>
  state.view.reservationComments.fetch;

export const reservationCommentsEditMetaSelector = (state: RootState) =>
  state.view.reservationComments.edit;

export const reservationTemplateSelector = (state: RootState) =>
  state.template.data.template;

export const reservationTemplateMetaSelector = (state: RootState) =>
  state.view.template.fetch;
