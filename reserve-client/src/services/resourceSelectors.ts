/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as R from 'ramda';
import { createSelector } from 'reselect';
import { RootState } from '@reserve/client/reducers';
//#endregion

export const resourceDataSelector = (state: RootState) => state.resource.data;

export const resourceListSelector = createSelector(
  resourceDataSelector,
  R.values
);

export const resourceMetaSelector = (state: RootState) =>
  state.view.resource.fetch;
