/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createSelector } from 'reselect';
import * as R from 'ramda';
import { RootState } from '@reserve/client/reducers';
//#endregion

export const userDataSelector = (state: RootState) => state.user.data;

export const userListSelector = createSelector(userDataSelector, R.values);

export const userMetaSelector = (state: RootState) => state.view.user.fetch;

export const userEditSelector = (state: RootState) => state.view.user.edit;
