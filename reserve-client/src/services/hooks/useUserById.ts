/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useSelector } from 'react-redux';
import { User } from '@reserve/client/lib/types/User';
import { userActions } from '@reserve/client/actions';
import { useEntityProvider } from '@reserve/client/lib/hooks';
import { userMetaSelector, userDataSelector } from '../userSelectors';
//#endregion

export function useUserById(userId: string): User | undefined {
  const meta = useSelector(userMetaSelector);
  const users = useSelector(userDataSelector);
  const user: User = users[userId!];

  useEntityProvider(
    userId!,
    { ...meta, retrieved: user != null },
    userActions.fetchById.request
  );

  return user;
}
