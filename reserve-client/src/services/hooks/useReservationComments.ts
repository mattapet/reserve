/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useParams } from 'react-router';
import { useSelector } from 'react-redux';
import { useDataProvider } from '@reserve/client/lib/hooks/useDataProvider';
import {
  reservationCommentsByReservationSelector,
  reservationCommentsMetaSelector,
} from '@reserve/client/services/reservationSelectors';
import { reservationComments } from '../../actions';
import { ReservationComment } from '../../lib/types/ReservationComment';
import { FetchMeta } from '../../reducers/view/reservationComments/fetch';
//#endregion

interface Result {
  readonly comments: ReservationComment[];
  readonly meta: FetchMeta;
}
export function useReservationComments(): Result {
  const { id } = useParams();
  const comments = useSelector(reservationCommentsByReservationSelector(id!));
  const meta = useSelector(reservationCommentsMetaSelector);

  const action = () => reservationComments.fetch.request(id!);
  useDataProvider({ ...meta, retrieved: meta.retrieved[id!] ?? false }, action);

  return { comments, meta };
}
