/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { FetchMeta } from '../../reducers/view/resource/fetch';
import { Resource } from '../../lib/types/Resource';
import { useSelector } from 'react-redux';
import {
  resourceMetaSelector,
  resourceListSelector,
} from '../resourceSelectors';
import { resourceActions } from '../../actions';
import { useDataProvider } from '../../lib/hooks';
//#endregion

interface Result {
  readonly resources: Resource[];
  readonly meta: FetchMeta;
}

export function useResources(): Result {
  const meta = useSelector(resourceMetaSelector);
  const resources = useSelector(resourceListSelector);

  const { request, cancel } = resourceActions.fetch;
  useDataProvider(meta, request, cancel);

  return { meta, resources };
}
