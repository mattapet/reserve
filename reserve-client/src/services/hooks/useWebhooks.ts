/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useSelector, useDispatch } from 'react-redux';
import { Webhook } from '@reserve/client/lib/types/Webhook';
import { FetchMeta } from '@reserve/client/reducers/view/user/fetch';
import { useDataProvider } from '@reserve/client/lib/hooks';
import { webhookActions } from '@reserve/client/actions';
import { WebhookTest } from '@reserve/client/lib/types/WebhookTest';
import { TestMeta } from '@reserve/client/reducers/view/webhook/test';

import {
  webhookListSelector,
  webhookMetaSelector,
  webhookTestDataSelector,
  webhookTestMetaSelector,
} from '../webhookSelectors';
//#endregion

interface Result {
  readonly webhooks: Webhook[];
  readonly meta: FetchMeta;
}

export function useWebhooks(): Result {
  const webhooks = useSelector(webhookListSelector);
  const meta = useSelector(webhookMetaSelector);

  const { request, cancel } = webhookActions.fetch;
  useDataProvider(meta, request, cancel);

  return { webhooks, meta };
}

interface TestResult {
  readonly testResponse?: WebhookTest;
  readonly meta: TestMeta;
  readonly test: (webhookId: number, content: string) => any;
}
export function useTestWebhook(): TestResult {
  const dispatch = useDispatch();
  const testResponse = useSelector(webhookTestDataSelector);
  const meta = useSelector(webhookTestMetaSelector);

  function test(webhookId: number, content: string) {
    dispatch(webhookActions.test.request({ id: webhookId, content }));
  }

  return { testResponse, meta, test };
}
