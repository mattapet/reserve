/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useSelector } from 'react-redux';
import { WorkspaceAnnouncement } from '@reserve/client/lib/types/WorkspaceAnnouncement';
import { FetchMeta } from '@reserve/client/reducers/view/user/fetch';
import { useDataProvider } from '@reserve/client/lib/hooks';
import { workspaceActions } from '@reserve/client/actions';
import {
  announcementListSelector,
  announcementMetaSelector,
} from '../announcementSelectors';
//#endregion

interface Result {
  readonly announcements: WorkspaceAnnouncement[];
  readonly meta: FetchMeta;
}

export function useAnnouncements(): Result {
  const announcements = useSelector(announcementListSelector);
  const meta = useSelector(announcementMetaSelector);

  const { request, cancel } = workspaceActions.fetchAnnouncements;
  useDataProvider(meta, request, cancel);

  return { announcements, meta };
}
