/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useSelector } from 'react-redux';
import { FetchMeta } from '../../reducers/view/resource/fetch';
import { reservationActions } from '../../actions';
import { useDataProvider } from '../../lib/hooks';
import {
  reservationMetaSelector,
  reservationListSelector,
} from '../reservationSelectors';
import { Reservation } from '../../lib/types/Reservation';
//#endregion

interface Result {
  readonly reservations: Reservation[];
  readonly meta: FetchMeta;
}

export function useReservations(): Result {
  const meta = useSelector(reservationMetaSelector);
  const reservations = useSelector(reservationListSelector);

  const { request, cancel } = reservationActions.fetch;
  useDataProvider(meta, request, cancel);

  return { meta, reservations };
}
