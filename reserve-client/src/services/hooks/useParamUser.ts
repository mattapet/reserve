/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useParams } from 'react-router';
import { User } from '@reserve/client/lib/types/User';
import { useUserById } from './useUserById';
//#endregion

export function useParamUser(): User | undefined {
  const { id } = useParams();
  return useUserById(id!);
}
