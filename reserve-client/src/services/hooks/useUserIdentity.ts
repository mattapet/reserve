/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useSelector } from 'react-redux';
import { Identity } from '@reserve/client/reducers/auth';
import { userIdentitySelector } from '../authSelectors';
//#endregion

export function useUserIdentity(): Identity {
  return useSelector(userIdentitySelector);
}
