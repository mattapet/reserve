/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useParams } from 'react-router';
import { useSelector } from 'react-redux';
import { Reservation } from '@reserve/client/lib/types/Reservation';
import { reservationActions } from '@reserve/client/actions';
import { useEntityProvider } from '@reserve/client/lib/hooks';
import {
  reservationMetaSelector,
  reservationDataSelector,
} from '../reservationSelectors';
//#endregion

export function useParamReservation(): Reservation | undefined {
  const meta = useSelector(reservationMetaSelector);
  const reservations = useSelector(reservationDataSelector);
  const { id } = useParams();
  const reservation = reservations[id!];

  useEntityProvider(
    id!,
    { ...meta, retrieved: reservation != null },
    reservationActions.fetchById.request
  );

  return reservation;
}
