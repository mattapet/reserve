/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { useSelector } from 'react-redux';
import { User } from '@reserve/client/lib/types/User';
import { FetchMeta } from '@reserve/client/reducers/view/user/fetch';
import { useDataProvider } from '@reserve/client/lib/hooks';
import { userActions } from '@reserve/client/actions';
import { userListSelector, userMetaSelector } from '../userSelectors';
//#endregion

interface Result {
  readonly users: User[];
  readonly meta: FetchMeta;
}

export function useUsers(): Result {
  const users = useSelector(userListSelector);
  const meta = useSelector(userMetaSelector);

  const { request, cancel } = userActions.fetch;
  useDataProvider(meta, request, cancel);

  return { users, meta };
}
