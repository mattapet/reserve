/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import config from '@reserve/client/config';
import { combineEpics, Epic } from 'redux-observable';
import { merge, EMPTY } from 'rxjs';
import { filter, map, tap, flatMap } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';

import { RootAction } from '@reserve/client/actions';
import { get, del, post } from '@reserve/client/actions/request';
import * as actions from '@reserve/client/actions/workspace';
//#endregion

export const fetchByNameEpic: Epic<RootAction> = (action$) =>
  action$.pipe(
    filter(isActionOf(actions.fetchByName.request)),
    map(({ meta }) => get(null, meta))
  );

export const createAnnouncementsEpic: Epic<RootAction> = (action$) =>
  action$.pipe(
    filter(isActionOf(actions.createAnnouncement.request)),
    map(({ payload, meta }) => post(payload, meta))
  );

export const fetchAnnouncementsEpic: Epic<RootAction> = (action$) =>
  action$.pipe(
    filter(isActionOf(actions.fetchAnnouncements.request)),
    map(({ meta }) => get(null, meta))
  );

export const fetchAnnouncementEpic: Epic<RootAction> = (action$) =>
  action$.pipe(
    filter(isActionOf(actions.fetchAnnouncement.request)),
    map(({ meta }) => get(null, meta))
  );

export const toggleAnnouncementEpic: Epic<RootAction> = (action$) =>
  action$.pipe(
    filter(isActionOf(actions.toggleAnnouncement.request)),
    map(({ meta }) => post(null, meta))
  );

export const deleteAnnouncementEpic: Epic<RootAction> = (action$) =>
  action$.pipe(
    filter(isActionOf(actions.deleteAnnouncement.request)),
    map(({ meta }) => del(null, meta))
  );

export const transferOwnershipEpic: Epic<RootAction> = (action$) =>
  action$.pipe(
    filter(isActionOf(actions.transferOwnership.request)),
    map(({ payload, meta }) => post(payload, meta))
  );

export const removeEpic: Epic<RootAction> = (action$) =>
  merge(
    action$.pipe(
      filter(isActionOf(actions.remove.request)),
      map(({ meta }) => del(null, meta))
    ),
    action$.pipe(
      filter(isActionOf(actions.remove.success)),
      tap(() => (window.location.href = config.landingUrl)),
      flatMap(() => EMPTY)
    )
  );

export const workspaceEpic = combineEpics(
  fetchByNameEpic,
  removeEpic,
  createAnnouncementsEpic,
  fetchAnnouncementsEpic,
  fetchAnnouncementEpic,
  toggleAnnouncementEpic,
  transferOwnershipEpic,
  deleteAnnouncementEpic
);
