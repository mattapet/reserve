/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineEpics, Epic } from 'redux-observable';
import { filter, tap, ignoreElements } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import { message } from 'antd';

import { notificationActions, RootAction } from '@reserve/client/actions';
//#endregion

export const success: Epic<RootAction> = action$ =>
  action$.pipe(
    filter(isActionOf(notificationActions.success)),
    tap(({ payload }) => message.success(payload)),
    ignoreElements()
  );

export const info: Epic<RootAction> = action$ =>
  action$.pipe(
    filter(isActionOf(notificationActions.info)),
    tap(({ payload }) => message.info(payload)),
    ignoreElements()
  );

export const warning: Epic<RootAction> = action$ =>
  action$.pipe(
    filter(isActionOf(notificationActions.warning)),
    tap(({ payload }) => message.warning(payload)),
    ignoreElements()
  );

export const error: Epic<RootAction> = action$ =>
  action$.pipe(
    filter(isActionOf(notificationActions.error)),
    tap(({ payload }) => message.error(payload)),
    ignoreElements()
  );

export const notificationEpic = combineEpics(success, info, warning, error);
