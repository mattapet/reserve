/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineEpics } from 'redux-observable';
import { authEpic } from './auth';
import { requestEpic } from './request';
import { resourceEpic } from './resource';
import { reservationEpic } from './reservation';
import { restrictionEpic } from './restriction';
import { userEpic } from './user';
import { webhookEpic } from './webhook';
import { workspaceEpic } from './workspace';
import { reservationCommentEpic } from './reservationComment';
import { notificationEpic } from './notifications';
import { templateEpic } from './template';
//#endregion

export const rootEpic = combineEpics(
  authEpic,
  requestEpic,
  resourceEpic,
  reservationEpic,
  restrictionEpic,
  userEpic,
  webhookEpic,
  workspaceEpic,
  reservationCommentEpic,
  templateEpic,
  notificationEpic
);
