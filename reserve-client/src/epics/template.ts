/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineEpics, Epic } from 'redux-observable';
import { filter, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';

import { RootAction } from '@reserve/client/actions';
import { get, put, del } from '@reserve/client/actions/request';
import * as actions from '@reserve/client/actions/template';
//#endregion

export const fetchEpic: Epic<RootAction> = (action$) =>
  action$.pipe(
    filter(isActionOf(actions.fetch.request)),
    map(({ meta }) => get(null, meta))
  );

export const setEpic: Epic<RootAction> = (action$) =>
  action$.pipe(
    filter(isActionOf(actions.set.request)),
    map(({ payload, meta }) => put(payload, meta))
  );

export const deleteEpic: Epic<RootAction> = (action$) =>
  action$.pipe(
    filter(isActionOf(actions.delete.request)),
    map(({ meta }) => del(null, meta))
  );

export const templateEpic = combineEpics(fetchEpic, setEpic, deleteEpic);
