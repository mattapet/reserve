/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';
import {
  reservationActions,
  resourceActions,
  restrictionActions,
  userActions,
  webhookActions,
} from '../actions';
import collectionProvider from './collectionProvider';
import entityProvider from './entityProvider';
import createReduxProvider from './createReduxProvider';
import { RootState } from '../reducers';
//#endregion

export const reservationCollectionProvider = createReduxProvider(
  (state: RootState) => ({
    data: state.reservation.data,
    meta: state.view.reservation.fetch,
  }),
  (dispatch: Dispatch) => ({
    onFetch: () => dispatch(reservationActions.fetch.request()),
  }),
  collectionProvider
);
export const resourceCollectionProvider = createReduxProvider(
  (state: RootState) => ({
    data: state.resource.data,
    meta: state.view.resource.fetch,
  }),
  (dispatch: Dispatch) => ({
    onFetch: () => dispatch(resourceActions.fetch.request()),
  }),
  collectionProvider
);
export const restrictionCollectionProvider = createReduxProvider(
  (state: RootState) => ({
    data: state.restriction.data,
    meta: state.view.restriction.fetch,
  }),
  (dispatch: Dispatch) => ({
    onFetch: () => dispatch(restrictionActions.fetch.request()),
  }),
  collectionProvider
);
export const userCollectionProvider = createReduxProvider(
  (state: RootState) => ({
    data: state.user.data,
    meta: {
      ...state.view.user.fetch,
      pending: state.view.user.edit.pending || state.view.user.fetch.pending,
    },
  }),
  (dispatch: Dispatch) => ({
    onFetch: () => dispatch(userActions.fetch.request()),
  }),
  collectionProvider
);
export const webhookCollectionProvider = createReduxProvider(
  (state: RootState) => ({
    data: state.webhook.data,
    meta: {
      ...state.view.webhook.fetch,
      pending:
        state.view.webhook.edit.pending || state.view.webhook.fetch.pending,
    },
  }),
  (dispatch: Dispatch) => ({
    onFetch: () => dispatch(webhookActions.fetch.request()),
  }),
  collectionProvider
);

export const reservationEntityProvider = createReduxProvider(
  (state: RootState) => ({ entities: state.reservation.data }),
  (dispatch: Dispatch) => ({
    onFetch: (id: string) => dispatch(reservationActions.fetchById.request(id)),
  }),
  entityProvider as any
);
export const resourceEntityProvider = createReduxProvider(
  (state: RootState) => ({ entities: state.resource.data }),
  (dispatch: Dispatch) => ({
    onFetch: (id: number) => dispatch(resourceActions.fetchById.request(id)),
  }),
  entityProvider as any
);
export const restrictionEntityProvider = createReduxProvider(
  (state: RootState) => ({ entities: state.restriction.data }),
  (dispatch: Dispatch) => ({
    onFetch: (id: number) => dispatch(restrictionActions.fetchById.request(id)),
  }),
  entityProvider as any
);
export const userEntityProvider = createReduxProvider(
  (state: RootState) => ({ entities: state.user.data }),
  (dispatch: Dispatch) => ({
    onFetch: (id: string) => dispatch(userActions.fetchById.request(id)),
  }),
  entityProvider as any
);
export const webhookEntityProvider = createReduxProvider(
  (state: RootState) => ({ entities: state.webhook.data }),
  (dispatch: Dispatch) => ({
    onFetch: (id: number) => dispatch(webhookActions.fetchById.request(id)),
  }),
  entityProvider as any
);

export { default as idProvider } from './idProvider';
