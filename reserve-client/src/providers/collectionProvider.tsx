/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect } from 'react';
//#endregion

interface MappedState<Entity> {
  readonly data: { readonly [id: string]: Entity };
  readonly meta: {
    readonly retrieved: boolean;
    readonly pending: boolean;
    readonly error?: Error;
  };
}

interface MappedDispatch {
  readonly onFetch: () => any;
}

export interface Props<Entity> extends MappedState<Entity>, MappedDispatch {}

export interface WrappedProps<Entity> extends MappedState<Entity> {}

export default function collectionProvider<
  Entity extends {},
  P extends MappedState<Entity>
>(Component: React.ComponentType<P>) {
  const Provider: React.FunctionComponent<P & MappedDispatch> = props => {
    const { onFetch, ...wrappedProps } = props;
    const {
      meta: { retrieved, pending },
    } = wrappedProps;

    useEffect(() => {
      !retrieved && !pending && onFetch();
    }, [retrieved, pending, onFetch]);

    return <Component {...(wrappedProps as P)} />;
  };

  return Provider;
}
