/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect } from 'react';
//#endregion

interface MappedState<Entity> {
  readonly entities: { readonly [id: number]: Entity };
}

interface MappedDispatch {
  readonly onFetch: (id: number | string) => any;
}

export interface Props<Entity> extends MappedState<Entity>, MappedDispatch {}

export interface WrappedProps<Entity> {
  readonly id: number | string;
  readonly entity?: Entity;
}

export default function provider<
  Entity extends {},
  P extends WrappedProps<Entity>
>(Component: React.ComponentType<P>) {
  const Provider: React.FunctionComponent<P & Props<Entity>> = props => {
    const { id, onFetch, entities, ...wrappedProps } = props;
    const entity = entities[this.props.id as number];

    useEffect(() => {
      entity && onFetch(id);
    }, [id, entity, onFetch]);

    return <Component {...(wrappedProps as P)} entity={entity} />;
  };

  return Provider;
}
