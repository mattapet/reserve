/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { RootState } from '../reducers';
//#endregion

export default function createReduxProvider<
  MappedState extends {},
  MappedDispatch extends {},
  ProviderProps extends {},
  Props extends {},
  Provider extends (
    Component: React.ComponentType<
      MappedState & MappedDispatch & ProviderProps & Props
    >
  ) => React.ComponentType<MappedState & MappedDispatch & Props>
>(
  mapStateToProps: (state: RootState) => MappedState,
  mapDispatchToProps: (dispatch: Dispatch) => MappedDispatch,
  provider: Provider
) {
  type ComponentProps = MappedState & MappedDispatch & ProviderProps & Props;
  return (Component: React.ComponentType<ComponentProps>) =>
    connect<MappedState, MappedDispatch, {}, RootState>(
      mapStateToProps,
      mapDispatchToProps
    )(provider(Component) as any) as React.ComponentType<Props>;
}
