/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '@reserve/client/actions';
import * as actions from '@reserve/client/actions/reservationComments';
import { ReservationComment } from '@reserve/client/lib/types/ReservationComment';
//#endregion

export interface ReservationCommentState {
  readonly data: {
    readonly [commentId: string]: ReservationComment;
  };
}

const initialState: ReservationCommentState = {
  data: {},
};

export function reservationComments(
  state = initialState,
  action: RootAction
): ReservationCommentState {
  switch (action.type) {
    case getType(actions.fetch.success):
      return {
        data: action.payload.comments.reduce(
          (data, res) => ({ ...data, [res.id]: res }),
          {}
        ),
      };

    case getType(actions.create.success):
    case getType(actions.edit.success):
    case getType(actions.remove.success):
      return {
        data: { ...state.data, [action.payload.id]: action.payload },
      };

    default:
      return state;
  }
}
