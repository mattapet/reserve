/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../actions';
import * as actions from '../actions/workspace';
import { WorkspaceAnnouncement } from '../lib/types/WorkspaceAnnouncement';
//#endregion

export interface WorkspaceAnnouncementState {
  readonly data: { [name: string]: WorkspaceAnnouncement };
  readonly active: WorkspaceAnnouncement | null;
}

const initialState: WorkspaceAnnouncementState = {
  data: {},
  active: null,
};

export function workspaceAnnouncement(
  state = initialState,
  action: RootAction
): WorkspaceAnnouncementState {
  switch (action.type) {
    case getType(actions.fetchAnnouncements.success):
      return {
        ...state,
        data: action.payload.reduce(
          (acc, next) => ({ ...acc, [next.name]: next }),
          {}
        ),
      };

    case getType(actions.fetchAnnouncement.success):
      return {
        ...state,
        active: action.payload ?? null,
      };

    case getType(actions.toggleAnnouncement.success): {
      const deactivated = state.active
        ? { [state.active.name]: { ...state.active, active: false } }
        : {};

      return {
        data: {
          ...state.data,
          ...deactivated,
          [action.payload.name]: action.payload,
        },
        active: action.payload.active ? action.payload : null,
      };
    }

    case getType(actions.createAnnouncement.success):
      return {
        ...state,
        data: { ...state.data, [action.payload.name]: action.payload },
      };

    case getType(actions.deleteAnnouncement.success): {
      const { [action.payload]: remove, ...rest } = state.data;

      return { ...state, data: rest };
    }

    default:
      return state;
  }
}
