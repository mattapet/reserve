/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '@reserve/client/actions';
import * as actions from '@reserve/client/actions/webhook';
import { Webhook } from '@reserve/client/lib/types/Webhook';
import { WebhookTest } from '@reserve/client/lib/types/WebhookTest';
//#endregion

export interface WebhookState {
  readonly data: {
    readonly [id: number]: Webhook;
  };
  readonly test?: WebhookTest;
}

const initialState: WebhookState = {
  data: {},
};

export function webhook(
  state = initialState,
  action: RootAction
): WebhookState {
  switch (action.type) {
    case getType(actions.fetch.success):
      return {
        data: action.payload.reduce(
          (data, res) => ({ ...data, [res.id]: res }),
          {}
        ),
      };

    case getType(actions.fetchById.success):
    case getType(actions.create.success):
    case getType(actions.edit.success):
    case getType(actions.toggleActive.success):
      return {
        data: { ...state.data, [action.payload.id]: action.payload },
      };

    case getType(actions.remove.success):
      const { [action.payload]: removed, ...rest } = state.data;
      return { data: rest };

    case getType(actions.test.success):
      return { ...state, test: action.payload };

    case getType(actions.test.reset): {
      const { data } = state;
      return { data };
    }

    default:
      return state;
  }
}
