/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../../actions';
import * as authActions from '../../../actions/auth';
//#endregion

export interface LoginMeta {
  readonly error?: Error;
  readonly retrieved: boolean;
  readonly pending: boolean;
}

const initialState: LoginMeta = {
  pending: false,
  retrieved: false,
};

export function login(state = initialState, action: RootAction): LoginMeta {
  switch (action.type) {
  case getType(authActions.login.request):
    return {
      pending: true,
      retrieved: false,
    };

  case getType(authActions.login.success):
    return {
      pending: false,
      retrieved: true,
    };

  case getType(authActions.login.cancel):
    return {
      pending: false,
      retrieved: false,
    };

  case getType(authActions.login.fail):
    return {
      error: action.payload,
      pending: false,
      retrieved: true,
    };
  
  default:
    return state;
  }
}
