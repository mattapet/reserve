/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineReducers } from 'redux';
import { auth, AuthMeta } from './auth';
import { login, LoginMeta } from './login';
import { register, RegisterMeta } from './register';
import { resource, ResourceMeta } from './resource';
import { reservation, ReservationMeta } from './reservation';
import { restriction, RestrictionMeta } from './restriction';
import { user, UserMeta } from './user';
import { webhook, WebhookMeta } from './webhook';
import {
  reservationComments,
  ReservationCommentsMeta,
} from './reservationComments';
import {
  WorkspaceAnnouncementMeta,
  workspaceAnnouncement,
} from './workspaceAnnouncement';
import { WorkspaceMeta, workspace } from './workspace';
import { template, TemplateMeta } from './template';
//#endregion

export interface ViewState {
  readonly auth: AuthMeta;
  readonly login: LoginMeta;
  readonly register: RegisterMeta;
  readonly resource: ResourceMeta;
  readonly reservation: ReservationMeta;
  readonly restriction: RestrictionMeta;
  readonly user: UserMeta;
  readonly webhook: WebhookMeta;
  readonly reservationComments: ReservationCommentsMeta;
  readonly workspaceAnnouncement: WorkspaceAnnouncementMeta;
  readonly workspace: WorkspaceMeta;
  readonly template: TemplateMeta;
}

export const view = combineReducers({
  auth,
  login,
  register,
  resource,
  reservation,
  restriction,
  user,
  webhook,
  reservationComments,
  workspaceAnnouncement,
  workspace,
  template,
});
