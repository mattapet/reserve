/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '@reserve/client/actions';
import * as actions from '@reserve/client/actions/webhook';
//#endregion

export interface TestMeta {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: TestMeta = {
  pending: false,
};

export function test(state = initialState, action: RootAction): TestMeta {
  switch (action.type) {
    case getType(actions.test.request):
      return {
        ...state,
        pending: true,
      };

    case getType(actions.test.success):
      return {
        pending: false,
      };

    case getType(actions.test.cancel):
      return {
        pending: false,
      };

    case getType(actions.test.fail):
      return {
        error: action.payload,
        pending: false,
      };

    default:
      return state;
  }
}
