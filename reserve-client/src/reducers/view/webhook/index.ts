/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineReducers } from 'redux';
import { create, CreateMeta } from './create';
import { fetch, FetchMeta } from './fetch';
import { edit, EditMeta } from './edit';
import { remove, RemoveMeta } from './remove';
import { test, TestMeta } from './test';
//#endregion

export interface WebhookMeta {
  readonly create: CreateMeta;
  readonly fetch: FetchMeta;
  readonly edit: EditMeta;
  readonly remove: RemoveMeta;
  readonly test: TestMeta;
}

export const webhook = combineReducers({ create, fetch, edit, remove, test });
