/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '@reserve/client/actions';
import * as actions from '@reserve/client/actions/reservationComments';
//#endregion

export interface EditMeta {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: EditMeta = {
  pending: false,
};

export function edit(state = initialState, action: RootAction): EditMeta {
  switch (action.type) {
    case getType(actions.edit.request):
    case getType(actions.remove.request):
      return {
        ...state,
        pending: true,
      };

    case getType(actions.edit.success):
    case getType(actions.remove.success):
      return {
        pending: false,
      };

    case getType(actions.edit.cancel):
    case getType(actions.remove.cancel):
      return {
        pending: false,
      };

    case getType(actions.edit.fail):
    case getType(actions.remove.fail):
      return {
        error: action.payload,
        pending: false,
      };

    default:
      return state;
  }
}
