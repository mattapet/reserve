/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '@reserve/client/actions';
import * as actions from '@reserve/client/actions/reservationComments';
//#endregion

export interface FetchMeta {
  readonly error?: Error;
  readonly retrieved: {
    readonly [reservationId: string]: boolean;
  };
  readonly pending: boolean;
}

const initialState: FetchMeta = {
  pending: false,
  retrieved: {},
};

export function fetch(state = initialState, action: RootAction): FetchMeta {
  switch (action.type) {
    case getType(actions.fetch.request):
      return {
        ...state,
        pending: true,
      };

    case getType(actions.fetch.success):
      return {
        pending: false,
        retrieved: {
          ...state.retrieved,
          [action.payload.reservationId]: true,
        },
      };

    case getType(actions.fetch.cancel):
      return {
        ...state,
        pending: false,
      };

    case getType(actions.fetch.fail):
      return {
        ...state,
        error: action.payload,
        pending: false,
      };

    default:
      return state;
  }
}
