/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineReducers } from 'redux';
import { fetch, FetchMeta } from './fetch';
import { create, CreateMeta } from './create';
//#endregion

export interface WorkspaceAnnouncementMeta {
  readonly fetch: FetchMeta;
  readonly create: CreateMeta;
}

export const workspaceAnnouncement = combineReducers({ fetch, create });
