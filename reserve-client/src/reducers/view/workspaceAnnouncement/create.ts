/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../../actions';
import * as actions from '../../../actions/workspace';
//#endregion

export interface CreateMeta {
  readonly error?: Error;
  readonly retrieved: boolean;
  readonly pending: boolean;
}

const initialState: CreateMeta = {
  pending: false,
  retrieved: false,
};

export function create(state = initialState, action: RootAction): CreateMeta {
  switch (action.type) {
    case getType(actions.createAnnouncement.request):
      return {
        ...state,
        pending: true,
      };

    case getType(actions.createAnnouncement.success):
      return {
        pending: false,
        retrieved: true,
      };

    case getType(actions.createAnnouncement.cancel):
      return {
        ...state,
        pending: false,
      };

    case getType(actions.createAnnouncement.fail):
      return {
        error: action.payload,
        pending: false,
        retrieved: true,
      };

    default:
      return state;
  }
}
