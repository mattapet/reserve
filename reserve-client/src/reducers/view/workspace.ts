/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '@reserve/client/actions';
import * as actions from '@reserve/client/actions/workspace';
//#endregion

export interface WorkspaceMeta {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: WorkspaceMeta = {
  pending: false,
};

export function workspace(
  state = initialState,
  action: RootAction
): WorkspaceMeta {
  switch (action.type) {
    case getType(actions.transferOwnership.request):
      return {
        ...state,
        pending: true,
      };

    case getType(actions.transferOwnership.success):
      return {
        pending: false,
      };

    case getType(actions.transferOwnership.cancel):
      return {
        pending: false,
      };

    case getType(actions.transferOwnership.fail):
      return {
        error: action.payload,
        pending: false,
      };

    default:
      return state;
  }
}
