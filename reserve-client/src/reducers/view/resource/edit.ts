/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../../actions';
import * as resourceActions from '../../../actions/resources';
//#endregion

export interface EditMeta {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: EditMeta = {
  pending: false,
};

export function edit(
  state = initialState,
  action: RootAction,
): EditMeta {
  switch (action.type) {
  case getType(resourceActions.edit.request):
  case getType(resourceActions.toggle.request):
    return {
      ...state,
      pending: true,
    };

  case getType(resourceActions.edit.success):
  case getType(resourceActions.toggle.success):
    return {
      pending: false,
    };

  case getType(resourceActions.edit.cancel):
  case getType(resourceActions.toggle.cancel):
    return {
      pending: false,
    };

  case getType(resourceActions.edit.fail):
  case getType(resourceActions.toggle.fail):
    return {
      error: action.payload,
      pending: false,
    };

  default:
    return state;
  }
}
