/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineReducers } from 'redux';
import { fetch, FetchMeta } from './fetch';
import { set, SetMeta } from './set';
import { del, DeleteMeta } from './delete';
//#endregion

export interface TemplateMeta {
  readonly fetch: FetchMeta;
  readonly set: SetMeta;
  readonly delete: DeleteMeta;
}

export const template = combineReducers({
  fetch,
  set,
  delete: del,
});
