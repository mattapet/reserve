/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '@reserve/client/actions';
import * as actions from '@reserve/client/actions/template';
//#endregion

export interface DeleteMeta {
  readonly error?: Error;
  readonly retrieved: boolean;
  readonly pending: boolean;
}

const initialState: DeleteMeta = {
  pending: false,
  retrieved: false,
};

export function del(state = initialState, action: RootAction): DeleteMeta {
  switch (action.type) {
    case getType(actions.delete.request):
      return {
        ...state,
        pending: true,
      };

    case getType(actions.delete.success):
      return {
        pending: false,
        retrieved: true,
      };

    case getType(actions.delete.cancel):
      return {
        ...state,
        pending: false,
      };

    case getType(actions.delete.fail):
      return {
        error: action.payload,
        pending: false,
        retrieved: true,
      };

    default:
      return state;
  }
}
