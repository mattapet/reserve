/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '@reserve/client/actions';
import * as actions from '@reserve/client/actions/template';
import { ReservationTemplate } from '@reserve/client/lib/types/ReservationTemplate';
//#endregion

export interface ReservationTemplateState {
  readonly data: ReservationTemplate;
}

const initialState: ReservationTemplateState = {
  data: {},
};

export function template(
  state = initialState,
  action: RootAction
): ReservationTemplateState {
  switch (action.type) {
    case getType(actions.fetch.success):
    case getType(actions.set.success):
      return { data: action.payload };

    case getType(actions.delete.success):
      return initialState;

    default:
      return state;
  }
}
