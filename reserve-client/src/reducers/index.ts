/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineReducers } from 'redux';
import { getType } from 'typesafe-actions';
import {
  authActions,
  RootAction,
  workspaceActions,
} from '@reserve/client/actions';

import { auth, AuthState } from './auth';
import { resource, ResourceState } from './resource';
import { reservation, ReservationState } from './reservation';
import { restriction, RestrictionState } from './restriction';
import { user, UserState } from './user';
import { webhook, WebhookState } from './webhook';
import { workspace, WorkspaceState } from './workspace';
import {
  reservationComments,
  ReservationCommentState,
} from './reservationComments';
import {
  workspaceAnnouncement,
  WorkspaceAnnouncementState,
} from './workspace-announcements';
import { template, ReservationTemplateState } from './template';
import { view, ViewState } from './view';
//#endregion

export interface RootState {
  readonly auth: AuthState;
  readonly resource: ResourceState;
  readonly reservation: ReservationState;
  readonly restriction: RestrictionState;
  readonly user: UserState;
  readonly webhook: WebhookState;
  readonly workspace: WorkspaceState;
  readonly reservationComments: ReservationCommentState;
  readonly workspaceAnnouncement: WorkspaceAnnouncementState;
  readonly template: ReservationTemplateState;
  readonly view: ViewState;
}

const appReducer = combineReducers<RootState>({
  auth,
  resource,
  reservation,
  restriction,
  user,
  webhook,
  workspace,
  reservationComments,
  workspaceAnnouncement,
  template,
  view,
});

export const rootReducer = (
  state: RootState | undefined,
  action: RootAction
) => {
  if (
    (getType(authActions.logout.success) === action.type ||
      getType(workspaceActions.transferOwnership.success) === action.type) &&
    state
  ) {
    const { workspace, auth } = state;
    return appReducer({ workspace, auth } as RootState, action);
  }
  return appReducer(state, action);
};
