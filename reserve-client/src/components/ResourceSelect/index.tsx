/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import { Select } from 'antd';

import { resourceCollectionProvider } from '@reserve/client/providers';
import { Props as CollectionProviderProps } from '@reserve/client/providers/collectionProvider';
import { Resource } from '@reserve/client/lib/types/Resource';
//#endregion

//#region Component interfaces
export interface Props extends CollectionProviderProps<Resource> {
  readonly value: number[];
  readonly onChange: (ids: number[]) => any;
}
//#endregion

//#region Styled
const StyledSelect = styled(Select as any)`
  width: 100%;
`;
//#endregion

const ResourceSelect: React.FunctionComponent<Props> = props => (
  <StyledSelect
    mode="multiple"
    value={props.value}
    onChange={props.onChange}
    loading={props.meta.pending || !props.meta.retrieved}
  >
    {Object.values(props.data).map((resource: Resource) => (
      <Select.Option key={`${resource.id}`} value={resource.id}>
        {resource.name}
      </Select.Option>
    ))}
  </StyledSelect>
);

export default resourceCollectionProvider(ResourceSelect as any);
