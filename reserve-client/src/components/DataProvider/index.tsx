/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import Spinner from '../Spinner';
//#endregion

export interface Props {
  readonly onFetch: () => any;
  readonly retrieved: boolean;
  readonly pending: boolean;
  readonly children: React.ReactNode;
}

const DataProvider: React.FunctionComponent<Props> = props => {
  const { pending, retrieved, children, onFetch } = props;

  if (!pending && !retrieved) {
    onFetch();
  }

  if (retrieved) {
    return <div>{children}</div>;
  } else {
    return <Spinner active />;
  }
};

export default DataProvider;
