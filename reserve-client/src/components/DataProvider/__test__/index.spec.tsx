/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { shallow } from 'enzyme';
import Spinner from '../../Spinner';
import DataProvider, { Props } from '../index';
//#endregion

const defaultProps: Props = {
  onFetch: jest.fn(),
  retrieved: false,
  pending: false,
  children: null,
};

describe('DataProvider', () => {
  describe('not retrieved data', () => {
    it('should render active Spinner', () => {
      const wrapper = shallow(<DataProvider {...defaultProps} />);

      expect(wrapper.find(Spinner).prop('active')).toBe(true);
    });

    it('should fetch the data if not retrieved', () => {
      const onFetch = jest.fn();

      shallow(<DataProvider {...defaultProps} onFetch={onFetch} />);

      expect(onFetch).toHaveBeenCalled();
    });

    it('should not call onFetch if pending', () => {
      const onFetch = jest.fn();

      shallow(<DataProvider {...defaultProps} onFetch={onFetch} pending />);

      expect(onFetch).not.toHaveBeenCalled();
    });
  });

  describe('retrieved data', () => {
    it('should set Spinner to inactive', () => {
      const wrapper = shallow(<DataProvider {...defaultProps} retrieved />);

      expect(wrapper.find(Spinner).exists()).toBe(false);
    });

    it('should not call onFetch', () => {
      const onFetch = jest.fn();

      shallow(<DataProvider {...defaultProps} onFetch={onFetch} retrieved />);

      expect(onFetch).not.toHaveBeenCalled();
    });

    it('should render nested component', () => {
      const Component = () => <div className="nested-component" />;

      const wrapper = shallow(
        <DataProvider {...defaultProps} retrieved>
          <Component />
        </DataProvider>
      );

      expect(wrapper.find(Component).exists()).toBe(true);
    });
  });
});
