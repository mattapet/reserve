/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import ANTDTextArea, { TextAreaProps } from 'antd/lib/input/TextArea';
//#endregion

export interface Props extends Omit<TextAreaProps, 'onChange'> {
  readonly onChange?: (value: string) => any;
}

const TextArea: React.FunctionComponent<Props> = ({
  onChange,
  ...props
}: Props) => (
  <ANTDTextArea
    {...props}
    onChange={e => onChange?.(e.target.value)}
  />
);

export default TextArea;
