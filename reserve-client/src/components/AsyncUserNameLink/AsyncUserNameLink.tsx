/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { User } from '@reserve/client/lib/types/User';

import UserNameLink from '../UserNameLink';
//#endregion

export interface Props {
  readonly users: { [id: string]: User };
  readonly onFetch: (id: string) => any;
  readonly pending?: boolean;
  readonly userId: string;
}

const AsyncUserNameLink: React.FunctionComponent<Props> = props => {
  const { users, userId, onFetch, pending } = props;
  const user = users[userId];

  if (!user && !pending) {
    onFetch(userId!);
  }

  if (user) {
    return <UserNameLink user={user} />;
  } else {
    return <i>Loading...</i>;
  }
};

export default AsyncUserNameLink;
