/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { shallow } from 'enzyme';
import { User } from '@reserve/client/lib/types/User';
import { UserRole } from '@reserve/client/lib/types/UserRole';

import UserNameLink from '../../UserNameLink';
import AsyncUserNameLink, { Props } from '../AsyncUserNameLink';
//#endregion

function make_user(): User {
  return {
    id: '12345',
    workspace: 'test',
    role: UserRole.admin,
    lastLogin: new Date(0),
    profile: {
      firstName: 'Test',
      lastName: 'Testovic',
    },
  };
}

const defaultProps: Props = {
  pending: false,
  onFetch: jest.fn(),
  userId: '12345',
  users: {},
};

describe('Async User name link', () => {
  describe('component without data', () => {
    it('should render "Loading..." text', () => {
      const wrapper = shallow(<AsyncUserNameLink {...defaultProps} />);

      expect(wrapper.find('i').text()).toBe('Loading...');
    });

    it("should call onFetch with user's id", () => {
      const onFetch = jest.fn();

      shallow(<AsyncUserNameLink {...defaultProps} onFetch={onFetch} />);

      expect(onFetch).toHaveBeenCalledWith('12345');
    });

    it('should not call onFetch if pending', () => {
      const onFetch = jest.fn();

      shallow(
        <AsyncUserNameLink {...defaultProps} onFetch={onFetch} pending />
      );

      expect(onFetch).not.toHaveBeenCalled();
    });
  });

  describe('component with fetched data', () => {
    it('shoudl render UserNameLink', () => {
      const user = make_user();

      const wrapper = shallow(
        <AsyncUserNameLink {...defaultProps} users={{ 12345: user }} />
      );

      expect(wrapper.find(UserNameLink).exists()).toBe(true);
    });

    it('shoudl should not call onFetch', () => {
      const user = make_user();
      const onFetch = jest.fn();

      shallow(
        <AsyncUserNameLink
          {...defaultProps}
          onFetch={onFetch}
          users={{ 12345: user }}
        />
      );

      expect(onFetch).not.toHaveBeenCalled();
    });
  });
});
