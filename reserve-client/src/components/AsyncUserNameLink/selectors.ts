/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';
import { RootState } from '@reserve/client/reducers';
import { userActions } from '@reserve/client/actions';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  users: state.user.data,
  pending: state.view.user.fetch.pending,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onFetch: (id: string) => dispatch(userActions.fetchById.request(id)),
});
