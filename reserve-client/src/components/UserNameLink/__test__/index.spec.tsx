/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { shallow } from 'enzyme';
import { User } from '@reserve/client/lib/types/User';
import { UserRole } from '@reserve/client/lib/types/UserRole';
import { Link } from 'react-router-dom';

import UserNameLink from '../index';
//#endregion

function make_user(): User {
  return {
    id: '12345',
    workspace: 'test',
    role: UserRole.admin,
    lastLogin: new Date(0),
    profile: {
      firstName: 'Test',
      lastName: 'Testovic',
    },
  };
}

describe('User name link', () => {
  it("should render a Link to user's details", () => {
    const user = make_user();

    const wrapper = shallow(<UserNameLink user={user} />);

    expect(wrapper.find(Link).prop('to')).toBe('/users/12345/details');
  });

  it("should render user's name", () => {
    const user = make_user();

    const wrapper = shallow(<UserNameLink user={user} />);

    expect(wrapper.find(Link).prop('children')).toBe('Test Testovic');
  });
});
