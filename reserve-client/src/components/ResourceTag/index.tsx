/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Tag } from 'antd';
import { Link } from 'react-router-dom';

import { Resource } from '@reserve/client/lib/types/Resource';
//#endregion

export interface Props {
  readonly id: number;
  readonly entity?: Resource;
}

const ResourceTag: React.FunctionComponent<Props> = props => (
  <Link to={`/resources/${props.id}/details`}>
    <Tag>{props.entity ? props.entity.name : `${props.id}`}</Tag>
  </Link>
);

export default ResourceTag;
