/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Meta } from '@reserve/client/lib/types/Meta';
//#endregion

export interface Props<Entity extends {}, EntityMeta extends Meta> {
  readonly data: { [resourceId: number]: Entity };
  readonly meta: EntityMeta;
  readonly onFetch: () => any;
  readonly onFetchCancel?: () => any;
}

class DataContainer<
  Entity extends {},
  EntityMeta extends Meta
> extends React.Component<Props<Entity, EntityMeta>> {
  public componentDidMount() {
    this.props.onFetch();
  }

  public componentWillUnmount() {
    this.props.onFetchCancel?.();
  }

  public render() {
    const { children } = this.props;
    return React.cloneElement(React.Children.only(children as any), {
      data: Object.values(this.props.data),
      pending: this.props.meta.pending || !this.props.meta.retrieved,
    });
  }
}

export default DataContainer;
