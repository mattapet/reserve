/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Layout } from 'antd';
import { Meta } from '@reserve/client/lib/types/Meta';
import Spinner from '@reserve/client/components/Spinner';
//#endregion

export interface Props<Entity extends {}> {
  readonly title: string;
  readonly meta: Meta;
  readonly data: { readonly [id: string]: Entity };
  readonly fetchById: (id: string) => any;
  readonly idExtractor: () => number;
  readonly onComplete: () => any;
}

class DataEdit<Entity extends {}> extends React.Component<Props<Entity>> {
  private get entity(): Entity | undefined {
    const id = `${this.props.idExtractor()}`;
    return this.props.data[id];
  }

  public componentDidMount() {
    this.ensureEntity();
  }

  public componentDidUpdate(prevProps: Props<Entity>) {
    if (prevProps.meta.pending && !this.props.meta.pending) {
      return this.props.onComplete();
    }
    this.ensureEntity();
  }

  public render() {
    return (
      <Layout>
        <Layout.Header>
          <h1>{this.props.title}</h1>
        </Layout.Header>
        <Layout.Content>
          {this.entity ? (
            React.cloneElement(
              React.Children.only(this.props.children as any),
              {
                defaultValue: this.entity,
              }
            )
          ) : (
            <Spinner />
          )}
        </Layout.Content>
      </Layout>
    );
  }

  private ensureEntity() {
    if (!this.entity) {
      this.props.fetchById(`${this.props.idExtractor()}`);
    }
  }
}

export default DataEdit;
