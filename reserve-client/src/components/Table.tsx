/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import { Button } from 'antd';
import ANTDTable, { TableProps } from 'antd/lib/table';
//#endregion

const Container = styled.div`
  height: 100%;
`;

export interface Props<Entity extends {}> extends TableProps<Entity> {
  readonly pageTitle: string | React.ReactElement<any> | null;
  readonly onCreate?: () => any;
}

const Table = <Entity extends {}>({
  pageTitle,
  onCreate,
  ...props
}: Props<Entity>) => (
  <Container>
    <h1>{pageTitle}</h1>
    {onCreate ? <Button onClick={onCreate}>Create</Button> : null}
    <ANTDTable<Entity> {...props} />
  </Container>
);

export default Table;
