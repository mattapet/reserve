/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Config } from '@reserve/client/lib/types/config';
//#endregion

export default (() => {
  const env = process.env.NODE_ENV ?? 'development';
  return require(`./${env}`).default as Config;
})();
