#!/bin/bash

VERSION_REGEX="v[0-9]*.[0-9]*.[0-9]*"
version="$(git describe --abbrev=0 --match="$VERSION_REGEX" --tags)"
major=0
minor=0
patch=0

# break down the version number into it's components
regex="^v([0-9]+).([0-9]+).([0-9]+)$"
if [[ $version =~ $regex ]]; then
  major="${BASH_REMATCH[1]}"
  minor="${BASH_REMATCH[2]}"
  patch="${BASH_REMATCH[3]}"
fi

# check paramater to see which number to increment
if [[ "$1" == "major" ]]; then
  major=$(echo $major + 1 | bc)
elif [[ "$1" == "feature" ]]; then
  minor=$(echo $minor + 1 | bc)
elif [[ "$1" == "patch" ]]; then
  patch=$(echo $patch + 1 | bc)
elif [[ "$1" == "--help" ]]; then
  echo "usage: ./version.sh [major/feature/patch] [--dry-run]"
  exit 0
elif [[ "$1" == "-h" ]]; then
  echo "usage: ./version.sh [major/feature/patch] [--dry-run]"
  exit 0
else
  echo "usage: ./version.sh [major/feature/patch] [--dry-run]"
  exit -1
fi

if [[ "$2" == "--dry-run" ]]; then
  echo "v${major}.${minor}.${patch}"
else
  # Create the new version tag
  git tag "v${major}.${minor}.${patch}" && git push --tags
fi

