/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
//#endregion

interface MappedState<Entity> {
  readonly entities: { readonly [id: number]: Entity };
}

interface MappedDispatch {
  readonly onFetch: (id: number | string) => any;
}

export interface Props<Entity> extends MappedState<Entity>, MappedDispatch { }

export interface WrappedProps<Entity> {
  readonly id: number | string;
  readonly entity?: Entity;
}

export default function provider<
  Entity extends {},
  P extends WrappedProps<Entity> = WrappedProps<Entity>,
>(Component: React.ComponentType<P>) {
  return class extends React.Component<Props<Entity> & P> {
    private get entity(): Entity | undefined {
      return this.props.entities[this.props.id as number];
    }

    public componentDidMount() {
      this.fetch();
    }

    public componentDidUpdate(prevProps: Props<Entity> & P) {
      if (prevProps.id === this.props.id) {
        this.fetch();
      }
    }

    private fetch() {
      const { id, onFetch } = this.props;
      !this.entity && onFetch(id);
    }

    public render() {
      const { entities, onFetch, ...wrappedProps } = this.props;
      return (
        <Component
          {...wrappedProps as P}
          entity={this.entity}
        />
      );
    }
  } as React.ComponentType<Props<Entity> & P>;
}
