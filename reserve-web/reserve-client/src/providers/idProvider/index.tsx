/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
//#endregion

export interface Props extends RouteComponentProps { }

export interface WrappedProps {
  readonly id: number | string;
}

function provider<P extends WrappedProps = WrappedProps>(
  Component: React.ComponentType<P>,
) {
  return withRouter(class extends React.Component<Props & P> {
    private get id(): number | string {
      return parseInt((this.props.match.params as any).id, 10);
    }

    public render() {
      const { ...wrappedProps } = this.props;
      return (
        <Component
          {...wrappedProps as P}
          id={this.id}
        />
      );
    }
  });
}

export default provider;
