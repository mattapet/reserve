/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
//#endregion

interface MappedState<Entity> {
  readonly data: { readonly [id: string]: Entity; };
  readonly meta: {
    readonly retrieved: boolean;
    readonly pending: boolean;
    readonly error?: Error;
  };
}

interface MappedDispatch {
  readonly onFetch: () => any;
}

export interface Props<Entity> extends MappedState<Entity>, MappedDispatch { }

export interface WrappedProps<Entity> extends MappedState<Entity> { }

export default function collectionProvider<
  Entity extends {},
  P extends WrappedProps<Entity>,
>(Component: React.ComponentType<P>) {
  return class extends React.Component<Props<Entity> & P> {
    public componentDidMount() {
      this.fetch();
    }

    public componentDidUpdate() {
      this.fetch();
    }

    private fetch() {
      const { meta, onFetch } = this.props;
      !meta.retrieved && !meta.pending && onFetch();
    }

    public render() {
      const { onFetch, ...wrappedProps } = this.props;
      return (
        <Component
          {...wrappedProps as P}
        />
      );
    }
  };
}
