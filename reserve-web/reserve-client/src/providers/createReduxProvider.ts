/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect, ConnectedComponentClass } from 'react-redux';
import { Dispatch } from 'redux';
import { RootState } from '../reducers';
//#endregion

export default function createReduxProvider<
  MappedState extends {},
  MappedDispatch extends {},
  Provider extends (Component: React.ComponentType<Props>) => any,
  Props extends {},
>(
  mapStateToProps: (state: RootState) => MappedState,
  mapDispatchToProps: (dispatch: Dispatch) => MappedDispatch,
  provider: Provider,
) {
  type Mapped = MappedState & MappedDispatch;
  return <
    WrappedProps extends Props,
    C extends React.ComponentType<WrappedProps>
      = React.ComponentType<WrappedProps>,
    R extends ConnectedComponentClass<C, Omit<WrappedProps, keyof Mapped>>
      = ConnectedComponentClass<C, Omit<WrappedProps, keyof Mapped>>,
  >(Component: C) => (
    connect(mapStateToProps, mapDispatchToProps)(
      provider(Component as any)
    ) as R
  );
}

