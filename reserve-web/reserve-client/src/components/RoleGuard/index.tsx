/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { connect } from 'react-redux';

import { UserRole } from 'lib/types/UserRole';
import { RootState } from '../..//reducers';
//#endregion

export interface Props {
  readonly defaultComponent?: React.ReactElement<any> | null;
  readonly userRole?: UserRole;
  readonly roles: UserRole[];
}

const GuardRole: React.FunctionComponent<Props> = (props) => (
  (props.userRole && new Set(props.roles).has(props.userRole) ?
    props.children : props.defaultComponent || null) as any
);

const mapStateToProps = (state: RootState) => {
  if (!state.auth.isLoggedIn) {
    return { };
  }
  return { userRole: state.auth.identity.role };
};

export default connect(mapStateToProps)(GuardRole);

