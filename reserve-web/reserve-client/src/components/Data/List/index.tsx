/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import styled from 'styled-components';
import { Table, Button } from 'antd';
import { ColumnProps } from 'antd/lib/table';
//#endregion

const Container = styled.div`
  height: 100%;
`;

export interface Props<Entity extends {}> {
  readonly title: string | React.ReactElement<any> | null;
  readonly data?: Entity[];
  readonly pending?: boolean;
  readonly columns: ColumnProps<Entity>[];
  readonly extractKey: (entity: Entity, index: number) => string;
  readonly expandedRowRender?: (
    entity: Entity,
    index: number,
  ) => React.ReactElement<any> | null;
  readonly onCreate?: () => any;
}

const DataList = <Entity extends {}>(props: Props<Entity>) => (
  <Container>
    <h1>{props.title}</h1>
    {props.onCreate ?
      <Button onClick={props.onCreate}>Create</Button> : null}
    <Table<Entity>
      dataSource={props.data}
      columns={props.columns}
      loading={props.pending}
      rowKey={props.extractKey}
      expandedRowRender={props.expandedRowRender}
    />
  </Container>
);

export default DataList;
