/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Layout } from 'antd';
import { Meta } from 'src/lib/types/Meta';
//#endregion

export interface Props {
  readonly title: string;
  readonly meta: Meta;
  readonly onComplete: () => any;
}

class DataCreate extends React.Component<Props> {
  public componentDidUpdate(prevProps: Props) {
    if (prevProps.meta.pending && !this.props.meta.pending) {
      return this.props.onComplete();
    }
  }

  public render() {
    return (
      <Layout>
        <Layout.Header>
          <h1>{this.props.title}</h1>
        </Layout.Header>
        <Layout.Content>
          {
              React.cloneElement(
                React.Children.only(this.props.children as any), {})
          }
        </Layout.Content>
      </Layout>
    );
  }
}

export default DataCreate;
