/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Layout, Menu } from 'antd';
import { NavLink, RouteComponentProps,withRouter } from 'react-router-dom';
import RoleGuard from '../../RoleGuard';
import { UserRole } from 'lib/types/UserRole';
//#endregion

const Sidebar: React.FunctionComponent<RouteComponentProps<{}>> = (props) => (
  <Layout.Sider>
    <RoleGuard
      roles={[ UserRole.owner, UserRole.maintainer ]}
      defaultComponent={
        <Menu
          mode="inline"
          theme="dark"
          defaultSelectedKeys={['/overview']}
          selectedKeys={[props.location.pathname]}
        >
          <Menu.Item key="/overview">
            <NavLink to="/overview">
              Overview
            </NavLink>
          </Menu.Item>
          <Menu.Item key="/my_reservations">
            <NavLink to="/my_reservations">
              My Reservations
            </NavLink>
          </Menu.Item>
        </Menu>
      }
    >
      <Menu
        mode="inline"
        theme="dark"
        defaultSelectedKeys={['/overview']}
        selectedKeys={[props.location.pathname]}
      >
        <Menu.Item key="/overview">
          <NavLink to="/overview">
            Overview
          </NavLink>
        </Menu.Item>
        <Menu.Item key="/my_reservations">
          <NavLink to="/my_reservations">
            My Reservations
          </NavLink>
        </Menu.Item>

        <Menu.Item key="/resources">
          <NavLink to="/resources">
            Resources
          </NavLink>
        </Menu.Item>
        <Menu.Item key="/reservations">
          <NavLink to="/reservations">
            Reservations
          </NavLink>
        </Menu.Item>
        <Menu.Item key="/restrictions">
          <NavLink to="/restrictions">
            Restrictions
          </NavLink>
        </Menu.Item>
        <Menu.Item key="/users">
          <NavLink to="/users">
            Users
          </NavLink>
        </Menu.Item>
        <Menu.Item key="/workspace">
          <NavLink to="/workspace">
            Workspace
          </NavLink>
        </Menu.Item>
      </Menu>
    </RoleGuard>
  </Layout.Sider>
);

export default withRouter(Sidebar);
