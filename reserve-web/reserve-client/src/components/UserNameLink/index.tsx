/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Link } from 'react-router-dom';

import { userEntityProvider, EntityProviderProps } from '../../providers';
import { User } from 'lib/types/User';
//#endregion

type Props = EntityProviderProps<User>;

const UserNameLink: React.FunctionComponent<Props> = ({ id, entity }) => (
  entity ?
    <Link to={`/users/${id}/details`}>
      {`${entity.profile!.firstName} ${entity.profile!.lastName}`}
    </Link>
    : <i>Loading...</i>
);

export default userEntityProvider<Props>(UserNameLink);

