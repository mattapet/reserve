/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import ANTDPassword, { PasswordProps } from 'antd/lib/input/Password';
//#endregion

export interface Props extends Omit<PasswordProps, 'onChange'> {
  readonly onChange?: (value: string) => any;
}

const Password: React.FunctionComponent<Props> = ({
  onChange,
  ...props
}: Props) => (
  <ANTDPassword
    {...props}
    onChange={e => onChange && onChange(e.target.value)}
  />
);

export default Password;
