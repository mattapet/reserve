/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Icon, Spin } from 'antd';
import styled from 'styled-components';
//#endregion

//#region Component interfaces
export interface SpinnerProps {
  readonly active?: boolean;
  readonly size?: number;
  readonly tip?: string;
}
//#endregion

//#region Styled
const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;
//#endregion

const Spinner: React.FunctionComponent<SpinnerProps> = (props) => (
  props.active === false ? null :
    <Container>
      <Spin
        size="large"
        spinning={true}
        indicator={<Icon type="loading" theme="outlined" />}
      >
        {props.children}
      </Spin>
    </Container>
);

export default Spinner;
