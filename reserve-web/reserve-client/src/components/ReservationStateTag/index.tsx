/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Tag } from 'antd';

import { ReservationState } from 'lib/types/ReservationState';
//#endregion

export interface Props {
  readonly state: ReservationState;
}

function colorForState(state: ReservationState): string {
  switch (state) {
  case ReservationState.confirmed: return 'green';
  case ReservationState.requested: return 'geekblue';
  case ReservationState.rejected: return 'red';
  case ReservationState.canceled: return 'orange';
  case ReservationState.completed: return '';
  }
}

const ReservationStateTag: React.FunctionComponent<Props> = ({ state }) => (
  <Tag color={colorForState(state)}>
    {state.toUpperCase()}
  </Tag>
);

export default ReservationStateTag;
