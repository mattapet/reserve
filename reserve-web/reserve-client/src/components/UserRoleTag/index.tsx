/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Tag } from 'antd';

import { UserRole } from 'lib/types/UserRole';
//#endregion

export interface Props {
  readonly role: UserRole;
}

function colorForRole(role: UserRole): string {
  switch (role) {
  case UserRole.owner: return 'green';
  case UserRole.maintainer: return 'geekblue';
  case UserRole.user: return '';
  }
}

const UserRoleTag: React.FunctionComponent<Props> = ({ role }) => (
  <Tag color={colorForRole(role)}>
    {role.toUpperCase()}
  </Tag>
);

export default UserRoleTag;
