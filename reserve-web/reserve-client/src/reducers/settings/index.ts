/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../actions';
import * as actions from '../../actions/settings';
import { ReservationSettings, UserSettings } from 'lib/types/Settings';
//#endregion

interface UnfetchedSettingsState {
  readonly fetched: false;
}

interface FetchedSettingsState {
  readonly fetched: true;
  readonly reservationSettings: ReservationSettings;
  readonly userSettings: UserSettings;
}

export type SettingsState = UnfetchedSettingsState | FetchedSettingsState;

const initialState: SettingsState = {
  fetched: false,
};

export function settings(
  state = initialState,
  action: RootAction,
): SettingsState {
  switch (action.type) {
  case getType(actions.fetch.success):
    return {
      fetched: true,
      reservationSettings: action.payload.reservations,
      userSettings: action.payload.users,
    };

  case getType(actions.editUser.success): {
    if (!state.fetched) { return state; }
    return {
      ...state,
      userSettings: action.payload,
    };
  }

  case getType(actions.editReservation.success): {
    if (!state.fetched) { return state; }
    return {
      ...state,
      reservationSettings: action.payload,
    };
  }

  default:
    return state;
  }
}

