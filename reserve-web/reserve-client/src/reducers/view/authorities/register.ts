/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../../actions';
import * as actions from '../../../actions/authorities';
//#endregion

export interface RegisterMeta {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: RegisterMeta = {
  pending: false,
};

export function register(
  state = initialState,
  action: RootAction,
): RegisterMeta {
  switch (action.type) {
  case getType(actions.register.request):
    return {
      ...state,
      pending: true,
    };

  case getType(actions.register.success):
    return {
      pending: false,
    };

  case getType(actions.register.cancel):
    return {
      pending: false,
    };

  case getType(actions.register.fail):
    return {
      error: action.payload,
      pending: false,
    };

  default:
    return state;
  }
}
