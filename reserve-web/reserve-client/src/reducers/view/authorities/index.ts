/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineReducers } from 'redux';
import { register, RegisterMeta } from './register';
import { fetch, FetchMeta } from './fetch';
import { edit, EditMeta } from './edit';
import { remove, RemoveMeta } from './remove';
//#endregion

export interface AuthoritiesMeta {
  readonly register: RegisterMeta;
  readonly fetch: FetchMeta;
  readonly edit: EditMeta;
  readonly remove: RemoveMeta;
}

export const authorities = combineReducers({ register, fetch, edit, remove });
