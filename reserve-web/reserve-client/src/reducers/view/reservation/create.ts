/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../../actions';
import * as actions from '../../../actions/reservations';
//#endregion

export interface CreateMeta {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: CreateMeta = {
  pending: false,
};

export function create(
  state = initialState,
  action: RootAction,
): CreateMeta {
  switch (action.type) {
  case getType(actions.create.request):
    return {
      ...state,
      pending: true,
    };

  case getType(actions.create.success):
    return {
      pending: false,
    };

  case getType(actions.create.cancel):
    return {
      pending: false,
    };

  case getType(actions.create.fail):
    return {
      error: action.payload,
      pending: false,
    };

  default:
    return state;
  }
}
