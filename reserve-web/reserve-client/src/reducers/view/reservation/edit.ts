/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../../actions';
import * as actions from '../../../actions/reservations';
//#endregion

export interface EditMeta {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: EditMeta = {
  pending: false,
};

export function edit(
  state = initialState,
  action: RootAction,
): EditMeta {
  switch (action.type) {
  case getType(actions.edit.request):
  case getType(actions.confirmById.request):
  case getType(actions.cancelById.request):
  case getType(actions.rejectById.request):
    return {
      ...state,
      pending: true,
    };

  case getType(actions.edit.success):
  case getType(actions.confirmById.success):
  case getType(actions.cancelById.success):
  case getType(actions.rejectById.success):
    return {
      pending: false,
    };

  case getType(actions.edit.cancel):
  case getType(actions.confirmById.cancel):
  case getType(actions.cancelById.cancel):
  case getType(actions.rejectById.cancel):
    return {
      pending: false,
    };

  case getType(actions.edit.fail):
  case getType(actions.confirmById.fail):
  case getType(actions.cancelById.fail):
  case getType(actions.rejectById.fail):
    return {
      error: action.payload,
      pending: false,
    };

  default:
    return state;
  }
}
