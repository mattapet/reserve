/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { authActions as actions, RootAction } from 'actions';
//#endregion

export interface SetPasswordMeta {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: SetPasswordMeta = {
  pending: false,
};

export function setPassword(
  state = initialState,
  action: RootAction,
): SetPasswordMeta {
  switch (action.type) {
  case getType(actions.setPassword.request):
    return {
      ...state,
      pending: true,
    };

  case getType(actions.setPassword.success):
    return {
      pending: false,
    };

  case getType(actions.setPassword.cancel):
    return {
      ...state,
      pending: false,
    };

  case getType(actions.setPassword.fail):
    return {
      error: action.payload,
      pending: false,
    };

  default:
    return state;
  }
}
