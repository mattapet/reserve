/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../../actions';
import * as authActions from '../../../actions/auth';
//#endregion

export interface RegisterMeta {
  readonly error?: Error;
  readonly retrieved: boolean;
  readonly pending: boolean;
}

const initialState: RegisterMeta = {
  pending: false,
  retrieved: false,
};

export function register(
  state = initialState,
  action: RootAction,
): RegisterMeta {
  switch (action.type) {
  case getType(authActions.register.request):
    return {
      pending: true,
      retrieved: false,
    };

  case getType(authActions.register.success):
    return {
      pending: false,
      retrieved: true,
    };

  case getType(authActions.register.cancel):
    return {
      pending: false,
      retrieved: false,
    };

  case getType(authActions.register.fail):
    return {
      error: action.payload,
      pending: false,
      retrieved: true,
    };

  default:
    return state;
  }
}
