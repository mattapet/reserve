/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../../actions';
import * as resourceActions from '../../../actions/restrictions';
//#endregion

export interface RemoveMeta {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: RemoveMeta = {
  pending: false,
};

export function remove(
  state = initialState,
  action: RootAction,
): RemoveMeta {
  switch (action.type) {
  case getType(resourceActions.remove.request):
    return {
      ...state,
      pending: true,
    };

  case getType(resourceActions.remove.success):
    return {
      pending: false,
    };

  case getType(resourceActions.remove.cancel):
    return {
      pending: false,
    };

  case getType(resourceActions.remove.fail):
    return {
      error: action.payload,
      pending: false,
    };

  default:
    return state;
  }
}
