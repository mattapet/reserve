/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineReducers } from 'redux';
import { auth, AuthMeta } from './auth';
import { login, LoginMeta } from './login';
import { authorities, AuthoritiesMeta } from './authorities';
import { register, RegisterMeta } from './register';
import { resource, ResourceMeta } from './resource';
import { reservation, ReservationMeta } from './reservation';
import { restriction, RestrictionMeta } from './restriction';
import { settings, SettingsState } from './settings';
import { user, UserMeta } from './user';
import { webhook, WebhookMeta } from './webhook';
//#endregion

export interface ViewState {
  readonly auth: AuthMeta;
  readonly login: LoginMeta;
  readonly authorities: AuthoritiesMeta;
  readonly register: RegisterMeta;
  readonly resource: ResourceMeta;
  readonly reservation: ReservationMeta;
  readonly restriction: RestrictionMeta;
  readonly settings: SettingsState;
  readonly user: UserMeta;
  readonly webhook: WebhookMeta;
}

export const view = combineReducers({
  auth,
  login,
  authorities,
  register,
  resource,
  reservation,
  restriction,
  settings,
  user,
  webhook,
});
