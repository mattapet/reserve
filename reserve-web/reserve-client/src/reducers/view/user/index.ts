/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineReducers } from 'redux';
import { fetch, FetchMeta } from './fetch';
import { edit, EditMeta } from './edit';
import { getMe, GetMeMeta } from './getMe';
import { deleteMe, DeleteMeMeta } from './deleteMe';
//#endregion

export interface UserMeta {
  readonly fetch: FetchMeta;
  readonly edit: EditMeta;
  readonly getMe: GetMeMeta;
  readonly deleteMe: DeleteMeMeta;
}

export const user = combineReducers({ fetch, edit, deleteMe, getMe });
