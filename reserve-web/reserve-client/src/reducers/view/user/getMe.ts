/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { authActions, userActions as actions, RootAction } from 'actions';
//#endregion

export interface GetMeMeta {
  readonly error?: Error;
  readonly pending: boolean;
  readonly retrieved: boolean;
}

const initialState: GetMeMeta = {
  pending: false,
  retrieved: false,
};

export function getMe(
  state = initialState,
  action: RootAction,
): GetMeMeta {
  switch (action.type) {
  case getType(actions.getMe.request):
    return {
      ...state,
      pending: true,
    };

  case getType(actions.getMe.success):
  case getType(authActions.logout.success):
    return {
      pending: false,
      retrieved: true,
    };

  case getType(actions.getMe.cancel):
    return {
      ...state,
      pending: false,
    };

  case getType(actions.getMe.fail):
    return {
      error: action.payload,
      pending: false,
      retrieved: true,
    };

  default:
    return state;
  }
}
