/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { userActions as actions, RootAction } from 'actions';
//#endregion

export interface EditMeta {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: EditMeta = {
  pending: false,
};

export function edit(
  state = initialState,
  action: RootAction,
): EditMeta {
  switch (action.type) {
  case getType(actions.toggleBan.request):
  case getType(actions.updateRole.request):
  case getType(actions.updateProfile.request):
    return {
      ...state,
      pending: true,
    };

  case getType(actions.toggleBan.success):
  case getType(actions.updateRole.success):
  case getType(actions.updateProfile.success):
    return {
      pending: false,
    };

  case getType(actions.toggleBan.cancel):
  case getType(actions.updateRole.cancel):
  case getType(actions.updateProfile.cancel):
    return {
      pending: false,
    };

  case getType(actions.toggleBan.fail):
  case getType(actions.updateRole.fail):
  case getType(actions.updateProfile.fail):
    return {
      error: action.payload,
      pending: false,
    };

  default:
    return state;
  }
}
