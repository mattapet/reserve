/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { userActions as actions, RootAction } from 'actions';
//#endregion

export interface DeleteMeMeta {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: DeleteMeMeta = {
  pending: false,
};

export function deleteMe(
  state = initialState,
  action: RootAction,
): DeleteMeMeta {
  switch (action.type) {
  case getType(actions.deleteMe.request):
    return {
      ...state,
      pending: true,
    };

  case getType(actions.deleteMe.success):
    return {
      pending: false,
    };

  case getType(actions.deleteMe.cancel):
    return {
      ...state,
      pending: false,
    };

  case getType(actions.deleteMe.fail):
    return {
      error: action.payload,
      pending: false,
    };

  default:
    return state;
  }
}
