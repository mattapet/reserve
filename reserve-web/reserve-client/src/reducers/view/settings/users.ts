/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from 'actions';
import * as actions from 'actions/settings';
//#endregion

export interface UserSettingsState {
  readonly error?: Error;
  readonly pending: boolean;
}

const initialState: UserSettingsState = {
  pending: false,
};

export function users(
  state = initialState,
  action: RootAction,
): UserSettingsState {
  switch (action.type) {
  case getType(actions.editUser.request):
    return {
      pending: true,
    };

  case getType(actions.editUser.success):
  case getType(actions.editUser.cancel):
    return {
      pending: false,
    };

  case getType(actions.editUser.fail):
    return {
      pending: false,
      error: action.payload,
    };

  default:
    return state;
  }
}

