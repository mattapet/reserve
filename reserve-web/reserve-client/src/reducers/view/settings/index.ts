/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { reservations, ReservationSettingsState } from './reservations';
import { users, UserSettingsState } from './users';
import { combineReducers } from 'redux';
//#endregion

export interface SettingsState {
  readonly reservations: ReservationSettingsState;
  readonly users: UserSettingsState;
}

export const settings = combineReducers({
  reservations,
  users,
});
