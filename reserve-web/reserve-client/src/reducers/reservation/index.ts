/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../actions';
import * as actions from '../../actions/reservations';
import { Reservation } from 'lib/types/Reservation';
//#endregion

export interface ReservationState {
  readonly data: {
    readonly [id: number]: Reservation;
  };
}

const initialState: ReservationState = {
  data: {},
};

export function reservation(
  state = initialState,
  action: RootAction,
): ReservationState {
  switch (action.type) {
  case getType(actions.fetch.success):
    return {
      data: action.payload.reduce(
        (data, res) =>  ({ ...data, [res.id]: res }),
        {},
      ),
    };

  case getType(actions.fetchById.success):
  case getType(actions.create.success):
  case getType(actions.edit.success):
  case getType(actions.confirmById.success):
  case getType(actions.cancelById.success):
  case getType(actions.rejectById.success):
    return {
      data: { ...state.data, [action.payload.id]: action.payload },
    };

  default:
    return state;
  }
}

