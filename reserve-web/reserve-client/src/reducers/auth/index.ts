/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';
import * as jwt from 'jsonwebtoken';

import { RootAction, authActions, userActions } from 'actions';
import { UserRole } from 'lib/types/UserRole';
import { Profile } from 'lib/types/Profile';
import config from 'config';
//#endregion

export interface Credentials {
  readonly accessToken: string;
  readonly refreshToken: string;
}

export interface Identity {
  readonly id: string;
  readonly role: UserRole;
  readonly email: string;
  readonly profile: Profile;
}

export interface LoggedOutState {
  readonly isLoggedIn: false;
  readonly workspace: string;
  readonly loginAuthorities: string[];
  readonly authoritiesFetched: boolean;
}

export interface LoggedInState {
  readonly isLoggedIn: true;
  readonly identity: Identity;
  readonly tokens: Credentials;
  readonly workspace: string;
  readonly loginAuthorities: string[];
  readonly authoritiesFetched: boolean;
}

export type AuthState =
    LoggedOutState
  | LoggedInState;

function getInitialState(): AuthState {
  const rawState = localStorage.getItem('session');
  if (rawState) {
    return JSON.parse(rawState) as AuthState;
  } else {
    return {
      isLoggedIn: false,
      workspace: config.workspaceName,
      loginAuthorities: [],
      authoritiesFetched: false,
    };
  }
}

export function auth(state = getInitialState(), action: RootAction): AuthState {
  switch (action.type) {
  case getType(authActions.fetchLoginAuthorities.success):
    return {
      ...state,
      loginAuthorities: action.payload,
      authoritiesFetched: true,
    };

  case getType(authActions.login.success):
  case getType(authActions.refresh.success): {
    const payload = jwt.decode(action.payload.access_token) as {
      readonly user: Identity;
    };
    state = {
      ...state,
      identity: payload.user,
      isLoggedIn: true,
      tokens: {
        accessToken: action.payload.access_token,
        refreshToken: action.payload.refresh_token,
      },
      workspace: state.workspace,
      loginAuthorities: state.loginAuthorities,
    };
    localStorage.setItem('session', JSON.stringify(state));
    return state;
  }

  case getType(userActions.getMe.success):
  case getType(userActions.updateProfile.success):
    if (!state.isLoggedIn) { return state; }
    return {
      ...state,
      identity: {
        id: action.payload.id,
        role: action.payload.role,
        email: action.payload.email!,
        profile: action.payload.profile!,
      },
    };


  case getType(authActions.login.fail):
  case getType(authActions.register.fail):
  case getType(authActions.logout.success):
  case getType(authActions.logout.fail):
  case getType(userActions.deleteMe.success):
    localStorage.removeItem('session');
    return {
      ...state,
      isLoggedIn: false,
      workspace: config.workspaceName,
      loginAuthorities: state.loginAuthorities,
    };

  default:
    return state;
  }
}
