/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../actions';
import * as resourceActions from '../../actions/resources';
import { Resource } from 'lib/types/Resource';
//#endregion

export interface ResourceState {
  readonly data: {
    readonly [id: number]: Resource;
  };
}

const initialState: ResourceState = {
  data: {},
};

export function resource(
  state = initialState,
  action: RootAction,
): ResourceState {
  switch (action.type) {
  case getType(resourceActions.fetch.success):
    return {
      data: action.payload.reduce(
        (data, res) =>  ({ ...data, [res.id]: res }),
        {},
      ),
    };

  case getType(resourceActions.fetchById.success):
  case getType(resourceActions.create.success):
  case getType(resourceActions.edit.success):
  case getType(resourceActions.toggle.success):
    return {
      data: { ...state.data, [action.payload.id]: action.payload },
    };

  case getType(resourceActions.remove.success):
    const { [action.payload]: removed, ...rest } = state.data;
    return { data: rest };

  default:
    return state;
  }
}

