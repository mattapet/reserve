/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineReducers } from 'redux';
import { auth, AuthState } from './auth';
import { authorities, AuthorityState } from './authorities';
import { resource, ResourceState } from './resource';
import { reservation, ReservationState } from './reservation';
import { restriction, RestrictionState } from './restriction';
import { settings, SettingsState } from './settings';
import { user, UserState } from './user';
import { webhook, WebhookState } from './webhook';
import { workspace, WorkspaceState } from './workspace';
import { view, ViewState } from './view';
import { authActions, RootAction } from 'actions';
import { getType } from 'typesafe-actions';
//#endregion

export interface RootState {
  readonly auth: AuthState;
  readonly authorities: AuthorityState;
  readonly resource: ResourceState;
  readonly reservation: ReservationState;
  readonly restriction: RestrictionState;
  readonly settings: SettingsState;
  readonly user: UserState;
  readonly webhook: WebhookState;
  readonly workspace: WorkspaceState;
  readonly view: ViewState;
}

const appReducer = combineReducers<RootState>({
  auth,
  authorities,
  resource,
  reservation,
  restriction,
  settings,
  user,
  webhook,
  workspace,
  view,
});

export const rootReducer = (state: RootState, action: RootAction) => {
  if (getType(authActions.logout.success) === action.type) {
    // tslint:disable
    const { workspace, settings, auth } = state;
    return appReducer({ workspace, settings, auth } as RootState, action);
  }
  return appReducer(state, action);
};
