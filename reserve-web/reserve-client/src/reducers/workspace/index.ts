/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../actions';
import * as actions from '../../actions/workspace';
import { Workspace } from 'lib/types/Workspace';
//#endregion

export type WorkspaceState = Workspace | null;

const initialState: WorkspaceState = null;

export function workspace(
  state = initialState,
  action: RootAction,
): WorkspaceState {
  switch (action.type) {
  case getType(actions.fetchByName.success):
    return action.payload;
  default:
    return state;
  }
}
