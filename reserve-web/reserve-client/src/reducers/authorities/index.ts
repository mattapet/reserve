/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../actions';
import * as actions from '../../actions/authorities';
import { Authority } from 'lib/types/Authority';
//#endregion

export interface AuthorityState {
  readonly data: {
    readonly [id: string]: Authority;
  };
}

const initialState: AuthorityState = {
  data: {},
};

export function authorities(
  state = initialState,
  action: RootAction,
): AuthorityState {
  switch (action.type) {
  case getType(actions.fetch.success):
    return {
      data: action.payload.reduce(
        (data, res) =>  ({ ...data, [res.name]: res }),
        {},
      ),
    };

  case getType(actions.edit.success):
  case getType(actions.register.success):
    return {
      data: { ...state.data, [action.payload.name]: action.payload },
    };

  case getType(actions.remove.success):
    const { [action.payload]: removed, ...rest } = state.data;
    return { data: rest };

  default:
    return state;
  }
}

