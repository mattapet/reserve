/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getType } from 'typesafe-actions';

import { RootAction } from '../../actions';
import * as actions from '../../actions/restrictions';
import { Restriction } from 'lib/types/Restriction';
//#endregion

export interface RestrictionState {
  readonly data: {
    readonly [id: number]: Restriction;
  };
}

const initialState: RestrictionState = {
  data: {},
};

export function restriction(
  state = initialState,
  action: RootAction,
): RestrictionState {
  switch (action.type) {
  case getType(actions.fetch.success):
    return {
      data: action.payload.reduce(
        (data, res) =>  ({ ...data, [res.id]: res }),
        {},
      ),
    };

  case getType(actions.fetchById.success):
  case getType(actions.create.success):
  case getType(actions.edit.success):
    return {
      data: { ...state.data, [action.payload.id]: action.payload },
    };

  case getType(actions.remove.success):
    const { [action.payload]: removed, ...rest } = state.data;
    return { data: rest };

  default:
    return state;
  }
}

