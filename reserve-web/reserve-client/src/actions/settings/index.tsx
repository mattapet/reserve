/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ActionType } from 'typesafe-actions';

import * as fetch from './fetch';
import * as editReservation from './editReservation';
import * as editUser from './editUser';
//#endregion

export { fetch, editReservation, editUser };

//#region types
export type SettingsActionFetch =
    ActionType<typeof fetch.request>
  | ActionType<typeof fetch.success>
  | ActionType<typeof fetch.cancel>
  | ActionType<typeof fetch.fail>;

export type SettingsActionReservationEdit =
    ActionType<typeof editReservation.request>
  | ActionType<typeof editReservation.success>
  | ActionType<typeof editReservation.cancel>
  | ActionType<typeof editReservation.fail>;

export type SettingsActionUserEdit =
    ActionType<typeof editUser.request>
  | ActionType<typeof editUser.success>
  | ActionType<typeof editUser.cancel>
  | ActionType<typeof editUser.fail>;

export type SettingsAction =
  | SettingsActionFetch
  | SettingsActionReservationEdit
  | SettingsActionUserEdit;
//#endregion
