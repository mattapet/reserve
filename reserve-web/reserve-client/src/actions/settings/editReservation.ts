/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { EDIT_RESERVATION_SETTINGS } from './constants';
import * as mapper from './mapper';
import { ReservationSettings } from 'lib/types/Settings';
import { ReservationSettingsDTO } from 'lib/types/dto/Settings';
//#endregion

export const request = createStandardAction(EDIT_RESERVATION_SETTINGS.REQUEST)
  .map(
    (workspaceId: string, settings: ReservationSettings) => ({
      payload: mapper.encodeReservation(settings),
      meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: `api/v1/workspace/${workspaceId}/settings/reservation`,
      },
    }),
  );

export const success = createStandardAction(EDIT_RESERVATION_SETTINGS.SUCCESS)
.map(
  (dto: ReservationSettingsDTO) => ({
    payload: mapper.decodeReservation(dto),
  }));

export const cancel = createAction(EDIT_RESERVATION_SETTINGS.CANCEL);

export const fail =
  createStandardAction(EDIT_RESERVATION_SETTINGS.FAIL)<Error>();
