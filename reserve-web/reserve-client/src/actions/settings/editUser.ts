/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { EDIT_USER_SETTINGS } from './constants';
import * as mapper from './mapper';
import { UserSettings } from 'lib/types/Settings';
import { UserSettingsDTO } from 'lib/types/dto/Settings';
//#endregion

export const request = createStandardAction(EDIT_USER_SETTINGS.REQUEST).map(
  (workspaceId: string, settings: UserSettings) => ({
    payload: mapper.encodeUser(settings),
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/workspace/${workspaceId}/settings/user`,
    },
  }),
);

export const success = createStandardAction(EDIT_USER_SETTINGS.SUCCESS).map(
  (dto: UserSettingsDTO) => ({
    payload: mapper.decodeUser(dto),
  }));

export const cancel = createAction(EDIT_USER_SETTINGS.CANCEL);

export const fail = createStandardAction(EDIT_USER_SETTINGS.FAIL)<Error>();
