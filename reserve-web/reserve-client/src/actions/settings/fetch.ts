/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { FETCH_SETTINGS } from './constants';
import * as mapper from './mapper';
import { SettingsDTO } from 'lib/types/dto/Settings';
//#endregion

export const request = createStandardAction(FETCH_SETTINGS.REQUEST).map(
  (workspaceId: string) => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/workspace/${workspaceId}/settings`,
    },
  }),
);

export const success = createStandardAction(FETCH_SETTINGS.SUCCESS).map(
  (payload: SettingsDTO) => ({
    payload: mapper.decode(payload),
  }));

export const cancel = createAction(FETCH_SETTINGS.CANCEL);

export const fail = createStandardAction(FETCH_SETTINGS.FAIL)<Error>();
