/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  SettingsDTO,
  UserSettingsDTO,
  ReservationSettingsDTO,
} from 'lib/types/dto/Settings';
import {
  Settings,
  UserSettings,
  ReservationSettings,
} from 'lib/types/Settings';
//#endregion

export function decode(dto: SettingsDTO): Settings {
  return {
    id: dto.id,
    reservations: decodeReservation(dto.reservations),
    users: decodeUser(dto.users),
  };
}

export function encode(settings: Settings): SettingsDTO {
  return {
    id: settings.id,
    reservations: encodeReservation(settings.reservations),
    users: encodeUser(settings.users),
  };
}

export function decodeReservation(
  dto: ReservationSettingsDTO,
): ReservationSettings {
  return {
    maxOffset: dto.max_offset,
    maxReservations: dto.max_reservations,
    maxRange: dto.max_range,
    timeUnit: dto.time_unit,
  };
}

export function encodeReservation(
  settings: ReservationSettings,
): ReservationSettingsDTO {
  return {
    max_offset: settings.maxOffset,
    max_range: settings.maxRange,
    max_reservations: settings.maxReservations,
    time_unit: settings.timeUnit,
  };
}

export function decodeUser(dto: UserSettingsDTO): UserSettings {
  return {
    allowedInvitations: dto.allowed_invitations,
    allowedRegistrations: dto.allowed_registrations,
    allowedToInvite: dto.allowed_to_invite,
  };
}

export function encodeUser(settings: UserSettings): UserSettingsDTO {
  return {
    allowed_invitations: settings.allowedInvitations,
    allowed_registrations: settings.allowedRegistrations,
    allowed_to_invite: settings.allowedToInvite,
  };
}
