/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  ActionType, createStandardAction,
} from 'typesafe-actions';
import { ActionCreator } from 'typesafe-actions/dist/is-action-of';
import { HTTPMethod } from 'lib/types/HTTPMethod';
import * as types from './constants';
//#endregion

export type RequestPayload = { [key: string]: any } | null;

export interface RequestMeta {
  readonly method: HTTPMethod;
  readonly url: string;
  readonly headers?: { [key: string]: any } | null;
  readonly onSuccess:
    (data?: any) => { type: any, payload?: any, meta?: any  };
  readonly onFail: (err: Error) => { type: any, payload: Error, meta?: any };
  readonly onCancel: ActionCreator<any>;
  readonly throttle?: number;
}

export interface SpecifiedRequestMeta {
  readonly url: string;
  readonly headers?: { [key: string]: any } | null;
  readonly onSuccess:
    (data?: any) => { type: any, payload?: any, meta?: any  };
  readonly onFail: (err: Error) => { type: any, payload: Error, meta?: any };
  readonly onCancel: ActionCreator<any>;
  readonly throttle?: number;
}

export const request = createStandardAction(
  types.REQUEST
)<RequestPayload, RequestMeta>();

export const get = createStandardAction(
  types.REQUEST
).map((payload: RequestPayload, meta: SpecifiedRequestMeta) => ({
  meta: {
    ...meta,
    method: HTTPMethod.GET
  },
  payload,
}));

export const post = createStandardAction(types.REQUEST)
.map((payload: RequestPayload, meta: SpecifiedRequestMeta) => ({
  meta: {
    ...meta,
    method: HTTPMethod.POST
  },
  payload,
}));

export const put = createStandardAction(types.REQUEST)
.map((payload: RequestPayload, meta: SpecifiedRequestMeta) => ({
  meta: {
    ...meta,
    method: HTTPMethod.PUT
  },
  payload,
}));

export const patch = createStandardAction(types.REQUEST)
.map((payload: RequestPayload, meta: SpecifiedRequestMeta) => ({
  meta: {
    ...meta,
    method: HTTPMethod.PATCH
  },
  payload,
}));

export const del = createStandardAction(types.REQUEST)
.map((payload: RequestPayload, meta: SpecifiedRequestMeta) => ({
  meta: {
    ...meta,
    method: HTTPMethod.DELETE
  },
  payload,
}));

export type RequestAction =
    ActionType<typeof request>
  | ActionType<typeof get>
  | ActionType<typeof post>
  | ActionType<typeof put>
  | ActionType<typeof patch>
  | ActionType<typeof del>;
