/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { REMOVE_WEBHOOK } from './constants';
//#endregion

export const request = createStandardAction(REMOVE_WEBHOOK.REQUEST).map(
  (id: number) => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: () => success(id),
      url: `api/v1/webhook/${id}`,
    },
  }),
);

export const success =
  createStandardAction(REMOVE_WEBHOOK.SUCCESS)<number>();

export const cancel = createAction(REMOVE_WEBHOOK.CANCEL);

export const fail = createStandardAction(REMOVE_WEBHOOK.FAIL)<Error>();
