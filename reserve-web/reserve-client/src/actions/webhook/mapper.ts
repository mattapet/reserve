/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WebhookDTO } from 'lib/types/dto/Webhook.dto';
import { Webhook } from 'lib/types/Webhook';
//#endregion

export function decode(dto: WebhookDTO): Webhook {
  return {
    id: dto.id,
    payloadUrl: dto.payload_url,
    secret: dto.secret,
    encoding: dto.encoding,
    events: dto.events,
    active: dto.active,
  };
}

export function encode(webhook: Webhook): WebhookDTO {
  return {
    id: webhook.id,
    payload_url: webhook.payloadUrl,
    secret: webhook.secret,
    encoding: webhook.encoding,
    events: webhook.events,
    active: webhook.active,
  };
}
