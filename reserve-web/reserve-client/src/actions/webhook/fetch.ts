/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { WebhookDTO } from 'lib/types/dto/Webhook.dto';
import { FETCH_WEBHOOKS } from './constants';
import * as mapper from './mapper';
//#endregion

export const request = createStandardAction(FETCH_WEBHOOKS.REQUEST).map(
  () => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/webhook',
    },
  }),
);

export const success = createStandardAction(FETCH_WEBHOOKS.SUCCESS).map(
  (payload: WebhookDTO[]) => ({
    payload: payload.map(mapper.decode),
  }),
);

export const cancel = createAction(FETCH_WEBHOOKS.CANCEL);

export const fail = createStandardAction(FETCH_WEBHOOKS.FAIL)<Error>();
