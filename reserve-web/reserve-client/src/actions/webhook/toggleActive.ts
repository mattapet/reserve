/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { TOGGLE_ACTIVE_WEBHOOK } from './constants';
import * as mappers from './mapper';
import { WebhookDTO } from 'lib/types/dto/Webhook.dto';
//#endregion

export const request = createStandardAction(TOGGLE_ACTIVE_WEBHOOK.REQUEST)
  .map(
    (id: number) => ({
      meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: `api/v1/webhook/${id}/toggle`,
      },
    }),
  );

export const success = createStandardAction(TOGGLE_ACTIVE_WEBHOOK.SUCCESS)
  .map((payload: WebhookDTO) => ({
    payload: mappers.decode(payload)
  }));

export const cancel = createAction(TOGGLE_ACTIVE_WEBHOOK.CANCEL);

export const fail = createStandardAction(TOGGLE_ACTIVE_WEBHOOK.FAIL)<Error>();
