/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { WebhookDTO } from 'lib/types/dto/Webhook.dto';
import { FETCH_WEBHOOK_BY_ID } from './constants';
import * as mapper from './mapper';
//#endregion

export const request = createStandardAction(FETCH_WEBHOOK_BY_ID.REQUEST).map(
  (id: number) => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/webhook/${id}`,
    },
  }),
);

export const success = createStandardAction(FETCH_WEBHOOK_BY_ID.SUCCESS).map(
  (payload: WebhookDTO) => ({
    payload: mapper.decode(payload),
  })
);

export const cancel = createAction(FETCH_WEBHOOK_BY_ID.CANCEL);

export const fail = createStandardAction(FETCH_WEBHOOK_BY_ID.FAIL)<Error>();
