/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { FETCH_LOGIN_AUTHORITOIES } from './constants';
//#endregion

export const request = createStandardAction(FETCH_LOGIN_AUTHORITOIES.REQUEST)
  .map((workspaceId: string) => ({
      meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: `api/v1/workspace/${workspaceId}/authority`,
      }
    }),
  );

export const success = createStandardAction(FETCH_LOGIN_AUTHORITOIES.SUCCESS)
  .map((payload: any) => ({ payload }));

export const cancel = createAction(FETCH_LOGIN_AUTHORITOIES.CANCEL);

export const fail = createStandardAction(
  FETCH_LOGIN_AUTHORITOIES.FAIL
)<Error>();
