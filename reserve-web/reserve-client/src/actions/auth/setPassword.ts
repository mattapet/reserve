/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { SET_PASSWORD } from './constants';
//#endregion

export const request = createStandardAction(SET_PASSWORD.REQUEST)
  .map((password: string) => ({
      payload: {
        password,
      },
      meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: 'api/v1/password',
      }
    }),
  );

export const success = createStandardAction(SET_PASSWORD.SUCCESS)
  .map((payload: any) => ({ payload }));

export const cancel = createAction(SET_PASSWORD.CANCEL);

export const fail = createStandardAction(
  SET_PASSWORD.FAIL
)<Error>();
