/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import config from 'config';
import { createAction, createStandardAction } from 'typesafe-actions';
import { LOGIN } from './constants';
//#endregion

export interface RequestPayload {
  readonly email: string;
  readonly password: string;
}

export const request = createStandardAction(LOGIN.REQUEST).map(
  (payload: RequestPayload) => ({
    meta: {
      headers: null,
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/oauth2/token',
    },
    payload: {
      grant_type: 'password',
      password: payload.password,
      username: payload.email,
      workspace: config.workspaceName,
    },
  }),
);

export const success = createStandardAction(LOGIN.SUCCESS).map(
  (payload: {}) => ({ payload: payload as any }),
);

export const cancel = createAction(LOGIN.CANCEL);

export const fail = createStandardAction(LOGIN.FAIL)<Error>();
