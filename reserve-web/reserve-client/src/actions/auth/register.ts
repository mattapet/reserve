/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import config from 'config';
import { createAction, createStandardAction } from 'typesafe-actions';
import { REGISTER } from './constants';
//#endregion

export interface RequestPayload {
  readonly firstName: string;
  readonly lastName: string;
  readonly phone?: string;
  readonly email: string;
  readonly password: string;
}

export const request = createStandardAction(REGISTER.REQUEST).map(
  (workspaceId: string, email: string) => ({
    meta: {
      headers: null,
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/email/verify',
    },
    payload: {
      workspace_id: workspaceId,
      email,
      redirect_uri: `${config.workspaceUrl}/register/confirm`
    },
  }),
);

export const success = createStandardAction(REGISTER.SUCCESS).map(
  (payload: any) => ({ payload }),
);

export const cancel = createAction(REGISTER.CANCEL);

export const fail = createStandardAction(REGISTER.FAIL)<Error>();
