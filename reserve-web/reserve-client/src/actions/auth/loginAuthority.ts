/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { LOGIN_AUTHORITY } from './constants';
//#endregion

export interface RequestPayload {
  readonly authority: string;
  readonly code: string;
  readonly workspaceId: string;
}

export const request = createStandardAction(LOGIN_AUTHORITY.REQUEST)
  .map((payload: RequestPayload) => ({
      payload: {
        ...payload,
        workspace_id: payload.workspaceId,
      },
      meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: 'api/v1/login/authority',
      }
    }),
  );

export const success = createStandardAction(LOGIN_AUTHORITY.SUCCESS)
  .map((payload: any) => ({ payload }));

export const cancel = createAction(LOGIN_AUTHORITY.CANCEL);

export const fail = createStandardAction(
  LOGIN_AUTHORITY.FAIL
)<Error>();
