/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ActionType } from 'typesafe-actions';
import * as fetchLoginAuthorities from './fetchLoginAuthorities';
import * as loginAuthority from './loginAuthority';
import * as login from './login';
import * as logout from './logout';
import * as register from './register';
import * as refresh from './refresh';
import * as confirmEmail from './confirmEmail';
import * as setPassword from './setPassword';
//#endregion

export {
  fetchLoginAuthorities,
  loginAuthority,
  login,
  logout,
  register,
  refresh,
  confirmEmail,
  setPassword,
};

//#region types
export type LoginAuthorityAction =
    ActionType<typeof loginAuthority.request>
  | ActionType<typeof loginAuthority.success>
  | ActionType<typeof loginAuthority.cancel>
  | ActionType<typeof loginAuthority.fail>;

export type FetchLoginAuthoritiesAction =
    ActionType<typeof fetchLoginAuthorities.request>
  | ActionType<typeof fetchLoginAuthorities.success>
  | ActionType<typeof fetchLoginAuthorities.cancel>
  | ActionType<typeof fetchLoginAuthorities.fail>;

export type LoginAction =
    ActionType<typeof login.request>
  | ActionType<typeof login.success>
  | ActionType<typeof login.cancel>
  | ActionType<typeof login.fail>;

 export type LogoutAction =
    ActionType<typeof logout.request>
  | ActionType<typeof logout.success>
  | ActionType<typeof logout.cancel>
  | ActionType<typeof logout.fail>;

 export type RegisterAction =
    ActionType<typeof register.request>
  | ActionType<typeof register.success>
  | ActionType<typeof register.cancel>
  | ActionType<typeof register.fail>;

 export type RefreshAction =
    ActionType<typeof refresh.request>
  | ActionType<typeof refresh.success>
  | ActionType<typeof refresh.cancel>
  | ActionType<typeof refresh.fail>;


 export type ConfirmEmailAction =
    ActionType<typeof confirmEmail.request>
  | ActionType<typeof confirmEmail.success>
  | ActionType<typeof confirmEmail.cancel>
  | ActionType<typeof confirmEmail.fail>;

 export type SetPasswordAction =
    ActionType<typeof setPassword.request>
  | ActionType<typeof setPassword.success>
  | ActionType<typeof setPassword.cancel>
  | ActionType<typeof setPassword.fail>;

export type AuthAction =
  | LoginAuthorityAction
  | FetchLoginAuthoritiesAction
  | LoginAction
  | LogoutAction
  | RefreshAction
  | RegisterAction
  | ConfirmEmailAction
  | SetPasswordAction;
//#endregion
