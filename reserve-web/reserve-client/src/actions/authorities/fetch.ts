/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { FETCH_AUTHORITIES } from './constants';
import * as mapper from './mapper';
import { AuthorityDTO } from 'lib/types/dto/Authority.dto';
//#endregion

export const request = createStandardAction(FETCH_AUTHORITIES.REQUEST).map(
  () => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/authority',
    },
  }),
);

export const success = createStandardAction(FETCH_AUTHORITIES.SUCCESS).map(
  (payload: AuthorityDTO[]) => ({
    payload: payload.map(mapper.decode),
  }));

export const cancel = createAction(FETCH_AUTHORITIES.CANCEL);

export const fail = createStandardAction(FETCH_AUTHORITIES.FAIL)<Error>();
