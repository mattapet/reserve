/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AuthorityDTO } from 'lib/types/dto/Authority.dto';
import { Authority } from 'lib/types/Authority';
//#endregion

export function decode(dto: AuthorityDTO): Authority {
  return {
    name: dto.name,
    description: dto.description,
    authorizeUrl: dto.authorize_url,
    tokenUrl: dto.token_url,
    profileUrls: dto.profile_urls,
    profileMap: {
      firstName: dto.profile_map.first_name,
      lastName: dto.profile_map.last_name,
      email: dto.profile_map.email,
      phone: dto.profile_map.phone,
    },
    redirectUri: dto.redirect_uri,
    clientId: dto.client_id,
    clientSecret: dto.client_secret,
    scope: dto.scope,
  } as Authority;
}

export function encode(authority: Authority): AuthorityDTO {
  return {
    name: authority.name,
    description: authority.description,
    authorize_url: authority.authorizeUrl,
    token_url: authority.tokenUrl,
    profile_urls: authority.profileUrls,
    profile_map: {
      first_name: authority.profileMap.firstName,
      last_name: authority.profileMap.lastName,
      email: authority.profileMap.email,
      phone: authority.profileMap.phone,
    },
    redirect_uri: authority.redirectUri,
    client_id: authority.clientId,
    client_secret: authority.clientSecret,
    scope: authority.scope,
  } as AuthorityDTO;
}
