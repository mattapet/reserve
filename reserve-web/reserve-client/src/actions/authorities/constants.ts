/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export const enum FETCH_AUTHORITIES {
  REQUEST = 'FETCH_AUTHORITIES_REQUEST',
  SUCCESS = 'FETCH_AUTHORITIES_SUCCESS',
  CANCEL = 'FETCH_AUTHORITIES_CANCEL',
  FAIL = 'FETCH_AUTHORITIES_FAIL',
}

export const enum EDIT_AUTHORITY {
  REQUEST = 'EDIT_AUTHORITY_REQUEST',
  SUCCESS = 'EDIT_AUTHORITY_SUCCESS',
  CANCEL = 'EDIT_AUTHORITY_CANCEL',
  FAIL = 'EDIT_AUTHORITY_FAIL',
}

export const enum REGISTER_AUTHORITY {
  REQUEST = 'REGISTER_AUTHORITY_REQUEST',
  SUCCESS = 'REGISTER_AUTHORITY_SUCCESS',
  CANCEL = 'REGISTER_AUTHORITY_CANCEL',
  FAIL = 'REGISTER_AUTHORITY_FAIL',
}

export const enum REMOVE_AUTHORITY {
  REQUEST = 'REMOVE_AUTHORITY_REQUEST',
  SUCCESS = 'REMOVE_AUTHORITY_SUCCESS',
  CANCEL = 'REMOVE_AUTHORITY_CANCEL',
  FAIL = 'REMOVE_AUTHORITY_FAIL',
}
