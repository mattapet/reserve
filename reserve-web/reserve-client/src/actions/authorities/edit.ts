/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { EDIT_AUTHORITY } from './constants';
import * as mapper from './mapper';
import { Authority } from 'lib/types/Authority';
import { AuthorityDTO } from 'lib/types/dto/Authority.dto';
//#endregion

export const request = createStandardAction(EDIT_AUTHORITY.REQUEST).map(
  (authority: Authority) => ({
    payload: mapper.encode(authority),
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/authority/${authority.name}`,
    },
  }),
);

export const success = createStandardAction(EDIT_AUTHORITY.SUCCESS).map(
  (dto: AuthorityDTO) => ({
    payload: mapper.decode(dto),
  }));

export const cancel = createAction(EDIT_AUTHORITY.CANCEL);

export const fail = createStandardAction(EDIT_AUTHORITY.FAIL)<Error>();
