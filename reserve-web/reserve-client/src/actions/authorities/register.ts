/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { REGISTER_AUTHORITY } from './constants';
import * as mapper from './mapper';
import { Authority } from 'lib/types/Authority';
import { AuthorityDTO } from 'lib/types/dto/Authority.dto';
//#endregion

export const request = createStandardAction(REGISTER_AUTHORITY.REQUEST).map(
  (payload: Authority) => ({
    payload: mapper.encode(payload),
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/authority',
    },
  }),
);

export const success = createStandardAction(REGISTER_AUTHORITY.SUCCESS).map(
  (dto: AuthorityDTO) => ({
    payload: mapper.decode(dto),
  }));


export const cancel = createAction(REGISTER_AUTHORITY.CANCEL);

export const fail = createStandardAction(REGISTER_AUTHORITY.FAIL)<Error>();
