/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ActionType } from 'typesafe-actions';

import * as fetch from './fetch';
import * as register from './register';
import * as edit from './edit';
import * as remove from './remove';
//#endregion

export { fetch, register, edit, remove };

//#region types
export type AuthorityActionFetch =
    ActionType<typeof fetch.request>
  | ActionType<typeof fetch.success>
  | ActionType<typeof fetch.cancel>
  | ActionType<typeof fetch.fail>;

export type AuthorityActionRegister =
    ActionType<typeof register.request>
  | ActionType<typeof register.success>
  | ActionType<typeof register.cancel>
  | ActionType<typeof register.fail>;

export type AuthorityActionEdit =
    ActionType<typeof edit.request>
  | ActionType<typeof edit.success>
  | ActionType<typeof edit.cancel>
  | ActionType<typeof edit.fail>;

export type AuthorityActionRemove =
    ActionType<typeof remove.request>
  | ActionType<typeof remove.success>
  | ActionType<typeof remove.cancel>
  | ActionType<typeof remove.fail>;

export type AuthorityAction =
  | AuthorityActionFetch
  | AuthorityActionRegister
  | AuthorityActionEdit
  | AuthorityActionRemove;
//#endregion
