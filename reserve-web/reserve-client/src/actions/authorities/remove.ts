/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { REMOVE_AUTHORITY } from './constants';
//#endregion

export const request = createStandardAction(REMOVE_AUTHORITY.REQUEST).map(
  (name: string) => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: () => success(name),
      url: `api/v1/authority/${name}`,
    },
  }),
);

export const success =
  createStandardAction(REMOVE_AUTHORITY.SUCCESS)<string>();

export const cancel = createAction(REMOVE_AUTHORITY.CANCEL);

export const fail = createStandardAction(REMOVE_AUTHORITY.FAIL)<Error>();
