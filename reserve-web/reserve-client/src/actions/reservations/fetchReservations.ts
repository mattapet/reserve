/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { FETCH_RESERVATIONS } from './constants';
import * as mapper from './mapper';
import { ReservationDTO } from 'lib/types/dto/Reservation.dto';
//#endregion

export const request = createStandardAction(FETCH_RESERVATIONS.REQUEST).map(
  () => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/reservation',
    },
  }),
);

export const success = createStandardAction(FETCH_RESERVATIONS.SUCCESS).map(
  (payload: ReservationDTO[]) => ({
    payload: payload.map(mapper.decode),
  }));

export const cancel = createAction(FETCH_RESERVATIONS.CANCEL);

export const fail = createStandardAction(FETCH_RESERVATIONS.FAIL)<Error>();
