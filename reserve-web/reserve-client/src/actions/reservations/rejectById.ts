/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { REJECT_RESERVATION } from './constants';
import * as mappers from './mapper';
import { ReservationDTO } from 'lib/types/dto/Reservation.dto';
//#endregion

export const request = createStandardAction(REJECT_RESERVATION.REQUEST)
  .map(
    (id: string) => ({
      meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: `api/v1/reservation/${id}/reject`,
      },
    }),
  );

export const success = createStandardAction(REJECT_RESERVATION.SUCCESS)
  .map((payload: ReservationDTO) => ({
    payload: mappers.decode(payload)
  }));

export const cancel = createAction(REJECT_RESERVATION.CANCEL);

export const fail = createStandardAction(REJECT_RESERVATION.FAIL)<Error>();
