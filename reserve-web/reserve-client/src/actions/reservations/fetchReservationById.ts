/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { FETCH_RESERVATION_BY_ID } from './constants';
import * as mappers from './mapper';
import { ReservationDTO } from 'lib/types/dto/Reservation.dto';
//#endregion

export const request = createStandardAction(FETCH_RESERVATION_BY_ID.REQUEST)
  .map(
    (id: string) => ({
      meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: `api/v1/reservation/${id}`,
        throttle: 200,
      },
    }),
  );

export const success = createStandardAction(FETCH_RESERVATION_BY_ID.SUCCESS)
  .map((payload: ReservationDTO) => ({
    payload: mappers.decode(payload)
  }));

export const cancel = createAction(FETCH_RESERVATION_BY_ID.CANCEL);

export const fail = createStandardAction(FETCH_RESERVATION_BY_ID.FAIL)<Error>();
