/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { ReservationDTO } from 'lib/types/dto/Reservation.dto';
import { CREATE_RESERVATION } from './constants';
import * as mapper from './mapper';
//#endregion

export interface Payload {
  readonly dateStart: Date;
  readonly dateEnd: Date;
  readonly resourceIds: number[];
  readonly allDay: boolean;
  readonly notes?: string;
  readonly userId?: string;
}

export const request = createStandardAction(CREATE_RESERVATION.REQUEST).map(
  ({ dateStart, dateEnd, resourceIds, notes, allDay, userId }: Payload) => ({
    payload: {
      date_start: dateStart.toISOString(),
      date_end: dateEnd.toISOString(),
      resource_ids: resourceIds,
      all_day: allDay,
      notes,
      user_id: userId,
    },
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/reservation',
    },
  }),
);

export const success = createStandardAction(CREATE_RESERVATION.SUCCESS).map(
  (dto: ReservationDTO) => ({
    payload: mapper.decode(dto),
  }));


export const cancel = createAction(CREATE_RESERVATION.CANCEL);

export const fail = createStandardAction(CREATE_RESERVATION.FAIL)<Error>();
