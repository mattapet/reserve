/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { Reservation } from 'lib/types/Reservation';
import { EDIT_RESERVATION } from './constants';
import { ReservationDTO } from 'lib/types/dto/Reservation.dto';
import * as mapper from './mapper';
//#endregion

export const request = createStandardAction(EDIT_RESERVATION.REQUEST).map(
  (reservation: Reservation) => ({
    payload: mapper.encode(reservation),
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/reservation/${reservation.id}`,
    },
  }),
);

export const success = createStandardAction(EDIT_RESERVATION.SUCCESS).map(
  (dto: ReservationDTO) => ({
    payload: mapper.decode(dto),
  }));

export const cancel = createAction(EDIT_RESERVATION.CANCEL);

export const fail = createStandardAction(EDIT_RESERVATION.FAIL)<Error>();
