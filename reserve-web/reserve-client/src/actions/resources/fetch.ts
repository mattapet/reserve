/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { ResourceDTO } from 'lib/types/dto/Resource.dto';
import { FETCH_RESOURCES } from './constants';
import * as mapper from './mapper';
//#endregion

export const request = createStandardAction(FETCH_RESOURCES.REQUEST).map(
  () => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/resource',
    },
  }),
);

export const success = createStandardAction(FETCH_RESOURCES.SUCCESS).map(
  (payload: ResourceDTO[]) => ({
    payload: payload.map(mapper.decode),
  }),
);

export const cancel = createAction(FETCH_RESOURCES.CANCEL);

export const fail = createStandardAction(FETCH_RESOURCES.FAIL)<Error>();
