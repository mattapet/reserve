/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { TOGGLE_RESOURCE } from './constants';
import { Resource } from 'lib/types/Resource';
//#endregion

export const request = createStandardAction(TOGGLE_RESOURCE.REQUEST).map(
  (id: number) => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: `api/v1/resource/${id}/toggle`,
    },
  }),
);

export const success =
  createStandardAction(TOGGLE_RESOURCE.SUCCESS)<Resource>();

export const cancel = createAction(TOGGLE_RESOURCE.CANCEL);

export const fail = createStandardAction(TOGGLE_RESOURCE.FAIL)<Error>();
