/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ResourceDTO } from 'lib/types/dto/Resource.dto';
import { Resource } from 'lib/types/Resource';
//#endregion

export function decode(dto: ResourceDTO): Resource {
  return { ...dto } as Resource;
}

export function encode(resource: Resource): ResourceDTO {
  return { ...resource } as ResourceDTO;
}
