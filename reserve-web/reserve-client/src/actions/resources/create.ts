/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { ResourceDTO } from 'lib/types/dto/Resource.dto';
import { CREATE_RESOURCE } from './constants';
//#endregion

export const request = createStandardAction(CREATE_RESOURCE.REQUEST).map(
  (name: string, description: string) => ({
    payload: { name, description },
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/resource',
    },
  }),
);

export const success =
  createStandardAction(CREATE_RESOURCE.SUCCESS)<ResourceDTO>();

export const cancel = createAction(CREATE_RESOURCE.CANCEL);

export const fail = createStandardAction(CREATE_RESOURCE.FAIL)<Error>();
