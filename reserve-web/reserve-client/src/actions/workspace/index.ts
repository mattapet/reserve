/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ActionType } from 'typesafe-actions';

import * as fetchByName from './fetchByName';
import * as remove from './remove';
//#endregion

export { fetchByName, remove };

//#region types
export type WorkspaceFetchByName =
    ActionType<typeof fetchByName.request>
  | ActionType<typeof fetchByName.success>
  | ActionType<typeof fetchByName.cancel>
  | ActionType<typeof fetchByName.fail>;

export type WorkspaceRemove =
    ActionType<typeof remove.request>
  | ActionType<typeof remove.success>
  | ActionType<typeof remove.cancel>
  | ActionType<typeof remove.fail>;

export type WorkspaceAction =
  | WorkspaceFetchByName
  | WorkspaceRemove;
//#endregion
