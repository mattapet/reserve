/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export const enum FETCH_BY_NAME {
  REQUEST = 'FETCH_BY_NAME_REQUEST',
  SUCCESS = 'FETCH_BY_NAME_SUCCESS',
  CANCEL = 'FETCH_BY_NAME_CANCEL',
  FAIL = 'FETCH_BY_NAME_FAIL',
}

export const enum REMOVE_WORKSPACE {
  REQUEST = 'REMOVE_WORKSPACE_REQUEST',
  SUCCESS = 'REMOVE_WORKSPACE_SUCCESS',
  CANCEL = 'REMOVE_WORKSPACE_CANCEL',
  FAIL = 'REMOVE_WORKSPACE_FAIL',
}
