/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { FETCH_USERS } from './constants';
import * as mapper from './mapper';
import { UserDTO } from 'lib/types/dto/User';
//#endregion

export const request = createStandardAction(FETCH_USERS.REQUEST).map(
  () => ({
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/user',
    },
  }),
);

export const success = createStandardAction(FETCH_USERS.SUCCESS).map(
  (payload: UserDTO[]) => ({
    payload: payload.map(mapper.decode),
  }));

export const cancel = createAction(FETCH_USERS.CANCEL);

export const fail = createStandardAction(FETCH_USERS.FAIL)<Error>();
