/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ActionType } from 'typesafe-actions';

import * as fetchById from './fetchById';
import * as fetch from './fetch';
import * as updateRole from './updateRole';
import * as toggleBan from './toggleBan';
import * as getMe from './getMe';
import * as updateProfile from './updateProfile';
import * as deleteMe from './deleteMe';
//#endregion

export {
  fetchById,
  fetch,
  updateRole,
  toggleBan,
  getMe,
  updateProfile,
  deleteMe,
};

//#region types
export type UserActionFetchById =
    ActionType<typeof fetchById.request>
  | ActionType<typeof fetchById.success>
  | ActionType<typeof fetchById.cancel>
  | ActionType<typeof fetchById.fail>;

export type UserActionFetch =
    ActionType<typeof fetch.request>
  | ActionType<typeof fetch.success>
  | ActionType<typeof fetch.cancel>
  | ActionType<typeof fetch.fail>;

export type UserActionUpdateRole =
    ActionType<typeof updateRole.request>
  | ActionType<typeof updateRole.success>
  | ActionType<typeof updateRole.cancel>
  | ActionType<typeof updateRole.fail>;

export type UserActionToggleBan =
    ActionType<typeof toggleBan.request>
  | ActionType<typeof toggleBan.success>
  | ActionType<typeof toggleBan.cancel>
  | ActionType<typeof toggleBan.fail>;

export type UserActionGetMe =
    ActionType<typeof getMe.request>
  | ActionType<typeof getMe.success>
  | ActionType<typeof getMe.cancel>
  | ActionType<typeof getMe.fail>;

export type UserActionUpdateProfile =
    ActionType<typeof updateProfile.request>
  | ActionType<typeof updateProfile.success>
  | ActionType<typeof updateProfile.cancel>
  | ActionType<typeof updateProfile.fail>;

export type UserActionDeleteMe =
    ActionType<typeof deleteMe.request>
  | ActionType<typeof deleteMe.success>
  | ActionType<typeof deleteMe.cancel>
  | ActionType<typeof deleteMe.fail>;

export type UserAction =
  | UserActionFetchById
  | UserActionFetch
  | UserActionUpdateRole
  | UserActionToggleBan
  | UserActionGetMe
  | UserActionUpdateProfile
  | UserActionDeleteMe;
//#endregion
