/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { TOGGLE_BAN } from './constants';
import * as mappers from './mapper';
import { UserDTO } from 'lib/types/dto/User';
//#endregion

export const request = createStandardAction(TOGGLE_BAN.REQUEST)
  .map(
  (id: string) => ({
    meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: `api/v1/user/${id}/ban`,
      },
    }),
  );

export const success = createStandardAction(TOGGLE_BAN.SUCCESS)
  .map((payload: UserDTO) => ({
    payload: mappers.decode(payload)
  }));

export const cancel = createAction(TOGGLE_BAN.CANCEL);

export const fail = createStandardAction(TOGGLE_BAN.FAIL)<Error>();
