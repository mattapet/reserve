/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { UPDATE_ROLE } from './constants';
import * as mappers from './mapper';
import { UserDTO } from 'lib/types/dto/User';
import { UserRole } from 'lib/types/UserRole';
//#endregion

export interface Payload {
  readonly id: string;
  readonly role: UserRole;
}

export const request = createStandardAction(UPDATE_ROLE.REQUEST)
  .map(
  ({ id, role }: Payload) => ({
    payload: { role },
    meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: `api/v1/user/${id}/role`,
      },
    }),
  );

export const success = createStandardAction(UPDATE_ROLE.SUCCESS)
  .map((payload: UserDTO) => ({
    payload: mappers.decode(payload)
  }));

export const cancel = createAction(UPDATE_ROLE.CANCEL);

export const fail = createStandardAction(UPDATE_ROLE.FAIL)<Error>();
