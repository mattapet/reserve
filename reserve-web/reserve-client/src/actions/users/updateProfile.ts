/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { UPDATE_PROFILE } from './constants';
import * as mappers from './mapper';
import { UserDTO } from 'lib/types/dto/User';
import { Profile } from 'lib/types/Profile';
//#endregion

export const request = createStandardAction(UPDATE_PROFILE.REQUEST)
  .map(
  (payload: Profile) => ({
    payload: {
      first_name: payload.firstName,
      last_name: payload.lastName,
      phone: payload.phone,
    },
    meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: 'api/v1/user/me/profile',
      },
    }),
  );

export const success = createStandardAction(UPDATE_PROFILE.SUCCESS)
  .map((payload: UserDTO) => ({
    payload: mappers.decode(payload)
  }));

export const cancel = createAction(UPDATE_PROFILE.CANCEL);

export const fail = createStandardAction(UPDATE_PROFILE.FAIL)<Error>();
