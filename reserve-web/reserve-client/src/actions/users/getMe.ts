/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { GET_ME } from './constants';
import * as mappers from './mapper';
import { UserDTO } from 'lib/types/dto/User';
//#endregion

export const request = createStandardAction(GET_ME.REQUEST)
  .map(
    () => ({
      meta: {
        onCancel: cancel,
        onFail: fail,
        onSuccess: success,
        url: 'api/v1/user/me',
      },
    }),
  );

export const success = createStandardAction(GET_ME.SUCCESS)
  .map((payload: UserDTO) => ({
    payload: mappers.decode(payload)
  }));

export const cancel = createAction(GET_ME.CANCEL);

export const fail = createStandardAction(GET_ME.FAIL)<Error>();
