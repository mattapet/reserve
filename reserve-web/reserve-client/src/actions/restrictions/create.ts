/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { createAction, createStandardAction } from 'typesafe-actions';
import { RestrictionDTO } from 'lib/types/dto/Restriction';
import { CREATE_RESTRICTION } from './constants';
import * as mapper from './mapper';
//#endregion

export interface Payload {
  readonly dateStart: Date;
  readonly dateEnd: Date;
  readonly resourceIds: number[];
  readonly description?: string;
}

export const request = createStandardAction(CREATE_RESTRICTION.REQUEST).map(
  ({ dateStart, dateEnd, resourceIds, description }: Payload) => ({
    payload: {
      date_start: dateStart.toISOString(),
      date_end: dateEnd.toISOString(),
      resource_ids: resourceIds,
      description,
    },
    meta: {
      onCancel: cancel,
      onFail: fail,
      onSuccess: success,
      url: 'api/v1/restriction',
    },
  }),
);

export const success = createStandardAction(CREATE_RESTRICTION.SUCCESS).map(
  (dto: RestrictionDTO) => ({
    payload: mapper.decode(dto),
  }));


export const cancel = createAction(CREATE_RESTRICTION.CANCEL);

export const fail = createStandardAction(CREATE_RESTRICTION.FAIL)<Error>();
