/*

* Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as authActions from './auth';
import * as authoritiesActions from './authorities';
import * as requestActions from './request';
import * as resourceActions from './resources';
import * as reservationActions from './reservations';
import * as restrictionActions from './restrictions';
import * as settingsActions from './settings';
import * as userActions from './users';
import * as webhookActions from './webhook';
import * as workspaceActions from './workspace';
//#endregion

export type RootAction =
  | authActions.AuthAction
  | authoritiesActions.AuthorityAction
  | requestActions.RequestAction
  | resourceActions.ResourceAction
  | reservationActions.ReservationAction
  | restrictionActions.RestrictionAction
  | settingsActions.SettingsAction
  | userActions.UserAction
  | webhookActions.WebhookAction
  | workspaceActions.WorkspaceAction;

export {
  authActions,
  authoritiesActions,
  requestActions,
  resourceActions,
  reservationActions,
  restrictionActions,
  settingsActions,
  userActions,
  webhookActions,
  workspaceActions,
};
