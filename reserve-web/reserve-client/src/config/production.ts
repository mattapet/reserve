/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Config } from 'lib/types/config';
//#endregion

const protocol = window.location.protocol;
const baseUrl = `${protocol}//${
  window.location.host.split('.').slice(1).join('.')
}/`;
const apiUrl = baseUrl;
const landingUrl = baseUrl;
const workspaceName = window.location.host.split('.')[0];
const workspaceUrl = `${window.location.origin}`;


export default {
  protocol,
  baseUrl,
  apiUrl,
  landingUrl,
  workspaceName,
  workspaceUrl,
} as Config;
