/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import styled from 'styled-components';
import { Layout } from 'antd';

import Header from './components/Header';
import Router from './router';
import { UserSettings } from 'lib/types/Settings';
//#endregion

//#region Component interfaces
export interface Props {
  readonly isLoggedIn: boolean;
  readonly userSettings?: UserSettings;
}
//#endregion

const Container = styled(Layout)`
  height: 100%;
`;

const LoggedOut: React.FunctionComponent<Props> = (props) => {
  if (!props.userSettings) {
    throw new Error();
  }
  return (
    <Container>
      <Header />
      <Router userSettings={props.userSettings} />
    </Container>
  );
};


export default LoggedOut;
