/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as authActions from '../../../actions/auth';
import { RootState } from 'reducers';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  workspaceId: state.workspace!.id,
  meta: state.view.login,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onConfirmEmail: (
    workspaceId: string,
    email: string,
    expiry: string,
    signature: string,
  ) =>
    dispatch(authActions.confirmEmail
      .request({ workspaceId, email, expiry, signature })),
});
