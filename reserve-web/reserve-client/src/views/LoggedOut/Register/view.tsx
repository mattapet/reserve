/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import { Button, Card, Form, Layout } from 'antd';
import { Link } from 'react-router-dom';

import Success from './components/Success';
import Input from 'components/Input';
//#endregion

//#region Component interfaces
export interface Props {
  readonly pending: boolean;
  readonly retrieved: boolean;
  readonly email: string;
  readonly error?: Error;
  readonly onEmailChange: (e: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
}
//#endregion

//#region Styled
const FormContainer = styled(Form as any)`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-content: center;

  * {
    margin: 5px;
  }
`;

const ContentWrapper = styled(Layout)`
  height: 100%;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const SubmitButton = styled(Button as any)`
  width: 100%;
`;
//#endregion

const Container: React.FunctionComponent<{}> = (props) => (
  <ContentWrapper>
    <Card title="Register">
      {props.children}
    </Card>
  </ContentWrapper>
);

const View: React.FunctionComponent<Props> = (props) => {
  if (props.retrieved && !props.error) {
    return (
      <Container>
        <Success email={props.email} />
      </Container>
    );
  }

  return (
    <Container>
      <FormContainer onSubmit={props.onSubmit}>
        <Input
          type="email"
          placeholder="Email"
          value={props.email}
          onChange={props.onEmailChange}
          autoFocus
          required
        />
        <SubmitButton type="primary" htmlType="submit" loading={props.pending}>
          Register
        </SubmitButton>
        <Button>
          <Link to="/login">
            Login
          </Link>
        </Button>
      </FormContainer>
    </Container>
  );
};

export default View;
