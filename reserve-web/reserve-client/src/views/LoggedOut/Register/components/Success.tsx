/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Alert } from 'antd';
//#endregion

//#region Component interfaces
export interface Props {
  readonly email: string;
}
//#endregion

const Success: React.FunctionComponent<Props> = (props) => (
  <Alert
    type="success"
    message={`Verificaiton email sent to ${props.email}`}
  />
);

export default Success;
