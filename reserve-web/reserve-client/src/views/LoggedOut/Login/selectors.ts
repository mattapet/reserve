/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as authActions from '../../../actions/auth';
import { RootState } from '../../../reducers';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  workpaceId: state.workspace!.id,
  userSettings: state.settings.fetched && state.settings.userSettings,
  auth: state.auth,
  meta: state.view.login,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onLogin: (email: string, password: string) =>
    dispatch(authActions.login.request({ email, password })),
  onLoginCancel: () =>
    dispatch(authActions.login.cancel()),
});
