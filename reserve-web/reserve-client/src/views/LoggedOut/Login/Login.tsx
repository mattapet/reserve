/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState, useEffect} from 'react';

import { UserSettings } from 'lib/types/Settings';
import { AuthState } from '../../../reducers/auth';
import { LoginMeta } from '../../../reducers/view/login';
import View from './view';
//#endregion

//#region Component interfaces
export interface Props {
  readonly workspaceId: string;
  readonly userSettings?: UserSettings;
  readonly auth: AuthState;

  readonly meta: LoginMeta;
  readonly onLogin: (email: string, password: string) => any;
  readonly onLoginCancel: () => any;
}
//#endregion

const Login: React.FunctionComponent<Props> = (props) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => () => props.onLoginCancel(), [props.onLoginCancel]);

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    props.onLogin(email, password);
  }

  return (
    <View
      email={email}
      password={password}
      workspaceId={props.workspaceId}
      register={props.userSettings && props.userSettings.allowedRegistrations}
      pending={props.meta.pending}
      onEmailChange={setEmail}
      onPasswordChange={setPassword}
      loginAuthorities={props.auth.loginAuthorities}
      onSubmit={handleSubmit}
    />
  );
};

export default Login;
