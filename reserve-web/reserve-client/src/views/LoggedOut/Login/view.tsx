/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import styled from 'styled-components';
import { Row, Button, Card, Form, Layout } from 'antd';
import { Link } from 'react-router-dom';

import config from 'config';
import Input from 'components/Input';
//#endregion

//#region Component interfaces
interface LoginViewProps {
  readonly register?: boolean;
  readonly pending: boolean;
  readonly email: string;
  readonly password: string;
  readonly loginAuthorities: string[];
  readonly workspaceId: string;
  readonly onEmailChange: (e: string) => any;
  readonly onPasswordChange: (e: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
}
//#endregion

//#region Styled
const FormContainer = styled(Form)`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-content: center;

  * {
    margin: 5px;
  }
`;

const ContentWrapper = styled(Layout)`
  height: 100%;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const SubmitButton = styled(Button)`
  width: 100%;
`;
//#endregion

const Container: React.FunctionComponent<{}> = (props) => (
  <ContentWrapper>
    <Card title="Login">
      {props.children}
    </Card>
  </ContentWrapper>
);

const View: React.FunctionComponent<LoginViewProps> = (props) => (
  <Container>
    <FormContainer onSubmit={props.onSubmit}>
      <Input
        type="email"
        placeholder="Email"
        value={props.email}
        onChange={props.onEmailChange}
        autoFocus
      />
      <Input
        type="password"
        placeholder="Password"
        value={props.password}
        onChange={props.onPasswordChange}
      />
      <SubmitButton type="primary" htmlType="submit" loading={props.pending}>
        Login
      </SubmitButton>
      <Row>
        {props.register ?
          <Button>
            <Link to="/register">
              Register
            </Link>
          </Button> : null}
        {props.loginAuthorities.map(authority => (
          <Button>
            <a href={
              `${config.apiUrl}api/v1/${props.workspaceId}/login/${authority}`
            }>Login with {authority}</a>
          </Button>
        ))}
      </Row>
    </FormContainer>
  </Container>
);

export default View;
