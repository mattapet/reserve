/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import LoggedOut from './LoggedOut';
import { connect } from 'react-redux';

import { mapStateToProps } from './selectors';
//#endregion

export { Props } from './LoggedOut';
export { default as Router } from './router';

export default connect(
  mapStateToProps,
)(LoggedOut);
