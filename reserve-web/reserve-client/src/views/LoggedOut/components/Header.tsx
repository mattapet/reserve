/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Button, Layout, Menu, Dropdown } from 'antd';
import {
  RouteComponentProps,
  withRouter,
} from 'react-router-dom';
import styled from 'styled-components';
import config from 'config';
//#endregion

const LayoutHeader = styled(Layout.Header)`
  display: flex;
  justify-content: space-between;
`;


const Title = styled.h1`
  font-size: 36px;
  color: whitesmoke;
  display: inline-block;
  margin: 0;
  cursor: pointer;

  transition: all .3s;

  &:hover {
    color: #40a9ff;
  }
`;

const MenuBar = styled(Menu)`
  float: left;
`;

const WorkspacePopover = styled(Dropdown)`
  justify-self: center;
  align-self: center;
`;

const WorkpsaceOptions: React.FunctionComponent<RouteComponentProps<any>> =
  (props) => (
  <Menu
    mode="vertical"
    selectedKeys={[props.location.pathname]}
  >
    <Menu.Item key="/workspace/login">
      <a href={`${config.landingUrl}workspace/login`}>
        Login to Existing Workspace
      </a>
    </Menu.Item>
    <Menu.Item key="/workspace/create">
      <a href={`${config.landingUrl}workspace/create`}>
        Create New Workspace
      </a>
    </Menu.Item>
  </Menu>
);

const Header: React.FunctionComponent<RouteComponentProps<any>> = (props) => (
  <LayoutHeader>
    <MenuBar
      theme="dark"
      mode="horizontal"
      selectedKeys={[props.location.pathname]}
    >
    <Menu.Item>
      <a href={`${config.landingUrl}`}>
        <Title>
          Reserve
        </Title>
      </a>
    </Menu.Item>

      <Menu.Item key="/about">
        <a href={`${config.landingUrl}about`}>
          About Reserve
        </a>
      </Menu.Item>
      <Menu.Item key="/docs">
        <a href={`${config.landingUrl}docs`}>
          Documentation
        </a>
      </Menu.Item>
    </MenuBar>

    <WorkspacePopover
      overlay={<WorkpsaceOptions {...props} />}
      trigger={["click"]}
      placement="bottomRight"
    >
      <Button icon="user" ghost={true}>
        Your Workspaces
      </Button>
    </WorkspacePopover>
  </LayoutHeader>
);

export default withRouter(Header);
