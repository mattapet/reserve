/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect } from 'react';
import * as querystring from 'querystring';
import { RouteComponentProps } from 'react-router-dom';
import Spinner from 'components/Spinner';

import { LoginMeta } from 'reducers/view/login';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly workspaceId: string;
  readonly meta: LoginMeta;
  readonly onLoginAuthority: (
    workspaceId: string,
    name: string,
    code: string,
  ) => any;
}
//#endregion

const Auth: React.FunctionComponent<Props> = (props) => {
  useEffect(() => {
    const authority = (props.match.params as any).authority;
    const query = props.location.search.slice(1);
    const { code } = querystring.parse(query);
    if (!code) {
      return props.history.replace('/');
    }
    props.onLoginAuthority(props.workspaceId, authority, code as string);
  }, []);

  useEffect(() => {
    if (props.meta.error) {
      props.history.replace('/');
    }
  }, [props.meta.error]);

  return <Spinner />;
};

export default Auth;
