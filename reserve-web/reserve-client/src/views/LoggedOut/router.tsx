/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import { UserSettings } from 'lib/types/Settings';
import Spinner from 'components/Spinner';
const Login = React.lazy(() => import('./Login'));
const Logout = React.lazy(() => import('./Logout'));
const Register = React.lazy(() => import('./Register'));
const Auth = React.lazy(() => import('./Auth'));
const ConfirmEmail = React.lazy(() => import('./ConfirmEmail'));
//#endregion

export interface Props {
  readonly userSettings: UserSettings;
}

const Router: React.FunctionComponent<Props> = (props) => (
  <React.Suspense fallback={<Spinner />}>
  {
    props.userSettings.allowedRegistrations ?
      <Switch>
        <Route path="/" component={Login} exact />
        <Route path="/login" component={Login} exact />
        <Route path="/logout" component={Logout} exact />
        <Route path="/register" component={Register} exact />
        <Route path="/register/confirm" component={ConfirmEmail} exact />
        <Route path="/auth/:authority/callback" component={Auth} exact />
        <Redirect to="/" />
      </Switch> :
      <Switch>
        <Route path="/" component={Login} exact />
        <Route path="/login" component={Login} exact />
        <Route path="/logout" component={Logout} exact />
        <Route path="/auth/:authority/callback" component={Auth} exact />
        <Redirect to="/" />
      </Switch>
  }
  </React.Suspense>
);

export default Router;
