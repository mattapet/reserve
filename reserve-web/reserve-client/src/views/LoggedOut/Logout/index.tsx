/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Button, Card, Layout } from 'antd';
import * as React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
//#endregion

//#region Styled
const ContentWrapper = styled(Layout)`
  height: 100%;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const SubmitButton = styled(Button as any)`
  width: 100%;
`;
//#endregion

const Logout: React.FunctionComponent = () => (
  <ContentWrapper>
    <Card title="Want to log back in?">
      <NavLink to="/">
        <SubmitButton type="primary">
          Login
        </SubmitButton>
      </NavLink>
    </Card>
  </ContentWrapper>
);

export default Logout;
