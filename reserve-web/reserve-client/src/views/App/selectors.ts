/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { RootState } from 'src/reducers';
import { Dispatch } from 'redux';
import { workspaceActions, userActions } from 'actions';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  isLoggedIn: state.auth.isLoggedIn,
  retrieved:
    state.settings.fetched
    && state.auth.authoritiesFetched
    && state.view.user.getMe.retrieved,
});

export const mapDisaptchToProps = (dispatch: Dispatch) => ({
  onFetchAppMeta: () => {
    dispatch(userActions.getMe.request()),
    dispatch(workspaceActions.fetchByName.request());
  },
});
