/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router';

const LoggedIn = React.lazy(() => import('../LoggedIn'));
const LoggedOut = React.lazy(() => import('../LoggedOut'));
import Spinner from 'components/Spinner';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly isLoggedIn: boolean;
  readonly retrieved: boolean;
  readonly onFetchAppMeta: () => any;
}
//#endregion

const App: React.FunctionComponent<Props> = (props) => {
  useEffect(() => {
    props.onFetchAppMeta();
  }, []);

  if (!props.retrieved) {
    return <Spinner />;
  }

  return (
    <React.Suspense fallback={<Spinner />}>
      {props.isLoggedIn ? <LoggedIn /> : <LoggedOut />}
    </React.Suspense>
  );
};

export default App;
