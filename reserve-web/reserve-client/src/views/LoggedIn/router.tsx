/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Spinner from 'components/Spinner';
const Logout = React.lazy(() => import('./Logout'));
const Profile = React.lazy(() => import('./Profile'));
const Settings = React.lazy(() => import('./Settings'));
const Workspace = React.lazy(() => import('./Workspace'));
//#endregion

const Router: React.FunctionComponent = () => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      <Redirect
        key="/auth/:authority/callback"
        from="/auth/:authority/callback"
        to="/"
      />
      <Redirect
        key="/register/confirm"
        from="/register/confirm"
        to="/profile"
      />
      <Route key="/" path="/" component={Workspace} exact />
      <Route key="/profile" path="/profile" component={Profile} exact />
      <Route
        key="/profile/settings"
        path="/profile/settings"
        component={Settings}
      />
      <Route key="/logout" path="/logout" component={Logout} />
      <Route component={Workspace} />
    </Switch>
  </React.Suspense>
);

export default Router;
