/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Layout } from 'antd';
import styled from 'styled-components';

import { UserRole } from 'lib/types/UserRole';
import Sidebar from './components/Sidebar';
import Router from './router';
//#endregion

//#region Component interfaces
export interface Props {
  readonly role: UserRole;
}
//#endregion

//#region Styled
const Container = styled(Layout)`
  height: 100%;
`;

const Content = styled(Layout.Content)`
  height: 100%;
`;
//#endregion

const Workspace: React.FunctionComponent<Props> = (props) => (
  <Container>
    <Sidebar role={props.role} />
    <Content>
      <Router role={props.role} />
    </Content>
  </Container>
);

export default Workspace;
