/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Badge } from 'antd';
import { ReservationState } from 'lib/types/ReservationState';
import { Resource } from 'lib/types/Resource';
import { resourceEntityProvider, EntityProviderProps } from 'providers';
//#endregion

function badgeStyleForReservationState(
  state: ReservationState
): 'default' | 'success' | 'warning' | 'error' | 'processing' {
  switch (state) {
  case ReservationState.requested: return 'default';
  case ReservationState.confirmed: return 'success';
  case ReservationState.canceled: return 'warning';
  case ReservationState.rejected: return 'error';
  case ReservationState.completed: return 'default';
  }
}

export interface Props extends EntityProviderProps<Resource> {
  readonly state: ReservationState;
}

const EventBadge: React.FunctionComponent<Props> = ({ id, entity, state }) => (
  <li>
    <Badge
      status={badgeStyleForReservationState(state)}
      text={entity ? entity.name : `${id}`}
    />
  </li>
);

export default resourceEntityProvider<Props>(EventBadge);
