/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import styled from 'styled-components';
import { Calendar } from 'antd';
import * as moment from 'moment';
import { Reservation } from 'lib/types/Reservation';
import Event from '../Event';
//#endregion

//#region Component interfaces
export interface Props {
  readonly reservations: Reservation[];
}
//#endregion

//#region Styled
const EventList = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
`;
//#endregion

function dateCellRender(date: moment.Moment, reservations: Reservation[]) {
  return (
    <EventList>
      {reservations.filter(reservation =>
        reservation.dateStart.valueOf() <= moment(date).add(1, 'day').valueOf()
          && reservation.dateEnd.valueOf() >= date.valueOf()
        ).map(reservation => (
          <Event key={`${reservation.id}`} event={reservation} />
        ))}
    </EventList>
  );
}

function monthCellRender(date: moment.Moment, reservations: Reservation[]) {
  const startDate = moment(date).startOf('month');
  const endDate = moment(date).startOf('month').add(1, 'month');
  return (
    <span>
      Resrevations:{' '}
      {reservations.filter(reservation =>
        reservation.dateStart.valueOf() <= endDate.valueOf()
          && reservation.dateEnd.valueOf() >= startDate.valueOf()
        ).length}
    </span>
  );
}

const View: React.FunctionComponent<Props> = (props) => {
  return (
    <Calendar
      monthCellRender={(date: moment.Moment) =>
        monthCellRender(date, props.reservations)}
      dateCellRender={(date: moment.Moment) =>
        dateCellRender(date, props.reservations)}
    />
  );
};

export default View;
