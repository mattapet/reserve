/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';

import View from './view';
import { Reservation } from 'lib/types/Reservation';
//#endregion

export interface Props {
  readonly reservations: { readonly [id: number]: Reservation };
}

class CalendarController extends React.Component<Props> {
  public render() {
    return (
      <View reservations={Object.values(this.props.reservations)} />
    );
  }
}

export default CalendarController;
