/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Reservation } from 'lib/types/Reservation';
import ResourceBadge from './ResourceBadge';
//#endregion

const Event: React.FunctionComponent<{ event: Reservation }> = (props) => (
  <React.Fragment>
    {props.event.resourceIds.map(id => (
      <ResourceBadge id={id} state={props.event.state} />
    ))}
  </React.Fragment>
);

export default Event;
