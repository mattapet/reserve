/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Timeline } from 'antd';

import { Reservation } from 'lib/types/Reservation';
import ResourceTag from 'components/ResourceTag';
import { timestamp } from 'lib/dateFormatter';
import { ReservationState } from 'lib/types/ReservationState';
//#endregion

export interface Props {
  readonly reservations: Reservation[];
  readonly date: Date;
}

function colorForReservationState(
  state: ReservationState
): 'green' | 'blue' | 'red' {
  switch (state) {
  case ReservationState.confirmed:
  case ReservationState.completed:
    return 'green';
  case ReservationState.rejected:
  case ReservationState.canceled:
    return 'red';
  default:
    return 'blue';
  }
}

const TimelineView: React.FunctionComponent<Props> = (props) => (
  <Timeline mode="alternate">
  {props.reservations
    .filter(reservation =>
      reservation.dateStart.valueOf() > props.date.valueOf())
    .map(reservation => (
      <Timeline.Item
        key={reservation.id}
        color={colorForReservationState(reservation.state)}
      >
        <span>
          {timestamp(reservation.dateStart)}
          {reservation.resourceIds.map(id => <ResourceTag key={id} id={id}/>)}
        </span>
      </Timeline.Item>
    ))
  }
  </Timeline>
);

export default TimelineView;
