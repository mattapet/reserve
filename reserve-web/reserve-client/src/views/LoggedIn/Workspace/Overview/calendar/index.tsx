/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import * as Data from 'components/Data';
import {
  mapDispatchToProps,
  mapStateToProps,
} from './selectors';
import { Meta } from 'lib/types/Meta';
import { Reservation } from 'lib/types/Reservation';
import Calendar from '../components/calendar';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps<any> {
  readonly reservations: { [id: number]: Reservation };
  readonly meta: Meta;
  readonly onFetch: () => any;
  readonly onFetchCancel: () => any;
}
//#endregion

const OverviewController = (props: Props) => (
  <Data.Container<Reservation, Meta>
    data={props.reservations}
    meta={props.meta}
    onFetch={props.onFetch}
  >
    <Calendar reservations={props.reservations} />
  </Data.Container>
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(OverviewController));
