/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import * as moment from 'moment';
import styled from 'styled-components';
import { DatePicker, Layout } from 'antd';
import Timeline from '../components/timeline';
import { Reservation } from 'lib/types/Reservation';
//#endregion

export interface Props {
  readonly date: Date;
  readonly reservations: Reservation[];
  readonly onDateChange: (date: Date) => any;
}

const Content = styled(Layout.Content)`
  width: 100%;
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const View: React.FunctionComponent<Props> = (props) => (
  <Layout>
    <Layout.Header>
      <DatePicker
        value={moment(props.date).utc()}
        onChange={(_, date) => props.onDateChange(new Date(date))}
        allowClear={false}
      />
    </Layout.Header>
    <Content>
      <Timeline date={props.date} reservations={props.reservations} />
    </Content>
  </Layout>
);

export default View;
