/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';

import {
  CollectionProviderProps,
  reservationCollectionProvider,
} from 'providers';
import View from './view';
import { Reservation } from 'lib/types/Reservation';
//#endregion

//#region Component interfaces
export interface Props extends CollectionProviderProps<Reservation> {
}

interface State {
  readonly date: Date;
}
//#endregion

class CalendarController extends React.Component<Props, State> {
  public state = {
    date: new Date(),
  };

  public render() {
    return (
      <View
        date={this.state.date}
        onDateChange={this.handleDateChange}
        reservations={Object.values(this.props.data)}
      />
    );
  }

  private handleDateChange = (date: Date) => {
    this.setState({ date });
  };
}

export default reservationCollectionProvider<Props>(CalendarController);
