/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import { ReservationSettings } from 'lib/types/Settings';
import NotFound from 'views/NotFound';
import Calendar from './calendar';
import Timeline from './timeline';
import { TimeUnit } from 'lib/types/TimeUnit';
import { mapStateToProps } from './selectors';
//#endregion

export interface Props {
  readonly settings: ReservationSettings;
}

const Router: React.FunctionComponent<Props> = (props) => (
  <Switch>
    <Route path="/overview" component={
      props.settings.timeUnit === TimeUnit.day ?
        Calendar : Timeline
      } exact />
    <Route component={NotFound} />
  </Switch>
);

export default connect(mapStateToProps)(Router);
