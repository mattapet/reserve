/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as resouceActions from 'actions/resources';
import { RootState } from 'reducers';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  meta: state.view.resource.create,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onSubmit: (name: string, description: string) =>
    dispatch(resouceActions.create.request(name, description)),
  onCancel: () => dispatch(resouceActions.create.cancel()),
});
