/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';

import View from './view';
import { Resource } from 'lib/types/Resource';
//#endregion

//#region Component interfaces
export interface Props {
  readonly defaultValue?: Resource;
  readonly onSubmit: (name: string, description: string) => any;
  readonly onCancel: () => any;
}

interface State {
  readonly name: string;
  readonly description: string;
}
//#endregion

class FormController extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    const { defaultValue } = props;
    this.state = {
      name: defaultValue && defaultValue.name || '',
      description: defaultValue && defaultValue.description || '',
    };
  }

  public render() {
    return (
      <View
        {...this.state}
        onCancel={this.props.onCancel}
        onSubmit={this.handleSubmit}
        onNameChange={this.handleNameChange}
        onDescriptionChange={this.handleDescriptionChange}
      />
    );
  }

  private handleNameChange = (e: React.FormEvent<HTMLInputElement>) => {
    const name = e.currentTarget.value;
    this.setState({ name });
  }

  private handleDescriptionChange = (e: React.FormEvent<HTMLInputElement>) => {
    const description = e.currentTarget.value;
    this.setState({ description });
  }

  private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { name, description } = this.state;
    this.props.onSubmit(name, description);
  }
}

export default FormController;
