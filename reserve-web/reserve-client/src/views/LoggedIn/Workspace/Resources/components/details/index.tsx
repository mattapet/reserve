/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Row, Col, Button } from 'antd';
import { withRouter, RouteComponentProps } from 'react-router';

import { Resource } from 'lib/types/Resource';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly entity: Resource;
}
//#endregion

//#region Styled
const LabelText = styled.span`
  padding-right: 10px;
  font-weight: 700;
  float: right;
`;
//#endregion

const Label: React.FunctionComponent = ({ children }) => (
  <Col span={3}>
    <LabelText>{ children }</LabelText>
  </Col>
);
const Text: React.FunctionComponent = ({ children }) => (
  <Col span={9}>
    <span>{ children }</span>
  </Col>
);

const ReservationDetails: React.FunctionComponent<Props> = (props) => {
  return (
    <React.Fragment>
      <Row>
        <Label>Name:</Label>
        <Text>{ props.entity.name }</Text>
      </Row>
      <Row>
        <Label>Description:</Label>
        <Text>{ props.entity.description }</Text>
      </Row>
      <Row>
        <Button onClick={() => props.history.goBack()}>
          Back
        </Button>
        <Button>
          <Link to={`/resources/${props.entity.id}/edit`}>
            Edit
          </Link>
        </Button>
      </Row>
    </React.Fragment>
  );
};

export default withRouter(ReservationDetails);
