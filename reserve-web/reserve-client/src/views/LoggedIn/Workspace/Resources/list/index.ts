/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import ResourceList from './ResourceList';
import { mapStateToProps, mapDispatchToProps } from './selectors';
//#endregion

export { Props } from './ResourceList';

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(ResourceList));
