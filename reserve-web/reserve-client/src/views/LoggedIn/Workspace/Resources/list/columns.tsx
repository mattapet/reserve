/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Link } from 'react-router-dom';
import { Popconfirm, Badge, Divider } from 'antd';
import { ColumnProps } from 'antd/lib/table/interface';

import { Resource } from 'lib/types/Resource';
//#endregion

export const columns = (
  onRemove: (id: number) => any,
  onToggle: (id: number) => any,
) =>  ([
  {
    key: 'id',
    title: 'ID',
    render: (_, { id }) =>
      <Link to={`/resources/${id}/details`}>{ id }</Link>,
  },
  {
    dataIndex: 'name',
    key: 'name',
    title: 'Name',
  },
  {
    dataIndex: 'description',
    key: 'description',
    title: 'Description',
  },
  {
    key: 'status',
    title: 'Status',
    render: (_, { active }) =>
      active
        ? <Badge status="success" text="Active"/>
        : <Badge status="error" text="Inactive"/>
  },
  {
    key: 'remove',
    title: 'Remove',
    render: (_, item) =>
      <React.Fragment>
        <Popconfirm
          title="Are you sure?"
          onConfirm={() => onToggle(item.id)}
        >
          <a href="javascript:;">{
            item.active ? 'Deactive' : 'Activate'
          }</a>
        </Popconfirm>
        <Divider type="vertical" />
        <Popconfirm
          title="Are you sure?"
          okText="Yes"
          okType="danger"
          onConfirm={() => onRemove(item.id)}
        >
          <a href="javascript:;">Remove</a>
        </Popconfirm>
      </React.Fragment>
  },
] as ColumnProps<Resource>[]);
