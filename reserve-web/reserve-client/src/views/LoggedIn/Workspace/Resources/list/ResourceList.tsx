/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';

import { Resource } from 'lib/types/Resource';
import Table from 'components/Table';
import { columns } from './columns';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps<any> {
  readonly data: { readonly [id: number]: Resource; };
  readonly meta: {
    readonly retrieved: boolean;
    readonly pending: boolean;
    readonly error?: Error;
  };
  readonly onFetch: () => any;
  readonly onRemove: (id: number) => any;
  readonly onToggle: (id: number) => any;
}
//#endregion

const ResourceList: React.FunctionComponent<Props> = (props) => {
  useEffect(() => {
    if (!props.meta.retrieved) {
      props.onFetch();
    }
  }, [props.onFetch, props.meta.retrieved]);

  return (
    <Table<Resource>
      pageTitle="Resources"
      rowKey={(entity, _) => `${entity.id}`}
      columns={columns(props.onRemove, props.onToggle)}
      onCreate={() => props.history.push('/resources/create')}
      dataSource={Object.values(props.data)}
      loading={props.meta.pending || !props.meta.retrieved}
    />
  );
};

export default ResourceList;
