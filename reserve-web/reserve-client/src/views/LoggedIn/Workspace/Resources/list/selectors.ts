/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import { RootState } from 'reducers';
import * as resouceActions from 'actions/resources';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  ...state.resource,
  meta: {
    ...state.view.resource.fetch,
    pending: state.view.resource.fetch.pending
      || state.view.resource.edit.pending
      || state.view.resource.remove.pending,
  }
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onFetch: () => dispatch(resouceActions.fetch.request()),
  onRemove: (id: number) => dispatch(resouceActions.remove.request(id)),
  onToggle: (id: number) => dispatch(resouceActions.toggle.request(id)),
});
