/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { Resource } from 'lib/types/Resource';
import {
  resourceEntityProvider,
  idProvider,
  IdProviderProps,
  EntityProviderProps,
} from 'providers';
import Spinner from 'components/Spinner';
import Details from '../components/details';
//#endregion

//#region Component interfaces
export interface Props extends
  IdProviderProps,
  EntityProviderProps<Resource>,
  RouteComponentProps {
  readonly onSubmit: (value: Resource) => any;
}
//#endregion

const EditResource: React.FunctionComponent<Props> = (props) => (
  props.entity ? <Details entity={props.entity} /> : <Spinner />
);

export default idProvider(withRouter(resourceEntityProvider<Props>(
  EditResource
)));
