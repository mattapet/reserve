/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Route, Switch } from 'react-router-dom';

import List from './list';
import Details from './details';
import Reservations from './reservations';
import NotFound from 'views/NotFound';
//#endregion

const Router: React.FunctionComponent<any> = () => (
  <Switch>
    <Route path="/users" component={List} exact />
    <Route path="/users/:id/details" component={Details} exact />
    <Route path="/users/:id/reservations" component={Reservations} exact />
    <Route component={NotFound} />
  </Switch>
);

export default Router;
