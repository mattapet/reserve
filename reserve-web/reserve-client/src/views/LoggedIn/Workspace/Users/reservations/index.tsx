/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { List } from 'components/Data';
import { mapDispatchToProps } from './selectors';
import {
  reservationCollectionProvider as provider,
  CollectionProviderProps,
} from 'providers';
import UserNameLink from 'components/UserNameLink';

import { Reservation } from 'lib/types/Reservation';
import ResourceTag from 'components/ResourceTag';
import { columns } from './columns';
//#endregion

//#region Component interfaces
export interface Props extends
  CollectionProviderProps<Reservation>,
  RouteComponentProps<any> {
  readonly onConfirm: (id: string) => any;
  readonly onCancel: (id: string) => any;
  readonly onReject: (id: string) => any;
}
//#endregion

const ReservationList: React.FunctionComponent<Props> = (props) => (
  <List<Reservation>
    title={
      <span>
        <UserNameLink id={props.match.params.id} />'s Reservations
      </span>
    }
    data={Object.values(props.data).filter(reservation => (
      reservation.userId === props.match.params.id
    ))}
    pending={props.meta.pending || !props.meta.retrieved}
    extractKey={(entity, _) => `${entity.id}`}
    columns={columns(props.onConfirm, props.onCancel, props.onReject)}
    expandedRowRender={reservation => (
      <React.Fragment>
        <b>Resources: </b>
        {reservation.resourceIds.map(id =>
          <ResourceTag key={`${id}`} id={id} />
        )}
      </React.Fragment>
    )}
  />
);

export default connect(
  null,
  mapDispatchToProps,
)(withRouter(provider<Props>(ReservationList)));
