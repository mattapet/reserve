/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from 'actions/users';
import { RootState } from 'reducers';
import { UserRole } from 'lib/types/UserRole';
//#endregion

export const mapStateToProps = (state: RootState) => {
  if (!state.auth.isLoggedIn) {
    throw new Error('Invalid state');
  }
  return {
    identity: state.auth.identity,
  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onRoleChange: (id: string, role: UserRole) =>
    dispatch(actions.updateRole.request({ id, role })),
  onBanToggle: (id: string) => dispatch(actions.toggleBan.request(id)),
});
