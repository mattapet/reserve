/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import styled from 'styled-components';
import { Row, Col, Button } from 'antd';
import { withRouter, RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';

import { User } from 'lib/types/User';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly users: { readonly [id: number]: User };
}
//#endregion

//#region Styled
const LabelText = styled.span`
  padding-right: 10px;
  font-weight: 700;
  float: right;
`;
//#endregion

const Label: React.FunctionComponent = ({ children }) => (
  <Col span={3}>
    <LabelText>{ children }</LabelText>
  </Col>
);
const Text: React.FunctionComponent = ({ children }) => (
  <Col span={9}>
    <span>{ children }</span>
  </Col>
);

const UserDetails: React.FunctionComponent<Props> = (props) => {
  const user = props.users[(props.match.params as any).id];
  if (!user || !user.profile || !user.email) {
    return null;
  }

  return (
    <React.Fragment>
      <Row>
        <Label>Name:</Label>
        <Text>{ `${user.profile.firstName} ${user.profile.lastName}` }</Text>
      </Row>
      <Row>
        <Label>Email:</Label>
        <Text>{ `${user.email}` }</Text>
      </Row>
      <Row>
        <Label>Phone:</Label>
        <Text>{ `${user.profile.phone}` }</Text>
      </Row>
      <Row>
        <Label>Role:</Label>
        <Text>{ `${user.role}` }</Text>
      </Row>
      <Row>
        <Label />
        <Text>
          <Link to={`/users/${user.id}/reservations`}>
            Reservations
          </Link>
        </Text>
      </Row>
      <Row>
        <Button onClick={() => props.history.goBack()}>
          Back
        </Button>
      </Row>
    </React.Fragment>
  );
};

export default withRouter(UserDetails);
