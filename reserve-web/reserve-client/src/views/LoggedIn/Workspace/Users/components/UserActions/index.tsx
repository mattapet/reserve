/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Divider, Popconfirm, Menu, Icon } from 'antd';

import { User } from 'lib/types/User';
import { UserRole } from 'lib/types/UserRole';
import { Identity } from 'reducers/auth';
import { mapStateToProps, mapDispatchToProps } from './selectors';
//#endregion

export interface Props {
  readonly identity: Identity;
  readonly onRoleChange: (id: string, role: UserRole) => any;
  readonly onBanToggle: (id: string) => any;
  readonly item: User;
}

interface RolesDropdownProps {
  readonly item: User;
  readonly onSelect: (id: string, role: UserRole) => any;
  readonly userRole: UserRole;
}

function roleValue(role: UserRole): number {
  switch (role) {
  case UserRole.user: return 1;
  case UserRole.maintainer: return 2;
  case UserRole.owner: return 3;
  }
}

const RolesDropdown: React.FunctionComponent<RolesDropdownProps> = (props) => (
  <Menu selectedKeys={[props.item.role]}>
    {[UserRole.user, UserRole.maintainer, UserRole.owner]
      .filter(role =>
        roleValue(role) <= roleValue(props.userRole))
      .map(role => (
        <Menu.Item
          key={role}
          onClick={() => props.onSelect(props.item.id, role)}
        >
          { role.toUpperCase() }
        </Menu.Item>
      ))}
  </Menu>
);

const UserActions: React.FunctionComponent<Props> = (props) => (
  props.item.id === props.identity.id
    || roleValue(props.identity.role) < roleValue(props.item.role) ? null :
  <React.Fragment>
    <Dropdown
      overlay={
        <RolesDropdown
          item={props.item}
          userRole={props.identity.role}
          onSelect={props.onRoleChange}
        />
      }
      trigger={[ 'click' ]}
    >
      <a href="javascript:;">
        Role <Icon type="down" />
      </a>
    </Dropdown>
    <Divider type="vertical" />
    <Popconfirm
      title="Are you sure"
      okType="danger"
      onConfirm={() => props.onBanToggle(props.item.id)}
    >
      <a href="javascript:;">{ props.item.banned ? 'Unban' : 'Ban' }</a>
    </Popconfirm>
  </React.Fragment>
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserActions);
