/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { User } from 'lib/types/User';
import * as Data from 'components/Data';
import {
  mapStateToProps,
  mapDispatchToProps,
} from './selectors';
import UserDetails from '../components/details';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly users: { readonly [id: string]: User };
  readonly fetchById: (id: string) => any;
}
//#endregion

const EditRestriction = (props: Props) => (
  <Data.Edit<User>
    title="User Details"
    meta={{} as any}
    data={props.users}
    idExtractor={() => (props.match.params as any).id}
    fetchById={props.fetchById}
    onComplete={() => props.history.replace('/users')}
  >
    <UserDetails users={props.users} />
  </Data.Edit>
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(EditRestriction));
