/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import {
  userCollectionProvider as provider,
  CollectionProviderProps,
} from 'providers';
import * as Data from 'components/Data';
import { User } from 'lib/types/User';
import { columns } from './columns';
import { UserRole } from 'lib/types/UserRole';
//#endregion

//#region Component interfaces
export interface Props extends
  CollectionProviderProps<User>,
  RouteComponentProps<any> {
  readonly id: string;
  readonly role: UserRole;
  readonly onRoleChange: (id: string, role: UserRole) => any;
  readonly onBanToggle: (id: string) => any;

}
//#endregion

const UsersController = (props: Props) => (
  <Data.List<User>
    title="Users"
    data={Object.values(props.data)}
    extractKey={(entity, _) => `${entity.id}`}
    pending={props.meta.pending || !props.meta.retrieved}
    columns={columns()}
  />
);

export default withRouter(provider<Props>(UsersController));
