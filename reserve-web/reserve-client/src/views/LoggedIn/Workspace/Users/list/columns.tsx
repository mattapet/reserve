/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { ColumnProps } from 'antd/lib/table/interface';
import { Link } from 'react-router-dom';

import UserRoleTag from 'components/UserRoleTag';
import { User } from 'lib/types/User';
import { timestamp } from 'lib/dateFormatter';
import UserActions from '../components/UserActions';
//#endregion

export const columns = () =>  ([
  {
    key: 'id',
    title: 'ID',
    render: (_, { id }) => <Link to={`/users/${id}/details`}>{id}</Link>
  },
  {
    key: 'email',
    title: 'Email',
    render: (_, { id, email }) =>
      <Link to={`/users/${id}/details`}>{email}</Link>
  },
  {
    dataIndex: 'profile.firstName',
    key: 'firstName',
    title: 'First Name',
  },
  {
    dataIndex: 'profile.lastName',
    key: 'lastName',
    title: 'Last Name',
  },
  {
    key: 'role',
    title: 'Role',
    render: (_, item) => <UserRoleTag role={item.role} />
  },
  {
    key: 'lastLogin',
    title: 'Last Login',
    render: (_, item) => timestamp(item.lastLogin),
  },
  {
    key: 'actions',
    title: 'Actions',
    render: (_, item) => <UserActions item={item} />
  },
] as ColumnProps<User>[]);
