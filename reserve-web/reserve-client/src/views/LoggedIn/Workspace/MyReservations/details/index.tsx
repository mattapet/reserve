/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { Reservation } from 'lib/types/Reservation';
import { Resource } from 'lib/types/Resource';
import * as Data from 'components/Data';
import Details from '../components/details';
import {
  mapStateToProps,
  mapDispatchToProps,
} from './selectors';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly resources: { readonly [id: number]: Resource };
  readonly reservations: { readonly [id: string]: Reservation };
  readonly fetchById: (id: string) => any;
}
//#endregion

const ReservationDetails = (props: Props) => (
  <Data.Edit<Reservation>
    title="Reservation Details"
    meta={{} as any}
    data={props.reservations}
    idExtractor={() => (props.match.params as any).id}
    fetchById={props.fetchById}
    onComplete={() => props.history.replace('/my_reservations')}
  >
    <Details
      resources={props.resources}
      reservations={props.reservations}
    />
  </Data.Edit>
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(ReservationDetails));
