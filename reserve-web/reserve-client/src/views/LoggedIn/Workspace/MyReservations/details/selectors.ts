/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from 'actions/reservations';
import { RootState } from 'reducers';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  reservations: state.reservation.data,
  resources: state.resource.data,
  meta: state.view.reservation.edit,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  fetchById: (id: string) => dispatch(actions.fetchById.request(id)),
});
