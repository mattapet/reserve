/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import Form from '../components/form';
import { Reservation } from 'lib/types/Reservation';
import * as Data from 'components/Data';
import {
  mapStateToProps,
  mapDispatchToProps,
} from './selectors';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly meta: {
    readonly error?: Error;
    readonly retrieved: boolean;
    readonly pending: boolean;
  };
  readonly userId: string;
  readonly reservations: { readonly [id: string]: Reservation };
  readonly fetchById: (id: string) => any;
  readonly onSubmit: (value: Reservation) => any;
  readonly onCancel: () => any;
}
//#endregion

const EditReservation = (props: Props) => (
  <Data.Edit<Reservation>
    title="Edit Reservation"
    meta={props.meta}
    data={props.reservations}
    idExtractor={() => (props.match.params as any).id}
    fetchById={props.fetchById}
    onComplete={() => props.history.replace('/my_reservations')}
  >
    <Form
      onSubmit={(
        dateStart,
        dateEnd,
        resourceIds,
        allDay,
        userId = props.userId
      ) => {
        props.onSubmit({
          ...props.reservations[(props.match.params as any).id],
          dateStart,
          dateEnd,
          resourceIds,
          userId,
        });
      }}
      onCancel={() => props.history.goBack()}
    />
  </Data.Edit>
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(EditReservation));
