/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from 'actions/reservations';
import { RootState } from 'reducers';
//#endregion

export const mapStateToProps = (state: RootState) => {
  if (!state.auth.isLoggedIn) {
    throw Error('Invalid state');
  }
  const userId = state.auth.identity.id;
  const reservations = Object.values(state.reservation.data)
    .filter(reservation => reservation.userId === userId)
    .reduce((result, value) => ({ ...result, [value.id]: value }), {});
  return {
    meta: state.view.reservation.fetch,
    reservations,
  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onFetch: () => dispatch(actions.fetch.request()),
  onFetchCancel: () => dispatch(actions.fetch.cancel()),
});
