/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Route, Switch } from 'react-router-dom';

import List from './list';
import Edit from './edit';
import Create from './create';
import Details from './details';
import NotFound from 'views/NotFound';
//#endregion

const Router: React.FunctionComponent<any> = () => (
  <Switch>
    <Route path="/my_reservations" component={List} exact />
    <Route path="/my_reservations/create" component={Create} exact />
    <Route path="/my_reservations/:id/edit" component={Edit} exact />
    <Route path="/my_reservations/:id/details" component={Details} exact />
    <Route component={NotFound} />
  </Switch>
);

export default Router;
