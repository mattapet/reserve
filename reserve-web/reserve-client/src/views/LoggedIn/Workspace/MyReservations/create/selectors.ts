/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from 'actions/reservations';
import { RootState } from 'reducers';
//#endregion

export const mapStateToProps = (state: RootState) => {
  if (!state.auth.isLoggedIn) {
    throw Error('Invalid state');
  }

  return {
    userId: state.auth.identity.id,
    meta: state.view.reservation.create,
  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onSubmit: (
    dateStart: Date,
    dateEnd: Date,
    resourceIds: number[],
    allDay: boolean,
    userId?: string,
  ) =>
    dispatch(actions.create.request(({
      dateStart,
      dateEnd,
      resourceIds,
      allDay,
      userId: userId!,
    }))),
  onCancel: () => dispatch(actions.create.cancel()),
});
