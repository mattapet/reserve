/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import moment from 'moment';
import { Form, DatePicker, Select, Button, Input } from 'antd';
import { Resource } from 'lib/types/Resource';
//#endregion

//#region Component interfaces
export interface Props {
  readonly resources: Resource[];
  readonly dateStart: Date;
  readonly dateEnd: Date;
  readonly resourceIds: { key: number, label: string }[];
  readonly notes?: string;
  readonly onDateRangeChange: (dateRagnge: [string, string]) => any;
  readonly onResourceIdsChange: (
    selected: { key: number, label: string }[],
  ) => any;
  readonly onNotesChange: (e: React.FormEvent<HTMLTextAreaElement>) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
  readonly onCancel: () => any;
}
//#endregion

const View: React.FunctionComponent<Props> = (props) => (
  <Form onSubmit={props.onSubmit}>
    <DatePicker.RangePicker
      onChange={(_, range) => props.onDateRangeChange(range)}
      value={[moment(props.dateStart), moment(props.dateEnd)]}
      allowClear={false}
    />

<Select<{ key: number, label: string }[]>
      mode="multiple"
      style={{ width: '100%' }}
      value={props.resourceIds}
      onChange={props.onResourceIdsChange}
      placeholder="Select resources"
      showArrow={true}
      labelInValue
    >
      {props.resources
        .filter(resource =>
          props.resourceIds.map(({ key }) => key).indexOf(resource.id) < 0)
        .map(resource => (
          <Select.Option key={`${resource.id}`} value={resource.id}>
            {resource.name}
          </Select.Option>
        )
      )}
    </Select>

    <Input.TextArea
      placeholder="Additional notes"
      value={props.notes}
      onChange={props.onNotesChange}
    />

    <Button onClick={props.onCancel}>Cancel</Button>
    <Button
      type="primary"
      htmlType="submit"
      disabled={!props.resourceIds.length}
    >
      Submit
    </Button>
  </Form>
);

export default View;
