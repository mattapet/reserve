/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';

import {
  CollectionProviderProps,
  resourceCollectionProvider,
} from 'providers';
import View from './view';
import { Reservation } from 'lib/types/Reservation';
import { Resource } from 'lib/types/Resource';
//#endregion

//#region Component interfaces
export interface Props extends CollectionProviderProps<Resource> {
  readonly defaultValue?: Reservation;
  onSubmit: (
    dateStart: Date,
    dateEnd: Date,
    resourceIds: number[],
    allDay: boolean,
    notes?: string,
    userId?: string,
  ) => any;
  readonly onCancel: () => any;
}

interface State {
  readonly dateStart: Date;
  readonly dateEnd: Date;
  readonly resourceIds: { key: number, label: string }[];
  readonly userId?: string;
  readonly notes?: string;
}
//#endregion

class FormController extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    const { defaultValue } = props;
    this.state = {
      dateStart: defaultValue && defaultValue.dateStart || new Date(),
      dateEnd: defaultValue && defaultValue.dateEnd || new Date(),
      resourceIds: defaultValue && defaultValue.resourceIds.map(id =>
        ({ key: id, label: props.data[id].name })) || [],
      userId: defaultValue && defaultValue.userId,
      notes: defaultValue && defaultValue.notes || ''
    };
  }

  public render() {
    return (
      <View
        {...this.state}
        resources={Object.values(this.props.data)}
        onCancel={this.props.onCancel}
        onSubmit={this.handleSubmit}
        onDateRangeChange={this.handleDateRangeChange}
        onResourceIdsChange={this.handleResourceIdsChange}
        onNotesChange={this.handleNotesChange}
      />
    );
  }

  private handleDateRangeChange = (dateRange: [string, string]) => {
    const [ dateStart, dateEnd ] = dateRange;
    this.setState({
      dateStart: new Date(dateStart),
      dateEnd: new Date(dateEnd),
    });
  };

  private handleResourceIdsChange = (
    resourceIds: { key: number, label: string }[],
  ) => {
    this.setState({ resourceIds });
  };

  private handleNotesChange = (e: React.FormEvent<HTMLTextAreaElement>) => {
    const notes = e.currentTarget.value;
    this.setState({ notes });
  };

  private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { dateStart, dateEnd, resourceIds, userId, notes } = this.state;
    this.props.onSubmit(
      dateStart,
      dateEnd,
      resourceIds.map(({ key }) => key),
      true,
      notes,
      userId
    );
  }
}

export default resourceCollectionProvider<Props>(FormController);
