/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from 'actions/reservations';
import { RootState } from 'reducers';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  meta: state.view.reservation.fetch,
  reservations: state.reservation.data,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onFetch: () => dispatch(actions.fetch.request()),
  onFetchCancel: () => dispatch(actions.fetch.cancel()),
  onConfirm: (id: string) => dispatch(actions.confirmById.request(id)),
  onCancel: (id: string) => dispatch(actions.cancelById.request(id)),
  onReject: (id: string) => dispatch(actions.rejectById.request(id)),
});
