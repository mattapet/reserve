/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationState } from 'lib/types/ReservationState';
//#endregion

export default function (state: ReservationState) {
  switch (state) {
  case ReservationState.requested:
    return [ ReservationState.confirmed, ReservationState.rejected ];
  case ReservationState.confirmed:
    return [ ReservationState.canceled ];
  case ReservationState.canceled:
  case ReservationState.rejected:
    return [ ReservationState.confirmed ];
  case ReservationState.completed:
    return [ ];
  }
}

export function transitionName(targetState: ReservationState) {
  switch (targetState) {
    case ReservationState.confirmed:
      return 'Confirm';
    case ReservationState.canceled:
      return 'Cancel';
    case ReservationState.rejected:
      return 'Reject';
    default:
      throw TypeError(
        `Cannot manually set reservation to state ${targetState}`
      );
  }
}
