/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { List } from 'components/Data';
import { mapDispatchToProps } from './selectors';
import {
  reservationCollectionProvider as provider,
  CollectionProviderProps,
} from 'providers';

import { Reservation } from 'lib/types/Reservation';
import ResourceTag from 'components/ResourceTag';
import { columns } from './columns';
//#endregion

//#region Component interfaces
export interface Props extends
  CollectionProviderProps<Reservation>,
  RouteComponentProps<any> {
  readonly onConfirm: (id: string) => any;
  readonly onCancel: (id: string) => any;
  readonly onReject: (id: string) => any;
}
//#endregion

const ReservationList: React.FunctionComponent<Props> = (props) => (
  <List<Reservation>
    title="Reservations"
    data={Object.values(props.data)}
    pending={props.meta.pending || !props.meta.retrieved}
    extractKey={(entity, _) => `${entity.id}`}
    columns={columns(props.onConfirm, props.onCancel, props.onReject)}
    onCreate={() => props.history.push('/reservations/create')}
    expandedRowRender={reservation => (
      <React.Fragment>
        <b>Resources: </b>
        {reservation.resourceIds.map(id =>
          <ResourceTag key={`${id}`} id={id} />
        )}
        {reservation.notes && reservation.notes.length ?
          <React.Fragment>
            <br />
            <b>Notes: </b>{reservation.notes}
          </React.Fragment> : null }
      </React.Fragment>
    )}
  />
);

export default connect(
  null,
  mapDispatchToProps,
)(withRouter(provider<Props>(ReservationList)));
