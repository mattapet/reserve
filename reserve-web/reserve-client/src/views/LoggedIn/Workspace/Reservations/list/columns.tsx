/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Link } from 'react-router-dom';
import { Divider, Popconfirm } from 'antd';
import { ColumnProps } from 'antd/lib/table/interface';

import stateTransitions, {
  transitionName,
} from './stateTransitions';
import { date } from 'lib/dateFormatter';
import { Reservation } from 'lib/types/Reservation';
import ReservationStateTag from 'components/ReservationStateTag';
import UserNameLink from 'components/UserNameLink';
import { ReservationState } from 'lib/types/ReservationState';
//#endregion

export const columns = (
  onConfirm: (id: string) => any,
  onCancel: (id: string) => any,
  onReject: (id: string) => any,
) =>  ([
  {
    key: 'id',
    title: 'ID',
    render: (_, { id }) =>
      <Link to={`/reservations/${id}/details`}>{ id }</Link>
  },
  {
    key: 'dateStart',
    title: 'Date Start',
    render: (_, item) => date(item.dateStart),
  },
  {
    key: 'dateEnd',
    title: 'Date End',
    render: (_, item) => date(item.dateEnd),
  },
  {
    key: 'state',
    title: 'State',
    render: (_, { state }) => <ReservationStateTag state={state} />,
  },
  {
    key: 'user',
    title: 'User',
    render: (_, { userId }) => (
      <UserNameLink id={userId} />
    ),
  },
  {
    key: 'actions',
    title: 'Actions',
    render: (_, { id, state }) => (
      stateTransitions(state).map((targetState, idx) => (
        <React.Fragment>
          {idx > 0 ? <Divider type="vertical" /> : null}
          <Popconfirm
            title="Are you sure?"
            okText="Yes"
            onConfirm={
              () => {
                switch (targetState) {
                case ReservationState.confirmed: return onConfirm(id);
                case ReservationState.canceled: return onCancel(id);
                case ReservationState.rejected: return onReject(id);
                default:
                  throw TypeError(`Invalid state transition to ${targetState}`);
                }
              }
            }
          >
            <a href="javascript:;">{transitionName(targetState)}</a>
          </Popconfirm>
        </React.Fragment>
      ))
    )
  }
] as ColumnProps<Reservation>[]);
