/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from 'actions/reservations';
import { RootState } from 'reducers';
import { Reservation } from 'lib/types/Reservation';
//#endregion

export const mapStateToProps = (state: RootState) => {
  if (!state.auth.isLoggedIn) {
    throw Error('Invalid state');
  }

  return {
    reservations: state.reservation.data,
    userId: state.auth.identity.id,
    meta: state.view.reservation.edit,
  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  fetchById: (id: string) => dispatch(actions.fetchById.request(id)),
  onSubmit: (payload: Reservation) =>
    dispatch(actions.edit.request(payload)),
  onCancel: () => dispatch(actions.create.cancel()),
});
