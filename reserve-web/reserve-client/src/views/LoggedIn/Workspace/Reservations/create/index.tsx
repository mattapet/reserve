/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import * as Data from 'components/Data';
import Form from '../components/form';
import {
  mapStateToProps,
  mapDispatchToProps,
} from './selectors';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly meta: {
    readonly error?: Error;
    readonly pending: boolean;
    readonly retrieved: boolean;
  };
  readonly onSubmit: (
    dateStart: Date,
    dateEnd: Date,
    resourceIds: number[],
    allDay: boolean,
    notes?: string,
    userId?: string,
  ) => any;
  readonly onCancel: () => any;
}
//#endregion

const CreateReservation: React.FunctionComponent<Props> = (props) => (
  <Data.Create
    meta={props.meta}
    title="Create Reservations"
    onComplete={() => props.history.replace('/reservations')}
  >
    <Form
      onSubmit={props.onSubmit}
      onCancel={() => props.history.goBack()}
    />
  </Data.Create>
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(CreateReservation));
