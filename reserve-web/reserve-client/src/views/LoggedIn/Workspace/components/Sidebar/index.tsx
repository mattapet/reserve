/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Layout, Menu } from 'antd';
import React from 'react';
import { NavLink, RouteComponentProps,withRouter } from 'react-router-dom';
import { UserRole } from 'lib/types/UserRole';
//#endregion

export interface Props extends RouteComponentProps {
  readonly role: UserRole;
}

const userLinks = [
  <Menu.Item key="/overview">
    <NavLink to="/overview">
      Overview
    </NavLink>
  </Menu.Item>,
  <Menu.Item key="/my_reservations">
    <NavLink to="/my_reservations">
      My Reservations
    </NavLink>
  </Menu.Item>
];

const maintainerLinks = [
  ...userLinks,

  <Menu.Item key="/resources">
    <NavLink to="/resources">
      Resources
    </NavLink>
  </Menu.Item>,
  <Menu.Item key="/reservations">
    <NavLink to="/reservations">
      Reservations
    </NavLink>
  </Menu.Item>,
  // <Menu.Item key="/restrictions">
  //   <NavLink to="/restrictions">
  //     Restrictions
  //   </NavLink>
  // </Menu.Item>,
  <Menu.Item key="/users">
    <NavLink to="/users">
      Users
    </NavLink>
  </Menu.Item>,
];

const ownerLinks = [
  ...maintainerLinks,

  <Menu.SubMenu key="/settings" title="Settings">
    <Menu.Item key="/settings/reservations">
      <NavLink to="/settings/reservations">
        Reservations
      </NavLink>
    </Menu.Item>

    <Menu.Item key="/settings/users">
      <NavLink to="/settings/users">
        Users
      </NavLink>
    </Menu.Item>

    <Menu.Item key="/settings/authorities">
      <NavLink to="/settings/authorities">
        Login Authorities
      </NavLink>
    </Menu.Item>

    <Menu.Item key="/settings/webhooks">
      <NavLink to="/settings/webhooks">
        Webhooks
      </NavLink>
    </Menu.Item>

    <Menu.Item key="/settings/workspace">
      <NavLink to="/settings/workspace">
        Workpsace
      </NavLink>
    </Menu.Item>
  </Menu.SubMenu>
];

const Sidebar: React.FunctionComponent<Props> = (props) => (
  <Layout.Sider>
    <Menu
      mode="inline"
      theme="dark"
      defaultSelectedKeys={[props.location.pathname]}
    >
      {props.role === UserRole.user ?
        userLinks : props.role === UserRole.maintainer ?
            maintainerLinks :
              ownerLinks}
    </Menu>
  </Layout.Sider>
);

export default withRouter(Sidebar);
