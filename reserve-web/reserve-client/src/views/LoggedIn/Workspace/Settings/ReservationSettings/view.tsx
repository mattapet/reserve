/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { Input, Row, Col, Select, Button, Form, Checkbox } from 'antd';

import { TimeUnit } from 'lib/types/TimeUnit';
import Entry from '../components/form/Entry';
import Section from '../components/form/Section';
//#endregion

export interface Props {
  readonly pending: boolean;
  readonly timeUnit: TimeUnit;
  readonly maxReservations: string;
  readonly maxRangeAmount: string;
  readonly maxRangeUnits: TimeUnit;
  readonly maxOffsetAmount: string;
  readonly maxOffsetUnits: TimeUnit;
  readonly activeEnabled: boolean;
  readonly rangeEnabled: boolean;
  readonly offsetEnabled: boolean;

  readonly onActiveEnabledChanged: (e: CheckboxChangeEvent) => any;
  readonly onRangeEnabledChanged: (e: CheckboxChangeEvent) => any;
  readonly onOffsetEnabledChanged: (e: CheckboxChangeEvent) => any;
  readonly onActiveReservationChange: (
    e: React.FormEvent<HTMLInputElement>,
  ) => any;
  readonly onReservationRangeValueChanged: (
    e: React.FormEvent<HTMLInputElement>,
  ) => any;
  readonly onReservationRangeUnitChanged: (unit: TimeUnit) => any;
  readonly onReservationOffsetValueChanged: (
    e: React.FormEvent<HTMLInputElement>
  ) => any;
  readonly onReservationOffsetUnitChanged: (unit: TimeUnit) => any;
  readonly onReservationUnitChanged: (unit: TimeUnit) => any;

  readonly onCancel: () => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
}

const View: React.FunctionComponent<Props> = (props) => (
  <Form onSubmit={props.onSubmit}>
    <Section title="Reservation limits">
      <Entry label="Active reservations">
        <Row>
          <Col span={1}>
            <Checkbox
              checked={props.activeEnabled}
              onChange={props.onActiveEnabledChanged}
            />
          </Col>
          <Col span={21}>
            <Input
              placeholder="5"
              type="number"
              value={props.maxReservations}
              onChange={props.onActiveReservationChange}
              disabled={!props.activeEnabled}
              required={props.activeEnabled}
            />
          </Col>
        </Row>
      </Entry>

      <Entry label="Reservation range">
        <Row>
          <Col span={1}>
            <Checkbox
              checked={props.rangeEnabled}
              onChange={props.onRangeEnabledChanged}
            />
          </Col>
          <Col span={11}>
            <Input
              placeholder="30"
              type="number"
              value={props.maxRangeAmount}
              onChange={props.onReservationOffsetValueChanged}
              disabled={!props.rangeEnabled}
              required={props.rangeEnabled}
            />
          </Col>
          <Col span={12}>
            <Select
              value={props.maxRangeUnits}
              onChange={props.onReservationRangeUnitChanged}
              disabled={!props.rangeEnabled}
            >
             {[TimeUnit.day, TimeUnit.hour, TimeUnit.minute].map(timeUnit => (
               <Select.Option key={timeUnit} value={timeUnit}>
                { timeUnit }
               </Select.Option>
             ))}
            </Select>
          </Col>
        </Row>
      </Entry>

      <Entry label="Reservation offset">
        <Row>
          <Col span={1}>
            <Checkbox
              checked={props.offsetEnabled}
              onChange={props.onOffsetEnabledChanged}
            />
          </Col>
          <Col span={11}>
            <Input
              placeholder="30"
              type="number"
              value={props.maxOffsetAmount}
              onChange={props.onReservationOffsetValueChanged}
              disabled={!props.offsetEnabled}
              required={props.offsetEnabled}
            />
          </Col>
          <Col span={12}>
            <Select
              value={props.maxOffsetUnits}
              onChange={props.onReservationOffsetUnitChanged}
              disabled={!props.offsetEnabled}
            >
             {[TimeUnit.day, TimeUnit.hour, TimeUnit.minute].map(timeUnit => (
               <Select.Option key={timeUnit} value={timeUnit}>
                { timeUnit }
               </Select.Option>
             ))}
            </Select>
          </Col>
        </Row>
      </Entry>
    </Section>

    <Section title="Reservation options">
      <Entry label="Time unit">
        <Select
          value={props.timeUnit}
          onChange={props.onReservationUnitChanged}
        >
          {[TimeUnit.day, TimeUnit.hour, TimeUnit.minute].map(timeUnit => (
            <Select.Option key={timeUnit} value={timeUnit}>
            { timeUnit }
            </Select.Option>
          ))}
        </Select>
      </Entry>
    </Section>

    <Button onClick={props.onCancel}>Cancel</Button>
    <Button type="primary" htmlType="submit" loading={props.pending}>
      Submit
    </Button>
  </Form>
);

export default View;
