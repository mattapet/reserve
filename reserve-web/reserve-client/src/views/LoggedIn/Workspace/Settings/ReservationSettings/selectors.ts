/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { RootState } from 'reducers';
import { Dispatch } from 'redux';
import { settingsActions } from 'actions';
import { ReservationSettings } from 'lib/types/Settings';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  reservationSettings: state.settings.fetched ?
    state.settings.reservationSettings : undefined,
  meta: state.view.settings.users,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onFetch: (workspaceId: string) =>
    dispatch(settingsActions.fetch.request(workspaceId)),
  onSubmit: (workspaceId: string, settings: ReservationSettings) =>
    dispatch(settingsActions.editReservation.request(workspaceId, settings)),
});
