/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { ReservationSettings } from 'lib/types/Settings';
import { TimeUnit } from 'lib/types/TimeUnit';
import Spinner from 'components/Spinner';
import View from './view';
import { mapStateToProps, mapDispatchToProps } from './selectors';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
//#endregion

export interface Props {
  readonly workspaceId: string;
  readonly reservationSettings?: ReservationSettings;
  readonly meta: {
    readonly pending: boolean;
    readonly error?: Error;
  };
  readonly onFetch: (workspaceId: string) => any;
  readonly onSubmit: (
    workspaceId: string,
    settings: ReservationSettings,
  ) => any;
}

interface State {
  readonly timeUnit: TimeUnit;
  readonly maxReservations: string;
  readonly maxRangeAmount: string;
  readonly maxRangeUnits: TimeUnit;
  readonly maxOffsetAmount: string;
  readonly maxOffsetUnits: TimeUnit;
  readonly activeEnabled: boolean;
  readonly rangeEnabled: boolean;
  readonly offsetEnabled: boolean;
  readonly fetched: boolean;
}

class ReservationSettingsController extends React.Component<Props, State> {
  public state = {
    timeUnit: TimeUnit.day,
    maxReservations: '5',
    maxRangeAmount: '1',
    maxRangeUnits: TimeUnit.day,
    maxOffsetAmount: '30',
    maxOffsetUnits: TimeUnit.day,
    activeEnabled: false,
    rangeEnabled: false,
    offsetEnabled: false,
    fetched: false,
  };

  public componentDidMount() {
    if (!this.props.reservationSettings) {
      this.props.onFetch(this.props.workspaceId);
    }
  }

  public render() {
    return (
      <React.Fragment>
        <h1>ReservationSettings</h1>
        {this.props.reservationSettings ?
          <View
            {...this.state}
            pending={this.props.meta.pending}
            onActiveEnabledChanged={this.handleActiveEnabledChanged}
            onRangeEnabledChanged={this.handleRangeEnabledChanged}
            onOffsetEnabledChanged={this.handleOffsetEnabledChanged}
            onActiveReservationChange={this.handleActiveReservationChange}
            onReservationRangeValueChanged={
              this.handleReservationRangeValueChanged}
            onReservationRangeUnitChanged={
              this.handleReservationRangeUnitChanged}
            onReservationOffsetValueChanged={
              this.handleReservationOffsetValueChanged}
            onReservationOffsetUnitChanged={
              this.handleReservationOffsetUnitChanged}
            onReservationUnitChanged={this.handleReservationUnitChanged}
            onCancel={this.handleCancel}
            onSubmit={this.handleSubmit}
          /> : <Spinner />
        }
      </React.Fragment>
    );
  }

  private handleActiveEnabledChanged = (e: CheckboxChangeEvent) => {
    const activeEnabled = e.target.checked;
    this.setState({ activeEnabled });
  };
  private handleRangeEnabledChanged = (e: CheckboxChangeEvent) => {
    const rangeEnabled = e.target.checked;
    this.setState({ rangeEnabled });
  };
  private handleOffsetEnabledChanged = (e: CheckboxChangeEvent) => {
    const offsetEnabled = e.target.checked;
    this.setState({ offsetEnabled });
  };

  private handleActiveReservationChange = (
    e: React.FormEvent<HTMLInputElement>,
  ) => {
    const maxReservations = e.currentTarget.value;
    this.setState({ maxReservations });
  };

  private handleReservationRangeValueChanged = (
    e: React.FormEvent<HTMLInputElement>,
  ) => {
    const maxRangeAmount = e.currentTarget.value;
    this.setState({ maxRangeAmount });
  };

  private handleReservationRangeUnitChanged = (maxRangeUnits: TimeUnit) => {
    this.setState({ maxRangeUnits });
  };

  private handleReservationOffsetValueChanged = (
    e: React.FormEvent<HTMLInputElement>,
  ) => {
    const maxOffsetAmount = e.currentTarget.value;
    this.setState({ maxOffsetAmount });
  };

  private handleReservationOffsetUnitChanged = (maxOffsetUnits: TimeUnit) => {
    this.setState({ maxOffsetUnits });
  };

  private handleReservationUnitChanged = (timeUnit: TimeUnit) => {
    this.setState({ timeUnit });
  };

  private handleCancel = () => {
    this.setState({
      ...ReservationSettingsController
        .extractStateFromSettings(this.props.reservationSettings!),
    });
  };

  private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const {
      timeUnit,
      maxReservations,
      maxRangeAmount,
      maxRangeUnits,
      maxOffsetAmount,
      maxOffsetUnits,
      ...rest
    } = this.state;
    this.props.onSubmit(
      this.props.workspaceId,
      {
        timeUnit,
        maxRange: rest.rangeEnabled ? {
          amount: parseInt(maxRangeAmount, 10),
          units: maxRangeUnits
        } : null,
        maxOffset: rest.offsetEnabled ? {
          amount: parseInt(maxOffsetAmount, 10),
          units: maxOffsetUnits
        } : null,
        maxReservations: rest.activeEnabled ?
          parseInt(maxReservations, 10) : null,
      },
    );
  };

  private static extractStateFromSettings(
    settings: ReservationSettings,
  ): State {
    const { maxOffset, maxRange, maxReservations, timeUnit } = settings;
    return {
      timeUnit,
      maxReservations: maxReservations && `${maxReservations}` || '5',
      maxRangeAmount: maxRange && `${maxRange.amount}` || '1',
      maxRangeUnits: maxRange && maxRange.units || TimeUnit.day,
      maxOffsetAmount: maxOffset && `${maxOffset.amount}` || '30',
      maxOffsetUnits: maxOffset && maxOffset.units || TimeUnit.day,
      activeEnabled: maxReservations !== null,
      rangeEnabled: maxRange !== null,
      offsetEnabled: maxOffset !== null,
      fetched: true,
    };
  }

  public static getDerivedStateFromProps(
    props: Props,
    state: State | null,
  ): State | null {
    if (props.reservationSettings && state && !state.fetched) {
      return ReservationSettingsController
        .extractStateFromSettings(props.reservationSettings);
    } else {
      return null;
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ReservationSettingsController);
