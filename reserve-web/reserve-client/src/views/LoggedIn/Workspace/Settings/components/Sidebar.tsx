/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Layout, Menu } from 'antd';
import * as React from 'react';
import { NavLink, RouteComponentProps,withRouter } from 'react-router-dom';
import { UserRole } from 'lib/types/UserRole';
//#endregion

export interface Props extends RouteComponentProps {
  readonly role: UserRole;
}

const userLinks: typeof Menu.Item[] = [ ];

const maintainerLinks = [
  ...userLinks,
];

const ownerLinks = [
  ...maintainerLinks,

  <Menu.Item key="/settings/reservations">
    <NavLink to="/settings/reservations">
      Reservations
    </NavLink>
  </Menu.Item>,

  <Menu.Item key="/settings/users">
    <NavLink to="/settings/users">
      Users
    </NavLink>
  </Menu.Item>,

  <Menu.Item key="/settings/authorities">
    <NavLink to="/settings/authorities">
      Login Authorities
    </NavLink>
  </Menu.Item>,
];

const Sidebar: React.FunctionComponent<Props> = (props) => (
  <Layout.Sider>
    <Menu
      mode="inline"
      theme="dark"
      defaultSelectedKeys={['/overview']}
      selectedKeys={[props.location.pathname]}
    >
      {props.role === UserRole.user ?
        userLinks : props.role === UserRole.maintainer ?
            maintainerLinks :
              ownerLinks}
    </Menu>
  </Layout.Sider>
);

export default withRouter(Sidebar);
