/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import styled from 'styled-components';
//#endregion

export interface Props {
  readonly title: string;
}

const SectionContainer = styled.div`
  margin-top: 1em;
`;

const Section: React.FunctionComponent<Props> = (props) => (
  <SectionContainer>
    <h3>{ props.title }</h3>
    <hr />
    { props.children }
  </SectionContainer>
);

export default Section;
