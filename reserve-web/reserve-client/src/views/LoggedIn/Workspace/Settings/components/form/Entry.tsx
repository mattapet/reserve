/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import styled from 'styled-components';
import { Row, Col } from 'antd';
//#endregion

export interface Props {
  readonly label: string;
}

const Label = styled(Col as any)`
  font-weight: 700;
  text-align: right;
  padding-right: .5em;
`;

const EntryRow = styled(Row)`
  padding-top: .5em;
`;

const Entry: React.FunctionComponent<Props> = (props) => (
  <EntryRow>
    <Label span={4}>{ props.label } </Label>
    <Col span={20}>{props.children}</Col>
  </EntryRow>
);

export default Entry;
