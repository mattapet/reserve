/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Button, Modal } from 'antd';

import Input from 'components/Input';
import Entry from '../components/form/Entry';
import Section from '../components/form/Section';
//#endregion

//#region Component interfaces
export interface Props {
  readonly workspaceName: string;
  readonly onDeleteRequest: () => any;
  readonly displayDeleteConfirm: boolean;
  readonly deleteConfirmValue: string;
  readonly onDeleteConfirmChanged: (e: string,) => any;
  readonly onDeleteConfirm: () => any;
  readonly onDeleteCancel: () => any;
}
//#endregion

const View: React.FunctionComponent<Props> = (props) => (
  <Section title="Danger zone">
    <Entry label="Delete workspace">
      <Button
        size="large"
        type="danger"
        onClick={props.onDeleteRequest}
      >
        <span>Delete workspace <b>{props.workspaceName}</b></span>
      </Button>
      <Modal
        title="Are you absolutely sure?"
        visible={props.displayDeleteConfirm}
        onOk={props.onDeleteConfirm}
        onCancel={props.onDeleteCancel}
        okButtonProps={{
          style: { width: '100%' },
          disabled: props.workspaceName !== props.deleteConfirmValue,
        }}
        okText="I understand, delete this workspace"
        okType="danger"
        cancelButtonProps={{ hidden: true }}
      >
        <React.Fragment>
          <p>
            This action <b>cannot</b> be undone. This will permanently delete
            the <b>{props.workspaceName}</b> workspace with all the data that
            may be associated with it including resources, reservations and user
            information.
          </p>
          <p>Please type in the name of the workspace to confirm.</p>
          <Input
            value={props.deleteConfirmValue}
            onChange={props.onDeleteConfirmChanged}
          />
        </React.Fragment>
      </Modal>
    </Entry>
  </Section>
);

export default View;

