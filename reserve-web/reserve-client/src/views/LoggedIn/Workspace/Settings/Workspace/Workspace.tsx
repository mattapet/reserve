/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import config from 'config';

import View from './view';
//#endregion

//#region Component interfaces
export interface Props {
  readonly workspaceId: string;
  readonly onDelete: (workspaceId: string) => any;
}
//#endregion

const WorkspaceSettings: React.FunctionComponent<Props> = (props) => {
  const [displayDeleteConfirm, setDisplayDeleteConfirm] = useState(false);
  const [deleteConfirmValue, setDeleteConfirmValue] = useState('');

  function handleDeleteConfirm() {
    setDisplayDeleteConfirm(false);
    props.onDelete(props.workspaceId);
  }

  return (
    <View
      displayDeleteConfirm={displayDeleteConfirm}
      deleteConfirmValue={deleteConfirmValue}
      workspaceName={config.workspaceName}
      onDeleteConfirmChanged={setDeleteConfirmValue}
      onDeleteRequest={() => setDisplayDeleteConfirm(true)}
      onDeleteConfirm={handleDeleteConfirm}
      onDeleteCancel={() => setDisplayDeleteConfirm(false)}
    />
  );
};

export default WorkspaceSettings;
