/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';
import { workspaceActions } from 'actions';
import { RootState } from 'reducers';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  worksapceId: state.workspace!.id,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onDelete: (worksapceId: string) =>
    dispatch(workspaceActions.remove.request(worksapceId)),
});
