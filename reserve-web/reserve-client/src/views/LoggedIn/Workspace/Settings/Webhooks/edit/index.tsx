/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import Spinner from 'components/Spinner';
import {
  webhookEntityProvider,
  idProvider,
  IdProviderProps,
  EntityProviderProps,
} from 'providers';
import { mapDispatchToProps } from './selectors';
import Form from '../components/form';
import { Webhook } from 'lib/types/Webhook';
import { EventType } from 'lib/types/EventType';
import { EncodingType } from 'lib/types/EncodingType';
//#endregion

//#region Component interfaces
export interface Props extends
  IdProviderProps,
  EntityProviderProps<Webhook>,
  RouteComponentProps {
  readonly onSubmit: (value: Webhook) => any;
}
//#endregion

const EditWebhook: React.FunctionComponent<Props> = (props) => (
  props.entity ?
    <Form
      defaultValue={props.entity}
      onSubmit={(
        payloadUrl: string,
        encoding: EncodingType,
        secret: string,
        events: EventType[],
      ) => props.onSubmit({
        ...props.entity!, payloadUrl, encoding, secret, events
      })}
      onCancel={() => props.history.goBack()}
    /> : <Spinner />
);

export default connect(
  null,
  mapDispatchToProps,
)(idProvider(withRouter(webhookEntityProvider<Props>(EditWebhook))));
