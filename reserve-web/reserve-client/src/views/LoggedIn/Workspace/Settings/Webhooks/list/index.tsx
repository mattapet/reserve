/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Tag } from 'antd';

import { List } from 'components/Data';
import { mapDispatchToProps } from './selectors';

import {
  webhookCollectionProvider as provider,
  CollectionProviderProps,
} from 'providers';
import { Webhook } from 'lib/types/Webhook';
import { columns } from './columns';
//#endregion

//#region Component interfaces
export interface Props extends
  CollectionProviderProps<Webhook>,
  RouteComponentProps<any> {
  readonly onRemove: (id: number) => any;
  readonly onToggle: (id: number) => any;
}
//#endregion

const WebhookList: React.FunctionComponent<Props> = (props) => (
  <List<Webhook>
    title="Webhooks"
    extractKey={(entity, _) => `${entity.id}`}
    columns={columns(props.onRemove, props.onToggle)}
    onCreate={() => props.history.push('/settings/webhooks/create')}
    data={Object.values(props.data)}
    pending={props.meta.pending || !props.meta.retrieved}
    expandedRowRender={webhook => (
      <React.Fragment>
        <b>Events: </b>
        {webhook.events.map(event => (
          <Tag key={`${event}`}>{event}</Tag>
        ))}
        <br />
        <b>Secret: </b> { webhook.secret }
      </React.Fragment>
    )}
  />
);

export default connect(
  null,
  mapDispatchToProps,
)(withRouter(provider<Props>(WebhookList)));

