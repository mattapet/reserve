/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from 'actions/webhook';
import { RootState } from 'reducers';
import { EncodingType } from 'lib/types/EncodingType';
import { EventType } from 'lib/types/EventType';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  meta: state.view.webhook.create,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onSubmit: (
    payloadUrl: string,
    encoding: EncodingType,
    secret: string,
    events: EventType[],
  ) =>
    dispatch(actions.create.request(({
      payloadUrl,
      encoding,
      secret,
      events,
    }))),
  onCancel: () => dispatch(actions.create.cancel()),
});
