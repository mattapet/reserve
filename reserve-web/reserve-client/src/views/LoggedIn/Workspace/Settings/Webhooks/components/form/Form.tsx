/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';

import View from './view';
import { Webhook } from 'lib/types/Webhook';
import { EncodingType } from 'lib/types/EncodingType';
import { EventType } from 'lib/types/EventType';
//#endregion

//#region Component interfaces
export interface Props {
  readonly defaultValue?: Webhook;
  readonly onSubmit: (
    payloadUrl: string,
    encoding: EncodingType,
    secret: string,
    events: EventType[],
  ) => any;
  readonly onCancel: () => any;
}

//#endregion

const FormController: React.FunctionComponent<Props> = (props) => {
  const { defaultValue } = props;
  const [payloadUrl, setPayloadUrl] =
    useState(defaultValue && defaultValue.payloadUrl || '');
  const [encoding, setEncoding] =
    useState(defaultValue && defaultValue.encoding || EncodingType.urlencoded);
  const [secret, setSecret] =
    useState(defaultValue && defaultValue.secret || '');
  const [events, setEvents] =
    useState(defaultValue && defaultValue.events || []);

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    props.onSubmit(payloadUrl, encoding, secret, events);
  }

  return (
    <View
      payloadUrl={payloadUrl}
      encoding={encoding}
      secret={secret}
      events={events}
      onCancel={props.onCancel}
      onSubmit={handleSubmit}
      onPayloadUrlChange={setPayloadUrl}
      onEncodingChange={setEncoding}
      onSecretChange={setSecret}
      onEventsChange={setEvents}
    />
  );
};

export default FormController;
