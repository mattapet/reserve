/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';

import Spinner from 'components/Spinner';
import { UserRole } from 'lib/types/UserRole';
import { UserSettings } from 'lib/types/Settings';
import { mapStateToProps, mapDispatchToProps } from './selectors';
import View from './view';
//#endregion

//#region Component interfaces
export interface Props {
  readonly workspaceId: string;
  readonly userSettings?: UserSettings;
  readonly meta: {
    readonly pending: boolean;
    readonly error?: Error;
  };
  readonly onFetch: (workspaceId: string) => any;
  readonly onSubmit: (workspaceId: string, settings: UserSettings) => any;
}

interface State {
  readonly allowedRegistrations: boolean;
  readonly allowedInvitations: boolean;
  readonly allowedToInvite: UserRole;
  readonly fetched: boolean;
}
//#endregion

class UserSettingsController extends React.Component<Props, State> {
  public state = {
    allowedRegistrations: true,
    allowedInvitations: false,
    allowedToInvite: UserRole.owner,
    fetched: false,
  };


  public componentDidMount() {
    if (!this.props.userSettings) {
      this.props.onFetch(this.props.workspaceId);
    }
  }

  public render() {
    return (
      <React.Fragment>
        <h1>User Settings</h1>
        {this.props.userSettings ?
          <View
            {...this.state}
            pending={this.props.meta.pending}
            onAllowedRegistrationChanged={this.handleAllowedRegistrationChanged}
            onAllowedInvitationChanged={this.handleAllowedInvitationChanged}
            onInvitationRoleChanged={this.handleInvitationRoleChanged}
            onCancel={this.handleCancel}
            onSubmit={this.handleSubmit}
          /> : <Spinner />}
      </React.Fragment>
    );
  }

  private handleAllowedRegistrationChanged = (
    allowedRegistrations: boolean,
  ) => {
    this.setState({ allowedRegistrations });
  };

  private handleAllowedInvitationChanged = (allowedInvitations: boolean) => {
    this.setState({ allowedInvitations });
  };

  private handleInvitationRoleChanged = (allowedToInvite: UserRole) => {
    this.setState({ allowedToInvite });
  };

  private handleCancel = () => {
    this.setState({ ...this.props.userSettings! });
  };

  private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.props.onSubmit(this.props.workspaceId, { ...this.state });
  };

  public static getDerivedStateFromProps(
    props: Props,
    state: State | null,
  ): State | null {
    if (props.userSettings && state && !state.fetched) {
      return { ...props.userSettings, fetched: true };
    } else {
      return null;
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserSettingsController);
