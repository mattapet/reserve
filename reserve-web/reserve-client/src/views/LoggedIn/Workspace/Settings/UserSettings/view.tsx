/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Switch, Button, Select, Form } from 'antd';

import { UserRole } from 'lib/types/UserRole';
import Entry from '../components/form/Entry';
import Section from '../components/form/Section';
//#endregion

export interface Props {
  readonly pending: boolean;
  readonly allowedRegistrations: boolean;
  readonly allowedInvitations: boolean;
  readonly allowedToInvite: UserRole;
  readonly onAllowedRegistrationChanged: (allowed: boolean) => any;
  readonly onAllowedInvitationChanged: (allowed: boolean) => any;
  readonly onInvitationRoleChanged: (role: UserRole) => any;
  readonly onCancel: () => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
}

const View: React.FunctionComponent<Props> = (props) => (
  <Form onSubmit={props.onSubmit}>
    <Section title="Registration of users">
      <Entry label="Allow">
        <Switch
          checked={props.allowedRegistrations}
          onChange={props.onAllowedRegistrationChanged}
        />
      </Entry>
    </Section>

    <Section title="Invitaion of users">
      <Entry label="Allow">
        <Switch
          checked={props.allowedInvitations}
          onChange={props.onAllowedInvitationChanged}
        />
      </Entry>

      <Entry label="Allowed roles">
        <Select<UserRole>
          value={props.allowedToInvite}
          onChange={props.onInvitationRoleChanged}
        >
          {[UserRole.owner, UserRole.maintainer, UserRole.user].map(role =>(
            <Select.Option key={role} value={role}>
              { role.toUpperCase() }
            </Select.Option>
          ))}
        </Select>
      </Entry>
    </Section>

    <Button onClick={props.onCancel}>Cancel</Button>
    <Button type="primary" htmlType="submit" loading={props.pending}>
      Submit
    </Button>
  </Form>
);

export default View;
