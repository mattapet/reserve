/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Button, Input, Form, Select } from 'antd';
//#endregion

//#region Component interface
export interface Props {
  readonly name: string;
  readonly description: string;
  readonly authorizeUrl: string;
  readonly tokenUrl: string;
  readonly profileUrls: string[];
  readonly redirectUri: string;
  readonly clientId: string;
  readonly clientSecret: string;
  readonly scope: string[];
  readonly firstNameKey: string;
  readonly lastNameKey: string;
  readonly emailKey: string;
  readonly phoneKey: string;
  readonly onNameChange: (e: React.FormEvent<HTMLInputElement>) => any;
  readonly onDescriptionChange: (
    e: React.FormEvent<HTMLTextAreaElement>,
  ) => any;
  readonly onAuthorizeUrlChange: (authorizeUrl: string) => any;
  readonly onTokenUrlChange: (tokenUrl: string) => any;
  readonly onProfileUrlsChange: (urls: string[]) => any;
  readonly onClientIdChange: (e: React.FormEvent<HTMLInputElement>) => any;
  readonly onClientSecretChange: (e: React.FormEvent<HTMLInputElement>) => any;
  readonly onScopeChange: (scope: string[]) => any;
  readonly onFirstNameKeyChange: (e: React.FormEvent<HTMLInputElement>) => any;
  readonly onLastNameKeyChange: (e: React.FormEvent<HTMLInputElement>) => any;
  readonly onEmailKeyChange: (e: React.FormEvent<HTMLInputElement>) => any;
  readonly onPhoneKeyChange: (e: React.FormEvent<HTMLInputElement>) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
  readonly onCancel: () => any;
}
//#endregion

const View: React.FunctionComponent<Props> = (props) => (
  <Form onSubmit={props.onSubmit}>
    <Input
      value={props.name}
      placeholder="Name"
      onChange={props.onNameChange}
      required
    />
    <Input.TextArea
      value={props.description}
      placeholder="Description"
      onChange={props.onDescriptionChange}
    />
    <Select<string[]>
      mode="tags"
      value={props.authorizeUrl.length ? [props.authorizeUrl] : []}
      placeholder="Authorization URL"
      onChange={([value]) => props.onAuthorizeUrlChange(value || '')}
      maxTagCount={1}
      dropdownStyle={{ visibility: 'hidden' }}
    />
    <Select<string[]>
      mode="tags"
      value={props.tokenUrl.length ? [props.tokenUrl] : []}
      placeholder="Token URL"
      onChange={([value]) => props.onTokenUrlChange(value || '')}
      maxTagCount={1}
      dropdownStyle={{ visibility: 'hidden' }}
    />
    <Select
      mode="tags"
      value={props.profileUrls}
      placeholder="Profile URLs"
      onChange={props.onProfileUrlsChange}
      dropdownStyle={{ visibility: 'hidden' }}
    />
    <Input
      type="url"
      value={props.redirectUri}
      placeholder="Redirect URI"
      disabled
    />
    <Input
      value={props.clientId}
      placeholder="Client ID"
      onChange={props.onClientIdChange}
      required
    />
    <Input.Password
      value={props.clientSecret}
      placeholder="Client Secret"
      onChange={props.onClientSecretChange}
      required
    />
    <Select
      mode="tags"
      value={props.scope}
      onChange={props.onScopeChange}
      placeholder="Scope"
      dropdownStyle={{ visibility: 'hidden' }}
    />
    <Input
      value={props.firstNameKey}
      placeholder="First name key"
      onChange={props.onFirstNameKeyChange}
      required
    />
    <Input
      value={props.lastNameKey}
      placeholder="Last name key"
      onChange={props.onLastNameKeyChange}
      required
    />
    <Input
      value={props.emailKey}
      placeholder="Email key"
      onChange={props.onEmailKeyChange}
      required
    />
    <Input
      value={props.phoneKey}
      placeholder="Phone key"
      onChange={props.onPhoneKeyChange}
    />
    <Button onClick={props.onCancel}>Cancel</Button>
    <Button type="primary" htmlType="submit">Submit</Button>
  </Form>
);

export default View;
