/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Popconfirm } from 'antd';
import { ColumnProps } from 'antd/lib/table/interface';

import { Authority } from 'lib/types/Authority';
//#endregion

export const columns = (onRemove: (name: string) => any) =>  ([
  {
    dataIndex: 'name',
    key: 'name',
    title: 'Name',
  },
  {
    dataIndex: 'authorizeUrl',
    key: 'authorizeUrl',
    title: 'Authorization URL',
  },
  {
    dataIndex: 'tokenUrl',
    key: 'tokenUrl',
    title: 'Token URL',
  },
  {
    key: 'remove',
    title: 'Remove',
    render: (_, item) =>
      <Popconfirm
        title="Are you sure?"
        okText="Yes"
        okType="danger"
        onConfirm={() => onRemove(item.name)}
      >
        <a href="javascript:;">Remove</a>
      </Popconfirm>
  },
] as ColumnProps<Authority>[]);
