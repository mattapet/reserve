/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from 'actions/authorities';
import { RootState } from 'reducers';
import { Authority } from 'lib/types/Authority';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  meta: state.view.authorities.register,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onSubmit: (authority: Authority) =>
    dispatch(actions.register.request(authority)),
  onCancel: () => dispatch(actions.register.cancel()),
});
