/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Tag } from 'antd';

import {
  authorityCollectionProvider as provider,
  CollectionProviderProps,
} from 'providers';
import { List } from 'components/Data';
import { mapDispatchToProps } from './selectors';
import { columns } from './columns';
import { Authority } from 'lib/types/Authority';
//#endregion

//#region Component interfaces
export interface Props extends
  CollectionProviderProps<Authority>,
  RouteComponentProps<any> {
  readonly onRemove: () => any;
}
//#endregion

const AuthorityList: React.FunctionComponent<Props> = (props) => (
  <List<Authority>
    title="Authorities"
    extractKey={(entity, _) => `${entity.name}`}
    columns={columns(props.onRemove)}
    onCreate={() => props.history.push('/settings/authorities/register')}
    data={Object.values(props.data)}
    pending={props.meta.pending || !props.meta.retrieved}
    expandedRowRender={authority => (
      <React.Fragment>
        <b>Profile URL: </b> {authority.profileUrls.map(url =>
          <Tag key={url}>{url}</Tag>)}
        <br />
        <b>Client ID: </b> {authority.clientId}
        <br />
        <b>Client Secret: </b> {authority.clientSecret}
        <br />
        <b>Scope: </b> {authority.scope.map(scope =>
          <Tag key={scope}>{scope}</Tag>)}
        <br />
        <b>Profile Map:</b>
        <pre>
          {JSON.stringify(authority.profileMap, null, 2)}
        </pre>
        <b>Description: </b> {authority.description}
      </React.Fragment>
    )}
  />
);

export default connect(
  null,
  mapDispatchToProps,
)(withRouter(provider<Props>(AuthorityList)));
