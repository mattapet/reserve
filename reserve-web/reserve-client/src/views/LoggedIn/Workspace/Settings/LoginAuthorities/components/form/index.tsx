/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';

import View from './view';
import { Authority } from 'lib/types/Authority';
import config from 'config';
//#endregion

//#region Component interfaces
export interface Props {
  readonly defaultValue?: Authority;
  readonly onSubmit: (authority: Authority) => any;
  readonly onCancel: () => any;
}

interface State {
  readonly name: string;
  readonly description: string;
  readonly authorizeUrl: string;
  readonly tokenUrl: string;
  readonly profileUrls: string[];
  readonly redirectUri: string;
  readonly clientId: string;
  readonly clientSecret: string;
  readonly scope: string[];
  readonly firstNameKey: string;
  readonly lastNameKey: string;
  readonly emailKey: string;
  readonly phoneKey: string;
}
//#endregion

class FormController extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    const { defaultValue } = props;
    const name = defaultValue && defaultValue.name || '';
    this.state = {
      name,
      description: defaultValue && defaultValue.description || '',
      authorizeUrl: defaultValue && defaultValue.authorizeUrl || '',
      tokenUrl: defaultValue && defaultValue.tokenUrl || '',
      profileUrls: defaultValue && defaultValue.profileUrls || [],
      redirectUri: `${config.workspaceUrl}/auth/${name}/callback`,
      clientId: defaultValue && defaultValue.clientId || '',
      clientSecret: defaultValue && defaultValue.clientSecret || '',
      scope: defaultValue && defaultValue.scope || [],
      firstNameKey: defaultValue && defaultValue.profileMap.firstName || '',
      lastNameKey: defaultValue && defaultValue.profileMap.lastName || '',
      emailKey: defaultValue && defaultValue.profileMap.email || '',
      phoneKey: defaultValue && defaultValue.profileMap.phone || '',
    };
  }

  public render() {
    return (
      <View
        {...this.state}
        onCancel={this.props.onCancel}
        onSubmit={this.handleSubmit}
        onNameChange={this.handleNameChange}
        onDescriptionChange={this.handleDescriptionChange}
        onAuthorizeUrlChange={this.handleAuthorizeUrlChange}
        onTokenUrlChange={this.handleTokenUrlChange}
        onProfileUrlsChange={this.handleProfileUrlsChange}
        onClientIdChange={this.handleClientIdChange}
        onClientSecretChange={this.handleClientSecretChange}
        onScopeChange={this.handleScopeChange}
        onFirstNameKeyChange={this.handleFirstNameKeyChange}
        onLastNameKeyChange={this.handleLastNameKeyChange}
        onEmailKeyChange={this.handleEmailKeyChange}
        onPhoneKeyChange={this.handlePhoneKeyChange}
      />
    );
  }

  private handleNameChange = (e: React.FormEvent<HTMLInputElement>) => {
    const name = e.currentTarget.value;
    const redirectUri = `${config.workspaceUrl}/auth/${name}/callback`;
    this.setState({ name, redirectUri });
  };
  private handleDescriptionChange = (
    e: React.FormEvent<HTMLTextAreaElement>,
  ) => {
    const description = e.currentTarget.value;
    this.setState({ description });
  };
  private handleAuthorizeUrlChange = (authorizeUrl: string) => {
    this.setState({ authorizeUrl });
  };
  private handleTokenUrlChange = (tokenUrl: string) => {
    this.setState({ tokenUrl });
  };
  private handleProfileUrlsChange = (profileUrls: string[]) => {
    this.setState({ profileUrls });
  };
  private handleClientIdChange = (e: React.FormEvent<HTMLInputElement>) => {
    const clientId = e.currentTarget.value;
    this.setState({ clientId });
  };
  private handleClientSecretChange = (e: React.FormEvent<HTMLInputElement>) => {
    const clientSecret = e.currentTarget.value;
    this.setState({ clientSecret });
  };
  private handleScopeChange = (scope: string[]) => {
    this.setState({ scope });
  };
  private handleFirstNameKeyChange = (e: React.FormEvent<HTMLInputElement>) => {
    const firstNameKey = e.currentTarget.value;
    this.setState({ firstNameKey });
  };
  private handleLastNameKeyChange = (e: React.FormEvent<HTMLInputElement>) => {
    const lastNameKey = e.currentTarget.value;
    this.setState({ lastNameKey });
  };
  private handleEmailKeyChange = (e: React.FormEvent<HTMLInputElement>) => {
    const emailKey = e.currentTarget.value;
    this.setState({ emailKey });
  };
  private handlePhoneKeyChange = (e: React.FormEvent<HTMLInputElement>) => {
    const phoneKey = e.currentTarget.value;
    this.setState({ phoneKey });
  };

  private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { firstNameKey, lastNameKey, emailKey, phoneKey, ...authority }
      = this.state;
    this.props.onSubmit({
      ...authority,
      profileMap: {
        firstName: firstNameKey,
        lastName: lastNameKey,
        email: emailKey,
        phone: phoneKey,
      }
    });
  }
}

export default FormController;
