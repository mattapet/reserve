/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Route, Switch } from 'react-router-dom';

import List from './list';
import Register from './register';
import NotFound from 'views/NotFound';
//#endregion

const Router: React.FunctionComponent<any> = () => (
  <Switch>
    <Route path="/settings/authorities" component={List} exact />
    <Route path="/settings/authorities/register" component={Register} exact />
    <Route component={NotFound} />
  </Switch>
);

export default Router;
