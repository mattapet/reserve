/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { List } from 'components/Data';
import { mapDispatchToProps } from './selectors';

import {
  restrictionCollectionProvider as provider,
  CollectionProviderProps,
} from 'providers';
import ResourceTag from 'components/ResourceTag';
import { Restriction } from 'lib/types/Restriction';
import { columns } from './columns';
//#endregion

//#region Component interfaces
export interface Props extends
  CollectionProviderProps<Restriction>,
  RouteComponentProps<any> {
  readonly onRemove: () => any;
}
//#endregion

const RestrictionList: React.FunctionComponent<Props> = (props) => (
  <List<Restriction>
    title="Restrictions"
    extractKey={(entity, _) => `${entity.id}`}
    columns={columns(props.onRemove)}
    onCreate={() => props.history.push('/restrictions/create')}
    data={Object.values(props.data)}
    pending={props.meta.pending || !props.meta.retrieved}
    expandedRowRender={restriction => (
      <React.Fragment>
        <b>Resources: </b>
        {restriction.resourceIds.map(id => (
          <ResourceTag key={`${id}`} id={id} />
        ))}
        {restriction.description && restriction.description.length ?
          <React.Fragment>
            <br />
            <b>Description: </b>{restriction.description}
          </React.Fragment> : null }

      </React.Fragment>
    )}
  />
);

export default connect(
  null,
  mapDispatchToProps,
)(withRouter(provider<Props>(RestrictionList)));

