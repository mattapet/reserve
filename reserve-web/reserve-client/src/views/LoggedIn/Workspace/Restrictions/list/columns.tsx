/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Link } from 'react-router-dom';
import { ColumnProps } from 'antd/lib/table/interface';
import { Popconfirm } from 'antd';

import { date } from 'lib/dateFormatter';
import { Restriction } from 'lib/types/Restriction';
//#endregion

export const columns = (onRemove: (id: number) => any) =>  ([
  {
    key: 'id',
    title: 'ID',
    render: (_, { id }) =>
      <Link to={`/restrictions/${id}/details`}>{ id }</Link>,
  },
  {
    key: 'dateStart',
    title: 'Date Start',
    render: (_, item) => date(item.dateStart),
  },
  {
    key: 'dateEnd',
    title: 'Date End',
    render: (_, item) => date(item.dateEnd),
  },
  {
    key: 'actions',
    title: 'Actions',
    render: (_, { id }) => (
      <Popconfirm
        title="Are you sure?"
        onConfirm={() => onRemove(id)}
      >
        <a href="javascript:;">Remove</a>
      </Popconfirm>
    ),
  },
] as ColumnProps<Restriction>[]);
