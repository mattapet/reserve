/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from 'actions/restrictions';
//#endregion

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onRemove: (id: number) => dispatch(actions.remove.request(id)),
});
