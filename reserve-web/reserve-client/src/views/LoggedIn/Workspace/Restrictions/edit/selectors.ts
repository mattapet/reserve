/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from 'actions/restrictions';
import { RootState } from 'reducers';
import { Restriction } from 'lib/types/Restriction';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  resources: Object.values(state.resource.data),
  restrictions: state.restriction.data,
  meta: state.view.restriction.edit,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  fetchById: (id: number) => dispatch(actions.fetchById.request(id)),
  onSubmit: (payload: Restriction) =>
    dispatch(actions.edit.request(payload)),
  onCancel: () => dispatch(actions.create.cancel()),
});
