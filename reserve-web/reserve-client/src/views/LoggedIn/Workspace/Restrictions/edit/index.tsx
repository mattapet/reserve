/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import { Restriction } from 'lib/types/Restriction';
import Spinner from 'components/Spinner';
import {
  restrictionEntityProvider,
  idProvider,
  IdProviderProps,
  EntityProviderProps,
} from 'providers';
import { mapDispatchToProps } from './selectors';
import Form from '../components/form';
//#endregion

//#region Component interfaces
export interface Props extends
  IdProviderProps,
  EntityProviderProps<Restriction>,
  RouteComponentProps {
  readonly onSubmit: (value: Restriction) => any;
}
//#endregion

const EditRestriction: React.FunctionComponent<Props> = (props) => (
  props.entity ?
    <Form
      defaultValue={props.entity}
      onSubmit={(dateStart, dateEnd, resourceIds, description) => {
        props.onSubmit({
          ...props.entity!,
          dateStart,
          dateEnd,
          resourceIds,
          description,
        });
      }}
      onCancel={() => props.history.goBack()}
    /> : <Spinner />
);

export default connect(
  null,
  mapDispatchToProps,
)(idProvider(withRouter(restrictionEntityProvider<Props>(EditRestriction))));
