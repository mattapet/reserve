/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import * as actions from 'actions/restrictions';
import { RootState } from 'reducers';
//#endregion

export const mapStateToProps = (state: RootState) => ({
  meta: state.view.restriction.create,
});

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onSubmit: (
    dateStart: Date,
    dateEnd: Date,
    resourceIds: number[],
    description?:  string,
  ) =>
    dispatch(actions.create.request(({
      dateStart,
      dateEnd,
      resourceIds,
      description,
    }))),
  onCancel: () => dispatch(actions.create.cancel()),
});
