/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router-dom';

import Details from '../components/details';
import { Restriction } from 'lib/types/Restriction';
import { Resource } from 'lib/types/Resource';
import * as Data from 'components/Data';
import {
  mapStateToProps,
  mapDispatchToProps,
} from './selectors';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly resources: { [id: number]: Resource };
  readonly restrictions: { readonly [id: number]: Restriction };
  readonly fetchById: (id: number) => any;
}
//#endregion

const RestrictionDetails = (props: Props) => (
  <Data.Edit<Restriction>
    title="Restriction Details"
    meta={{} as any}
    data={props.restrictions}
    idExtractor={() => (props.match.params as any).id}
    fetchById={props.fetchById}
    onComplete={() => props.history.replace('/restrictions')}
  >
    <Details
      resources={props.resources}
      restrictions={props.restrictions}
    />
  </Data.Edit>
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(RestrictionDetails));
