/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Row, Col, Button } from 'antd';
import { withRouter, RouteComponentProps } from 'react-router';

import { Restriction } from 'lib/types/Restriction';
import { Resource } from 'lib/types/Resource';
//#endregion

//#region Component interfaces
export interface Props extends RouteComponentProps {
  readonly restrictions: { readonly [id: number]: Restriction };
  readonly resources: { readonly [id: number]: Resource };
}
//#endregion

//#region Styled
const LabelText = styled.span`
  padding-right: 10px;
  font-weight: 700;
  float: right;
`;
//#endregion

const Label: React.FunctionComponent = ({ children }) => (
  <Col span={3}>
    <LabelText>{ children }</LabelText>
  </Col>
);
const Text: React.FunctionComponent = ({ children }) => (
  <Col span={9}>
    <span>{ children }</span>
  </Col>
);

const RestrictionDetails: React.FunctionComponent<Props> = (props) => {
  const { restrictions, resources } = props;
  const restriction = restrictions[(props.match.params as any).id];
  if (!restriction) {
    return null;
  }

  return (
    <React.Fragment>
      <Row>
        <Label>Date Start:</Label>
        <Text>{ restriction.dateStart.toISOString() }</Text>
      </Row>
      <Row>
        <Label>Date End:</Label>
        <Text>{ restriction.dateEnd.toISOString() }</Text>
      </Row>
      <Row>
        <Label>Resources:</Label>
        <Text>
        <Link to={`/resources/${restriction.resourceIds[0]}/details`}>
            {resources[restriction.resourceIds[0]]
              && resources[restriction.resourceIds[0]].name
              || restriction.resourceIds[0]
            }
          </Link>
        </Text>
      </Row>
      {restriction.resourceIds.slice(1).map(id => (
        <Row>
          <Label />
          <Text>
            <Link to={`/resources/${id}/details`}>
              {resources[id] && resources[id].name || id}
            </Link>
          </Text>
        </Row>
      ))}
      <Row>
        <Label>Description:</Label>
        <Text>{ restriction.description || '' }</Text>
      </Row>
      <Row>
        <Button onClick={() => props.history.goBack()}>
          Back
        </Button>
        <Button>
          <Link to={`/restrictions/${restriction.id}/edit`}>
            Edit
          </Link>
        </Button>
      </Row>
    </React.Fragment>
  );
};

export default withRouter(RestrictionDetails);
