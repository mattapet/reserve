/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';

import View from './view';
import { Restriction } from 'lib/types/Restriction';
//#endregion

//#region Component interfaces
export interface Props {
  readonly defaultValue?: Restriction;
  readonly onSubmit: (
    dateStart: Date,
    dateEnd: Date,
    resourceIds: number[],
    description?: string,
  ) => any;
  readonly onCancel: () => any;
}

interface State {
  readonly dateStart: Date;
  readonly dateEnd: Date;
  readonly resourceIds: number[];
  readonly description?: string;
}
//#endregion

class FormController extends React.Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    const { defaultValue } = props;
    this.state = {
      dateStart: defaultValue && defaultValue.dateStart || new Date(),
      dateEnd: defaultValue && defaultValue.dateEnd || new Date(),
      resourceIds: defaultValue && defaultValue.resourceIds || [],
      description: defaultValue && defaultValue.description,
    };
  }

  public render() {
    return (
      <View
        {...this.state}
        onCancel={this.props.onCancel}
        onSubmit={this.handleSubmit}
        onDateRangeChange={this.handleDateRangeChange}
        onResourceIdsChange={this.handleResourceIdsChange}
        onDescriptionChange={this.handleDescriptionChange}
      />
    );
  }

  private handleDateRangeChange = (dateRange: [string, string]) => {
    const [ dateStart, dateEnd ] = dateRange;
    this.setState({
      dateStart: new Date(dateStart),
      dateEnd: new Date(dateEnd),
    });
  };

  private handleResourceIdsChange = (resourceIds: number[]) => {
    this.setState({ resourceIds });
  };

  private handleDescriptionChange = (
    e: React.FormEvent<HTMLTextAreaElement>,
  ) => {
    const description = e.currentTarget.value;
    this.setState({ description });
  };

  private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { dateStart, dateEnd, resourceIds, description } = this.state;
    this.props.onSubmit(dateStart, dateEnd, resourceIds, description);
  }
}

export default FormController;
