/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import * as moment from 'moment';
import { Form, DatePicker, Button, Input } from 'antd';
import ResourceSelect from 'components/ResourceSelect';
//#endregion

//#region Component interfaces
export interface Props {
  readonly dateStart: Date;
  readonly dateEnd: Date;
  readonly resourceIds: number[];
  readonly description?: string;
  readonly onDateRangeChange: (dateRagnge: [string, string]) => any;
  readonly onResourceIdsChange: (ids: number[]) => any;
  readonly onDescriptionChange: (
    e: React.FormEvent<HTMLTextAreaElement>,
  ) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
  readonly onCancel: () => any;
}
//#endregion

const View: React.FunctionComponent<Props> = (props) => (
  <Form onSubmit={props.onSubmit}>
    <DatePicker.RangePicker
      onChange={(_, range) => props.onDateRangeChange(range)}
      value={[moment(props.dateStart), moment(props.dateEnd)]}
    />
    <ResourceSelect
      value={props.resourceIds}
      onChange={props.onResourceIdsChange}
    />

    <Input.TextArea
      placeholder="Description..."
      value={props.description}
      onChange={props.onDescriptionChange}
    />

    <Button onClick={props.onCancel}>Cancel</Button>
    <Button type="primary" htmlType="submit">Submit</Button>
  </Form>
);

export default View;
