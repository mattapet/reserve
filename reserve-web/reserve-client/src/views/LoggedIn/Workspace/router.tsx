/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { UserRole } from 'lib/types/UserRole';
import NotFound from 'views/NotFound';
import Spinner from 'components/Spinner';
const MyReservations = React.lazy(() => import('./MyReservations'));
const Overview = React.lazy(() => import('./Overview'));
const Reservations = React.lazy(() => import('./Reservations'));
const Resources = React.lazy(() => import('./Resources'));
const Users = React.lazy(() => import('./Users'));
const ReservationSettings =
  React.lazy(() => import('./Settings/ReservationSettings'));
const UserSettings = React.lazy(() => import('./Settings/UserSettings'));
const LoginAuthorities =
  React.lazy(() => import('./Settings/LoginAuthorities'));
const Webhooks = React.lazy(() => import('./Settings/Webhooks'));
const Workspace = React.lazy(() => import('./Settings/Workspace'));
//#endregion

const userRoutes = [
  <Redirect key="/" from="/" to="overview" push={false} exact />,
  <Redirect key="/login" from="/login" to="overview" push={false} exact />,
  <Redirect
    key="/register"
    from="/register"
    to="overview"
    push={false}
    exact
  />,
  <Route key="/overview" path="/overview" component={Overview} />,
  <Route
    key="/my_reservations"
    path="/my_reservations"
    component={MyReservations}
  />,
];

const maintainerRoutes = [
  ...userRoutes,

  <Route key="/resources" path="/resources" component={Resources} />,
  <Route key="/reservations" path="/reservations" component={Reservations} />,
  <Route key="/users" path="/users" component={Users} />,
];

const ownerRoutes = [
  ...maintainerRoutes,

  <Redirect
    key="/settings"
    from="/settings"
    to="/settings/reservations"
    exact
  />,

  <Route
    key="/settings/reservations"
    path="/settings/reservations"
    component={ReservationSettings}
  />,
  <Route
    key="/settings/users"
    path="/settings/users"
    component={UserSettings}
  />,
  <Route
    key="/settings/authorities"
    path="/settings/authorities"
    component={LoginAuthorities}
  />,
  <Route
    key="/settings/webhooks"
    path="/settings/webhooks"
    component={Webhooks}
  />,
  <Route
    key="/settings/workspace"
    path="/settings/workspace"
    component={Workspace}
  />,
];

export interface Props {
  readonly role: UserRole;
}

const Router: React.FunctionComponent<Props> = (props) => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      {props.role === UserRole.user ?
        userRoutes : props.role === UserRole.maintainer ?
        maintainerRoutes :
        ownerRoutes}
      <Route component={NotFound} />
    </Switch>
  </React.Suspense>
);

export default Router;
