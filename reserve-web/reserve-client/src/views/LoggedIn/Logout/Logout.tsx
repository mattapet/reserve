/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect } from 'react';

import Spinner from 'components/Spinner';
//#endregion

//#region Component interfaces
export interface Props {
  readonly onLogout: () => any;
  readonly onLogoutCancel: () => any;
}
//#endregion

const Logout: React.FunctionComponent<Props> = (props) => {
  const { onLogout, onLogoutCancel } = props;

  useEffect(() => {
    onLogout();
    return () => onLogoutCancel();
  }, [onLogout, onLogoutCancel]);

  return <Spinner />;
};

export default Logout;
