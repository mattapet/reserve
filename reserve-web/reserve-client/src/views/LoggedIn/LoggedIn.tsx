/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Layout } from 'antd';
import styled from 'styled-components';

import { Header } from 'components/Navigation';
import Router from './router';
//#endregion

//#region Component interfaces
export interface Props {

}
//#endregion

//#region Styled
const Container = styled(Layout)`
  height: 100%;
`;
//#endregion

const LoggedIn: React.FunctionComponent<Props> = () => (
  <Container>
    <Header/>
    <Router />
  </Container>
);

export default LoggedIn;
