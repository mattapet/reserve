/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';

import View from './view';
import { Profile } from 'lib/types/Profile';
//#endregion

//#region Component interfaces
export interface Props {
  readonly pending: boolean;
  readonly profile: Profile;
  readonly onSubmit: (profile: Profile) => any;
}
//#endregion

const Profile: React.FunctionComponent<Props> = (props) => {
  const [firstName, setFirstName] = useState(props.profile.firstName);
  const [lastName, setLastName] = useState(props.profile.lastName);
  const [phone, setPhone] = useState(props.profile.phone || '');

  function handleSubmit(e: React.FormEvent<HTMLElement>) {
    e.preventDefault();
    props.onSubmit({ firstName, lastName, phone });
  }

  return (
    <View
      pending={props.pending}
      firstName={firstName}
      lastName={lastName}
      phone={phone}
      onFirstNameChange={setFirstName}
      onLastNameChange={setLastName}
      onPhoneChange={setPhone}
      onSubmit={handleSubmit}
    />
  );
};

export default Profile;
