/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import { Layout, Card, Form, Button } from 'antd';

import Input from 'components/Input';
//#endregion

//#region Component interfaces
export interface Props {
  readonly firstName: string;
  readonly lastName: string;
  readonly phone: string;
  readonly pending: boolean;
  readonly onFirstNameChange: (value: string) => any;
  readonly onLastNameChange: (value: string) => any;
  readonly onPhoneChange: (value: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLElement>) => any;
}
//#endregion

//#region Styled
const Container = styled(Layout)`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const StyledForm = styled(Form)`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-content: center;

  * {
    margin: 5px;
  }
`;

const StyledButton = styled(Button)`
  width: 100%;
`;
//#endregion

const View: React.FunctionComponent<Props> = (props) => (
  <Container>
    <Card title="Profile">
      <StyledForm onSubmit={props.onSubmit}>
        <Input
          placeholder="First name"
          value={props.firstName}
          onChange={props.onFirstNameChange}
          autoCapitalize="words"
        />
        <Input
          placeholder="Last name"
          value={props.lastName}
          onChange={props.onLastNameChange}
          autoCapitalize="words"
        />
        <Input
          placeholder="Phone"
          value={props.phone}
          onChange={props.onPhoneChange}
        />
        <StyledButton
          type="primary"
          htmlType="submit"
          onClick={props.onSubmit}
          loading={props.pending}
        >
          Submit
        </StyledButton>
      </StyledForm>
    </Card>
  </Container>
);

export default View;
