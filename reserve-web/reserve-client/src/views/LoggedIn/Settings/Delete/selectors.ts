/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Dispatch } from 'redux';

import { RootState } from 'reducers';
import { userActions } from 'actions';
//#endregion

export const mapStateToProps = (state: RootState) => {
  if (!state.auth.isLoggedIn) {
    throw new Error('User must be logged in.');
  }

  return {
    userId: state.auth.identity.id,
    email: state.auth.identity.email,
    pending: state.view.user.deleteMe.pending,
  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  onDelete: () => dispatch(userActions.deleteMe.request()),
});
