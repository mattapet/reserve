/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { connect } from 'react-redux';

import Delete from './Delete';
import { mapStateToProps, mapDispatchToProps } from './selectors';
//#endregion

export { Props } from './Delete';

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Delete);
