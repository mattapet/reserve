/*
 * Copyright (c) 2019-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
//#endregion

export default () => <h1>Password</h1>;
