/*
 * Copyright (c) 2019-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { connect } from 'react-redux';

import Password from './Password';
import { mapStateToProps, mapDispatchToProps } from './selectors';
//#endregion

export { Props } from './Password';

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Password);

