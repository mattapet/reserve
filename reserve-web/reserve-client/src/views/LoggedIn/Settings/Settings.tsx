/*
 * Copyright (c) 2019-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import { Layout } from 'antd';

import Sidebar from './components/Sidebar';
import Router from './router';
//#endregion

//#region Styled
const Container = styled(Layout)`
  height: 100%;
`;
//#endregion


const Settings: React.FunctionComponent = () => (
  <Container>
    <Sidebar />
    <Router />
  </Container>
);

export default Settings;
