/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Layout, Menu } from 'antd';
import { NavLink, RouteComponentProps, withRouter } from 'react-router-dom';
//#endregion

const Sidebar: React.FunctionComponent<RouteComponentProps> = (props) => (
  <Layout.Sider>
    <Menu
      mode="inline"
      theme="dark"
      defaultSelectedKeys={[props.location.pathname]}
    >
      <Menu.Item key="/profile/settings/password">
        <NavLink to="/profile/settings/password">
          Password
        </NavLink>
      </Menu.Item>,
      <Menu.Item key="/profile/settings/delete">
        <NavLink to="/profile/settings/delete">
          Delete account
        </NavLink>
      </Menu.Item>
    </Menu>
  </Layout.Sider>
);

export default withRouter(Sidebar);
