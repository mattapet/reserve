/*
 * Copyright (c) 2019-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Spinner from 'components/Spinner';
const Delete = React.lazy(() => import('./Delete'));
const Password = React.lazy(() => import('./Password'));
//#endregion

const Router: React.FunctionComponent = () => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      <Redirect
        key="/profile/settings"
        from="/profile/settings"
        to="/profile/settings/password"
        exact
      />
      <Route
        key="/profile/settings/password"
        path="/profile/settings/password"
        component={Password}
      />
      <Route
        key="/profile/settings/delete"
        path="/profile/settings/delete"
        component={Delete}
      />
    </Switch>
  </React.Suspense>
);

export default Router;
