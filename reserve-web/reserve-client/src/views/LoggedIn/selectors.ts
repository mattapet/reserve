/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { RootState } from 'reducers';
//#endregion

export const mapStateToProps = (state: RootState) => {
  if (!state.auth.isLoggedIn) {
    throw Error('Invalid state');
  }
  return  {
    role: state.auth.identity.role,
  };
};
