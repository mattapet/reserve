/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Epic } from 'redux-observable';
import { of, defer, merge } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  catchError,
  filter,
  map,
  takeUntil,
  take,
  mergeMap,
} from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import { apiRequest } from 'lib/apiRequest';
import { RootAction } from '../../actions';
import * as actions from '../../actions/request';

import { refresh } from '../../actions/auth';
import { RootState } from '../../reducers';
//#endregion

const throttleSet: Set<string> = new Set();
export const requestEpic: Epic<RootAction, RootAction, RootState> =
(action$, store$) => (
  action$.pipe(
    filter(isActionOf(actions.request)),
    // Add optional throttling
    filter(({ meta }) => {
      // Allow all action that do not specify throttling
      if (!meta.throttle) {
        return true;
      }
      // Make sure to identify each request by HTTP method and url
      const httpRequest = `${meta.method} ${meta.url}`;
      // Drop request if such request is being processed
      if (throttleSet.has(httpRequest)) {
        return false;
      }
      // Add request to trottled collection
      throttleSet.add(httpRequest);
      // Remove request from throttled set
      setTimeout(() => throttleSet.delete(httpRequest), meta.throttle);
      return true;
    }),
    // Make sure we hadle all request actions
    mergeMap(({ payload, meta }) =>
      // Make observable that emits upon each subcription
      defer(() => apiRequest(meta.method, meta.url, payload, meta.headers))
      .pipe(
        map(res => meta.onSuccess(res.response)),
        catchError((err: AjaxError, caught) => {
          // Make sure Unauthorized error is emmited
          if (err.status !== 401 || !store$.value.auth.isLoggedIn) {
            return of(meta.onFail(err));
          }

          return merge(
            action$.pipe(
              // Listen for refresh success actions
              filter(isActionOf(refresh.success)),
              takeUntil(action$.pipe(filter(isActionOf(refresh.fail)))),
              take(1),
              // Retry the request
              mergeMap(() => caught)
            ),
            // Emit refresh request action
            of(refresh.request()),
          );
        }),
      ),
    ),
  )
);
