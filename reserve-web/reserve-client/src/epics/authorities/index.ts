/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineEpics, Epic } from 'redux-observable';
import { filter, map, } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';

import { RootAction } from '../../actions';
import { get, post, put, del } from '../../actions/request';
import * as actions from '../../actions/authorities';
//#endregion

export const fetchEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.fetch.request)),
    map(({meta}) => get(null, meta)),
  )
);

export const registerEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.register.request)),
    map(({payload, meta}) => post(payload, meta)),
  )
);

export const editEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.edit.request)),
    map(({payload, meta}) => put(payload, meta)),
  )
);

export const removeEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.remove.request)),
    map(({meta}) => del(null, meta)),
  )
);

export const authorityEpic = combineEpics(
  fetchEpic,
  registerEpic,
  editEpic,
  removeEpic,
);
