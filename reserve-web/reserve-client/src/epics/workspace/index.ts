/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import config from 'config';
import { combineEpics, Epic } from 'redux-observable';
import { of, merge, EMPTY } from 'rxjs';
import { filter, map, tap, flatMap, } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';

import { RootAction } from 'actions';
import { get, del } from 'actions/request';
import * as actions from 'actions/workspace';
import * as settingsActions from 'actions/settings';
import * as authActions from 'actions/auth';
//#endregion

export const fetchByNameEpic: Epic<RootAction> = (action$) => (
  merge(
    action$.pipe(
      filter(isActionOf(actions.fetchByName.request)),
      map(({meta}) => get(null, meta)),
    ),
    action$.pipe(
      filter(isActionOf(actions.fetchByName.success)),
      flatMap(({ payload }) => merge(
        of(settingsActions.fetch.request(payload.id)),
        of(authActions.fetchLoginAuthorities.request(payload.id)),
      ))
    )
  )
);

export const removeEpic: Epic<RootAction> = (action$) => (
  merge(
    action$.pipe(
      filter(isActionOf(actions.remove.request)),
      map(({meta}) => del(null, meta)),
    ),
    action$.pipe(
      filter(isActionOf(actions.remove.success)),
      tap(() => window.location.href = config.landingUrl),
      flatMap(() => EMPTY),
    )
  )
);

export const workspaceEpic = combineEpics(
  fetchByNameEpic,
  removeEpic,
);
