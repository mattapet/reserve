/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineEpics, Epic } from 'redux-observable';
import { filter, map, } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';

import { RootAction } from '../../actions';
import { get, put } from '../../actions/request';
import * as actions from '../../actions/settings';
import { authActions } from 'actions';
import { merge } from 'rxjs';
//#endregion

export const fetchEpic: Epic<RootAction> = (action$) => (
  merge(
    action$.pipe(
      filter(isActionOf(actions.fetch.request)),
      map(({meta}) => get(null, meta)),
    ),
    action$.pipe(
      filter(isActionOf(authActions.login.success)),
      map(({payload}) => actions.fetch.request(payload)),
    ),
  )
);

export const editUserEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.editUser.request)),
    map(({payload, meta}) => put(payload, meta)),
  )
);

export const editReservationEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.editReservation.request)),
    map(({payload, meta}) => put(payload, meta)),
  )
);

export const settingsEpic = combineEpics(
  fetchEpic,
  editReservationEpic,
  editUserEpic,
);
