/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineEpics, Epic } from 'redux-observable';
// import { Observable, merge } from 'rxjs';
import { filter, map, } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';

import { RootAction } from '../../actions';
import { get, post, put, del } from '../../actions/request';
import * as actions from '../../actions/restrictions';
// import * as resourceActions from '../../actions/resources';
// import { RootState } from '../../reducers';
// import { Restriction } from 'lib/types/Restriction';
//#endregion

export const fetchByIdEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.fetchById.request)),
    map(({meta}) => get(null, meta)),
  )
);

export const fetchEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.fetch.request)),
    map(({meta}) => get(null, meta)),
  )
);

export const createEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.create.request)),
    map(({payload, meta}) => post(payload, meta)),
  )
);

export const editEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.edit.request)),
    map(({payload, meta}) => put(payload, meta)),
  )
);

export const removeEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.remove.request)),
    map(({meta}) => del(null, meta)),
  )
);

// function fetchMerge(
//   action$: ActionsObservable<RootAction>,
// ): Observable<Restriction> {
//   return merge(
//     action$.pipe(
//       filter(isActionOf(actions.fetch.success)),
//       flatMap(({ payload }) => payload)
//     ),
//     action$.pipe(
//       filter(isActionOf(actions.fetchById.success)),
//       map(({ payload }) => payload),
//     ),
//   );
// }

// export const resourceCompletionEpic: Epic<
//   RootAction,
//   RootAction,
//   RootState
// > = (action$, state$) => (
//   fetchMerge(action$).pipe(
//     flatMap(({ resourceIds }) =>
//       resourceIds
//         .filter(id => !state$.value.resource.data[id])
//         .map(id => resourceActions.fetchById.request(id))
//     )
//   )
// );

export const restrictionEpic = combineEpics(
  fetchByIdEpic,
  fetchEpic,
  createEpic,
  editEpic,
  removeEpic,
  // resourceCompletionEpic,
);
