/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineEpics, Epic } from 'redux-observable';
import { filter, map, } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';

import { RootAction } from 'actions';
import { get, patch, put, del } from 'actions/request';
import * as actions from 'actions/users';
import * as authActions from 'actions/auth';
import { merge } from 'rxjs';
//#endregion

export const fetchByIdEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.fetchById.request)),
    map(({meta}) => get(null, meta)),
  )
);

export const fetchEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.fetch.request)),
    map(({meta}) => get(null, meta)),
  )
);

export const updateRoleEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.updateRole.request)),
    map(({payload, meta}) => patch(payload, meta)),
  )
);

export const toggleBanEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.toggleBan.request)),
    map(({meta}) => patch(null, meta)),
  )
);

export const getMeEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.getMe.request)),
    map(({meta}) => get(null, meta)),
  )
);

export const updateProfileEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(actions.updateProfile.request)),
    map(({payload, meta}) => put(payload, meta)),
  )
);

export const deleteMeEpic: Epic<RootAction> = (action$) => (
  merge(
    action$.pipe(
      filter(isActionOf(actions.deleteMe.request)),
      map(({meta}) => del(null, meta)),
    ),
    action$.pipe(
      filter(isActionOf(actions.deleteMe.success)),
      map(() => authActions.logout.success()),
    )
  )
);

export const userEpic = combineEpics(
  fetchByIdEpic,
  fetchEpic,
  updateRoleEpic,
  toggleBanEpic,
  getMeEpic,
  updateProfileEpic,
  deleteMeEpic,
);
