/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineEpics, Epic } from 'redux-observable';
import { of, merge } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';
import {
  filter,
  map,
  catchError,
  takeUntil,
  switchMap,
} from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';

import { RootAction } from '../../actions';
import { authActions, userActions } from 'actions';
import { post, get } from '../../actions/request';
import { refreshToken } from 'lib/apiRequest';
import { RootState } from 'src/reducers';
//#endregion

export const fetchLoginAuthoritiesEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(authActions.fetchLoginAuthorities.request)),
    map(({meta}) => get(null, meta)),
  )
);

export const loginAuthorityEpic: Epic<RootAction> = (action$) => (
  merge(
    action$.pipe(
      filter(isActionOf(authActions.loginAuthority.request)),
      map(({payload, meta}) => post(payload, meta)),
    ),
    action$.pipe(
      filter(isActionOf(authActions.loginAuthority.success)),
      map(({payload}) => authActions.login.success(payload)),
    ),
    action$.pipe(
      filter(isActionOf(authActions.loginAuthority.fail)),
      map(({payload}) => authActions.login.fail(payload)),
    ),
  )
);

export const confirmEmailEpic: Epic<RootAction> = (action$) => (
  merge(
    action$.pipe(
      filter(isActionOf(authActions.confirmEmail.request)),
      map(({payload, meta}) => post(payload, meta)),
    ),
    action$.pipe(
      filter(isActionOf(authActions.confirmEmail.success)),
      map(({payload}) => authActions.login.success(payload)),
    ),
    action$.pipe(
      filter(isActionOf(authActions.confirmEmail.fail)),
      map(({payload}) => authActions.login.fail(payload)),
    ),
  )
);

export const loginEpic: Epic<RootAction> = (action$) => (
  merge(
    action$.pipe(
      filter(isActionOf(authActions.login.request)),
      map(({payload, meta}) => post(payload, meta)),
    ),
    action$.pipe(
      filter(isActionOf(authActions.login.success)),
      map(() => userActions.getMe.request()),
    )
  )
);

export const logoutEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(authActions.logout.request)),
    map(({meta}) => post(null, meta)),
));

export const registerEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(authActions.register.request)),
    map(({payload, meta}) => post(payload, meta)),
));

export const setPasswordEpic: Epic<RootAction> = (action$) => (
  action$.pipe(
    filter(isActionOf(authActions.setPassword.request)),
    map(({payload, meta}) => post(payload, meta)),
));

export const refreshEpic: Epic<
  RootAction,
  RootAction,
  RootState
> = (action$, state$) => (
  merge(
    action$.pipe(
      filter(isActionOf(authActions.refresh.request)),
      // Make sure only one refresh request is active at a time
      switchMap(() => {
        const { auth } = state$.value;
        if (!auth.isLoggedIn) {
          // Make sure we have user credentials
          throw new Error();
        }
        // Perform the refresh request
        return refreshToken(auth.tokens.refreshToken);
      }),
      takeUntil(action$.pipe(filter(isActionOf(authActions.refresh.cancel)))),
      // Create success action
      map(res => authActions.refresh.success(res.response)),
      // Create fail action
      catchError((err: AjaxError) => of(authActions.refresh.fail(err))),
    ),
    action$.pipe(
      filter(isActionOf(authActions.refresh.fail)),
      filter(action => (action.payload as AjaxError).status === 400),
      map(() => authActions.logout.success()),
    )
  )
);

export const authEpic = combineEpics(
  fetchLoginAuthoritiesEpic,
  loginAuthorityEpic,
  loginEpic,
  logoutEpic,
  registerEpic,
  confirmEmailEpic,
  setPasswordEpic,
  refreshEpic,
);
