/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { combineEpics } from 'redux-observable';
import { authEpic } from './auth';
import { authorityEpic } from './authorities';
import { requestEpic } from './request';
import { resourceEpic } from './resource';
import { reservationEpic } from './reservation';
import { restrictionEpic } from './restriction';
import { settingsEpic } from './settings';
import { userEpic } from './user';
import { webhookEpic } from './webhook';
import { workspaceEpic } from './workspace';
//#endregion

export const rootEpic = combineEpics(
  authEpic,
  authorityEpic,
  requestEpic,
  resourceEpic,
  reservationEpic,
  restrictionEpic,
  settingsEpic,
  userEpic,
  webhookEpic,
  workspaceEpic,
);
