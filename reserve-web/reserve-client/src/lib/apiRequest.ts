/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as querystring from 'querystring';
import config from 'config';
import { Observable } from 'rxjs';
import { ajax, AjaxResponse } from 'rxjs/ajax';

import store from '../store';
import { HTTPMethod } from './types/HTTPMethod';
//#endregion

function _request(
  method: HTTPMethod,
  url: string,
  data?: { [key: string]: any } | null,
  headers?: { [key: string]: any } | null,
): Observable<AjaxResponse> {
  const { auth } = store.getState();
  url = `${config.apiUrl}${url}`;

  if (!headers) {
    headers = {};
  }
  if (!headers.Authorization && auth.isLoggedIn) {
    headers.Authorization = `Bearer ${auth.tokens.accessToken}`;
  }
  if (!headers['Content-Type']) {
    headers['Content-Type'] = 'application/json';
  }

  if (method === HTTPMethod.GET && data) {
    const query = querystring.stringify(data);
    url = `${url}${url.indexOf('?') < 0 ? '?' : '&'}${query}`;
  }

  switch (method) {
  case HTTPMethod.GET:
    return ajax.get(url, headers);
  case HTTPMethod.POST:
    return ajax.post(url, data, headers);
  case HTTPMethod.PUT:
    return ajax.put(url, data, headers);
  case HTTPMethod.PATCH:
    return ajax.patch(url, data, headers);
  case HTTPMethod.DELETE:
    return ajax.delete(url, headers);
  }
}

export function refreshToken(token: string) {
  return _request(HTTPMethod.POST, 'api/v1/oauth2/token', {
    grant_type: 'refresh_token',
    refresh_token: token,
  });
}

export function apiRequest(
  method: HTTPMethod,
  url: string,
  data?: { [key: string]: any } | null,
  headers?: { [key: string]: any } | null,
): Observable<AjaxResponse> {
  return _request(method, url, data, headers);
}
