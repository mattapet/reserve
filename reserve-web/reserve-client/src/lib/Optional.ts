/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export type Optional<T> = Some<T> | None<T>;

const enum OptionalKind {
  some = 'some',
  none = 'none',
}

export class Some<T> {
  public constructor(private readonly value: T) { }

  public get kind(): OptionalKind.some {
    return OptionalKind.some;
  }

  public get(): T {
    return this.value;
  }

  public getOr(defaultValue: () => T): T {
    return this.value;
  }

  public getOrDefault(defaultValue: T): T {
    return this.value;
  }

  public getOrUndefined(): T | undefined {
    return this.value;
  }

  public map<U>(transformer: (wrapped: T) => U): Optional<U> {
    return some(transformer(this.value));
  }

  public flatMap<U>(transformer: (wrapped: T) => Optional<U>): Optional<U> {
    return transformer(this.value);
  }

  public equals(other: Optional<T>): boolean {
    switch (other.kind) {
    case OptionalKind.some: return this.value === other.value;
    case OptionalKind.none: return false;
    }
  }

  public nequals(other: Optional<T>): boolean {
    return !this.equals(other);
  }
}

// tslint:disable:max-classes-per-file
export class None<T = never> {
  public get kind(): OptionalKind.none {
    return OptionalKind.none;
  }

  public get(): T {
    throw new TypeError('None found while unwrapping an optional value.');
  }

  public getOr(defaultValue: () => T): T {
    return defaultValue();
  }

  public getOrDefault(defaultValue: T): T {
    return defaultValue;
  }

  public getOrUndefined(): T | undefined {
    return undefined;
  }

  public map<U>(transformer: (wrapped: T) => U): Optional<U> {
    return none;
  }

  public flatMap<U>(transformer: (wrapped: T) => U): Optional<U> {
    return none;
  }

  public equals(other: Optional<T>): boolean {
    switch (other.kind) {
    case OptionalKind.some: return false;
    case OptionalKind.none: return true;
    }
  }

  public nequals(other: Optional<T>): boolean {
    return !this.equals(other);
  }
}

export function tryOptional<T>(throwable: () => T): Optional<T> {
  try {
    return some(throwable());
  } catch (e) {
    return none;
  }
}

export function from<T>(wrapped?: T): Optional<T> {
  return wrapped ? some(wrapped) : none;
}

export function some<T>(wrapped: T): Optional<T> {
  return new Some(wrapped);
}

export const none = new None<any>();
