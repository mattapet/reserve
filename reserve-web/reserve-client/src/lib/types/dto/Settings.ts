
/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TimeUnit } from '../TimeUnit';
import { Timeframe } from '../Timeframe';
import { UserRole } from '../UserRole';
//#endregion

export interface SettingsDTO {
  readonly id: number;
  readonly reservations: ReservationSettingsDTO;
  readonly users: UserSettingsDTO;
}

export interface ReservationSettingsDTO {
  readonly time_unit: TimeUnit;
  readonly max_reservations: number | null;
  readonly max_range: Timeframe | null;
  readonly max_offset: Timeframe | null;
}

export interface UserSettingsDTO {
  readonly allowed_registrations: boolean;
  readonly allowed_invitations: boolean;
  readonly allowed_to_invite: UserRole;
}
