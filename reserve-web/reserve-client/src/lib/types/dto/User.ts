/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ProfileDTO } from './Profile.dto';
import { UserRole } from '../UserRole';
//#endregion

export interface UserDTO {
  readonly id: string;
  readonly workspace: string;
  readonly role: UserRole;
  readonly last_login: string;
  readonly email?: string;
  readonly profile?: ProfileDTO;
  readonly banned: boolean;
}
