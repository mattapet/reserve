/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export interface LoginRequestDTO {
  readonly grant_type: 'password';
  readonly username: string;
  readonly password: string;
  readonly workspaceId: string;
}
