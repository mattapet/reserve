/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationState } from '../ReservationState';
//#endregion

export interface ReservationDTO {
  readonly id: string;
  readonly date_start: string;
  readonly date_end: string;
  readonly all_day: boolean;
  readonly resource_ids: number[];
  readonly notes?: string;
  readonly state: ReservationState;
  readonly user_id: string;
}
