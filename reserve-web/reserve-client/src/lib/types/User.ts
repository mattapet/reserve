/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Profile } from './Profile';
import { UserRole } from './UserRole';
//#endregion

export interface User {
  readonly id: string;
  readonly workspace: string;
  readonly role: UserRole;
  readonly lastLogin: Date;
  readonly email?: string;
  readonly profile?: Profile;
  readonly banned?: boolean;
}
