/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationState } from './ReservationState';
//#endregion

export interface Reservation {
  readonly id: string;
  readonly dateStart: Date;
  readonly dateEnd: Date;
  readonly resourceIds: number[];
  readonly allDay: boolean;
  readonly notes?: string;
  readonly state: ReservationState;
  readonly userId: string;
}
