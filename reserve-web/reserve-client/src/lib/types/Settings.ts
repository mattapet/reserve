/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from './UserRole';
import { Timeframe } from './Timeframe';
import { TimeUnit } from './TimeUnit';
//#endregion

export interface Settings {
  readonly id: number;
  readonly reservations: ReservationSettings;
  readonly users: UserSettings;
}

export interface ReservationSettings {
  readonly timeUnit: TimeUnit;
  readonly maxReservations: number | null;
  readonly maxRange: Timeframe | null;
  readonly maxOffset: Timeframe | null;
}

export interface UserSettings {
  readonly allowedRegistrations: boolean;
  readonly allowedInvitations: boolean;
  readonly allowedToInvite: UserRole;
}
