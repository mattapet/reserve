/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ProfileMap } from './ProfileMap';
//#endregion

export interface Authority {
  readonly name: string;
  readonly description: string;
  readonly authorizeUrl: string;
  readonly tokenUrl: string;
  readonly profileUrls: string[];
  readonly profileMap: ProfileMap;
  readonly redirectUri: string;
  readonly clientId: string;
  readonly clientSecret: string;
  readonly scope: string[];
}
