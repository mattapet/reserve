/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as moment from 'moment';
//#endregion

export function date(
  value: Date | moment.Moment | number | string,
): string {
  return moment(value).utc().format('YYYY-MM-DD');
}

export function timestamp(
  value: Date | moment.Moment | number | string,
): string {
  return moment(value).format('YYYY-MM-DD HH:mm:SS');
}

export function ISOString(
  value: Date | moment.Moment | number | string,
): string {
  return moment(value).toISOString();
}
