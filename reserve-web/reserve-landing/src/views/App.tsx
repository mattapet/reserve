/*
 * Copyright (c) 2019-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import 'antd/dist/antd.css';
import React from 'react';
import { Layout } from 'antd';
import styled, { createGlobalStyle } from 'styled-components';

import Header from './components/Header';
import Router from './router';
//#endregion

const GlobalStyles = createGlobalStyle`
  html { height: 100%; }
  body { height: 100%; }
  #root { height: 100%; }
`;

const Container = styled(Layout)`
  height: 100%;
`;

const App: React.FunctionComponent = () => (
  <React.Fragment>
    <Container>
      <Header />
      <Router />
    </Container>
    <GlobalStyles />
  </React.Fragment>
);

export default App;
