/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import { Icon, Steps, Button, Card, Form, Layout } from 'antd';

import Success from './components/Success';
import Input from 'components/Input';
//#endregion

//#region Component interfaces
export interface Props {
  readonly pending: boolean;
  readonly retrieved: boolean;
  readonly email: string;
  readonly error: Error | null;
  readonly onEmailChange: (e: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
}
//#endregion

//#region Styled
const FormContainer = styled(Form)`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-content: center;

  * {
    margin: 5px;
  }
`;

const ContentWrapper = styled(Layout)`
  height: 100%;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const SubmitButton = styled(Button)`
  width: 100%;
`;
//#endregion

const Container: React.FunctionComponent = (props) => (
  <ContentWrapper>
    <Card title="Verify Email">
      {props.children}
    </Card>
  </ContentWrapper>
);

const View: React.FunctionComponent<Props> = (props) => {
  const complete = props.retrieved && !props.error;
  const status = (
    <Steps>
      <Steps.Step
        status={complete ? 'finish' : 'process'}
        icon={<Icon type="solution" />}
        title="Email Verification"
      />
      <Steps.Step
        status="wait"
        icon={<Icon type="idcard" />}
        title="Workpsace ID"
      />
      <Steps.Step
        status="wait"
        icon={<Icon type="key" />}
        title="Password"
      />
      <Steps.Step
        status="wait"
        icon={<Icon type="check-circle" />}
        title="Done"
      />
    </Steps>
  );

  if (complete) {
    return (
      <React.Fragment>
        {status}
        <Container>
          <Success email={props.email} />
        </Container>
      </React.Fragment>
    );
  }

  return (
    <React.Fragment>
      {status}
      <Container>
        <FormContainer onSubmit={props.onSubmit}>
          <Input
            type="email"
            placeholder="Email"
            value={props.email}
            onChange={props.onEmailChange}
            autoFocus
            required
            />
          <SubmitButton
            type="primary"
            htmlType="submit"
            loading={props.pending}
          >
            Submit
          </SubmitButton>
        </FormContainer>
      </Container>
    </React.Fragment>
  );
};

export default View;
