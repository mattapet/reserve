/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import config from 'config';

import View from './view';
//#endregion

const Register: React.FunctionComponent = () => {
  const [email, setEmail] = useState('');
  const [pending, setPending] = useState(false);
  const [retrieved, setRetrieved] = useState(false);
  const [error, setError] = useState<Error | null>(null);

  async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    await verifyEmail();
  }

  async function verifyEmail() {
    setPending(true);
    try {
      await fetch(
        `${config.apiUrl}api/v1/workspace/verify`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email,
            redirect_uri: `${config.landingUrl}/workspace/create/complete`,
          }),
        }
      );
      setError(null);
      setPending(false);
      setRetrieved(true);
    } catch (error) {
      setError(error);
      setPending(false);
      setRetrieved(true);
    }
  }

  return (
    <View
      pending={pending}
      retrieved={retrieved}
      error={error}
      email={email}
      onEmailChange={setEmail}
      onSubmit={handleSubmit}
    />
  );
};

export default Register;
