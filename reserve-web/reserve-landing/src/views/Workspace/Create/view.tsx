/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import { Steps, Icon } from 'antd';

import { FormStep } from './FormStep';
import Workspace from './components/Workspace';
import Password from './components/Password';
import SettingUp from './components/SettingUp';
//#endregion

//#region Component interfaces
export interface Props {
  readonly step: FormStep;
  readonly workspace: string;
  readonly password: string;
  readonly onWorkspaceChange: (workspace: string) => any;
  readonly onPasswordChange: (password: string) => any;
  readonly onCancel: () => any;
}
//#endregion

const View: React.FunctionComponent<Props> = (props) => (
  <React.Fragment>
    <Steps>
      <Steps.Step
        status={
          props.step < FormStep.email ?
          'wait' : props.step > FormStep.email ?
          'finish' : 'process'
        }
        icon={<Icon type="solution" />}
        title="Email Verification"
      />
      <Steps.Step
        status={
          props.step < FormStep.workspace ?
            'wait' : props.step > FormStep.workspace ?
              'finish' : 'process'
        }
        icon={<Icon type="idcard" />}
        title="Workpsace ID"
      />
      <Steps.Step
        status={
          props.step < FormStep.password ?
            'wait' : props.step > FormStep.password ?
              'finish' : 'process'
        }
        icon={<Icon type="key" />}
        title="Password"
      />
      <Steps.Step
        status={
          props.step < FormStep.loading ?
            'wait' : props.step > FormStep.loading ?
              'finish' : 'process'
        }
        icon={props.step === FormStep.loading ?
          <Icon type="loading" />: <Icon type="check-circle" />}
        title="Done"
      />
    </Steps>
    {
      props.step === FormStep.workspace ?
        <Workspace {...props} onSubmit={props.onWorkspaceChange} /> :
      props.step === FormStep.password ?
        <Password {...props} onSubmit={props.onPasswordChange} /> :
        <SettingUp />
    }
  </React.Fragment>
);

export default View;
