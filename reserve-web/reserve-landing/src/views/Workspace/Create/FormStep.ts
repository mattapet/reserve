/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export const enum FormStep {
  email = 0x1,
  workspace = 0x2,
  password = 0x4,
  loading = 0x8,
}
