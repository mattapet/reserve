/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
//#endregion

//#region Styled
const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;
//#endregion

const View: React.FunctionComponent = () => (
  <Container>
    <h4>Setting up workspace...</h4>
  </Container>
);

export default View;
