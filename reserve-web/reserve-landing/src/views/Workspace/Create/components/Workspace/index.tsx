/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';

import config from 'config';
import View from './view';
//#endregion

//#region Component interfaces
export interface Props {
  readonly workspace: string;
  readonly onSubmit: (workspace: string) => any;
}
//#endregion

const WorkspaceForm: React.FunctionComponent<Props> = (props) => {
  const [workspace, setWorkspace] = useState(props.workspace);
  const [pending, setPending] = useState(false);

  async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    setPending(true);
    try {
      const response =
        await fetch(`${config.apiUrl}api/v1/workspace?name=${
          encodeURIComponent(workspace)
      }`);
      if (response.status === 404) {
        props.onSubmit(workspace);
      } else {
        setPending(false);
      }
    } catch (e) {
      setPending(false);
    }
  }

  return (
    <View
      workspace={workspace}
      pending={pending}
      onWorkspaceChange={setWorkspace}
      onSubmit={handleSubmit}
    />
  );
};

export default WorkspaceForm;
