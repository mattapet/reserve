/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import { Button, Card, Form, Layout } from 'antd';
import Input from 'components/Input';
//#endregion

//#region Component interfaces
export interface Props {
  readonly pending: boolean;
  readonly password: string;
  readonly verifyPassword: string;
  readonly onPasswordChange: (value: string) => any;
  readonly onVerifyPasswordChange: (value: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
  readonly onCancel: () => any;
}
//#endregion

//#region Styled
const FormContainer = styled(Form as any)`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-content: center;

  * {
    margin: 5px;
  }
`;

const ContentWrapper = styled(Layout)`
  height: 100%;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const SubmitButton = styled(Button as any)`
  width: 100%;
`;
//#endregion

const Container: React.FunctionComponent<{}> = (props) => (
  <ContentWrapper>
    <Card title="Register Email">
      {props.children}
    </Card>
  </ContentWrapper>
);

const View: React.FunctionComponent<Props> = (props) => (
  <Container>
    <FormContainer onSubmit={props.onSubmit}>
      <Input
        type="password"
        placeholder="Password"
        value={props.password}
        onChange={props.onPasswordChange}
        required
        autoFocus
      />
      <Input
        type="password"
        placeholder="Verify Password"
        value={props.verifyPassword}
        onChange={props.onVerifyPasswordChange}
        required
      />
      <SubmitButton
        type="primary"
        htmlType="submit"
        loading={props.pending}
        disabled={
          props.password.length === 0 ||
          props.verifyPassword.length === 0
        }
      >
        Submit
      </SubmitButton>
      <SubmitButton onClick={props.onCancel}>
        Back
      </SubmitButton>
    </FormContainer>
  </Container>
);

export default View;
