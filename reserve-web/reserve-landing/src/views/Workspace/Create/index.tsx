/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import * as querystring from 'querystring';

import View from './view';
import { FormStep } from './FormStep';
import config from 'config';
import { redirect } from 'lib/redirect';
//#endregion

//#region Component interfaces
interface FormData {
  readonly workspace: string;
  readonly password: string;
  readonly verifyPassword: string;
}
//#endregion

const CreateWorkspace: React.FunctionComponent<RouteComponentProps> = (props) =>
{
  const [step, setStep] = useState(FormStep.workspace);
  const [formData, setFormData] = useState<FormData>({
    workspace: '',
    password: '',
    verifyPassword: '',
  });

  function handleWorkspaceChange(workspace: string) {
    setFormData({ ...formData,  workspace });
    setStep(FormStep.password);
  }

  async function handlePasswordChange(password: string) {
    setFormData({ ...formData, password });
    setStep(FormStep.loading);
    await handleSubmit({ ...formData, password });
  }

  function handleCancel() {
    switch (step) {
    case FormStep.password:
      return setStep(FormStep.workspace);
    default:
      throw new Error('Unreachable');
    }
  }

  async function handleSubmit(data: FormData) {
    const {
      workspace,
      password,
    } = data;
    const query = props.location.search.slice(1); // Drop leading `?`
    const { workspaceId, email, expiry, signature } = querystring.parse(query);

    try {
      const response = await fetch(
        `${config.apiUrl}api/v1/workspace/complete`, {
        method: 'POST',
        body: JSON.stringify({
          workspace_id: workspaceId,
          email,
          expiry,
          signature,
          workspace,
          password,
        }),
        headers: {
          ['Content-Type']: 'application/json',
        }
      });
      const { name } = await response.json();
      redirect(`${config.protocol}//${name}.${config.baseHost}`);
    } catch (e) {
      setStep(FormStep.password);
    }
  }

  return (
    <View
      {...formData}
      step={step}
      onWorkspaceChange={handleWorkspaceChange}
      onPasswordChange={handlePasswordChange}
      onCancel={handleCancel}
    />
  );
};

export default withRouter(CreateWorkspace);
