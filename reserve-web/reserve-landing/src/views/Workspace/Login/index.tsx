/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import View from './view';

import config from 'config';
import { redirect } from 'lib/redirect';
//#endregion

const Login: React.FunctionComponent = () => {
  const [workspace, setWorkspace] = useState('');
  const [pending, setPending] = useState(false);

  async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    setPending(true);
    await loginWorkspace();
  }

  async function loginWorkspace() {
    try {
      const response = await fetch(
        `${config.apiUrl}api/v1/workspace?name=${encodeURIComponent(workspace)}`
      );
      const { name } = await response.json();
      if (!name) throw new Error('Invalid workspace-id');
      redirect(`${config.protocol}//${name}.${config.baseHost}`);
    } catch (error) {
      setPending(false);
    }
  }

  return (
    <View
      workspace={workspace}
      onWorkspaceChange={setWorkspace}
      pending={pending}
      onSubmit={handleSubmit}
    />
  );
};

export default Login;
