/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as React from 'react';
import styled from 'styled-components';
import { Button, Card, Form,  Layout } from 'antd';

import Input from 'components/Input';
//#endregion

//#region Component interfaces
interface LoginViewProps {
  readonly pending: boolean;
  readonly workspace: string;
  readonly onWorkspaceChange: (value: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
}
//#endregion

//#region Styled
const FormContainer = styled(Form as any)`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-content: center;

  * {
    margin: 5px;
  }
`;

const ContentWrapper = styled(Layout)`
  height: 100%;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const SubmitButton = styled(Button as any)`
  width: 100%;
`;
//#endregion

const Container: React.FunctionComponent<{}> = (props) => (
  <ContentWrapper>
    <Card title="Login">
      {props.children}
    </Card>
  </ContentWrapper>
);

const View: React.FunctionComponent<LoginViewProps> = (props) => (
  <Container>
    <FormContainer onSubmit={props.onSubmit}>
      <Input
        type="text"
        placeholder="your-workspace-name"
        value={props.workspace}
        onChange={props.onWorkspaceChange}
      />
      <SubmitButton type="primary" htmlType="submit" loading={props.pending}>
        Continue
      </SubmitButton>
    </FormContainer>
  </Container>
);

export default View;
