/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import Home from './Home';
import About from './About';
import Docs from './Docs';
import Login from './Workspace/Login';
import Email from './Workspace/Email';
import Create from './Workspace/Create';
import NotFound from './NotFound';
//#endregion

const Router: React.FunctionComponent = () => (
  <Switch>
    <Route path="/" component={Home} exact />
    <Route path="/about" component={About} exact />
    <Route path="/docs" component={Docs} exact />
    <Route path="/workspace/login" component={Login} />
    <Redirect
      from="/workspace/create"
      to="/workspace/create/verify"
      exact
    />
    <Route path="/workspace/create/verify" component={Email} />
    <Route path="/workspace/create/complete" component={Create} />
    <Route component={NotFound} />
  </Switch>
);

export default Router;
