/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Button, Layout, Menu, Dropdown } from 'antd';
import {
  Link,
  NavLink,
  RouteComponentProps,
  withRouter,
} from 'react-router-dom';
import styled from 'styled-components';
//#endregion

const LayoutHeader = styled(Layout.Header)`
  display: flex;
  justify-content: space-between;
`;

const Title = styled.h1`
  font-size: 36px;
  color: whitesmoke;
  display: inline-block;
  margin: 0;
  cursor: pointer;

  transition: all .3s;

  &:hover {
    color: #40a9ff;
  }
`;

const MenuBar = styled(Menu)`
  float: left;
`;

const WorkspacePopover = styled(Dropdown)`
  justify-self: center;
  align-self: center;
`;

const WorkpsaceOptions: React.FunctionComponent<RouteComponentProps<any>> =
  (props) => (
  <Menu
    mode="vertical"
    selectedKeys={[props.location.pathname]}
  >
    <Menu.Item key="/workspace/login">
      <NavLink to="/workspace/login">
        Login to Existing Workspace
      </NavLink>
    </Menu.Item>
    <Menu.Item key="/workspace/create">
      <NavLink to="/workspace/create">
        Create New Workspace
      </NavLink>
    </Menu.Item>
  </Menu>
);

const Header: React.FunctionComponent<RouteComponentProps<any>> = (props) => (
  <LayoutHeader>
    <MenuBar
      theme="dark"
      mode="horizontal"
      selectedKeys={[props.location.pathname]}
    >
    <Menu.Item>
      <Link to="/">
        <Title>
          Reserve
        </Title>
      </Link>
    </Menu.Item>

      <Menu.Item key="/about">
        <Link to="/about">
          About Reserve
        </Link>
      </Menu.Item>
      <Menu.Item key="/docs">
        <Link to="/docs">
          Documentation
        </Link>
      </Menu.Item>
    </MenuBar>

    <WorkspacePopover
      overlay={<WorkpsaceOptions {...props} />}
      trigger={["click"]}
      placement="bottomRight"
    >
      <Button icon="user" ghost={true}>
        Your Workspaces
      </Button>
    </WorkspacePopover>
  </LayoutHeader>
);

export default withRouter(Header);
