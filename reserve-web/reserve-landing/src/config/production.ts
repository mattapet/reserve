/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Config } from 'lib/types/config';
//#endregion

const protocol = window.location.protocol;
const baseHost = window.location.host;
const baseUrl = `${window.location.origin}/`;
const apiUrl = baseUrl;
const landingUrl = baseUrl;

export default {
  protocol,
  baseHost,
  apiUrl,
  landingUrl,
} as Config;
