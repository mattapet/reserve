/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Config } from 'lib/types/config';
//#endregion

const protocol = window.location.protocol;
const baseHost = 'localtest.me:8080';
const apiUrl = `${protocol}//localhost:3000/`;
const landingUrl = window.location.origin;

export default {
  protocol,
  baseHost,
  apiUrl,
  landingUrl,
} as Config;
