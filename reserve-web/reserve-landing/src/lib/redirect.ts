/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export function redirect(url: string) {
  const root = document.getElementById('root');
  const link = document.createElement('a');
  link.setAttribute('href', url);
  root && root.appendChild(link);
  link.click();
}
