/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import config from 'config';
import morgan from 'morgan';
import { NestFactory } from '@nestjs/core';
import { RootModule } from './root.module';
import { ValidationPipe } from '@nestjs/common';
//#endregion

const PORT = process.env.PORT || config.get('main.port');

async function bootstrap() {
  const app = await NestFactory.create(RootModule);
  app.enableCors();
  app.use(morgan('dev'));
  app.useGlobalPipes(new ValidationPipe());
  try {
    await app.listen(PORT || 3000);
  } catch (err) {
    // tslint:disable:no-console
    console.error(err);
    process.exit(1);
  }
}
bootstrap();
