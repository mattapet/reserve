/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EncodingType } from '../../lib/webhook/encoding-type';
import { EventType } from '../../lib/webhook/event-type';
//#endregion

export interface WebhookDTO {
  readonly id: number;
  readonly payload_url: string;
  readonly secret: string;
  readonly encoding: EncodingType;
  readonly events: EventType[];
  readonly active: boolean;
}
