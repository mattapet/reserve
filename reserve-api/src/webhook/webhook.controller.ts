/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  UseGuards,
  Get,
  Req,
  Param,
  Post,
  Body,
  Put,
  Patch,
  Delete,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { UserRole } from '../lib/user/user-role.type';
import { RoleGuard } from '../lib/guards/RoleGuard';
import { WebhookService } from './webhook.service';
import { WorkspaceService } from '../workspace/nest';
import {
  AuthorizedRequest,
} from '../lib/interface/authorized-request.interface';
import { Webhook } from './webhook.entity';
import { EventType } from '../lib/webhook/event-type';
import { EncodingType } from '../lib/webhook/encoding-type';
import { TransformerInterceptor } from './interceptors/transformer.interceptor';
//#endregion

@Controller('api/v1/webhook')
@UseGuards(AuthGuard('jwt'), RoleGuard(UserRole.owner))
export class WebhookController {
  public constructor(
    private readonly service: WebhookService,
    private readonly workspace: WorkspaceService,
  ) { }

  @Get()
  @UseInterceptors(new TransformerInterceptor())
  public async getAllWebhooks(
    @Req() req: AuthorizedRequest,
  ): Promise<Webhook[]> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      return await this.service.getAll(workspace.id, trx);
    });
  }

  @Post()
  @UseInterceptors(new TransformerInterceptor())
  public async createWebhook(
    @Req() req: AuthorizedRequest,
    @Body('payload_url') payloadUrl: string,
    @Body('secret') secret: string,
    @Body('events') events: EventType[],
    @Body('encoding') encoding: EncodingType = EncodingType.urlencoded,
  ): Promise<Webhook> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      return await this.service.create(
        payloadUrl,
        secret,
        encoding,
        events,
        workspace.id,
        trx,
      );
    });
  }

  @Get(':id')
  @UseInterceptors(new TransformerInterceptor())
  public async getById(
    @Req() req: AuthorizedRequest,
    @Param('id') webhookId: number,
  ): Promise<Webhook> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      return await this.service.getById(webhookId, workspace.id, trx);
    });
  }

  @Put(':id')
  @UseInterceptors(new TransformerInterceptor())
  public async updateWebhookById(
    @Req() req: AuthorizedRequest,
    @Param('id') webhookId: number,
    @Body('payload_url') payloadUrl: string,
    @Body('secret') secret: string,
    @Body('events') events: EventType[],
    @Body('encoding') encoding: EncodingType = EncodingType.urlencoded,
  ): Promise<Webhook> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      return await this.service.updateById(
        webhookId,
        payloadUrl,
        secret,
        encoding,
        events,
        workspace.id,
        trx,
      );
    });
  }

  @Patch(':id/toggle')
  @UseInterceptors(new TransformerInterceptor())
  public async toggleActiveById(
    @Req() req: AuthorizedRequest,
    @Param('id') webhookId: number,
  ): Promise<Webhook> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      return await this.service.toggleActiveById(webhookId, workspace.id, trx);
    });
  }

  @Delete(':id')
  public async removeByid(
    @Req() req: AuthorizedRequest,
    @Param('id') webhookId: number,
  ): Promise<void> {
    const workspaceId = req.user.workspace;
    await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      await this.service.removeById(webhookId, workspace.id, trx);
    });
  }
}
