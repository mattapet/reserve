/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Webhook } from '../webhook.entity';
import { WebhookDTO } from '../dto/webhook.dto';
//#endregion

@Injectable()
export class TransformerInterceptor implements NestInterceptor<
  Webhook | Webhook[],
  WebhookDTO | WebhookDTO[]
> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<Webhook | Webhook[]>,
  ): Observable<WebhookDTO | WebhookDTO[]> {
    return call$.handle().pipe(
      map((payload: Webhook | Webhook[]) => Array.isArray(payload) ?
        payload.map(this.encode) : this.encode(payload),
      ),
    );
  }

  private encode = (webhook: Webhook) => ({
    id: webhook.id,
    active: webhook.active,
    encoding: webhook.encoding,
    events: webhook.events,
    payload_url: webhook.payloadUrl,
    secret: webhook.secret,
  }) as WebhookDTO
}
