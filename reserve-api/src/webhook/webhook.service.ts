/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import fetch from 'node-fetch';
import crypto from 'crypto';
import qs from 'qs';
import {
  Injectable, NotFoundException,
} from '@nestjs/common';
import { EntityManager } from 'typeorm';
import { InjectEntityManager } from '@nestjs/typeorm';

import { Webhook } from './webhook.entity';
import { EncodingType } from '../lib/webhook/encoding-type';
import { EventType } from '../lib/webhook/event-type';
import { AnyObject } from '../lib/AnyObject';
//#endregion

@Injectable()
export class WebhookService {
  public constructor(
    @InjectEntityManager()
    private readonly manager: EntityManager,
  ) { }

  public async getAll(
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<Webhook[]> {
    return await trx.find(Webhook, { workspaceId });
  }

  public async getById(
    webhookId: number,
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<Webhook> {
    const item = await trx.findOne(Webhook, { id: webhookId, workspaceId });
    if (!item) {
      throw new NotFoundException();
    }
    return item;
  }

  public async create(
    payloadUrl: string,
    secret: string,
    encoding: EncodingType,
    events: EventType[],
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<Webhook> {
    events = Array.from(new Set(events));
    const webhook = new Webhook(
      payloadUrl,
      secret,
      encoding,
      events,
      workspaceId,
    );
    return await trx.save(webhook);
  }

  public async updateById(
    webhookId: number,
    payloadUrl: string,
    secret: string,
    encoding: EncodingType,
    events: EventType[],
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<Webhook> {
    events = Array.from(new Set(events));
    await trx.update(Webhook, { workspaceId, id: webhookId }, {
      payloadUrl, secret, encoding, events,
    });
    return await this.getById(webhookId, workspaceId, trx);
  }

  public async toggleActiveById(
    webhookId: number,
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<Webhook> {
    const item = await this.getById(webhookId, workspaceId, trx);
    item.active = !item.active;
    return await trx.save(item);
  }

  public async removeById(
    webhookId: number,
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<void> {
    const item = await this.getById(webhookId, workspaceId, trx);
    await trx.remove(item);
  }

  public async dispatchEvent<Payload extends AnyObject>(
    events: EventType[],
    payload: Payload,
    workspaceId: string,
  ): Promise<void> {
    const webhooks = await this.getAll(workspaceId);
    const eventPayload = { payload, events };
    await Promise.all(webhooks
      .filter(webhook => webhook.active)
      .filter(webhook => this.shouldDispatch(webhook, new Set(events)))
      .map(webhook => this.dispatchWebhook(webhook, eventPayload)));
  }

  private shouldDispatch(
    webhook: Webhook,
    events: Set<EventType>,
  ): boolean {
    for (const event of webhook.events) {
      if (events.has(event)) {
        return true;
      }
    }
    return false;
  }

  private async dispatchWebhook<Payload extends AnyObject>(
    webhook: Webhook,
    payload: Payload,
  ): Promise<void> {
    const body = webhook.encoding === EncodingType.json ?
      JSON.stringify(payload) : qs.stringify(payload);
    const signature = crypto.createHmac('sha1', webhook.secret)
      .update(body)
      .digest('hex');

    await fetch(webhook.payloadUrl, {
      method: 'POST',
      headers: {
        ['Content-Type']: webhook.encoding,
        ['X-Reserve-Signature']: signature,
      },
      body,
    });
  }

  public async transaction<T>(
    runInTrx: (em: EntityManager) => Promise<T>,
  ): Promise<T> {
    return await this.manager.transaction(runInTrx);
  }
}
