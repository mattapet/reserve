/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  AfterLoad,
} from 'typeorm';

import { EncodingType } from '../lib/webhook/encoding-type';
import { EventType } from '../lib/webhook/event-type';
//#endregion

@Entity()
export class Webhook {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ name: 'workspace_id' })
  public workspaceId: string;

  @Column({ name: 'payload_url' })
  public payloadUrl: string;

  @Column()
  public secret: string;

  @Column({ type: 'enum', enum: [
    EncodingType.urlencoded,
    EncodingType.json,
  ], default: EncodingType.urlencoded })
  public encoding: EncodingType;

  @Column({ type: 'simple-array' })
  public events: EventType[];

  @Column({ default: true })
  public active: boolean;

  public constructor(
    payloadUrl: string,
    secret: string,
    encoding: EncodingType,
    events: EventType[],
    workspaceId: string,
  ) {
    this.workspaceId = workspaceId;
    this.payloadUrl = payloadUrl;
    this.secret = secret;
    this.encoding = encoding;
    this.events = events;
    this.active = true;
  }

  @AfterLoad()
  // @ts-ignore
  private normalieze() {
    this.active = !!this.active;
  }
}
