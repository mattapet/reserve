/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export const enum Scope {
  /** Performing authorization of user. (OAuth2.0) */
  authorization = 'authorizaion',

  /** Fetch reservations. */
  reservationRead = 'reservation.read',
  /** Request a reservation. */
  reservationRequest = 'reservation.request',
  /** Update, cancel or reject reservations depending on the user role. */
  reservationManage = 'reservation.manage',

  /** Fetch resources. */
  resourceRead = 'resource.read',
  /** Create, update and remove resources. */
  resourceWrite = 'resource.write',

  /** Fetch restrictions. */
  restrictionReact = 'restriction.read',
  /** Create, update and remove restrictions. */
  restrictionWrite = 'restriction.write',

  /** Fetch user's profile. */
  profileRead = 'profile.read',
  /** Update user's profile. */
  profileWrite = 'profile.write',

  /** Fetch profiles of any user. */
  userProfileRead = 'user.profile.read',
  /** Update profiles of any user. */
  userProfileWrite = 'user.profile.write',

  /** Fetch all users */
  userRead = 'user.read',
  /** Change user roles etc. */
  userWrite = 'user.write',

  /** Fetch workspace details. */
  workspaceRead = 'worksapce.read',
  /** Update, remove workspace and it's settings. */
  workspaceWrite = 'workspace.write',

  /** Create and manage apps. */
  developer = 'developer',
}

export const SCOPE_ALL = [
  Scope.authorization,
  Scope.reservationRead,
  Scope.reservationRequest,
  Scope.reservationManage,
  Scope.resourceRead,
  Scope.resourceWrite,
  Scope.restrictionReact,
  Scope.restrictionWrite,
  Scope.profileRead,
  Scope.profileWrite,
  Scope.userProfileRead,
  Scope.userProfileWrite,
  Scope.userRead,
  Scope.userWrite,
  Scope.workspaceRead,
  Scope.workspaceWrite,
  Scope.developer,
];
