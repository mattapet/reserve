/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Scope } from '../oauth2/scope.type';
import { AuthorizedRequest } from '../interface/authorized-request.interface';
//#endregion

export function ScopeGuard(...scopes: Scope[]) {
  return new Guard(new Set(scopes));
}

@Injectable()
export class Guard implements CanActivate {
  public constructor(private readonly scopes: Set<Scope>) { }

  public canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context
    .switchToHttp()
    .getRequest<AuthorizedRequest>();

    for (const scope of req.user.scope) {
      if (this.scopes.has(scope)) {
        return true;
      }
    }
    return false;
  }
}
