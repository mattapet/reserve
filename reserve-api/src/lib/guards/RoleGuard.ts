/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { UserRole } from '../user/user-role.type';
import { AuthorizedRequest } from '../interface/authorized-request.interface';
//#endregion

export function RoleGuard(...roles: UserRole[]) {
  return new Guard(new Set(roles));
}

@Injectable()
export class Guard implements CanActivate {
  public constructor(private readonly roles: Set<UserRole>) { }

  public canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context
      .switchToHttp()
      .getRequest<AuthorizedRequest>();
    return this.roles.has(req.user.role);
  }
}
