/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export type Optional<T> = Some<T> | None<T>;

export class Some<T> {
  public constructor(private readonly value: T) { }

  public get type(): 'some' {
    return 'some';
  }

  public get(): T {
    return this.value;
  }

  public getOr(defaultValue: () => T): T {
    return this.value;
  }

  public getOrDefault(defaultValue: T): T {
    return this.value;
  }

  public map<U>(transformer: (T) => U): Optional<U> {
    return some(transformer(this.value));
  }

  public flatMap<U>(transformer: (T) => Optional<U>): Optional<U> {
    return transformer(this.value);
  }

  public equals(other: Optional<T>): boolean {
    switch (other.type) {
    case 'some': return this.value === other.value;
    case 'none': return false;
    }
  }

  public nequals(other: Optional<T>): boolean {
    return !this.equals(other);
  }
}

export class None<T = never> {
  public get type(): 'none' {
    return 'none';
  }

  public get(): T {
    throw new TypeError('None found while unwrapping an optional value.');
  }

  public getOr(defaultValue: () => T): T {
    return defaultValue();
  }

  public getOrDefault(defaultValue: T): T {
    return defaultValue;
  }

  public map<U>(transformer: (T) => U): Optional<U> {
    return none;
  }

  public flatMap<U>(transformer: (T) => U): Optional<U> {
    return none;
  }

  public equals(other: Optional<T>): boolean {
    switch (other.type) {
    case 'some': return false;
    case 'none': return true;
    }
  }

  public nequals(other: Optional<T>): boolean {
    return !this.equals(other);
  }
}

export function tryOptional<T>(throwable: () => T): Optional<T> {
  try {
    return some(throwable());
  } catch (e) {
    return none;
  }
}

export function some<T>(wrapped: T): Optional<T> {
  return new Some(wrapped);
}

export const none = new None<any>();
