/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { Request } from 'express';
import { UserRole } from '../user/user-role.type';
import { Scope } from '../oauth2/scope.type';

export interface AuthorizedRequest extends Request {
  readonly user: {
    readonly id: string;
    readonly workspace: string;
    readonly role: UserRole;
    readonly scope: Scope[];
  };
}
