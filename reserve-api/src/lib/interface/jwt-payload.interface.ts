/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Scope } from '../oauth2/scope.type';
import { UserRole } from '../user/user-role.type';
//#endregion

export const enum TokenKind {
  accessToken = 'access_token',
  refreshToken = 'refresh_token',
}

export interface JwtPayload {
  /** Token ID */
  readonly jit: string;
  /** Expiration time */
  readonly exp: number;
  /** Subject of the JWT a.k.a. authenticated user */
  readonly sub: string;
  /** Issuer of the JWT */
  readonly iss?: string | undefined;
  /** Audience for which the JWT is for */
  readonly aud?: string | undefined;
  /** Issued time */
  readonly iat?: number | undefined;
  /** Token kind */
  readonly kind: TokenKind;
  /** Scope of the token */
  readonly scope: Scope[];
  /** User */
  readonly user: {
    /** User's ID */
    readonly id: string;
    /** User's workspace */
    readonly workspace: string;
    /** User role */
    readonly role: UserRole;
  };
}
