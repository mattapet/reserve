/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export function flatMap<Element, Result>(
  collection: Element[],
  mapper: (element: Element) => Result[],
): Result[] {
  const results: Result[] = [];
  for (const element of collection) {
    results.push(...mapper(element));
  }
  return results;
}
