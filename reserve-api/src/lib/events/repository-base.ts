/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from './event';
import { Command } from './command';
//#endregion

/**
 * Function that consumes an aggregate and a command producing an array of
 * events which should be applied to the aggregate.
 */
export type CommandHandler<
  CommandType extends Command,
  EventType extends Event,
  Aggregate
> = (
  aggregate: Aggregate,
  command: CommandType,
) => EventType[];

/**
 * Returns given aggregate applying proivded event to it.
 */
export type EventHandler<EventType extends Event, Aggregate> = (
  aggregate: Aggregate,
  event: EventType,
) => Aggregate;

/**
 * Basic event repository.
 */
export interface BaseEventRepository<EventType extends Event> {
  /**
   * Returns all events for the given `rowId`.
   */
  findByRowId(rowId: string): Promise<EventType[]>;
  /**
   * Returns all events for the give `rowId` and `aggregateId`.
   */
  findByRowIdAndAggregateId(
    rowId: string,
    aggregateId: string,
  ): Promise<EventType[]>;
  /**
   * Saves all provided events returning their identities.
   */
  saveEvents(...events: EventType[]): Promise<EventType[]>;
}

/**
 * Shape representing events grouped by their `aggregateId`.
 */
export interface GroupedById<EventType extends Event> {
  [aggregateId: string]: EventType[];
}

/**
 * Base class for all event-based repositories.
 */
export class BaseRepository<
  CommandType extends Command,
  EventType extends Event,
  Aggregate,
> {
  public constructor(
    private readonly repository: BaseEventRepository<EventType>,
    private readonly commandHandler:
      CommandHandler<CommandType, EventType, Aggregate>,
    private readonly eventHandler: EventHandler<EventType, Aggregate>,
    private readonly defaultAggregate: Aggregate,
  ) { }

  /**
   * Retrieves all events for the given `rowId`.
   */
  public async dumpEvents(
    rowId: string,
    since?: Date,
    until?: Date,
  ): Promise<EventType[]> {
    const events = await this.repository.findByRowId(rowId);
    return events
      .filter(({ timestamp }) => since != null ? timestamp >= since : true)
      .filter(({ timestamp }) => until != null ? timestamp <= until : true);
  }

  /**
   * Retrieves all aggregates for the given `rowId`.
   */
  public async getAll(rowId: string): Promise<Aggregate[]> {
    const events = await this.repository.findByRowId(rowId);
    return this.groupByAggregate(events).map(e =>
      e.reduce(this.eventHandler, this.defaultAggregate));
  }

  /**
   * Returns an aggregate for the given `aggregateId` and `rowId`.
   */
  public async getById(
    rowId: string,
    aggregateId: string,
  ): Promise<Aggregate> {
    const events = await this.repository
      .findByRowIdAndAggregateId(rowId, aggregateId);
    return events.reduce(this.eventHandler, this.defaultAggregate);
  }

  /**
   * Performs given `command` on the provided `aggregate`. If no aggregate is
   * provided, the `defaultAggregate` will be used.
   */
  public async execute(
    command: CommandType,
    aggregate: Aggregate = this.defaultAggregate,
  ): Promise<Aggregate> {
    const events = this.commandHandler(aggregate, command);
    const saved = await this.repository.saveEvents(...events);
    return saved.reduce(this.eventHandler, aggregate);
  }

  /**
   * Groups events by their `aggregateId`.
   */
  private groupByAggregate(events: EventType[]): EventType[][] {
    const grouped: GroupedById<EventType> =
      events.reduce((acc, event) => {
        const aggregateEvents = acc[event.aggregateId] || [];
        return {
          ...acc,
          [event.aggregateId]: [
            ...aggregateEvents,
            event,
          ],
        };
      }, {});
    return Object.values(grouped);
  }
}
