/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  PrimaryGeneratedColumn,
  PrimaryColumn,
  Index,
  Column,
} from 'typeorm';
//#endregion

export class BaseEvent<Payload extends {}> {
  @PrimaryGeneratedColumn()
  public id!: number;

  @PrimaryColumn({ name: 'row_id' })
  public rowId: string;

  @Index()
  @Column({ name: 'aggregate_id' })
  public aggregateId: string;

  @Index()
  @Column({ transformer: {
    to: (d) => d || new Date().toISOString(),
    from: (d: Date) => d,
  }, type: 'timestamp' })
  public timestamp: Date;

  @Column({ type: 'simple-json' })
  public payload: Payload;

  public constructor(
    rowId: string,
    aggregateId: string,
    timestamp: Date,
    payload: Payload,
  ) {
    this.rowId = rowId;
    this.aggregateId = aggregateId;
    this.timestamp = timestamp;
    this.payload = payload;
  }
}
