/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from './command';
import { Event } from './event';
//#endregion

/**
 * Base class for all event-based repositories.
 */
export interface Repository<
  CommandType extends Command,
  EventType extends Event,
  Aggregate
> {
  /**
   * Retrieves all events for the given `rowId`.
   */
  dumpEvents(rowId: string): Promise<EventType[]>;

  /**
   * Retrieves all aggregates for the given `rowId`.
   */
  getAll(rowId: string): Promise<Aggregate[]>;

  /**
   * Returns an aggregate for the given `aggregateId` and `rowId`.
   */
  getById(rowId: string, reservationId: string): Promise<Aggregate>;

  /**
   * Performs given `command` on the provided `aggregate`. If no aggregate is
   * provided, the `defaultAggregate` will be used.
   */
  execute(command: CommandType, aggregate?: Aggregate): Promise<Aggregate>;
}
