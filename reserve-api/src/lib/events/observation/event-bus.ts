/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '../event';
import { EventBus as Interface } from './event-bus.interface';
//#endregion

export class EventBus implements Interface {
  public static readonly instance: EventBus = new EventBus();

  private constructor() { }

  private consumers: { [type: string]: ((event: Event) => void)[]} = {};

  public publish(event: Event): void {
    const consumers = this.consumers[event.type] || [];
    consumers.forEach(consumer => this.tryConsume(event, consumer));
  }

  public consume(type: string, consumer: (event: Event) => void): void {
    const constumers = this.consumers[type] || [];
    this.consumers[type] = [...constumers, consumer];
  }

  private async tryConsume(
    event: Event,
    consumer: (evnet: Event) => void,
    timeout: number = 100,
  ): Promise<void> {
    try {
      await consumer(event);
    } catch (error) {
      // tslint:disable:no-console
      console.error(error);
      setTimeout(() => this.tryConsume(event, consumer, timeout * 2), timeout);
    }
  }
}
