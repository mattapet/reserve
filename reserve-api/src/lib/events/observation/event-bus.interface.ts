/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '../event';
//#endregion

export interface EventBus {
  publish(event: Event): void;
  consume(type: string, callback: (event: Event) => void | Promise<void>): void;
}
