/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '../event';
//#endregion

export class BaseEventRepository<EventType extends Event> {
  private data: { [rowId: string]: EventType[] } = {};

  public async clean(rowId?: string): Promise<void> {
    if (rowId) {
      this.data[rowId] = [];
    } else {
      this.data = {};
    }
  }

  public async findByRowId(rowId: string): Promise<EventType[]> {
    return this.data[rowId] || [];
  }

  public async findByRowIdAndAggregateId(
    rowId: string,
    aggregateId: string,
  ): Promise<EventType[]> {
    return (this.data[rowId] || [])
      .filter(event => event.aggregateId === aggregateId);
  }

  public async saveEvents(...events: EventType[]): Promise<EventType[]> {
    events.forEach(event => {
      this.data[event.workspaceId] = [
        ...(this.data[event.workspaceId] || []),
        event,
      ];
    });
    return events;
  }
}
