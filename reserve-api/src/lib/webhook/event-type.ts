/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export const enum EventType {
  reservation = 'reservation',
  reservationCreated = 'reservation.created',
  reservationConfirmed = 'reservation.confirmed',
  reservationCanceled = 'reservation.canceled',
  reservationRejected = 'reservation.rejected',
}
