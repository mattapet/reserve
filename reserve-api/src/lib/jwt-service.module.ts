/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import config from 'config';
import * as fs from 'fs';
import { JwtModule } from '@nestjs/jwt';
//#endregion

const SERVICE = process.env.SERVICE_NAME || config.get('service.name');
const JWT_PUBKEY = process.env.JWT_PUBK;
const JWT_ALG = process.env.JWT_ALG || config.get('jwt.alg') as string;
const JWT_SECRET = config.get('jwt.secret') as string;
const JWT_PRIVK = process.env.JWT_PRIVK;

export const registerJWTServiceModule = () =>
  JwtModule.register({
    publicKey: JWT_PUBKEY ? fs.readFileSync(JWT_PUBKEY) : undefined,
    privateKey: JWT_PRIVK ? fs.readFileSync(JWT_PRIVK) : undefined,
    secret: JWT_SECRET,
    verifyOptions: {
      issuer: SERVICE,
      algorithms: [JWT_ALG],
    },
    signOptions: {
      issuer: SERVICE,
      algorithm: JWT_ALG,
    },
  });
