/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// #region imports
import config from 'config';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { WorkspaceModule } from './workspace/nest';
import { UserModule } from './user/nest';
import { IdentityModule } from './identity/nest';
import { AuthModule } from './auth/auth.module';
import { ResourceModule } from './resource/resource.module';
import { ReservationModule } from './reservation/nest';
import { OAuth2Module } from './oauth2/oauth2.module';
import { SettingsModule } from './settings/nest';
import { AuthorityModule } from './authority/authority.module';
import { WebhookModule } from './webhook/webhook.module';
import { SetupModule } from './setup/nest';
// #endregion

const DB_TYPE = process.env.DB_TYPE || config.get('db.type');
const DB_HOST = process.env.DB_HOST || config.get('db.host');
const DB_PORT = process.env.DB_PORT || config.get('db.port');
const DB_USERNAME = process.env.DB_USERNAME || config.get('db.username');
const DB_PASSWORD = process.env.DB_PASSWORD || config.get('db.password');
const DB_DATABASE = process.env.DB_DATABASE || config.get('db.database');

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: DB_TYPE as 'mysql',
      host: DB_HOST,
      port: parseInt(DB_PORT || '3000', 10),
      username: DB_USERNAME,
      password: DB_PASSWORD,
      database: DB_DATABASE,
      entities: [`${__dirname}/**/*.entity{.ts,.js}`],
      synchronize: true,
    }),
    AuthModule,
    ReservationModule,
    ResourceModule,
    UserModule,
    IdentityModule,
    WorkspaceModule,
    OAuth2Module,
    SettingsModule,
    AuthorityModule,
    WebhookModule,
    SetupModule,
  ],
})
export class RootModule { }
