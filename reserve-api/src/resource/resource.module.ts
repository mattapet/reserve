/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

 //#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Resource } from './resource.entity';
import { ResourceController } from './resource.controller';
import { ResourceService } from './resource.service';
import { WorkspaceModule } from '../workspace/nest';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([Resource]),
    WorkspaceModule,
  ],
  controllers: [ResourceController],
  providers: [ResourceService],
  exports: [ResourceService],
})
export class ResourceModule { }
