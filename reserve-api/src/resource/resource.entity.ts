/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  AfterLoad,
} from 'typeorm';
//#endregion

@Entity()
export class Resource {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ name: 'workspace_id' })
  public workspaceId: string;

  @Column({ length: 255 })
  public name: string;

  @Column({ length: 510 })
  public description: string;

  @Column({ default: true })
  public active: boolean;

  public constructor(workspaceId: string, name: string, description: string) {
    this.workspaceId = workspaceId;
    this.name = name;
    this.description = description;
    this.active = true;
  }

  @AfterLoad()
  // @ts-ignore
  private normalize() {
    this.active = !!this.active;
  }
}
