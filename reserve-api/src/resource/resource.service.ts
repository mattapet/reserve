/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager, In } from 'typeorm';

import { Resource } from './resource.entity';
//#endregion

@Injectable()
export class ResourceService {
  public constructor(
    @InjectEntityManager()
    private readonly manager: EntityManager,
  ) { }

  public async create(
    workspaceId: string,
    name: string,
    description: string,
    trx: EntityManager = this.manager,
  ): Promise<Resource> {
    const item = new Resource(workspaceId, name, description);
    return await trx.save(item);
  }

  public async getAll(
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<Resource[]> {
    return await trx.find(Resource, { workspaceId });
  }

  public async getById(
    workspaceId: string,
    resourceId: number,
    trx: EntityManager = this.manager,
  ): Promise<Resource> {
    const item = await trx.findOne(Resource, { workspaceId, id: resourceId });
    if (!item) {
      throw new NotFoundException();
    }
    return item;
  }

  public async getByIds(
    workspaceId: string,
    resourceIds: number[],
    trx: EntityManager = this.manager,
  ): Promise<Resource[]> {
    if (!resourceIds.length) { return []; }
    const items =
      await trx.find(Resource, { workspaceId, id: In(resourceIds) });
    if (items.length !== resourceIds.length) {
      throw new NotFoundException('One or more resources were not found.');
    }
    return items;
  }

  public async updateById(
    workspaceId: string,
    resourceId: number,
    name: string,
    description: string,
    trx: EntityManager = this.manager,
  ): Promise<Resource> {
    await trx.update(Resource, { workspaceId, id: resourceId }, {
      id: resourceId, name, description,
    });
    return await this.getById(workspaceId, resourceId, trx);
  }

  public async toggleActiveById(
    workspaceId: string,
    resourceId: number,
    trx: EntityManager = this.manager,
  ): Promise<Resource> {
    const resource = await this.getById(workspaceId, resourceId, trx);
    resource.active = !resource.active;
    return await trx.save(resource);
  }

  public async removeById(
    workspaceId: string,
    resourceId: number,
    trx: EntityManager = this.manager,
  ): Promise<void> {
    await trx.delete(Resource, { workspaceId, id: resourceId });
  }

  public async transaction<T>(
    runInTrx: (em: EntityManager) => Promise<T>,
  ): Promise<T> {
    return await this.manager.transaction(runInTrx);
  }
}
