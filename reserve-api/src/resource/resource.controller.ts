/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  UseGuards,
  Post,
  Req,
  Body,
  Get,
  Param,
  Put,
  Delete,
  Patch,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ResourceService } from './resource.service';
import { Resource } from './resource.entity';
import { WorkspaceService } from '../workspace/nest';
import { RoleGuard } from '../lib/guards/RoleGuard';
import { UserRole } from '../lib/user/user-role.type';
import {
  AuthorizedRequest,
} from '../lib/interface/authorized-request.interface';
//#endregion

@Controller('api/v1/resource')
@UseGuards(AuthGuard('jwt'))
export class ResourceController {
  public constructor(
    private readonly service: ResourceService,
    private readonly workspace: WorkspaceService,
  ) { }

  @Post()
  @UseGuards(RoleGuard(UserRole.owner))
  public async create(
    @Req() req: AuthorizedRequest,
    @Body('name') name: string,
    @Body('description') description: string,
  ): Promise<Resource> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      return await this.service.create(workspace.id, name, description);
    });
  }

  @Get()
  public async getAll(@Req() req: AuthorizedRequest): Promise<Resource[]> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      return await this.service.getAll(workspace.id, trx);
    });
  }

  @Get(':id')
  public async getById(
    @Req() req: AuthorizedRequest,
    @Param('id') resourceId: number,
  ): Promise<Resource> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      return await this.service.getById(workspace.id, resourceId, trx);
    });
  }

  @Put(':id')
  @UseGuards(RoleGuard(UserRole.owner))
  public async updateById(
    @Req() req: AuthorizedRequest,
    @Param('id') resourceId: number,
    @Body('name') name: string,
    @Body('description') description: string,
  ): Promise<Resource> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
    const workspace = await this.workspace.getById(workspaceId, trx);
    return await this.service
      .updateById(workspace.id, resourceId, name, description, trx);
    });
  }

  @Patch(':id/toggle')
  @UseGuards(RoleGuard(UserRole.owner))
  public async toggleActiveById(
    @Req() req: AuthorizedRequest,
    @Param('id') resourceId: number,
  ): Promise<Resource> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      return await this.service.toggleActiveById(workspace.id, resourceId, trx);
    });
  }

  @Delete(':id')
  @UseGuards(RoleGuard(UserRole.owner))
  public async removeById(
    @Req() req: AuthorizedRequest,
    @Param('id') resourceId: number,
  ): Promise<void> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      return await this.service.removeById(workspace.id, resourceId, trx);
    });
  }
}
