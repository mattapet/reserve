/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import crypto, { BinaryLike } from 'crypto';
//#endregion

export function encrypt(
  value: string,
  key: BinaryLike,
  iv: BinaryLike = crypto.randomBytes(16),
): Buffer {
  return crypto.createCipheriv('aes-256-gcm', key, iv).update(value);
}
