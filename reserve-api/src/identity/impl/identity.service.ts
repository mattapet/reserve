/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import uuid from 'uuid/v4';

import { Identity } from '../identity.interface';
import { IdentityService } from '../identity.service';
import { IdentityRepository } from '../identity.repository';
import { generateAnonymizationKey } from './generate-anonymization-key';
import { encrypt } from './encrypt';
//#endregion

export class IdentityServiceImpl implements IdentityService {
  public constructor(
    private readonly repository: IdentityRepository,
  ) { }

  // - MARK: Queries

  public async getAll(workspaceId: string): Promise<Identity[]> {
    return await this.repository.getAll(workspaceId);
  }

  public async getByUserId(
    workspaceId: string,
    userId: string,
  ): Promise<Identity> {
    return await this.repository.getByUserId(workspaceId, userId);
  }

  public async getByEmail(
    workspaceId: string,
    email: string,
  ): Promise<Identity> {
    return await this.repository.getByEmail(workspaceId, email);
  }

  public async findByEmail(
    workspaceId: string,
    email: string,
  ): Promise<Identity | undefined> {
    return await this.repository.findByEmail(workspaceId, email);
  }

  // - MARK: Commands

  public async create(
    workspaceId: string,
    email: string,
    firstName: string = '',
    lastName: string = '',
    phone?: string,
  ): Promise<Identity> {
    return await this.repository.save({
      userId: uuid(),
      workspaceId,
      email,
      firstName,
      lastName,
      phone,
      anonymized: false,
    });
  }

  public async update(
    workspaceId: string,
    userId: string,
    firstName: string,
    lastName: string,
    phone?: string,
  ): Promise<Identity> {
    await this.repository.save({
      userId,
      workspaceId,
      firstName,
      lastName,
      phone,
      anonymized: false,
    });
    return await this.repository.getByUserId(workspaceId, userId);
  }

  public async setPassword(
    workspaceId: string,
    userId: string,
    password: string,
  ): Promise<void> {
    const item = await this.getByUserId(workspaceId, userId);
    item.password = password;
    await this.repository.save(item);
  }

  public async anonymizeByUserId(
    workspaceId: string,
    userId: string,
  ): Promise<void> {
    const identity = await this.getByUserId(workspaceId, userId);
    const key = generateAnonymizationKey();
    identity.email = encrypt(identity.email, key).toString('hex');
    identity.firstName = encrypt(identity.firstName, key).toString('hex');
    identity.lastName = encrypt(identity.lastName, key).toString('hex');
    if (identity.phone) {
      identity.phone = encrypt(identity.phone, key).toString('hex');
    }
    if (identity.password) {
      identity.password = encrypt(identity.password, key).toString('hex');
    }
    identity.anonymized = true;
    await this.repository.save(identity);
  }
}
