/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository, Repository } from 'typeorm';

import { Identity } from './identity.entity';
import { IdentityRepository } from '../identity.repository';
import { NotFoundException } from '@nestjs/common';
//#endregion

@EntityRepository(Identity)
export class NestIdentityRepository
  extends Repository<Identity>
  implements IdentityRepository
{
  public async getAll(workspaceId: string): Promise<Identity[]> {
    return await this.find({ workspaceId, anonymized: false });
  }

  public async getByUserId(
    workspaceId: string,
    userId: string,
  ): Promise<Identity> {
    const item = await this.findOne({ workspaceId, userId, anonymized: false });
    if (!item) {
      throw new NotFoundException();
    }
    return item;
  }

  public async getByEmail(
    workspaceId: string,
    email: string,
  ): Promise<Identity> {
    const item = await this.findOne({ workspaceId, email, anonymized: false});
    if (!item) {
      throw new NotFoundException();
    }
    return item;
  }

  public async findByEmail(
    workspaceId: string,
    email: string,
  ): Promise<Identity | undefined> {
    return await this.findOne({ workspaceId, email, anonymized: false});
  }
}
