/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { Connection } from 'typeorm';

import { NestIdentityRepository } from './identity.repository';
import { IdentityServiceImpl } from '../impl/identity.service';
//#endregion

@Injectable()
export class NestIdentityService extends IdentityServiceImpl {
  public constructor(
    @InjectConnection()
    connection: Connection,
  ) {
    super(connection.getCustomRepository(NestIdentityRepository));
  }
}
