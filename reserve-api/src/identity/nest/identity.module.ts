/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Identity } from './identity.entity';
import { NestIdentityRepository } from './identity.repository';
import { NestIdentityService } from './identity.service';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([Identity, NestIdentityRepository]),
  ],
  providers: [NestIdentityService],
  exports: [NestIdentityService],
})
export class IdentityModule { }
