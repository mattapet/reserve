/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity, PrimaryColumn, Index, Column, AfterLoad } from 'typeorm';
import { Identity as Interface } from '../identity.interface';
//#endregion

@Entity()
export class Identity implements Interface {
  @PrimaryColumn({ name: 'user_id' })
  public userId: string;

  @PrimaryColumn({ name: 'workspace_id' })
  public workspaceId: string;

  @Index()
  @Column()
  public email: string;

  @Column({ name: 'first_name' })
  public firstName: string;

  @Column({ name: 'last_name' })
  public lastName: string;

  @Column()
  public anonymized: boolean;

  @Column({ nullable: true })
  public phone?: string;

  @Column({ nullable: true })
  public password?: string;

  public constructor(
    userId: string,
    workspaceId: string,
    email: string,
    firstName: string,
    lastName: string,
    phone?: string,
    password?: string,
  ) {
    this.userId = userId;
    this.workspaceId = workspaceId;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phone = phone;
    this.password = password;
    this.anonymized = false;
  }

  @AfterLoad()
  // @ts-ignore
  private normalize() {
    this.anonymized = !!this.anonymized;
  }
}
