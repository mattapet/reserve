/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Identity } from './identity.interface';
//#endregion

export interface IdentityService {
  getAll(workspaceId: string): Promise<Identity[]>;

  getByUserId(workspaceId: string, userId: string): Promise<Identity>;

  getByEmail(workspaceId: string, email: string): Promise<Identity>;

  findByEmail(
    workspaceId: string,
    email: string,
  ): Promise<Identity | undefined>;

  create(
    workspaceid: string,
    email: string,
    firstName?: string,
    lastName?: string,
    phone?: string,
  ): Promise<Identity>;

  update(
    workspaceid: string,
    userId: string,
    firstName?: string,
    lastName?: string,
    phone?: string,
  ): Promise<Identity>;

  setPassword(
    workspaceId: string,
    userId: string,
    password: string,
  ): Promise<void>;

  anonymizeByUserId(workspaceId: string, id: string): Promise<void>;
}
