/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';
import { WorkspaceModule } from '../workspace/nest';
import { SettingsModule } from '../settings/nest';
import { AuthorityModule } from '../authority/authority.module';
import { registerJWTServiceModule } from '../lib/jwt-service.module';
import { UserModule } from '../user/nest';
import { IdentityModule } from '../identity/nest';
import { OAuth2Module } from '../oauth2/oauth2.module';
import { VerificationModule } from '../verfication/nest';
//#endregion

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    registerJWTServiceModule(),
    OAuth2Module,
    WorkspaceModule,
    SettingsModule,
    AuthorityModule,
    IdentityModule,
    UserModule,
    VerificationModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
  exports: [AuthService, JwtStrategy],
})
export class AuthModule { }
