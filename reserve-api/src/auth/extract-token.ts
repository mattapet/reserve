/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Request } from 'express';
import { ExtractJwt } from 'passport-jwt';
//#endregion

export function ExtractToken(tokenKey: string = 'access_token') {
  return (req: Request) => (
    ExtractJwt.fromAuthHeaderAsBearerToken()(req)
    || ExtractJwt.fromBodyField(tokenKey)(req)
    || ExtractJwt.fromUrlQueryParameter(tokenKey)(req)
  );
}
