/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as bcrypt from 'bcryptjs';
import {
  Injectable,
  BadRequestException,
  ForbiddenException,
} from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';

import { Workspace, WorkspaceService } from '../workspace/nest';
import { AuthorizedUser } from '../lib/interface/authorized-user.interface';
import { JwtPayload } from '../lib/interface/jwt-payload.interface';
import { NestBannedService } from '../user/banned/nest';
import { NestIdentityService } from '../identity/nest';
import { Identity } from '../identity';
//#endregion

@Injectable()
export class AuthService {
  public constructor(
    @InjectEntityManager()
    private readonly manager: EntityManager,
    private readonly banned: NestBannedService,
    private readonly workspace: WorkspaceService,
    private readonly identityService: NestIdentityService,
  ) { }

  public async login(
    workspaceId: string,
    email: string,
    password: string,
    trx: EntityManager = this.manager,
  ): Promise<Identity> {
    const workspace = await trx.findOne(Workspace, workspaceId);
    const item = await this.identityService.getByEmail(workspaceId, email);
    if (!workspace || !item || !item.password
      || !await bcrypt.compare(password, item.password)) {
      throw new BadRequestException('Invalid username or password');
    }
    if (await this.banned.getById(workspaceId, item.userId)) {
      throw new ForbiddenException('You have been banned');
    }
    return item;
  }

  public async setPassword(
    workspaceId: string,
    userId: string,
    password: string,
  ): Promise<Identity> {
    const hashed = await bcrypt.hash(password, 10);

    const identity = await this.identityService
      .getByUserId(workspaceId, userId);
    await this.identityService
      .setPassword(workspaceId, identity.userId, hashed);
    return identity;
  }

  public async authorityLogin(
    workspaceId: string,
    profile: {
      readonly email: string,
      readonly firstName: string,
      readonly lastName: string,
      readonly phone?: string,
    },
    trx: EntityManager = this.manager,
  ): Promise<Identity> {
    const identity = await this.identityService
      .findByEmail(workspaceId, profile.email);

    if (identity) {
      if (await this.banned.getById(workspaceId, identity.userId)) {
        throw new ForbiddenException('You have been banned');
      }

      return await this.identityService.update(
        workspaceId,
        identity.userId,
        profile.firstName,
        profile.lastName,
        profile.phone || identity.phone,
      );
    } else {
      return this.identityService.create(
        workspaceId,
        profile.email,
        profile.firstName,
        profile.lastName,
        profile.phone,
      );
    }
  }

  public async validateUser(
    payload: JwtPayload,
  ): Promise<AuthorizedUser | undefined> {
    const userId = payload.user.id;
    const workspaceName = payload.user.workspace;
    const workspace = await this.workspace.getByName(workspaceName);

    const banend = await this.banned.getById(workspace.id, userId);
    if (banend) {
      return undefined;
    }

    return {
      ...payload.user,
      workspace: workspace.id,
      scope: payload.scope,
    };
  }

  public async verifyEmail(
    workspaceId: string,
    email: string,
  ): Promise<void> {
    if (!await this.identityService.findByEmail(workspaceId, email)) {
      await this.identityService.create(workspaceId, email);
    }
  }

  public async transaction<T>(
    runInTrx: (em: EntityManager) => Promise<T>,
  ): Promise<T> {
    return await this.manager.transaction(runInTrx);
  }
}
