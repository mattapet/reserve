/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Response } from 'express';
import {
  Controller,
  Post,
  Body,
  Req,
  UseGuards,
  HttpCode,
  HttpStatus,
  Param,
  ForbiddenException,
  Get,
  Res,
  BadRequestException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import qs from 'qs';

import { AuthService } from './auth.service';
import {
  AuthorizedRequest,
} from '../lib/interface/authorized-request.interface';
import { TokenService } from '../oauth2/services/token.service';
import { UserSettingsService } from '../settings/user/nest';
import { WorkspaceService } from '../workspace/nest';
import { AuthorityService } from '../authority/authroity.service';
import { ClientService } from '../oauth2/services/client.service';
import { TokenResponse } from '../oauth2/dto/token-response.dto';
import { NestIdentityService } from '../identity/nest';
import { NestVerificationService } from '../verfication/nest';
//#endregion

@Controller('api/v1')
export class AuthController {
  public constructor(
    private readonly service: AuthService,
    private readonly identityService: NestIdentityService,
    private readonly tokenService: TokenService,
    private readonly clientService: ClientService,
    private readonly workspace: WorkspaceService,
    private readonly settings: UserSettingsService,
    private readonly authority: AuthorityService,
    private readonly verificationService: NestVerificationService,
  ) { }

  @Post('logout')
  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(AuthGuard('jwt'))
  public async logout(
    @Req() req: AuthorizedRequest,
  ): Promise<void> {
    const workspace = await this.workspace.getById(req.user.workspace);
    const user = await this.identityService
      .getByUserId(workspace.id, req.user.id);
    return await this.tokenService.logout(user.userId);
  }

  @Get('workspace/:workspace/authority')
  public async fetchLoginAuthoritiesByWorkspace(
    @Param('workspace') workspaceId: string,
  ): Promise<string[]> {
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId);
      const authorities = await this.authority.getByWorkspace(workspace.id);
      return authorities.map(authority => authority.name);
    });
  }

  @Get(':workspace/login/:authority')
  public async loginWithAuthority(
    @Param('workspace') workspaceId: string,
    @Param('authority') name: string,
    @Res() res: Response,
  ): Promise<void> {
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      const authority = await this.authority.getByName(name, workspace.id, trx);
      const query = {
        scope: authority.scope.join(' '),
        redirect_uri: authority.redirectUri,
        client_id: authority.clientId,
        response_type: 'code',
      };
      res.redirect(`${authority.authorizeUrl}${
        authority.authorizeUrl.indexOf('?') < 0 ? '?' : '&'
      }${qs.stringify(query)}`);
    });
  }

  @Post('login/authority')
  public async loginWithAuthorityCallback(
    @Body('workspace_id') workspaceId: string,
    @Body('authority') name: string,
    @Body('code') code: string,
  ): Promise<TokenResponse> {
    const workspace = await this.workspace.getById(workspaceId);
    const authority = await this.authority.getByName(name, workspace.id);
    const accessToken = await this.authority.excahngeCode(code, authority);
    const profile =
      await this.authority.fetchProfile(authority, accessToken);
    const [at, rt] = await this.service.transaction(async (trx) => {
      const user =
        await this.service.authorityLogin(workspaceId, profile, trx);
      const client =
        await this.clientService.getWorkspaceClient(workspaceId, trx);
      return await this.tokenService
        .authorize(client, user.userId, client.scope, trx);
    });
    return {
      access_token: await this.tokenService.sign(workspace.name, at),
      expires_in: at.expiry,
      token_type: 'Bearer',
      refresh_token: await this.tokenService.sign(workspace.name, rt),
      scope: at.scope.join(' '),
      workspace: workspace.name,
    };
  }

  @Post('password')
  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(AuthGuard('jwt'))
  public async setPassword(
    @Req() req: AuthorizedRequest,
    @Body('password') password: string,
  ): Promise<void> {
    const worksapceId = req.user.workspace;
    const userId = req.user.id;
    await this.service.setPassword(worksapceId, userId, password);
  }

  @Post('email/verify')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async verifyEmail(
    @Body('workspace_id') workspaceId: string,
    @Body('email') email: string,
    @Body('redirect_uri') redirectUri: string,
  ): Promise<void> {
    const settings = await this.settings.getByWorkspace(workspaceId);
    if (!settings.allowedRegistrations) {
      throw new ForbiddenException('Registration of users is disabled.');
    }
    if (await this.identityService.findByEmail(workspaceId, email)) {
      throw new ForbiddenException('Email address already registered.');
    }
    await this.verificationService.verify(redirectUri, workspaceId, email);
  }

  @Post('email/confirm')
  public async confirmEmail(
    @Body('workspace_id') workspaceId: string,
    @Body('email') email: string,
    @Body('expiry') expiryStr: string,
    @Body('signature') signature: string,
  ): Promise<TokenResponse> {
    const expiry = parseInt(expiryStr, 10);
    const valid = await this.verificationService
      .confirm(workspaceId, email, expiry, signature);

    if (!valid) {
      throw new BadRequestException('Corrupted signature');
    }

    const workspace = await this.workspace.getById(workspaceId);
    await this.service.verifyEmail(workspaceId, email);

    const user = await this.identityService.getByEmail(workspaceId, email);
    const client = await this.clientService.getWorkspaceClient(workspaceId);
    const [at, rt] = await this.tokenService
      .authorize(client, user.userId, client.scope);

    return {
      access_token: await this.tokenService.sign(workspace.name, at),
      expires_in: at.expiry,
      token_type: 'Bearer',
      refresh_token: await this.tokenService.sign(workspace.name, rt),
      scope: at.scope.join(' '),
      workspace: workspace.name,
    };
  }
}
