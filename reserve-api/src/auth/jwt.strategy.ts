/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import config from 'config';
import * as fs from 'fs';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-jwt';

import { AuthService } from './auth.service';
import { ExtractToken } from './extract-token';
import { JwtPayload } from '../lib/interface/jwt-payload.interface';
import { AuthorizedUser } from '../lib/interface/authorized-user.interface';
//#endregion

const JWT_SECRET = config.get('jwt.secret') as string;
const JWT_PRIVK = process.env.JWT_PRIVK;

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  public constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractToken(),
      secretOrKey: JWT_PRIVK ? fs.readFileSync(JWT_PRIVK) : JWT_SECRET,
    });
  }

  public async validate(payload: JwtPayload): Promise<AuthorizedUser> {
    const user = await this.authService.validateUser(payload);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
