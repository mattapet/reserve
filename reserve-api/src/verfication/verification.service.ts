/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export interface VerificationService {
  verify(baseUrl: string, workspaceId: string, email: string): Promise<void>;
  confirm(
    workspaceId: string,
    email: string,
    expiry: number,
    signature: string,
  ): Promise<boolean>;
}
