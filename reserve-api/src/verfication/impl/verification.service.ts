/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import qs from 'qs';
import { VerificationService } from '../verification.service';
import { EmailService } from '../../email';
import { signEmailVerification } from './sign-email-verification';
//#endregion

export class VerificationServiceImpl implements VerificationService {
  public constructor(
    private readonly emailService: EmailService,
  ) { }

  public async verify(
    baseUrl: string,
    workspaceId: string,
    email: string,
  ): Promise<void> {
    // Set expiry date 15 minutes into the future
    // tslint:disable:no-bitwise
    const expiry = (Date.now() / 1000 + 60 * 15) >>> 0;
    const signature = signEmailVerification(workspaceId, email, expiry);
    const payload = {
      workspaceId,
      email,
      expiry,
      signature,
    };
    const url = `${baseUrl}?${qs.stringify(payload)}`;
    await this.emailService.sendPlainText(url, email, 'Reserve Email Verify');
  }

  public async confirm(
    workspaceId: string,
    email: string,
    expiry: number,
    signature: string,
  ): Promise<boolean> {
    if (expiry * 1000 < Date.now()) {
      throw new Error('Verification code expired');
    }

    return signature === signEmailVerification(workspaceId, email, expiry);
  }
}
