/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import crypto from 'crypto';
import config from 'config';
//#endregion

export function signEmailVerification(
  workspaceId: string,
  email: string,
  expiry: number, // Unix timestamp
): string {
  const algorithm = config.get('email.verification.alg') as string;
  const secret = config.get('email.verification.secret') as string;

  return crypto.createHmac(algorithm, secret)
    .update(`${workspaceId}:${email}:${expiry}`)
    .digest('hex');
}
