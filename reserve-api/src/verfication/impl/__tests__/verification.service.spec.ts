/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EmailService } from '../../../email';
import { VerificationServiceImpl } from '../verification.service';
import { EmailServiceMock } from '../../../email/__mocks__';
//#endregion

jest.mock('../sign-email-verification', () => ({
  signEmailVerification: jest.fn()
    .mockImplementation((workspaceId: string, email: string, expiry: number) =>
      `${workspaceId}:${email}:${expiry}:secretsignature`),
}));

describe('verification.service', () => {
  let emailService!: EmailService;
  let verificationService!: VerificationServiceImpl;
  const baseUrl = 'http://test.localhost:3000/api/v1/email/confirm';

  beforeEach(() => {
    emailService = new EmailServiceMock();
    verificationService = new VerificationServiceImpl(emailService);
  });

  describe('#verify()', () => {
    const workspaceId = 'workspaceId';
    const email = 'email';

    it(
      'should sign workspace and email and create appropriate url',
      async () =>
    {
      // tslint:disable:no-bitwise
      const expiry = (Date.now() / 1000 + 60 * 15) >>> 0;
      const sendPlainText = jest.spyOn(emailService, 'sendPlainText');

      await verificationService.verify(baseUrl, workspaceId, email);

      const url = `${baseUrl}?workspaceId=${workspaceId}&email=${
        email
      }&expiry=${expiry}&signature=${
        workspaceId
      }%3A${email}%3A${expiry}%3Asecretsignature`;
      const title = 'Reserve Email Verify';
      expect(sendPlainText).toHaveBeenCalledTimes(1);
      expect(sendPlainText).toHaveBeenCalledWith(url, email, title);
    });
  });

  describe('#confirm()', () => {
    const workspaceId = 'workspaceId';
    const email = 'email';

    it('should return `true` if signature is matches', async () => {
      // tslint:disable:no-bitwise
      const expiry = (Date.now() / 1000 + 60 * 15) >>> 0;
      const signature = `${workspaceId}:${email}:${expiry}:secretsignature`;

      const result = await verificationService
        .confirm(workspaceId, email, expiry, signature);

      expect(result).toBe(true);
    });

    it('should return `false` if signature is matches', async () => {
      // tslint:disable:no-bitwise
      const expiry = (Date.now() / 1000 + 60 * 15) >>> 0;
      const signature = `${workspaceId}:${email}:${expiry}:secretsignatur`;

      const result = await verificationService
        .confirm(workspaceId, email, expiry, signature);

      expect(result).toBe(false);
    });

    it('throw an error if expiry date in past', async () => {
      // tslint:disable:no-bitwise
      const expiry = (Date.now() / 1000 - 60 * 15) >>> 0;
      const signature = `${workspaceId}:${email}:${expiry}:secretsignatur`;

      await expect(verificationService
        .confirm(workspaceId, email, expiry, signature),
      ).rejects.toEqual(new Error('Verification code expired'));
    });
  });
});
