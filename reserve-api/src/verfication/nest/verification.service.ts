/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { VerificationServiceImpl } from '../impl/verification.service';
import { NestEmailService } from '../../email/nest';
//#endregion

@Injectable()
export class NestVerificationService extends VerificationServiceImpl {
  public constructor(
    emailService: NestEmailService,
  ) {
    super(emailService);
  }
}
