/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationSettingsAggregate } from './reservation-settings.aggregate';
import { TimeUnit } from '../../lib/settings/time-unit';
import { Timeframe } from '../../lib/interface/timeframe.interface';
import { ReservationSettingsEvent } from './reservation-settings.event';
//#endregion

export interface ReservationSettingsService {
  // - MARK: Queries
  dumpEvents(workspaceId: string): Promise<ReservationSettingsEvent[]>;

  getByWorkspace(workspaceId: string): Promise<ReservationSettingsAggregate>;

  // - MARK: Command

  setTimeUnit(
    workspaceId: string,
    value: TimeUnit,
    changedBy: string,
  ): Promise<ReservationSettingsAggregate>;

  setMaxReservations(
    workspaceId: string,
    value: number | null,
    changedBy: string,
  ): Promise<ReservationSettingsAggregate>;

  setMaxRange(
    workspaceId: string,
    value: Timeframe | null,
    changedBy: string,
  ): Promise<ReservationSettingsAggregate>;

  setMaxOffset(
    workspaceId: string,
    value: Timeframe | null,
    changedBy: string,
  ): Promise<ReservationSettingsAggregate>;
}
