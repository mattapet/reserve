/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  ReservationSettingsService as Interface,
} from '../reservation-settings.service';
//#endregion

export const ReservationSettingsServiceMock = jest.fn<Interface, []>(() => ({
  dumpEvents: jest.fn(),
  getByWorkspace: jest.fn(),
  setTimeUnit: jest.fn(),
  setMaxReservations: jest.fn(),
  setMaxRange: jest.fn(),
  setMaxOffset: jest.fn(),
}));
