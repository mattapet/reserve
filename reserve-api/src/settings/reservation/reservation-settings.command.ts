/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '../../lib/events/command';
import { TimeUnit } from '../../lib/settings/time-unit';
import { Timeframe } from '../../lib/interface/timeframe.interface';
//#endregion

export enum ReservationSettingsCommandType {
  setTimeUnit = 'set_time_unit',
  setMaxReservations = 'set_max_reservations',
  setMaxRange = 'set_max_range',
  setMaxOffset = 'set_max_offset',
}

interface SetTimeUnit extends Command {
  readonly type: ReservationSettingsCommandType.setTimeUnit;
  readonly payload: {
    readonly value: TimeUnit;
    readonly changedBy: string;
  };
}

interface SetMaxReservations extends Command {
  readonly type: ReservationSettingsCommandType.setMaxReservations;
  readonly payload: {
    readonly value: number | null;
    readonly changedBy: string;
  };
}

interface SetMaxRange extends Command {
  readonly type: ReservationSettingsCommandType.setMaxRange;
  readonly payload: {
    readonly value: Timeframe | null;
    readonly changedBy: string;
  };
}

interface SetMaxOffset extends Command {
  readonly type: ReservationSettingsCommandType.setMaxOffset;
  readonly payload: {
    readonly value: Timeframe | null;
    readonly changedBy: string;
  };
}

export type ReservationSettingsCommand =
  | SetTimeUnit
  | SetMaxReservations
  | SetMaxRange
  | SetMaxOffset;
