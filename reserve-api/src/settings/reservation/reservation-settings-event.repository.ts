/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationSettingsEvent } from './reservation-settings.event';
import { BaseEventRepository } from '../../lib/events/repository-base';
//#endregion

export interface ReservationSettingsEventRepository
  extends BaseEventRepository<ReservationSettingsEvent> { }
