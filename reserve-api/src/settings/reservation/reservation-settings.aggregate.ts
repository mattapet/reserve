/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TimeUnit } from '../../lib/settings/time-unit';
import { Timeframe } from '../../lib/interface/timeframe.interface';
//#endregion

export interface ReservationSettingsAggregate {
  readonly timeUnit: TimeUnit;
  readonly maxReservations: number | null;
  readonly maxRange: Timeframe | null;
  readonly maxOffset: Timeframe | null;
}

export const defaultAggregate: ReservationSettingsAggregate = {
  timeUnit: TimeUnit.day,
  maxReservations: null,
  maxRange: null,
  maxOffset: null,
};
