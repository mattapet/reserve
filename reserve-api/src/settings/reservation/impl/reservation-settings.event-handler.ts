/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  ReservationSettingsAggregate,
} from '../reservation-settings.aggregate';
import {
  ReservationSettingsEvent,
  ReservationSettingsEventType,
} from '../reservation-settings.event';
//#endregion

export function reservationSettingsEventHandler(
  aggregate: ReservationSettingsAggregate,
  event: ReservationSettingsEvent,
): ReservationSettingsAggregate {
  switch (event.type) {
  case ReservationSettingsEventType.timeUnitSet:
    return {
      ...aggregate,
      timeUnit: event.payload.value,
    };

  case ReservationSettingsEventType.maxRangeSet:
    return {
      ...aggregate,
      maxRange: event.payload.value,
    };
  case ReservationSettingsEventType.maxRangeDisabled:
    return {
      ...aggregate,
      maxRange: null,
    };

  case ReservationSettingsEventType.maxOffsetSet:
    return {
      ...aggregate,
      maxOffset: event.payload.value,
    };
  case ReservationSettingsEventType.maxOffsetDisabled:
    return {
      ...aggregate,
      maxOffset: null,
    };

  case ReservationSettingsEventType.maxReservationsSet:
    return {
      ...aggregate,
      maxReservations: event.payload.value,
    };
  case ReservationSettingsEventType.maxReservationsDisabled:
    return {
      ...aggregate,
      maxReservations: null,
    };
  }
}
