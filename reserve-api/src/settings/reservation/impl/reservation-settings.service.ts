/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  ReservationSettingsCommandType,
} from '../reservation-settings.command';
import {
  ReservationSettingsRepository,
} from '../reservation-settings.repository';
import {
  ReservationSettingsAggregate,
} from '../reservation-settings.aggregate';
import {

} from '../';
import { UserRoleService } from '../../../user/user-role';
import { TimeUnit } from '../../../lib/settings/time-unit';
import { Timeframe } from '../../../lib/interface/timeframe.interface';
import { ReservationSettingsEvent } from '../reservation-settings.event';
import {
  ReservationSettingsService as Interface,
} from '../reservation-settings.service';
//#endregion

export class ReservationSettingsService implements Interface {
  public constructor(
    private readonly repository: ReservationSettingsRepository,
    private readonly userRole: UserRoleService,
  ) { }

  // - MARK: Queries

  public async dumpEvents(
    workspaceId: string,
  ): Promise<ReservationSettingsEvent[]> {
    return await this.repository.dumpEvents(workspaceId);
  }

  public async getByWorkspace(
    workspaceId: string,
  ): Promise<ReservationSettingsAggregate> {
    return this.repository.getById(workspaceId, workspaceId);
  }

  // - MARK: Command

  public async setTimeUnit(
    workspaceId: string,
    value: TimeUnit,
    changedBy: string,
  ): Promise<ReservationSettingsAggregate> {
    await this.ensureOwner(workspaceId, changedBy);
    const aggregate = await this.getByWorkspace(workspaceId);

    return await this.repository.execute({
      type: ReservationSettingsCommandType.setTimeUnit,
      workspaceId,
      aggregateId: workspaceId,
      payload: {
        value,
        changedBy,
      },
    }, aggregate);
  }

  public async setMaxReservations(
    workspaceId: string,
    value: number | null,
    changedBy: string,
  ): Promise<ReservationSettingsAggregate> {
    await this.ensureOwner(workspaceId, changedBy);
    const aggregate = await this.getByWorkspace(workspaceId);

    return this.repository.execute({
      type: ReservationSettingsCommandType.setMaxReservations,
      workspaceId,
      aggregateId: workspaceId,
      payload: {
        value,
        changedBy,
      },
    }, aggregate);
  }

  public async setMaxRange(
    workspaceId: string,
    value: Timeframe | null,
    changedBy: string,
  ): Promise<ReservationSettingsAggregate> {
    await this.ensureOwner(workspaceId, changedBy);
    const aggregate = await this.getByWorkspace(workspaceId);

    return this.repository.execute({
      type: ReservationSettingsCommandType.setMaxRange,
      workspaceId,
      aggregateId: workspaceId,
      payload: {
        value,
        changedBy,
      },
    }, aggregate);
  }

  public async setMaxOffset(
    workspaceId: string,
    value: Timeframe | null,
    changedBy: string,
  ): Promise<ReservationSettingsAggregate> {
    await this.ensureOwner(workspaceId, changedBy);
    const aggregate = await this.getByWorkspace(workspaceId);

    return this.repository.execute({
      type: ReservationSettingsCommandType.setMaxOffset,
      workspaceId,
      aggregateId: workspaceId,
      payload: {
        value,
        changedBy,
      },
    }, aggregate);
  }

  private async ensureOwner(
    workspaceId: string,
    changedBy: string,
  ): Promise<void> {
    if (!await this.userRole.isOwner(workspaceId, changedBy)) {
      throw new Error('Only Owners can change worksapce settings.');
    }
  }
}
