/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  reservationSettingsCommandHandler,
} from '../reservation-settings.command-handler';
import {
  ReservationSettingsCommandType,
} from '../../reservation-settings.command';
import { ReservationSettingsEventType } from '../../reservation-settings.event';
import { defaultAggregate } from '../../reservation-settings.aggregate';
import { TimeUnit } from '../../../../lib/settings/time-unit';
//#endregion

describe('reservation-settings.command-handler', () => {
  const workspaceId = 'test';
  const aggregateId = workspaceId;
  const changedBy = '49';

  const makeCommandCreator = <T extends ReservationSettingsCommandType>(
    type: T,
  ) =>
    <Value>(value: Value) => ({
      type,
      workspaceId,
      aggregateId,
      payload: {
        value,
        changedBy,
      },
    });

  describe('setMaxReservations command', () => {
    const makeCommand =
      makeCommandCreator(ReservationSettingsCommandType.setMaxReservations);

    it('should produce `maxReservationsSet` event', () => {
      const value = 94;
      const command = makeCommand(value);

      const [result] =
        reservationSettingsCommandHandler(defaultAggregate, command);

      expect(result.type)
        .toEqual(ReservationSettingsEventType.maxReservationsSet);
      expect(result.payload).toEqual(command.payload);
    });

    it('should produce `maxReservationsDisabled` event', () => {
      const value = null;
      const command = makeCommand(value);

      const [result] =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxReservations: 49,
        }, command);

      expect(result.type)
        .toEqual(ReservationSettingsEventType.maxReservationsDisabled);
      expect(result.payload).toEqual({ changedBy });
    });

    it('should produce no events when no change made', () => {
      const value = null;
      const command = makeCommand(value);

      const results =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxReservations: null,
        }, command);

      expect(results).toEqual([]);
    });

    it('should produce no events when no change made', () => {
      const value = 94;
      const command = makeCommand(value);

      const results =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxReservations: 94,
        }, command);

      expect(results).toEqual([]);
    });

    it('should throw an error is trying to set negative value', () => {
      const value = -94;
      const command = makeCommand(value);

      const fn = () =>
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxReservations: 94,
        }, command);

      expect(fn)
        .toThrowError('Max reservation must be a posistive value.');
    });
  });

  describe('setMaxRange command', () => {
    const makeCommand =
      makeCommandCreator(ReservationSettingsCommandType.setMaxRange);

    it('should produce `maxRangeSet` event', () => {
      const value = {
        amount: 30,
        units: TimeUnit.day,
      };
      const command = makeCommand(value);

      const [result] =
        reservationSettingsCommandHandler(defaultAggregate, command);

      expect(result.type)
        .toEqual(ReservationSettingsEventType.maxRangeSet);
      expect(result.payload).toEqual(command.payload);
    });

    it('should produce `maxRangeSet` event', () => {
      const value = {
        amount: 30,
        units: TimeUnit.day,
      };
      const command = makeCommand(value);

      const [result] =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxRange: { ...value, amount: 4 },
        }, command);

      expect(result.type)
        .toEqual(ReservationSettingsEventType.maxRangeSet);
      expect(result.payload).toEqual(command.payload);
    });

    it('should produce `maxRangeSet` event', () => {
      const value = {
        amount: 30,
        units: TimeUnit.day,
      };
      const command = makeCommand(value);

      const [result] =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxRange: { ...value, units: TimeUnit.hour },
        }, command);

      expect(result.type)
        .toEqual(ReservationSettingsEventType.maxRangeSet);
      expect(result.payload).toEqual(command.payload);
    });

    it('should produce `maxRangeDisabled` event', () => {
      const value = null;
      const command = makeCommand(value);

      const [result] =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxRange: {
            amount: 30,
            units: TimeUnit.day,
          },
        }, command);

      expect(result.type)
        .toEqual(ReservationSettingsEventType.maxRangeDisabled);
      expect(result.payload).toEqual({ changedBy });
    });

    it('should produce no events when no change made', () => {
      const value = null;
      const command = makeCommand(value);

      const results =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxRange: null,
        }, command);

      expect(results).toEqual([]);
    });

    it('should produce no events when no change made', () => {
      const value = {
        amount: 30,
        units: TimeUnit.day,
      };
      const command = makeCommand(value);

      const results =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxRange: value,
        }, command);

      expect(results).toEqual([]);
    });

    it('should thow an error if negative amount is set', () => {
      const value = {
        amount: -30,
        units: TimeUnit.day,
      };
      const command = makeCommand(value);

      const fn = () =>
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxRange: value,
        }, command);

      expect(fn).toThrowError('Max range must be a posistive value.');
    });
  });

  describe('setMaxOffset command', () => {
    const makeCommand =
      makeCommandCreator(ReservationSettingsCommandType.setMaxOffset);

    it('should produce `maxOffsetSet` event', () => {
      const value = {
        amount: 30,
        units: TimeUnit.day,
      };
      const command = makeCommand(value);

      const [result] =
        reservationSettingsCommandHandler(defaultAggregate, command);

      expect(result.type)
        .toEqual(ReservationSettingsEventType.maxOffsetSet);
      expect(result.payload).toEqual(command.payload);
    });

    it('should produce `maxOffsetSet` event', () => {
      const value = {
        amount: 30,
        units: TimeUnit.day,
      };
      const command = makeCommand(value);

      const [result] =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxOffset: { ...value, amount: 4 },
        }, command);

      expect(result.type)
        .toEqual(ReservationSettingsEventType.maxOffsetSet);
      expect(result.payload).toEqual(command.payload);
    });

    it('should produce `maxOffsetSet` event', () => {
      const value = {
        amount: 30,
        units: TimeUnit.day,
      };
      const command = makeCommand(value);

      const [result] =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxOffset: { ...value, units: TimeUnit.hour },
        }, command);

      expect(result.type)
        .toEqual(ReservationSettingsEventType.maxOffsetSet);
      expect(result.payload).toEqual(command.payload);
    });

    it('should produce `maxOffsetDisabled` event', () => {
      const value = null;
      const command = makeCommand(value);

      const [result] =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxOffset: {
            amount: 30,
            units: TimeUnit.day,
          },
        }, command);

      expect(result.type)
        .toEqual(ReservationSettingsEventType.maxOffsetDisabled);
      expect(result.payload).toEqual({ changedBy });
    });

    it('should produce no events when no change made', () => {
      const value = null;
      const command = makeCommand(value);

      const results =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxOffset: null,
        }, command);

      expect(results).toEqual([]);
    });

    it('should produce no events when no change made', () => {
      const value = {
        amount: 30,
        units: TimeUnit.day,
      };
      const command = makeCommand(value);

      const results =
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxOffset: value,
        }, command);

      expect(results).toEqual([]);
    });

    it('should thow an error if negative amount is set', () => {
      const value = {
        amount: -30,
        units: TimeUnit.day,
      };
      const command = makeCommand(value);

      const fn = () =>
        reservationSettingsCommandHandler({
          ...defaultAggregate,
          maxOffset: value,
        }, command);

      expect(fn).toThrowError('Max offset must be a posistive value.');
    });
  });

  describe('setTimeUnit command', () => {
    const makeCommand =
      makeCommandCreator(ReservationSettingsCommandType.setTimeUnit);

    it('should produce a `timeUnitSet` event', () => {
      const value = TimeUnit.minute;
      const command = makeCommand(value);

      const [result] =
        reservationSettingsCommandHandler(
          { ...defaultAggregate, timeUnit: TimeUnit.day },
          command,
        );

      expect(result.type).toEqual(ReservationSettingsEventType.timeUnitSet);
      expect(result.payload).toEqual(command.payload);
    });

    it('should produce a no events', () => {
      const value = TimeUnit.minute;
      const command = makeCommand(value);

      const results =
        reservationSettingsCommandHandler(
          { ...defaultAggregate, timeUnit: value },
          command,
        );

      expect(results).toEqual([]);
    });
  });
});
