/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  reservationSettingsEventHandler,
} from '../reservation-settings.event-handler';
import { ReservationSettingsEventType } from '../../reservation-settings.event';
import { defaultAggregate } from '../../reservation-settings.aggregate';
import { TimeUnit } from '../../../../lib/settings/time-unit';
//#endregion

describe('reservation-settings.event-handler', () => {
  const workspaceId = 'test';
  const aggregateId = workspaceId;
  const timestamp = new Date(0);
  const changedBy = '49';

  const makeEventCreator = <T extends ReservationSettingsEventType>(
    type: T,
  ) =>
    <Value extends {}>(value: Value) => ({
      type,
      workspaceId,
      aggregateId,
      timestamp,
      payload: {
        value,
        changedBy,
      },
    });

  describe('timeUnitSet evnet', () => {
    const makeEvent =
      makeEventCreator(ReservationSettingsEventType.timeUnitSet);

    it('should set `timeUnit`', () => {
      const value = TimeUnit.day;
      const event = makeEvent(value);

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        timeUnit: TimeUnit.hour,
      }, event);

      expect(result.timeUnit).toEqual(value);
    });

    it('should set `timeUnit`', () => {
      const value = TimeUnit.minute;
      const event = makeEvent(value);

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        timeUnit: TimeUnit.hour,
      }, event);

      expect(result.timeUnit).toEqual(value);
    });

    it('should set `timeUnit`', () => {
      const value = TimeUnit.hour;
      const event = makeEvent(value);

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        timeUnit: TimeUnit.day,
      }, event);

      expect(result.timeUnit).toEqual(value);
    });
  });

  describe('maxRangeSet event', () => {
    const makeEvent =
      makeEventCreator(ReservationSettingsEventType.maxRangeSet);

    it('should set `maxRangeSet`', () => {
      const value = {
        units: TimeUnit.day,
        amount: 49,
      };
      const event = makeEvent(value);

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        maxRange: null,
      }, event);

      expect(result.maxRange).toEqual(value);
    });

    it('should set `maxRangeSet`', () => {
      const value = {
        units: TimeUnit.hour,
        amount: 44,
      };
      const event = makeEvent(value);

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        maxRange: {
          units: TimeUnit.minute,
          amount: 12,
        },
      }, event);

      expect(result.maxRange).toEqual(value);
    });
  });

  describe('maxRangeDisable', () => {
    const makeEvent =
      makeEventCreator(ReservationSettingsEventType.maxRangeDisabled);

    it('should set `maxRangeSet` to `null`', () => {
      const event = makeEvent({});

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        maxRange: {
          units: TimeUnit.day,
          amount: 12,
        },
      }, event);

      expect(result.maxRange).toBeNull();
    });
  });

  describe('maxOffsetSet event', () => {
    const makeEvent =
      makeEventCreator(ReservationSettingsEventType.maxOffsetSet);

    it('should set `maxOffset`', () => {
      const value = {
        units: TimeUnit.day,
        amount: 49,
      };
      const event = makeEvent(value);

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        maxOffset: null,
      }, event);

      expect(result.maxOffset).toEqual(value);
    });

    it('should set `maxOffset`', () => {
      const value = {
        units: TimeUnit.hour,
        amount: 44,
      };
      const event = makeEvent(value);

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        maxOffset: {
          units: TimeUnit.minute,
          amount: 12,
        },
      }, event);

      expect(result.maxOffset).toEqual(value);
    });
  });

  describe('maxOffsetDisabled', () => {
    const makeEvent =
      makeEventCreator(ReservationSettingsEventType.maxOffsetDisabled);

    it('should set `maxOffset` to `null`', () => {
      const event = makeEvent({});

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        maxOffset: {
          units: TimeUnit.day,
          amount: 12,
        },
      }, event);

      expect(result.maxOffset).toBeNull();
    });
  });

  describe('maxReservationsSet event', () => {
    const makeEvent =
      makeEventCreator(ReservationSettingsEventType.maxReservationsSet);

    it('should set `maxReservations`', () => {
      const value = 49;
      const event = makeEvent(value);

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        maxReservations: null,
      }, event);

      expect(result.maxReservations).toEqual(value);
    });

    it('should set `maxReservations`', () => {
      const value = 44;
      const event = makeEvent(value);

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        maxReservations: 12,
      }, event);

      expect(result.maxReservations).toEqual(value);
    });
  });

  describe('maxReservationsDisabled', () => {
    const makeEvent =
      makeEventCreator(ReservationSettingsEventType.maxReservationsDisabled);

    it('should set `maxReservations` to `null`', () => {
      const event = makeEvent({});

      const result = reservationSettingsEventHandler({
        ...defaultAggregate,
        maxReservations: 12,
      }, event);

      expect(result.maxReservations).toBeNull();
    });
  });
});
