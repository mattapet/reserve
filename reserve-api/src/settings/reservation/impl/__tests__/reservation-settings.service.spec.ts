/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationSettingsService } from '../reservation-settings.service';
import { defaultAggregate } from '../../reservation-settings.aggregate';
import {
  ReservationSettingsRepository,
} from '../../reservation-settings.repository';
import { ReservationSettingsRepositoryMock } from '../../__mocks__';
import { UserRoleService } from '../../../../user/user-role';
import { UserRoleServiceMock } from '../../../../user/user-role/__mocks__';
import { TimeUnit } from '../../../../lib/settings/time-unit';
import {
  ReservationSettingsCommandType,
} from '../../reservation-settings.command';
//#endregion

describe('reservation-settings.service', () => {
  let userRoleService!: UserRoleService;
  let reservationSettingsRepository!: ReservationSettingsRepository;
  let reservationSettingsService!: ReservationSettingsService;

  beforeEach(() => {
    userRoleService = new UserRoleServiceMock();
    reservationSettingsRepository = new ReservationSettingsRepositoryMock();
    reservationSettingsService = new ReservationSettingsService(
      reservationSettingsRepository,
      userRoleService,
    );
  });

  describe('#dumpEvents()', () => {
    it('should return all events from repository', async () => {
      const workspaceId = 'test';
      const fn = jest.spyOn(reservationSettingsRepository, 'dumpEvents');

      await reservationSettingsService.dumpEvents(workspaceId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId);
    });
  });

  describe('#getByWorkspace()', () => {
    it('should return settings for workspace from repository', async () => {
      const workspaceId = 'test';
      const fn = jest.spyOn(reservationSettingsRepository, 'getById');

      await reservationSettingsService.getByWorkspace(workspaceId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId, workspaceId);
    });
  });

  describe('#getByWorkspace()', () => {
    it('should return settings for workspace from repository', async () => {
      const workspaceId = 'test';
      const fn = jest.spyOn(reservationSettingsRepository, 'getById');

      await reservationSettingsService.getByWorkspace(workspaceId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId, workspaceId);
    });
  });

  describe('#setTimeUnit()', () => {
    it('should produce `setTimeUnit` command', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const value = TimeUnit.day;
      const isOwner = jest
        .spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(true));
      const getById = jest
        .spyOn(reservationSettingsRepository, 'getById')
        .mockImplementation(() => Promise.resolve(defaultAggregate));
      const execute = jest.spyOn(reservationSettingsRepository, 'execute');

      await reservationSettingsService.setTimeUnit(
        workspaceId,
        value,
        changedBy,
      );

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, workspaceId);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith(
        {
          type: ReservationSettingsCommandType.setTimeUnit,
          workspaceId,
          aggregateId: workspaceId,
          payload: {
            value,
            changedBy,
          },
        },
        defaultAggregate,
      );
    });

    it('should throw an error if not an owner sets time unit', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const value = TimeUnit.day;
      const isOwner = jest
        .spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(false));
      const getById = jest.spyOn(reservationSettingsRepository, 'getById');
      const execute = jest.spyOn(reservationSettingsRepository, 'execute');

      await expect(
        reservationSettingsService.setTimeUnit(workspaceId, value, changedBy),
      ).rejects.toEqual(
        new Error('Only Owners can change worksapce settings.'),
      );

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).not.toHaveBeenCalled();
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#setMaxReservations()', () => {
    it('should produce `setMaxReservations` command', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const value = 94;
      const isOwner = jest
        .spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(true));
      const getById = jest
        .spyOn(reservationSettingsRepository, 'getById')
        .mockImplementation(() => Promise.resolve(defaultAggregate));
      const execute = jest.spyOn(reservationSettingsRepository, 'execute');

      await reservationSettingsService.setMaxReservations(
        workspaceId,
        value,
        changedBy,
      );

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, workspaceId);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith(
        {
          type: ReservationSettingsCommandType.setMaxReservations,
          workspaceId,
          aggregateId: workspaceId,
          payload: {
            value,
            changedBy,
          },
        },
        defaultAggregate,
      );
    });

    it('should throw an error if not an owner sets time unit', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const value = 94;
      const isOwner = jest
        .spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(false));
      const getById = jest.spyOn(reservationSettingsRepository, 'getById');
      const execute = jest.spyOn(reservationSettingsRepository, 'execute');

      await expect(
        reservationSettingsService
          .setMaxReservations(workspaceId, value, changedBy),
      ).rejects.toEqual(
        new Error('Only Owners can change worksapce settings.'),
      );

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).not.toHaveBeenCalled();
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#setMaxRange()', () => {
    it('should produce `setMaxRange` command', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const value = {
        amount: 30,
        units: TimeUnit.day,
      };
      const isOwner = jest
        .spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(true));
      const getById = jest
        .spyOn(reservationSettingsRepository, 'getById')
        .mockImplementation(() => Promise.resolve(defaultAggregate));
      const execute = jest.spyOn(reservationSettingsRepository, 'execute');

      await reservationSettingsService.setMaxRange(
        workspaceId,
        value,
        changedBy,
      );

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, workspaceId);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith(
        {
          type: ReservationSettingsCommandType.setMaxRange,
          workspaceId,
          aggregateId: workspaceId,
          payload: {
            value,
            changedBy,
          },
        },
        defaultAggregate,
      );
    });

    it('should throw an error if not an owner sets time unit', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const value = null;
      const isOwner = jest
        .spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(false));
      const getById = jest.spyOn(reservationSettingsRepository, 'getById');
      const execute = jest.spyOn(reservationSettingsRepository, 'execute');

      await expect(
        reservationSettingsService.setMaxRange(workspaceId, value, changedBy),
      ).rejects.toEqual(
        new Error('Only Owners can change worksapce settings.'),
      );

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).not.toHaveBeenCalled();
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#setMaxOffset()', () => {
    it('should produce `setMaxOffset` command', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const value = {
        amount: 30,
        units: TimeUnit.day,
      };
      const isOwner = jest
        .spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(true));
      const getById = jest
        .spyOn(reservationSettingsRepository, 'getById')
        .mockImplementation(() => Promise.resolve(defaultAggregate));
      const execute = jest.spyOn(reservationSettingsRepository, 'execute');

      await reservationSettingsService.setMaxOffset(
        workspaceId,
        value,
        changedBy,
      );

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, workspaceId);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith(
        {
          type: ReservationSettingsCommandType.setMaxOffset,
          workspaceId,
          aggregateId: workspaceId,
          payload: {
            value,
            changedBy,
          },
        },
        defaultAggregate,
      );
    });

    it('should throw an error if not an owner sets time unit', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const value = null;
      const isOwner = jest
        .spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(false));
      const getById = jest.spyOn(reservationSettingsRepository, 'getById');
      const execute = jest.spyOn(reservationSettingsRepository, 'execute');

      await expect(
        reservationSettingsService.setMaxRange(workspaceId, value, changedBy),
      ).rejects.toEqual(
        new Error('Only Owners can change worksapce settings.'),
      );

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).not.toHaveBeenCalled();
      expect(execute).not.toHaveBeenCalled();
    });
  });
});
