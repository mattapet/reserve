/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  ReservationSettingsAggregate,
} from '../reservation-settings.aggregate';
import {
  ReservationSettingsCommand,
  ReservationSettingsCommandType,
} from '../reservation-settings.command';
import {
  ReservationSettingsEvent,
  ReservationSettingsEventType,
} from '../reservation-settings.event';
//#endregion

export function reservationSettingsCommandHandler(
  aggregate: ReservationSettingsAggregate,
  command: ReservationSettingsCommand,
): ReservationSettingsEvent[] {
  const { workspaceId, aggregateId } = command;
  switch (command.type) {
  case ReservationSettingsCommandType.setMaxReservations:
    if (aggregate.maxReservations === command.payload.value) {
      return [];
    }

    if (command.payload.value != null) {
      if (command.payload.value <= 0) {
        throw new Error('Max reservation must be a posistive value.');
      }

      return [{
        type: ReservationSettingsEventType.maxReservationsSet,
        workspaceId,
        aggregateId,
        timestamp: new Date(),
        payload: {
          value: command.payload.value,
          changedBy: command.payload.changedBy,
        },
      }];
    } else {
      return [{
        type: ReservationSettingsEventType.maxReservationsDisabled,
        workspaceId,
        aggregateId,
        timestamp: new Date(),
        payload: {
          changedBy: command.payload.changedBy,
        },
      }];
    }

  case ReservationSettingsCommandType.setMaxRange:
      if (command.payload.value) {
        if (command.payload.value.amount <= 0) {
          throw new Error('Max range must be a posistive value.');
        }

        if (aggregate.maxRange &&
          aggregate.maxRange.amount === command.payload.value.amount &&
          aggregate.maxRange.units === command.payload.value.units
        ) {
          return [];
        }

        return [{
          type: ReservationSettingsEventType.maxRangeSet,
          workspaceId,
          aggregateId,
          timestamp: new Date(),
          payload: {
            value: command.payload.value,
            changedBy: command.payload.changedBy,
          },
        }];

      } else {
        if (!aggregate.maxRange) {
          return [];
        }

        return [{
          type: ReservationSettingsEventType.maxRangeDisabled,
          workspaceId,
          aggregateId,
          timestamp: new Date(),
          payload: {
            changedBy: command.payload.changedBy,
          },
        }];
      }

  case ReservationSettingsCommandType.setMaxOffset:
      if (command.payload.value) {
        if (command.payload.value.amount <= 0) {
          throw new Error('Max offset must be a posistive value.');
        }

        if (aggregate.maxOffset &&
          aggregate.maxOffset.amount === command.payload.value.amount &&
          aggregate.maxOffset.units === command.payload.value.units
        ) {
          return [];
        }

        return [{
          type: ReservationSettingsEventType.maxOffsetSet,
          workspaceId,
          aggregateId,
          timestamp: new Date(),
          payload: {
            value: command.payload.value,
            changedBy: command.payload.changedBy,
          },
        }];

      } else {
        if (!aggregate.maxOffset) {
          return [];
        }

        return [{
          type: ReservationSettingsEventType.maxOffsetDisabled,
          workspaceId,
          aggregateId,
          timestamp: new Date(),
          payload: {
            changedBy: command.payload.changedBy,
          },
        }];
      }

  case ReservationSettingsCommandType.setTimeUnit:
    if (aggregate.timeUnit === command.payload.value) {
      return [];
    }

    return [{
      type: ReservationSettingsEventType.timeUnitSet,
      workspaceId,
      aggregateId,
      timestamp: new Date(),
      payload: command.payload,
    }];
  }
}
