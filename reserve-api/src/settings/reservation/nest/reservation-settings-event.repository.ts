/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository, Repository } from 'typeorm';

import {
  ReservationSettingsEventEntity,
} from './reservation-settings-event.entity';
import {
  ReservationSettingsEventRepository as Interface,
} from '../reservation-settings-event.repository';
import { ReservationSettingsEvent } from '../reservation-settings.event';
//#endregion

@EntityRepository(ReservationSettingsEventEntity)
export class ReservationSettingsEventRepository
  extends Repository<ReservationSettingsEventEntity>
  implements Interface
{
  public async findByRowId(rowId: string): Promise<ReservationSettingsEvent[]> {
    const events = await this.find({
      where: { rowId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async findByRowIdAndAggregateId(
    rowId: string,
    aggregateId: string,
  ): Promise<ReservationSettingsEvent[]> {
    const events = await this.find({
      where: { rowId, aggregateId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async saveEvents(
    ...events: ReservationSettingsEvent[]
  ): Promise<ReservationSettingsEvent[]> {
    const results = await this.save(events.map(this.wrap));
    return results.map(this.unwrap);
  }

  public wrap(event: ReservationSettingsEvent): ReservationSettingsEventEntity {
    const { workspaceId, aggregateId, timestamp } = event;
    return new ReservationSettingsEventEntity(
      workspaceId,
      aggregateId,
      timestamp,
      event,
    );
  }

  public unwrap(
    entity: ReservationSettingsEventEntity,
  ): ReservationSettingsEvent {
    return entity.payload;
  }
}
