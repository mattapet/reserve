/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity } from 'typeorm';

import { BaseEvent } from '../../../lib/events/base-event.entity';
import {
  ReservationSettingsEvent as Payload,
} from '../reservation-settings.event';
//#endregion

@Entity({ name: 'reservation_settings_event' })
export class ReservationSettingsEventEntity extends BaseEvent<Payload> { }
