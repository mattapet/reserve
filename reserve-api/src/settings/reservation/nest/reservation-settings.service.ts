/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
} from '@nestjs/common';
import { Connection } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';

import { NestUserRoleService } from '../../../user/user-role/nest';
import {
  ReservationSettingsService as Impl,
} from '../impl/reservation-settings.service';
import {
  ReservationSettingsEventRepository,
} from './reservation-settings-event.repository';
import { BaseRepository } from '../../../lib/events/repository-base';
import {
  reservationSettingsCommandHandler,
} from '../impl/reservation-settings.command-handler';
import {
  reservationSettingsEventHandler,
} from '../impl/reservation-settings.event-handler';
import { defaultAggregate } from '../reservation-settings.aggregate';
//#endregion

@Injectable()
export class ReservationSettingsService extends Impl {
  public constructor(
    @InjectConnection()
    connection: Connection,
    userRole: NestUserRoleService,
  ) {
    super(
      new BaseRepository(
        connection.getCustomRepository(ReservationSettingsEventRepository),
        reservationSettingsCommandHandler,
        reservationSettingsEventHandler,
        defaultAggregate,
      ),
      userRole,
    );
  }
}
