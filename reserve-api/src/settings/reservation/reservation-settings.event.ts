/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '../../lib/events/Event';
import { TimeUnit } from '../../lib/settings/time-unit';
import { Timeframe } from '../../lib/interface/timeframe.interface';
//#endregion

export enum ReservationSettingsEventType {
  timeUnitSet = 'time_unit_set',
  maxReservationsSet = 'max_reservations_set',
  maxReservationsDisabled = 'max_reservations_disabled',
  maxRangeSet = 'max_range_set',
  maxRangeDisabled = 'max_range_disabled',
  maxOffsetSet = 'max_offset_set',
  maxOffsetDisabled = 'max_offset_disabled',
}

interface SetTimeUnit extends Event {
  readonly type: ReservationSettingsEventType.timeUnitSet;
  readonly payload: {
    readonly value: TimeUnit;
    readonly changedBy: string;
  };
}

interface SetMaxReservations extends Event {
  readonly type: ReservationSettingsEventType.maxReservationsSet;
  readonly payload: {
    readonly value: number;
    readonly changedBy: string;
  };
}

interface MaxReservationsDisabled extends Event {
  readonly type: ReservationSettingsEventType.maxReservationsDisabled;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface SetMaxRange extends Event {
  readonly type: ReservationSettingsEventType.maxRangeSet;
  readonly payload: {
    readonly value: Timeframe;
    readonly changedBy: string;
  };
}

interface MaxRangeDisabled extends Event {
  readonly type: ReservationSettingsEventType.maxRangeDisabled;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface SetMaxOffset extends Event {
  readonly type: ReservationSettingsEventType.maxOffsetSet;
  readonly payload: {
    readonly value: Timeframe;
    readonly changedBy: string;
  };
}

interface MaxOffsetDisabled extends Event {
  readonly type: ReservationSettingsEventType.maxOffsetDisabled;
  readonly payload: {
    readonly changedBy: string;
  };
}

export type ReservationSettingsEvent =
  | SetTimeUnit
  | SetMaxReservations
  | MaxReservationsDisabled
  | SetMaxRange
  | MaxRangeDisabled
  | SetMaxOffset
  | MaxOffsetDisabled;
