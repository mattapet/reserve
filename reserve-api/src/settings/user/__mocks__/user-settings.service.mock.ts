/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserSettingsService } from '../user-settings.service';
//#endregion

export const UserSettingsServiceMock = jest.fn<UserSettingsService, []>(() => ({
  dumpEvents: jest.fn(),
  getByWorkspace: jest.fn(),
  toggleAllowRegistration: jest.fn(),
  toggleAllowInvitations: jest.fn(),
  setAllowedToInviteRole: jest.fn(),
}));
