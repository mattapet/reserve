/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserSettingsAggregate } from '../user-settings.aggregate';
import {
  UserSettingsCommand,
  UserSettingsCommandType,
} from '../user-settings.command';
import {
  UserSettingsEventType,
  UserSettingsEvent,
} from '../user-settings.event';
//#endregion

export function userSettingsCommandHandler(
  aggregate: UserSettingsAggregate,
  command: UserSettingsCommand,
): UserSettingsEvent[] {
  const { workspaceId, aggregateId } = command;
  switch (command.type) {
  case UserSettingsCommandType.toggleAllowRegistrations:
    return [{
      type: aggregate.allowedRegistrations ?
        UserSettingsEventType.registrationsDisallowed :
        UserSettingsEventType.registrationsAllowed,
      workspaceId,
      aggregateId,
      timestamp: new Date(),
      payload: command.payload,
    }];

  case UserSettingsCommandType.toggleAllowInvitations:
    return [{
      type: aggregate.allowedInvitations ?
        UserSettingsEventType.invitationsDisallowed :
        UserSettingsEventType.invitationsAllowed,
      workspaceId,
      aggregateId,
      timestamp: new Date(),
      payload: command.payload,
    }];

  case UserSettingsCommandType.setAllowedToInviteRole:
    if (aggregate.allowedToInvite === command.payload.role) {
      return [];
    }

    return [{
      type: UserSettingsEventType.allowedToInviteRoleChanged,
      workspaceId,
      aggregateId,
      timestamp: new Date(),
      payload: command.payload,
    }];
  }
}
