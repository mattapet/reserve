/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { userSettingsEventHandler } from '../user-settings.event-handler';
import { defaultAggregate } from '../../user-settings.aggregate';
import {
  UserSettingsEvent,
  UserSettingsEventType,
} from '../../user-settings.event';
import { UserRole } from '../../../../lib/user/user-role.type';
//#endregion

describe('user-settings.event-handler', () => {
  const workspaceId = 'test';
  const aggregateId = workspaceId;
  const timestamp = new Date(0);
  const changedBy = '49';

  it('should set `allowedRegistrations` to `true`', () => {
    const event: UserSettingsEvent = {
      type: UserSettingsEventType.registrationsAllowed,
      workspaceId,
      aggregateId,
      timestamp,
      payload: {
        changedBy,
      },
    };

    const result = userSettingsEventHandler({
      ...defaultAggregate,
      allowedRegistrations: false,
    }, event);

    expect(result.allowedRegistrations).toBe(true);
  });

  it('should set `allowedRegistrations` to `false`', () => {
    const event: UserSettingsEvent = {
      type: UserSettingsEventType.registrationsDisallowed,
      workspaceId,
      aggregateId,
      timestamp,
      payload: {
        changedBy,
      },
    };

    const result = userSettingsEventHandler({
      ...defaultAggregate,
      allowedRegistrations: true,
    }, event);

    expect(result.allowedRegistrations).toBe(false);
  });

  it('should set `allowedInvitations` to `true`', () => {
    const event: UserSettingsEvent = {
      type: UserSettingsEventType.invitationsAllowed,
      workspaceId,
      aggregateId,
      timestamp,
      payload: {
        changedBy,
      },
    };

    const result = userSettingsEventHandler({
      ...defaultAggregate,
      allowedInvitations: false,
    }, event);

    expect(result.allowedInvitations).toBe(true);
  });

  it('should set `allowedInvitation` to `false`', () => {
    const event: UserSettingsEvent = {
      type: UserSettingsEventType.invitationsDisallowed,
      workspaceId,
      aggregateId,
      timestamp,
      payload: {
        changedBy,
      },
    };

    const result = userSettingsEventHandler({
      ...defaultAggregate,
      allowedInvitations: true,
    }, event);

    expect(result.allowedInvitations).toBe(false);
  });

  it('should set `allowedToInvite` to `maintainer`', () => {
    const role = UserRole.maintainer;
    const event: UserSettingsEvent = {
      type: UserSettingsEventType.allowedToInviteRoleChanged,
      workspaceId,
      aggregateId,
      timestamp,
      payload: {
        role,
        changedBy,
      },
    };

    const result = userSettingsEventHandler(defaultAggregate, event);

    expect(result.allowedInvitations).toBe(false);
    expect(result.allowedToInvite).toEqual(role);
  });

  it('should set `allowedToInvite` to `maintainer`', () => {
    const role = UserRole.maintainer;
    const event: UserSettingsEvent = {
      type: UserSettingsEventType.allowedToInviteRoleChanged,
      workspaceId,
      aggregateId,
      timestamp,
      payload: {
        role,
        changedBy,
      },
    };

    const result = userSettingsEventHandler(defaultAggregate, event);

    expect(result.allowedInvitations).toBe(false);
    expect(result.allowedToInvite).toEqual(role);
  });

  it('should set `allowedToInvite` to `owner`', () => {
    const role = UserRole.owner;
    const event: UserSettingsEvent = {
      type: UserSettingsEventType.allowedToInviteRoleChanged,
      workspaceId,
      aggregateId,
      timestamp,
      payload: {
        role,
        changedBy,
      },
    };

    const result = userSettingsEventHandler({
      ...defaultAggregate,
      allowedToInvite: UserRole.maintainer,
    }, event);

    expect(result.allowedInvitations).toBe(false);
    expect(result.allowedToInvite).toEqual(role);
  });
});
