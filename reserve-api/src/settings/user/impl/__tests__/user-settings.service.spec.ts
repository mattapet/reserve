/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserSettingsService } from '../user-settings.service';

import { UserSettingsCommandType } from '../../user-settings.command';
import { defaultAggregate } from '../../user-settings.aggregate';
import { UserSettingsRepository } from '../../user-settings.repository';
import { UserRoleService } from '../../../../user/user-role';
import { UserRoleServiceMock } from '../../../../user/user-role/__mocks__';
import { UserSettingsRepositoryMock } from '../../../user/__mocks__';
import { UserRole } from '../../.././../lib/user/user-role.type';
//#endregion

describe('user-settings.service', () => {
  let userRoleService!: UserRoleService;
  let userSettingsRepository!: UserSettingsRepository;
  let userSettingsService!: UserSettingsService;

  beforeEach(() => {
    userRoleService = new UserRoleServiceMock();
    userSettingsRepository = new UserSettingsRepositoryMock();
    userSettingsService =
      new UserSettingsService(userSettingsRepository, userRoleService);
  });

  describe('#dumpEvents()', () => {
    it('should call dump events from the repository', async () => {
      const workspaceId = 'test';
      const fn = jest.spyOn(userSettingsRepository, 'dumpEvents');

      await userSettingsService.dumpEvents(workspaceId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId);
    });
  });

  describe('#getByWorkspace()', () => {
    it('should call get by id from the repository', async () => {
      const workspaceId = 'test';
      const fn = jest.spyOn(userSettingsRepository, 'getById');

      await userSettingsService.getByWorkspace(workspaceId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId, workspaceId);
    });
  });

  describe('#toggleAllowRegistration()', () => {
    it('should execute `toggleAllowRegistration` command', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const isOwner = jest.spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(true));
      const getById = jest
        .spyOn(userSettingsRepository, 'getById')
        .mockImplementation(() => Promise.resolve(defaultAggregate));
      const execute = jest.spyOn(userSettingsRepository, 'execute');

      await userSettingsService.toggleAllowRegistration(workspaceId, changedBy);

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, workspaceId);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: UserSettingsCommandType.toggleAllowRegistrations,
        workspaceId,
        aggregateId: workspaceId,
        payload: {
          changedBy,
        },
      }, defaultAggregate);
    });

    it('should throw an error if user is not an owner', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const isOwner = jest.spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(false));
      const getById = jest.spyOn(userSettingsRepository, 'getById');
      const execute = jest.spyOn(userSettingsRepository, 'execute');

      await expect(
        userSettingsService.toggleAllowRegistration(workspaceId, changedBy),
      ).rejects
        .toEqual(new Error('Only Owners can change workspace settings.'));

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).not.toHaveBeenCalled();
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#toggleAllowInvitations()', () => {
    it('should execute `toggleAllowInvitations` command', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const isOwner = jest.spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(true));
      const getById = jest
        .spyOn(userSettingsRepository, 'getById')
        .mockImplementation(() => Promise.resolve(defaultAggregate));
      const execute = jest.spyOn(userSettingsRepository, 'execute');

      await userSettingsService.toggleAllowInvitations(workspaceId, changedBy);

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, workspaceId);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: UserSettingsCommandType.toggleAllowInvitations,
        workspaceId,
        aggregateId: workspaceId,
        payload: {
          changedBy,
        },
      }, defaultAggregate);
    });

    it('should throw an error if user is not an owner', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const isOwner = jest.spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(false));
      const getById = jest.spyOn(userSettingsRepository, 'getById');
      const execute = jest.spyOn(userSettingsRepository, 'execute');

      await expect(
        userSettingsService.toggleAllowInvitations(workspaceId, changedBy),
      ).rejects
        .toEqual(new Error('Only Owners can change workspace settings.'));

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).not.toHaveBeenCalled();
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#setAllowedToInviteRole()', () => {
    it('should execute `setAllowedToInviteRole` command', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const isOwner = jest.spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(true));
      const getById = jest
        .spyOn(userSettingsRepository, 'getById')
        .mockImplementation(() => Promise.resolve(defaultAggregate));
      const execute = jest.spyOn(userSettingsRepository, 'execute');

      await userSettingsService
        .setAllowedToInviteRole(workspaceId, UserRole.owner, changedBy);

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, workspaceId);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: UserSettingsCommandType.setAllowedToInviteRole,
        workspaceId,
        aggregateId: workspaceId,
        payload: {
          role: UserRole.owner,
          changedBy,
        },
      }, defaultAggregate);
    });

    it('should throw an error if user is not an owner', async () => {
      const workspaceId = 'test';
      const changedBy = '45';
      const isOwner = jest.spyOn(userRoleService, 'isOwner')
        .mockImplementation(() => Promise.resolve(false));
      const getById = jest.spyOn(userSettingsRepository, 'getById');
      const execute = jest.spyOn(userSettingsRepository, 'execute');

      await expect(
        userSettingsService
          .setAllowedToInviteRole(workspaceId, UserRole.owner, changedBy),
      ).rejects
        .toEqual(new Error('Only Owners can change workspace settings.'));

      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getById).not.toHaveBeenCalled();
      expect(execute).not.toHaveBeenCalled();
    });
  });
});
