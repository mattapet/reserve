/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { userSettingsCommandHandler } from '../user-settings.command-handler';
import {
  UserSettingsCommand,
  UserSettingsCommandType,
} from '../../user-settings.command';
import { UserSettingsEventType } from '../../user-settings.event';
import { defaultAggregate } from '../../user-settings.aggregate';
import { UserRole } from '../../../../lib/user/user-role.type';
//#endregion

describe('user-settings.command-handler', () => {
  const workspaceId = 'test';
  const aggregateId = workspaceId;
  const changedBy = '49';

  it('should produce `registrationsDisallowed`', () => {
    const command: UserSettingsCommand = {
      type: UserSettingsCommandType.toggleAllowRegistrations,
      workspaceId,
      aggregateId,
      payload: {
        changedBy,
      },
    };

    const [result] =
      userSettingsCommandHandler(defaultAggregate, command);

    expect(result.type).toEqual(UserSettingsEventType.registrationsDisallowed);
  });

  it('should produce `registrationsAllowed`', () => {
    const command: UserSettingsCommand = {
      type: UserSettingsCommandType.toggleAllowRegistrations,
      workspaceId,
      aggregateId,
      payload: {
        changedBy,
      },
    };

    const [result] =
      userSettingsCommandHandler({
        ...defaultAggregate,
        allowedRegistrations: false,
    }, command);

    expect(result.type).toEqual(UserSettingsEventType.registrationsAllowed);
  });

  it('should produce `invitationsAllowed`', () => {
    const command: UserSettingsCommand = {
      type: UserSettingsCommandType.toggleAllowInvitations,
      workspaceId,
      aggregateId,
      payload: {
        changedBy,
      },
    };

    const [result] =
      userSettingsCommandHandler(defaultAggregate, command);

    expect(result.type).toEqual(UserSettingsEventType.invitationsAllowed);
  });

  it('should produce `invitationsDisallowed`', () => {
    const command: UserSettingsCommand = {
      type: UserSettingsCommandType.toggleAllowInvitations,
      workspaceId,
      aggregateId,
      payload: {
        changedBy,
      },
    };

    const [result] =
      userSettingsCommandHandler({
        ...defaultAggregate,
        allowedInvitations: true,
    }, command);

    expect(result.type).toEqual(UserSettingsEventType.invitationsDisallowed);
  });

  it('should produce `setAllowedToInviteRoleChanged`', () => {
    const role = UserRole.maintainer;
    const command: UserSettingsCommand = {
      type: UserSettingsCommandType.setAllowedToInviteRole,
      workspaceId,
      aggregateId,
      payload: {
        role,
        changedBy,
      },
    };

    const [result] =
      userSettingsCommandHandler({
        ...defaultAggregate,
        allowedInvitations: true,
    }, command);

    expect(result.type)
      .toEqual(UserSettingsEventType.allowedToInviteRoleChanged);
    expect(result.payload).toEqual({ role, changedBy });
  });

  it('should produce `setAllowedToInviteRoleChanged`', () => {
    const role = UserRole.user;
    const command: UserSettingsCommand = {
      type: UserSettingsCommandType.setAllowedToInviteRole,
      workspaceId,
      aggregateId,
      payload: {
        role,
        changedBy,
      },
    };

    const [result] =
      userSettingsCommandHandler({
        ...defaultAggregate,
        allowedInvitations: true,
    }, command);

    expect(result.type)
      .toEqual(UserSettingsEventType.allowedToInviteRoleChanged);
    expect(result.payload).toEqual({ role, changedBy });
  });

  it('should return no events if no change made', () => {
    const role = UserRole.owner;
    const command: UserSettingsCommand = {
      type: UserSettingsCommandType.setAllowedToInviteRole,
      workspaceId,
      aggregateId,
      payload: {
        role,
        changedBy,
      },
    };

    const results = userSettingsCommandHandler({
      ...defaultAggregate,
      allowedInvitations: true,
    }, command);

    expect(results).toEqual([]);
  });
});
