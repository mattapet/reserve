/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserSettingsAggregate } from '../user-settings.aggregate';
import {
  UserSettingsEvent,
  UserSettingsEventType,
} from '../user-settings.event';
//#endregion

export function userSettingsEventHandler(
  aggregate: UserSettingsAggregate,
  event: UserSettingsEvent,
): UserSettingsAggregate {
  switch (event.type) {
  case UserSettingsEventType.registrationsAllowed:
    return {
      ...aggregate,
      allowedRegistrations: true,
    };

  case UserSettingsEventType.registrationsDisallowed:
    return {
      ...aggregate,
      allowedRegistrations: false,
    };

  case UserSettingsEventType.invitationsAllowed:
    return {
      ...aggregate,
      allowedInvitations: true,
    };

  case UserSettingsEventType.invitationsDisallowed:
    return {
      ...aggregate,
      allowedInvitations: false,
    };

  case UserSettingsEventType.allowedToInviteRoleChanged:
    return {
      ...aggregate,
      allowedToInvite: event.payload.role,
    };
  }
}
