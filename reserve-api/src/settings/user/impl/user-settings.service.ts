/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserSettingsEvent } from '../user-settings.event';
import { UserSettingsAggregate } from '../user-settings.aggregate';
import { UserSettingsCommandType } from '../user-settings.command';
import { UserSettingsService as Interface } from '../user-settings.service';
import { UserSettingsRepository } from '../user-settings.repository';
import { UserRoleService } from '../../../user/user-role';
import { UserRole } from '../../../lib/user/user-role.type';
//#endregion

export class UserSettingsService implements Interface {
  public constructor(
    private readonly repository: UserSettingsRepository,
    private readonly userRole: UserRoleService,
  ) { }

  // - MARK: Queries

  public async dumpEvents(workspaceId: string): Promise<UserSettingsEvent[]> {
    return await this.repository.dumpEvents(workspaceId);
  }

  public async getByWorkspace(
    workspaceId: string,
  ): Promise<UserSettingsAggregate> {
    return this.repository.getById(workspaceId, workspaceId);
  }

  // - MARK: Command

  public async toggleAllowRegistration(
    workspaceId: string,
    changedBy: string,
  ): Promise<UserSettingsAggregate> {
    if (!await this.userRole.isOwner(workspaceId, changedBy)) {
      throw new Error('Only Owners can change workspace settings.');
    }

    const aggregate = await this.getByWorkspace(workspaceId);

    return await this.repository.execute({
      type: UserSettingsCommandType.toggleAllowRegistrations,
      workspaceId,
      aggregateId: workspaceId,
      payload: {
        changedBy,
      },
    }, aggregate);
  }

  public async toggleAllowInvitations(
    workspaceId: string,
    changedBy: string,
  ): Promise<UserSettingsAggregate> {
    if (!await this.userRole.isOwner(workspaceId, changedBy)) {
      throw new Error('Only Owners can change workspace settings.');
    }

    const aggregate = await this.getByWorkspace(workspaceId);

    return await this.repository.execute({
      type: UserSettingsCommandType.toggleAllowInvitations,
      workspaceId,
      aggregateId: workspaceId,
      payload: {
        changedBy,
      },
    }, aggregate);
  }

  public async setAllowedToInviteRole(
    workspaceId: string,
    role: UserRole,
    changedBy: string,
  ): Promise<UserSettingsAggregate> {
    if (!await this.userRole.isOwner(workspaceId, changedBy)) {
      throw new Error('Only Owners can change workspace settings.');
    }

    const aggregate = await this.getByWorkspace(workspaceId);

    return await this.repository.execute({
      type: UserSettingsCommandType.setAllowedToInviteRole,
      workspaceId,
      aggregateId: workspaceId,
      payload: {
        role,
        changedBy,
      },
    }, aggregate);
  }
}
