/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '../../lib/events/command';
import { UserRole } from 'lib/user/user-role.type';
//#endregion

export enum UserSettingsCommandType {
  toggleAllowRegistrations = 'toggle_allow_registrartions',
  toggleAllowInvitations = 'toggle_allow_invitations',
  setAllowedToInviteRole = 'set_allowed_to_invite_role',
}

interface ToggleAllowRegistrations extends Command {
  readonly type: UserSettingsCommandType.toggleAllowRegistrations;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface ToggleAllowInvitations extends Command {
  readonly type: UserSettingsCommandType.toggleAllowInvitations;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface SetAllowedToInviteRole extends Command {
  readonly type: UserSettingsCommandType.setAllowedToInviteRole;
  readonly payload: {
    readonly role: UserRole;
    readonly changedBy: string;
  };
}

export type UserSettingsCommand =
  | ToggleAllowRegistrations
  | ToggleAllowInvitations
  | SetAllowedToInviteRole;
