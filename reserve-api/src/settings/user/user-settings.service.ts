/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserSettingsEvent } from './user-settings.event';
import { UserSettingsAggregate } from './user-settings.aggregate';
import { UserRole } from '../../lib/user/user-role.type';
//#endregion

export interface UserSettingsService {
  // - MARK: Queries

  dumpEvents(workspaceId: string): Promise<UserSettingsEvent[]>;

  getByWorkspace(workspaceId: string): Promise<UserSettingsAggregate>;

  // - MARK: Command

  toggleAllowRegistration(
    workspaceId: string,
    changedBy: string,
  ): Promise<UserSettingsAggregate>;

  toggleAllowInvitations(
    workspaceId: string,
    changedBy: string,
  ): Promise<UserSettingsAggregate>;

  setAllowedToInviteRole(
    workspaceId: string,
    role: UserRole,
    changedBy: string,
  ): Promise<UserSettingsAggregate>;
}
