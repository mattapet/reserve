/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';

import { UserSettingsEventRepository } from './user-settings-event.repository';
import { UserSettingsService as Impl } from '../impl/user-settings.service';
import { userSettingsEventHandler } from '../impl/user-settings.event-handler';
import {
  userSettingsCommandHandler,
} from '../impl/user-settings.command-handler';
import { defaultAggregate } from '../user-settings.aggregate';
import { NestUserRoleService } from '../../../user/user-role/nest';
import { BaseRepository } from '../../../lib/events/repository-base';
//#endregion

@Injectable()
export class UserSettingsService extends Impl {
  public constructor(
    @InjectConnection()
    connection: Connection,
    userRole: NestUserRoleService,
  ) {
    super(
      new BaseRepository(
        connection.getCustomRepository(UserSettingsEventRepository),
        userSettingsCommandHandler,
        userSettingsEventHandler,
        defaultAggregate,
      ),
      userRole,
    );
  }
}
