/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity } from 'typeorm';

import { BaseEvent } from '../../../lib/events/base-event.entity';
import { UserSettingsEvent as Payload } from '../user-settings.event';
//#endregion

@Entity({ name: 'user_settings_event' })
export class UserSettingsEventEntity extends BaseEvent<Payload> { }
