/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository, Repository } from 'typeorm';

import { UserSettingsEventEntity } from './user-settings-event.entity';
import { BaseEventRepository } from '../../../lib/events/repository-base';
import { UserSettingsEvent } from '../user-settings.event';
//#endregion

@EntityRepository(UserSettingsEventEntity)
export class UserSettingsEventRepository
  extends Repository<UserSettingsEventEntity>
  implements BaseEventRepository<UserSettingsEvent>
{
  public async findByRowId(rowId: string): Promise<UserSettingsEvent[]> {
    const events = await this.find({
      where: { rowId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async findByRowIdAndAggregateId(
    rowId: string,
    aggregateId: string,
  ): Promise<UserSettingsEvent[]> {
    const events = await this.find({
      where: { rowId, aggregateId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async saveEvents(
    ...events: UserSettingsEvent[]
  ): Promise<UserSettingsEvent[]> {
    const results = await this.save(events.map(this.wrap));
    return results.map(this.unwrap);
  }

  public wrap(event: UserSettingsEvent): UserSettingsEventEntity {
    const { workspaceId, aggregateId, timestamp } = event;
    return new UserSettingsEventEntity(
      workspaceId,
      aggregateId,
      timestamp,
      event,
    );
  }

  public unwrap(entity: UserSettingsEventEntity): UserSettingsEvent {
    return entity.payload;
  }
}
