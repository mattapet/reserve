/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserSettingsCommand } from './user-settings.command';
import { UserSettingsEvent } from './user-settings.event';
import { UserSettingsAggregate } from './user-settings.aggregate';
import { Repository } from '../../lib/events/repository-base.interface';
//#endregion

export interface UserSettingsRepository extends Repository<
  UserSettingsCommand,
  UserSettingsEvent,
  UserSettingsAggregate
> { }
