/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '../../lib/events/event';
import { UserRole } from '../../lib/user/user-role.type';
//#endregion

export enum UserSettingsEventType {
  registrationsAllowed = 'registrations_allowed',
  registrationsDisallowed = 'registrations_disallowed',
  invitationsAllowed = 'invitations_allowed',
  invitationsDisallowed = 'invitations_disallowed',
  allowedToInviteRoleChanged = 'allowed_to_invite_role_changed',
}

interface RegistrationsAllowed extends Event {
  readonly type: UserSettingsEventType.registrationsAllowed;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface RegistrationsDisallowed extends Event {
  readonly type: UserSettingsEventType.registrationsDisallowed;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface InvitationsAllowed extends Event {
  readonly type: UserSettingsEventType.invitationsAllowed;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface InvitationsDisallowed extends Event {
  readonly type: UserSettingsEventType.invitationsDisallowed;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface AllowedToInviteRoleChanged extends Event {
  readonly type: UserSettingsEventType.allowedToInviteRoleChanged;
  readonly payload: {
    readonly role: UserRole;
    readonly changedBy: string;
  };
}

export type UserSettingsEvent =
   | RegistrationsAllowed
   | RegistrationsDisallowed
   | InvitationsAllowed
   | InvitationsDisallowed
   | AllowedToInviteRoleChanged;
