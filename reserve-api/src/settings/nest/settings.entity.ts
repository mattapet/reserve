/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationSettingsAggregate } from '../reservation';
import { UserSettingsAggregate } from '../user';
//#endregion

export interface Settings {
  readonly reservations: ReservationSettingsAggregate;
  readonly users: UserSettingsAggregate;
}
