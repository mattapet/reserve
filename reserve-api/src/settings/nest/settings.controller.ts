/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Get,
  UseGuards,
  UseInterceptors,
  Put,
  Body,
  Param,
  Req,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { Settings } from './settings.entity';
import { ReservationSettingsDTO } from './dto/reservation-settings.dto';
import {
  ReservationTransformerInterceptor,
} from './interceptors/reservation-transformer.interceptor';
import { ReservationSettingsService } from '../reservation/nest';
import { UserSettingsService } from '../user/nest';
import { TransformerInterceptor } from './interceptors/transformer.interceptor';
import {
  AuthorizedRequest,
} from '../../lib/interface/authorized-request.interface';
import { UserSettingsDTO } from './dto/user-settings.dto';
import { ReservationSettingsAggregate } from '../../settings/reservation';
import { UserSettingsAggregate } from '../../settings/user';
//#endregion

@Controller('api/v1/workspace')
export class SettingsController {
  public constructor(
    private readonly reservation: ReservationSettingsService,
    private readonly user: UserSettingsService,
  ) { }

  @Get(':workspace_id/settings')
  @UseInterceptors(new TransformerInterceptor())
  public async getWorkspaceSettings(
    @Param('workspace_id') workspaceId: string,
  ): Promise<Settings> {
    return {
      reservations: await this.reservation.getByWorkspace(workspaceId),
      users: await this.user.getByWorkspace(workspaceId),
    };
  }

  @Put(':workspace_id/settings/reservation')
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(new ReservationTransformerInterceptor())
  public async updateReservationSettings(
    @Req() req: AuthorizedRequest,
    @Param('workspace_id') workspaceId: string,
    @Body() {
      time_unit: timeUnit,
      max_reservations: maxReservations,
      max_range: maxRange,
      max_offset: maxOffset,
    }: ReservationSettingsDTO,
  ): Promise<ReservationSettingsAggregate> {
    const userId = req.user.id;
    const settings = await this.reservation.getByWorkspace(workspaceId);

    if (settings.maxReservations !== maxReservations) {
      await this.reservation
        .setMaxReservations(workspaceId, maxReservations, userId);
    }
    if (settings.maxRange !== maxRange) {
      await this.reservation.setMaxRange(workspaceId, maxRange, userId);
    }
    if (settings.maxOffset !== maxOffset) {
      await this.reservation.setMaxOffset(workspaceId, maxOffset, userId);
    }
    if (settings.timeUnit !== timeUnit) {
      await this.reservation.setTimeUnit(workspaceId, timeUnit, userId);
    }
    return await this.reservation.getByWorkspace(workspaceId);
  }

  @Put(':workspace_id/settings/user')
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(new ReservationTransformerInterceptor())
  public async updateUserSettings(
    @Req() req: AuthorizedRequest,
    @Param('workspace_id') workspaceId: string,
    @Body() {
      allowed_invitations: allowedInvitations,
      allowed_registrations: allowedRegistrations,
      allowed_to_invite: allowedToInvite,
    }: UserSettingsDTO,
  ): Promise<UserSettingsAggregate> {
    const userId = req.user.id;
    const settings = await this.user.getByWorkspace(workspaceId);
    if (settings.allowedInvitations !== allowedInvitations) {
      await this.user.toggleAllowInvitations(workspaceId, userId);
    }
    if (settings.allowedRegistrations !== allowedRegistrations) {
      await this.user.toggleAllowRegistration(workspaceId, userId);
    }
    if (settings.allowedToInvite !== allowedToInvite) {
      await this.user
        .setAllowedToInviteRole(workspaceId, allowedToInvite, userId);
    }
    return await this.user.getByWorkspace(workspaceId);
  }
}
