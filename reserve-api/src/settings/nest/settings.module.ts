/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// #region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserModule } from '../../user/nest';
import {
  ReservationSettingsEventEntity,
  ReservationSettingsEventRepository,
  ReservationSettingsService,
} from '../reservation/nest';
import {
  UserSettingsEventEntity,
  UserSettingsEventRepository,
  UserSettingsService,
} from '../user/nest';
import { SettingsController } from './settings.controller';
// #endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ReservationSettingsEventEntity,
      ReservationSettingsEventRepository,
      UserSettingsEventEntity,
      UserSettingsEventRepository,
    ]),
    UserModule,
  ],
  controllers: [SettingsController],
  providers: [ReservationSettingsService, UserSettingsService],
  exports: [ReservationSettingsService, UserSettingsService],
})
export class SettingsModule { }
