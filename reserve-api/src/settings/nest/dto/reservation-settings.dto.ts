/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IsIn, IsNumber, IsOptional, ValidateNested } from 'class-validator';

import { TimeUnit } from '../../../lib/settings/time-unit';
//#endregion

class Timeframe {
  @IsIn([TimeUnit.day, TimeUnit.hour, TimeUnit.minute])
  public amount!: number;
  @IsNumber()
  public units!: TimeUnit;
}

export class ReservationSettingsDTO {
  @IsIn([TimeUnit.day, TimeUnit.hour, TimeUnit.minute])
  public time_unit!: TimeUnit;

  @IsNumber()
  @IsOptional()
  public max_reservations!: number | null;

  @IsOptional()
  @ValidateNested()
  public max_range!: Timeframe | null;

  @IsOptional()
  @ValidateNested()
  public max_offset!: Timeframe | null;
}
