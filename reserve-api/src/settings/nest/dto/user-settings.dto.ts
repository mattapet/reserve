/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IsBoolean, IsIn } from 'class-validator';
import { UserRole } from '../../../lib/user/user-role.type';
//#endregion

export class UserSettingsDTO {
  @IsBoolean()
  public allowed_registrations!: boolean;

  @IsBoolean()
  public allowed_invitations!: boolean;

  @IsIn([UserRole.owner, UserRole.maintainer, UserRole.user])
  public allowed_to_invite!: UserRole;
}
