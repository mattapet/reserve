/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ValidateNested } from 'class-validator';

import { ReservationSettingsDTO } from './reservation-settings.dto';
import { UserSettingsDTO } from './user-settings.dto';
//#endregion

export class SettingsDTO {
  @ValidateNested()
  public reservations!: ReservationSettingsDTO;
  @ValidateNested()
  public users!: UserSettingsDTO;
}
