/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SettingsDTO } from '../dto/settings.dto';
import { Settings } from '../settings.entity';
import { ReservationSettingsDTO } from '../dto/reservation-settings.dto';
import { UserSettingsDTO } from '../dto/user-settings.dto';
//#endregion

@Injectable()
export class TransformerInterceptor implements NestInterceptor<
  Settings,
  SettingsDTO
> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<Settings>,
  ): Observable<SettingsDTO> {
    return call$.handle().pipe(
      map((payload: Settings) => this.encode(payload),
      ),
    );
  }

  private encode = (settings: Settings): SettingsDTO => ({
    reservations: {
      time_unit: settings.reservations.timeUnit,
      max_reservations: settings.reservations.maxReservations,
      max_range: settings.reservations.maxRange,
      max_offset: settings.reservations.maxOffset,
    } as ReservationSettingsDTO,
    users: {
      allowed_invitations: settings.users.allowedInvitations,
      allowed_registrations: settings.users.allowedRegistrations,
      allowed_to_invite: settings.users.allowedToInvite,
    } as UserSettingsDTO,
  })
}
