/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserSettingsDTO } from '../dto/user-settings.dto';
import { UserSettingsAggregate } from '../../user';
//#endregion

@Injectable()
export class ReservationTransformerInterceptor implements NestInterceptor<
  UserSettingsAggregate,
  UserSettingsDTO
> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<UserSettingsAggregate>,
  ): Observable<UserSettingsDTO> {
    return call$.handle().pipe(
      map((payload: UserSettingsAggregate) => this.encode(payload)),
    );
  }

  private encode = (settings: UserSettingsAggregate): UserSettingsDTO => ({
    allowed_invitations: settings.allowedInvitations,
    allowed_registrations: settings.allowedRegistrations,
    allowed_to_invite: settings.allowedToInvite,
  })
}
