/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  ReservationSettingsAggregate,
} from '../../reservation';
import { ReservationSettingsDTO } from '../dto/reservation-settings.dto';
//#endregion

@Injectable()
export class ReservationTransformerInterceptor implements NestInterceptor<
  ReservationSettingsAggregate,
  ReservationSettingsDTO
> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<ReservationSettingsAggregate>,
  ): Observable<ReservationSettingsDTO> {
    return call$.handle().pipe(
      map((payload: ReservationSettingsAggregate) => this.encode(payload)),
    );
  }

  private encode = (
    settings: ReservationSettingsAggregate,
  ): ReservationSettingsDTO => ({
    time_unit: settings.timeUnit,
    max_reservations: settings.maxReservations,
    max_range: settings.maxRange,
    max_offset: settings.maxOffset,
  })
}
