/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { SetupController } from './setup.controller';
import { IdentityModule } from '../../identity/nest';
import { VerificationModule } from '../../verfication/nest';
import { AuthModule } from '../../auth';
import { WorkspaceModule } from '../../workspace/nest';
import { UserModule } from '../../user/nest';
import { OAuth2Module } from '../../oauth2/oauth2.module';
//#endregion

@Module({
  imports: [
    IdentityModule,
    VerificationModule,
    AuthModule,
    WorkspaceModule,
    UserModule,
    OAuth2Module,
  ],
  controllers: [SetupController],
})
export class SetupModule { }
