/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import uuid from 'uuid/v4';
import {
  Controller,
  Post,
  Body,
  BadRequestException,
  ClassSerializerInterceptor,
  UseInterceptors,
} from '@nestjs/common';

import { NestVerificationService } from '../../verfication/nest';
import { WorkspaceService, Workspace } from '../../workspace/nest';
import { NestIdentityService } from '../../identity/nest';
import { AuthService } from '../../auth';
import { NestUserRoleService } from '../../user/user-role/nest';
import { ClientService } from '../../oauth2/services/client.service';
//#endregion

@Controller('api/v1')
export class SetupController {
  public constructor(
    private readonly verificationService: NestVerificationService,
    private readonly identityService: NestIdentityService,
    private readonly authService: AuthService,
    private readonly workspaceService: WorkspaceService,
    private readonly userRoleService: NestUserRoleService,
    private readonly clientService: ClientService,
  ) { }

  @Post('workspace/verify')
  public async verifyOwner(
    @Body('email') email: string,
    @Body('redirect_uri') redirectUri: string,
  ): Promise<void> {
    await this.verificationService.verify(redirectUri, uuid(), email);
  }

  @Post('workspace/complete')
  @UseInterceptors(ClassSerializerInterceptor )
  public async completeSetup(
    @Body('workspace_id') workspaceId: string,
    @Body('email') email: string,
    @Body('expiry') expiry: string,
    @Body('signature') signature: string,
    @Body('workspace') name: string,
    @Body('password') password: string,
  ): Promise<Workspace> {
    const valid = await this.verificationService
      .confirm(workspaceId, email, parseInt(expiry, 10), signature);
    if (!valid) throw new BadRequestException('Corrupted token.');

    await this.clientService.register(workspaceId);

    const owner = await this.identityService.create(workspaceId, email);
    await this.userRoleService.createOwner(workspaceId, owner.userId);
    await this.authService.setPassword(workspaceId, owner.userId, password);
    return await this.workspaceService.create(workspaceId, name);
  }
}
