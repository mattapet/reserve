/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import config from 'config';
import nodemailer from 'nodemailer';
import { EmailServiceImpl } from '../impl/email.service';
//#endregion

export class NestEmailService extends EmailServiceImpl {
  public constructor() {
    super(
      nodemailer.createTransport({
        host: config.get('email.host'),
        port: config.get('email.port'),
        auth: {
          user: config.get('email.username'),
          pass: config.get('email.password'),
        },
      }),
      config.get('email.sender'),
    );
  }
}
