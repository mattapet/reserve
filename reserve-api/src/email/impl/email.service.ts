/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import nodemailer, { Transporter } from 'nodemailer';
import { EmailService } from '../email.service';
//#endregion

export class EmailServiceImpl implements EmailService {
  public constructor(
    private readonly transporter: Transporter,
    private readonly sender: string,
  ) { }

  public async sendPlainText(
    content: string,
    to: string,
    subject: string,
    cc?: string,
  ): Promise<void> {
    const info = await this.transporter.sendMail({
      text: content,
      from: this.sender,
      to,
      subject,
      cc,
    });

    if (process.env.NODE_ENV !== 'production') {
      // tslint:disable:no-console
      console.info(nodemailer.getTestMessageUrl(info));
    }
  }
}
