/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module, forwardRef } from '@nestjs/common';

import { WorkspaceModule } from '../workspace/nest';
import { AuthorityController } from './authority.controller';
import { AuthorityService } from './authroity.service';
import { Authority } from './authority.entity';

//#endregions

@Module({
  imports: [
    TypeOrmModule.forFeature([Authority]),
    forwardRef(() => WorkspaceModule),
  ],
  controllers: [AuthorityController],
  providers: [AuthorityService],
  exports: [AuthorityService],
})
export class AuthorityModule { }
