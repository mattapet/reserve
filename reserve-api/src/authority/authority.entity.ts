/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Entity,
  Column,
  PrimaryColumn,
  Unique,
} from 'typeorm';

import { ProfileMap } from './profile-map';
//#endregion

@Entity({ name: 'authority' })
@Unique([ 'clientId', 'clientSecret', 'workspaceId' ])
export class Authority {
  @PrimaryColumn({ name: 'workspace_id' })
  public workspaceId: string;

  @PrimaryColumn()
  public name: string;

  @Column()
  public description: string;

  @Column({ name: 'authorize_url' })
  public authorizeUrl: string;

  @Column({ name: 'token_url' })
  public tokenUrl: string;

  @Column({ name: 'profile_urls', type: 'simple-array' })
  public profileUrls: string[];

  @Column({ name: 'profile_map', type: 'simple-json' })
  public profileMap: ProfileMap;

  @Column({ name: 'redirect_uri' })
  public redirectUri: string;

  @Column({ name: 'client_id'})
  public clientId: string;

  @Column({ name: 'client_secret' })
  public clientSecret: string;

  @Column({ type: 'simple-array' })
  public scope: string[];

  public constructor(
    name: string,
    description: string,
    authorizeUrl: string,
    tokenUrl: string,
    profileUrls: string[],
    profileMap: ProfileMap,
    redirectUri: string,
    clientId: string,
    clientSecret: string,
    scope: string[],
    workspaceId: string,
  ) {
    this.name = name;
    this.description = description;
    this.authorizeUrl = authorizeUrl;
    this.tokenUrl = tokenUrl;
    this.profileUrls = profileUrls;
    this.profileMap = profileMap;
    this.redirectUri = redirectUri;
    this.clientId = clientId;
    this.clientSecret = clientSecret;
    this.scope = scope;
    this.workspaceId = workspaceId;
  }
}
