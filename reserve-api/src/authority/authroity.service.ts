/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import qs from 'qs';
import fetch from 'node-fetch';
import {
  Injectable, NotFoundException, ForbiddenException,
} from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';

import { Authority } from './authority.entity';
import { GrantType } from '../lib/oauth2/grant-type.type';
//#endregion

@Injectable()
export class AuthorityService {
  public constructor(
    @InjectEntityManager()
    private readonly manager: EntityManager,
  ) { }

  public async getByWorkspace(
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<Authority[]> {
    return await trx.find(Authority, { workspaceId });
  }

  public async getByName(
    name: string,
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<Authority> {
    const item = await trx.findOne(Authority, { workspaceId, name });
    if (!item) {
      throw new NotFoundException();
    }
    return item;
  }

  public async register(
    authority: Authority,
    trx: EntityManager = this.manager,
  ): Promise<Authority> {
    return await trx.save(authority);
  }

  public async update(
    authority: Authority,
    trx: EntityManager = this.manager,
  ): Promise<Authority> {
    await this.getByName(authority.name, authority.workspaceId, trx);
    return await trx.save(authority);
  }

  public async removeByName(
    name: string,
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<void> {
    const item = await this.getByName(name, workspaceId, trx);
    await trx.remove(item);
  }

  public async excahngeCode(
    code: string,
    authority: Authority,
  ): Promise<string> {
    const body = {
      client_id: authority.clientId,
      client_secret: authority.clientSecret,
      grant_type: GrantType.authorizationCode,
      redirect_uri: authority.redirectUri,
      code,
    };
    const response = await fetch(`${authority.tokenUrl}`, {
      method: 'POST',
      body: qs.stringify(body),
      headers: {
        ['Content-Type']: 'application/x-www-form-urlencoded;charset=UTF-8',
        ['Accept']: 'application/json',
      },
    });
    // console.log(response.status, response.statusText);
    const data = await response.json();
    // console.log(data);
    const { access_token: accessToken } = data;
    return accessToken;
  }

  public async fetchProfile(
    authority: Authority,
    accessToken: string,
  ) {
    const fetchProfile = async (url: string) => {
      const response = await fetch(url, {
        headers: {
          ['Authorization']: `Bearer ${accessToken}`,
          ['Accept']: 'application/json',
        },
      });
      // tslint:disable
      const data = await response.json();
      const firstName = authority.profileMap.firstName
        .split('.').reduce((obj, key) => obj && obj[key], data);
      const lastName = authority.profileMap.lastName
        .split('.').reduce((obj, key) => obj && obj[key], data);
      const email = authority.profileMap.email
        .split('.').reduce((obj, key) => obj && obj[key], data);
      const phone = authority.profileMap.phone && authority.profileMap.phone
        .split('.').reduce((obj, key) => obj && obj[key], data);
      let results = { firstName, lastName, email, phone };
      for (const key in results) {
        if (!results[key]) {
          delete results[key];
        }
      }

      return results;
    };
    const results = await Promise.all(authority.profileUrls.map(fetchProfile));
    const { email, firstName, lastName, phone } =
      results.reduce((result, next) =>
        ({ ...result, ...next }), {} as any);
    if (!email || !firstName || !lastName) {
      throw new ForbiddenException('Insufficient user information.');
    }
    return { email, firstName, lastName, phone };
  }

  public async transaction<T>(
    runInTrx: (em: EntityManager) => Promise<T>,
  ): Promise<T> {
    return await this.manager.transaction(runInTrx);
  }
}
