/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Authority } from '../authority.entity';
import { AuthorityDTO } from '../dto/authority.dto';
//#endregion

@Injectable()
export class TransformerInterceptor implements NestInterceptor<
  Authority | Authority[],
  AuthorityDTO | AuthorityDTO[]
> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<Authority | Authority[]>,
  ): Observable<AuthorityDTO | AuthorityDTO[]> {
    return call$.handle().pipe(
      map((payload: Authority | Authority[]) => Array.isArray(payload) ?
        payload.map(this.encode) : this.encode(payload),
      ),
    );
  }

  private encode = (authority: Authority) => ({
    name: authority.name,
    description: authority.description,
    authorize_url: authority.authorizeUrl,
    token_url: authority.tokenUrl,
    profile_urls: authority.profileUrls,
    profile_map: {
      first_name: authority.profileMap.firstName,
      last_name: authority.profileMap.lastName,
      email: authority.profileMap.email,
      phone: authority.profileMap.phone,
    },
    redirect_uri: authority.redirectUri,
    client_id: authority.clientId,
    client_secret: authority.clientSecret,
    scope: authority.scope,
  })
}
