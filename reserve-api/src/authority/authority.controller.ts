/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Post,
  Body,
  Req,
  UseGuards,
  UseInterceptors,
  Get,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { RoleGuard } from '../lib/guards/RoleGuard';
import { UserRole } from '../lib/user/user-role.type';
import {
  AuthorizedRequest,
} from '../lib/interface/authorized-request.interface';
import { WorkspaceService } from '../workspace/nest/workspace.service';
import { AuthorityService } from './authroity.service';
import { Authority } from './authority.entity';
import { TransformerInterceptor } from './interceptors/transformer.interceptor';
import { ProfileMapDTO } from './dto/profile-map.dto';
//#endregion

@Controller('api/v1/authority')
export class AuthorityController {
  public constructor(
    private readonly service: AuthorityService,
    private readonly workspace: WorkspaceService,
  ) { }

  @Get()
  @UseGuards(AuthGuard('jwt'), RoleGuard(UserRole.owner))
  @UseInterceptors(new TransformerInterceptor())
  public async getAllAuthorities(
    @Req() req: AuthorizedRequest,
  ): Promise<Authority[]> {
    const workspaceId = req.user.workspace;
    return await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      return await this.service.getByWorkspace(workspace.id, trx);
    });
  }

  @Post()
  @UseGuards(AuthGuard('jwt'), RoleGuard(UserRole.owner))
  @UseInterceptors(new TransformerInterceptor())
  public async register(
    @Req() req: AuthorizedRequest,
    @Body('name') name: string,
    @Body('description') description: string,
    @Body('authorize_url') authorizeUrl: string,
    @Body('token_url') tokenUrl: string,
    @Body('profile_urls') profileUrls: string[],
    @Body('profile_map') profile_map: ProfileMapDTO,
    @Body('redirect_uri') redirectUri: string,
    @Body('client_id') clientId: string,
    @Body('client_secret') clientSecret: string,
    @Body('scope') scope: string[],
  ): Promise<Authority> {
    const workspaceId = req.user.workspace;
    const profileMap = {
      firstName: profile_map.first_name,
      lastName: profile_map.last_name,
      email: profile_map.email,
      phone: profile_map.phone,
    };
    return this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      const authority = new Authority(
        name,
        description,
        authorizeUrl,
        tokenUrl,
        profileUrls,
        profileMap,
        redirectUri,
        clientId,
        clientSecret,
        scope,
        workspace.id,
      );
      return await this.service.register(authority, trx);
    });
  }

  @Put(':name')
  @UseGuards(AuthGuard('jwt'), RoleGuard(UserRole.owner))
  @UseInterceptors(new TransformerInterceptor())
  public async updateByName(
    @Req() req: AuthorizedRequest,
    @Param('name') name: string,
    @Body('description') description: string,
    @Body('authorize_url') authorizeUrl: string,
    @Body('token_url') tokenUrl: string,
    @Body('profile_urls') profileUrls: string[],
    @Body('profile_map') profile_map: ProfileMapDTO,
    @Body('redirect_uri') redirectUri: string,
    @Body('client_id') clientId: string,
    @Body('client_secret') clientSecret: string,
    @Body('scope') scope: string[],
  ): Promise<Authority> {
    const workspaceId = req.user.workspace;
    const profileMap = {
      firstName: profile_map.first_name,
      lastName: profile_map.last_name,
      email: profile_map.email,
      phone: profile_map.phone,
    };
    return this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      const authority = new Authority(
        name,
        description,
        authorizeUrl,
        tokenUrl,
        profileUrls,
        profileMap,
        redirectUri,
        clientId,
        clientSecret,
        scope,
        workspace.id,
      );
      return await this.service.update(authority, trx);
    });
  }

  @Delete(':name')
  @UseGuards(AuthGuard('jwt'), RoleGuard(UserRole.owner))
  public async removeByName(
    @Req() req: AuthorizedRequest,
    @Param('name') name: string,
  ): Promise<void> {
    const workspaceId = req.user.workspace;
    await this.service.transaction(async (trx) => {
      const workspace = await this.workspace.getById(workspaceId, trx);
      await this.service.removeByName(name, workspace.id, trx);
    });
  }
}
