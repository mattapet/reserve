/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ProfileMapDTO } from './profile-map.dto';
//#endregion

export class AuthorityDTO {
  public name!: string;
  public description!: string;
  public authorize_url!: string;
  public token_url!: string;
  public profile_urls!: string[];
  public profile_map!: ProfileMapDTO;
  public redirect_uri!: string;
  public client_id!: string;
  public client_secret!: string;
  public scope!: string[];
}
