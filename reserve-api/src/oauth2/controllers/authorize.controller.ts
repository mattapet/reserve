/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region import
import {
  Controller,
  UseGuards,
  Post,
  Req,
  Body,
  Res,
  HttpCode,
  HttpStatus,
  BadRequestException,
  Get,
  Query,
  UsePipes,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import qs from 'qs';

import {
  AuthorizeResponseCode,
  AuthorizeResponse,
} from '../dto/authorize-response';
import { TokenResponse } from '../dto/token-response.dto';
import { AuthorizeRequest } from '../dto/authorize-request.dto';
import { AuhtorizeService } from '../services/authorize.service';
import { TokenService } from '../services/token.service';
import { ClientService } from '../services/client.service';
import { Client } from '../entities/client.entity';
import { ScopeGuard } from '../../lib/guards/ScopeGuard';
import { Scope } from '../../lib/oauth2/scope.type';
import {
  AuthorizedRequest,
} from '../../lib/interface/authorized-request.interface';
import { ResponseType } from '../../lib/oauth2/response-type.type';
import { AuthorizeValidation } from './validations/authorize.validation';
import {
  WorkspaceService,
  Workspace,
} from '../../workspace/nest';
import { Identity } from '../../identity';
import { NestIdentityService } from '../../identity/nest';
//#endregion

@Controller('api/v1/oauth2/authorize')
@UseGuards(AuthGuard('jwt'), ScopeGuard(Scope.authorization))
export class AuthorizeController {
  public constructor(
    private readonly service: AuhtorizeService,
    private readonly token: TokenService,
    private readonly client: ClientService,
    private readonly workspace: WorkspaceService,
    private readonly identityService: NestIdentityService,
  ) { }

  @Get()
  @UsePipes(new AuthorizeValidation())
  @HttpCode(HttpStatus.PERMANENT_REDIRECT)
  public async authorizeGet(
    @Req() req: AuthorizedRequest,
    @Res() res: Response,
    @Query() data: AuthorizeRequest,
  ): Promise<void> {
    const workspace = await this.workspace.getByName(req.user.workspace);
    const response = await this.authorize(workspace, data, req.user.id);
    const redirect = `${data.redirect_uri}${
      data.redirect_uri.indexOf('?') > 0 ? '&' : '?'
    }${qs.stringify(response)}`;
    return res.redirect(redirect);
  }

  @Post()
  @HttpCode(HttpStatus.PERMANENT_REDIRECT)
  public async authorizePost(
    @Req() req: AuthorizedRequest,
    @Res() res: Response,
    @Body() data: AuthorizeRequest,
  ): Promise<void> {
    const workspace = await this.workspace.getById(req.user.workspace);
    const response = await this.authorize(workspace, data, req.user.id);
    const redirect = `${data.redirect_uri}${
      data.redirect_uri.indexOf('?') > 0 ? '&' : '?'
    }${qs.stringify(response)}`;
    return res.redirect(redirect);
  }

  private async authorize(
    workspace: Workspace,
    data: AuthorizeRequest,
    userId: string,
  ): Promise<AuthorizeResponse> {
    const redirectUri = data.redirect_uri;
    const [user, client] = await Promise.all([
      this.identityService.getByUserId(workspace.id, userId),
      this.client.getBy(data.client_id),
    ]);
    const scope = data.scope ? data.scope.split(' ') as Scope[] : client.scope;
    this.checkWorkspace(client, user, workspace.id);
    this.checkRedirectUri(client, redirectUri);
    this.checkScope(client, scope);

    switch (data.response_type) {
    case ResponseType.code:
      return await
        this.authorizeCode(client, user, scope, redirectUri, data.state);
    case ResponseType.token:
      return await this.authorizeToken(workspace, client, user, scope);
    }
  }

  private async authorizeCode(
    client: Client,
    user: Identity,
    scope: Scope[],
    redirectUri: string,
    state?: string,
  ): Promise<AuthorizeResponseCode> {
    const code =
      await this.service.authorize(client, user.userId, scope, redirectUri);
    return {
      code: code.value,
      workspace: user.workspaceId,
      state,
    };
  }

  private async authorizeToken(
    workspace: Workspace,
    client: Client,
    user: Identity,
    scope: Scope[],
  ): Promise<TokenResponse> {
    const [at, rt] =
      await this.token.authorize(client, user.userId, scope);

    return {
      access_token: await this.token.sign(workspace.name, at),
      expires_in: at.expiry,
      token_type: 'Bearer',
      refresh_token: await this.token.sign(workspace.name, rt),
      scope: scope.join(' '),
      workspace: workspace.name,
    };
  }

  private checkWorkspace(client: Client, user: Identity, workspaceId: string) {
    if (client.workspaceId !== user.workspaceId
      || user.workspaceId !== workspaceId)
    {
      throw new BadRequestException('Invalid workspace');
    }
  }

  private checkRedirectUri(client: Client, redirectUri: string): void {
    if (!new Set(client.redirectUris).has(redirectUri)) {
      throw new BadRequestException('Unauthorized redirect_uri');
    }
  }

  private checkScope(client: Client, scope: Scope[]): void {
    const clientScope = new Set(client.scope);
    for (const scp of scope) {
      if (!clientScope.has(scp)) {
        throw new BadRequestException('Unauthorized scope');
      }
    }
  }
}
