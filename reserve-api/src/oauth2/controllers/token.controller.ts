/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Body,
  NotImplementedException,
  BadRequestException,
  Post,
  HttpCode,
  Query,
  HttpStatus,
  UsePipes,
} from '@nestjs/common';

import {
  TokenRequest,
  TokenRequestPassword,
  TokenRequestRefreshToken,
  TokenRequestAuthorizationCode,
} from '../dto/token-request.dto';
import { TokenResponse } from '../dto/token-response.dto';
import { TokenService } from '../services/token.service';
import { ClientService } from '../services/client.service';
import { TokenPair } from '../interfaces/token-pair.interface';
import { AuhtorizeService } from '../services/authorize.service';
import { Client } from '../entities/client.entity';
import { AuthService } from '../../auth/auth.service';
import { GrantType } from '../../lib/oauth2/grant-type.type';
import { Scope } from '../../lib/oauth2/scope.type';
import { TokenValidation } from './validations/token.validation';
import { WorkspaceService } from '../../workspace/nest';
//#endregion

@Controller('api/v1/oauth2/token')
export class TokenController {
  public constructor(
    private readonly service: TokenService,
    private readonly auth: AuthService,
    private readonly client: ClientService,
    private readonly authorize: AuhtorizeService,
    private readonly workspace: WorkspaceService,
  ) { }

  @Post()
  @UsePipes(new TokenValidation())
  @HttpCode(HttpStatus.OK)
  public async token(@Body() data: TokenRequest): Promise<TokenResponse> {
    const [at, rt] = await this.getTokens(data);
    const workspace = await this.workspace.getById(at.client!.workspaceId);
    return {
      access_token: await this.service.sign(workspace.name, at),
      expires_in: at.expiry,
      token_type: 'Bearer',
      refresh_token: await this.service.sign(workspace.name, rt),
      scope: at.scope.join(' '),
      workspace: workspace.name,
    };
  }

  @Post('revoke')
  @HttpCode(HttpStatus.NO_CONTENT)
  public async revoke(
    @Body('refresh_token') refreshToken1?: string,
    @Query('refresh_token') refreshToken2?: string,
  ): Promise<void> {
    const refreshToken = refreshToken1 || refreshToken2 || '';
    await this.service.revokeRefreshToken(refreshToken);
  }

  private async getTokens(data: TokenRequest): Promise<TokenPair> {
    switch (data.grant_type) {
    case GrantType.authorizationCode:
      return this.tokenAuthorizationCode(data);
    case GrantType.clientCredentials:
      throw new NotImplementedException();
    case GrantType.password:
      return this.tokenPassword(data);
    case GrantType.refreshToken:
      return this.tokenRefreshToken(data);
    }
  }

  private async tokenAuthorizationCode(
    data: TokenRequestAuthorizationCode,
  ): Promise<TokenPair> {
    const [code, client] = await Promise.all([
      this.authorize.getCode(data.code),
      this.client.getBy(data.client_id, data.client_secret),
    ]);
    if (!code.isValid()) {
      throw new BadRequestException();
    }
    this.checkGrantType(client, GrantType.authorizationCode);
    this.checkScope(client, code.scope);
    return await this.service.authorize(client, code.userId, code.scope);
  }

  private async tokenPassword(
    data: TokenRequestPassword,
  ): Promise<TokenPair> {
    const { workspace: workspaceName, username, password } = data;
    const workspace = await this.workspace.getByName(workspaceName);
    const user = await this.auth.login(workspace.id, username, password);
    const client = await this.client.getWorkspaceClient(workspace.id);
    const scope = data.scope ? data.scope.split(' ') as Scope[] : client.scope;
    this.checkGrantType(client, GrantType.password);
    this.checkScope(client, scope);
    return await this.service.authorize(client, user.userId, scope);
  }

  private async tokenRefreshToken(
    data: TokenRequestRefreshToken,
  ): Promise<TokenPair> {
    return await this.service.transaction(async (trx) => {
      const rt = await this.service.getRefreshToken(data.refresh_token, trx);
      const client = await this.client.getById(rt.clientId, trx);
      const scope =
        data.scope ? data.scope.split(' ') as Scope[] : client.scope;
      this.checkGrantType(client, GrantType.refreshToken);
      this.checkScope(client, scope);
      const at = await this.service.refreshAccessToken(rt, client, scope, trx);
      return [at, rt] as TokenPair;
    });
  }

  private checkGrantType(client: Client, grantType: GrantType): void {
    if (client.grantType.indexOf(grantType) < 0) {
      throw new BadRequestException('Invalid grant_type');
    }
  }

  private checkScope(client: Client, scope: Scope[]): void {
    const clientScope = new Set(client.scope);
    for (const scp of scope) {
      if (!clientScope.has(scp)) {
        throw new BadRequestException('Unauthorized scope');
      }
    }
  }
}
