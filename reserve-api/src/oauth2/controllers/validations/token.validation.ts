/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  PipeTransform,
  BadRequestException,
  ArgumentMetadata,
} from '@nestjs/common';
import {
  TokenRequest,
  TokenRequestAuthorizationCode,
  TokenRequestClientCredentials,
  TokenRequestPassword,
  TokenRequestRefreshToken,
} from '../../dto/token-request.dto';
import { GrantType } from '../../../lib/oauth2/grant-type.type';
//#endregion

@Injectable()
export class TokenValidation implements PipeTransform {
  public transform(
    value: any,
    meta: ArgumentMetadata,
  ): TokenRequest {
    if (meta.type !== 'body') {
      return value;
    }
    switch (value.grant_type) {
    case GrantType.authorizationCode:
      return this.validateAuthorizationCode(value);
    case GrantType.clientCredentials:
      return this.validateClientCredentials(value);
    case GrantType.password:
      return this.validatePassword(value);
    case GrantType.refreshToken:
      return this.validateRefreshToken(value);
    default:
      throw new BadRequestException(
        'Invalid `grant_type` required string parameter.',
      );
    }
  }

  private validateAuthorizationCode(
    value: Partial<TokenRequestAuthorizationCode>,
  ): TokenRequestAuthorizationCode {
    if (!value.client_id || typeof value.client_id !== 'string') {
      throw new BadRequestException(
        '`client_id` is required and extected to be a string.',
      );
    }
    if (!value.client_secret || typeof value.client_secret !== 'string') {
      throw new BadRequestException(
        '`client_secret` is required and extected to be a string.',
      );
    }
    if (!value.code || typeof value.code !== 'string') {
      throw new BadRequestException(
        '`code` is required and extected to be a string.',
      );
    }
    if (value.state && typeof value.state !== 'string') {
      throw new BadRequestException(
        '`state` is extected to be a string.',
      );
    }
    return value as TokenRequestAuthorizationCode;
  }

  private validateClientCredentials(
    value: Partial<TokenRequestClientCredentials>,
  ): TokenRequestClientCredentials {
    if (!value.client_id || typeof value.client_id !== 'string') {
      throw new BadRequestException(
        '`client_id` is required and extected to be a string.',
      );
    }
    if (!value.client_secret || typeof value.client_secret !== 'string') {
      throw new BadRequestException(
        '`client_secret` is required and extected to be a string.',
      );
    }
    if (value.scope && typeof value.scope !== 'string') {
      throw new BadRequestException(
        '`code` is required and extected to be a string.',
      );
    }
    return value as TokenRequestClientCredentials;
  }

  private validatePassword(
    value: Partial<TokenRequestPassword>,
  ): TokenRequestPassword {
    if (!value.username || typeof value.username !== 'string') {
      throw new BadRequestException(
        '`username` is required and extected to be a string.',
      );
    }
    if (!value.password || typeof value.password !== 'string') {
      throw new BadRequestException(
        '`password` is required and extected to be a string.',
      );
    }
    if (!value.workspace || typeof value.workspace !== 'string') {
      throw new BadRequestException(
        '`workspace` is required and extected to be a string.',
      );
    }
    if (value.scope && typeof value.scope !== 'string') {
      throw new BadRequestException(
        '`scope` is extected to be a string.',
      );
    }
    return value as TokenRequestPassword;
  }

  private validateRefreshToken(
    value: Partial<TokenRequestRefreshToken>,
  ): TokenRequestRefreshToken {
    if (!value.refresh_token || typeof value.refresh_token !== 'string') {
      throw new BadRequestException(
        '`refresh_token` is required and extected to be a string.',
      );
    }
    if (value.scope && typeof value.scope !== 'string') {
      throw new BadRequestException(
        '`scope` is extected to be a string.',
      );
    }
    return value as TokenRequestRefreshToken;
  }
}
