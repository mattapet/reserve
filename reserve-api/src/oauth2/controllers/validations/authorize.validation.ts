/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  PipeTransform,
  BadRequestException,
  ArgumentMetadata,
} from '@nestjs/common';
import { AuthorizeRequest } from '../../dto/authorize-request.dto';
import { ResponseType } from '../../../lib/oauth2/response-type.type';
//#endregion

@Injectable()
export class AuthorizeValidation implements PipeTransform {
  public transform(
    value: Partial<AuthorizeRequest>,
    meta: ArgumentMetadata,
  ): any {
    if (meta.type !== 'body' && meta.type !== 'query') {
      return value;
    }
    if (!value.workspace || typeof value.workspace !== 'string') {
      throw new BadRequestException(
        '`workspace` is required and extected to be a string.',
      );
    }
    if (!value.client_id || typeof value.client_id !== 'string') {
      throw new BadRequestException(
        '`client_id` is required and extected to be a string.',
      );
    }
    if (!value.redirect_uri || typeof value.redirect_uri !== 'string') {
      throw new BadRequestException(
        '`redirect_uri` is required and extected to be a string.',
      );
    }
    switch (value.response_type) {
    case ResponseType.code:
    case ResponseType.token:
      break;
    default:
      throw new BadRequestException(
        '`response_type` is required and extected to be `token` or `code.',
      );
    }
    if (!value.scope || typeof value.scope !== 'string') {
      throw new BadRequestException(
        '`scope` is required and extected to be a string.',
      );
    }
    if (value.state && typeof value.state !== 'string') {
      throw new BadRequestException(
        '`state` is extected to be a string.',
      );
    }
  }
}
