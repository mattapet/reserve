/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Client } from './entities/client.entity';
import { ClientService } from './services/client.service';
import { AuthorizeController } from './controllers/authorize.controller';
import { TokenController } from './controllers/token.controller';
import { AuhtorizeService } from './services/authorize.service';
import { TokenService } from './services/token.service';
import { AuthModule } from '../auth/auth.module';
import { UserModule } from '../user/nest';
import { WorkspaceModule } from '../workspace/nest';
import { IdentityModule } from '../identity/nest';
import { registerJWTServiceModule } from '../lib/jwt-service.module';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([ Client ]),
    registerJWTServiceModule(),
    forwardRef(() => AuthModule),
    forwardRef(() => WorkspaceModule),
    UserModule,
    IdentityModule,
  ],
  controllers: [AuthorizeController, TokenController],
  providers: [AuhtorizeService, ClientService, TokenService],
  exports: [AuhtorizeService, ClientService, TokenService],
})
export class OAuth2Module { }
