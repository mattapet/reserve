/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
} from 'typeorm';
import { GrantType } from '../../lib/oauth2/grant-type.type';
import { Scope } from '../../lib/oauth2/scope.type';
import { AccessToken } from './access-token.entity';
import { RefreshToken } from './refresh-token.entity';
//#endregion

@Entity({ name: 'client_credentials' })
export class Client {
  @PrimaryGeneratedColumn('uuid', { name: 'client_id' })
  public clientId!: string;

  @Column({ length: 64, name: 'client_secret' })
  public clientSecret: string;

  @Column({ name: 'workspace_id' })
  public workspaceId: string;

  @Column('simple-array', { name: 'redirect_uris' })
  public redirectUris: string[];

  @Column('simple-array', { name: 'grant_type' })
  public grantType: GrantType[];

  @Column('simple-array')
  public scope: Scope[];

  @OneToMany(type => AccessToken, item => item.client)
  public accessTokens?: AccessToken[];

  @OneToMany(type => RefreshToken, item => item.client)
  public refreshTokens?: RefreshToken[];

  public constructor(
    clientSecret: string,
    workspaceId: string,
    redirectUris: string[],
    grantType: GrantType[],
    scope: Scope[],
  ) {
    this.clientSecret = clientSecret;
    this.workspaceId = workspaceId;
    this.redirectUris = redirectUris;
    this.grantType = grantType;
    this.scope = scope;
  }
}
