/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Entity,
  PrimaryColumn,
  Column,
  ManyToOne,
  JoinColumn,
  RelationId,
  CreateDateColumn,
} from 'typeorm';

import { Client } from './client.entity';
import { Scope } from '../../lib/oauth2/scope.type';
//#endregion

@Entity()
export class AccessToken {
  @PrimaryColumn({ length: 64 })
  public value: string;

  @Column()
  public expiry: number;

  @Column('simple-array')
  public scope: Scope[];

  @CreateDateColumn({ transformer: {
    to: (d) => d || new Date().toISOString(),
    from: (d: Date) => d,
  } })
  public created!: Date;

  @Column({ name: 'user_id' })
  public userId: string;

  @ManyToOne(type => Client, item => item.accessTokens, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'client_id', referencedColumnName: 'clientId' })
  public client?: Client;

  @RelationId((item: AccessToken) => item.client)
  public clientId!: string;

  public constructor(
    value: string,
    expiry: number,
    scope: Scope[],
    userId: string,
    client: Client,
  ) {
    this.value = value;
    this.expiry = expiry;
    this.scope = scope;
    this.userId = userId;
    this.client = client;
  }

  public isValid(): boolean {
    return Date.now() < this.created.valueOf() + this.expiry * 1000;
  }
}
