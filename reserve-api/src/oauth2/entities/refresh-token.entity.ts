/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Entity,
  PrimaryColumn,
  Column,
  ManyToOne,
  JoinColumn,
  RelationId,
  CreateDateColumn,
  AfterLoad,
} from 'typeorm';

import { Client } from './client.entity';
import { Scope } from '../../lib/oauth2/scope.type';
//#endregion

@Entity()
export class RefreshToken {
  @PrimaryColumn({ length: 64 })
  public value: string;

  @Column()
  public expiry: number;

  @Column()
  public revoked: boolean;

  @Column('simple-array')
  public scope: Scope[];

  @CreateDateColumn({ transformer: {
    to: (d) => d || new Date().toISOString(),
    from: (d: Date) => d,
  } })
  public created!: Date;

  @Column({ name: 'user_id' })
  public userId: string;

  @ManyToOne(type => Client, item => item.refreshTokens, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'client_id', referencedColumnName: 'clientId' })
  public client?: Client;

  @RelationId((item: RefreshToken) => item.client)
  public clientId!: string;

  public constructor(
    value: string,
    expiry: number,
    scope: Scope[],
    userId: string,
    client: Client,
  ) {
    this.value = value;
    this.expiry = expiry;
    this.scope = scope;
    this.userId = userId;
    this.client = client;
    this.revoked = false;
  }

  @AfterLoad()
  // @ts-ignore:line
  private converBoolean() {
    this.revoked = !!this.revoked;
  }

  public isValid(): boolean {
    return !this.revoked
      && Date.now() < this.created.valueOf() + this.expiry * 1000;
  }
}
