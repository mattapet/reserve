/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';

import { AuthorizationCode } from '../entities/authorization-code.entity';
import { Client } from '../entities/client.entity';
import { Scope } from '../../lib/oauth2/scope.type';
import { generateToken } from '../../lib/token';
//#endregion

@Injectable()
export class AuhtorizeService {
  public constructor(
    @InjectEntityManager()
    private readonly manager: EntityManager,
  ) { }

  public async authorize(
    client: Client,
    userId: string,
    scope: Scope[],
    redirectUri: string,
    trx: EntityManager = this.manager,
  ): Promise<AuthorizationCode> {
    const value = generateToken();
    const item =
      new AuthorizationCode(value, 900, scope, redirectUri, userId, client);
    return await trx.save(item);
  }

  public async getCode(
    value: string,
    trx: EntityManager = this.manager,
  ): Promise<AuthorizationCode> {
    const item = await trx.findOne(AuthorizationCode, value);
    if (!item) {
      throw new NotFoundException();
    }
    return item;
  }

  public async transaction<T>(
    runInTrx: (em: EntityManager) => Promise<T>,
  ): Promise<T> {
    return await this.manager.transaction(runInTrx);
  }
}
