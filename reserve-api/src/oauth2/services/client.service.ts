/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';

import { Client } from '../entities/client.entity';
import { generateToken } from '../../lib/token';
import { GrantType } from '../../lib/oauth2/grant-type.type';
import { SCOPE_ALL } from '../../lib/oauth2/scope.type';
//#endregion

@Injectable()
export class ClientService {
  public constructor(
    @InjectEntityManager()
    private readonly manager: EntityManager,
  ) { }

  public async register(
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<Client> {
    const secret = generateToken();
    const grantType = [GrantType.password, GrantType.refreshToken];
    const item = new Client(secret, workspaceId, [], grantType, SCOPE_ALL);
    return await trx.save(item);
  }

  public async getBy(
    clientId: string,
    clientSecret?: string,
    trx: EntityManager = this.manager,
  ): Promise<Client> {
    const item = await trx.findOne(Client, { clientId, clientSecret });
    if (!item) {
      throw new BadRequestException('Client error.');
    }
    return item;
  }

  public async getById(
    clientId: string,
    trx: EntityManager = this.manager,
  ): Promise<Client> {
    const item = await trx.findOne(Client, { clientId });
    if (!item) {
      throw new BadRequestException('Client error.');
    }
    return item;
  }

  public async getWorkspaceClient(
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<Client> {
    const item = await trx.findOne(Client, { workspaceId });
    if (!item) {
      throw new BadRequestException('Client error.');
    }
    return item;
  }

  public async transaction<T>(
    runInTrx: (em: EntityManager) => Promise<T>,
  ): Promise<T> {
    return await this.manager.transaction(runInTrx);
  }
}
