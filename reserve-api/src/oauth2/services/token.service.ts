/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  BadRequestException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';

import { TokenPair } from '../interfaces/token-pair.interface';
import { AccessToken } from '../entities/access-token.entity';
import { RefreshToken } from '../entities/refresh-token.entity';
import { generateToken } from '../../lib/token';
import { Client } from '../../oauth2/entities/client.entity';
import { Scope } from '../../lib/oauth2/scope.type';
import {
  JwtPayload,
  TokenKind,
} from '../../lib/interface/jwt-payload.interface';
import { NestUserRoleService } from '../../user/user-role/nest';
import { NestBannedService } from '../../user/banned/nest';
//#endregion

@Injectable()
export class TokenService {
  public constructor(
    @InjectEntityManager()
    private readonly manager: EntityManager,
    private readonly jwtService: JwtService,
    private readonly userRole: NestUserRoleService,
    private readonly userBanned: NestBannedService,
  ) { }

  public async authorize(
    client: Client,
    userId: string,
    scope: Scope[],
    trx: EntityManager = this.manager,
  ): Promise<TokenPair> {
    const at = this.createAccessToken(userId, client, scope);
    const rt = this.createRefreshToken(userId, client, scope);
    if (await this.userBanned.getById(client.workspaceId, userId)) {
      throw new UnauthorizedException('You have been banned');
    }
    return await Promise.all([trx.save(at), trx.save(rt)]);
  }

  public async logout(
    userId: string,
    trx: EntityManager = this.manager,
  ): Promise<void> {
    await trx.update(RefreshToken, { userId }, { revoked: true });
  }

  public async revokeRefreshToken(
    token: string,
    trx: EntityManager = this.manager,
  ): Promise<void> {
    const tok = this.verifyToken(token);
    await trx.update(RefreshToken, { value: tok.jit }, { revoked: true });
  }

  public async refreshAccessToken(
    refreshToken: RefreshToken,
    client: Client,
    scope: Scope[],
    trx: EntityManager = this.manager,
  ): Promise<AccessToken> {
    const at = this.createAccessToken(refreshToken.userId, client, scope);
    return await trx.save(at);
  }

  public async getRefreshToken(
    token: string,
    trx: EntityManager = this.manager,
  ): Promise<RefreshToken> {
    const tok = this.verifyToken(token);
    const rt = await trx.findOne(RefreshToken, tok.jit, {
      relations: ['client'],
    });
    if (!rt || !rt.isValid()) {
      throw new BadRequestException('Invalid refresh token');
    }
    return rt;
  }

  public async sign(workspace: string, token: AccessToken): Promise<string>;
  public async sign(workspace: string, token: RefreshToken): Promise<string> {
    const tokenKind = token instanceof AccessToken ?
      TokenKind.accessToken : TokenKind.refreshToken;
    return this.jwtService.sign({
      jit: token.value,
      sub: token.userId,
      exp: Math.floor(token.created.valueOf() / 1000) + token.expiry,
      iat: Math.floor(token.created.valueOf() / 1000),
      scope: token.scope,
      kind: tokenKind,
      user: {
        id: token.userId,
        workspace,
        role: await this.userRole
          .getById(token.client!.workspaceId, token.userId),
      },
    } as JwtPayload);
  }

  private verifyToken(token: string): JwtPayload {
    try {
      return this.jwtService.verify<JwtPayload>(token);
    } catch (err) {
      throw new BadRequestException('Invalid refresh token');
    }
  }

  private createAccessToken(
    userId: string,
    client: Client,
    scope: Scope[],
  ): AccessToken {
    return new AccessToken(generateToken(), 300, scope, userId, client);
  }

  private createRefreshToken(
    userId: string,
    client: Client,
    scope: Scope[],
  ): RefreshToken {
    return new RefreshToken(generateToken(), 84600, scope, userId, client);
  }

  public async transaction<T>(
    runInTrx: (em: EntityManager) => Promise<T>,
  ): Promise<T> {
    return await this.manager.transaction(runInTrx);
  }
}
