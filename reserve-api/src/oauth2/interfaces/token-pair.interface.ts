/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { RefreshToken } from '../entities/refresh-token.entity';
import { AccessToken } from '../entities/access-token.entity';
//#endregion

export type TokenPair = [AccessToken, RefreshToken];
