/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export interface TokenResponse {
  readonly access_token: string;
  readonly expires_in: number;
  readonly token_type: 'Bearer';
  readonly refresh_token: string;
  readonly scope: string;
  readonly workspace: string;
}
