/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TokenResponse } from './token-response.dto';
//#endregion

export interface AuthorizeResponseCode {
  readonly code: string;
  readonly workspace: string;
  readonly state?: string;
}

export type AuthorizeResponse =
    AuthorizeResponseCode
  | TokenResponse;
