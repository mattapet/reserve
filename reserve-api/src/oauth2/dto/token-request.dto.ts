/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { GrantType } from '../../lib/oauth2/grant-type.type';
//#endregion

export interface TokenRequestAuthorizationCode {
  readonly grant_type: GrantType.authorizationCode;
  readonly code: string;
  readonly client_id: string;
  readonly client_secret: string;
  readonly state?: string;
}

export interface TokenRequestClientCredentials {
  readonly grant_type: GrantType.clientCredentials;
  readonly client_id: string;
  readonly client_secret: string;
  readonly scope?: string;
}

export interface TokenRequestPassword {
  readonly grant_type: GrantType.password;
  readonly username: string;
  readonly password: string;
  readonly workspace: string;
  readonly scope?: string;
}

export interface TokenRequestRefreshToken {
  readonly grant_type: GrantType.refreshToken;
  readonly refresh_token: string;
  readonly scope?: string;
}

export type TokenRequest =
    TokenRequestAuthorizationCode
  | TokenRequestClientCredentials
  | TokenRequestPassword
  | TokenRequestRefreshToken;
