/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ResponseType } from '../../lib/oauth2/response-type.type';
//#endregion

export interface AuthorizeRequestCode {
  readonly response_type: ResponseType.code;
  readonly client_id: string;
  readonly workspace: string;
  readonly scope: string;
  readonly redirect_uri: string;
  readonly state?: string;
}

export interface AuthorizeRequestToken {
  readonly response_type: ResponseType.token;
  readonly client_id: string;
  readonly workspace: string;
  readonly scope: string;
  readonly redirect_uri: string;
  readonly state?: string;
}

export type AuthorizeRequest =
    AuthorizeRequestCode
  | AuthorizeRequestToken;
