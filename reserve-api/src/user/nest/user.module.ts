 /*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { NestUserService } from './user.service';
import { UserController } from './user.controller';
import {
  NestBannedService,
  BannedEventEntity,
  BannedEventRepository,
} from '../banned/nest';
import {
  NestUserRoleService,
  UserRoleEventEntity,
  UserRoleEventRepository,
} from '../user-role/nest';
import { WorkspaceModule } from '../../workspace/nest';
import { IdentityModule } from '../../identity/nest';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BannedEventEntity,
      UserRoleEventEntity,
      BannedEventRepository,
      UserRoleEventRepository,
    ]),
    forwardRef(() => WorkspaceModule),
    IdentityModule,
  ],
  controllers: [UserController],
  providers: [NestUserService, NestBannedService, NestUserRoleService],
  exports: [NestUserService, NestBannedService, NestUserRoleService],
})
export class UserModule { }
