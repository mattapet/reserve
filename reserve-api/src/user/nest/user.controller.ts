/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Get,
  UseGuards,
  Req,
  Param,
  UsePipes,
  UseInterceptors,
  Patch,
  Body,
  Put,
  ClassSerializerInterceptor,
  Delete,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { NestUserService } from './user.service';
import { User } from '../user.interface';
import { GetUserValidation } from './validation/id.validation';
import { WorkspaceService } from '../../workspace/nest';
import { RoleGuard } from '../../lib/guards/RoleGuard';
import { UserRole } from '../../lib/user/user-role.type';
import {
  AuthorizedRequest,
} from '../../lib/interface/authorized-request.interface';
import { TransformerInterceptor } from './interceptors/transformer.interceptor';
import { NestBannedService } from '../banned/nest';
import { NestUserRoleService } from '../user-role/nest';
import { ProfileDTO } from './dto/profile.dto';
//#endregion

@Controller('api/v1/user')
@UseGuards(AuthGuard('jwt'))
export class UserController {
  public constructor(
    private readonly service: NestUserService,
    private readonly bannedService: NestBannedService,
    private readonly roleService: NestUserRoleService,
    private readonly workspace: WorkspaceService,
  ) { }

  @Get()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.owner))
  @UseInterceptors(new TransformerInterceptor())
  public async getAll(@Req() req: AuthorizedRequest): Promise<User[]> {
    const workspace = await this.workspace.getById(req.user.workspace);
    return await this.service.getAll(workspace.id);
  }

  @Get('me')
  @UseInterceptors(new TransformerInterceptor())
  public async getMe(@Req() req: AuthorizedRequest): Promise<User> {
    const workspace = await this.workspace.getById(req.user.workspace);
    return await this.service.getById(workspace.id, req.user.id);
  }

  @Put('me/profile')
  @UseInterceptors(ClassSerializerInterceptor, new TransformerInterceptor())
  public async updateProfile(
    @Req() req: AuthorizedRequest,
    @Body() {
      first_name: firstName,
      last_name: lastName,
      phone,
    }: ProfileDTO,
  ): Promise<User> {
    const workspaceId = req.user.workspace;
    const userId = req.user.id;
    const profile = { firstName, lastName, phone };
    return await this.service.updateProfile(workspaceId, userId, profile);
  }

  @Get(':id')
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.owner))
  @UsePipes(new GetUserValidation())
  @UseInterceptors(new TransformerInterceptor())
  public async getById(
    @Req() req: AuthorizedRequest,
    @Param('id') userId: string,
  ): Promise<User> {
    const workspace = await this.workspace.getById(req.user.workspace);
    return await this.service.getById(userId, workspace.id);
  }

  @Patch(':id/role')
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.owner))
  @UsePipes(new GetUserValidation())
  @UseInterceptors(new TransformerInterceptor())
  public async updateRole(
    @Req() req: AuthorizedRequest,
    @Param('id') userId: string,
    @Body('role') role: UserRole,
  ): Promise<User> {
    const workspaceId = req.user.workspace;
    const workspace = await this.workspace.getById(workspaceId);
    await this.roleService
      .changeById(workspaceId, role, userId, req.user.id);
    return await this.service.getById(workspace.id, userId);
  }

  @Patch(':id/ban')
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.owner))
  @UsePipes(new GetUserValidation())
  @UseInterceptors(new TransformerInterceptor())
  public async toggleBan(
    @Req() req: AuthorizedRequest,
    @Param('id') userId: string,
  ): Promise<User> {
    const workspaceId = req.user.workspace;
    const workspace = await this.workspace.getById(workspaceId);
    await this.bannedService.toggleById(workspace.id, userId, req.user.id);
    return await this.service.getById(workspace.id, userId);
  }

  @Delete('me')
  public async removeUser(@Req() req: AuthorizedRequest): Promise<void> {
    const workspaceId = req.user.workspace;
    const userId = req.user.id;

    await this.service.deleteById(workspaceId, userId);
  }
}
