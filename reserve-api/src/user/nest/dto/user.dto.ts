/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from '../../../lib/user/user-role.type';
//#endregion

export interface BriefUserDTO {
  readonly id: string;
  readonly role: UserRole;
  readonly workspace: string;
  readonly last_login?: string;
}

export interface FullUserDTO {
  readonly id: string;
  readonly role: UserRole;
  readonly workspace: string;
  readonly last_login?: string;
  readonly email: string;
  readonly profile: {
    readonly first_name: string;
    readonly last_name: string;
    readonly phone?: string;
  };
  readonly banned: boolean;
}

export type UserDTO =
    BriefUserDTO
  | FullUserDTO;
