/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
//#endregion

export class ProfileDTO {
  @IsNotEmpty()
  readonly first_name!: string;

  @IsNotEmpty()
  readonly last_name!: string;

  @IsOptional()
  @IsString()
  readonly phone?: string;
}
