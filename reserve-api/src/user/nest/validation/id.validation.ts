/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  PipeTransform,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';
//#endregion

@Injectable()
export class GetUserValidation implements PipeTransform {
  public transform(
    value: any,
    meta: ArgumentMetadata,
  ): any {
    if (meta.type !== 'param' || meta.data !== 'id') {
      return value;
    }
    if (!value || !value.length) {
      throw new BadRequestException(
        '`id` parameter expected to be a non-empty string.',
      );
    }
    return value;
  }
}
