/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';

import { UserServiceImpl } from '../impl/user.service';
import { NestIdentityService } from '../../identity/nest';
import { NestUserRoleService } from '../user-role/nest';
import { NestBannedService } from '../banned/nest';
//#endregion

@Injectable()
export class NestUserService extends UserServiceImpl {
  public constructor(
    idenityService: NestIdentityService,
    userRoleService: NestUserRoleService,
    bannedRoleService: NestBannedService,
  ) {
    super(idenityService, userRoleService, bannedRoleService);
  }
}
