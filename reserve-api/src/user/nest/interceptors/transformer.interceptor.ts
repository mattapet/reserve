/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../user.interface';
import { FullUserDTO } from '../dto/user.dto';
//#endregion

@Injectable()
export class TransformerInterceptor implements NestInterceptor<
  User | User[],
  FullUserDTO | FullUserDTO[]
> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<User | User[]>,
  ): Observable<FullUserDTO | FullUserDTO[]> {
    return call$.handle().pipe(
      map((payload: User | User[]) => Array.isArray(payload) ?
        payload.map(this.encode) : this.encode(payload),
      ),
    );
  }

  private encode = (user: User) => ({
    id: user.id,
    workspace: user.workspaceId,
    role: user.role,
    email: user.email,
    profile: {
      first_name: user.profile!.firstName,
      last_name: user.profile!.lastName,
      phone: user.profile!.phone,
    },
    banned: user.banned,
  }) as FullUserDTO
}
