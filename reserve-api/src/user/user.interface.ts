/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from '../lib/user/user-role.type';
//#endregion

export interface User {
  readonly id: string;
  readonly workspaceId: string;
  readonly email: string;
  readonly banned: boolean;
  readonly role: UserRole;
  readonly profile: {
    readonly firstName: string;
    readonly lastName: string;
    readonly phone?: string;
  };
}
