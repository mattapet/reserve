/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { User } from '../user.interface';
import { UserService } from '../user.service';
import { UserRoleService } from '../user-role';
import { BannedService } from '../banned';
import { Identity, IdentityService } from '../../identity';
import { Profile } from '../profile.interface';
//#endregion

export class UserServiceImpl implements UserService {
  public constructor(
    private readonly identityService: IdentityService,
    private readonly userRoleService: UserRoleService,
    private readonly bannedService: BannedService,
  ) { }

  public async getAll(workspaceId: string): Promise<User[]> {
    const identities = await this.identityService.getAll(workspaceId);
    return Promise.all(identities.map(identity => this.fillIdentity(identity)));
  }

  public async getById(workspaceId: string, userId: string): Promise<User> {
    const identity = await this.identityService
      .getByUserId(workspaceId, userId);
    return await this.fillIdentity(identity);
  }

  public async updateProfile(
    workspaceId: string,
    userId: string,
    profile: Profile,
  ): Promise<User> {
    const updated = await this.identityService.update(
      workspaceId,
      userId,
      profile.firstName,
      profile.lastName,
      profile.phone,
    );
    return await this.fillIdentity(updated);
  }

  public async deleteById(workspaceId: string, userId: string): Promise<void> {
    await this.identityService.anonymizeByUserId(workspaceId, userId);
  }

  private async fillIdentity(identity: Identity): Promise<User> {
    const [role, banned] = await Promise.all([
      this.userRoleService.getById(identity.workspaceId, identity.userId),
      this.bannedService.getById(identity.workspaceId, identity.userId),
    ]);
    return {
      id: identity.userId,
      workspaceId: identity.workspaceId,
      email: identity.email,
      role,
      banned,
      profile: {
        firstName: identity.firstName,
        lastName: identity.lastName,
        phone: identity.phone,
      },
    };
  }
}
