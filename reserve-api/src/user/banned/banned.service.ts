/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { BannedAggregate } from './banned.aggregate';
import { BannedEvent } from './banned.event';
//#endregion

export interface BannedService {
  // - MARK: Queries

  dumpEvents(worksapceId: string): Promise<BannedEvent[]>;

  getById(worksapceId: string, userId: string): Promise<BannedAggregate>;

  // - MARK: Commands

  toggleById(
    workspaceId: string,
    userId: string,
    toggledBy: string,
  ): Promise<BannedAggregate>;
}
