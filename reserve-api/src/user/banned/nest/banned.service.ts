/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';

import { bannedCommandHandler } from '../impl/banned.command-handler';
import { bannedEventHandler } from '../impl/banned.event-handler';
import { defaultAggregate } from '../banned.aggregate';
import { BannedServiceImpl } from '../impl/banned.service';
import { BannedEventRepository } from './banned-event.repository';
import { NestUserRoleService } from '../../user-role/nest';
import { BaseRepository } from '../../../lib/events/repository-base';
//#endregion

@Injectable()
export class NestBannedService extends BannedServiceImpl {
  public constructor(
    @InjectConnection()
    connection: Connection,
    userRole: NestUserRoleService,
  ) {
    super(
      new BaseRepository(
        connection.getCustomRepository(BannedEventRepository),
        bannedCommandHandler,
        bannedEventHandler,
        defaultAggregate,
      ),
      userRole,
    );
  }
}
