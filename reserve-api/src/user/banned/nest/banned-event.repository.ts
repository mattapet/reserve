/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository, Repository } from 'typeorm';

import { BannedEventEntity } from './banned-event.entity';
import { BannedEventRepository as Interface } from '../banned-event.repository';
import { BannedEvent } from '../banned.event';
//#endregion

@EntityRepository(BannedEventEntity)
export class BannedEventRepository extends Repository<BannedEventEntity>
  implements Interface
{
  public async findByRowId(rowId: string): Promise<BannedEvent[]> {
    const events = await this.find({
      where: { rowId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async findByRowIdAndAggregateId(
    rowId: string,
    aggregateId: string,
  ): Promise<BannedEvent[]> {
    const events = await this.find({
      where: { rowId, aggregateId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async saveEvents(...events: BannedEvent[]): Promise<BannedEvent[]> {
    const results = await this.manager.transaction(async (trx) => {
      return await trx.save(BannedEventEntity, events.map(this.wrap));
    });
    return results.map(this.unwrap);
  }

  public wrap(event: BannedEvent): BannedEventEntity {
    const { workspaceId, aggregateId, timestamp } = event;
    return new BannedEventEntity(workspaceId, aggregateId, timestamp, event);
  }

  public unwrap(entity: BannedEventEntity): BannedEvent {
    return entity.payload;
  }
}
