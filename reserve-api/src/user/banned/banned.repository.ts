/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { BanCommand } from './banned.command';
import { BannedEvent } from './banned.event';
import { BannedAggregate } from './banned.aggregate';
import { Repository } from '../../lib/events/repository-base.interface';
//#endregion

export interface BannedRepository extends Repository<
  BanCommand,
  BannedEvent,
  BannedAggregate
> { }
