/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '../../lib/events/event';
//#endregion

export enum BannedEventType {
  placed = 'placed',
  lifted = 'lifted',
}

export interface BanPlaced extends Event {
  readonly type: BannedEventType.placed;
  readonly payload: {
    readonly placedBy: string;
  };
}

export interface BanLifted extends Event {
  readonly type: BannedEventType.lifted;
  readonly payload: {
    readonly liftedBy: string;
  };
}

export type BannedEvent =
  | BanPlaced
  | BanLifted;
