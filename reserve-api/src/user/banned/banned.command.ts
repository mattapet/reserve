/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '../../lib/events/command';
//#endregion

export enum BanCommandType {
  toggle = 'toggle',
}

export interface BanToggle extends Command {
  readonly type: BanCommandType.toggle;
  readonly payload: {
    readonly toggledBy: string;
  };
}

export type BanCommand = BanToggle;
