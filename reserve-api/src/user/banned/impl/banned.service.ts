/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { BannedAggregate } from '../banned.aggregate';
import { BannedEvent } from '../banned.event';
import { BanCommandType } from '../banned.command';
import { UserRoleService } from '../../user-role';
import { BannedRepository } from '../banned.repository';
import { BannedService } from '../banned.service';
//#endregion

export class BannedServiceImpl implements BannedService {
  public constructor(
    private readonly repository: BannedRepository,
    private readonly userRole: UserRoleService,
  ) { }

  // - MARK: Queries

  public async dumpEvents(worksapceId: string): Promise<BannedEvent[]> {
    return await this.repository.dumpEvents(worksapceId);
  }

  public async getById(
    worksapceId: string,
    userId: string,
  ): Promise<BannedAggregate> {
    return await this.repository.getById(worksapceId, `${userId}`);
  }

  // - MARK: Commands

  public async toggleById(
    workspaceId: string,
    userId: string,
    toggledBy: string,
  ): Promise<BannedAggregate> {
    const aggregate = await this.repository.getById(workspaceId, `${userId}`);

    if (await this.userRole.isUser(workspaceId, toggledBy)) {
      throw new Error('Only Owners or Maintainers can ban people.');
    }

    if (userId === toggledBy) {
      throw new Error('Cannot change your own ban.');
    }

    if (await this.userRole.isOwner(workspaceId, userId)
      && !await this.userRole.isOwner(workspaceId, toggledBy))
    {
      throw new Error('Don\'t have priviledges to chagne user\'s ban.');
    }

    return await this.repository.execute({
      type: BanCommandType.toggle,
      aggregateId: `${userId}`,
      workspaceId,
      payload: { toggledBy },
    }, aggregate);
  }
}
