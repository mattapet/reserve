/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { BannedAggregate } from '../banned.aggregate';
import { BannedEvent, BannedEventType } from '../banned.event';
//#endregion

export const bannedEventHandler = (
  aggregate: BannedAggregate,
  event: BannedEvent,
): BannedAggregate => {
  switch (event.type) {
  case BannedEventType.placed:
    return true;
  case BannedEventType.lifted:
    return false;
  }
};
