/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { BanCommand } from '../banned.command';
import { BannedEvent, BannedEventType } from '../banned.event';
import { BannedAggregate } from '../banned.aggregate';
//#endregion

export const bannedCommandHandler = (
  aggregate: BannedAggregate,
  command: BanCommand,
): BannedEvent[] => {
  const { workspaceId, aggregateId } = command;
  if (aggregate) {
    return [{
      type: BannedEventType.lifted,
      workspaceId,
      aggregateId,
      timestamp: new Date(),
      payload: {
        liftedBy: command.payload.toggledBy,
      },
    }];
  } else {
    return [{
      type: BannedEventType.placed,
      workspaceId,
      aggregateId,
      timestamp: new Date(),
      payload: {
        placedBy: command.payload.toggledBy,
      },
    }];
  }
};
