/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { bannedEventHandler } from '../banned.event-handler';
import { BannedEventType, BannedEvent } from '../../banned.event';
//#endregion

describe('banned.event-handler', () => {
  it('should return `true` if ban was palced', () => {
    const workspaceId = 'test';
    const aggregateId = '45';
    const placedBy = '49';
    const aggregate = false;
    const event: BannedEvent = {
      type: BannedEventType.placed,
      workspaceId,
      aggregateId: `${aggregateId}`,
      timestamp: new Date(0),
      payload: {
        placedBy,
      },
    };

    const result = bannedEventHandler(aggregate, event);

    expect(result).toBe(true);
  });

  it('should return `false` if ban was lifted', () => {
    const workspaceId = 'test';
    const aggregateId = '45';
    const liftedBy = '49';
    const aggregate = true;
    const event: BannedEvent = {
      type: BannedEventType.lifted,
      workspaceId,
      aggregateId: `${aggregateId}`,
      timestamp: new Date(0),
      payload: {
        liftedBy,
      },
    };

    const result = bannedEventHandler(aggregate, event);

    expect(result).toBe(false);
  });
});
