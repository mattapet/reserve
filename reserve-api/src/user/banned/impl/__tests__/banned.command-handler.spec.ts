/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { bannedCommandHandler } from '../banned.command-handler';
import { BanCommand, BanCommandType } from '../../banned.command';
import { BannedEventType } from '../../banned.event';
//#endregion

describe('banned.command-handler', () => {
  it('should produce `placed` event when currently not banned', () => {
    const workspaceId = 'test';
    const aggregateId = '45';
    const toggledBy = '49';
    const aggregate = false;
    const command: BanCommand = {
      type: BanCommandType.toggle,
      workspaceId,
      aggregateId: `${aggregateId}`,
      payload: {
        toggledBy,
      },
    };

    const result = bannedCommandHandler(aggregate, command);

    expect(result[0].type).toEqual(BannedEventType.placed);
  });

  it('should produce `lifted` event when currently banned', () => {
    const workspaceId = 'test';
    const aggregateId = '45';
    const toggledBy = '49';
    const aggregate = true;
    const command: BanCommand = {
      type: BanCommandType.toggle,
      workspaceId,
      aggregateId: `${aggregateId}`,
      payload: {
        toggledBy,
      },
    };

    const result = bannedCommandHandler(aggregate, command);

    expect(result[0].type).toEqual(BannedEventType.lifted);
  });
});
