/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { BannedServiceImpl } from '../banned.service';
import { BannedRepository } from '../../banned.repository';
import { BanCommandType } from '../../banned.command';
import { UserRoleService } from '../../../user-role';
import { BannedRepositoryMock } from '../../__mocks__';
import { UserRoleServiceMock } from '../../../user-role/__mocks__';
//#endregion

describe('banned.service', () => {
  let userRoleService!: UserRoleService;
  let bannedRepository!: BannedRepository;
  let bannedService!: BannedServiceImpl;

  beforeEach(() => {
    userRoleService = new UserRoleServiceMock();
    bannedRepository = new BannedRepositoryMock();
    bannedService = new BannedServiceImpl(bannedRepository, userRoleService);
  });

  describe('#dumpEvents()', () => {
    it('should return events from the repository', async () => {
      const workspaceId = 'test';
      const fn = jest.spyOn(bannedRepository, 'dumpEvents');

      await bannedService.dumpEvents(workspaceId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId);
    });
  });

  describe('#getById()', () => {
    it('should return events from the repository for aggregate', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const fn = jest.spyOn(bannedRepository, 'getById');

      await bannedService.getById(workspaceId, aggregateId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId, `${aggregateId}`);
    });
  });

  describe('#toggleById()', () => {
    it('should produce a toggle command', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const toggledBy = '49';
      const aggregate = false;
      const getById = jest.spyOn(bannedRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementationOnce(() => Promise.resolve(false));
      const isOwner = jest.spyOn(userRoleService, 'isOwner')
        .mockImplementationOnce(() => Promise.resolve(false));
      const fn = jest.spyOn(bannedRepository, 'execute');

      await bannedService.toggleById(workspaceId, aggregateId, toggledBy);

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, `${aggregateId}`);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, toggledBy);
      expect(isOwner).toHaveBeenCalledTimes(1);
      expect(isOwner).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith({
        type: BanCommandType.toggle,
        workspaceId,
        aggregateId: `${aggregateId}`,
        payload: {
          toggledBy,
        },
      }, aggregate);
    });

    it('should throw an error if user tried to ban', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const toggledBy = '49';
      const aggregate = false;
      const getById = jest.spyOn(bannedRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementationOnce(() => Promise.resolve(true));
      const isOwner = jest.spyOn(userRoleService, 'isOwner');
      const fn = jest.spyOn(bannedRepository, 'execute');

      await expect(
        bannedService.toggleById(workspaceId, aggregateId, toggledBy),
      ).rejects
        .toEqual(new Error('Only Owners or Maintainers can ban people.'));

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, `${aggregateId}`);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, toggledBy);
      expect(isOwner).not.toHaveBeenCalled();
      expect(fn).not.toHaveBeenCalled();
    });

    it('should throw an error if trying to toggle yourself', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const toggledBy = '45';
      const aggregate = false;
      const getById = jest.spyOn(bannedRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementationOnce(() => Promise.resolve(false));
      const isOwner = jest.spyOn(userRoleService, 'isOwner');
      const fn = jest.spyOn(bannedRepository, 'execute');

      await expect(
        bannedService.toggleById(workspaceId, aggregateId, toggledBy),
      ).rejects
        .toEqual(new Error('Cannot change your own ban.'));

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, `${aggregateId}`);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, toggledBy);
      expect(isOwner).not.toHaveBeenCalled();
      expect(fn).not.toHaveBeenCalled();
    });

    it('should throw an error if trying to ban an Owner', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const toggledBy = '49';
      const aggregate = false;
      const getById = jest.spyOn(bannedRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementationOnce(() => Promise.resolve(false));
      const isOwner = jest.spyOn(userRoleService, 'isOwner')
        .mockImplementationOnce(() => Promise.resolve(true))
        .mockImplementationOnce(() => Promise.resolve(false));
      const fn = jest.spyOn(bannedRepository, 'execute');

      await expect(
        bannedService.toggleById(workspaceId, aggregateId, toggledBy),
      ).rejects
        .toEqual(new Error('Don\'t have priviledges to chagne user\'s ban.'));

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, `${aggregateId}`);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, toggledBy);
      expect(isOwner).toHaveBeenCalledTimes(2);
      expect(isOwner).toHaveBeenNthCalledWith(1, workspaceId, aggregateId);
      expect(isOwner).toHaveBeenNthCalledWith(2, workspaceId, toggledBy);
      expect(fn).not.toHaveBeenCalled();
    });
  });
});
