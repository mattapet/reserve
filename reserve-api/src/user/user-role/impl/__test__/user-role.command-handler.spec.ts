/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { userRoleCommandHandler } from '../user-role.command-handler';
import { UserRoleCommandType, UserRoleCommand } from '../../user-role.command';
import { UserRoleEventType } from '../../user-role.event';
import { defaultAggregate } from '../../user-role.aggregate';
import { UserRole } from '../../../../lib/user/user-role.type';
//#endregion

describe('userRoleCommandHandler', () => {
  it('should produce a `ownerCreated` event', () => {
    const workspaceId = 'test';
    const aggregateId = '45';
    const command: UserRoleCommand = {
      type: UserRoleCommandType.createOwner,
      workspaceId,
      aggregateId,
      payload: {},
    };

    const result = userRoleCommandHandler(defaultAggregate, command);

    expect(result[0].type).toBe(UserRoleEventType.ownerCreated);
  });

  it('should produce a `changed` event', () => {
    const workspaceId = 'test';
    const aggregateId = '45';
    const changedBy = '49';
    const command: UserRoleCommand = {
      type: UserRoleCommandType.change,
      workspaceId,
      aggregateId,
      payload: {
        role: UserRole.owner,
        changedBy,
      },
    };

    const result = userRoleCommandHandler(defaultAggregate, command);

    expect(result[0].type).toBe(UserRoleEventType.changed);
    expect(result[0].payload).toEqual(command.payload);
  });

  it('should produce no events if no change made', () => {
    const workspaceId = 'test';
    const aggregateId = '45';
    const changedBy = '49';
    const command: UserRoleCommand = {
      type: UserRoleCommandType.change,
      workspaceId,
      aggregateId,
      payload: {
        role: UserRole.user,
        changedBy,
      },
    };

    const results = userRoleCommandHandler(defaultAggregate, command);

    expect(results).toEqual([]);
  });
});
