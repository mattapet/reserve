/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { userRoleEventHandler } from '../user-role.event-handler';
import { defaultAggregate } from '../../user-role.aggregate';
import { UserRoleEvent, UserRoleEventType } from '../../user-role.event';
import { UserRole } from '../../../../lib/user/user-role.type';
//#endregion

describe('user-role.event-handler', () => {
  it('should return selected role on `changed` event', () => {
    const workspaceId = 'test';
    const aggregateId = '45';
    const changedBy = '49';
    const event: UserRoleEvent = {
      type: UserRoleEventType.changed,
      workspaceId,
      aggregateId,
      timestamp: new Date(0),
      payload: {
        role: UserRole.owner,
        changedBy,
      },
    };

    const result = userRoleEventHandler(defaultAggregate, event);

    expect(result).toEqual(UserRole.owner);
  });

  it('should return owner role on `ownerCreated event', () => {
    const workspaceId = 'test';
    const aggregateId = '45';
    const event: UserRoleEvent = {
      type: UserRoleEventType.ownerCreated,
      workspaceId,
      aggregateId,
      timestamp: new Date(0),
      payload: {},
    };

    const result = userRoleEventHandler(defaultAggregate, event);

    expect(result).toEqual(UserRole.owner);
  });
});
