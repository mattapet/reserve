/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRoleServiceImpl } from '../user-role.service';

import { UserRoleCommandType } from '../../user-role.command';
import { UserRoleRepository } from '../../user-role.repository';
import { UserRoleRepositoryMock } from '../../__mocks__';
import { UserRole } from '../../../../lib/user/user-role.type';
//#endregion

describe('user-role.service', () => {
  let userRoleRepository!: UserRoleRepository;
  let userRoleService!: UserRoleServiceImpl;

  beforeEach(() => {
    userRoleRepository = new UserRoleRepositoryMock();
    userRoleService = new UserRoleServiceImpl(userRoleRepository);
  });

  describe('#dumpEvents()', () => {
    it('should return events from the repository', async () => {
      const workspaceId = 'test';
      const fn = jest.spyOn(userRoleRepository, 'dumpEvents');

      await userRoleService.dumpEvents(workspaceId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId);
    });
 });

  describe('#getById()', () => {
    it('should return aggregates from the repository', async () => {
      const worksapceId = 'test';
      const aggregateId = '45';
      const fn = jest.spyOn(userRoleRepository, 'getById');

      await userRoleService.getById(worksapceId, aggregateId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(worksapceId, `${aggregateId}`);
    });
  });

  describe('#isUser()', () => {
    it('should return true is aggregate is user', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const fn = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.user))
        .mockImplementationOnce(() => Promise.resolve(UserRole.maintainer))
        .mockImplementationOnce(() => Promise.resolve(UserRole.owner));

      const [r1, r2, r3] = await Promise.all([
        userRoleService.isUser(workspaceId, aggregateId),
        userRoleService.isUser(workspaceId, aggregateId),
        userRoleService.isUser(workspaceId, aggregateId),
      ]);

      expect(r1).toBe(true);
      expect(r2).toBe(false);
      expect(r3).toBe(false);
      expect(fn).toHaveBeenCalledTimes(3);
      expect(fn).toHaveBeenNthCalledWith(1, workspaceId, `${aggregateId}`);
      expect(fn).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).toHaveBeenNthCalledWith(3, workspaceId, `${aggregateId}`);
    });
  });

  describe('#isMaintainer()', () => {
    it('should return true is aggregate is maintainer', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const fn = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.user))
        .mockImplementationOnce(() => Promise.resolve(UserRole.maintainer))
        .mockImplementationOnce(() => Promise.resolve(UserRole.owner));

      const [r1, r2, r3] = await Promise.all([
        userRoleService.isMaintainer(workspaceId, aggregateId),
        userRoleService.isMaintainer(workspaceId, aggregateId),
        userRoleService.isMaintainer(workspaceId, aggregateId),
      ]);

      expect(r1).toBe(false);
      expect(r2).toBe(true);
      expect(r3).toBe(false);
      expect(fn).toHaveBeenCalledTimes(3);
      expect(fn).toHaveBeenNthCalledWith(1, workspaceId, `${aggregateId}`);
      expect(fn).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).toHaveBeenNthCalledWith(3, workspaceId, `${aggregateId}`);
    });
  });

  describe('#isOwner()', () => {
    it('should return true is aggregate is owner', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const fn = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.user))
        .mockImplementationOnce(() => Promise.resolve(UserRole.maintainer))
        .mockImplementationOnce(() => Promise.resolve(UserRole.owner));

      const [r1, r2, r3] = await Promise.all([
        userRoleService.isOwner(workspaceId, aggregateId),
        userRoleService.isOwner(workspaceId, aggregateId),
        userRoleService.isOwner(workspaceId, aggregateId),
      ]);

      expect(r1).toBe(false);
      expect(r2).toBe(false);
      expect(r3).toBe(true);
      expect(fn).toHaveBeenCalledTimes(3);
      expect(fn).toHaveBeenNthCalledWith(1, workspaceId, `${aggregateId}`);
      expect(fn).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).toHaveBeenNthCalledWith(3, workspaceId, `${aggregateId}`);
    });
  });

  describe('#createOwner()', () => {
    it('should execute a `createOwner` command', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const fn = jest.spyOn(userRoleRepository, 'execute');

      await userRoleService.createOwner(workspaceId, aggregateId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith({
        type: UserRoleCommandType.createOwner,
        workspaceId,
        aggregateId: `${aggregateId}`,
        payload: {},
      });
    });
  });

  describe('#changeById()', () => {
    it('should execute `changeById` command', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const changedBy = '49';
      const aggregate = UserRole.user;
      const role = UserRole.maintainer;
      const getById = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.owner))
        .mockImplementationOnce(() => Promise.resolve(UserRole.user));
      const fn = jest.spyOn(userRoleRepository, 'execute');

      await userRoleService
        .changeById(workspaceId, role, aggregateId, changedBy);

      expect(getById).toHaveBeenCalledTimes(2);
      expect(getById).toHaveBeenNthCalledWith(1, workspaceId, `${changedBy}`);
      expect(getById).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith({
        type: UserRoleCommandType.change,
        workspaceId,
        aggregateId: `${aggregateId}`,
        payload: {
          role,
          changedBy,
        },
      }, aggregate);
    });

    it('should execute `changeById` command', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const changedBy = '49';
      const aggregate = UserRole.user;
      const role = UserRole.owner;
      const getById = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.owner))
        .mockImplementationOnce(() => Promise.resolve(aggregate));
      const fn = jest.spyOn(userRoleRepository, 'execute');

      await userRoleService
        .changeById(workspaceId, role, aggregateId, changedBy);

      expect(getById).toHaveBeenCalledTimes(2);
      expect(getById).toHaveBeenNthCalledWith(1, workspaceId, `${changedBy}`);
      expect(getById).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith({
        type: UserRoleCommandType.change,
        workspaceId,
        aggregateId: `${aggregateId}`,
        payload: {
          role,
          changedBy,
        },
      }, aggregate);
    });

    it('should execute `changeById` command', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const changedBy = '49';
      const aggregate = UserRole.user;
      const role = UserRole.maintainer;
      const getById = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.maintainer))
        .mockImplementationOnce(() => Promise.resolve(aggregate));
      const fn = jest.spyOn(userRoleRepository, 'execute');

      await userRoleService
        .changeById(workspaceId, role, aggregateId, changedBy);

      expect(getById).toHaveBeenCalledTimes(2);
      expect(getById).toHaveBeenNthCalledWith(1, workspaceId, `${changedBy}`);
      expect(getById).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith({
        type: UserRoleCommandType.change,
        workspaceId,
        aggregateId: `${aggregateId}`,
        payload: {
          role,
          changedBy,
        },
      }, aggregate);
    });

    it('should execute `changeById` command', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const changedBy = '49';
      const aggregate = UserRole.maintainer;
      const role = UserRole.owner;
      const getById = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.owner))
        .mockImplementationOnce(() => Promise.resolve(aggregate));
      const fn = jest.spyOn(userRoleRepository, 'execute');

      await userRoleService
        .changeById(workspaceId, role, aggregateId, changedBy);

      expect(getById).toHaveBeenCalledTimes(2);
      expect(getById).toHaveBeenNthCalledWith(1, workspaceId, `${changedBy}`);
      expect(getById).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith({
        type: UserRoleCommandType.change,
        workspaceId,
        aggregateId: `${aggregateId}`,
        payload: {
          role,
          changedBy,
        },
      }, aggregate);
    });

    it('should throw an error if changed by user', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const changedBy = '49';
      const aggregate = UserRole.maintainer;
      const role = UserRole.owner;
      const getById = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.user))
        .mockImplementationOnce(() => Promise.resolve(aggregate));
      const fn = jest.spyOn(userRoleRepository, 'execute');

      await expect(
        userRoleService
          .changeById(workspaceId, role, aggregateId, changedBy),
      ).rejects.toEqual(
        new Error('Only Maintainers or Owners can change user roles.'),
      );

      expect(getById).toHaveBeenCalledTimes(2);
      expect(getById).toHaveBeenNthCalledWith(1, workspaceId, `${changedBy}`);
      expect(getById).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).not.toHaveBeenCalled();
    });

    it('should throw an error if changing your own permissions', async () => {
      const workspaceId = 'test';
      const aggregateId = '45';
      const changedBy = '45';
      const aggregate = UserRole.maintainer;
      const role = UserRole.owner;
      const getById = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.owner))
        .mockImplementationOnce(() => Promise.resolve(aggregate));
      const fn = jest.spyOn(userRoleRepository, 'execute');

      await expect(
        userRoleService
          .changeById(workspaceId, role, aggregateId, changedBy),
      ).rejects.toEqual(
        new Error('You cannot change your own permissions.'),
      );

      expect(getById).toHaveBeenCalledTimes(2);
      expect(getById).toHaveBeenNthCalledWith(1, workspaceId, `${changedBy}`);
      expect(getById).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).not.toHaveBeenCalled();
    });

    it(
      'should throw an error if changed by someon with lower prividedges',
      async () =>
    {
      const workspaceId = 'test';
      const aggregateId = '45';
      const changedBy = '49';
      const aggregate = UserRole.owner;
      const role = UserRole.maintainer;
      const getById = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.maintainer))
        .mockImplementationOnce(() => Promise.resolve(aggregate));
      const fn = jest.spyOn(userRoleRepository, 'execute');

      await expect(
        userRoleService
          .changeById(workspaceId, role, aggregateId, changedBy),
      ).rejects.toEqual(
        new Error(
          'Cannot change priviledges of '
          + 'users with higher than your priviledges.',
        ),
      );

      expect(getById).toHaveBeenCalledTimes(2);
      expect(getById).toHaveBeenNthCalledWith(1, workspaceId, `${changedBy}`);
      expect(getById).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).not.toHaveBeenCalled();
    });

    it(
      'should throw an error if modifying someone with higher priviledges',
      async () =>
    {
      const workspaceId = 'test';
      const aggregateId = '45';
      const changedBy = '49';
      const aggregate = UserRole.owner;
      const role = UserRole.maintainer;
      const getById = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.maintainer))
        .mockImplementationOnce(() => Promise.resolve(aggregate));
      const fn = jest.spyOn(userRoleRepository, 'execute');

      await expect(
        userRoleService
          .changeById(workspaceId, role, aggregateId, changedBy),
      ).rejects.toEqual(
        new Error(
          'Cannot change priviledges of '
          + 'users with higher than your priviledges.',
        ),
      );

      expect(getById).toHaveBeenCalledTimes(2);
      expect(getById).toHaveBeenNthCalledWith(1, workspaceId, `${changedBy}`);
      expect(getById).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).not.toHaveBeenCalled();
    });

    it(
      'should throw an error if granding higher than your priviledges',
      async () =>
    {
      const workspaceId = 'test';
      const aggregateId = '45';
      const changedBy = '49';
      const aggregate = UserRole.user;
      const role = UserRole.owner;
      const getById = jest.spyOn(userRoleRepository, 'getById')
        .mockImplementationOnce(() => Promise.resolve(UserRole.maintainer))
        .mockImplementationOnce(() => Promise.resolve(aggregate));
      const fn = jest.spyOn(userRoleRepository, 'execute');

      await expect(
        userRoleService
          .changeById(workspaceId, role, aggregateId, changedBy),
      ).rejects.toEqual(
        new Error('Cannot grang priviledges of higher than yours.'),
      );

      expect(getById).toHaveBeenCalledTimes(2);
      expect(getById).toHaveBeenNthCalledWith(1, workspaceId, `${changedBy}`);
      expect(getById).toHaveBeenNthCalledWith(2, workspaceId, `${aggregateId}`);
      expect(fn).not.toHaveBeenCalled();
    });
  });
});
