/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRoleAggregate } from '../user-role.aggregate';
import { UserRoleCommandType } from '../user-role.command';
import { UserRoleEvent } from '../user-role.event';
import { UserRoleService } from '../user-role.service';
import { UserRoleRepository } from '../user-role.repository';

import { UserRole } from '../../../lib/user/user-role.type';
//#endregion

export class UserRoleServiceImpl implements UserRoleService {
  public constructor(
    private readonly repository: UserRoleRepository,
  ) { }

  // - MARK: Queries

  public async dumpEvents(worksapceId: string): Promise<UserRoleEvent[]> {
    return await this.repository.dumpEvents(worksapceId);
  }

  public async getById(
    worksapceId: string,
    userId: string,
  ): Promise<UserRoleAggregate> {
    return await this.repository.getById(worksapceId, `${userId}`);
  }

  public async isUser(
    worksapceId: string,
    userId: string,
  ): Promise<boolean> {
    return UserRole.user === await this.repository
      .getById(worksapceId, `${userId}`);
  }

  public async isMaintainer(
    worksapceId: string,
    userId: string,
  ): Promise<boolean> {
    return UserRole.maintainer === await this.repository
      .getById(worksapceId, `${userId}`);
  }

  public async isOwner(
    worksapceId: string,
    userId: string,
  ): Promise<boolean> {
    return UserRole.owner ===  await this.repository
      .getById(worksapceId, `${userId}`);
  }

  // - MARK: Commands

  public async createOwner(
    workspaceId: string,
    userId: string,
  ): Promise<UserRoleAggregate> {
    return await this.repository.execute({
      type: UserRoleCommandType.createOwner,
      aggregateId: `${userId}`,
      workspaceId,
      payload: {},
    });
  }

  public async changeById(
    workspaceId: string,
    role: UserRole,
    userId: string,
    changedBy: string,
  ): Promise<UserRoleAggregate> {
    const [changedByRole, aggregate] = await Promise.all([
      this.repository.getById(workspaceId, `${changedBy}`),
      this.repository.getById(workspaceId, `${userId}`),
    ]);

    if (changedByRole === UserRole.user) {
      throw new Error('Only Maintainers or Owners can change user roles.');
    }

    if (userId === changedBy) {
      throw new Error('You cannot change your own permissions.');
    }

    if (aggregate === UserRole.owner && changedByRole !== UserRole.owner) {
      throw new Error(
        'Cannot change priviledges of users with higher than your priviledges.',
      );
    }

    if (role === UserRole.owner && changedByRole !== UserRole.owner) {
      throw new Error('Cannot grang priviledges of higher than yours.');
    }

    return await this.repository.execute({
      type: UserRoleCommandType.change,
      aggregateId: userId,
      workspaceId,
      payload: { role, changedBy },
    }, aggregate);
  }
}
