/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRoleAggregate } from '../user-role.aggregate';
import { UserRoleCommand, UserRoleCommandType } from '../user-role.command';
import { UserRoleEvent, UserRoleEventType } from '../user-role.event';
//#endregion

export const userRoleCommandHandler = (
  aggregate: UserRoleAggregate,
  command: UserRoleCommand,
): UserRoleEvent[] => {
  const { workspaceId, aggregateId } = command;
  switch (command.type) {
  case UserRoleCommandType.change:
    if (aggregate === command.payload.role) {
      return [];
    }
    return [{
      type: UserRoleEventType.changed,
      timestamp: new Date(),
      workspaceId,
      aggregateId,
      payload: command.payload,
    }];

  case UserRoleCommandType.createOwner:
    return [{
      type: UserRoleEventType.ownerCreated,
      timestamp: new Date(),
      workspaceId,
      aggregateId,
      payload: {},
    }];
  }
};
