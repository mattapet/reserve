/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRoleAggregate } from '../user-role.aggregate';
import { UserRoleEvent, UserRoleEventType } from '../user-role.event';
import { UserRole } from '../../../lib/user/user-role.type';
//#endregion

export const userRoleEventHandler = (
  aggregate: UserRoleAggregate,
  event: UserRoleEvent,
): UserRoleAggregate => {
  switch (event.type) {
  case UserRoleEventType.changed:
    return event.payload.role;
  case UserRoleEventType.ownerCreated:
    return UserRole.owner;
  }
};
