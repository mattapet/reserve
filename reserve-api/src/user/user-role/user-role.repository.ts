/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRoleCommand } from './user-role.command';
import { UserRoleEvent } from './user-role.event';
import { UserRoleAggregate } from './user-role.aggregate';
import { Repository } from '../../lib/events/repository-base.interface';
//#endregion

export interface UserRoleRepository extends Repository<
  UserRoleCommand,
  UserRoleEvent,
  UserRoleAggregate
> { }
