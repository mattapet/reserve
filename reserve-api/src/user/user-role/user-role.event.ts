/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '../../lib/events/event';
import { UserRole } from '../../lib/user/user-role.type';
//#endregion

export enum UserRoleEventType {
  changed = 'changed',
  ownerCreated = 'owner_created',
}

export interface UserRoleChanged extends Event {
  readonly type: UserRoleEventType.changed;
  readonly payload: {
    readonly role: UserRole;
    readonly changedBy: string;
  };
}

export interface UserRoleOwnerCreated extends Event {
  readonly type: UserRoleEventType.ownerCreated;
  readonly timestamp: Date;
}

export type UserRoleEvent =
    UserRoleChanged
  | UserRoleOwnerCreated;
