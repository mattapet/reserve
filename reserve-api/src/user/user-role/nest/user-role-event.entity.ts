/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity } from 'typeorm';

import { BaseEvent } from '../../../lib/events/base-event.entity';
import { UserRoleEvent as Payload } from '../user-role.event';
//#endregion

@Entity({ name: 'user_role_event' })
export class UserRoleEventEntity extends BaseEvent<Payload> { }
