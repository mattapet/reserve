/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';

import { UserRoleEventRepository } from './user-role-event.repository';
import { UserRoleServiceImpl } from '../impl/user-role.service';
import { userRoleCommandHandler } from '../impl/user-role.command-handler';
import { userRoleEventHandler } from '../impl/user-role.event-handler';
import { defaultAggregate } from '../user-role.aggregate';
import { BaseRepository } from '../../../lib/events/repository-base';
//#endregion

@Injectable()
export class NestUserRoleService extends UserRoleServiceImpl {
  public constructor(
    @InjectConnection()
    connection: Connection,
  ) {
    super(
      new BaseRepository(
        connection.getCustomRepository(UserRoleEventRepository),
        userRoleCommandHandler,
        userRoleEventHandler,
        defaultAggregate,
      ),
    );
  }
}
