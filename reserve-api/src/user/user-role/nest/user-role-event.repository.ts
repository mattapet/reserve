/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository, Repository } from 'typeorm';

import { UserRoleEventEntity } from './user-role-event.entity';
import {
  UserRoleEventRepository as Interface,
} from '../user-role-event.repository';
import { UserRoleEvent } from '../user-role.event';
//#endregion

@EntityRepository(UserRoleEventEntity)
export class UserRoleEventRepository extends Repository<UserRoleEventEntity>
  implements Interface
{
  public async findByRowId(rowId: string): Promise<UserRoleEvent[]> {
    const events = await this.find({
      where: { rowId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async findByRowIdAndAggregateId(
    rowId: string,
    aggregateId: string,
  ): Promise<UserRoleEvent[]> {
    const events = await this.find({
      where: { rowId, aggregateId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async saveEvents(
    ...events: UserRoleEvent[]
  ): Promise<UserRoleEvent[]> {
    const results = await this.manager.transaction(async (trx) => {
      return await trx.save(UserRoleEventEntity, events.map(this.wrap));
    });
    return results.map(this.unwrap);
  }

  public wrap(event: UserRoleEvent): UserRoleEventEntity {
    const { workspaceId, aggregateId, timestamp } = event;
    return new UserRoleEventEntity(workspaceId, aggregateId, timestamp, event);
  }

  public unwrap(entity: UserRoleEventEntity): UserRoleEvent {
    return entity.payload;
  }
}
