/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRoleService as Interface } from '../user-role.service';
//#endregion

export const UserRoleServiceMock = jest.fn<Interface, []>(() => ({
  dumpEvents: jest.fn(),
  getById: jest.fn(),
  isUser: jest.fn(),
  isMaintainer: jest.fn(),
  isOwner: jest.fn(),
  changeById: jest.fn(),
  createOwner: jest.fn(),
}));
