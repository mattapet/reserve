/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRoleAggregate } from './user-role.aggregate';
import { UserRoleEvent } from './user-role.event';

import { UserRole } from '../../lib/user/user-role.type';
//#endregion

export interface UserRoleService {
  // - MARK: Queries

  dumpEvents(worksapceId: string): Promise<UserRoleEvent[]>;

  getById(worksapceId: string, userId: string): Promise<UserRoleAggregate>;

  isUser(worksapceId: string, userId: string): Promise<boolean>;

  isMaintainer(worksapceId: string, userId: string): Promise<boolean>;

  isOwner(worksapceId: string, userId: string): Promise<boolean>;

  // - MARK: Commands

  createOwner(workspaceId: string, userId: string): Promise<UserRoleAggregate>;

  changeById(
    workspaceId: string,
    role: UserRole,
    userId: string,
    changedBy: string,
  ): Promise<UserRoleAggregate>;
}
