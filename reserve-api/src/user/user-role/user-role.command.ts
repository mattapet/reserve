/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '../../lib/events/command';
import { UserRole } from '../../lib/user/user-role.type';
//#endregion

export enum UserRoleCommandType {
  createOwner = 'create_owner',
  change = 'change',
}

export interface UserRoleChange extends Command {
  readonly type: UserRoleCommandType.change;
  readonly payload: {
    readonly role: UserRole;
    readonly changedBy: string;
  };
}

export interface UserRoleCreateOwner extends Command {
  readonly type: UserRoleCommandType.createOwner;
  readonly workspaceId: string;
}

export type UserRoleCommand =
    UserRoleChange
  | UserRoleCreateOwner;
