/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { User } from './user.interface';
import { Profile } from './profile.interface';
//#endregion

export interface UserService {
  getAll(workspaceId: string): Promise<User[]>;

  getById(workspaceId: string, userId: string): Promise<User>;

  updateProfile(
    workspaceId: string,
    userId: string,
    profile: Profile,
  ): Promise<User>;

  deleteById(workspaceId: string, userId: string): Promise<void>;
}
