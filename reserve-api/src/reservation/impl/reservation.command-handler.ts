/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  ReservationCommand,
  ReservationCommandType,
} from '../reservation.command';
import {
  ReservationEvent,
  ResourceAdded,
  ReservationEventType,
} from '../reservation.event';
import { ReservationAggregate } from '../reservation.aggregate';
//#endregion

export function reservationCommandHandler(
  aggregate: ReservationAggregate,
  command: ReservationCommand,
): ReservationEvent[] {
  const { workspaceId, aggregateId } = command;
  switch (command.type) {
  case ReservationCommandType.create:
    const timestamp = new Date();
    return [
      {
        type: ReservationEventType.created,
        timestamp,
        workspaceId,
        aggregateId,
        payload: {
          dateStart: command.payload.dateStart.toISOString(),
          dateEnd: command.payload.dateEnd.toISOString(),
          createdBy: command.payload.createdBy,
        },
      },
      {
        type: ReservationEventType.assigned,
        timestamp,
        workspaceId,
        aggregateId,
        payload: {
          assignedTo: command.payload.userId,
          changedBy: command.payload.createdBy,
        },
      },
      ...command.payload.resources.map(resource => ({
        type: ReservationEventType.resourceAdded,
        timestamp,
        workspaceId,
        aggregateId,
        payload: {
          resource,
          changedBy: command.payload.createdBy,
        },
      } as ResourceAdded)),
    ];

  case ReservationCommandType.assign:
    if (aggregate.userId === command.payload.assignedTo) {
      return [];
    }
    return [{
      type: ReservationEventType.assigned,
      timestamp: new Date(),
      workspaceId,
      aggregateId,
      payload: command.payload,
    }];

  case ReservationCommandType.editDates:
    const { dateStart, dateEnd } = command.payload;
    if (aggregate.dateStart === dateStart && aggregate.dateEnd === dateEnd) {
      return [];
    }
    return [{
        type: ReservationEventType.dateChanged,
        timestamp: new Date(),
        workspaceId,
        aggregateId,
        payload: {
          ...command.payload,
          dateStart: command.payload.dateStart.toISOString(),
          dateEnd: command.payload.dateEnd.toISOString(),
        },
      }];

  case ReservationCommandType.addResource:
    if (aggregate.resourceIds.indexOf(command.payload.resource) >= 0) {
      return [];
    }
    return [{
        type: ReservationEventType.resourceAdded,
        timestamp: new Date(),
        workspaceId,
        aggregateId,
        payload: command.payload,
      }];

  case ReservationCommandType.removeResource:
    if (aggregate.resourceIds.indexOf(command.payload.resource) < 0) {
      return [];
    }
    return [{
        type: ReservationEventType.resourceRemoved,
        timestamp: new Date(),
        workspaceId,
        aggregateId,
        payload: command.payload,
      }];
  }
}
