/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { reservationEventHandler } from '../reservation.event-handler';
import { defaultAggregate } from '../../reservation.aggregate';
import { ReservationEventType } from '../../reservation.event';
//#endregion

describe('reservation.event-handler', () => {
  const workspaceId = 'test';
  const aggregateId = '49';
  const dateStart = new Date(100);
  const dateEnd = new Date(200);
  const timestamp = new Date(0);

  const makeEventCreator = <T extends ReservationEventType>(
    type: T,
  ) =>
    <Value extends {}>(value: Value) => ({
      type,
      workspaceId,
      aggregateId,
      timestamp,
      payload: value,
    });

  it('should set all values from a new reservation', () => {
    const event = makeEventCreator(ReservationEventType.created)({
      dateStart: dateStart.toISOString(),
      dateEnd: dateEnd.toISOString(),
      createdBy: '40',
    });

    const aggregate = reservationEventHandler(defaultAggregate, event);

    expect(aggregate.id).toEqual(aggregateId);
    expect(aggregate.dateStart).toEqual(dateStart);
    expect(aggregate.dateEnd).toEqual(dateEnd);
  });

  it('should set date range', () => {
    const event = makeEventCreator(ReservationEventType.dateChanged)({
      dateStart: dateStart.toISOString(),
      dateEnd: dateEnd.toISOString(),
      changedBy: '40',
    });

    const aggregate = reservationEventHandler(defaultAggregate, event);
    expect(aggregate.dateStart).toEqual(dateStart);
    expect(aggregate.dateEnd).toEqual(dateEnd);
  });

  it('should set assigned user', () => {
    const userId = '22';
    const event = makeEventCreator(ReservationEventType.assigned)({
      assignedTo: userId,
      changedBy: '40',
    });

    const aggregate = reservationEventHandler(defaultAggregate, event);
    expect(aggregate.userId).toEqual(userId);
  });

  it('add a resource to the reservation', () => {
    const resource = 94;
    const event = makeEventCreator(ReservationEventType.resourceAdded)({
      resource,
      changedBy: '40',
    });

    const aggregate = reservationEventHandler(defaultAggregate, event);
    expect(aggregate.resourceIds).toEqual([resource]);
  });

  it('add a resource to the reservation', () => {
    const resource = 94;
    const event = makeEventCreator(ReservationEventType.resourceAdded)({
      resource,
      changedBy: '40',
    });

    const aggregate = reservationEventHandler({
      ...defaultAggregate,
      resourceIds: [1, 2],
    }, event);
    expect(aggregate.resourceIds).toEqual([1, 2, resource]);
  });

  it('remove a resource to the reservation', () => {
    const resource = 94;
    const event = makeEventCreator(ReservationEventType.resourceRemoved)({
      resource,
      changedBy: '40',
    });

    const aggregate = reservationEventHandler({
      ...defaultAggregate,
      resourceIds: [resource],
    }, event);

    expect(aggregate.resourceIds).toEqual([]);
  });

  it('remove a resource to the reservation', () => {
    const resource = 94;
    const event = makeEventCreator(ReservationEventType.resourceRemoved)({
      resource,
      changedBy: '40',
    });

    const aggregate = reservationEventHandler({
      ...defaultAggregate,
      resourceIds: [1, resource, 2],
    }, event);

    expect(aggregate.resourceIds).toEqual([1, 2]);
  });
});
