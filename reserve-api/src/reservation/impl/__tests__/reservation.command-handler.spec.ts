/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { reservationCommandHandler } from '../reservation.command-handler';
import { ReservationCommandType } from '../../reservation.command';
import { defaultAggregate } from '../../reservation.aggregate';
import { ReservationEventType } from '../../reservation.event';
//#endregion

describe('reservation.command-handler', () => {
  const workspaceId = 'test';
  const aggregateId = '49';

  const makeCommandCreator = <T extends ReservationCommandType>(
    type: T,
  ) =>
    <Value extends {}>(value: Value) => ({
      type,
      aggregateId,
      workspaceId,
      payload: value,
    });

  describe('create command', () => {
    const dateStart = new Date(0);
    const dateEnd = new Date(0);
    const createdBy = '44';
    const makeCommand = <Value extends {}>(value: Value) =>
      makeCommandCreator(ReservationCommandType.create)({
        ...value,
        createdBy,
      });

    it(
      'should produce `created`, `assigned` and  addedResource` events',
      () =>
    {
      const userId = '40';
      const resources = [1, 2, 3];
      const command = makeCommand({
        dateStart,
        dateEnd,
        resources,
        userId,
      });

      const [created, assigned, ...resourcesAdded] =
        reservationCommandHandler(defaultAggregate, command);

      expect(created.type).toEqual(ReservationEventType.created);
      expect(created.payload).toEqual({
        dateStart: dateStart.toISOString(),
        dateEnd: dateEnd.toISOString(),
        createdBy,
      });
      expect(assigned.type).toEqual(ReservationEventType.assigned);
      expect(assigned.payload).toEqual({
        assignedTo: userId,
        changedBy: createdBy,
      });
      resourcesAdded.forEach((resource, idx) => {
        expect(resource.type).toEqual(ReservationEventType.resourceAdded);
        expect(resource.payload).toEqual({
          resource: resources[idx],
          changedBy: createdBy,
        });
      });
    });
  });

  describe('assign command', () => {
    const changedBy = '44';
    const makeCommand = <Value extends {}>(value: Value) =>
      makeCommandCreator(ReservationCommandType.assign)({
        ...value,
        changedBy,
      });

    it('should produce an `assigned` event', () => {
      const assignedTo = '40';
      const command = makeCommand({ assignedTo });

      const [result] = reservationCommandHandler(defaultAggregate, command);

      expect(result.type).toEqual(ReservationEventType.assigned);
      expect(result.payload).toEqual({ changedBy, assignedTo });
    });

    it('should not produce any events', () => {
      const assignedTo = '40';
      const command = makeCommand({ assignedTo });

      const results = reservationCommandHandler({
        ...defaultAggregate,
        userId: assignedTo,
      }, command);

      expect(results).toEqual([]);
    });
  });

  describe('editDates command', () => {
    const changedBy = '44';
    const dateStart = new Date(100);
    const dateEnd = new Date(200);

    const makeCommand = <Value extends {}>(value: Value) =>
      makeCommandCreator(ReservationCommandType.editDates)({
        ...value,
        changedBy,
      });

    it('should produce a `dateChanged` event', () => {
      const command = makeCommand({ dateStart, dateEnd });

      const [result] = reservationCommandHandler(defaultAggregate, command);

      expect(result.type).toEqual(ReservationEventType.dateChanged);
      expect(result.payload).toEqual({
        dateStart: dateStart.toISOString(),
        dateEnd: dateEnd.toISOString(),
        changedBy,
      });
    });

    it('should not produce any events', () => {
      const command = makeCommand({ dateStart, dateEnd });

      const results = reservationCommandHandler({
        ...defaultAggregate,
        dateStart,
        dateEnd,
      }, command);

      expect(results).toEqual([]);
    });
  });

  describe('addResouce command', () => {
    const changedBy = '40';
    const resource = 1;
    const makeCommand = <Value extends {}>(value: Value) =>
      makeCommandCreator(ReservationCommandType.addResource)({
        ...value,
        changedBy,
      });

    it('should produce a `resourceAdded` event', () => {
      const command = makeCommand({ resource });

      const [result] = reservationCommandHandler(defaultAggregate, command);

      expect(result.type).toEqual(ReservationEventType.resourceAdded);
      expect(result.payload).toEqual(command.payload);
    });

    it('should produce no events', () => {
      const command = makeCommand({ resource });

      const results = reservationCommandHandler({
        ...defaultAggregate,
        resourceIds: [resource],
      }, command);

      expect(results).toEqual([]);
    });
  });

  describe('`removeResource` command', () => {
    const changedBy = '40';
    const resource = 1;
    const makeCommand = <Value extends {}>(value: Value) =>
      makeCommandCreator(ReservationCommandType.removeResource)({
        ...value,
        changedBy,
      });

    it('should produce a `resourceRemoved` event', () => {
      const command = makeCommand({ resource });

      const [result] = reservationCommandHandler({
        ...defaultAggregate,
        resourceIds: [resource],
      }, command);

      expect(result.type).toEqual(ReservationEventType.resourceRemoved);
      expect(result.payload).toEqual(command.payload);
    });

    it('should produce no events', () => {
      const command = makeCommand({ resource });

      const results = reservationCommandHandler(defaultAggregate, command);

      expect(results).toEqual([]);
    });
  });
});
