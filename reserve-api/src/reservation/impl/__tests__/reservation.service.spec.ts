/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationServiceImpl } from '../reservation.service';

import { defaultAggregate } from '../../reservation.aggregate';
import { ReservationCommandType } from '../../reservation.command';
import { ReservationRepository } from '../../reservation.repository';
import { ReservationRepositoryMock } from '../../__mocks__';
import { UserRoleService } from '../../../user/user-role';
import { UserRoleServiceMock } from '../../../user/user-role/__mocks__';
//#endregion

describe('reservation.service', () => {
  let reservationRepository!: ReservationRepository;
  let userRoleService!: UserRoleService;
  let reservationService!: ReservationServiceImpl;

  beforeEach(() => {
    reservationRepository = new ReservationRepositoryMock();
    userRoleService = new UserRoleServiceMock();
    reservationService =
      new ReservationServiceImpl(reservationRepository, userRoleService);
  });

  describe('#dumpEvents()', () => {
    it('should get all events from repository', async () => {
      const workspaceId = 'test';
      const fn = jest.spyOn(reservationRepository, 'dumpEvents');

      await reservationService.dumpEvents(workspaceId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId);
    });
  });

  describe('#getAll()', () => {
    it('should get all aggregates from repository', async () => {
      const workspaceId = 'test';
      const fn = jest.spyOn(reservationRepository, 'getAll');

      await reservationService.getAll(workspaceId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId);
    });
  });

  describe('#getById()', () => {
    it('should get aggregates with given id from repository', async () => {
      const workspaceId = 'test';
      const aggregateId = '49';
      const fn = jest.spyOn(reservationRepository, 'getById');

      await reservationService.getById(workspaceId, aggregateId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId, aggregateId);
    });
  });

  describe('#create()', () => {
    const workspaceId = 'test';
    const dateStart = new Date(Date.now() + 100_000);
    const dateEnd = new Date(Date.now() + 200_000);
    const resources = [1, 2, 3];
    const userId = '49';
    const createdBy = '44';

    it('should execute a `create` command', async () => {
      let uuid: string | undefined;
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(reservationRepository, 'execute')
        .mockImplementation((command) => {
          uuid = command.aggregateId;
          return Promise.resolve(defaultAggregate);
        });

      await reservationService.create(
        workspaceId,
        dateStart,
        dateEnd,
        resources,
        userId,
        createdBy,
      );

      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, createdBy);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: ReservationCommandType.create,
        workspaceId,
        aggregateId: uuid,
        payload: {
          dateStart,
          dateEnd,
          userId,
          createdBy,
          resources,
        },
      });
    });

    it('should execute a `create` command', async () => {
      let uuid: string | undefined;
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(true));
      const execute = jest.spyOn(reservationRepository, 'execute')
        .mockImplementation((command) => {
          uuid = command.aggregateId;
          return Promise.resolve(defaultAggregate);
        });

      await reservationService.create(
        workspaceId,
        dateStart,
        dateEnd,
        resources,
        userId,
        userId,
      );

      expect(isUser).not.toHaveBeenCalled();
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: ReservationCommandType.create,
        workspaceId,
        aggregateId: uuid,
        payload: {
          dateStart,
          dateEnd,
          userId,
          createdBy: userId,
          resources,
        },
      });
    });

    it(
      'should throw an error when trying to assing to someone else',
      async () =>
    {
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(true));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await expect(
        reservationService.create(
          workspaceId,
          dateStart,
          dateEnd,
          resources,
          userId,
          createdBy,
        ),
      ).rejects.toEqual(
        new Error(
          'Only Maintainerners or Owners can assign else\'s '
          + 'reservation to someone else.',
        ),
      );

      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, createdBy);
      expect(execute).not.toHaveBeenCalled();
    });

    it(
      'should throw an error when reservation ends before it starts',
      async () =>
    {
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await expect(
        reservationService.create(
          workspaceId,
          dateEnd,
          dateStart,
          resources,
          userId,
          createdBy,
        ),
      ).rejects.toEqual(
        new Error('Reservation cannot end before it starts.'),
      );

      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, createdBy);
      expect(execute).not.toHaveBeenCalled();
    });

    it(
      'should throw an error when reservation start in past',
      async () =>
    {
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await expect(
        reservationService.create(
          workspaceId,
          new Date(0),
          dateEnd,
          resources,
          userId,
          createdBy,
        ),
      ).rejects.toEqual(
        new Error('Reservation cannot start in the past.'),
      );

      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, createdBy);
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#editDates()', () => {
    const workspaceId = 'test';
    const aggregateId = '49';
    const dateStart = new Date(Date.now() + 100_000);
    const dateEnd = new Date(Date.now() + 200_000);
    const userId = '49';
    const changedBy = '44';

    it('should execute a `editDates` command', async () => {
      const aggregate = { ...defaultAggregate, userId };
      const getById = jest.spyOn(reservationRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await reservationService.editDates(
        workspaceId,
        aggregateId,
        dateStart,
        dateEnd,
        changedBy,
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: ReservationCommandType.editDates,
        workspaceId,
        aggregateId,
        payload: {
          dateStart,
          dateEnd,
          changedBy,
        },
      }, aggregate);
    });

    it(
      'should throw an error when trying to change someone else\'s reservation',
      async () =>
    {
      const aggregate = { ...defaultAggregate, userId };
      const getById = jest.spyOn(reservationRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(true));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await expect(
        reservationService.editDates(
          workspaceId,
          aggregateId,
          dateStart,
          dateEnd,
          changedBy,
        ),
      ).rejects.toEqual(
        new Error(
          'Only Maintainerners or Owners can edit someone else\'s reservations',
        ),
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).not.toHaveBeenCalled();
    });

    it(
      'should throw an error when reservation ends before it starts',
      async () =>
    {
      const aggregate = { ...defaultAggregate, userId };
      const getById = jest.spyOn(reservationRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await expect(
        reservationService.editDates(
          workspaceId,
          aggregateId,
          dateEnd,
          dateStart,
          changedBy,
        ),
      ).rejects.toEqual(
        new Error(
          'Reservation cannot end before it starts.',
        ),
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).not.toHaveBeenCalled();
    });

    it(
      'should throw an error when reservation start in past',
      async () =>
    {
      const aggregate = { ...defaultAggregate, userId };
      const getById = jest.spyOn(reservationRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await expect(
        reservationService.editDates(
          workspaceId,
          aggregateId,
          new Date(0),
          dateEnd,
          changedBy,
        ),
      ).rejects.toEqual(
        new Error('Reservation cannot start in the past.'),
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#assign()', () => {
    const workspaceId = 'test';
    const aggregateId = '49';
    const userId = '49';
    const changedBy = '44';

    it('should execute a `assign` command', async () => {
      const aggregate = { ...defaultAggregate, userId };
      const getById = jest.spyOn(reservationRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await reservationService.assign(
        workspaceId,
        aggregateId,
        userId,
        changedBy,
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: ReservationCommandType.assign,
        workspaceId,
        aggregateId,
        payload: {
          assignedTo: userId,
          changedBy,
        },
      }, aggregate);
    });

    it(
      'should throw an error when trying to reassign reservation',
      async () =>
    {
      const aggregate = { ...defaultAggregate, userId };
      const getById = jest.spyOn(reservationRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(true));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await expect(
        reservationService.assign(
          workspaceId,
          aggregateId,
          userId,
          changedBy,
        ),
      ).rejects.toEqual(
        new Error('Only Maintainers or Owners can reassign reservations'),
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#addResource()', () => {
    const workspaceId = 'test';
    const aggregateId = '49';
    const resource = 1;
    const userId = '49';
    const changedBy = '44';

    it('should execute a `addResource` command', async () => {
      const aggregate = { ...defaultAggregate, userId };
      const getById = jest.spyOn(reservationRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await reservationService.addResource(
        workspaceId,
        aggregateId,
        resource,
        changedBy,
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: ReservationCommandType.addResource,
        workspaceId,
        aggregateId,
        payload: {
          resource,
          changedBy,
        },
      }, aggregate);
    });

    it(
      'should throw an error when trying to change someone else\'s reservation',
      async () =>
    {
      const aggregate = { ...defaultAggregate, userId };
      const getById = jest.spyOn(reservationRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(true));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await expect(
        reservationService.addResource(
          workspaceId,
          aggregateId,
          resource,
          changedBy,
        ),
      ).rejects.toEqual(
        new Error(
          'Only Maintainerners or Owners can edit someone else\'s reservations',
        ),
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#removeResource()', () => {
    const workspaceId = 'test';
    const aggregateId = '49';
    const resource = 1;
    const userId = '49';
    const changedBy = '44';

    it('should execute a `removeResource` command', async () => {
      const aggregate = { ...defaultAggregate, userId };
      const getById = jest.spyOn(reservationRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await reservationService.removeResource(
        workspaceId,
        aggregateId,
        resource,
        changedBy,
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: ReservationCommandType.removeResource,
        workspaceId,
        aggregateId,
        payload: {
          resource,
          changedBy,
        },
      }, aggregate);
    });

    it(
      'should throw an error when trying to change someone else\'s reservation',
      async () =>
    {
      const aggregate = { ...defaultAggregate, userId };
      const getById = jest.spyOn(reservationRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(true));
      const execute = jest.spyOn(reservationRepository, 'execute');

      await expect(
        reservationService.removeResource(
          workspaceId,
          aggregateId,
          resource,
          changedBy,
        ),
      ).rejects.toEqual(
        new Error(
          'Only Maintainerners or Owners can edit someone else\'s reservations',
        ),
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).not.toHaveBeenCalled();
    });
  });
});
