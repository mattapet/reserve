/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { reservationCommandHandler } from './reservation.command-handler';
import { reservationEventHandler } from './reservation.event-handler';
import {
  ReservationAggregate,
  defaultAggregate,
} from '../reservation.aggregate';
import { ReservationCommand } from '../reservation.command';
import { ReservationEvent } from '../reservation.event';
import { ReservationRepository } from '../reservation.repository';
import {
  BaseRepository,
  BaseEventRepository,
} from '../../lib/events/repository-base';
//#endregion

export class ReservationRepositoryImpl
  extends BaseRepository<
    ReservationCommand,
    ReservationEvent,
    ReservationAggregate
  >
  implements ReservationRepository
{
  public constructor(
    eventRepository: BaseEventRepository<ReservationEvent>,
  ) {
    super(
      eventRepository,
      reservationCommandHandler,
      reservationEventHandler,
      defaultAggregate,
    );
  }
}
