/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import uuid from 'uuid/v4';

import { ReservationAggregate } from '../reservation.aggregate';
import { ReservationRepository } from '../reservation.repository';
import { ReservationCommandType } from '../reservation.command';
import { ReservationEvent } from '../reservation.event';
import { ReservationService } from '../reservation.service';

import { UserRoleService } from '../../user/user-role';
//#endregion

export class ReservationServiceImpl implements ReservationService {
  public constructor(
    private readonly repository: ReservationRepository,
    private readonly userRole: UserRoleService,
  ) { }

  // - MARK: Queries

  public async dumpEvents(workspaceId: string): Promise<ReservationEvent[]> {
    return await this.repository.dumpEvents(workspaceId);
  }

  public async getAll(workspaceId: string): Promise<ReservationAggregate[]> {
    return await this.repository.getAll(workspaceId);
  }

  public async getById(
    workspaceId: string,
    reservationId: string,
  ): Promise<ReservationAggregate> {
    return await this.repository.getById(workspaceId, reservationId);
  }

  // - MARK: Commands

  public async create(
    workspaceId: string,
    dateStart: Date,
    dateEnd: Date,
    resources: number[],
    userId: string,
    createdBy: string,
  ): Promise<ReservationAggregate> {
    if (
      userId !== createdBy &&
      await this.userRole.isUser(workspaceId, createdBy)
    ) {
      throw new Error(
        'Only Maintainerners or Owners can assign else\'s '
        + 'reservation to someone else.',
      );
    }
    if (dateStart > dateEnd) {
      throw new Error('Reservation cannot end before it starts.');
    }
    if (+dateStart < Date.now()) {
      throw new Error('Reservation cannot start in the past.');
    }
    // Check max date range
    // Check for max reservations

    return await this.repository.execute({
      type: ReservationCommandType.create,
      workspaceId,
      aggregateId: uuid(),
      payload: {
        dateStart,
        dateEnd,
        userId,
        createdBy,
        resources,
      },
    });
  }

  public async editDates(
    workspaceId: string,
    reservationId: string,
    dateStart: Date,
    dateEnd: Date,
    changedBy: string,
  ): Promise<ReservationAggregate> {
    const aggregate = await this.getById(workspaceId, reservationId);
    if (
      aggregate.userId !== changedBy &&
      await this.userRole.isUser(workspaceId, changedBy)
    ) {
      throw new Error(
        'Only Maintainerners or Owners can edit someone else\'s reservations',
      );
    }
    // Check if its active

    if (dateStart > dateEnd) {
      throw new Error('Reservation cannot end before it starts.');
    }
    if (+dateStart < Date.now()) {
      throw new Error('Reservation cannot start in the past.');
    }

    // Check max date range
    // Check for max reservations

    return await this.repository.execute({
      type: ReservationCommandType.editDates,
      workspaceId,
      aggregateId: reservationId,
      payload: {
        dateStart,
        dateEnd,
        changedBy,
      },
    }, aggregate);
  }

  public async assign(
    workspaceId: string,
    reservationId: string,
    assignedTo: string,
    changedBy: string,
  ): Promise<ReservationAggregate> {
    const aggregate = await this.getById(workspaceId, reservationId);
    if (await this.userRole.isUser(workspaceId, changedBy)) {
      throw new Error('Only Maintainers or Owners can reassign reservations');
    }

    // Check if active

    return await this.repository.execute({
      type: ReservationCommandType.assign,
      workspaceId,
      aggregateId: reservationId,
      payload: {
        assignedTo,
        changedBy,
      },
    }, aggregate);
  }

  public async addResource(
    workspaceId: string,
    reservationId: string,
    resource: number,
    changedBy: string,
  ): Promise<ReservationAggregate> {
    const aggregate = await this.getById(workspaceId, reservationId);
    if (
      aggregate.userId !== changedBy &&
      await this.userRole.isUser(workspaceId, changedBy)
    ) {
      throw new Error(
        'Only Maintainerners or Owners can edit someone else\'s reservations',
      );
    }

    // Check if active
    // Check if available

    return await this.repository.execute({
      type: ReservationCommandType.addResource,
      workspaceId,
      aggregateId: reservationId,
      payload: {
        resource,
        changedBy,
      },
    }, aggregate);
  }

  public async removeResource(
    workspaceId: string,
    reservationId: string,
    resource: number,
    changedBy: string,
  ): Promise<ReservationAggregate> {
    const aggregate = await this.getById(workspaceId, reservationId);
    if (
      aggregate.userId !== changedBy &&
      await this.userRole.isUser(workspaceId, changedBy)
    ) {
      throw new Error(
        'Only Maintainerners or Owners can edit someone else\'s reservations',
      );
    }

    // Check if active

    return await this.repository.execute({
      type: ReservationCommandType.removeResource,
      workspaceId,
      aggregateId: reservationId,
      payload: {
        resource,
        changedBy,
      },
    }, aggregate);
  }
}
