/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  ReservationEvent,
  ReservationEventType,
} from '../reservation.event';
import { ReservationAggregate } from '../reservation.aggregate';
//#endregion

export function reservationEventHandler(
  aggregate: ReservationAggregate,
  event: ReservationEvent,
): ReservationAggregate {
  switch (event.type) {
  case ReservationEventType.created:
    return {
      ...aggregate,
      id: event.aggregateId,
      dateStart: new Date(event.payload.dateStart),
      dateEnd: new Date(event.payload.dateEnd),
    };
  case ReservationEventType.dateChanged:
    return {
      ...aggregate,
      dateStart: new Date(event.payload.dateStart),
      dateEnd: new Date(event.payload.dateEnd),
    };

  case ReservationEventType.assigned:
    return {
      ...aggregate,
      userId: event.payload.assignedTo,
    };

  case ReservationEventType.resourceAdded:
    return {
      ...aggregate,
      resourceIds: [
        ...aggregate.resourceIds,
        event.payload.resource,
      ],
    };

  case ReservationEventType.resourceRemoved:
    return {
      ...aggregate,
      resourceIds: aggregate.resourceIds
        .filter(resource => resource !== event.payload.resource),
    };
  }
}
