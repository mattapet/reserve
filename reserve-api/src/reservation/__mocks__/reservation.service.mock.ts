/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationService } from '../reservation.service';
//#endregion

export const ReservationServiceMock = jest.fn<ReservationService, []>(() => ({
  dumpEvents: jest.fn(),
  getAll: jest.fn(),
  getById: jest.fn(),
  create: jest.fn(),
  editDates: jest.fn(),
  assign: jest.fn(),
  addResource: jest.fn(),
  removeResource: jest.fn(),
}));
