/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { StateRepository as Interface } from '../state.repository';
//#endregion

export const StateRepositoryMock = jest.fn<Interface, []>(() => ({
  dumpEvents: jest.fn(),
  getAll: jest.fn(),
  getById: jest.fn(),
  execute: jest.fn(),
}));
