/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { StateService } from '../state.service';
//#endregion

export const StateServiceMock = jest.fn<StateService, []>(() => ({
  dumpEvents: jest.fn(),
  getById: jest.fn(),
  confirm: jest.fn(),
  cancel: jest.fn(),
  reject: jest.fn(),
  complete: jest.fn(),
}));
