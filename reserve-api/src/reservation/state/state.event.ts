/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '../../lib/events/event';
//#endregion

export enum StateEventType {
  confirmed = 'confirmed',
  canceled = 'canceled',
  rejected = 'rejected',
  completed = 'completed',
}

interface Confirmed extends Event {
  readonly type: StateEventType.confirmed;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface Canceled extends Event {
  readonly type: StateEventType.canceled;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface Rejected extends Event {
  readonly type: StateEventType.rejected;
  readonly payload: {
    readonly reason: string;
    readonly changedBy: string;
  };
}

interface Completed extends Event {
  readonly type: StateEventType.completed;
}

export type StateEvent =
  | Confirmed
  | Canceled
  | Rejected
  | Completed;
