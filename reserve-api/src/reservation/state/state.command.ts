/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '../../lib/events/command';
//#endregion

export enum StateCommandType {
  confirm = 'confirm',
  cancel = 'cancel',
  reject = 'reject',
  complete = 'complete',
}

interface Confirm extends Command {
  readonly type: StateCommandType.confirm;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface Cancel extends Command {
  readonly type: StateCommandType.cancel;
  readonly payload: {
    readonly changedBy: string;
  };
}

interface Reject extends Command {
  readonly type: StateCommandType.reject;
  readonly payload: {
    readonly reason: string;
    readonly changedBy: string;
  };
}

interface Complete extends Command {
  readonly type: StateCommandType.complete;
}

export type StateCommand =
  | Confirm
  | Cancel
  | Reject
  | Complete;
