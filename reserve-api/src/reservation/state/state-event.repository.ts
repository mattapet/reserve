/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { BaseEventRepository } from '../../lib/events/repository-base';
import { StateEvent } from './state.event';
//#endregion

export interface StateEventRepository
  extends BaseEventRepository<StateEvent> { }
