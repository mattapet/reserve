/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  ReservationState,
} from '../../../lib/reservation/reservation-state.type';
import { StateCommand, StateCommandType } from '../state.command';
import { StateEvent, StateEventType } from '../state.event';
import { StateAggregate } from '../state.aggregate';
//#endregion

export function stateCommandHandler(
  aggregate: StateAggregate,
  command: StateCommand,
): StateEvent[] {
  const { workspaceId, aggregateId } = command;
  switch (command.type) {
  case StateCommandType.confirm:
    switch (aggregate) {
    case ReservationState.confirmed:
      return [];
    case ReservationState.requested:
    case ReservationState.canceled:
    case ReservationState.rejected:
      return [{
        type: StateEventType.confirmed,
        workspaceId,
        aggregateId,
        timestamp: new Date(),
        payload: command.payload,
      }];
    default:
      throw new Error('Invalid state change');
    }

  case StateCommandType.cancel:
    switch (aggregate) {
    case ReservationState.canceled:
      return [];
    case ReservationState.confirmed:
      return [{
        type: StateEventType.canceled,
        workspaceId,
        aggregateId,
        timestamp: new Date(),
        payload: command.payload,
      }];
    default:
      throw new Error('Invalid state change');
    }

  case StateCommandType.reject:
    switch (aggregate) {
    case ReservationState.rejected:
      return [];
    case ReservationState.requested:
      return [{
        type: StateEventType.rejected,
        workspaceId,
        aggregateId,
        timestamp: new Date(),
        payload: command.payload,
      }];
    default:
      throw new Error('Invalid state change');
    }

  case StateCommandType.complete:
    switch (aggregate) {
    case ReservationState.completed:
      return [];
    case ReservationState.confirmed:
      return [{
        type: StateEventType.completed,
        workspaceId,
        aggregateId,
        timestamp: new Date(),
        payload: {},
      }];
    default:
      throw new Error('Invalid state change');
    }
  }
}
