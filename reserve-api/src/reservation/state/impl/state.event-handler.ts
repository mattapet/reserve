/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  ReservationState,
} from '../../../lib/reservation/reservation-state.type';
import { StateEvent, StateEventType } from '../state.event';
import { StateAggregate } from '../state.aggregate';
//#endregion

export function stateEventHandler(
  aggregate: StateAggregate,
  event: StateEvent,
): StateAggregate {
  switch (event.type) {
  case StateEventType.confirmed:
    return ReservationState.confirmed;
  case StateEventType.canceled:
    return ReservationState.canceled;
  case StateEventType.rejected:
    return ReservationState.rejected;
  case StateEventType.completed:
    return ReservationState.completed;
  }
}
