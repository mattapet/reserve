/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { StateAggregate } from '../state.aggregate';
import { StateRepository } from '../state.repository';
import { StateCommandType } from '../state.command';
import { StateEvent } from '../state.event';
import { StateService } from '../state.service';
import { ReservationService } from '../../reservation.service';
import { UserRoleService } from '../../../user/user-role';
import { AvailabilityService } from '../../availability';
import { zip } from '../../../lib/zip';
//#endregion

export class StateServiceImpl implements StateService {
  public constructor(
    private readonly repository: StateRepository,
    private readonly reservation: ReservationService,
    private readonly userRole: UserRoleService,
    private readonly availability: AvailabilityService,
  ) { }

  // - MARK: Queries

  public async dumpEvents(workspaceId: string): Promise<StateEvent[]> {
    return await this.repository.dumpEvents(workspaceId);
  }

  public async getById(
    workspaceId: string,
    reservationId: string,
  ): Promise<StateAggregate> {
    return await this.repository.getById(workspaceId, reservationId);
  }

  // - MARK: Comamnds

  public async confirm(
    workspaceId: string,
    reservationId: string,
    changedBy: string,
  ): Promise<StateAggregate> {
    const aggregate = await this.getById(workspaceId, reservationId);
    if (await this.userRole.isUser(workspaceId, changedBy)) {
      throw new Error('Only Maintainers or Owners can confirm reservations.');
    }

    const { dateStart, dateEnd, resourceIds } = await this.reservation
      .getById(workspaceId, reservationId);

    const availabilities = await Promise.all(
      resourceIds.map(resourceId => this.availability
        .isAvailable(workspaceId, dateStart, dateEnd, resourceId)),
    );

    const unavailable = zip(availabilities, resourceIds)
      .filter(([availability]) => !availability)
      .map(([, resourceId]) => resourceId);

    if (unavailable.length) {
      throw new Error(
        `Resources with ids ${unavailable.join(', ')} are already reserved.`,
      );
    }

    return await this.repository.execute({
      type: StateCommandType.confirm,
      workspaceId,
      aggregateId: reservationId,
      payload: {
        changedBy,
      },
    }, aggregate);
  }

  public async cancel(
    workspaceId: string,
    reservationId: string,
    changedBy: string,
  ): Promise<StateAggregate> {
    const aggregate = await this.getById(workspaceId, reservationId);
    const { userId } = await this.reservation
      .getById(workspaceId, reservationId);

    if (
      userId !== changedBy &&
      await this.userRole.isUser(workspaceId, changedBy)
    ) {
      throw new Error(
        'Only Maintainers or Owners can modify someone else\'s reservations.',
      );
    }

    return await this.repository.execute({
      type: StateCommandType.cancel,
      workspaceId,
      aggregateId: reservationId,
      payload: {
        changedBy,
      },
    }, aggregate);
  }

  public async reject(
    workspaceId: string,
    reservationId: string,
    changedBy: string,
    reason: string,
  ): Promise<StateAggregate> {
      const aggregate = await this.getById(workspaceId, reservationId);
      if (await this.userRole.isUser(workspaceId, changedBy)) {
        throw new Error('Only Maintainers or Owners can reject reservations.');
      }

      const { dateStart } = await this.reservation
        .getById(workspaceId, reservationId);

      if (dateStart < new Date()) {
        throw new Error('Cannot reject reservation that starts in past.');
      }

      return await this.repository.execute({
        type: StateCommandType.reject,
        workspaceId,
        aggregateId: reservationId,
        payload: {
          reason,
          changedBy,
        },
      }, aggregate);
  }

  public async complete(
    workspaceId: string,
    reservationId: string,
  ): Promise<StateAggregate> {
      const aggregate = await this.getById(workspaceId, reservationId);

      const { dateEnd } = await this.reservation
        .getById(workspaceId, reservationId);

      if (dateEnd >= new Date()) {
        throw new Error('Cannot complete reservation has not ended yet.');
      }

      return await this.repository.execute({
        type: StateCommandType.complete,
        workspaceId,
        aggregateId: reservationId,
        payload: { },
      }, aggregate);
  }
}
