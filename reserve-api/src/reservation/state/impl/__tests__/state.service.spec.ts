/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { StateServiceImpl } from '../state.service';

import { StateRepository } from '../../state.repository';
import { StateRepositoryMock } from '../../__mocks__';
import { ReservationService } from '../../../reservation.service';
import { ReservationServiceMock } from '../../../__mocks__';
import { UserRoleService } from '../../../../user/user-role';
import { UserRoleServiceMock } from '../../../../user/user-role/__mocks__';
import { AvailabilityService } from '../../../availability';
import { AvailabilityServiceMock } from '../../../availability/__mocks__';
import {
  ReservationState,
} from '../../../../lib/reservation/reservation-state.type';
import { StateCommandType } from '../../state.command';
//#endregion

describe('state.service', () => {
  let stateRepository!: StateRepository;
  let reservationService!: ReservationService;
  let userRoleService!: UserRoleService;
  let availabilityService!: AvailabilityService;
  let stateService!: StateServiceImpl;

  beforeEach(() => {
    stateRepository = new StateRepositoryMock();
    reservationService = new ReservationServiceMock();
    userRoleService = new UserRoleServiceMock();
    availabilityService = new AvailabilityServiceMock();
    stateService = new StateServiceImpl(
      stateRepository,
      reservationService,
      userRoleService,
      availabilityService,
    );
  });

  const workspaceId = 'test';
  const aggregateId = '49';
  const userId = '44';
  const dateStart = new Date(Date.now() + 1000);
  const dateEnd = new Date(Date.now() + 2000);
  const resourceIds = [1, 2, 3];
  const reservationAggregate = {
    workspaceId,
    id: aggregateId,
    dateStart,
    dateEnd,
    resourceIds,
    userId,
  };

  describe('#dumpEvents()', () => {
    it('should return all events from repository', async () => {
      const fn = jest.spyOn(stateRepository, 'dumpEvents');

      await stateService.dumpEvents(workspaceId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId);
    });
  });

  describe('#getById()', () => {
    it('should return aggregate by id from repository', async () => {
      const fn = jest.spyOn(stateRepository, 'getById');

      await stateService.getById(workspaceId, aggregateId);

      expect(fn).toHaveBeenCalledTimes(1);
      expect(fn).toHaveBeenCalledWith(workspaceId, aggregateId);
    });
  });

  describe('#confirm()', () => {
    it('should produce a `confirm` command', async () => {
      const changedBy = '45';
      const aggregate = ReservationState.requested;
      const getById = jest.spyOn(stateRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementation(() => Promise.resolve(reservationAggregate));
      const isAvailable = jest.spyOn(availabilityService, 'isAvailable')
        .mockImplementation(() => Promise.resolve(true));
      const execute = jest.spyOn(stateRepository, 'execute');

      await stateService.confirm(workspaceId, aggregateId, changedBy);

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getReservationById).toHaveBeenCalledTimes(1);
      expect(getReservationById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isAvailable).toHaveBeenCalledTimes(resourceIds.length);
      resourceIds.forEach((resourceId, idx) =>
        expect(isAvailable)
          .toHaveBeenNthCalledWith(
            idx + 1,
            workspaceId,
            dateStart,
            dateEnd,
            resourceId,
          ),
      );
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: StateCommandType.confirm,
        workspaceId,
        aggregateId,
        payload: { changedBy },
      }, aggregate);
    });

    it(
      'should throw an error some of the resources are unavailable',
      async () =>
    {
      const changedBy = '45';
      const aggregate = ReservationState.requested;
      const getById = jest.spyOn(stateRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementation(() => Promise.resolve(reservationAggregate));
      const isAvailable = jest.spyOn(availabilityService, 'isAvailable')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(stateRepository, 'execute');

      await expect(
        stateService.confirm(workspaceId, aggregateId, changedBy),
      ).rejects.toEqual(
        new Error(
          `Resources with ids ${resourceIds.join(', ')} are already reserved.`,
        ),
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getReservationById).toHaveBeenCalledTimes(1);
      expect(getReservationById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isAvailable).toHaveBeenCalledTimes(resourceIds.length);
      resourceIds.forEach((resourceId, idx) =>
        expect(isAvailable)
          .toHaveBeenNthCalledWith(
            idx + 1,
            workspaceId,
            dateStart,
            dateEnd,
            resourceId,
          ),
      );
      expect(execute).not.toHaveBeenCalled();
    });

    it('should throw an error that users cannot confirm', async () => {
      const changedBy = '45';
      const aggregate = ReservationState.requested;
      const getById = jest.spyOn(stateRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(true));
      const execute = jest.spyOn(stateRepository, 'execute');

      await expect(
        stateService.confirm(workspaceId, aggregateId, changedBy),
      ).rejects.toEqual(
        new Error('Only Maintainers or Owners can confirm reservations.'),
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#cancel()', () => {
    it('should produce a `cancel` command', async () => {
      const changedBy = '45';
      const aggregate = ReservationState.requested;
      const getById = jest.spyOn(stateRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementation(() => Promise.resolve(reservationAggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(stateRepository, 'execute');

      await stateService.cancel(workspaceId, aggregateId, changedBy);

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(getReservationById).toHaveBeenCalledTimes(1);
      expect(getReservationById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: StateCommandType.cancel,
        workspaceId,
        aggregateId,
        payload: { changedBy },
      }, aggregate);
    });

    it('should produce a `cancel` command', async () => {
      const changedBy = userId;
      const aggregate = ReservationState.requested;
      const getById = jest.spyOn(stateRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementation(() => Promise.resolve(reservationAggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser');
      const execute = jest.spyOn(stateRepository, 'execute');

      await stateService.cancel(workspaceId, aggregateId, changedBy);

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(getReservationById).toHaveBeenCalledTimes(1);
      expect(getReservationById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).not.toHaveBeenCalled();
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: StateCommandType.cancel,
        workspaceId,
        aggregateId,
        payload: { changedBy },
      }, aggregate);
    });

    it(
      'should produce an error when user tries to cancel different reservation',
      async () =>
    {
      const changedBy = '40';
      const aggregate = ReservationState.requested;
      const getById = jest.spyOn(stateRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementation(() => Promise.resolve(reservationAggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(true));
      const execute = jest.spyOn(stateRepository, 'execute');

      await expect(
        stateService.cancel(workspaceId, aggregateId, changedBy),
      ).rejects.toEqual(
        new Error(
          'Only Maintainers or Owners can modify someone else\'s reservations.',
        ),
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(getReservationById).toHaveBeenCalledTimes(1);
      expect(getReservationById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#reject()', () => {
    it('should produce a `reject` command', async () => {
      const changedBy = '45';
      const aggregate = ReservationState.requested;
      const reason = 'because we need to test';
      const getById = jest.spyOn(stateRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const execute = jest.spyOn(stateRepository, 'execute');
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementation(() => Promise.resolve(reservationAggregate));

      await stateService.reject(workspaceId, aggregateId, changedBy, reason);

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getReservationById).toHaveBeenCalledTimes(1);
      expect(getReservationById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: StateCommandType.reject,
        workspaceId,
        aggregateId,
        payload: { changedBy, reason },
      }, aggregate);
    });

    it('should throw an error that users cannot reject', async () => {
      const changedBy = '45';
      const aggregate = ReservationState.requested;
      const reason = 'because we need to test!';
      const getById = jest.spyOn(stateRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(true));
      const execute = jest.spyOn(stateRepository, 'execute');

      await expect(
        stateService.reject(workspaceId, aggregateId, changedBy, reason),
      ).rejects.toEqual(
        new Error('Only Maintainers or Owners can reject reservations.'),
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(execute).not.toHaveBeenCalled();
    });

    it('should throw an error if reservation in past', async () => {
      const changedBy = '45';
      const aggregate = ReservationState.requested;
      const reason = 'because we need to test!';
      const getById = jest.spyOn(stateRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const isUser = jest.spyOn(userRoleService, 'isUser')
        .mockImplementation(() => Promise.resolve(false));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementation(() => Promise.resolve({
          ...reservationAggregate,
          dateStart: new Date(0),
      }));
      const execute = jest.spyOn(stateRepository, 'execute');

      await expect(
        stateService.reject(workspaceId, aggregateId, changedBy, reason),
      ).rejects.toEqual(
        new Error('Cannot reject reservation that starts in past.'),
      );

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(isUser).toHaveBeenCalledTimes(1);
      expect(isUser).toHaveBeenCalledWith(workspaceId, changedBy);
      expect(getReservationById).toHaveBeenCalledTimes(1);
      expect(getReservationById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(execute).not.toHaveBeenCalled();
    });
  });

  describe('#complete()', () => {
    it('should produce a `complete` command', async () => {
      const aggregate = ReservationState.confirmed;
      const getById = jest.spyOn(stateRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementation(() => Promise.resolve({
          ...reservationAggregate,
          dateStart: new Date(0),
          dateEnd: new Date(0),
      }));
      const execute = jest.spyOn(stateRepository, 'execute');

      await stateService.complete(workspaceId, aggregateId);

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(getReservationById).toHaveBeenCalledTimes(1);
      expect(getReservationById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(execute).toHaveBeenCalledTimes(1);
      expect(execute).toHaveBeenCalledWith({
        type: StateCommandType.complete,
        workspaceId,
        aggregateId,
        payload: { },
      }, aggregate);
    });

    it('should produce an error when resrvation in future', async () => {
      const aggregate = ReservationState.confirmed;
      const getById = jest.spyOn(stateRepository, 'getById')
        .mockImplementation(() => Promise.resolve(aggregate));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementation(() => Promise.resolve(reservationAggregate));
      const execute = jest.spyOn(stateRepository, 'execute');

      await expect(
        stateService.complete(workspaceId, aggregateId),
      ).rejects
        .toEqual(new Error('Cannot complete reservation has not ended yet.'));

      expect(getById).toHaveBeenCalledTimes(1);
      expect(getById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(getReservationById).toHaveBeenCalledTimes(1);
      expect(getReservationById).toHaveBeenCalledWith(workspaceId, aggregateId);
      expect(execute).not.toHaveBeenCalled();
    });
  });
});
