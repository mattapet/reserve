/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { stateCommandHandler } from '../state.command-handler';
import {
  ReservationState,
} from '../../../../lib/reservation/reservation-state.type';
import { StateCommandType } from '../../state.command';
import { StateEventType } from '../../state.event';
//#endregion

describe('state.command-handler', () => {
  const workspaceId = 'test';
  const aggregateId = '49';
  const changedBy = '45';

  const makeCommandCreator = <T extends StateCommandType>(
    type: T,
  ) =>
    <Value extends {} = {}>(value: Value) => ({
      type,
      workspaceId,
      aggregateId,
      payload: {
        ...value,
        changedBy,
      },
    });

  describe('`confirm` command', () => {
    const makeCommand = makeCommandCreator(StateCommandType.confirm);

    it('should produce `confirmed` event', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.requested;

      const [result] = stateCommandHandler(aggregate, command);

      expect(result.type).toEqual(StateEventType.confirmed);
      expect(result.payload).toEqual({ changedBy });
    });

    it('should produce `confirmed` event', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.canceled;

      const [result] = stateCommandHandler(aggregate, command);

      expect(result.type).toEqual(StateEventType.confirmed);
      expect(result.payload).toEqual({ changedBy });
    });

    it('should produce `confirmed` event', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.rejected;

      const [result] = stateCommandHandler(aggregate, command);

      expect(result.type).toEqual(StateEventType.confirmed);
      expect(result.payload).toEqual({ changedBy });
    });

    it('should no events if no state change', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.confirmed;

      const results = stateCommandHandler(aggregate, command);

      expect(results).toEqual([]);
    });

    it('should produce Invalid state change error', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.completed;

      const fn = () => stateCommandHandler(aggregate, command);

      expect(fn).toThrowError('Invalid state change');
    });
  });

  describe('`cancel` command', () => {
    const makeCommand = makeCommandCreator(StateCommandType.cancel);

    it('should produce `canceled` event', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.confirmed;

      const [result] = stateCommandHandler(aggregate, command);

      expect(result.type).toEqual(StateEventType.canceled);
      expect(result.payload).toEqual({ changedBy });
    });

    it('should no events if no state change', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.canceled;

      const results = stateCommandHandler(aggregate, command);

      expect(results).toEqual([]);
    });

    it('should produce Invalid state change error', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.requested;

      const fn = () => stateCommandHandler(aggregate, command);

      expect(fn).toThrowError('Invalid state change');
    });

    it('should produce Invalid state change error', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.rejected;

      const fn = () => stateCommandHandler(aggregate, command);

      expect(fn).toThrowError('Invalid state change');
    });

    it('should produce Invalid state change error', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.completed;

      const fn = () => stateCommandHandler(aggregate, command);

      expect(fn).toThrowError('Invalid state change');
    });
  });

  describe('`reject` command', () => {
    const reason = 'because we need to test!';
    const makeCommand = makeCommandCreator(StateCommandType.reject);

    it('should produce `rejected` event', () => {
      const command = makeCommand({ reason });
      const aggregate = ReservationState.requested;

      const [result] = stateCommandHandler(aggregate, command);

      expect(result.type).toEqual(StateEventType.rejected);
      expect(result.payload).toEqual({ reason, changedBy });
    });

    it('should no events if no state change', () => {
      const command = makeCommand({ reason });
      const aggregate = ReservationState.rejected;

      const results = stateCommandHandler(aggregate, command);

      expect(results).toEqual([]);
    });

    it('should produce Invalid state change error', () => {
      const command = makeCommand({ reason });
      const aggregate = ReservationState.confirmed;

      const fn = () => stateCommandHandler(aggregate, command);

      expect(fn).toThrowError('Invalid state change');
    });

    it('should produce Invalid state change error', () => {
      const command = makeCommand({ reason });
      const aggregate = ReservationState.confirmed;

      const fn = () => stateCommandHandler(aggregate, command);

      expect(fn).toThrowError('Invalid state change');
    });

    it('should produce Invalid state change error', () => {
      const command = makeCommand({ reason });
      const aggregate = ReservationState.completed;

      const fn = () => stateCommandHandler(aggregate, command);

      expect(fn).toThrowError('Invalid state change');
    });
  });

  describe('`complete` command', () => {
    const makeCommand = makeCommandCreator(StateCommandType.complete);

    it('should produce `completed` event', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.confirmed;

      const [result] = stateCommandHandler(aggregate, command);

      expect(result.type).toEqual(StateEventType.completed);
      expect(result.payload).toEqual({});
    });

    it('should no events if no state change', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.completed;

      const results = stateCommandHandler(aggregate, command);

      expect(results).toEqual([]);
    });

    it('should produce Invalid state change error', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.canceled;

      const fn = () => stateCommandHandler(aggregate, command);

      expect(fn).toThrowError('Invalid state change');
    });

    it('should produce Invalid state change error', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.rejected;

      const fn = () => stateCommandHandler(aggregate, command);

      expect(fn).toThrowError('Invalid state change');
    });

    it('should produce Invalid state change error', () => {
      const command = makeCommand({});
      const aggregate = ReservationState.requested;

      const fn = () => stateCommandHandler(aggregate, command);

      expect(fn).toThrowError('Invalid state change');
    });
  });
});
