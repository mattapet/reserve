/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { stateEventHandler } from '../state.event-handler';
import { StateEventType } from '../../state.event';
import {
  ReservationState,
} from '../../../../lib/reservation/reservation-state.type';
//#endregion

describe('state.event-handler', () => {
  const workspaceId = 'test';
  const aggregateId = '49';
  const changedBy = '45';
  const timestamp = new Date(0);
  const aggregate = ReservationState.rejected;
  const reason = 'because we need to test!';
  const makeEventCreator = <T extends StateEventType>(type: T) =>
    <Value extends {}>(value: Value) => ({
      type,
      workspaceId,
      aggregateId,
      timestamp,
      payload: {
        ...value,
        changedBy,
      },
    });

  it('should produce `confirmed` state', () => {
    const event = makeEventCreator(StateEventType.confirmed)({});

    const result = stateEventHandler(aggregate, event);

    expect(result).toEqual(ReservationState.confirmed);
  });

  it('should produce `canceled` state', () => {
    const event = makeEventCreator(StateEventType.canceled)({});

    const result = stateEventHandler(aggregate, event);

    expect(result).toEqual(ReservationState.canceled);
  });

  it('should produce `rejected` state', () => {
    const event = makeEventCreator(StateEventType.rejected)({ reason });

    const result = stateEventHandler(aggregate, event);

    expect(result).toEqual(ReservationState.rejected);
  });

  it('should produce `completed` state', () => {
    const event = makeEventCreator(StateEventType.completed)({});

    const result = stateEventHandler(aggregate, event);

    expect(result).toEqual(ReservationState.completed);
  });
});
