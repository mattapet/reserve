/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { StateAggregate } from './state.aggregate';
import { StateEvent } from './state.event';
import { StateCommand } from './state.command';
import { Repository } from '../../lib/events/repository-base.interface';
//#endregion

export interface StateRepository extends
  Repository<
    StateCommand,
    StateEvent,
    StateAggregate
  >
{ }
