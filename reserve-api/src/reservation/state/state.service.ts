/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { StateAggregate } from './state.aggregate';
import { StateEvent } from './state.event';
//#endregion

export interface StateService {
  // - MARK: Queries

  dumpEvents(workspaceId: string): Promise<StateEvent[]>;

  getById(workspaceId: string, reservationId: string): Promise<StateAggregate>;

  // - MARK: Comamnds

  confirm(
    workspaceId: string,
    reservationId: string,
    changedBy: string,
  ): Promise<StateAggregate>;

  cancel(
    workspaceId: string,
    reservationId: string,
    changedBy: string,
  ): Promise<StateAggregate>;

  reject(
    workspaceId: string,
    reservationId: string,
    changedBy: string,
    reason: string,
  ): Promise<StateAggregate>;

  complete(workspaceId: string, reservationId: string): Promise<StateAggregate>;
}
