/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';

import { NestStateEventRepository } from './state-event.repository';
import { StateServiceImpl} from '../impl/state.service';
import { stateCommandHandler } from '../impl/state.command-handler';
import { stateEventHandler } from '../impl/state.event-handler';
import { defaultAggregate } from '../state.aggregate';
import { NestReservationService } from '../../nest';
import { NestUserRoleService } from '../../../user/user-role/nest';
import { BaseRepository } from '../../../lib/events/repository-base';
import { NestAvailabilityService } from '../../availability/nest';
//#endregion

@Injectable()
export class NestStateService extends StateServiceImpl {
  public constructor(
    @InjectConnection()
    connection: Connection,
    reservation: NestReservationService,
    userRole: NestUserRoleService,
    availability: NestAvailabilityService,
  ) {
    super(
      new BaseRepository(
        connection.getCustomRepository(NestStateEventRepository),
        stateCommandHandler,
        stateEventHandler,
        defaultAggregate,
      ),
      reservation,
      userRole,
      availability,
    );
  }
}
