/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository, Repository } from 'typeorm';

import { StateEventEntity } from './state-event.entity';
import { StateEventRepository } from '../state-event.repository';
import { StateEvent } from '../state.event';
//#endregion

@EntityRepository(StateEventEntity)
export class NestStateEventRepository
  extends Repository<StateEventEntity>
  implements StateEventRepository
{
  public async findByRowId(rowId: string): Promise<StateEvent[]> {
    const events = await this.find({
      where: { rowId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async findByRowIdAndAggregateId(
    rowId: string,
    aggregateId: string,
  ): Promise<StateEvent[]> {
    const events = await this.find({
      where: { rowId, aggregateId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async saveEvents(...events: StateEvent[]): Promise<StateEvent[]> {
    const results = await this.manager.transaction(async (trx) => {
      return await trx.save(StateEventEntity, events.map(this.wrap));
    });
    return results.map(this.unwrap);
  }

  private wrap(event: StateEvent): StateEventEntity {
    const { workspaceId, aggregateId, timestamp } = event;
    return new StateEventEntity(
      workspaceId,
      aggregateId,
      timestamp,
      event,
    );
  }

  private unwrap(entity: StateEventEntity): StateEvent {
    return entity.payload;
  }
}
