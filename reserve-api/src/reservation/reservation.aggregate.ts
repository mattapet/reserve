/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export interface ReservationAggregate {
  readonly id: string;
  readonly dateStart: Date;
  readonly dateEnd: Date;
  readonly userId: string;
  readonly resourceIds: number[];
}

export const defaultAggregate: ReservationAggregate = {
  id: '',
  dateStart: new Date(0),
  dateEnd: new Date(0),
  userId: '',
  resourceIds: [],
};
