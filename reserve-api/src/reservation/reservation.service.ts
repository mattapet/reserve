/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationAggregate } from './reservation.aggregate';
import { ReservationEvent } from './reservation.event';
//#endregion

export interface ReservationService {
  // - MARK: Queries

  dumpEvents(workspaceId: string): Promise<ReservationEvent[]>;

  getAll(workspaceId: string): Promise<ReservationAggregate[]>;

  getById(
    workspaceId: string,
    reservationId: string,
  ): Promise<ReservationAggregate>;

  // - MARK: Commands

  create(
    workspaceId: string,
    dateStart: Date,
    dateEnd: Date,
    resources: number[],
    userId: string,
    createdBy: string,
  ): Promise<ReservationAggregate>;

  editDates(
    workspaceId: string,
    reservationId: string,
    dateStart: Date,
    dateEnd: Date,
    changedBy: string,
  ): Promise<ReservationAggregate>;

  assign(
    workspaceId: string,
    reservationId: string,
    assignTo: string,
    changedBy: string,
  ): Promise<ReservationAggregate>;

  addResource(
    workspaceId: string,
    reservationId: string,
    resource: number,
    changedBy: string,
  ): Promise<ReservationAggregate>;

  removeResource(
    workspaceId: string,
    reservationId: string,
    resource: number,
    changedBy: string,
  ): Promise<ReservationAggregate>;
}
