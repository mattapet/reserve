/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '../lib/events/event';
//#endregion

export enum ReservationEventType {
  created = 'created',
  dateChanged = 'date_changed',
  assigned = 'assigned',
  resourceAdded = 'resource_added',
  resourceRemoved = 'resource_removed',
}

export interface Created extends Event {
  readonly type: ReservationEventType.created;
  readonly payload: {
    readonly dateStart: string;
    readonly dateEnd: string;
    readonly createdBy: string;
  };
}

export interface DateChanged extends Event {
  readonly type: ReservationEventType.dateChanged;
  readonly payload: {
    readonly dateStart: string;
    readonly dateEnd: string;
    readonly changedBy: string;
  };
}

export interface Assigned extends Event {
  readonly type: ReservationEventType.assigned;
  readonly payload: {
    readonly assignedTo: string;
    readonly changedBy: string;
  };
}

export interface ResourceAdded extends Event {
  readonly type: ReservationEventType.resourceAdded;
  readonly payload: {
    readonly resource: number;
    readonly changedBy: string;
  };
}

export interface ResourceRemoved extends Event {
  readonly type: ReservationEventType.resourceRemoved;
  readonly payload: {
    readonly resource: number;
    readonly changedBy: string;
  };
}

export type ReservationEvent =
  | Created
  | DateChanged
  | Assigned
  | ResourceAdded
  | ResourceRemoved;
