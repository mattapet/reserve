/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, forwardRef, Inject } from '@nestjs/common';
import { AvailabilityServiceImpl } from '../impl/availability.service';
import { NestReservationService } from '../../nest';
import { NestStateService } from '../../state/nest';
import { NestSearchService } from '../../search/nest';
//#endregion

@Injectable()
export class NestAvailabilityService extends AvailabilityServiceImpl {
  public constructor(
    reservationService: NestReservationService,
    @Inject(forwardRef(() => NestStateService))
    stateService: NestStateService,
    searchService: NestSearchService,
  ) { super(reservationService, stateService, searchService); }
}
