/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AvailabilityService } from '../availability.service';
import { SearchService } from '../../search';
import { StateService } from '../../state';
import { ReservationService } from '../..';
import { zip } from '../../../lib/zip';
import { flatMap } from '../../../lib/flatMap';
import {
  ReservationState,
} from '../../../lib/reservation/reservation-state.type';
//#endregion

export class AvailabilityServiceImpl implements AvailabilityService {
  public constructor(
    private readonly reservationService: ReservationService,
    private readonly stateService: StateService,
    private readonly searchService: SearchService,
  ) { }

  public async isAvailable(
    workspaceId: string,
    dateStart: Date,
    dateEnd: Date,
    resource: number,
  ): Promise<boolean> {
    const matchingProjections = await this.searchService
      .getByDateRange(workspaceId, dateStart, dateEnd);

    const matchingStates = await Promise.all(
      matchingProjections.map(({ reservationId }) =>
        this.stateService.getById(workspaceId, reservationId),
      ),
    );

    const matchingReservations = await Promise.all(
      zip(matchingProjections, matchingStates)
        .filter(([, state]) => state === ReservationState.confirmed)
        .map(([{ reservationId }]) => this.reservationService
          .getById(workspaceId, reservationId)),
    );

    const confirmedSet = new Set(
      flatMap(matchingReservations, ({ resourceIds }) => resourceIds),
    );
    return !confirmedSet.has(resource);
  }
}
