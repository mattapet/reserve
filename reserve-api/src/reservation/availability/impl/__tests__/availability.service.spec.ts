/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AvailabilityServiceImpl } from '../availability.service';
import { SearchService } from '../../../search';
import { SearchServiceMock } from '../../../search/__mocks__';
import { StateService } from '../../../state';
import { StateServiceMock } from '../../../state/__mocks__';
import { ReservationService } from '../../..';
import { ReservationServiceMock } from '../../../__mocks__';
import {
  ReservationState,
} from '../../../../lib/reservation/reservation-state.type';
//#endregion

describe('availability.service', () => {
  let reservationService!: ReservationService;
  let stateService!: StateService;
  let searchService!: SearchService;
  let availabilityService!: AvailabilityServiceImpl;

  beforeEach(() => {
    reservationService = new ReservationServiceMock();
    stateService = new StateServiceMock();
    searchService = new SearchServiceMock();
    availabilityService = new AvailabilityServiceImpl(
      reservationService,
      stateService,
      searchService,
    );
  });

  describe('#isAvailable()', () => {
    const workspaceId = 'test';
    const userId = '45';
    const reservations = [...new Array(3)].map((_, idx) => ({
      workspaceId,
      id: `${idx + 1}`,
      dateStart: new Date(idx * 100),
      dateEnd: new Date((idx + 1) * 100),
      resourceIds: [idx + 1],
      userId,
    }));
    const projections = reservations.map(projection => ({
      dateStart: projection.dateStart,
      dateEnd: projection.dateEnd,
      workspaceId: projection.workspaceId,
      reservationId: projection.id,
    }));

    it(
      'should return `true` if no reservations in given date range',
      async () =>
    {
      const dateStart = new Date(0);
      const dateEnd = new Date(0);
      const getByDateRange = jest.spyOn(searchService, 'getByDateRange')
        .mockImplementation(() => Promise.resolve([]));
      const getStateById = jest.spyOn(stateService, 'getById');
      const getReservationById = jest.spyOn(reservationService, 'getById');

      const result = await availabilityService
        .isAvailable(workspaceId, dateStart, dateEnd, 1);

      expect(result).toBe(true);
      expect(getByDateRange).toHaveBeenCalledTimes(1);
      expect(getByDateRange)
        .toHaveBeenCalledWith(workspaceId, dateStart, dateEnd);
      expect(getStateById).not.toHaveBeenCalled();
      expect(getReservationById).not.toHaveBeenCalled();
    });

    it(
      'should return `true` if no reservation with given resource intersect'
      + 'within given date range',
      async () =>
    {
      const dateStart = new Date(0);
      const dateEnd = new Date(1000);
      const getByDateRange = jest.spyOn(searchService, 'getByDateRange')
        .mockImplementation(() => Promise.resolve(projections));
      const getStateById = jest.spyOn(stateService, 'getById')
        .mockImplementation(() => Promise.resolve(ReservationState.confirmed));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementationOnce(() => Promise.resolve(reservations[0]))
        .mockImplementationOnce(() => Promise.resolve(reservations[1]))
        .mockImplementationOnce(() => Promise.resolve(reservations[2]));

      const result = await availabilityService
        .isAvailable(workspaceId, dateStart, dateEnd, 45);

      expect(result).toBe(true);
      expect(getByDateRange).toHaveBeenCalledTimes(1);
      expect(getByDateRange)
        .toHaveBeenCalledWith(workspaceId, dateStart, dateEnd);
      expect(getStateById).toHaveBeenCalledTimes(reservations.length);
      reservations.forEach(({ id }, idx) =>
        expect(getStateById).toHaveBeenNthCalledWith(idx + 1, workspaceId, id),
      );
      expect(getReservationById).toHaveBeenCalledTimes(reservations.length);
      reservations.forEach(({ id }, idx) =>
        expect(getReservationById)
          .toHaveBeenNthCalledWith(idx + 1, workspaceId, id),
      );
    });

    it(
      'should return `true` if no reservation with given resource intersect'
      + 'within given date range',
      async () =>
    {
      const dateStart = new Date(0);
      const dateEnd = new Date(1000);
      const getByDateRange = jest.spyOn(searchService, 'getByDateRange')
        .mockImplementation(() => Promise.resolve(projections));
      const getStateById = jest.spyOn(stateService, 'getById')
        .mockImplementationOnce(() =>
          Promise.resolve(ReservationState.confirmed))
        .mockImplementationOnce(() =>
          Promise.resolve(ReservationState.rejected))
        .mockImplementationOnce(() =>
          Promise.resolve(ReservationState.confirmed));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementationOnce(() => Promise.resolve(reservations[0]))
        .mockImplementationOnce(() => Promise.resolve(reservations[2]));

      const result = await availabilityService
        .isAvailable(workspaceId, dateStart, dateEnd, 2);

      expect(result).toBe(true);
      expect(getByDateRange).toHaveBeenCalledTimes(1);
      expect(getByDateRange)
        .toHaveBeenCalledWith(workspaceId, dateStart, dateEnd);
      expect(getStateById).toHaveBeenCalledTimes(reservations.length);
      reservations.forEach(({ id }, idx) =>
        expect(getStateById).toHaveBeenNthCalledWith(idx + 1, workspaceId, id),
      );
      expect(getReservationById).toHaveBeenCalledTimes(2);
      expect(getReservationById)
        .toHaveBeenNthCalledWith(1, workspaceId, reservations[0].id);
      expect(getReservationById)
        .toHaveBeenNthCalledWith(2, workspaceId, reservations[2].id);
    });

    it(
      'should return `false` if a reservation with given resource intersect'
      + 'within given date range',
      async () =>
    {
      const dateStart = new Date(100);
      const dateEnd = new Date(200);
      const getByDateRange = jest.spyOn(searchService, 'getByDateRange')
        .mockImplementation(() => Promise.resolve(projections));
      const getStateById = jest.spyOn(stateService, 'getById')
        .mockImplementationOnce(() =>
          Promise.resolve(ReservationState.confirmed))
        .mockImplementationOnce(() =>
          Promise.resolve(ReservationState.rejected))
        .mockImplementationOnce(() =>
          Promise.resolve(ReservationState.confirmed));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementationOnce(() => Promise.resolve(reservations[0]))
        .mockImplementationOnce(() => Promise.resolve(reservations[2]));

      const result = await availabilityService
        .isAvailable(workspaceId, dateStart, dateEnd, 1);

      expect(result).toBe(false);
      expect(getByDateRange).toHaveBeenCalledTimes(1);
      expect(getByDateRange)
        .toHaveBeenCalledWith(workspaceId, dateStart, dateEnd);
      expect(getStateById).toHaveBeenCalledTimes(reservations.length);
      reservations.forEach(({ id }, idx) =>
        expect(getStateById).toHaveBeenNthCalledWith(idx + 1, workspaceId, id),
      );
      expect(getReservationById).toHaveBeenCalledTimes(2);
      expect(getReservationById)
        .toHaveBeenNthCalledWith(1, workspaceId, reservations[0].id);
      expect(getReservationById)
        .toHaveBeenNthCalledWith(2, workspaceId, reservations[2].id);
    });

    it(
      'should return `false` if a reservation with given resource intersect'
      + 'within given date range',
      async () =>
    {
      const dateStart = new Date(100);
      const dateEnd = new Date(200);
      const getByDateRange = jest.spyOn(searchService, 'getByDateRange')
        .mockImplementation(() => Promise.resolve(projections));
      const getStateById = jest.spyOn(stateService, 'getById')
        .mockImplementationOnce(() =>
          Promise.resolve(ReservationState.confirmed))
        .mockImplementationOnce(() =>
          Promise.resolve(ReservationState.rejected))
        .mockImplementationOnce(() =>
          Promise.resolve(ReservationState.confirmed));
      const getReservationById = jest.spyOn(reservationService, 'getById')
        .mockImplementationOnce(() => Promise.resolve(reservations[0]))
        .mockImplementationOnce(() => Promise.resolve(reservations[2]));

      const result = await availabilityService
        .isAvailable(workspaceId, dateStart, dateEnd, 3);

      expect(result).toBe(false);
      expect(getByDateRange).toHaveBeenCalledTimes(1);
      expect(getByDateRange)
        .toHaveBeenCalledWith(workspaceId, dateStart, dateEnd);
      expect(getStateById).toHaveBeenCalledTimes(reservations.length);
      reservations.forEach(({ id }, idx) =>
        expect(getStateById).toHaveBeenNthCalledWith(idx + 1, workspaceId, id),
      );
      expect(getReservationById).toHaveBeenCalledTimes(2);
      expect(getReservationById)
        .toHaveBeenNthCalledWith(1, workspaceId, reservations[0].id);
      expect(getReservationById)
        .toHaveBeenNthCalledWith(2, workspaceId, reservations[2].id);
    });
  });
});
