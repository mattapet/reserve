/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Column, PrimaryColumn, Index, Entity } from 'typeorm';
import { SearchProjection } from '../search-projection.interface';
//#endregion

@Entity({ name: 'search_projection' })
export class SearchEntity implements SearchProjection {
  @PrimaryColumn({ name: 'workspace_id' })
  public workspaceId: string;

  @PrimaryColumn({ name: 'reservation_id' })
  public reservationId: string;

  @Index()
  @Column({ transformer: {
    to: (d) => d || new Date().toISOString(),
    from: (d: Date) => d,
  }, type: 'datetime', name: 'date_start' })
  public dateStart: Date;

  @Index()
  @Column({ transformer: {
    to: (d) => d || new Date().toISOString(),
    from: (d: Date) => d,
  }, type: 'datetime', name: 'date_end' })
  public dateEnd: Date;

  @Index()
  @Column({ transformer: {
    to: (d) => d || new Date().toISOString(),
    from: (d: Date) => d,
  }, type: 'timestamp', name: 'last_updated' })
  public lastUpdated: Date;

  public constructor(
    workspaceId: string,
    reservationId: string,
    dateStart: Date,
    dateEnd: Date,
    lastUpdated: Date,
  ) {
    this.workspaceId = workspaceId;
    this.reservationId = reservationId;
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
    this.lastUpdated = lastUpdated;
  }
}
