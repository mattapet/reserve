/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  EntityRepository,
  Repository,
  LessThanOrEqual,
  MoreThanOrEqual,
} from 'typeorm';
import { SearchEntity } from './search.entity';
import { SearchProjectionRepository } from '../search-projection.repository';
import { SearchProjection } from '../search-projection.interface';
//#endregion

@EntityRepository(SearchEntity)
export class NestSearchProjectionRepository
  extends Repository<SearchEntity>
  implements SearchProjectionRepository
{
  public async getByDateRange(
    workspaceId: string,
    dateStart: Date,
    dateEnd: Date,
  ): Promise<SearchProjection[]> {
    return await this.find({
      where: {
        workspaceId,
        dateStart: LessThanOrEqual(dateEnd),
        dateEnd: MoreThanOrEqual(dateStart),
      },
    });
  }

  public async getLatestUpdate(): Promise<Date> {
    const latest = await this.findOne({
      order: {
        lastUpdated: 'DESC',
      },
    });
    return latest ? latest.lastUpdated : new Date(0);
  }

  public async updateProjection(
    worksapceId: string,
    reservationId: string,
    dateStart: Date,
    dateEnd: Date,
    timestamp: Date,
  ): Promise<void> {
    const item = new SearchEntity(
      worksapceId,
      reservationId,
      dateStart,
      dateEnd,
      timestamp,
    );
    await this.save(item);
  }
}
