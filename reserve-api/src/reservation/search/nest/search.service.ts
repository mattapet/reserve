/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { InjectConnection } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';

import { SearchRepositoryImpl } from '../impl/search.repository';
import { SearchServiceImpl } from '../impl/search.service';
import { NestSearchProjectionRepository } from './search-projection.repository';
import { ReservationEventRepository } from '../../nest';
import { EventBus } from '../../../lib/events/observation/event-bus';
//#endregion

@Injectable()
export class NestSearchService extends SearchServiceImpl {
  public constructor(
    @InjectConnection()
    connection: Connection,
  ) {
    const repository = new SearchRepositoryImpl(
      connection.getCustomRepository(NestSearchProjectionRepository),
      connection.getCustomRepository(ReservationEventRepository),
      EventBus.instance,
    );
    super(repository);
    repository.setup();
  }
}
