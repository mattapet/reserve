/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { SearchProjection } from './search-projection.interface';
//#endregion

export interface SearchProjectionRepository {
  getLatestUpdate(): Promise<Date>;

  getByDateRange(
    worksapceId: string,
    dateStart: Date,
    dateEnd: Date,
  ): Promise<SearchProjection[]>;

  updateProjection(
    worksapceId: string,
    reservationId: string,
    dateStart: Date,
    dateEnd: Date,
    timestamp: Date,
  ): Promise<void>;
}
