/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { SearchProjectionRepository } from '../search-projection.repository';
//#endregion

export const SearchProjectionRepositoryMock =
  jest.fn<SearchProjectionRepository, []>(() => ({
    getLatestUpdate: jest.fn(),
    getByDateRange: jest.fn(),
    updateProjection: jest.fn(),
  }));
