/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { SearchService } from '../search.service';
//#endregion

export const SearchServiceMock = jest.fn<SearchService, []>(() => ({
  getByDateRange: jest.fn(),
}));
