/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { SearchRepositoryImpl } from '../search.repository';
import { SearchProjectionRepository } from '../../search-projection.repository';
import { SearchProjectionRepositoryMock } from '../../__mocks__';
import {
  ReservationEventRepository,
} from '../../../reservation-event.repository';
import { ReservationEventRepositoryMock } from '../../../__mocks__';
import {
  EventBus,
} from '../../../../lib/events/observation/event-bus.interface';
import {
  EventBusMock,
} from '../../../../lib/events/observation/__mocks__';
import {
  ReservationEventType,
  ReservationEvent,
} from '../../../reservation.event';
//#endregion

describe('search.repository', () => {
  let reservationEventRepository!: ReservationEventRepository;
  let searchProjectionRepository!: SearchProjectionRepository;
  let eventBus!: EventBus;
  let searchRepository!: SearchRepositoryImpl;

  beforeEach(() => {
    searchProjectionRepository = new SearchProjectionRepositoryMock();
    reservationEventRepository = new ReservationEventRepositoryMock();
    eventBus = new EventBusMock();

    searchRepository = new SearchRepositoryImpl(
      searchProjectionRepository,
      reservationEventRepository,
      eventBus,
    );
  });

  describe('#setup()', () => {
    const workspaceId = 'test';
    const aggregateId = '49';
    const timestamp = new Date(0);
    const makeEventCreator = <T extends ReservationEventType>(
      type: T,
    ) => <Value extends {}> (value: Value) => ({
      type,
      workspaceId,
      aggregateId,
      timestamp,
      payload: value,
    });
    const makeEventCreated = (dateStart: Date, dateEnd: Date) =>
      makeEventCreator(ReservationEventType.created)({
      dateStart: dateStart.toISOString(),
      dateEnd: dateEnd.toISOString(),
      createdBy: 14,
    });
    const makeEventEdited = (dateStart: Date, dateEnd: Date) =>
      makeEventCreator(ReservationEventType.dateChanged)({
      dateStart: dateStart.toISOString(),
      dateEnd: dateEnd.toISOString(),
      changedBy: '14',
    });
    const makeEventAssigned = (assignedTo: number) =>
      makeEventCreator(ReservationEventType.assigned)({
      assignedTo,
      changedBy: '14',
    });

    it('should update all missed events', async () => {
      const events = [
        makeEventCreated(new Date(10), new Date(12)),
        makeEventCreated(new Date(12), new Date(14)),
        makeEventEdited(new Date(14), new Date(16)),
      ];

      const getLatestUpdate = jest
        .spyOn(searchProjectionRepository, 'getLatestUpdate')
        .mockImplementation(() => Promise.resolve(new Date(14)));
      const findSince = jest
        .spyOn(reservationEventRepository, 'findSince')
        .mockImplementation(() =>
          Promise.resolve(events as ReservationEvent[]),
        );
      const updateProjection = jest
        .spyOn(searchProjectionRepository, 'updateProjection');

      await searchRepository.setup();

      expect(getLatestUpdate).toHaveBeenCalledTimes(1);
      expect(findSince).toHaveBeenCalledTimes(1);
      expect(findSince).toHaveBeenCalledWith(new Date(14));
      expect(updateProjection).toHaveBeenCalledTimes(events.length);
      events.forEach((event, idx) =>
        expect(updateProjection).toHaveBeenNthCalledWith(idx + 1,
          event.workspaceId,
          event.aggregateId,
          new Date(event.payload.dateStart),
          new Date(event.payload.dateEnd),
          event.timestamp,
        ),
      );
    });

    it('should subscribe to event bus', async () => {
      const consume = jest.spyOn(eventBus, 'consume');
      jest
        .spyOn(searchProjectionRepository, 'getLatestUpdate')
        .mockImplementation(() => Promise.resolve(new Date()));
      jest
        .spyOn(reservationEventRepository, 'findSince')
        .mockImplementation(() => Promise.resolve([]));

      await searchRepository.setup();

      expect(consume).toHaveBeenCalledTimes(2);
    });

    it('should update projection', async () => {
      const events = [
        makeEventCreated(new Date(12), new Date(14)),
        makeEventEdited(new Date(12), new Date(14)),
      ];

      const consume = jest.spyOn(eventBus, 'consume')
        .mockImplementationOnce((_, cb) =>  cb(events[0]));
      jest.spyOn(eventBus, 'consume')
        .mockImplementationOnce((_, cb) =>  cb(events[1]));
      const updateProjection = jest
        .spyOn(searchProjectionRepository, 'updateProjection');
      jest
        .spyOn(searchProjectionRepository, 'getLatestUpdate')
        .mockImplementation(() => Promise.resolve(new Date()));
      jest
        .spyOn(reservationEventRepository, 'findSince')
        .mockImplementation(() => Promise.resolve([]));

      await searchRepository.setup();

      expect(consume).toHaveBeenCalledTimes(2);
      expect(updateProjection).toHaveBeenCalledTimes(events.length);
      events.forEach((event, idx) =>
        expect(updateProjection).toHaveBeenNthCalledWith(idx + 1,
          event.workspaceId,
          event.aggregateId,
          new Date(event.payload.dateStart),
          new Date(event.payload.dateEnd),
          event.timestamp,
        ),
      );
    });

    it('should ignore events other than subscribed to', async () => {
      const assignedEvent = makeEventAssigned(14);
      const editedEvent = makeEventEdited(new Date(12), new Date(14));

      const consume = jest.spyOn(eventBus, 'consume')
        .mockImplementationOnce((_, cb) =>  cb(assignedEvent));
      jest.spyOn(eventBus, 'consume')
        .mockImplementationOnce((_, cb) =>  cb(editedEvent));
      const updateProjection = jest
        .spyOn(searchProjectionRepository, 'updateProjection');
      jest
        .spyOn(searchProjectionRepository, 'getLatestUpdate')
        .mockImplementation(() => Promise.resolve(new Date()));
      jest
        .spyOn(reservationEventRepository, 'findSince')
        .mockImplementation(() => Promise.resolve([]));

      await searchRepository.setup();

      expect(consume).toHaveBeenCalledTimes(2);
      expect(updateProjection).toHaveBeenCalledTimes(1);
      expect(updateProjection).toHaveBeenCalledWith(
        editedEvent.workspaceId,
        editedEvent.aggregateId,
        new Date(editedEvent.payload.dateStart),
        new Date(editedEvent.payload.dateEnd),
        editedEvent.timestamp,
      );
    });
  });

  describe('#getByDateRange()', () => {
    it('should get projections from projection repository', async () => {
      const workspaceId = 'test';
      const dateStart = new Date(12);
      const dateEnd = new Date(14);
      const getByDateRange = jest
        .spyOn(searchProjectionRepository, 'getByDateRange');

      await searchRepository.getByDateRange(workspaceId, dateStart, dateEnd);

      expect(getByDateRange).toHaveBeenCalledTimes(1);
      expect(getByDateRange)
        .toHaveBeenCalledWith(workspaceId, dateStart, dateEnd);
    });
  });
});
