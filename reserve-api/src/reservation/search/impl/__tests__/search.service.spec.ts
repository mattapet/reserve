/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { SearchServiceImpl } from '../search.service';
import { SearchRepository } from '../../search.repository';
import { SearchRepositoryMock } from '../../__mocks__/';
//#endregion

describe('search.service', () => {
  let searchRepository!: SearchRepository;
  let searchService!: SearchServiceImpl;

  beforeEach(() => {
    searchRepository = new SearchRepositoryMock();
    searchService = new SearchServiceImpl(searchRepository);
  });

  describe('#getByDateRange()', () => {
    it('should get by date range from repository', async () => {
      const workspaceId = 'test';
      const dateStart = new Date(0);
      const dateEnd = new Date(0);
      const getByDateRange = jest.spyOn(searchRepository, 'getByDateRange');

      await searchService.getByDateRange(workspaceId, dateStart, dateEnd);

      expect(getByDateRange).toHaveBeenCalledTimes(1);
      expect(getByDateRange)
        .toHaveBeenCalledWith(workspaceId, dateStart, dateEnd);
    });
  });
});
