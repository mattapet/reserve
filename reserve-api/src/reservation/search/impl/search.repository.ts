/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EventBus } from '../../../lib/events/observation/event-bus.interface';
import { Event } from '../../../lib/events/event';
import { SearchProjection } from '../search-projection.interface';
import { SearchProjectionRepository } from '../search-projection.repository';
import { SearchRepository } from '../search.repository';
import {
  ReservationEventRepository,
} from '../../reservation-event.repository';
import {
  ReservationEvent,
  ReservationEventType,
} from '../../reservation.event';
//#endregion

export class SearchRepositoryImpl implements SearchRepository {
  public constructor(
    private readonly searchProjectionRepository: SearchProjectionRepository,
    private readonly reservationEventRepository: ReservationEventRepository,
    private readonly eventBus: EventBus,
  ) { }

  public async setup(): Promise<void> {
    await this.updateState();
    await this.registerEventBus();
  }

  public async getByDateRange(
    worksapceId: string,
    dateStart: Date,
    dateEnd: Date,
  ): Promise<SearchProjection[]> {
    return await this.searchProjectionRepository
      .getByDateRange(worksapceId, dateStart, dateEnd);
  }

  private async updateState(): Promise<void> {
    const lastUpdate = await this.searchProjectionRepository.getLatestUpdate();
    const events = await this.reservationEventRepository.findSince(lastUpdate);
    await Promise.all(events.map(event => this.consume(event)));
  }

  private registerEventBus(): void {
    this.eventBus
      .consume(ReservationEventType.created, event => this.consume(event));
    this.eventBus
      .consume(ReservationEventType.dateChanged, event => this.consume(event));
  }

  private async consume(event: Event): Promise<void> {
    const reservationEvent = event as ReservationEvent;
    if (
      reservationEvent.type !== ReservationEventType.created &&
      reservationEvent.type !== ReservationEventType.dateChanged
    ) {
      return;
    }

    const { workspaceId, aggregateId, timestamp } = reservationEvent;
    const { dateStart, dateEnd } = reservationEvent.payload;
    await this.searchProjectionRepository.updateProjection(
      workspaceId,
      aggregateId,
      new Date(dateStart),
      new Date(dateEnd),
      timestamp,
    );
  }
}
