/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { SearchService } from '../search.service';
import { SearchRepository } from '../search.repository';
import { SearchProjection } from '../search-projection.interface';
//#endregion

export class SearchServiceImpl implements SearchService {
  public constructor(
    private readonly repository: SearchRepository,
  ) { }

  public async getByDateRange(
    workspaceId: string,
    dateStart: Date,
    dateEnd: Date,
  ): Promise<SearchProjection[]> {
    return this.repository.getByDateRange(workspaceId, dateStart, dateEnd);
  }
}
