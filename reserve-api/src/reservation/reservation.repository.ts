/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationAggregate } from './reservation.aggregate';
import { Repository } from '../lib/events/repository-base.interface';
import { ReservationEvent } from './reservation.event';
import { ReservationCommand } from './reservation.command';
//#endregion

export interface ReservationRepository extends
  Repository<
    ReservationCommand,
    ReservationEvent,
    ReservationAggregate
  >
{ }
