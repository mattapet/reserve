/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationState } from '../../lib/reservation/reservation-state.type';
//#endregion

export interface Reservation {
  readonly id: string;
  readonly userId: string;
  readonly dateStart: Date;
  readonly dateEnd: Date;
  readonly state: ReservationState;
  readonly resourceIds: number[];
}
