/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { InjectConnection } from '@nestjs/typeorm';

import { ReservationServiceImpl } from '../impl/reservation.service';
import { NestUserRoleService } from '../../user/user-role/nest';
import { ReservationRepositoryImpl } from '../impl/reservation.repository';
import { ReservationEventRepository } from './reservation-event.repository';
//#endregion

@Injectable()
export class NestReservationService extends ReservationServiceImpl {
  public constructor(
    @InjectConnection()
    connection: Connection,
    userRole: NestUserRoleService,
  ) {
    super(
      new ReservationRepositoryImpl(
        connection.getCustomRepository(ReservationEventRepository),
      ),
      userRole,
    );
  }
}
