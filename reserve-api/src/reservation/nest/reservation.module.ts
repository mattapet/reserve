/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { NestReservationService } from './reservation.service';
import { ReservationEventEntity } from './reservation-event.entity';
import { ReservationEventRepository } from './reservation-event.repository';
import { ReservationController } from './reservation.controller';

import {
  NestStateService,
  StateEventEntity,
  NestStateEventRepository,
} from '../state/nest';
import {
  NestSearchProjectionRepository,
  NestSearchService,
  SearchEntity,
} from '../search/nest';
import { NestAvailabilityService } from '../availability/nest';
import { UserModule } from '../../user/nest';
import { ResourceModule } from '../../resource/resource.module';
import { WorkspaceModule } from '../../workspace/nest';
import { WebhookModule } from '../../webhook/webhook.module';
import { SettingsModule } from '../../settings/nest';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ReservationEventEntity,
      ReservationEventRepository,
      StateEventEntity,
      NestStateEventRepository,
      NestSearchProjectionRepository,
      SearchEntity,
    ]),
    ResourceModule,
    UserModule,
    WorkspaceModule,
    WebhookModule,
    SettingsModule,
  ],
  controllers: [ReservationController],
  providers: [
    NestReservationService,
    NestStateService,
    NestSearchService,
    NestAvailabilityService,
  ],
  exports: [NestReservationService],
})
export class ReservationModule { }
