/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  UseGuards,
  Post,
  UseInterceptors,
  Req,
  Body,
  Get,
  Param,
  Put,
  Patch,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { NestReservationService } from './reservation.service';
import { NestStateService } from '../state/nest';
import { TransformerInterceptor } from './interceptors/transformer.interceptor';
import { WebhookInterceptor } from './interceptors/webhook.interceptor';
import { AuthorizedRequest } from 'lib/interface/authorized-request.interface';
import { Reservation } from './reservation.entity';
import { ReservationState } from '../../lib/reservation/reservation-state.type';
import {
  ReservationAggregate,
} from '../reservation.aggregate';
//#endregion

@Controller('api/v1/reservation')
@UseGuards(AuthGuard('jwt'))
export class ReservationController {
  public constructor(
    private readonly reservation: NestReservationService,
    private readonly state: NestStateService,
  ) { }

  @Post()
  @UseInterceptors(TransformerInterceptor, WebhookInterceptor)
  public async create(
    @Req() req: AuthorizedRequest,
    @Body('date_start') dateStart: string,
    @Body('date_end') dateEnd: string,
    @Body('resource_ids') resourceIds: number[],
    @Body('user_id') userId: string = req.user.id,
  ): Promise<Reservation> {
    const reservation = await this.reservation.create(
      req.user.workspace,
      new Date(dateStart),
      new Date(dateEnd),
      resourceIds,
      userId,
      req.user.id,
    );
    return {
      ...reservation,
      state: ReservationState.requested,
    };
  }

  @Get()
  @UseInterceptors(TransformerInterceptor)
  public async getAll(@Req() req: AuthorizedRequest): Promise<Reservation[]> {
    const aggregates = await this.reservation.getAll(req.user.workspace);
    return await this.fillReservations(req.user.workspace, ...aggregates);
  }

  @Get(':id')
  @UseInterceptors(TransformerInterceptor)
  public async getById(
    @Req() req: AuthorizedRequest,
    @Param('id') reservationId: string,
  ): Promise<Reservation> {
    return await this.getReservationById(req.user.workspace, reservationId);
  }

  @Put(':id')
  @UseInterceptors(TransformerInterceptor)
  public async updateById(
    @Req() req: AuthorizedRequest,
    @Param('id') reservationId: string,
    @Body('date_start') dateStart?: string,
    @Body('date_end') dateEnd?: string,
    @Body('resource_ids') resourceIds: number[] = [],
    @Body('user_id') userId?: string,
  ): Promise<Reservation> {
    const aggregate = await this.reservation
      .getById(req.user.workspace, reservationId);

    if (dateStart || dateEnd) {
      await this.reservation.editDates(
        req.user.workspace,
        reservationId,
        dateStart ? new Date(dateStart) : aggregate.dateStart,
        dateEnd ? new Date(dateEnd) : aggregate.dateEnd,
        req.user.id,
      );
    }
    await Promise.all(aggregate.resourceIds
      .filter(id => resourceIds.indexOf(id) < 0)
      .map(id => this.reservation
        .removeResource(req.user.workspace, reservationId, id, req.user.id),
      ),
    );
    await Promise.all(resourceIds
      .filter(id => aggregate.resourceIds.indexOf(id) > 0)
      .map(id => this.reservation
        .addResource(req.user.workspace, reservationId, id, req.user.id),
      ),
    );
    if (userId != null && userId !== aggregate.userId) {
      await this.reservation
        .assign(req.user.workspace, reservationId, userId, req.user.id);
    }
    return await this.getReservationById(req.user.workspace, reservationId);
  }

  @Patch(':id/confirm')
  @UseInterceptors(TransformerInterceptor)
  public async confirmById(
    @Req() req: AuthorizedRequest,
    @Param('id') reservationId: string,
  ): Promise<Reservation> {
    await this.state.confirm(req.user.workspace, reservationId, req.user.id);
    return await this.getReservationById(req.user.workspace, reservationId);
  }

  @Patch(':id/cancel')
  @UseInterceptors(TransformerInterceptor)
  public async cancelById(
    @Req() req: AuthorizedRequest,
    @Param('id') reservationId: string,
  ): Promise<Reservation> {
    await this.state.cancel(req.user.workspace, reservationId, req.user.id);
    return await this.getReservationById(req.user.workspace, reservationId);
  }

  @Patch(':id/reject')
  @UseInterceptors(TransformerInterceptor)
  public async rejectById(
    @Req() req: AuthorizedRequest,
    @Param('id') reservationId: string,
    @Body('reason') reason: string = '',
  ): Promise<Reservation> {
    await this.state
      .reject(req.user.workspace, reservationId, req.user.id, reason);
    return await this.getReservationById(req.user.workspace, reservationId);
  }

  private async getReservationById(
    worksapceId: string,
    reservationId: string,
  ): Promise<Reservation> {
    const aggregate = await this.reservation
      .getById(worksapceId, reservationId);
    const [reservation] = await this.fillReservations(worksapceId, aggregate);
    return reservation;
  }

  private async fillReservations(
    workspaceId: string,
    ...aggregates: ReservationAggregate[]
  ): Promise<Reservation[]> {
    const states = await Promise.all(aggregates.map(({ id }) =>
      this.state.getById(workspaceId, id)));
    return states.map((state, idx) => ({
      ...aggregates[idx],
      state,
    }));
  }
}
