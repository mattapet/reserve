/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository, Repository, MoreThanOrEqual } from 'typeorm';

import { EventBus } from '../../lib/events/observation/event-bus';
import { ReservationEventEntity } from './reservation-event.entity';
import {
  ReservationEventRepository as Interface,
} from '../reservation-event.repository';
import { ReservationEvent } from '../reservation.event';
//#endregion

@EntityRepository(ReservationEventEntity)
export class ReservationEventRepository
  extends Repository<ReservationEventEntity>
  implements Interface
{
  private readonly eventBus: EventBus = EventBus.instance;

  public async findByRowId(rowId: string): Promise<ReservationEvent[]> {
    const events = await this.find({
      where: { rowId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async findByRowIdAndAggregateId(
    rowId: string,
    aggregateId: string,
  ): Promise<ReservationEvent[]> {
    const events = await this.find({
      where: { rowId, aggregateId },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async findSince(since: Date): Promise<ReservationEvent[]> {
    const events = await this.find({
      where: { timestamp: MoreThanOrEqual(since) },
      order: { timestamp: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async saveEvents(
    ...events: ReservationEvent[]
  ): Promise<ReservationEvent[]> {
    const results = await this.manager.transaction(async (trx) => {
      return await trx.save(ReservationEventEntity, events.map(this.wrap));
    });
    results.forEach(event => this.eventBus.publish(this.unwrap(event)));
    return results.map(this.unwrap);
  }

  private wrap(event: ReservationEvent): ReservationEventEntity {
    const { workspaceId, aggregateId, timestamp } = event;
    return new ReservationEventEntity(
      workspaceId,
      aggregateId,
      timestamp,
      event,
    );
  }

  private unwrap(entity: ReservationEventEntity): ReservationEvent {
    return entity.payload;
  }
}
