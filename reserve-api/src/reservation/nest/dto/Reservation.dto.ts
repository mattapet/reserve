/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationState } from 'lib/reservation/reservation-state.type';
//#endregion

export class ReservationDTO {
  public readonly id!: string;
  public readonly date_start!: string;
  public readonly date_end!: string;
  public readonly user_id!: string;
  public readonly state!: ReservationState;
  public readonly resource_ids!: number[];
}
