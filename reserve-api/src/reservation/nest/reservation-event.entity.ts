/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity } from 'typeorm';

import { BaseEvent } from '../../lib/events/base-event.entity';
import { ReservationEvent as Payload } from '../reservation.event';
//#endregion

@Entity({ name: 'reservation_event' })
export class ReservationEventEntity extends BaseEvent<Payload> { }
