/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  Inject,
  CallHandler,
} from '@nestjs/common';
import { ReservationDTO } from '../dto/Reservation.dto';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { WebhookService } from '../../../webhook/webhook.service';
import {
  AuthorizedRequest,
} from '../../../lib/interface/authorized-request.interface';
import { EventType } from '../../../lib/webhook/event-type';
import { WorkspaceService } from '../../../workspace/nest';
import {
  ReservationState,
} from '../../../lib/reservation/reservation-state.type';
//#endregion

@Injectable()
export class WebhookInterceptor implements NestInterceptor<
  ReservationDTO,
  ReservationDTO
> {
  public constructor(
    @Inject(WebhookService)
    private readonly webhook: WebhookService,
    @Inject(WorkspaceService)
    private readonly workspace: WorkspaceService,
  ) { }

  public intercept(
    context: ExecutionContext,
    call$: CallHandler<ReservationDTO>,
  ): Observable<ReservationDTO> {
    const req = context
      .switchToHttp()
      .getRequest<AuthorizedRequest>();
    const { workspace } = req.user;

    return call$.handle().pipe(
      // Make sure we don't return the promise from the async method call.
      tap(dto => { this.applyWebhooks(workspace, dto); }),
    );
  }

  private async applyWebhooks(workspaceId: string, dto: ReservationDTO) {
    try {
      const eventTypes = this.getEventTypes(dto);
      const workspace = await this.workspace.getById(workspaceId);
      await this.webhook.dispatchEvent(eventTypes, dto, workspace.id);
    } catch (e) {
      // tslint:disable:no-console
      console.error(e);
    }
  }

  private getEventTypes(dto: ReservationDTO): EventType[] {
    switch (dto.state) {
    case ReservationState.requested:
      return [EventType.reservationCreated, EventType.reservation];
    case ReservationState.confirmed:
      return [EventType.reservationConfirmed, EventType.reservation];
    case ReservationState.canceled:
      return [EventType.reservationCanceled, EventType.reservation];
    case ReservationState.rejected:
      return [EventType.reservationRejected, EventType.reservation];
    default: throw new Error('Unreachable');
    }
  }
}
