/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Reservation } from '../reservation.entity';
import { ReservationDTO } from '../dto/Reservation.dto';
//#endregion

@Injectable()
export class TransformerInterceptor implements NestInterceptor<
  Reservation | Reservation[],
  ReservationDTO | ReservationDTO[]
> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<Reservation | Reservation[]>,
  ): Observable<ReservationDTO | ReservationDTO[]> {
    return call$.handle().pipe(
      map((payload: Reservation | Reservation[]) => Array.isArray(payload) ?
        payload.map(this.encode) : this.encode(payload),
      ),
    );
  }

  private encode = (reservation: Reservation) => ({
    id: reservation.id,
    date_start: reservation.dateStart.toISOString(),
    date_end: reservation.dateEnd.toISOString(),
    resource_ids: reservation.resourceIds,
    state: reservation.state,
    user_id: reservation.userId,
  })
}
