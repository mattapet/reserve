/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '../lib/events/command';
//#endregion

export enum ReservationCommandType {
  create = 'create',
  editDates = 'edit_dates',
  assign = 'assign',
  addResource = 'add_resource',
  removeResource = 'remove_resource',
}

interface Create extends Command {
  readonly type: ReservationCommandType.create;
  readonly payload: {
    readonly dateStart: Date;
    readonly dateEnd: Date;
    readonly resources: number[];
    readonly userId: string;
    readonly createdBy: string;
  };
}

interface EditDates extends Command {
  readonly type: ReservationCommandType.editDates;
  readonly payload: {
    readonly dateStart: Date;
    readonly dateEnd: Date;
    readonly changedBy: string;
  };
}

interface Assign extends Command {
  readonly type: ReservationCommandType.assign;
  readonly payload: {
    readonly assignedTo: string;
    readonly changedBy: string;
  };
}

interface AddResource extends Command {
  readonly type: ReservationCommandType.addResource;
  readonly payload: {
    readonly resource: number;
    readonly changedBy: string;
  };
}

interface RemoveResource extends Command {
  readonly type: ReservationCommandType.removeResource;
  readonly payload: {
    readonly resource: number;
    readonly changedBy: string;
  };
}

export type ReservationCommand =
  | Create
  | EditDates
  | Assign
  | AddResource
  | RemoveResource;
