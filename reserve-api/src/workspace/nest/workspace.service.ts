/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';
import { Workspace } from './workspace.entity';
//#endregion

@Injectable()
export class WorkspaceService {
  public constructor(
    @InjectEntityManager()
    private readonly manager: EntityManager,
  ) { }

  public async create(
    workspaceId: string,
    worksapceName?: string,
    trx: EntityManager = this.manager,
  ): Promise<Workspace> {
    const item = new Workspace(workspaceId, worksapceName);
    return await trx.save(item);
  }

  public async getById(
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<Workspace> {
    const item = await trx.findOne(Workspace, workspaceId);
    if (!item) {
      throw new NotFoundException();
    }
    return item;
  }

  public async getByName(
    name: string,
    trx: EntityManager = this.manager,
  ): Promise<Workspace> {
    const item = await trx.findOne(Workspace, { name });
    if (!item) {
      throw new NotFoundException();
    }
    return item;
  }

  public async removeById(
    workspaceId: string,
    trx: EntityManager = this.manager,
  ): Promise<void> {
    const item = await trx.findOne(Workspace, workspaceId);
    await trx.remove(item);
  }

  public async transaction<T>(
    runInTrx: (em: EntityManager) => Promise<T>,
  ): Promise<T> {
    return await this.manager.transaction(runInTrx);
  }
}
