/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Get,
  UseGuards,
  Req,
  Param,
  Delete,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { IdParam } from './id.param';
import { WorkspaceService } from './workspace.service';
import { Workspace } from './workspace.entity';
import { UserRole } from '../../lib/user/user-role.type';
import {
  AuthorizedRequest,
} from '../../lib/interface/authorized-request.interface';
import { RoleGuard } from '../../lib/guards/RoleGuard';
import { NameQuery } from './name.query';
//#endregion

@Controller('api/v1/workspace')
export class WorkspaceController {
  public constructor(
    private readonly service: WorkspaceService,
  ) { }

  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  public async getByName(
    @Query() { name }: NameQuery,
  ): Promise<Workspace> {
    return await this.service.getByName(name);
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(ClassSerializerInterceptor)
  public async getById(
    @Param() { id }: IdParam,
  ): Promise<Workspace> {
    return await this.service.getById(id);
  }

  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(AuthGuard('jwt'))
  public async getCurrent(
    @Req() req: AuthorizedRequest,
  ): Promise<Workspace> {
    return await this.service.getById(req.user.workspace);
  }

  @Delete(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(AuthGuard('jwt'), RoleGuard(UserRole.owner))
  public async deleteWorkspace(
    @Param() { id }: IdParam,
  ): Promise<void> {
    await this.service.removeById(id);
  }
}
