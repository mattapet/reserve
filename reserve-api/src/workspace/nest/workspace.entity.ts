/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Entity,
  Column,
  PrimaryColumn,
} from 'typeorm';
import { Expose } from 'class-transformer';
//#endregion

@Entity()
export class Workspace {
  @Expose()
  @PrimaryColumn()
  public id!: string;

  @Expose()
  @Column({ unique: true })
  public name: string;

  public constructor(id: string, name: string = '') {
    this.id = id;
    this.name = name;
  }
}
