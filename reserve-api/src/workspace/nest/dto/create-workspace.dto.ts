/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Matches,
  IsEmail,
  MinLength,
  IsNotEmpty,
  IsString,
  IsOptional,
} from 'class-validator';
//#endregion

export class CreateWorksapceDTO {
  @Matches(/^[a-zA-Z0-9_-]+$/)
  readonly workspace!: string;

  @IsEmail()
  readonly email!: string;

  @MinLength(5)
  readonly password!: string;

  @IsNotEmpty()
  readonly first_name!: string;

  @IsNotEmpty()
  readonly last_name!: string;

  @IsOptional()
  @IsString()
  readonly phone?: string;
}
