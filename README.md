# Reserve
---
[![build status](https://gitlab.fit.cvut.cz/mattapet/reserve/badges/master/build.svg)](https://gitlab.fit.cvut.cz/mattapet/reserve/commits/master)

Reserve is a web application that provides creating custom reservation systems
and their management.
