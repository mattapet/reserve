# Reserve
---
[![pipeline status](https://gitlab.com/mattapet/reserve/badges/master/pipeline.svg)](https://gitlab.com/mattapet/reserve/-/commits/master)

[![coverage report](https://gitlab.com/mattapet/reserve/badges/master/coverage.svg)](https://gitlab.com/mattapet/reserve/-/commits/master)

Reserve is a web application that provides creating custom reservation systems
and their management.
