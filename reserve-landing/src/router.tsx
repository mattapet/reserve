/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Spinner from '@reserve/landing/components/Spinner';

const landingApp = React.lazy(() => import('./landingApp'));
const consoleApp = React.lazy(() => import('./consoleApp'));
//#endregion

const Router: React.FunctionComponent = () => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      <Route path="/console" component={consoleApp} />
      <Route component={landingApp} />
    </Switch>
  </React.Suspense>
);

export default Router;
