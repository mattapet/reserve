/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useContext, useEffect, useState } from 'react';
import * as R from 'ramda';
import { Table, PageHeader, message } from 'antd';

import { WorkspaceRequestContext } from '../../stores/WorkspaceRequestsStore';

import { columns } from './columns';
//#endregion

const Workspaces: React.FunctionComponent = () => {
  const { retrieved, fetchAll, confirm, decline, requests } = useContext(
    WorkspaceRequestContext
  );
  const [pending, setPending] = useState(false);

  useEffect(() => {
    if (!retrieved) {
      fetchAll();
    }
  }, [retrieved, fetchAll]);

  async function handleConfirm(workspaceId: string): Promise<void> {
    try {
      setPending(true);
      await confirm(workspaceId);
      setPending(false);
      message.success('Workspace request have been confirmed');
    } catch (error) {
      message.error(error.message);
      setPending(false);
    }
  }

  async function handleDecline(workspaceId: string): Promise<void> {
    try {
      setPending(true);
      await decline(workspaceId);
      setPending(false);
      message.success('Workspace request have been declined');
    } catch (error) {
      message.error(error.message);
      setPending(false);
    }
  }

  return (
    <React.Fragment>
      <PageHeader title="Workspace requests">
        <Table
          loading={!retrieved || pending}
          rowKey={R.prop('workspaceId')}
          dataSource={R.values(requests)}
          columns={columns(handleConfirm, handleDecline)}
        />
      </PageHeader>
    </React.Fragment>
  );
};

export default Workspaces;
