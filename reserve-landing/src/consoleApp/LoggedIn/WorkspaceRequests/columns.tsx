/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import moment from 'moment';
import { ColumnProps } from 'antd/lib/table';

import RequestStateColumn from './components/RequestStateColumn';
import RequestActionSelect from './components/RequestActionSelect';
import { WorkspaceRequest } from '../../types/WorkspaceRequest';
//#endregion

export const columns = (
  onConfirm: (workspaceId: string) => any,
  onDecline: (workspaceId: string) => any
): ColumnProps<WorkspaceRequest>[] => [
  {
    title: 'ID',
    dataIndex: 'workspaceId',
  },
  {
    title: 'Name',
    dataIndex: 'workspaceName',
  },
  {
    title: 'State',
    render: (_, item) => <RequestStateColumn request={item} />,
  },
  {
    title: 'Opened At',
    render: (_, item) => moment(item.openedAt).format('MM/DD/YYYY HH:mm'),
  },
  {
    title: 'Closed At',
    render: (_, item) =>
      item.closedAt && moment(item.closedAt).format('MM/DD/YYYY HH:mm'),
  },
  {
    title: 'Requested By',
    render: (_, item) => (
      <a href={`mailto:${item.ownerEmail}`}>{item.ownerEmail}</a>
    ),
  },
  {
    title: 'Actions',
    render: (_, item) => (
      <RequestActionSelect
        request={item}
        onConfirm={onConfirm}
        onDecline={onDecline}
      />
    ),
  },
];
