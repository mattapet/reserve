/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Tag } from 'antd';

import { WorkspaceRequest } from '../../../types/WorkspaceRequest';
import { WorkspaceRequestState } from '../../../types/WorkspaceRequestState';
//#endregion

export interface Props {
  readonly request: WorkspaceRequest;
}

function getTagColorForState(
  state: WorkspaceRequestState
): 'processing' | 'success' | 'error' {
  switch (state) {
    case WorkspaceRequestState.opened:
      return 'processing';
    case WorkspaceRequestState.confirmed:
      return 'success';
    case WorkspaceRequestState.declined:
      return 'error';
  }
}

const RequestStateColumn: React.FunctionComponent<Props> = props => (
  <Tag color={getTagColorForState(props.request.state)}>
    {props.request.state.toUpperCase()}
  </Tag>
);

export default RequestStateColumn;
