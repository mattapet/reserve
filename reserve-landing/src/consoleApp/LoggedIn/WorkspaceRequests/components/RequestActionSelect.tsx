/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Menu, Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';

import { WorkspaceRequest } from '../../../types/WorkspaceRequest';
import { WorkspaceRequestState } from '../../../types/WorkspaceRequestState';
//#endregion

export interface Props {
  readonly request: WorkspaceRequest;
  readonly onConfirm: (reservationId: string) => any;
  readonly onDecline: (reservationId: string) => any;
}

const RequestActionSelect: React.FunctionComponent<Props> = props =>
  props.request.state === WorkspaceRequestState.opened ? (
    <Dropdown
      trigger={['click']}
      overlay={
        <Menu selectedKeys={[]}>
          <Menu.Item
            key="confirm"
            onClick={() => props.onConfirm(props.request.workspaceId)}
          >
            Confirm
          </Menu.Item>
          <Menu.Item
            key="decline"
            onClick={() => props.onDecline(props.request.workspaceId)}
          >
            Decline
          </Menu.Item>
        </Menu>
      }
    >
      {/* eslint-disable-next-line */}
      <a href="javascript:;">
        Action <DownOutlined />
      </a>
    </Dropdown>
  ) : null;

export default RequestActionSelect;
