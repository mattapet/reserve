/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
import Spinner from '@reserve/landing/components/Spinner';

const Workspaces = React.lazy(() => import('./Workspaces'));
const WorkspaceRequests = React.lazy(() => import('./WorkspaceRequests'));
const Accounts = React.lazy(() => import('./Accounts'));
const Logout = React.lazy(() => import('./Logout'));
//#endregion

const Router: React.FunctionComponent = () => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      <Redirect from="/console/login" to="/console/workspaces" exact />
      <Redirect from="/console" to="/console/workspaces" exact />
      <Route path="/console/logout" component={Logout} exact />
      <Route path="/console/workspaces" component={Workspaces} />
      <Route path="/console/requests" component={WorkspaceRequests} />
      <Route path="/console/accounts" component={Accounts} />
    </Switch>
  </React.Suspense>
);

export default Router;
