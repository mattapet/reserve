/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Switch, Route } from 'react-router';

import WorkspaceList from './screens/WorkspaceList';
import WorkspaceDetails from './screens/WorkspaceDetails';
import WorkspaceStore from '../../stores/WorkspaceStore';
//#endregion

const Router: React.FunctionComponent = () => (
  <WorkspaceStore>
    <Switch>
      <Route path="/console/workspaces" component={WorkspaceList} exact />
      <Route
        path="/console/workspaces/:id"
        component={WorkspaceDetails}
        exact
      />
    </Switch>
  </WorkspaceStore>
);

export default Router;
