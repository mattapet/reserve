/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React, { useContext, useEffect, useState } from 'react';
import { Select, Form, message } from 'antd';
import { UserContext } from '../../../../stores/UserStore';

export interface Props {
  readonly ownerId: string;
  readonly workspaceId: string;
  readonly onChange: (ownerId: string) => Promise<void>;
}

const WorkspaceOwnerSelect: React.FunctionComponent<Props> = (props) => {
  const { users, retrieved, fetchByWorkspaceId } = useContext(UserContext);
  const [selected, setSelected] = useState(props.ownerId);
  const [pending, setPending] = useState(false);

  useEffect(() => {
    if (!retrieved) {
      fetchByWorkspaceId(props.workspaceId);
    }
  }, [fetchByWorkspaceId, retrieved, props.workspaceId]);

  async function handleChange(newOwner: string) {
    if (newOwner === selected) {
      return;
    }

    setSelected(newOwner);
    setPending(true);
    try {
      await props.onChange(newOwner);
      message.success(
        `Workspace ownership successfully assigned to ${users[newOwner].firstName} ${users[newOwner].lastName}`
      );
      setPending(false);
    } catch (error) {
      message.error(error.message);
      setSelected(props.ownerId);
      setPending(false);
    }
  }

  return (
    <Form>
      <Form.Item label="Workspace Owner">
        <Select
          value={selected}
          onChange={handleChange}
          loading={!retrieved || pending}
          disabled={!retrieved || pending}
        >
          {Object.values(users).map((user) => (
            <Select.Option key={user.id} value={user.id}>
              {`${user.firstName} ${user.lastName}`}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
    </Form>
  );
};

export default WorkspaceOwnerSelect;
