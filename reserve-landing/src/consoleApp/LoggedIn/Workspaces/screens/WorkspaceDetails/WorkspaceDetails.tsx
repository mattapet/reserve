/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useContext, useEffect } from 'react';
import { PageHeader, Card } from 'antd';

import { useHistory, useParams } from 'react-router';
import { WorkspaceContext } from '../../../../stores';
import WorkspaceOwnerSelect from './WorkspaceOwnerSelect';
//#endregion

const WorkspaceDetails: React.FunctionComponent = () => {
  const { workspaces, retrieved, fetchAll, transferOwnership } = useContext(
    WorkspaceContext
  );
  const history = useHistory();
  const { id } = useParams();
  const workspace = workspaces[id!];

  function handleBack() {
    history.push('/console/workspaces');
  }

  useEffect(() => {
    if (!retrieved) {
      console.log('FETCHING');
      fetchAll();
    }
  }, [retrieved, fetchAll]);

  async function handleTransferOwnership(to: string) {
    await transferOwnership(id!, to);
  }
  console.log(retrieved);

  return (
    <PageHeader title="Workspaces" subTitle="Details" onBack={handleBack}>
      <Card loading={!retrieved}>
        <h1>Workspace details</h1>

        <WorkspaceOwnerSelect
          onChange={handleTransferOwnership}
          ownerId={workspace?.ownerId}
          workspaceId={id!}
        />
      </Card>
    </PageHeader>
  );
};

export default WorkspaceDetails;
