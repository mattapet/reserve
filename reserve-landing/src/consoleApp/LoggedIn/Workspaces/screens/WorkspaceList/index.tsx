/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useContext, useEffect } from 'react';
import * as R from 'ramda';
import { Table, PageHeader } from 'antd';

import { WorkspaceContext } from '../../../../stores/WorkspaceStore';

import { columns } from './columns';
//#endregion

const WorkspaceList: React.FunctionComponent = () => {
  const { retrieved, fetchAll, workspaces } = useContext(WorkspaceContext);

  useEffect(() => {
    if (!retrieved) {
      fetchAll();
    }
  }, [retrieved, fetchAll]);

  return (
    <React.Fragment>
      <PageHeader title="Workspaces">
        <Table
          loading={!retrieved}
          rowKey={R.prop('id')}
          dataSource={R.values(workspaces)}
          columns={columns()}
        />
      </PageHeader>
    </React.Fragment>
  );
};

export default WorkspaceList;
