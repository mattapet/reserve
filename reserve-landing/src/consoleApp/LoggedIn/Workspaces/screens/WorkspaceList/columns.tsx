/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import config from '@reserve/landing/config';
import { ColumnProps } from 'antd/lib/table';
import { Workspace } from '../../../../types/Workspace';
import { Link } from 'react-router-dom';
//#endregion

export const columns = (): ColumnProps<Workspace>[] => [
  {
    title: 'ID',
    dataIndex: 'id',
  },
  {
    title: 'Name',
    dataIndex: 'name',
  },
  {
    title: 'Owner',
    dataIndex: 'ownerEmail',
    render: (_, item) => (
      <a href={`mailto:${item.ownerEmail}`}>{item.ownerEmail}</a>
    ),
  },
  {
    title: 'URL',
    render: (_, item) => (
      <a
        href={`${config.protocol}//${item.name}.${config.baseHost}`}
        rel="noopener noreferrer"
        target="_blank"
      >{`${config.protocol}//${item.name}.${config.baseHost}`}</a>
    ),
  },
  {
    title: 'Details',
    render: (_, item) => (
      <Link to={`/console/workspaces/${item.id}`}>Details</Link>
    ),
  },
];
