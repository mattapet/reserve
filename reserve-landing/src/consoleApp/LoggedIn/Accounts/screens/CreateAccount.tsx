/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState, useContext } from 'react';
import { message } from 'antd';
import { useHistory } from 'react-router';

import FullPageCard from '@reserve/landing/components/FullPageCard';
import FormContainer from '@reserve/landing/components/FormContainer';
import Input, { Password } from '@reserve/landing/components/Input';
import FullWidthButton from '@reserve/landing/components/FullWidthButton';

import { AccountContext } from '../../../stores/AccountsStore';
//#endregion

const CreateAccount: React.FunctionComponent = () => {
  const { createAccount } = useContext(AccountContext);
  const history = useHistory();
  const [pending, setPending] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordVerify, setPasswordVerify] = useState('');

  async function handleSubmit(e: React.FormEvent) {
    e.preventDefault();
    try {
      setPending(true);
      await createAccount(username, password);
      message.success('Account created successfully ');
      history.replace('/console/accounts');
    } catch (error) {
      message.error(error.message);
      setPending(false);
    }
  }

  function handleCancel() {
    history.replace('/console/accounts');
  }

  return (
    <FullPageCard title="Create new console account">
      <FormContainer onSubmitCapture={handleSubmit}>
        <Input
          placeholder="Username"
          value={username}
          onChange={setUsername}
          required
          autoFocus
        />
        <Password
          placeholder="Password"
          value={password}
          onChange={setPassword}
          required
        />
        <Password
          placeholder="Password Verify"
          value={passwordVerify}
          onChange={setPasswordVerify}
          required
        />
        <FullWidthButton
          type="primary"
          htmlType="submit"
          loading={pending}
          disabled={
            !(username.length && password.length && password === passwordVerify)
          }
        >
          Submit
        </FullWidthButton>
        <FullWidthButton onClick={handleCancel}>Cancel</FullWidthButton>
      </FormContainer>
    </FullPageCard>
  );
};

export default CreateAccount;
