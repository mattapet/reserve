/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */

//#region imports
import React from 'react';
import { Popconfirm } from 'antd';
import { ColumnProps } from 'antd/lib/table';
import { Account } from '../../../../types/Account';
//#endregion

export const columns = (
  loggedInAccountUsername: string,
  onRemove: (username: string) => any
): ColumnProps<Account>[] => [
  {
    title: 'Username',
    dataIndex: 'username',
  },
  {
    title: 'Actions',
    width: 100,
    fixed: 'right',
    render: (_, item) =>
      item.username !== loggedInAccountUsername && (
        <Popconfirm
          title="Are you sure?"
          onConfirm={() => onRemove(item.username)}
          okType="danger"
          okText="Yes"
          cancelText="No"
        >
          <a href="javascript:;">Delete</a>
        </Popconfirm>
      ),
  },
];
