/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react';
import { Link } from 'react-router-dom';
import { FormOutlined } from '@ant-design/icons';

const CreateAccountLink: React.FunctionComponent = () => (
  <Link to="/console/accounts/create">
    <FormOutlined /> Create new account
  </Link>
);

export default CreateAccountLink;
