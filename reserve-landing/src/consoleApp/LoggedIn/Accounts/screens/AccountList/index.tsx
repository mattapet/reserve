/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as R from 'ramda';
import React, { useContext, useEffect, useState } from 'react';
import { message } from 'antd';
import { PageHeader, Table } from 'antd';

import { AccountContext } from '../../../../stores/AccountsStore';
import { AuthenticationContext } from '../../../../stores/AuthenticationStore';

import CreateNewAccount from './components/CreateAccountLink';
import { columns } from './columns';
//#endregion

const Accounts: React.FunctionComponent = () => {
  const { username } = useContext(AuthenticationContext);
  const { accounts, fetchAll, removeAccount, retrieved } = useContext(
    AccountContext
  );
  const [pending, setPending] = useState(false);

  useEffect(() => {
    if (!retrieved) {
      fetchAll();
    }
  }, [retrieved, fetchAll]);

  async function handleRemove(username: string): Promise<void> {
    try {
      setPending(true);
      await removeAccount(username);
      message.success('Account removed successfully');
      setPending(false);
    } catch (error) {
      message.error(error.message);
      setPending(false);
    }
  }

  return (
    <PageHeader title="Accounts" extra={[<CreateNewAccount />]}>
      <Table
        loading={!retrieved || pending}
        rowKey={R.prop('username')}
        dataSource={R.values(accounts)}
        columns={columns(username ?? '', handleRemove)}
      />
    </PageHeader>
  );
};

export default Accounts;
