/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Spinner from '../../../components/Spinner';

const AccountList = React.lazy(() => import('./screens/AccountList'));
const CreateAccount = React.lazy(() => import('./screens/CreateAccount'));
//#endregion

const Router: React.FunctionComponent = () => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      <Route path="/console/accounts" component={AccountList} exact />
      <Route path="/console/accounts/create" component={CreateAccount} exact />
    </Switch>
  </React.Suspense>
);

export default Router;
