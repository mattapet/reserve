/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useEffect, useContext } from 'react';
import Spinner from '@reserve/landing/components/Spinner';
import { AuthenticationContext } from '../stores';
//#endregion

const Logout: React.FunctionComponent = () => {
  const { logout } = useContext(AuthenticationContext);

  useEffect(() => {
    logout();
  }, [logout]);

  return <Spinner />;
};

export default Logout;
