/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Button, Layout, Menu } from 'antd';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { LogoutOutlined } from '@ant-design/icons';
//#endregion

const LayoutHeader = styled(Layout.Header)`
  display: flex;
  justify-content: space-between;
`;

const TitleItem = styled(Menu.Item)`
  font-size: 36px;
`;

const MenuBar = styled(Menu)`
  float: left;
`;

const LogoutButton = styled(Button)`
  justify-self: center;
  align-self: center;
`;

const Header: React.FunctionComponent<RouteComponentProps<any>> = (props) => (
  <LayoutHeader>
    <MenuBar
      theme="dark"
      mode="horizontal"
      selectedKeys={[props.location.pathname.split('/')[2]]}
    >
      <TitleItem>
        <Link to="/console">Reserve Console</Link>
      </TitleItem>

      <Menu.Item key="workspaces">
        <Link to="/console/workspaces">Workspaces</Link>
      </Menu.Item>
      <Menu.Item key="requests">
        <Link to="/console/requests">Requests</Link>
      </Menu.Item>
      <Menu.Item key="accounts">
        <Link to="/console/accounts">Accounts</Link>
      </Menu.Item>
    </MenuBar>

    <Link to="/console/logout">
      <LogoutButton icon={<LogoutOutlined />} ghost={true}>
        Log out
      </LogoutButton>
    </Link>
  </LayoutHeader>
);

export default withRouter(Header);
