/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Layout } from 'antd';

import Header from './components/Header';
import Router from './router';
//#endregion

const LoggedIn: React.FunctionComponent = () => (
  <Layout>
    <Header />
    <Layout.Content>
      <Router />
    </Layout.Content>
  </Layout>
);

export default LoggedIn;
