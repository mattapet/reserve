/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { WorkspaceDTO } from '../../types/Workspace.dto';
import { Workspace } from '../../types/Workspace';

export function decode(dto: WorkspaceDTO): Workspace {
  return {
    id: dto.id,
    name: dto.name,
    ownerId: dto.owner_id,
    ownerEmail: dto.owner_email,
  };
}
