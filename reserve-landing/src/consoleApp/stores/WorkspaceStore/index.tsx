/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState, useContext } from 'react';

import config from '@reserve/landing/config';

import { Workspace } from '../../types/Workspace';
import { WorkspaceDTO } from '../../types/Workspace.dto';
import { AuthenticationContext } from '../AuthenticationStore';
import { decode } from './decode';
//#endregion

export interface WorkspaceContextProps {
  readonly workspaces: { [id: string]: Workspace };
  readonly retrieved: boolean;
  readonly fetchAll: () => Promise<void>;
  readonly transferOwnership: (
    workspaceId: string,
    to: string
  ) => Promise<void>;
}

export const WorkspaceContext = React.createContext<WorkspaceContextProps>({
  workspaces: {},
  retrieved: false,
  fetchAll: () => Promise.resolve(),
  transferOwnership: () => Promise.resolve(),
});

const WorkspaceStore: React.FunctionComponent = (props) => {
  const { authRequest } = useContext(AuthenticationContext);
  const [retrieved, setRetrieved] = useState(false);
  const [workspaces, setWorkspaces] = useState<{ [id: string]: Workspace }>({});

  async function fetchAll(): Promise<void> {
    console.log('FETCHING');
    const response = await authRequest()
      .get(`${config.apiUrl}api/v1/console/workspace`)
      .send();

    if (response.status !== 200) {
      setRetrieved(true);
      throw new Error(await response.text());
    }
    const data: WorkspaceDTO[] = await response.json();
    setWorkspaces(
      data.map(decode).reduce((acc, next) => ({ ...acc, [next.id]: next }), {})
    );
    console.log(data);
    setRetrieved(true);
  }

  async function transferOwnership(
    workspaceId: string,
    to: string
  ): Promise<void> {
    const response = await authRequest()
      .post(`${config.apiUrl}api/v1/console/workspace/${workspaceId}/transfer`)
      .set('Content-Type', 'application/json')
      .send(JSON.stringify({ to }));

    if (response.status !== 200) {
      throw new Error(await response.text());
    }
    const data: WorkspaceDTO = await response.json();
    setWorkspaces({ ...workspaces, [data.id]: decode(data) });
    setRetrieved(true);
  }

  return (
    <WorkspaceContext.Provider
      value={{ workspaces, retrieved, fetchAll, transferOwnership }}
    >
      {props.children}
    </WorkspaceContext.Provider>
  );
};

export default WorkspaceStore;
