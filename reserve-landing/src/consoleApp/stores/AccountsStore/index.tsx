/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState, useContext } from 'react';

import config from '@reserve/landing/config';

import { Account } from '../../types/Account';
import { AuthenticationContext } from '../AuthenticationStore';
//#endregion

export interface AccountContextProps {
  readonly accounts: { [usename: string]: Account };
  readonly fetchAll: () => Promise<void>;
  readonly createAccount: (username: string, password: string) => Promise<void>;
  readonly removeAccount: (username: string) => Promise<void>;
  readonly retrieved: boolean;
}

export const AccountContext = React.createContext<AccountContextProps>({
  accounts: {},
  fetchAll: () => Promise.resolve(),
  createAccount: () => Promise.resolve(),
  removeAccount: () => Promise.resolve(),
  retrieved: false,
});

const AccountStore: React.FunctionComponent = (props) => {
  const { authRequest } = useContext(AuthenticationContext);
  const [retrieved, setRetrieved] = useState(false);
  const [accounts, setAccounts] = useState<{ [username: string]: Account }>({});

  async function createAccount(
    username: string,
    password: string
  ): Promise<void> {
    const response = await authRequest()
      .post(`${config.apiUrl}api/v1/console/auth/register`)
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .send(JSON.stringify({ username, password }));

    if (response.status !== 201) {
      throw new Error(await response.text());
    }
    const account: Account = await response.json();
    setAccounts({ ...accounts, [account.username]: account });
  }

  async function removeAccount(username: string): Promise<void> {
    const response = await authRequest()
      .delete(`${config.apiUrl}api/v1/console/account/${username}`)
      .send();

    if (response.status !== 204) {
      throw new Error(await response.text());
    }
    const { [username]: deleted, ...rest } = accounts;
    setAccounts(rest);
  }

  async function fetchAll(): Promise<void> {
    const response = await authRequest()
      .get(`${config.apiUrl}api/v1/console/account`)
      .send();

    if (response.status !== 200) {
      setRetrieved(true);
      throw new Error(await response.text());
    }
    const data: Account[] = await response.json();
    setAccounts(
      data.reduce((acc, next) => ({ ...acc, [next.username]: next }), {})
    );
    setRetrieved(true);
  }

  return (
    <AccountContext.Provider
      value={{ accounts, fetchAll, createAccount, removeAccount, retrieved }}
    >
      {props.children}
    </AccountContext.Provider>
  );
};

export default AccountStore;
