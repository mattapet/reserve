/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  RequestBuilder,
  RequestMethodPicker,
  request,
} from '@reserve/landing/lib/request';
import { Credentials, requestRefreshToken } from './actions';
//#endregion

class AuthorizedRequestMethodPicker implements RequestMethodPicker {
  public constructor(
    private readonly wrapped: RequestMethodPicker,
    private readonly onRefresh: () => Promise<string | undefined>,
    private readonly accessToken: string
  ) {}

  public get(url: string): RequestBuilder {
    return new AuthorizedRequestBuilder(
      this.wrapped.get(url),
      this.onRefresh,
      this.accessToken
    );
  }

  public post(url: string): RequestBuilder {
    return new AuthorizedRequestBuilder(
      this.wrapped.post(url),
      this.onRefresh,
      this.accessToken
    );
  }

  public put(url: string): RequestBuilder {
    return new AuthorizedRequestBuilder(
      this.wrapped.put(url),
      this.onRefresh,
      this.accessToken
    );
  }

  public delete(url: string): RequestBuilder {
    return new AuthorizedRequestBuilder(
      this.wrapped.delete(url),
      this.onRefresh,
      this.accessToken
    );
  }
}

class AuthorizedRequestBuilder implements RequestBuilder {
  public constructor(
    private readonly wrapped: RequestBuilder,
    private readonly onRefresh: () => Promise<string | undefined>,
    private readonly accessToken: string
  ) {}

  public set(key: string, value: string): RequestBuilder {
    this.wrapped.set(key, value);
    return this;
  }

  public async send(body?: string): Promise<Response> {
    const response = await this.wrapped
      .set('Authorization', `Bearer ${this.accessToken}`)
      .send(body);

    if (response.status === 401) {
      const accessToken = await this.onRefresh();
      if (!accessToken) {
        return response;
      }

      return this.wrapped
        .set('Authorization', `Bearer ${accessToken}`)
        .send(body);
    } else {
      return response;
    }
  }
}

export function authorizedRequest(
  credentials: Credentials,
  onCredentialsRefresh: (creds: Credentials | null) => void
): RequestMethodPicker {
  async function refreshToken(): Promise<string | undefined> {
    try {
      const newCreds = await requestRefreshToken(credentials.refresh_token);
      onCredentialsRefresh(newCreds);
      return newCreds.access_token;
    } catch (error) {
      onCredentialsRefresh(null);
      return undefined;
    }
  }

  return new AuthorizedRequestMethodPicker(
    request(),
    refreshToken,
    credentials.access_token
  );
}
