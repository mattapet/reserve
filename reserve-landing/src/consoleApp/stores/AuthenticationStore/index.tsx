/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';
import jwt from 'jsonwebtoken';

import { RequestMethodPicker } from '@reserve/landing/lib/request';

import { authorizedRequest } from './authorized-request';
import { loginRequest, requestLogout, Credentials } from './actions';
//#endregion

export interface AuthenticationContextProps {
  readonly credentials: Credentials | null;
  readonly username: string | null;
  readonly login: (username: string, password: string) => Promise<void>;
  readonly logout: () => Promise<void>;
  readonly authRequest: () => RequestMethodPicker;
  readonly isLoggedIn: boolean;
}

const creds: Credentials | null = JSON.parse(
  localStorage.getItem('credentials') ?? 'null'
);
const user: string | null =
  creds && (jwt.decode(creds!.access_token) as any)?.user;

export const AuthenticationContext = React.createContext<
  AuthenticationContextProps
>({
  credentials: creds,
  username: user,
  login: () => Promise.resolve(),
  logout: () => Promise.resolve(),
  authRequest: () => ({} as any),
  isLoggedIn: false,
});

const AuthenticationStore: React.FunctionComponent = props => {
  const [credentials, setCredentials] = useState<Credentials | null>(creds);
  const [username, setUsername] = useState<string | null>(user);
  const isLoggedIn = credentials != null;

  function updateCredentials(credentials: Credentials | null): void {
    setCredentials(credentials);
    if (credentials != null) {
      setUsername((jwt.decode(credentials.access_token) as any)?.user);
      localStorage.setItem('credentials', JSON.stringify(credentials));
    } else {
      localStorage.removeItem('credentials');
    }
  }

  async function login(username: string, password: string): Promise<void> {
    const credentials = await loginRequest(username, password);
    updateCredentials(credentials);
  }

  async function logout(): Promise<void> {
    if (!credentials) {
      return;
    }

    await requestLogout(credentials);
    setCredentials(null);
    setCredentials(null);
    localStorage.removeItem('credentials');
  }

  function authRequest() {
    if (!credentials) {
      throw new Error('Unauthorized');
    }

    return authorizedRequest(credentials, updateCredentials);
  }

  return (
    <AuthenticationContext.Provider
      value={{ credentials, username, login, logout, authRequest, isLoggedIn }}
    >
      {props.children}
    </AuthenticationContext.Provider>
  );
};

export default AuthenticationStore;
