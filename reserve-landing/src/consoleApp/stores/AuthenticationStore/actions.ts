/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import config from '@reserve/landing/config';
import { request } from '@reserve/landing/lib/request';
//#endregion

export interface Credentials {
  readonly access_token: string;
  readonly refresh_token: string;
}

export async function loginRequest(
  username: string,
  password: string
): Promise<Credentials> {
  const response = await request()
    .post(`${config.apiUrl}api/v1/console/auth/login`)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .send(JSON.stringify({ username, password }));

  if (response.status !== 200) {
    throw new Error(await response.text());
  }
  return response.json();
}

export async function requestLogout({
  refresh_token,
}: Credentials): Promise<void> {
  const response = await request()
    .post(`${config.apiUrl}api/v1/console/auth/token/revoke`)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .send(JSON.stringify({ refresh_token }));

  if (response.status !== 204) {
    throw new Error(await response.text());
  }
}

export async function requestRefreshToken(
  refreshToken: string
): Promise<Credentials> {
  const response = await request()
    .post(`${config.apiUrl}api/v1/console/auth/token/refresh`)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .send(JSON.stringify({ refresh_token: refreshToken }));

  if (response.status !== 200) {
    throw new Error(await response.text());
  }
  return response.json();
}
