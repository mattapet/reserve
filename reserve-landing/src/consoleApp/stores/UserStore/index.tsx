/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState, useContext } from 'react';

import config from '@reserve/landing/config';
import { User } from '../../types/User';
import { AuthenticationContext } from '../AuthenticationStore';
import { UserDTO } from '../../types/User.dto';
import { decode } from './decode';
//#endregion

export interface UserContextProps {
  readonly users: { [id: string]: User };
  readonly retrieved: boolean;
  readonly fetchByWorkspaceId: (workspaceId: string) => Promise<void>;
}

export const UserContext = React.createContext<UserContextProps>({
  users: {},
  retrieved: true,
  fetchByWorkspaceId: () => Promise.resolve(),
});

const UserStore: React.FunctionComponent = (props) => {
  const { authRequest } = useContext(AuthenticationContext);
  const [users, setUsers] = useState<{ [id: string]: User }>({});
  const [retrieved, setRetrieved] = useState(false);

  async function fetchByWorkspaceId(workspaceId: string): Promise<void> {
    const response = await authRequest()
      .get(`${config.apiUrl}api/v1/console/workspace/${workspaceId}/user`)
      .send();

    if (response.status !== 200) {
      setRetrieved(true);
      throw new Error(await response.text());
    }

    const data: UserDTO[] = await response.json();
    setUsers(
      data.map(decode).reduce((acc, next) => ({ ...acc, [next.id]: next }), {})
    );
    setRetrieved(true);
  }

  return (
    <UserContext.Provider value={{ users, retrieved, fetchByWorkspaceId }}>
      {props.children}
    </UserContext.Provider>
  );
};

export default UserStore;
