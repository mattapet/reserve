/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { UserDTO } from '../../types/User.dto';
import { User } from '../../types/User';

export function decode(dto: UserDTO): User {
  return {
    id: dto.id,
    workspaceId: dto.workspace_id,
    email: dto.email,
    firstName: dto.first_name,
    lastName: dto.last_name,
    isOwner: dto.is_owner,
  };
}
