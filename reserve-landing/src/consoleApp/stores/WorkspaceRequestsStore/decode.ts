/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { WorkspaceRequest } from '../../types/WorkspaceRequest';
import { WorkspaceRequestDTO } from '../../types/WorkspaceRequest.dto';

export function decode(dto: WorkspaceRequestDTO): WorkspaceRequest {
  return {
    workspaceId: dto.workspace_id,
    workspaceName: dto.workspace_name,
    state: dto.state,
    ownerId: dto.owner_id,
    ownerEmail: dto.owner_email,
    openedAt: new Date(dto.opened_at),
    closedAt: dto.closed_at ? new Date(dto.closed_at) : undefined,
  };
}
