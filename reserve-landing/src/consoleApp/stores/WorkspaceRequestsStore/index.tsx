/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState, useContext } from 'react';

import config from '@reserve/landing/config';

import { decode } from './decode';
import { WorkspaceRequest } from '../../types/WorkspaceRequest';
import { WorkspaceRequestDTO } from '../../types/WorkspaceRequest.dto';
import { AuthenticationContext } from '../AuthenticationStore';
//#endregion

export interface WorkspaceRequestContextProps {
  readonly requests: { [id: string]: WorkspaceRequest };
  readonly retrieved: boolean;
  readonly fetchAll: () => Promise<void>;
  readonly confirm: (workspaceId: string) => Promise<void>;
  readonly decline: (workspaceId: string) => Promise<void>;
}

export const WorkspaceRequestContext = React.createContext<
  WorkspaceRequestContextProps
>({
  requests: {},
  retrieved: false,
  fetchAll: () => Promise.resolve(),
  confirm: () => Promise.resolve(),
  decline: () => Promise.resolve(),
});

const WorkspaceRequestStore: React.FunctionComponent = (props) => {
  const { authRequest } = useContext(AuthenticationContext);
  const [retrieved, setRetrieved] = useState(false);
  const [requests, setRequests] = useState<{ [id: string]: WorkspaceRequest }>(
    {}
  );

  async function fetchAll(): Promise<void> {
    const response = await authRequest()
      .get(`${config.apiUrl}api/v1/console/workspace/request`)
      .send();

    if (response.status !== 200) {
      setRetrieved(true);
      throw new Error(await response.text());
    }
    const data: WorkspaceRequestDTO[] = await response.json();
    setRequests(
      data
        .map(decode)
        .reduce((acc, next) => ({ ...acc, [next.workspaceId]: next }), {})
    );
    setRetrieved(true);
  }

  async function confirm(workspaceId: string): Promise<void> {
    const response = await authRequest()
      .post(
        `${config.apiUrl}api/v1/console/workspace/request/${workspaceId}/confirm`
      )
      .set('Content-Type', 'application/json')
      .send();

    if (response.status !== 200) {
      throw new Error(await response.text());
    }
    const data: WorkspaceRequest = decode(await response.json());
    setRequests({ ...requests, [data.workspaceId]: data });
  }

  async function decline(workspaceId: string): Promise<void> {
    const response = await authRequest()
      .post(
        `${config.apiUrl}api/v1/console/workspace/request/${workspaceId}/decline`
      )
      .set('Content-Type', 'application/json')
      .send();

    if (response.status !== 200) {
      throw new Error(await response.text());
    }
    const data: WorkspaceRequest = decode(await response.json());
    setRequests({ ...requests, [data.workspaceId]: data });
  }

  return (
    <WorkspaceRequestContext.Provider
      value={{ requests, retrieved, fetchAll, confirm, decline }}
    >
      {props.children}
    </WorkspaceRequestContext.Provider>
  );
};

export default WorkspaceRequestStore;
