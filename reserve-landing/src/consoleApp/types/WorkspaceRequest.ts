/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { WorkspaceRequestState } from './WorkspaceRequestState';

export interface WorkspaceRequest {
  readonly workspaceId: string;
  readonly workspaceName: string;
  readonly ownerId: string;
  readonly ownerEmail?: string;
  readonly state: WorkspaceRequestState;
  readonly openedAt: Date;
  readonly closedAt?: Date;
}
