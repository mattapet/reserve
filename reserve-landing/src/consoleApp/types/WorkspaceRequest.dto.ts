/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { WorkspaceRequestState } from './WorkspaceRequestState';

export interface WorkspaceRequestDTO {
  readonly workspace_id: string;
  readonly workspace_name: string;
  readonly owner_id: string;
  readonly owner_email?: string;
  readonly state: WorkspaceRequestState;
  readonly opened_at: string;
  readonly closed_at?: string;
}
