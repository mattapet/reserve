/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useContext } from 'react';

import LoggedIn from './LoggedIn';
import LoggedOut from './LoggedOut';
import { AuthenticationContext } from './stores/AuthenticationStore';
//#endregion

const ConsoleApp: React.FunctionComponent = props => {
  const { isLoggedIn } = useContext(AuthenticationContext);
  return isLoggedIn ? <LoggedIn /> : <LoggedOut />;
};

export default ConsoleApp;
