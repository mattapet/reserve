/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';
import Login from './Login';

const Router: React.FunctionComponent = () => (
  <Switch>
    <Redirect from="/console" to="/console/login" exact />

    <Route path="/console/login" component={Login} />

    <Redirect from="/console" to="/console/login" />
  </Switch>
);

export default Router;
