/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useContext, useState } from 'react';
import Input, { Password } from '@reserve/landing/components/Input';
import FullPageCard from '@reserve/landing/components/FullPageCard';
import FormContainer from '@reserve/landing/components/FormContainer';
import FullWidthButton from '@reserve/landing/components/FullWidthButton';

import { AuthenticationContext } from '../stores/AuthenticationStore';
//#endregion

const Login = () => {
  const { login } = useContext(AuthenticationContext);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  async function handleSubmit(e: React.FormEvent) {
    e.preventDefault();
    try {
      await login(username, password);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <FullPageCard title="Login">
      <FormContainer onSubmitCapture={handleSubmit}>
        <Input
          placeholder="username"
          value={username}
          onChange={setUsername}
          required
          autoFocus
        />
        <Password
          placeholder="password"
          value={password}
          onChange={setPassword}
          required
        />
        <FullWidthButton type="primary" htmlType="submit">
          Login
        </FullWidthButton>
      </FormContainer>
    </FullPageCard>
  );
};

export default Login;
