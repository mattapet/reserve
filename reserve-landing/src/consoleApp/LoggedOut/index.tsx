/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import Router from './router';
import { Layout } from 'antd';
//#endregion

export default () => (
  <Layout>
    <Layout.Content>
      <Router />
    </Layout.Content>
  </Layout>
);
