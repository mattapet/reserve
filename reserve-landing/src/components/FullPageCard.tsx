/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import { Card, Layout } from 'antd';
//#endregion

export interface Props {
  readonly title: string;
}

const FullScreenLayout = styled(Layout)`
  height: 100%;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const FullPageCard: React.FunctionComponent<Props> = props => (
  <FullScreenLayout>
    <Card title={props.title}>{props.children}</Card>
  </FullScreenLayout>
);

export default FullPageCard;
