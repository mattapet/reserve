/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region
import styled from 'styled-components';
import { Form } from 'antd';
////#endregion

const FormContainer = styled(Form)`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-content: center;

  > * {
    margin: 5px;
  }
`;

export default FormContainer;
