/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import ANTDInput, { InputProps } from 'antd/lib/input/Input';
import ANTDPassword, {
  PasswordProps as ANTDPasswordProps,
} from 'antd/lib/input/Password';
//#endregion

export interface Props extends Omit<InputProps, 'onChange'> {
  readonly onChange?: (value: string) => any;
}

export interface PasswordProps extends Omit<ANTDPasswordProps, 'onChange'> {
  readonly onChange?: (value: string) => any;
}

const Input: React.FunctionComponent<Props> = ({ onChange, ...props }) => (
  <ANTDInput {...props} onChange={e => onChange?.(e.target.value)} />
);

export const Password: React.FunctionComponent<PasswordProps> = ({
  onChange,
  ...props
}) => <ANTDPassword {...props} onChange={e => onChange?.(e.target.value)} />;

export default Input;
