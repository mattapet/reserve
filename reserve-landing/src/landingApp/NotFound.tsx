/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
//#endregion

const NotFound: React.FunctionComponent = () => <h1>404</h1>;

export default NotFound;
