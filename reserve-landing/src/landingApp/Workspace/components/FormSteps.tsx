/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Steps } from 'antd';
import {
  SolutionOutlined,
  IdcardOutlined,
  KeyOutlined,
  LoadingOutlined,
  CheckCircleOutlined,
} from '@ant-design/icons';
import { FormStep } from '@reserve/landing/lib/types/FormStep';
//#endregion

export interface Props {
  readonly step: FormStep;
}

function getStepStatus(
  currentStep: FormStep,
  requiredStep: FormStep
): 'wait' | 'finish' | 'process' {
  return currentStep < requiredStep
    ? 'wait'
    : currentStep > requiredStep
    ? 'finish'
    : 'process';
}

const FormSteps: React.FunctionComponent<Props> = props => (
  <Steps>
    <Steps.Step
      status={getStepStatus(props.step, FormStep.email)}
      icon={<SolutionOutlined />}
      title="Email Verification"
    />
    <Steps.Step
      status={getStepStatus(props.step, FormStep.workspace)}
      icon={<IdcardOutlined />}
      title="Workpsace ID"
    />
    <Steps.Step
      status={getStepStatus(props.step, FormStep.password)}
      icon={<KeyOutlined />}
      title="Password"
    />
    <Steps.Step
      status={getStepStatus(props.step, FormStep.loading)}
      icon={
        props.step === FormStep.loading ? (
          <LoadingOutlined />
        ) : (
          <CheckCircleOutlined />
        )
      }
      title="Done"
    />
  </Steps>
);

export default FormSteps;
