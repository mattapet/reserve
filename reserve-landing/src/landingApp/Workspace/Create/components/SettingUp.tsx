/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import styled from 'styled-components';
import config from '@reserve/landing/config';
import FullPageCard from '@reserve/landing/components/FullPageCard';
//#endregion

export interface Props {
  readonly workspace: string;
}

//#region Styled
const CenterTextContainer = styled.div`
  text-align: center;
`;
//#endregion

const View: React.FunctionComponent<Props> = props => (
  <FullPageCard title="Done">
    <CenterTextContainer>
      <h4>
        Setting up your workspace <code>{props.workspace}</code>...
      </h4>
      <p>
        Workspace should be ready in the next 24 hours and accessible at{' '}
        <a
          href={`${config.protocol}//${props.workspace}.${config.baseHost}`}
        >{`${config.protocol}//${props.workspace}.${config.baseHost}`}</a>
      </p>
    </CenterTextContainer>
  </FullPageCard>
);

export default View;
