/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';

import Input from '@reserve/landing/components/Input';
import FormContainer from '@reserve/landing/components/FormContainer';
import FullWidthButton from '@reserve/landing/components/FullWidthButton';
import FullPageCard from '@reserve/landing/components/FullPageCard';
//#endregion

//#region Component interfaces
export interface Props {
  readonly pending: boolean;
  readonly workspace: string;
  readonly onWorkspaceChange: (value: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
}
//#endregion

const View: React.FunctionComponent<Props> = props => (
  <FullPageCard title="Choose Workspace Name">
    <FormContainer onSubmitCapture={props.onSubmit}>
      <Input
        type="text"
        placeholder="your-workspace-name"
        value={props.workspace}
        onChange={props.onWorkspaceChange}
        autoFocus
        required
      />
      <FullWidthButton
        type="primary"
        htmlType="submit"
        loading={props.pending}
        disabled={props.workspace.length === 0}
      >
        Create Workspace
      </FullWidthButton>
    </FormContainer>
  </FullPageCard>
);

export default View;
