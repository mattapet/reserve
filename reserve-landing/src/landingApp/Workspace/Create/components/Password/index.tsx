/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState } from 'react';

import View from './view';
//#endregion

//#region Component interfaces
export interface Props {
  readonly onSubmit: (password: string) => any;
  readonly onCancel: () => any;
}
//#endregion

const RegisterEmail: React.FunctionComponent<Props> = (props) => {
  const [password, setPassword] = useState('');
  const [verifyPassword, setVerifyPassword] = useState('');
  const [pending, setPending] = useState(false);

  async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (password !== verifyPassword) {
      return;
    }
    setPending(true);
    props.onSubmit(password);
  }

  return (
    <View
      password={password}
      verifyPassword={verifyPassword}
      pending={pending}
      onPasswordChange={setPassword}
      onVerifyPasswordChange={setVerifyPassword}
      onSubmit={handleSubmit}
      onCancel={props.onCancel}
    />
  );
};

export default RegisterEmail;
