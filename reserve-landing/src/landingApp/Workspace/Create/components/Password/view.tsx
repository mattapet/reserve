/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';

import Input from '@reserve/landing/components/Input';
import FormContainer from '@reserve/landing/components/FormContainer';
import FullWidthButton from '@reserve/landing/components/FullWidthButton';
import FullPageCard from '@reserve/landing/components/FullPageCard';
//#endregion

//#region Component interfaces
export interface Props {
  readonly pending: boolean;
  readonly password: string;
  readonly verifyPassword: string;
  readonly onPasswordChange: (value: string) => any;
  readonly onVerifyPasswordChange: (value: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
  readonly onCancel: () => any;
}
//#endregion

const View: React.FunctionComponent<Props> = props => (
  <FullPageCard title="Choose password">
    <FormContainer onSubmitCapture={props.onSubmit}>
      <Input
        type="password"
        placeholder="Password"
        value={props.password}
        onChange={props.onPasswordChange}
        required
        autoFocus
      />
      <Input
        type="password"
        placeholder="Verify Password"
        value={props.verifyPassword}
        onChange={props.onVerifyPasswordChange}
        required
      />
      <FullWidthButton
        type="primary"
        htmlType="submit"
        loading={props.pending}
        disabled={
          props.password.length === 0 || props.verifyPassword.length === 0
        }
      >
        Submit
      </FullWidthButton>
      <FullWidthButton onClick={props.onCancel}>Back</FullWidthButton>
    </FormContainer>
  </FullPageCard>
);

export default View;
