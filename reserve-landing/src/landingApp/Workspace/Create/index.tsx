/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React, { useState, useEffect } from 'react';
import View from './view';
import { FormStep } from '@reserve/landing/lib/types/FormStep';
//#endregion

//#region Component interfaces
export interface Props {
  readonly workspaceCompletionStep: FormStep;
  readonly onSubmit: (data: FormData) => Promise<void>;
}

export interface FormData {
  readonly workspace: string;
  readonly password: string;
  readonly verifyPassword: string;
}
//#endregion

const CreateWorkspace: React.FunctionComponent<Props> = props => {
  const [step, setStep] = useState(FormStep.workspace);
  const [formData, setFormData] = useState<FormData>({
    workspace: '',
    password: '',
    verifyPassword: '',
  });

  useEffect(() => {
    if (step === FormStep.loading) {
      // tslint:disable
      handleSubmit({ ...formData });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [step, props.onSubmit, formData]);

  function handleWorkspaceChange(workspace: string) {
    setFormData({ ...formData, workspace });
    setStep(props.workspaceCompletionStep);
  }

  function handlePasswordChange(password: string) {
    setFormData({ ...formData, password });
    setStep(FormStep.loading);
  }

  function handleCancel() {
    switch (step) {
      case FormStep.password:
        return setStep(FormStep.workspace);
      default:
        throw new Error('Unreachable');
    }
  }

  async function handleSubmit(data: FormData) {
    try {
      await props.onSubmit(data);
    } catch (e) {
      setStep(FormStep.password);
    }
  }

  return (
    <View
      {...formData}
      step={step}
      onWorkspaceChange={handleWorkspaceChange}
      onPasswordChange={handlePasswordChange}
      onCancel={handleCancel}
    />
  );
};

export default CreateWorkspace;
