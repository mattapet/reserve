/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';

import { FormStep } from '@reserve/landing/lib/types/FormStep';
import Workspace from './components/Workspace';
import Password from './components/Password';
import SettingUp from './components/SettingUp';
import FormSteps from '../components/FormSteps';
//#endregion

//#region Component interfaces
export interface Props {
  readonly step: FormStep;
  readonly workspace: string;
  readonly password: string;
  readonly onWorkspaceChange: (workspace: string) => any;
  readonly onPasswordChange: (password: string) => any;
  readonly onCancel: () => any;
}
//#endregion

const View: React.FunctionComponent<Props> = props => (
  <React.Fragment>
    <FormSteps step={props.step} />
    {props.step === FormStep.workspace ? (
      <Workspace {...props} onSubmit={props.onWorkspaceChange} />
    ) : props.step === FormStep.password ? (
      <Password {...props} onSubmit={props.onPasswordChange} />
    ) : (
      <SettingUp {...props} />
    )}
  </React.Fragment>
);

export default View;
