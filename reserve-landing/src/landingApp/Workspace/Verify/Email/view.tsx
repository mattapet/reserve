/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';

import Success from './components/Success';
import Input from '@reserve/landing/components/Input';
import FullPageCard from '@reserve/landing/components/FullPageCard';
import { FormStep } from '@reserve/landing/lib/types/FormStep';
import FormContainer from '@reserve/landing/components/FormContainer';
import FullWidthButton from '@reserve/landing/components/FullWidthButton';

import FormSteps from '../../components/FormSteps';
//#endregion

//#region Component interfaces
export interface Props {
  readonly pending: boolean;
  readonly retrieved: boolean;
  readonly email: string;
  readonly error: Error | null;
  readonly onEmailChange: (e: string) => any;
  readonly onSubmit: (e: React.FormEvent<HTMLFormElement>) => any;
}
//#endregion

const View: React.FunctionComponent<Props> = props => {
  const complete = props.retrieved && !props.error;

  if (complete) {
    return (
      <React.Fragment>
        <FormSteps step={FormStep.email} />
        <FullPageCard title="VerifyEmail">
          <Success email={props.email} />
        </FullPageCard>
      </React.Fragment>
    );
  }

  return (
    <React.Fragment>
      <FormSteps step={FormStep.email} />
      <FullPageCard title="VerifyEmail">
        <FormContainer onSubmitCapture={props.onSubmit}>
          <Input
            type="email"
            placeholder="Email"
            value={props.email}
            onChange={props.onEmailChange}
            autoFocus
            required
          />
          <FullWidthButton
            type="primary"
            htmlType="submit"
            loading={props.pending}
          >
            Submit
          </FullWidthButton>
        </FormContainer>
      </FullPageCard>
    </React.Fragment>
  );
};

export default View;
