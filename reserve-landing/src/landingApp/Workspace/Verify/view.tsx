/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Link } from 'react-router-dom';

import { FormStep } from '@reserve/landing/lib/types/FormStep';
import FullPageCard from '@reserve/landing/components/FullPageCard';
import FullWidthButton from '@reserve/landing/components/FullWidthButton';

import FormSteps from '../components/FormSteps';
//#endregion

//#region Component interfaces
export interface Props {
  readonly onAuthoritze: (authority: string) => Promise<void>;
}
//#endregion

const View: React.FunctionComponent<Props> = props => (
  <React.Fragment>
    <FormSteps step={FormStep.email} />
    <FullPageCard title="Verify Email">
      <FullWidthButton
        type="primary"
        onClick={() => props.onAuthoritze('silicon_hill')}
      >
        Authorize with IS
      </FullWidthButton>
      <Link to="/workspace/create/verify/email">
        <FullWidthButton>Veriry email</FullWidthButton>
      </Link>
    </FullPageCard>
  </React.Fragment>
);

export default View;
