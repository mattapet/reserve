/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import * as querystring from 'querystring';

import config from '@reserve/landing/config';
import { redirect } from '@reserve/landing/lib/redirect';
import View from './view';
//#endregion

const Verify: React.FunctionComponent = () => {
  async function handleAuthorize(authority: string) {
    const query = {
      state: JSON.stringify({
        clientUrl: config.landingUrl.substring(0, config.landingUrl.length - 1),
      }),
    };
    const url = `${config.apiUrl}api/v1/setup/verify/authority/${authority}`;

    redirect(`${url}?${querystring.stringify(query)}`);
  }

  return <View onAuthoritze={handleAuthorize} />;
};

export default Verify;
