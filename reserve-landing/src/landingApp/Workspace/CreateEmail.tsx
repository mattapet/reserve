/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import * as querystring from 'querystring';
import { RouteComponentProps, withRouter } from 'react-router';

import config from '@reserve/landing/config';
import { FormStep } from '@reserve/landing/lib/types/FormStep';
import Create, { FormData } from './Create';
//#endregion

const CreateEmail: React.FunctionComponent<RouteComponentProps> = (props) => {
  async function handleSubmit(data: FormData) {
    const { workspace, password } = data;
    const query = props.location.search.slice(1); // Drop leading `?`
    const { workspaceId, email, expiry, signature } = querystring.parse(query);

    await fetch(`${config.apiUrl}api/v1/setup/complete/email`, {
      method: 'POST',
      body: JSON.stringify({
        workspace_id: workspaceId,
        email,
        expiry: parseInt(expiry as string, 10),
        signature,
        workspace,
        password,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  return (
    <Create
      workspaceCompletionStep={FormStep.password}
      onSubmit={handleSubmit}
    />
  );
};

export default withRouter(CreateEmail);
