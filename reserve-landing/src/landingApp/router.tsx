/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import Spinner from '@reserve/landing/components/Spinner';

import CreateAuthority from './Workspace/CreateAuthority';
import NotFound from './NotFound';

const Home = React.lazy(() => import('./Home'));
const About = React.lazy(() => import('./About'));
const Docs = React.lazy(() => import('./Docs'));
const Login = React.lazy(() => import('./Workspace/Login'));
const Verify = React.lazy(() => import('./Workspace/Verify'));
const VerifyEmail = React.lazy(() => import('./Workspace/Verify/Email'));
const CreateEmail = React.lazy(() => import('./Workspace/CreateEmail'));
//#endregion

const Router: React.FunctionComponent = () => (
  <React.Suspense fallback={<Spinner />}>
    <Switch>
      <Route path="/" component={Home} exact />
      <Route path="/about" component={About} exact />
      <Route path="/docs" component={Docs} exact />
      <Route path="/workspace/login" component={Login} />

      <Redirect from="/workspace/create" to="/workspace/create/verify" exact />

      <Route path="/workspace/create/verify" component={Verify} exact />
      <Route
        path="/workspace/create/verify/email"
        component={VerifyEmail}
        exact
      />
      <Route path="/workspace/create/complete" component={CreateEmail} exact />
      <Route
        path="/workspace/create/complete/email"
        component={CreateEmail}
        exact
      />
      <Route
        path="/auth/:authority/callback"
        component={CreateAuthority}
        exact
      />
      <Route component={NotFound} />
    </Switch>
  </React.Suspense>
);

export default Router;
