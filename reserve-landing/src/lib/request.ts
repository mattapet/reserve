/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

enum HTTPMethod {
  get = 'GET',
  post = 'POST',
  put = 'PUT',
  delete = 'DELETE',
}

class BasicRequestMethodPicker implements RequestMethodPicker {
  public get(url: string): RequestBuilder {
    return new BasicRequestBuilder(HTTPMethod.get, url);
  }
  public post(url: string): RequestBuilder {
    return new BasicRequestBuilder(HTTPMethod.post, url);
  }
  public put(url: string): RequestBuilder {
    return new BasicRequestBuilder(HTTPMethod.put, url);
  }
  public delete(url: string): RequestBuilder {
    return new BasicRequestBuilder(HTTPMethod.delete, url);
  }
}

class BasicRequestBuilder implements RequestBuilder {
  private headers: { [headers: string]: string } = {};

  public constructor(
    private readonly method: HTTPMethod,
    private readonly url: string
  ) {}

  public set(headerName: string, value: string): this {
    this.headers[headerName] = value;
    return this;
  }

  public send(body?: string): Promise<Response> {
    return fetch(this.url, {
      method: this.method,
      headers: this.headers,
      body,
    });
  }
}

export interface RequestBuilder {
  set(headerName: string, value: string): RequestBuilder;
  send(body?: string): Promise<Response>;
}

export interface RequestMethodPicker {
  get(url: string): RequestBuilder;
  post(url: string): RequestBuilder;
  put(url: string): RequestBuilder;
  delete(url: string): RequestBuilder;
}

export function request(): RequestMethodPicker {
  return new BasicRequestMethodPicker();
}
