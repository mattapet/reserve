/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

class Task {
  public constructor(
    private readonly work: () => Promise<void>,
    private readonly onSuccess: () => void,
    private readonly onFailure: (error: Error) => void,
  ) {}

  public async execute(): Promise<void> {
    try {
      await this.work();
      this.onSuccess();
    } catch (error) {
      this.onFailure(error);
    }
  }
}

export class TaskQueue {
  private taskQueue: Task[] = [];
  private inExecution: boolean = false;

  public async execute(work: () => Promise<void>): Promise<void> {
    return new Promise((resolve, reject) => {
      const task = new Task(work, resolve, reject);
      this.pushTask(task);
    });
  }

  private pushTask(task: Task): void {
    this.taskQueue.push(task);
    if (!this.inExecution) {
      this.runTasks();
    }
  }

  private async runTasks(): Promise<void> {
    this.inExecution = true;
    await this.runTasksExecution();
    this.inExecution = false;
  }

  private async runTasksExecution(): Promise<void> {
    while (this.taskQueue.length) {
      await this.taskQueue.shift()?.execute();
    }
  }
}
