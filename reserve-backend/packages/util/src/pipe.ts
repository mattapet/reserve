/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

type F<Input, Output> = (value: Input) => Output;

export function pipe<T>(value: T): T;
export function pipe<T1, T2>(value: T1, f1: F<T1, T2>): T2;
export function pipe<T1, T2, T3>(value: T1, f1: F<T1, T2>, f2: F<T2, T3>): T3;
export function pipe<T1, T2, T3, T4>(
  value: T1,
  f1: F<T1, T2>,
  f2: F<T2, T3>,
  f3: F<T3, T4>,
): T4;
export function pipe<T1, T2, T3, T4, T5>(
  value: T1,
  f1: F<T1, T2>,
  f2: F<T2, T3>,
  f3: F<T3, T4>,
  f4: F<T4, T5>,
): T5;
export function pipe<T1, T2, T3, T4, T5, T6>(
  value: T1,
  f1: F<T1, T2>,
  f2: F<T2, T3>,
  f3: F<T3, T4>,
  f4: F<T4, T5>,
  f5: F<T5, T6>,
): T6;
export function pipe<T1, T2, T3, T4, T5, T6, T7>(
  value: T1,
  f1: F<T1, T2>,
  f2: F<T2, T3>,
  f3: F<T3, T4>,
  f4: F<T4, T5>,
  f5: F<T5, T6>,
  f6: F<T6, T7>,
): T7;
export function pipe<T1, T2, T3, T4, T5, T6, T7, T8>(
  value: T1,
  f1: F<T1, T2>,
  f2: F<T2, T3>,
  f3: F<T3, T4>,
  f4: F<T4, T5>,
  f5: F<T5, T6>,
  f6: F<T6, T7>,
  f7: F<T7, T8>,
): T8;
export function pipe<T1, T2, T3, T4, T5, T6, T7, T8, T9>(
  value: T1,
  f1: F<T1, T2>,
  f2: F<T2, T3>,
  f3: F<T3, T4>,
  f4: F<T4, T5>,
  f5: F<T5, T6>,
  f6: F<T6, T7>,
  f7: F<T7, T8>,
  f8: F<T8, T9>,
): T9;
export function pipe<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(
  value: T1,
  f1: F<T1, T2>,
  f2: F<T2, T3>,
  f3: F<T3, T4>,
  f4: F<T4, T5>,
  f5: F<T5, T6>,
  f6: F<T6, T7>,
  f7: F<T7, T8>,
  f8: F<T8, T9>,
  f9: F<T9, T10>,
): T10;
export function pipe<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(
  value: T1,
  f1: F<T1, T2>,
  f2: F<T2, T3>,
  f3: F<T3, T4>,
  f4: F<T4, T5>,
  f5: F<T5, T6>,
  f6: F<T6, T7>,
  f7: F<T7, T8>,
  f8: F<T8, T9>,
  f9: F<T9, T10>,
  f10: F<T10, T11>,
): T11;
export function pipe<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(
  value: T1,
  f1: F<T1, T2>,
  f2: F<T2, T3>,
  f3: F<T3, T4>,
  f4: F<T4, T5>,
  f5: F<T5, T6>,
  f6: F<T6, T7>,
  f7: F<T7, T8>,
  f8: F<T8, T9>,
  f9: F<T9, T10>,
  f10: F<T10, T11>,
  f11: F<T11, T12>,
): T12;
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function pipe(initialValue: any, ...mutators: F<any, any>[]): any {
  return mutators.reduce((result, nextFn) => nextFn(result), initialValue);
}
