/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export function compactMap<Element, Result>(
  collection: Element[],
  transform: (element: Element) => Result | undefined | null,
): Result[] {
  const results: Result[] = [];
  for (const element of collection) {
    const result = transform(element);
    if (result !== null && result !== undefined) {
      results.push(result);
    }
  }
  return results;
}
