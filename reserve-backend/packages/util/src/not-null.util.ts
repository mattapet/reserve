/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReserveError } from './reserve-error';
//#endregion

export class UnexpectedNullError extends ReserveError {}

export function notNull<T>(value?: T, message?: string): T {
  if (value == null) {
    throw new UnexpectedNullError(message ?? 'Unexpected null found.');
  }
  return value;
}
