/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export function keyBy<T>(
  elements: T[],
  keyExtractor: (el: T) => string,
): { [key: string]: T } {
  return elements.reduce(
    (acc, next) => ({ ...acc, [keyExtractor(next)]: next }),
    {},
  );
}
