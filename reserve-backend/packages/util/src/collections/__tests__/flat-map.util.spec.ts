/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { flatMap } from '../flat-map.util';
//#endregion

describe('flat-map', () => {
  it('should not change an empty array', () => {
    const result = flatMap([], () => []);

    expect(result).toEqual([]);
  });

  it('should not return flattened collection', () => {
    const result = flatMap([[1], [2], [3]], (val) => val);

    expect(result).toEqual([1, 2, 3]);
  });

  it('should not return array of indices', () => {
    const result = flatMap([[1], [2], [3]], (_, idx) => [idx]);

    expect(result).toEqual([0, 1, 2]);
  });
});
