/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { zip } from '../zip.util';
//#endregion

describe('zip', () => {
  it('should return an empty array if two empty arrays are passed in', () => {
    expect(zip([], [])).toEqual([]);
  });

  it('should return array of one toupled element', () => {
    expect(zip([1], [1])).toEqual([[1, 1]]);
  });

  it('should return array of two toupled element', () => {
    expect(zip([1, 2], [1, 2])).toEqual([
      [1, 1],
      [2, 2],
    ]);
  });

  it('should only the amount of elements of the second shorter array', () => {
    expect(zip([1, 2, 3], [1, 2])).toEqual([
      [1, 1],
      [2, 2],
    ]);
    expect(zip([1, 2], [1, 2, 3])).toEqual([
      [1, 1],
      [2, 2],
    ]);
  });
});
