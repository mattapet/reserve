/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export function zip<T1, T2>(arr1: T1[], arr2: T2[]): [T1, T2][] {
  const results: [T1, T2][] = [];
  const len = Math.min(arr1.length, arr2.length);
  for (let i = 0; i < len; i++) {
    results.push([arr1[i], arr2[i]]);
  }
  return results;
}
