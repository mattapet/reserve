/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export function flatMap<Element, Result>(
  collection: Element[],
  mapper: (element: Element, index: number) => Result[],
): Result[] {
  const results: Result[] = [];
  for (let i = 0; i < collection.length; i++) {
    results.push(...mapper(collection[i], i));
  }
  return results;
}
