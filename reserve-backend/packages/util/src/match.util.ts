/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export class UnresolvedError extends Error {}

export function match<T, Result = never>(value: T): Matcher<T, Result> {
  return new MatcherImpl<T, Result>(value);
}

export interface Matcher<T, Result = never> {
  when(cmp: (value: T) => boolean): MatchResult<T, Result>;
  whenEq(expected: T): MatchResult<T, Result>;
  whenType<R extends T>(cmp: new (...args: any[]) => R): MatchResult<T, Result>;
  any(): MatchResult<T, Result>;
  get(): Result;
  getOrDefault<U>(defaultValue: () => U): Result | U;
}

abstract class MatchResult<T, Result = never> {
  public constructor(
    protected readonly value: T,
    protected readonly result?: Result,
  ) {}

  public abstract where(cmp: (value: T) => boolean): MatchResult<T, Result>;
  public abstract then<U>(fn: (value: T) => U): Matcher<T, Result | U>;

  public orWhen(cmp: (value: T) => boolean): MatchResult<T, Result> {
    if (cmp(this.value)) return new Match(this.value, this.result);
    return this;
  }

  public orWhenType(cmp: new (...args: any[]) => T): MatchResult<T, Result> {
    return this.orWhen((val) => val instanceof cmp);
  }

  public orWhenEq(value: T): MatchResult<T, Result> {
    return this.orWhen((val) => val === value);
  }
}

class Match<T, Result = never> extends MatchResult<T, Result> {
  public where(cmp: (value: T) => boolean): MatchResult<T, Result> {
    if (cmp(this.value)) return new Match(this.value, this.result);
    return new Missmatch(this.value, this.result);
  }

  public then<U>(fn: (value: T) => Result | U): Matcher<T, Result | U> {
    if (!this.result) return new MatcherImpl(this.value, fn(this.value));
    return new MatcherImpl(this.value, this.result);
  }
}

class Missmatch<T, Result = never> extends MatchResult<T, Result> {
  public where(): MatchResult<T, Result> {
    return this;
  }

  public then(): Matcher<T, Result> {
    return new MatcherImpl(this.value, this.result);
  }
}

class MatcherImpl<T, Result = never> implements Matcher<T, Result> {
  constructor(public readonly value: T, public readonly result?: Result) {}

  public when(cmp: (value: T) => boolean): MatchResult<T, Result> {
    if (cmp(this.value)) return new Match(this.value, this.result);
    return new Missmatch(this.value, this.result);
  }

  public whenEq(expected: T): MatchResult<T, Result> {
    return this.when((value) => expected === value);
  }

  public whenType<R extends T>(
    expectedType: new (...args: any[]) => R,
  ): MatchResult<T, Result> {
    return this.when((value) => value instanceof expectedType);
  }

  public any(): MatchResult<T, Result> {
    return new Match(this.value, this.result);
  }

  public get(): Result {
    if (!this.result) throw new UnresolvedError();
    return this.result;
  }

  public getOrDefault<U>(fn: () => U): Result | U {
    if (!this.result) return fn();
    return this.result;
  }
}
