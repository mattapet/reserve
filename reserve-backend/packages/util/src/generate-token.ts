/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import crypto from 'crypto';
//#endregion

export function generateToken(): string {
  return crypto
    .createHash('sha256')
    .update(crypto.randomBytes(256))
    .digest('hex');
}
