/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// source: https://coderwall.com/p/flonoa/simple-string-format-in-javascript
export function formatString(fmt: string, ...args: any[]): string {
  return fmt.replace(
    /((?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{([0-9]+)\})/g,
    (_, str: string, index: number) => {
      if (str) {
        return str.replace(/(?:{{)|(?:}})/g, (match: any) => match[0]);
      } else {
        return args[index];
      }
    },
  );
}
