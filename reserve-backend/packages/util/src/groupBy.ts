/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

interface GroupedByResult<Element> {
  readonly [key: string]: Element[];
}
export function groupBy<Element>(
  elements: Element[],
  extractor: (element: Element) => string,
): GroupedByResult<Element> {
  return elements.reduce<GroupedByResult<Element>>(
    (accumulator, nextElement) => {
      const key = extractor(nextElement);
      const groupedElements = accumulator[key] ?? [];
      return { ...accumulator, [key]: [...groupedElements, nextElement] };
    },
    {},
  );
}
