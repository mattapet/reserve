/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { match, UnresolvedError } from '../match.util';

describe('match', () => {
  it('should throw Unresolved error if no match are performed', () => {
    const matcher = match(1);
    expect(() => matcher.get()).toThrow(UnresolvedError);
  });

  it('should return result of match when match is found', () => {
    const matcher = match(1)
      .whenEq(1)
      .then(() => 'success');
    expect(matcher.get()).toEqual('success');
  });

  it('should be able to capture the value when matched', () => {
    let captured: number | undefined;
    const matcher = match(1)
      .whenEq(1)
      .then((matched) => {
        captured = matched;
        return 'success';
      });
    expect(matcher.get()).toEqual('success');
    expect(captured).toEqual(1);
  });

  it('should throw Unresolved error if no match is found', () => {
    const matcher = match(1)
      .whenEq(99)
      .then(() => 'failure');
    expect(() => matcher.get()).toThrow(UnresolvedError);
  });

  it('should return matched value if multiple cases not matched', () => {
    const matcher = match(1)
      .whenEq(99)
      .where(() => true)
      .then(() => 'failure')
      .whenEq(88)
      .then(() => 'failure')
      .whenEq(1)
      .then(() => 'success');
    expect(matcher.get()).toEqual('success');
  });

  it('should return matched class instance type', () => {
    const matcher = match(new Error())
      .whenType(Error)
      .then(() => 'success');
    expect(matcher.get()).toEqual('success');
  });

  it('should be able to capture the value when matched class', () => {
    let captured: Error | undefined;
    const error = new Error();
    const matcher = match(error)
      .whenType(Error)
      .then((value) => {
        captured = value;
        return 'success';
      });
    expect(matcher.get()).toEqual('success');
    expect(captured).toBe(error);
  });

  it('should throw Unresolved error if no class match is found', () => {
    const matcher = match(new Error())
      .whenType(TypeError)
      .then(() => 'success');
    expect(() => matcher.get()).toThrow(UnresolvedError);
  });

  it('should return default value if nothing matched', () => {
    const matcher = match(1)
      .whenEq(99)
      .then(() => 'failure')
      .whenEq(88)
      .then(() => 'failure');
    expect(matcher.getOrDefault(() => 'default')).toEqual('default');
  });

  it('should not return default value if there was a match', () => {
    const matcher = match(1)
      .whenEq(1)
      .then(() => 'success')
      .whenEq(99)
      .then(() => 'failure')
      .whenEq(88)
      .then(() => 'failure');
    expect(matcher.getOrDefault(() => 'default')).toEqual('success');
  });

  it('should be able to match by arbitrary function matcher', () => {
    const matcher = match(1)
      .when((value) => value === 1)
      .then(() => 'success');
    expect(matcher.get()).toEqual('success');
  });

  it('should throw an UnresolvedError if no matching function found', () => {
    const matcher = match(1)
      .when(() => false)
      .then(() => 'failure');
    expect(() => matcher.get()).toThrow(UnresolvedError);
  });

  it('Should return first match if there are multiple matches', () => {
    const matcher = match(1)
      .whenEq(1)
      .then(() => 'first_success')
      .whenEq(1)
      .then(() => 'second_success');
    expect(matcher.get()).toEqual('first_success');
  });

  it('should match when further constraint holds', () => {
    const matcher = match(new TypeError('Some error'))
      .whenType(Error)
      .where((error) => error.message === 'Some error')
      .then(() => 'success');
    expect(matcher.get()).toEqual('success');
  });

  it('should throw UnresolvedError when match constraint does not hold', () => {
    const matcher = match(new TypeError('Some error'))
      .whenType(Error)
      .where(() => false)
      .then(() => 'failure');
    expect(() => matcher.get()).toThrow(UnresolvedError);
  });

  it('should match any value', () => {
    const matcher = match(1)
      .any()
      .then(() => 'success');
    expect(matcher.get()).toEqual('success');
  });

  it('should not match any value if previous match was successful', () => {
    const matcher = match(1)
      .whenEq(1)
      .then(() => 'success')
      .any()
      .then(() => 'default_success');
    expect(matcher.get()).toEqual('success');
  });

  it('should second or-matched value', () => {
    const matcher = match(1)
      .whenEq(0)
      .orWhenEq(1)
      .then(() => 'or matched');

    expect(matcher.get()).toBe('or matched');
  });

  it('should return first matched value', () => {
    const matcher = match(1)
      .whenEq(1)
      .then(() => 'first result')
      .whenEq(0)
      .orWhenEq(1)
      .then(() => 'second result');

    expect(matcher.get()).toBe('first result');
  });

  it('should return or-matched type', () => {
    const matcher = match(new TypeError('Some error'))
      .whenType(EvalError)
      .orWhenType(TypeError)
      .then(() => 'correct error matched');

    expect(matcher.get()).toBe('correct error matched');
  });
});
