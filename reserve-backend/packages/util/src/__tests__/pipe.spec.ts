/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { pipe } from '../pipe';
//#endregion

describe('pipe', () => {
  it('should not mutate single value', () => {
    const result: number = pipe(14);

    expect(result).toBe(14);
  });

  it('should apply single mutator function', () => {
    const result: number = pipe(14, (value) => value * 2);

    expect(result).toBe(28);
  });

  it('should apply two mutator functions', () => {
    const result: string = pipe(
      14,
      (value) => value * 2,
      (value) => `${value}`,
    );

    expect(result).toBe('28');
  });

  it('should apply three mutator functions', () => {
    const result: string = pipe(
      14,
      (value) => value * 2,
      (value) => `${value}`,
      (value) => value + ' 55',
    );

    expect(result).toBe('28 55');
  });

  it('should apply four mutator functions', () => {
    const result: string = pipe(
      [1, 2, 3, 4, 5],
      (values) => values.map((value) => value * 2),
      (values): [number[], number] => [
        values,
        values.reduce((a, b) => a + b, 0),
      ],
      ([values, sum]): [number[], number, number] => [
        values,
        sum,
        sum / values.length,
      ],
      ([values, sum, avg]) => `Sum of ${values} is ${sum} with avg ${avg}`,
    );

    expect(result).toBe('Sum of 2,4,6,8,10 is 30 with avg 6');
  });

  it('should apply 10 mutator functions', () => {
    const increment = (value: number) => value + 1;

    const result: string = pipe(
      1,
      increment,
      increment,
      increment,
      increment,
      increment,
      increment,
      increment,
      increment,
      increment,
      increment,
      (value) => `${value}`,
    );

    expect(result).toBe('11');
  });
});
