/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region
import { formatString } from '../format-string.util';
//#endregion

describe('format string', () => {
  it('should not change a string without any placeholders', () => {
    const format = 'Some string without placeholders';
    const args = ['arg1', 'arg2'];

    const result = formatString(format, ...args);

    expect(result).toEqual('Some string without placeholders');
  });

  it('should inject first arguments', () => {
    const format = 'Some string with a single argument {0}';
    const args = ['arg1', 'arg2'];

    const result = formatString(format, ...args);

    expect(result).toEqual('Some string with a single argument arg1');
  });

  it('should inject second arguments', () => {
    const format = 'Some string with a single argument {1}';
    const args = ['arg1', 'arg2'];

    const result = formatString(format, ...args);

    expect(result).toEqual('Some string with a single argument arg2');
  });
});
