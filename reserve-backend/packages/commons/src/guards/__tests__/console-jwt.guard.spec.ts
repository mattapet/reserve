/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UnauthorizedException } from '@nestjs/common';
import { UserRole } from '@rsaas/util/lib/user-role.type';

import { ConsoleJwtGuard } from '../console-jwt.guard';
//#endregion

describe('service jwt guard', () => {
  let guard!: ConsoleJwtGuard;

  beforeEach(() => {
    guard = new ConsoleJwtGuard();
  });

  it('should return authorized user', () => {
    const user = { username: 'root', scope: [] };

    const result = guard.handleRequest(null, user);

    expect(result).toEqual(user);
  });

  it('should throw UnauthorizedException if error is not null', () => {
    const fn = () => guard.handleRequest(new Error(), null);

    expect(fn).toThrow(UnauthorizedException);
  });

  it('should throw UnauthorizedException if user is null', () => {
    const fn = () => guard.handleRequest(null, null);

    expect(fn).toThrow(UnauthorizedException);
  });

  it('should throw UnauthorizedException if user is not service account', () => {
    const user = { id: '99', workspace: 'test', role: UserRole.admin };

    const fn = () => guard.handleRequest(null, user);

    expect(fn).toThrow(UnauthorizedException);
  });
});
