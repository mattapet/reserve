/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UnauthorizedException } from '@nestjs/common';
import { UserRole } from '@rsaas/util/lib/user-role.type';

import { JwtGuard } from '../jwt.guard';
//#endregion

describe('jwt guard', () => {
  it('should return authorized user', () => {
    const guard = new JwtGuard();
    const user = { id: '99', workspace: 'test', role: UserRole.admin };

    const result = guard.handleRequest(null, user);

    expect(result).toEqual(user);
  });

  it('should throw UnauthorizedException if error is not null', () => {
    const guard = new JwtGuard();

    const fn = () => guard.handleRequest(new Error(), null);

    expect(fn).toThrow(UnauthorizedException);
  });

  it('should throw UnauthorizedException if user is null', () => {
    const guard = new JwtGuard();

    const fn = () => guard.handleRequest(null, null);

    expect(fn).toThrow(UnauthorizedException);
  });

  it('should throw UnauthorizedException if users workspace is not set', () => {
    const guard = new JwtGuard();
    const user = { id: '99' };

    const fn = () => guard.handleRequest(null, user);

    expect(fn).toThrow(UnauthorizedException);
  });
});
