/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AuthGuard } from '@nestjs/passport';
import { UnauthorizedException, Injectable } from '@nestjs/common';

import { UserRole } from '@rsaas/util/lib/user-role.type';
//#endregion

@Injectable()
export class JwtGuard extends AuthGuard('jwt') {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public handleRequest(error: any, user: any): any {
    if (error != null || !user || !this.isRegularAccount(user)) {
      throw new UnauthorizedException(error?.message ?? '');
    }
    return user;
  }

  private isRegularAccount(user: any): boolean {
    if (!user.id || !user.workspace) {
      return false;
    }

    switch (user.role) {
      case UserRole.user:
        return true;
      case UserRole.maintainer:
        return true;
      case UserRole.admin:
        return true;
      default:
        return false;
    }
  }
}
