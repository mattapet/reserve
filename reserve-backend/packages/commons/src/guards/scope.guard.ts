/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';

import { Scope } from '@rsaas/util/lib/oauth2/scope.type';
import { WorkspaceAuthorizedRequest } from '../interface/workspace-authorized-request.interface';
//#endregion

export function ScopeGuard(...scopes: Scope[]): CanActivate {
  return new Guard(new Set(scopes));
}

@Injectable()
export class Guard implements CanActivate {
  public constructor(private readonly scopes: Set<Scope>) {}

  public canActivate(context: ExecutionContext): boolean {
    const req = context.switchToHttp().getRequest<WorkspaceAuthorizedRequest>();

    for (const scope of req.user.scope) {
      if (this.scopes.has(scope)) {
        return true;
      }
    }
    return false;
  }
}
