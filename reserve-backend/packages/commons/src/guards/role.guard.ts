/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';

import { UserRole } from '@rsaas/util/lib/user-role.type';
import { WorkspaceAuthorizedRequest } from '../interface/workspace-authorized-request.interface';
//#endregion

export function RoleGuard(...roles: UserRole[]): CanActivate {
  return new Guard(new Set(roles));
}

@Injectable()
export class Guard implements CanActivate {
  public constructor(private readonly roles: Set<UserRole>) {}

  public canActivate(context: ExecutionContext): boolean {
    const req = context.switchToHttp().getRequest<WorkspaceAuthorizedRequest>();
    return this.roles.has(req.user.role);
  }
}
