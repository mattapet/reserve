/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AuthGuard } from '@nestjs/passport';
import { UnauthorizedException, Injectable } from '@nestjs/common';
//#endregion

@Injectable()
export class ConsoleJwtGuard extends AuthGuard('jwt') {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public handleRequest(error: any, user: any): any {
    if (error != null || !user || !this.isServiceAccount(user)) {
      throw new UnauthorizedException(error?.message ?? '');
    }
    return user;
  }

  private isServiceAccount(user: any): boolean {
    return !!user.username && !!user.scope;
  }
}
