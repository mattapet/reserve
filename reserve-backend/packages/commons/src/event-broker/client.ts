/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region
import { Inject, Injectable } from '@nestjs/common';

import { Connection } from './connection.interface';
import { EVENT_BROKER_CONNECTION } from './constants';
import { Event } from '../events/event.entity';
import { EventConsumer } from './event-consumer';
//#endregion

@Injectable()
export class Client {
  public constructor(
    @Inject(EVENT_BROKER_CONNECTION)
    private readonly connection: Connection,
  ) {}

  public async publish(topic: string, event: Event): Promise<void> {
    return this.connection.publish(topic, event);
  }

  public subscribe(consumer: EventConsumer): void {
    this.connection.subscribe(consumer);
  }
}
