/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export class EventConsumerStorage {
  private static readonly storage: EventConsumer[] = [];

  public static addConsumer(consumer: EventConsumer): void {
    EventConsumerStorage.storage.push(consumer);
  }

  public static getConsumers(): EventConsumer[] {
    return [...EventConsumerStorage.storage];
  }

  public static clear(): void {
    EventConsumerStorage.storage.splice(0, EventConsumerStorage.storage.length);
  }
}

export interface EventConsumer {
  readonly name: string;
}
