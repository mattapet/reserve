/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export const EVENT_PATTERN_METADATA = '__EVENT_PATTERN_METADATA__';
export const EVENT_BROKER_CONNECTION = '__EVENT_BROKER_CONNECTION__';
export const EVENT_BROKER_CLIENT = '__EVENT_BROKER_CLIENT__';
