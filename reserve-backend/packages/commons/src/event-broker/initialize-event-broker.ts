/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { INestApplicationContext } from '@nestjs/common';

import { EVENT_PATTERN_METADATA, EVENT_BROKER_CLIENT } from './constants';
import { Client } from './client';
import { EventConsumerStorage } from './event-consumer.storage';
import { EventPattern } from './event-pattern.type';
import { EventConsumer } from './event-consumer';
import { Inbox } from './inbox';
//#endregion

export function initializeEventBroker(context: INestApplicationContext): void {
  const client = context.get<Client>(EVENT_BROKER_CLIENT);
  const inbox = context.get<Inbox>(Inbox);
  const consumers = EventConsumerStorage.getConsumers();

  for (const { name } of consumers) {
    const instance = context.get(name);
    const eventPatterns: EventPattern[] =
      Reflect.getMetadata(EVENT_PATTERN_METADATA, instance.constructor) ?? [];

    const consumer = new EventConsumer(inbox, name, eventPatterns, instance);
    client.subscribe(consumer);
  }
}
