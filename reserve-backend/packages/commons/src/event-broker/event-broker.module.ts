/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module, DynamicModule, Global } from '@nestjs/common';

import { EVENT_BROKER_CLIENT } from './constants';
import { Client } from './client';
import { InMemoryBrokerModule } from './in-memory/in-memory-broker.module';
import { InboxModule } from './inbox';
//#endregion

@Global()
@Module({})
export class EventBrokerModule {
  public static inMemory(): DynamicModule {
    return {
      module: InMemoryBrokerModule,
      imports: [InboxModule],
      providers: [
        {
          provide: EVENT_BROKER_CLIENT,
          useClass: Client,
        },
      ],
      exports: [
        InMemoryBrokerModule,
        {
          provide: EVENT_BROKER_CLIENT,
          useClass: Client,
        },
        InboxModule,
      ],
    };
  }
}
