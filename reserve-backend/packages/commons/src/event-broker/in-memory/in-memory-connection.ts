/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Optional } from '@nestjs/common';
import { TaskQueue } from '@rsaas/util/lib/task-queue';

import { Connection } from '../connection.interface';
import { Event } from '../../events/event.entity';
import { EventConsumer } from '../event-consumer';
//#endregion

@Injectable()
export class InMemoryConnection implements Connection {
  private readonly taskQueue = new TaskQueue();

  public constructor(
    @Optional()
    private readonly consumers: EventConsumer[] = [],
  ) {}

  public async subscribe(consumer: EventConsumer): Promise<void> {
    this.consumers.push(consumer);
  }

  public async publish(pattern: string, event: Event): Promise<void> {
    await this.taskQueue.execute(async () => {
      for (const consumer of this.consumers) {
        await consumer.consume(pattern, event);
      }
    });
  }
}
