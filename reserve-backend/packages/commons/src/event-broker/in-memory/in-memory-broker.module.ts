/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { EVENT_BROKER_CONNECTION } from '../constants';
import { InMemoryConnection } from './in-memory-connection';
//#endregion

@Module({
  providers: [
    InMemoryConnection,
    {
      provide: EVENT_BROKER_CONNECTION,
      useClass: InMemoryConnection,
    },
  ],
  exports: [InMemoryConnection],
})
export class InMemoryBrokerModule {}
