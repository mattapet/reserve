/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TaskQueue } from '@rsaas/util/lib/task-queue';

import { Event } from '../events/event.entity';
import { EventHandler } from './event-handler.type';
import { EventPattern } from './event-pattern.type';
import { Inbox } from './inbox';
//#endregion

export class EventConsumer {
  private readonly handlers: Map<string, EventHandler> = new Map();
  private readonly taskQueue = new TaskQueue();

  public constructor(
    private readonly inbox: Inbox,
    private readonly name: string,
    patterns: EventPattern[],
    // eslint-disable-next-line @typescript-eslint/ban-types
    private readonly instance: Function,
  ) {
    for (const [pattern, handler] of patterns) {
      this.handlers.set(pattern, handler);
    }
  }

  public getName(): string {
    return this.name;
  }

  public getPatterns(): string[] {
    return [...this.handlers.keys()];
  }

  public async consume(pattern: string, event: Event): Promise<void> {
    await this.taskQueue.execute(async () => {
      const handler = this.handlers.get(pattern);
      if (!handler) {
        return;
      }

      await this.inbox.idempotentProcess(this.name, event, async () => {
        await handler.bind(this.instance)(event);
      });
    });
  }
}
