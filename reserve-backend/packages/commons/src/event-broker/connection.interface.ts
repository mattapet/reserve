/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '../events/event.entity';
import { EventConsumer } from './event-consumer';
//#endregion

export interface Connection {
  publish(topic: string, event: Event): void;
  subscribe(consumer: EventConsumer): void;
}
