/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EventConsumerStorage } from '../event-consumer.storage';
//#endregion

/**
 * Decorator that annotates a class that whishes to register for event
 * consumption. Any method that should consume any particular topic of events
 * should be annotated with `EventPattern` decorator.
 *
 * @param options Options further specifying consumer.
 */
export function EventConsumer(): ClassDecorator {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return <F extends Function>(constructor: F) => {
    const consumer = { name: constructor.name };
    EventConsumerStorage.addConsumer(consumer);
  };
}
