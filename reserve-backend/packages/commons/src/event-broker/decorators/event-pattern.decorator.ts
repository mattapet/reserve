/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EVENT_PATTERN_METADATA } from '../constants';
//#endregion

/**
 * Decorator that annotates the method and registeres it as the even handler of
 * the class' event consumer for the given `topic`.
 *
 * @note The class the method is part of is expected to be annotated with
 * `EventConsumer` decorator.
 */
export function EventPattern(topic: string): MethodDecorator {
  return (
    target: any,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor,
  ) => {
    const originamMethod = descriptor.value;

    const eventTopics =
      Reflect.getMetadata(EVENT_PATTERN_METADATA, target.constructor) ?? [];

    Reflect.defineMetadata(
      EVENT_PATTERN_METADATA,
      [...eventTopics, [topic, originamMethod]],
      target.constructor,
    );
  };
}
