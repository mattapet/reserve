/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { InboxMeta } from './inbox-meta.entity';
import { Inbox } from './inbox';
import { InboxMetaRepository } from './inbox-meta.repository';
//#endregion

@Module({
  imports: [TypeOrmModule.forFeature([InboxMeta, InboxMetaRepository])],
  providers: [Inbox],
  exports: [Inbox],
})
export class InboxModule {}
