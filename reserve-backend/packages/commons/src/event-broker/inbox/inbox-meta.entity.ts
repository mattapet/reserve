/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity, PrimaryColumn, Column } from 'typeorm';
//#endregion

@Entity({ name: 'inbox_meta' })
export class InboxMeta {
  @PrimaryColumn({ name: 'consumer_name', charset: 'utf8' })
  public consumerName: string;

  @Column({ name: 'latest_consumed', default: 0 })
  public latestConsumed: number;

  public constructor(consumerName: string, latestConsumed: number = 0) {
    this.consumerName = consumerName;
    this.latestConsumed = latestConsumed;
  }
}
