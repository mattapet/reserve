/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { TaskQueue } from '@rsaas/util/lib/task-queue';

import { Transactional, PropagationLevel, IsolationLevel } from '../../typeorm';
import { Event } from '../../events/event.entity';
import { InboxMeta } from './inbox-meta.entity';
import { InboxMetaRepository } from './inbox-meta.repository';
//#endregion

@Injectable()
export class Inbox {
  private readonly taskQueue = new TaskQueue();
  public constructor(private readonly repository: InboxMetaRepository) {}

  public async idempotentProcess(
    name: string,
    event: Event,
    process: () => Promise<void>,
  ): Promise<void> {
    await this.taskQueue.execute(() => this.process(name, event, process));
  }

  @Transactional({
    isolationLevel: IsolationLevel.REPEATABLE_READ,
    propagationLevel: PropagationLevel.REQUIRES_NEW,
  })
  private async process(
    name: string,
    event: Event,
    process: () => Promise<void>,
  ): Promise<void> {
    const meta = (await this.repository.findOne(name)) ?? new InboxMeta(name);
    if (event.eventId! <= meta.latestConsumed) {
      return;
    }
    await process();
    meta.latestConsumed = event.eventId!;
    await this.repository.save(meta);
  }
}
