/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as fs from 'fs';
import { JwtModule } from '@nestjs/jwt';
import { DynamicModule } from '@nestjs/common';
//#endregion

const SERVICE = process.env.SERVICE_NAME;
const JWT_ALG = process.env.JWT_ALG;
const JWT_SECRET = process.env.JWT_SECRET;
const JWT_PUBKEY = process.env.JWT_PUBK;
const JWT_PRIVK = process.env.JWT_PRIVK;

export const registerJWTServiceModule = (): DynamicModule =>
  JwtModule.register({
    publicKey: JWT_PUBKEY ? fs.readFileSync(JWT_PUBKEY) : undefined,
    privateKey: JWT_PRIVK ? fs.readFileSync(JWT_PRIVK) : undefined,
    secret: JWT_SECRET,
    verifyOptions: {
      issuer: SERVICE,
      algorithms: [JWT_ALG! as any],
    },
    signOptions: {
      issuer: SERVICE,
      algorithm: JWT_ALG as any,
    },
  });
