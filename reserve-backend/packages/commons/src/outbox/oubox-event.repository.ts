/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository } from 'typeorm';

import { BaseRepository } from '../typeorm';
import { OutboxEvent } from './outbox-event.entity';
//#endregion

@EntityRepository(OutboxEvent)
export class OutboxEventRepository extends BaseRepository<OutboxEvent> {}
