/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Event } from '../events/event.entity';
import { EventBrokerModule } from '../event-broker';
import { OutboxEvent } from './outbox-event.entity';
import { OutboxProcessor } from './outbox';
import { OutboxEventRepository } from './oubox-event.repository';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([Event, OutboxEvent, OutboxEventRepository]),
    EventBrokerModule.inMemory(),
  ],
  providers: [OutboxProcessor],
  exports: [TypeOrmModule],
})
export class OutboxModule {}
