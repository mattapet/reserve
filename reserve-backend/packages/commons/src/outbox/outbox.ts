/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Inject, Injectable } from '@nestjs/common';
import { Interval } from '@nestjs/schedule';

import { Client } from '../event-broker/client';
import { Event } from '../events/event.entity';
import { EVENT_BROKER_CLIENT } from '../event-broker/constants';
import { OutboxEventRepository } from './oubox-event.repository';
import { Transactional } from '../typeorm';
//#endregion

@Injectable()
export class OutboxProcessor {
  private processing: boolean = false;

  public constructor(
    private readonly repository: OutboxEventRepository,
    @Inject(EVENT_BROKER_CLIENT)
    private readonly client: Client,
  ) {}

  @Interval(200)
  public async processUnpublished(): Promise<void> {
    if (this.processing) {
      return;
    }
    this.processing = true;
    while (await this.processNextEvent());
    this.processing = false;
  }

  /**
   * Return `true`, if an event was successfully processed, `false` otherwise.
   */
  @Transactional()
  private async processNextEvent(): Promise<boolean> {
    const toPublish = await this.repository.findOne({
      order: { eventId: 'ASC' },
    });
    if (!toPublish) {
      return false;
    }

    await this.processEvent(toPublish.event);
    await this.repository.remove(toPublish);
    return true;
  }

  private async processEvent(event: Event): Promise<void> {
    await this.client.publish(event.type, event);
  }
}
