/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity, PrimaryColumn, Column } from 'typeorm';

import { Event } from '../events/event.entity';
//#endregion

@Entity({ name: 'outbox' })
export class OutboxEvent {
  @PrimaryColumn({ name: 'event_id' })
  public readonly eventId: number;

  @Column({ type: 'simple-json' })
  public readonly event: Event;

  public constructor(event: Event) {
    this.eventId = event && event.eventId!;
    this.event = event;
  }
}
