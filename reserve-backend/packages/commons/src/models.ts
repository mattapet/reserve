/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { InboxMeta } from './event-broker/inbox/inbox-meta.entity';
import { Event } from './events/event.entity';
import { Snapshot } from './events/snapshot.entity';
import { OutboxEvent } from './outbox/outbox-event.entity';

export const COMMONS_MODELS = [InboxMeta, Event, Snapshot, OutboxEvent];
