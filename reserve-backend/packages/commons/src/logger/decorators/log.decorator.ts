/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Logger } from '../logger.interface';
import { LogBuilder } from '../builders';
import { LOGGER_INSTANCE } from '../constants';

import { InjectLogger } from './inject-logger.decorator';
//#endregion

function createLogMessage(logBuilder: LogBuilder, format?: string): string {
  if (format) {
    return logBuilder.withFormat(format).build();
  } else {
    return logBuilder.withDefaultFormat().build();
  }
}

export interface LoggerOptions {
  readonly format?: string;
}

export function Log(options: LoggerOptions = {}): MethodDecorator {
  const injectLogger = InjectLogger();

  return (
    target: any,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor,
  ) => {
    injectLogger(target, LOGGER_INSTANCE);
    const originalMethod = descriptor.value;

    descriptor.value = function (this: any, ...args: any[]) {
      const logger: Logger | undefined = this[LOGGER_INSTANCE];
      const { constructor } = Object.getPrototypeOf(this);
      logger?.setContext(constructor.name);

      const logBuilder = new LogBuilder()
        .withArguments(args)
        .withClassName(constructor.name)
        .withMethodName(propertyKey.toString());

      const start = Date.now();
      const result = originalMethod.apply(this, args);

      if (result instanceof Promise) {
        return new Promise<any>(async (resolve, reject) => {
          try {
            const res = await result;
            const end = Date.now();
            logger?.log(
              createLogMessage(
                logBuilder.withReturn(result).withExecutionTime(end - start),
                options.format,
              ),
            );
            resolve(res);
          } catch (error) {
            reject(error);
          }
        });
      } else {
        const end = Date.now();
        logger?.log(
          createLogMessage(
            logBuilder.withReturn(result).withExecutionTime(end - start),
            options.format,
          ),
        );
        return result;
      }
    };
  };
}
