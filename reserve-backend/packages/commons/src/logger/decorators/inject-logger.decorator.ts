/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { applyDecorators, Inject } from '@nestjs/common';

import { NestLogger } from '../nest/logger.service';
//#endregion

// eslint-disable-next-line @typescript-eslint/ban-types
export function InjectLogger(): <TFunction extends Function, Y>(
  target: Record<string, any> | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void {
  return applyDecorators(Inject(NestLogger));
}
