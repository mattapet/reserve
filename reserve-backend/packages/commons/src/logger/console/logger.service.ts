/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

//#region imports
import { Logger } from '../logger.interface';
//#endregion

const SUPPRESS_LOGS = process.env.SUPPRESS_LOGS;

enum LogLevel {
  error = 'ERROR',
  log = 'LOG',
  warn = 'WARN',
  debug = 'DEBUG',
  verbose = 'VERBOSE',
}

export class ConsoleLogger implements Logger {
  private context?: string;

  public error(message: any, trace?: string, context?: string): void {
    this.writeLog(
      LogLevel.error,
      message + trace ?? '',
      context ?? this.context,
    );
  }

  public log(message: any, context?: string): void {
    this.writeLog(LogLevel.log, message, context ?? this.context);
  }

  public warn(message: any, context?: string): void {
    this.writeLog(LogLevel.warn, message, context ?? this.context);
  }

  public debug(message: any, context?: string): void {
    this.writeLog(LogLevel.debug, message, context ?? this.context);
  }

  public verbose(message: any, context?: string): void {
    this.writeLog(LogLevel.verbose, message, context ?? this.context);
  }

  public setContext(context: string): void {
    this.context = context;
  }

  private writeLog(level: LogLevel, message: any, context?: string): void {
    if (SUPPRESS_LOGS) {
      return;
    }

    const logMessage = [`[${level}]`];
    if (context) {
      logMessage.push(`[${context}]`);
    }
    logMessage.push('-', message);
    // tslint:disable:no-console
    console.log(logMessage.join(' '));
  }
}
