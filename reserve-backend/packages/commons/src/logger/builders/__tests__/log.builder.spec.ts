/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { LogBuilder } from '../log.builder';

describe('log.builder', () => {
  it('should return a default formatted log message', () => {
    const message = new LogBuilder()
      .withArguments([1, 2, 3])
      .withClassName('SomeClass')
      .withMethodName('someMethod')
      .withReturn({ value: 44 })
      .withExecutionTime(44)
      .withDefaultFormat()
      .build();

    expect(message).toBe(
      `Method '${'someMethod'}' with arguments ${'["1","2","3"]'} returned ${'{"value":44}'} in 44ms`,
    );
  });

  it('should return a custom formatted log message', () => {
    const message = new LogBuilder()
      .withArguments([1, 2, 3])
      .withClassName('SomeClass')
      .withMethodName('someMethod')
      .withReturn({ value: 44 })
      .withExecutionTime(44)
      .withFormat(
        'Second {1} and first {0} arguments produced %return within method %M of class %C in %Tms',
      )
      .build();

    expect(message).toBe(
      `Second ${'2'} and first ${'1'} arguments produced ${'{"value":44}'} within method someMethod of class SomeClass in 44ms`,
    );
  });
});
