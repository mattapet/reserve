/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

//#region imports
import { formatString, notNull } from '@rsaas/util';
//#endregion

export class LogBuilder {
  private className?: string;
  private methodName?: string;
  private methodArguments?: any[];
  private returnValue?: any;
  private executionTime?: number;
  private format?: string;

  public withClassName(className: string): this {
    this.className = className;
    return this;
  }

  public withMethodName(methodName: string): this {
    this.methodName = methodName;
    return this;
  }

  public withArguments(methodArguments: any[]): this {
    this.methodArguments = methodArguments.map((arg) => JSON.stringify(arg));
    return this;
  }

  public withReturn(returnValue: any): this {
    this.returnValue = returnValue;
    return this;
  }

  public withExecutionTime(executionTime: number): this {
    this.executionTime = executionTime;
    return this;
  }

  public withFormat(format: string): this {
    this.format = format;
    return this;
  }

  public withDefaultFormat(): this {
    this.format = `Method '%M' with arguments %args returned %return in %Tms`;
    return this;
  }

  public build(): string {
    return formatString(notNull(this.format), ...notNull(this.methodArguments))
      .replace('%return', JSON.stringify(this.returnValue))
      .replace('%args', JSON.stringify(notNull(this.methodArguments)))
      .replace('%C', `${notNull(this.className)}`)
      .replace('%M', `${notNull(this.methodName)}`)
      .replace('%T', `${notNull(this.executionTime)}`);
  }
}
