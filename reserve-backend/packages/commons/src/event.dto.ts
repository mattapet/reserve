/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class EventDTO {
  @ApiProperty()
  readonly eventId?: number;
  @ApiProperty()
  readonly type!: string;
  @ApiProperty({ type: 'timestamp' })
  readonly timestamp!: string;
  @ApiProperty()
  readonly payload!: { [p: string]: any };
}
