/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { LoggerModule } from './logger';
import { HttpModule } from './http';
import { EventBrokerModule } from './event-broker';
import { TypeormEventRepository } from './events/repositories/typeorm/typeorm-event.repository';
import { Event } from './events/event.entity';
import { OutboxModule } from './outbox';
import { Snapshot } from './events/snapshot.entity';
import { TypeormSnapshotRepository } from './events/repositories/typeorm/typeorm-snapshot.repository';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Event,
      Snapshot,
      TypeormEventRepository,
      TypeormSnapshotRepository,
    ]),
    LoggerModule,
    EventBrokerModule.inMemory(),
    HttpModule,
    OutboxModule,
  ],
  exports: [
    TypeOrmModule,
    EventBrokerModule.inMemory(),
    LoggerModule,
    HttpModule,
  ],
})
export class CommonsModule {}
