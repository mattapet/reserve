/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { PrimaryGeneratedColumn, Index, Column, Entity } from 'typeorm';
import { AnyObject } from '@rsaas/util/lib/AnyObject';
//#endregion

@Index(['rowId', 'aggregateId'])
@Entity({ name: 'events_table' })
export class Event<Payload extends AnyObject = AnyObject> {
  @PrimaryGeneratedColumn({ name: 'event_id' })
  public readonly eventId?: number;

  @Column({ name: 'type', charset: 'utf8' })
  public readonly type: string;

  @Column({ name: 'row_id', charset: 'utf8' })
  public readonly rowId: string;

  @Column({ name: 'aggregate_id', charset: 'utf8' })
  public readonly aggregateId: string;

  @Index()
  @Column({
    transformer: {
      to: (d) => d ?? new Date(Date.now()).toISOString(),
      from: (d: Date) => d,
    },
  })
  public readonly timestamp: Date;

  @Column({ type: 'simple-json', charset: 'utf8' })
  public readonly payload: Payload;

  public constructor(
    type: string,
    rowId: string,
    aggregateId: string,
    timestamp: Date,
    payload: Payload,
  ) {
    this.type = type;
    this.rowId = rowId;
    this.aggregateId = aggregateId;
    this.timestamp = timestamp;
    this.payload = payload;
  }
}
