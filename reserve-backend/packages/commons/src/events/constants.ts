/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export const COMMAND_HANDLER_METADATA = '__COMMAND_HANDLER_METADATA__';
export const EVENT_HANDLER_METADATA = '__EVENT_HANDLER_METADATA__';
