/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from './event.entity';
//#endregion

export interface EventRepository<EventType extends Event> {
  /**
   * Returns the timestamp of the latest stored event in the store.
   */
  getLatestTimestamp(): Promise<Date>;

  dumpEvents(dateRange?: { since?: Date; until?: Date }): Promise<EventType[]>;

  getAll(id?: number): Promise<EventType[]>;

  /**
   * Returns all events for the given `rowId`.
   */
  findByRowId(rowId: string, since?: number): Promise<EventType[]>;

  /**
   * Returns all events for the give `rowId` and `aggregateId`.
   */
  findByRowIdAndAggregateId(
    rowId: string,
    aggregateId: string,
  ): Promise<EventType[]>;

  /**
   * Saves all provided events returning their identities.
   */
  saveEvents(...events: EventType[]): Promise<EventType[]>;
}
