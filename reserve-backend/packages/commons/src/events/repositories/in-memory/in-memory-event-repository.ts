/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EventRepository } from '../../event.repository';
import { Event } from '../../event.entity';
//#endregion

export class InMemoryEventRepository<EventType extends Event>
  implements EventRepository<EventType> {
  private eventId: number = 1;
  private storage: EventType[] = [];

  public async getLatestTimestamp(): Promise<Date> {
    return this.storage.length
      ? this.storage[this.storage.length - 1].timestamp
      : new Date(0);
  }

  public async getAll(id: number = 0): Promise<EventType[]> {
    return this.storage.filter(({ eventId }) => eventId! > id);
  }

  public async dumpEvents(dateRange?: {
    since?: Date;
    until?: Date;
  }): Promise<EventType[]> {
    const { since, until } = dateRange || {};
    return this.getAllEvents()
      .filter(this.getFilterSince(since))
      .filter(this.getFilterUntil(until));
  }

  private getAllEvents(): EventType[] {
    return this.storage;
  }

  private getFilterSince(since?: Date): (event: EventType) => boolean {
    return ({ timestamp }) => !since || timestamp >= since;
  }

  private getFilterUntil(until?: Date): (event: EventType) => boolean {
    return ({ timestamp }) => !until || timestamp <= until;
  }

  public async findByRowId(
    rowId: string,
    since?: number,
  ): Promise<EventType[]> {
    return this.storage
      .filter((event) => event.rowId === rowId)
      .filter((e) => since == null || e.eventId! > since);
  }

  public async findByRowIdAndAggregateId(
    rowId: string,
    aggregateId: string,
  ): Promise<EventType[]> {
    const row = await this.findByRowId(rowId);
    return row.filter((event) => event.aggregateId === aggregateId);
  }

  public async saveEvents(...events: EventType[]): Promise<EventType[]> {
    for (const event of events) {
      this.saveEvent(event);
    }
    return events;
  }

  private saveEvent(event: EventType): void {
    this.storage.push({ ...event, eventId: this.eventId++ });
  }
}
