/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { SnapshotRepository } from '../../snapshot.repository';
import { Snapshot } from '../../snapshot.entity';
//#endregion

export class InMemorySnapshotRepository implements SnapshotRepository {
  public constructor(
    private readonly storage: { [rowId: string]: Snapshot } = {},
  ) {}

  public async getByRowId(rowId: string): Promise<Snapshot> {
    const snapshot = this.storage[rowId];
    if (!snapshot) {
      return new Snapshot(rowId, 0, []);
    }
    return snapshot;
  }

  public async save(snapshot: Snapshot): Promise<Snapshot> {
    return (this.storage[snapshot.rowId] = snapshot);
  }
}
