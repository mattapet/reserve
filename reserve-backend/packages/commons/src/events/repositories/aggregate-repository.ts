/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { groupBy } from '@rsaas/util/lib/groupBy';
import { Identifiable } from '@rsaas/util/lib/identifiable.interface';
import { keyBy } from '@rsaas/util/lib/key-by';
import { deserialize, serialize } from 'class-transformer';

import { Log } from '../../logger';

import { Command } from '../command';
import { Event } from '../event.entity';
import { EventRepository } from '../event.repository';
import { AggregateCommandHandler } from '../handlers/aggregate.command-handler';
import { AggregateEventHandler } from '../handlers/aggregate.event-handler';
import { SnapshotRepository } from '../snapshot.repository';
import { Snapshot } from '../snapshot.entity';
import { Transactional } from '../../typeorm';
//#endregion

/**
 * Shape representing events grouped by their `aggregateId`.
 */
export interface GroupedById<EventType extends Event> {
  [aggregateId: string]: EventType[];
}

/**
 * A base class providing an event-sourced interface to an aggregate root. An
 * instance of `AggregateRepository` operates only on a single aggregate, using
 * associated instance of `AggregateCommandHandler` and `AggregateEventHandler`.
 * Each `AggregateRepository` also requires an instance of `EventRepository` to
 * access the underlying event store.
 */
export class AggregateRepository<
  CommandType extends Command,
  EventType extends Event,
  Aggregate extends Identifiable
> {
  public constructor(
    private readonly eventRepository: EventRepository<EventType>,
    private readonly snapshotRepository: SnapshotRepository,
    private readonly commandHandler: AggregateCommandHandler<
      Aggregate,
      EventType,
      CommandType
    >,
    private readonly eventHandler: AggregateEventHandler<Aggregate, EventType>,
    private readonly AggregateClass: new () => Aggregate,
  ) {}

  /**
   * Retrieves all events from specified date range.
   */
  public async dumpEvents(
    rowId: string,
    aggregateId?: string,
  ): Promise<EventType[]> {
    if (aggregateId) {
      return this.eventRepository.findByRowIdAndAggregateId(rowId, aggregateId);
    } else {
      return this.eventRepository.findByRowId(rowId);
    }
  }

  /**
   * Retrieves all aggregates for the given `rowId`.
   */
  public async getAll(rowId: string): Promise<Aggregate[]> {
    const [events, snapshot] = await this.getEventsWithLatestSnapshot(rowId);
    return this.assembleAggregates(events, snapshot);
  }

  private async getEventsWithLatestSnapshot(
    rowId: string,
  ): Promise<[EventType[], Aggregate[]]> {
    const [since, snapshot] = await this.getLatestSnapshot(rowId);
    const events = await this.eventRepository.findByRowId(rowId, since);
    return [events, snapshot];
  }

  private async assembleAggregates(
    events: EventType[],
    snapshot: Aggregate[],
  ): Promise<Aggregate[]> {
    const aggregates = this.applyEventsToAggregates(
      this.groupEventsByAggregate(events, snapshot),
    );

    await this.createSnapshotIfNumberOfEventsThresholds(aggregates, events);
    return aggregates;
  }

  private applyEventsToAggregates(
    grouped: [Aggregate, EventType[]][],
  ): Aggregate[] {
    return grouped.map(([aggregate, events]) =>
      this.eventHandler.apply(aggregate, events),
    );
  }

  private async getLatestSnapshot(
    rowId: string,
  ): Promise<[number, Aggregate[]]> {
    const snapshot = await this.snapshotRepository.getByRowId(rowId);
    return [snapshot.eventId, this.deserializeSnapshot(snapshot)];
  }

  private async createSnapshotIfNumberOfEventsThresholds(
    aggregates: Aggregate[],
    events: EventType[],
  ): Promise<void> {
    if (events.length < 500) {
      return;
    }

    const { rowId, eventId } = events[events.length - 1];
    const snapshot = this.serializeSnapshot(rowId, eventId!, aggregates);
    await this.snapshotRepository.save(snapshot);
  }

  private deserializeSnapshot(snapshot: Snapshot): Aggregate[] {
    return snapshot.payload.map((serialized) =>
      deserialize(this.AggregateClass, serialized),
    );
  }

  @Log({
    format:
      // tslint:disable-next-line:quotemark
      "Created snapshot for row id '{0}' with latest event '{1}' and aggregates {2}",
  })
  private serializeSnapshot(
    rowId: string,
    eventId: number,
    aggregates: Aggregate[],
  ): Snapshot {
    const payload = aggregates.map((aggregate) => serialize(aggregate));
    return new Snapshot(rowId, eventId, payload);
  }

  /**
   * Returns an aggregate for the given `aggregateId` and `rowId`.
   */
  public async getById(rowId: string, aggregateId: string): Promise<Aggregate> {
    const events = await this.eventRepository.findByRowIdAndAggregateId(
      rowId,
      aggregateId,
    );
    return this.eventHandler.apply(new this.AggregateClass(), events);
  }

  /**
   * Performs given `command` on the provided `aggregate`.
   */
  @Transactional()
  public async execute(
    command: CommandType,
    aggregate: Aggregate,
  ): Promise<Aggregate> {
    const events = this.commandHandler.execute(aggregate, command);
    const saved = await this.eventRepository.saveEvents(...events);
    return this.eventHandler.apply(aggregate, saved);
  }

  /**
   * Groups events by their `aggregateId`.
   */
  private groupEventsByAggregate(
    events: EventType[],
    snapshot: Aggregate[],
  ): [Aggregate, EventType[]][] {
    const keyedAggregates = this.keyAggregatesById(snapshot);
    const eventsGroupedByAggregateId = groupBy(
      events,
      (event) => event.aggregateId,
    );

    const aggregateIds = new Set([
      ...Object.keys(keyedAggregates),
      ...Object.keys(eventsGroupedByAggregateId),
    ]);

    return [...aggregateIds].map((aggregateId) => [
      keyedAggregates[aggregateId] ?? new this.AggregateClass(),
      eventsGroupedByAggregateId[aggregateId] ?? [],
    ]);
  }

  private keyAggregatesById(
    aggregates: Aggregate[],
  ): { [id: string]: Aggregate } {
    return keyBy(aggregates, (next) => next.getId());
  }
}
