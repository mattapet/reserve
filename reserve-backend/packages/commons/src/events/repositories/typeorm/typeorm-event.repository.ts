/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  EntityRepository,
  MoreThanOrEqual,
  Between,
  LessThanOrEqual,
  MoreThan,
} from 'typeorm';

import { BaseRepository, Transactional } from '../../../typeorm';
import { Event } from '../../event.entity';
import { EventRepository } from '../../event.repository';
import { OutboxEvent } from '../../../outbox/outbox-event.entity';
//#endregion

@EntityRepository(Event)
export class TypeormEventRepository<EventType extends Event>
  extends BaseRepository<Event>
  implements EventRepository<EventType> {
  public async getLatestTimestamp(): Promise<Date> {
    const event = await this.findOne({
      order: { eventId: 'DESC' },
    });
    return event?.timestamp ?? new Date(0);
  }

  public async getAll(id?: number): Promise<EventType[]> {
    const where: { eventId?: any } = {};
    if (id != null) {
      where.eventId = MoreThanOrEqual(id);
    }

    const events = await this.find({ where, order: { eventId: 'ASC' } });
    return events.map(this.unwrap);
  }

  public async dumpEvents(dateRange?: {
    since?: Date;
    until?: Date;
  }): Promise<EventType[]> {
    const where: { timestamp?: any } = {};
    if (dateRange) {
      if (dateRange.since && dateRange.until) {
        where.timestamp = Between(dateRange.since, dateRange.until);
      } else if (dateRange.since) {
        where.timestamp = MoreThanOrEqual(dateRange.since);
      } else {
        where.timestamp = LessThanOrEqual(dateRange.until);
      }
    }

    const events = await this.find({
      where,
      order: { eventId: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  public async findByRowId(
    rowId: string,
    since?: number,
  ): Promise<EventType[]> {
    const where: any = { rowId };

    if (since) {
      where.eventId = MoreThan(since);
    }

    const events = await this.find({ where, order: { eventId: 'ASC' } });
    return events.map(this.unwrap);
  }

  public async findByRowIdAndAggregateId(
    rowId: string,
    aggregateId: string,
  ): Promise<EventType[]> {
    const events = await this.find({
      where: { rowId, aggregateId },
      order: { eventId: 'ASC' },
    });
    return events.map(this.unwrap);
  }

  @Transactional()
  public async saveEvents(...events: EventType[]): Promise<EventType[]> {
    const stored = await this.save(events.map(this.wrap));
    await this.manager.save(OutboxEvent, stored.map(this.wrapOutboxed));
    return stored.map(this.unwrap);
  }

  private wrap(event: EventType): Event {
    const { type, rowId: workspaceId, aggregateId, timestamp, payload } = event;
    return new Event(type, workspaceId, aggregateId, timestamp, payload);
  }

  private wrapOutboxed(event: Event): OutboxEvent {
    return new OutboxEvent(event);
  }

  private unwrap(entity: Event): EventType {
    return (entity as any) as EventType;
  }
}
