/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository } from 'typeorm';

import { Snapshot } from '../../snapshot.entity';
import { SnapshotRepository } from '../../snapshot.repository';
import { BaseRepository } from '../../../typeorm';
//#endregion

@EntityRepository(Snapshot)
export class TypeormSnapshotRepository
  extends BaseRepository<Snapshot>
  implements SnapshotRepository {
  public async getByRowId(rowId: string): Promise<Snapshot> {
    const item = await this.findOne(rowId);
    if (!item) {
      return new Snapshot(rowId, 0, []);
    }
    return item;
  }
}
