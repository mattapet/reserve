/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AggregateRepository } from '../aggregate-repository';

import { InMemoryEventRepository } from '../in-memory/in-memory-event-repository';
import { AggregateCommandHandler } from '../../handlers/aggregate.command-handler';
import { AggregateEventHandler } from '../../handlers/aggregate.event-handler';

// Fakes
import { CommandType } from './counter/counter.command';
import { EventType } from './counter/counter.event';
import { Counter } from './counter/counter.entity';
import { CounterCommandHandler } from './counter/counter-command-handler';
import { CounterEventHandler } from './counter/counter.event-handler';
import { InMemorySnapshotRepository } from '../in-memory/in-memory-snapshot.repository';
//#endregion

describe('aggregate repository', () => {
  let aggregateRepository!: AggregateRepository<
    CommandType,
    EventType,
    Counter
  >;
  let eventRepository!: InMemoryEventRepository<EventType>;
  let snapshotRepository!: InMemorySnapshotRepository;
  let commandHandler!: CounterCommandHandler;
  let eventHandler!: CounterEventHandler;

  beforeEach(() => {
    eventRepository = new InMemoryEventRepository();
    snapshotRepository = new InMemorySnapshotRepository();
    commandHandler = new CounterCommandHandler();
    eventHandler = new CounterEventHandler();
    aggregateRepository = new AggregateRepository(
      eventRepository,
      snapshotRepository,
      commandHandler,
      eventHandler,
      Counter,
    );
  });

  //#region helper functions
  function make_command<T extends string>(type: T) {
    return {
      type,
      rowId: 'test',
      aggregateId: '99',
      payload: {},
    };
  }

  function make_commandWithRowId<T extends string>(type: T, rowId: string) {
    return { ...make_command(type), rowId };
  }

  function make_commandWithAggregateId<T extends string>(
    type: T,
    aggregateId: string,
  ) {
    return { ...make_command(type), aggregateId };
  }

  function make_event<T extends string>(type: T) {
    return {
      type,
      rowId: 'test',
      aggregateId: '99',
      timestamp: new Date(Date.now()),
      payload: {},
    };
  }
  function make_eventWithRowId<T extends string>(type: T, rowId: string) {
    return { ...make_event(type), rowId };
  }

  async function effect_executeCommands(...commands: CommandType[]) {
    await commands.reduce(async (promise, command) => {
      await promise;
      await aggregateRepository.execute(command, new Counter());
    }, Promise.resolve());
  }
  //#endregion

  describe('executing commands', () => {
    it('should return incremented aggregate', async () => {
      const command = make_command('counter.increment');

      const result = await aggregateRepository.execute(command, new Counter());

      expect(result).toEqual(new Counter('99', 1));
    });

    it('should return decremented aggregate', async () => {
      const command = make_command('counter.decrement');

      const result = await aggregateRepository.execute(command, new Counter());

      expect(result).toEqual(new Counter('99', -1));
    });
  });

  describe('retrieving events', () => {
    const rowId = 'test';

    it('should return an empty array if no command were executed', async () => {
      const result = await aggregateRepository.dumpEvents(rowId);

      expect(result).toEqual([]);
    });

    it('should return an all stored events', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const commands = [
        make_command('counter.increment'),
        make_command('counter.increment'),
      ];
      await effect_executeCommands(...commands);

      const result = await aggregateRepository.dumpEvents(rowId);

      expect(result).toEqual([
        { ...make_event('counter.incremented'), eventId: 1 },
        { ...make_event('counter.incremented'), eventId: 2 },
      ]);
    });

    it('should return an all stored events from different rows', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const commands = [
        make_commandWithRowId('counter.increment', 'row1'),
        make_commandWithRowId('counter.increment', 'row2'),
      ];
      await effect_executeCommands(...commands);

      const result1 = await aggregateRepository.dumpEvents('row1');
      const result2 = await aggregateRepository.dumpEvents('row2');

      expect([...result1, ...result2]).toEqual([
        { ...make_eventWithRowId('counter.incremented', 'row1'), eventId: 1 },
        { ...make_eventWithRowId('counter.incremented', 'row2'), eventId: 2 },
      ]);
    });
  });

  describe('retrieving aggregates', () => {
    it('should return default aggregate if no events are present', async () => {
      const result = await aggregateRepository.getById('test', '99');

      expect(result).toEqual(new Counter(undefined, 0));
    });

    it('should return an empty array if no events are present', async () => {
      const result = await aggregateRepository.getAll('test');

      expect(result).toEqual([]);
    });

    it('should return aggregate for stored events', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const commands = [
        make_command('counter.increment'),
        make_command('counter.increment'),
      ];
      await effect_executeCommands(...commands);

      const result = await aggregateRepository.getById('test', '99');

      expect(result).toEqual(new Counter('99', 2));
    });

    it('should return all stored aggregates', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const commands = [
        make_commandWithAggregateId('counter.increment', 'aggregate1'),
        make_commandWithAggregateId('counter.decrement', 'aggregate2'),
      ];
      await effect_executeCommands(...commands);

      const results = await aggregateRepository.getAll('test');

      expect(results).toEqual([
        new Counter('aggregate1', 1),
        new Counter('aggregate2', -1),
      ]);
    });
  });

  describe('snapshotting', () => {
    it('should create snapshot when number of events reaches 500', async () => {
      const commands = [...new Array(500)].map(() =>
        make_command('counter.increment'),
      );
      await effect_executeCommands(...commands);
      const result1 = await aggregateRepository.getAll('test');
      const result2 = await aggregateRepository.getAll('test');

      expect(result1).toEqual([new Counter('99', 500)]);
      expect(result2).toEqual([new Counter('99', 500)]);
    });
  });

  describe('invalid commands', () => {
    it('should throw an error if unrecognized command executed', async () => {
      const command = make_command('counter.unrecognized_command');

      const fn = aggregateRepository.execute(command as any, new Counter());

      await expect(fn).rejects.toThrow(Error);
    });

    it('should throw an error if no command handlers specified', async () => {
      const handler = new AggregateCommandHandler();
      const command = make_command('counter.increment');

      const fn = () => handler.execute({}, command);

      expect(fn).toThrow(Error);
    });
  });

  describe('invalid events', () => {
    it('should throw an error if unrecognized event encountered', async () => {
      const command = make_command('counter.increment');
      jest
        .spyOn(commandHandler, 'execute')
        .mockImplementation(() => [
          make_event('counter.unrecognized_event') as any,
        ]);

      const fn = aggregateRepository.execute(command, new Counter());

      await expect(fn).rejects.toThrow(Error);
    });

    it('should throw an error if no event handlers specified', async () => {
      const handler = new AggregateEventHandler();
      const event = make_event('counter.incremented');

      const fn = () => handler.apply({}, [event]);

      expect(fn).toThrow(Error);
    });
  });
});
