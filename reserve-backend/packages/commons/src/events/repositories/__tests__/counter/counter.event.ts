/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '../../../event.entity';
//#endregion

export interface Incremented extends Event {
  readonly type: 'counter.incremented';
}
export interface Decremented extends Event {
  readonly type: 'counter.decremented';
}
export type EventType = Incremented | Decremented;
