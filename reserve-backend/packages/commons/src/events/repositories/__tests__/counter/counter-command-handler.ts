/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AggregateCommandHandler } from '../../../handlers';
import { Counter } from './counter.entity';
import { EventType } from './counter.event';
import { CommandType, Increment, Decrement } from './counter.command';
import { CommandHandler } from '../../../decorators';
//#endregion

export class CounterCommandHandler extends AggregateCommandHandler<
  Counter,
  EventType,
  CommandType
> {
  @CommandHandler('counter.increment')
  public executeIncrement(
    aggregate: Counter,
    { rowId, aggregateId }: Increment,
  ): EventType[] {
    return [
      {
        type: 'counter.incremented',
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {},
      },
    ];
  }

  @CommandHandler('counter.decrement')
  public executeDecrement(
    aggregate: Counter,
    { rowId, aggregateId }: Decrement,
  ): EventType[] {
    return [
      {
        type: 'counter.decremented',
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {},
      },
    ];
  }
}
