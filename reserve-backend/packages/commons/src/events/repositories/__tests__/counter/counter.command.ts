/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '../../../command';
//#endregion

export interface Increment extends Command {
  readonly type: 'counter.increment';
}
export interface Decrement extends Command {
  readonly type: 'counter.decrement';
}
export type CommandType = Increment | Decrement;
