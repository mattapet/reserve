/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AggregateEventHandler } from '../../../handlers';
import { Counter } from './counter.entity';
import { EventType, Incremented, Decremented } from './counter.event';
import { EventHandler } from '../../../decorators';
//#endregion

export class CounterEventHandler extends AggregateEventHandler<
  Counter,
  EventType
> {
  @EventHandler('counter.incremented')
  public applyIncremented(aggregate: Counter, event: Incremented): Counter {
    aggregate.setId(event.aggregateId);
    aggregate.increment();
    return aggregate;
  }

  @EventHandler('counter.decremented')
  public applyDecremented(aggregate: Counter, event: Decremented): Counter {
    aggregate.setId(event.aggregateId);
    aggregate.decrement();
    return aggregate;
  }
}
