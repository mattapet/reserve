/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Identifiable } from '@rsaas/util/lib/identifiable.interface';
//#endregion

export class Counter implements Identifiable {
  public constructor(private id?: string, private value: number = 0) {}

  public setId(id: string): void {
    this.id = id;
  }

  public getId(): string {
    return this.id!;
  }

  public getValue(): number {
    return this.value;
  }

  public increment(): void {
    this.value += 1;
  }

  public decrement(): void {
    this.value -= 1;
  }
}
