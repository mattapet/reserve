/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Inject } from '@nestjs/common';

import { TypeormEventRepository } from '../repositories/typeorm/typeorm-event.repository';
//#endregion

export function InjectEventRepository(): (
  target: Record<string, any>,
  key: string | symbol,
  index?: number,
) => void {
  return Inject(TypeormEventRepository);
}
