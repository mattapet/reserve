/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Inject } from '@nestjs/common';

import { TypeormSnapshotRepository } from '../repositories/typeorm/typeorm-snapshot.repository';
//#endregion

export function InjectSnapshotRepository(): (
  target: Record<string, any>,
  key: string | symbol,
  index?: number,
) => void {
  return Inject(TypeormSnapshotRepository);
}
