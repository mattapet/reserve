/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EVENT_HANDLER_METADATA } from '../constants';
//#endregion

export function EventHandler<T extends string>(eventType: T): MethodDecorator {
  return (
    target: any,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor,
  ) => {
    const originalMethod = descriptor.value;

    const eventHandlers =
      Reflect.getMetadata(EVENT_HANDLER_METADATA, target.constructor) ?? [];

    Reflect.defineMetadata(
      EVENT_HANDLER_METADATA,
      [...eventHandlers, [eventType, originalMethod]],
      target.constructor,
    );
  };
}
