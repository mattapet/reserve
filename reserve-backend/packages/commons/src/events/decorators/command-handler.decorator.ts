/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { COMMAND_HANDLER_METADATA } from '../constants';
//#endregion

export function CommandHandler<T extends string>(
  commandType: T,
): MethodDecorator {
  return (
    target: any,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor,
  ) => {
    const originalMethod = descriptor.value;

    const commandHandlers =
      Reflect.getMetadata(COMMAND_HANDLER_METADATA, target.constructor) ?? [];

    Reflect.defineMetadata(
      COMMAND_HANDLER_METADATA,
      [...commandHandlers, [commandType, originalMethod]],
      target.constructor,
    );
  };
}
