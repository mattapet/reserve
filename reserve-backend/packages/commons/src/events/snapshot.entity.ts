/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { PrimaryColumn, Column, Entity } from 'typeorm';
//#endregion

@Entity({ name: 'snapshot_table' })
export class Snapshot {
  @PrimaryColumn({ name: 'row_id', charset: 'utf8' })
  public rowId: string;

  @Column({ name: 'event_id' })
  public readonly eventId: number;

  @Column({ type: 'simple-json', charset: 'utf8' })
  public readonly payload: string[];

  public constructor(rowId: string, eventId: number, payload: string[]) {
    this.rowId = rowId;
    this.eventId = eventId;
    this.payload = payload;
  }
}
