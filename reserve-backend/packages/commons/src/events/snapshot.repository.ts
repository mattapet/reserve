/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Snapshot } from './snapshot.entity';
//#endregion

export interface SnapshotRepository {
  getByRowId(rowId: string): Promise<Snapshot>;
  save(snapshot: Snapshot): Promise<Snapshot>;
}
