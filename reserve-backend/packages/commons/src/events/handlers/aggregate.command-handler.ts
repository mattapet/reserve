/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Log } from '../../logger';

import { COMMAND_HANDLER_METADATA } from '../constants';
import { Event } from '../event.entity';
import { Command } from '../command';
//#endregion

/**
 * A base class that ane aggregate command handler shall extend. Subclasses of
 * `AggregateCommandHandler` are expected to register their methods using
 * `CommandHandler` decorator, to map them for handling specific commands. If an
 * appropriate method is not found, an error is thrown.
 */
export class AggregateCommandHandler<
  Aggregate,
  EventType extends Event,
  CommandType extends Command
> {
  @Log({ format: 'Executed command {1} producing events %return' })
  public execute(aggregate: Aggregate, command: CommandType): EventType[] {
    const handlerMethod = this.getHandlerMethodFor(command);
    return handlerMethod.bind(this)(aggregate, command);
  }

  private getHandlerName(): string {
    return this.getConstructor().name;
  }

  private getHandlerMethodFor(
    command: CommandType,
  ): (a: Aggregate, e: Command) => EventType[] {
    const handlerName = this.getHandlerName();
    const handlers = this.getEventHandlers();

    const eventHandler = handlers.find(([type]) => command.type === type);
    if (!eventHandler) {
      throw new Error(
        `Event handler '${handlerName}' does not have handler for event type '${command.type}'`,
      );
    }

    return eventHandler[1] as any;
  }

  private getConstructor() {
    return Object.getPrototypeOf(this).constructor;
  }

  private getEventHandlers(): any[] {
    const constructor = this.getConstructor();
    return Reflect.getMetadata(COMMAND_HANDLER_METADATA, constructor) ?? [];
  }
}
