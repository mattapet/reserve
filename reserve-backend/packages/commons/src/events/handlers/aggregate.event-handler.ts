/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EVENT_HANDLER_METADATA } from '../constants';
import { Event } from '../event.entity';
//#endregion

/**
 * A base class that ane aggregate event handler shall extend. Subclasses of
 * `AggregateEventHandler` are expected to register their methods using
 * `EventHandler` decorator, to map them for handling specific events. If an
 * appropriate method is not found, an error is thrown.
 */
export class AggregateEventHandler<Aggregate, EventType extends Event> {
  public apply(aggregate: Aggregate, events: EventType[]): Aggregate {
    return events.reduce(this.getEventApplier(), aggregate);
  }

  private getEventApplier(): (
    aggregate: Aggregate,
    event: EventType,
  ) => Aggregate {
    return (aggregate: Aggregate, event: EventType) => {
      const handlerMethod = this.getHandlerMethodFor(event);
      return handlerMethod.bind(this)(aggregate, event);
    };
  }

  private getHandlerName(): string {
    return this.getConstructor().name;
  }

  private getHandlerMethodFor(
    event: EventType,
  ): (a: Aggregate, e: Event) => Aggregate {
    const handlerName = this.getHandlerName();
    const handlers = this.getEventHandlers();

    const eventHandler = handlers.find(([type]) => event.type === type);
    if (!eventHandler) {
      throw new Error(
        `Event handler '${handlerName}' does not have handler for event type '${event.type}'`,
      );
    }

    return eventHandler[1] as any;
  }

  private getConstructor() {
    return Object.getPrototypeOf(this).constructor;
  }

  private getEventHandlers(): any[] {
    const constructor = this.getConstructor();
    return Reflect.getMetadata(EVENT_HANDLER_METADATA, constructor) ?? [];
  }
}
