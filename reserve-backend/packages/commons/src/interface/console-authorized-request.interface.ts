/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
// tslint:disable:no-implicit-dependencies
import { Request } from 'express';
import { ConsoleAuthorizedUser } from './authorized-user.interface';
//#endregion

export interface ConsoleAuthorizedRequest extends Request {
  readonly user: ConsoleAuthorizedUser;
}
