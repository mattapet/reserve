/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from '@rsaas/util/lib/user-role.type';
import { Scope } from '@rsaas/util/lib/oauth2/scope.type';
//#endregion

export interface ConsoleAuthorizedUser {
  readonly username: string;
  readonly scope: Scope[];
}

export interface WorkspaceAuthorizedUser {
  readonly id: string;
  readonly workspace: string;
  readonly role: UserRole;
  readonly scope: Scope[];
  readonly isOwner: boolean;
}

export type AuthorizedUser = ConsoleAuthorizedUser | WorkspaceAuthorizedUser;
