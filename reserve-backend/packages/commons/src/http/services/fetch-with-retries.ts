/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Response } from 'node-fetch';

import { HTTPMethod } from '../http-method.type';
import { Fetch } from '../types/fetch.type';
import { Headers } from '../types/headers.types';
//#endregion

async function sleep(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export interface FetchWithRetriesOptions {
  method: HTTPMethod;
  headers: Headers;
  retries: number;
  retryTimeout: number;
  body?: string | Buffer;
}

/**
 * A wrapper arount the provided `fetch` function that adds retry logic.
 */
export async function fetchWithReties(
  fetch: Fetch,
  url: string,
  options: FetchWithRetriesOptions,
): Promise<Response> {
  const { retries, retryTimeout, ...fetchOptions } = options;

  async function retry() {
    await sleep(retryTimeout);
    return fetchWithReties(fetch, url, {
      ...options,
      retries: retries - 1,
      retryTimeout: retryTimeout * 2,
    });
  }

  try {
    const response = await fetch(url, { ...fetchOptions });
    if (response.status < 400 || retries === 0) {
      return response;
    }
  } catch (error) {
    if (retries === 0) {
      throw error;
    }
  }

  return retry();
}
