/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import qs from 'qs';

import { Encoding } from '../types/encoding.type';
//#endregion

export function encodeHttpContent(
  payload: undefined,
  encoding: Encoding,
): undefined;
export function encodeHttpContent(
  payload: any,
  encoding: Encoding,
): string | Buffer;
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function encodeHttpContent(payload: any, encoding: any): any {
  if (!payload || typeof payload === 'string' || payload instanceof Buffer) {
    return payload;
  } else if (encoding === Encoding.json) {
    return JSON.stringify(payload);
  } else {
    return qs.stringify(payload);
  }
}
