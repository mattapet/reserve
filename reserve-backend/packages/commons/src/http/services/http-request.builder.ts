/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Response } from 'node-fetch';

import { fetchWithReties } from './fetch-with-retries';
import { HTTPMethod } from '../http-method.type';
import { Encoding } from '../types/encoding.type';
import { Fetch } from '../types/fetch.type';
import { encodeHttpContent } from './encode-http-content';
//#endregion

export class HttpRequestBuilder {
  private readonly headers: { [key: string]: string } = {};
  private retries: number = 0;
  private retryTimeout: number = 100;

  public constructor(
    private readonly fetch: Fetch,
    private readonly method: HTTPMethod,
    private readonly url: string,
  ) {}

  private get contentType(): string | undefined {
    return this.headers['Content-Type'];
  }

  public set(key: string, value: string): this {
    this.headers[key] = value;
    return this;
  }

  public retry(retries: number): this {
    this.retries = retries;
    return this;
  }

  public withRetryTimeout(retryTimeout: number): this {
    this.retryTimeout = retryTimeout;
    return this;
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async send(payload?: any): Promise<Response> {
    if (payload && !this.headers['Content-Type']) {
      this.headers['Content-Type'] = Encoding.urlencoded;
    }

    return fetchWithReties(this.fetch, this.url, {
      retries: this.retries,
      retryTimeout: this.retryTimeout,
      body: encodeHttpContent(payload, this.contentType as Encoding),
      headers: this.headers,
      method: this.method,
    });
  }
}
