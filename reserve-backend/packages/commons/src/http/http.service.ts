/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';
import { RequestInit, Response } from 'node-fetch';

import { HttpRequestBuilder } from './services/http-request.builder';
import { HTTPMethod } from './http-method.type';
import { NODE_FETCH } from './contants';
//#endregion

@Injectable()
export class HttpService {
  public constructor(
    @Inject(NODE_FETCH)
    private readonly fetch: (
      url: string,
      options: RequestInit,
    ) => Promise<Response>,
  ) {}

  public get(url: string): HttpRequestBuilder {
    return new HttpRequestBuilder(this.fetch, HTTPMethod.get, url);
  }

  public post(url: string): HttpRequestBuilder {
    return new HttpRequestBuilder(this.fetch, HTTPMethod.post, url);
  }

  public put(url: string): HttpRequestBuilder {
    return new HttpRequestBuilder(this.fetch, HTTPMethod.put, url);
  }

  public patch(url: string): HttpRequestBuilder {
    return new HttpRequestBuilder(this.fetch, HTTPMethod.patch, url);
  }
}
