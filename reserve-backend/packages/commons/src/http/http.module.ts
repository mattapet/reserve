/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import fetch from 'node-fetch';

import { HttpService } from './http.service';
import { NODE_FETCH } from './contants';
//#endregion

@Module({
  providers: [
    HttpService,
    {
      provide: NODE_FETCH,
      useValue: fetch,
    },
  ],
  exports: [HttpService],
})
export class HttpModule {}
