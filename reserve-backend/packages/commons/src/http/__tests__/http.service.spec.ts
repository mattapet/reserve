/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { HttpService } from '../http.service';
import { HTTPMethod } from '../http-method.type';
import { Encoding } from '../types/encoding.type';
//#endregion

describe('http.service', () => {
  describe('supported http methods', () => {
    it('should get an empty request', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.resolve({ status: 200 }));
      const service = new HttpService(fetch);

      await service.get('http://example.com').send();

      expect(fetch).toHaveBeenCalledWith('http://example.com', {
        headers: {},
        method: HTTPMethod.get,
      });
    });

    it('should post an empty request', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.resolve({ status: 200 }));
      const service = new HttpService(fetch);

      await service.post('http://example.com').send();

      expect(fetch).toHaveBeenCalledWith('http://example.com', {
        headers: {},
        method: HTTPMethod.post,
      });
    });

    it('should put an empty request', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.resolve({ status: 200 }));
      const service = new HttpService(fetch);

      await service.put('http://example.com').send();

      expect(fetch).toHaveBeenCalledWith('http://example.com', {
        headers: {},
        method: HTTPMethod.put,
      });
    });

    it('should patch an empty request', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.resolve({ status: 200 }));
      const service = new HttpService(fetch);

      await service.patch('http://example.com').send();

      expect(fetch).toHaveBeenCalledWith('http://example.com', {
        headers: {},
        method: HTTPMethod.patch,
      });
    });
  });

  describe('content encoding', () => {
    it('should encode the content to query string', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.resolve({ status: 200 }));
      const service = new HttpService(fetch);

      await service
        .post('http://example.com')
        .send({ message: 'some-message' });

      expect(fetch).toHaveBeenCalledWith('http://example.com', {
        body: 'message=some-message',
        headers: {
          'Content-Type': Encoding.urlencoded,
        },
        method: HTTPMethod.post,
      });
    });

    it('should encode the content to JSON', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.resolve({ status: 200 }));
      const service = new HttpService(fetch);

      await service
        .post('http://example.com')
        .set('Content-Type', Encoding.json)
        .send({ message: 'some-message' });

      expect(fetch).toHaveBeenCalledWith('http://example.com', {
        body: '{"message":"some-message"}',
        headers: {
          'Content-Type': Encoding.json,
        },
        method: HTTPMethod.post,
      });
    });

    it('should not encode content if it is string', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.resolve({ status: 200 }));
      const service = new HttpService(fetch);

      await service
        .post('http://example.com')
        .set('Content-Type', Encoding.json)
        .send('{"message":"some-message"}');

      expect(fetch).toHaveBeenCalledWith('http://example.com', {
        body: '{"message":"some-message"}',
        headers: {
          'Content-Type': Encoding.json,
        },
        method: HTTPMethod.post,
      });
    });

    it('should not encode content if it is Buffer', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.resolve({ status: 200 }));
      const service = new HttpService(fetch);

      await service
        .post('http://example.com')
        .set('Content-Type', Encoding.json)
        .send(Buffer.from('{"message":"some-message"}'));

      expect(fetch).toHaveBeenCalledWith('http://example.com', {
        body: Buffer.from('{"message":"some-message"}'),
        headers: {
          'Content-Type': Encoding.json,
        },
        method: HTTPMethod.post,
      });
    });
  });

  describe('multiple retrying', () => {
    it('should retry once', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.reject())
        .mockImplementationOnce(() => Promise.resolve({ status: 200 }));
      const service = new HttpService(fetch);

      const response = await service
        .post('http://example.com')
        .set('Content-Type', Encoding.json)
        .retry(2)
        .send({ message: 'some-message' });

      expect(fetch).toHaveBeenCalledTimes(2);
      expect(response.status).toBe(200);
    });

    it('should retry once with 200ms timeout', async () => {
      const timeout = jest.spyOn(global, 'setTimeout');
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.reject())
        .mockImplementationOnce(() => Promise.resolve({ status: 200 }));
      const service = new HttpService(fetch);

      await service
        .post('http://example.com')
        .set('Content-Type', Encoding.json)
        .retry(2)
        .withRetryTimeout(200)
        .send({ message: 'some-message' });

      expect(timeout).toHaveBeenCalledWith(expect.anything(), 200);
    });

    it('should retry is errornous response', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.resolve({ status: 400 }))
        .mockImplementationOnce(() => Promise.resolve({ status: 200 }));
      const service = new HttpService(fetch);

      const response = await service
        .post('http://example.com')
        .set('Content-Type', Encoding.json)
        .retry(2)
        .send({ message: 'some-message' });

      expect(fetch).toHaveBeenCalledTimes(2);
      expect(response.status).toBe(200);
    });
  });
});
