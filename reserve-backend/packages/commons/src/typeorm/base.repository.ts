/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Repository, ObjectLiteral, EntityManager } from 'typeorm';

import { CLSManager } from './cls-manager.decorator';
//#endregion

export class BaseRepository<Entity extends ObjectLiteral> extends Repository<
  Entity
> {
  @CLSManager()
  public manager!: EntityManager;
}
