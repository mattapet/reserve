/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Namespace,
  getNamespace,
  createNamespace,
  destroyNamespace,
} from 'cls-hooked';
import { EntityManager, getManager } from 'typeorm';

import { ENTITY_MANAGER_PREFIX, NAMESPACE_NAME } from './constants';
//#endregion

export const initializeTransactionalContext = (): Namespace =>
  getNamespace(NAMESPACE_NAME) ?? createNamespace(NAMESPACE_NAME);

export const destroyTransactionalContext = (): void =>
  destroyNamespace(NAMESPACE_NAME);

export const getEntityManagerOrDefault = (
  connectionName: string,
  entityManager?: EntityManager,
): EntityManager => {
  const context = getNamespace(NAMESPACE_NAME);

  if (context && context.active) {
    const transactional = getEntityManagerForConnection(
      connectionName,
      context,
    );

    if (transactional) {
      return transactional;
    }
  }

  return entityManager ?? getManager(connectionName);
};

export const setEntityManagerForConnection = (
  connectionName: string,
  context: Namespace,
  entityManager: EntityManager,
): void => {
  context.set(`${ENTITY_MANAGER_PREFIX}${connectionName}`, entityManager);
};

export const getEntityManagerForConnection = (
  connectionName: string,
  context: Namespace,
): EntityManager | null =>
  context.get(`${ENTITY_MANAGER_PREFIX}${connectionName}`);

export const clearEntityManagerForConnection = (
  connectionName: string,
  context: Namespace,
): void => {
  context.set(`${ENTITY_MANAGER_PREFIX}${connectionName}`, null);
};
