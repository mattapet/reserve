/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export const NAMESPACE_NAME = '__CLS_TYPEORM_TRANSACTION_NAMESPACE__';
export const ENTITY_MANAGER_PREFIX = '__CLS_TYPEORM_ENTITY_MANAGER_PREFIX__';
