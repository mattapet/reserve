/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { getNamespace } from 'cls-hooked';
import { getConnectionManager, EntityManager, getManager } from 'typeorm';

import { NAMESPACE_NAME } from './constants';
import {
  setEntityManagerForConnection,
  clearEntityManagerForConnection,
  getEntityManagerForConnection,
} from './transactional.util';
import { IsolationLevel } from './isolation-level.enum';
import { PropagationLevel } from './propagation-level.enum';
//#endregion

/**
 * A decorator used to declare a transaction to be opened upon method call. For
 * the transactions to work properly, the methods annotated  with
 * `Transactional` decorator shall use repositories derived from the
 * `BaseRepository` class.
 *
 * Credit: https://github.com/odavid/typeorm-transactional-cls-hooked
 */
export function Transactional(options?: {
  connectionName?: string;
  isolationLevel?: IsolationLevel;
  propagationLevel?: PropagationLevel;
}): MethodDecorator {
  return (
    target: any,
    methodName: string | symbol,
    descriptor: TypedPropertyDescriptor<any>,
  ) => {
    const originalMethod = descriptor.value;

    // tslint:disable-next-line:only-arrow-functions
    descriptor.value = function (...args: any[]) {
      const connectionName = (options && options.connectionName) ?? 'default';
      const isolationLevel = options?.isolationLevel;
      const propagationLevel =
        options?.propagationLevel ?? PropagationLevel.REQUIRED;

      const getTransactionalContext = () => {
        const ctx = getNamespace(NAMESPACE_NAME);
        if (!ctx && process.env.NODE_ENV !== 'TEST') {
          throw new Error(
            'Cannot find CLS context. Please make sure to call `initializeTransactionalContext`.',
          );
        }
        return ctx;
      };

      const context = getTransactionalContext();
      if (!context) {
        return originalMethod.apply(this, [...args]);
      }

      const runOriginal = async () => originalMethod.apply(this, [...args]);

      const runWithNewTransaction = async () => {
        const transactionCallback = async (entityManager: EntityManager) => {
          setEntityManagerForConnection(connectionName, context, entityManager);
          try {
            const result = await originalMethod.apply(this, [...args]);
            clearEntityManagerForConnection(connectionName, context);
            return result;
          } catch (error) {
            clearEntityManagerForConnection(connectionName, context);
            throw error;
          }
        };

        if (isolationLevel) {
          return getManager(connectionName).transaction(
            isolationLevel,
            transactionCallback,
          );
        } else {
          return getManager(connectionName).transaction(transactionCallback);
        }
      };

      return context.runAndReturn(async () => {
        if (!getConnectionManager().has(connectionName)) {
          return runOriginal();
        }

        const currentTransaction = getEntityManagerForConnection(
          connectionName,
          context,
        );

        switch (propagationLevel) {
          case PropagationLevel.MANDATORY:
            if (!currentTransaction) {
              throw new Error(
                'Function marked to have MANDATORY transaction is missing one.',
              );
            }
            return runOriginal();
          case PropagationLevel.REQUIRES_NEW:
            return runWithNewTransaction();
          case PropagationLevel.REQUIRED:
            if (currentTransaction) {
              return runOriginal();
            }
            return runWithNewTransaction();
        }
      });
    };

    Reflect.getMetadataKeys(originalMethod).forEach((previousMetadataKey) => {
      const previousMetadata = Reflect.getMetadata(
        previousMetadataKey,
        originalMethod,
      );
      Reflect.defineMetadata(
        previousMetadataKey,
        previousMetadata,
        descriptor.value,
      );
    });

    Object.defineProperty(descriptor.value, 'name', {
      value: originalMethod.name,
      writable: false,
    });
  };
}
