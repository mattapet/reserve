/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Entity,
  PrimaryColumn,
  EntityRepository,
  getCustomRepository,
  createConnection,
  Connection,
} from 'typeorm';
import { BaseRepository } from '../base.repository';
import { Transactional } from '../transactional.decorator';
import {
  initializeTransactionalContext,
  destroyTransactionalContext,
} from '../transactional.util';
import { IsolationLevel } from '../isolation-level.enum';
import { PropagationLevel } from '../propagation-level.enum';
//#endregion

const CONNECTION_NAME = 'transactional-test';
const DATABASE_NAME = 'transactional_test.db';

@Entity()
class TestEntity {
  @PrimaryColumn()
  public id!: number;

  public constructor(id: number) {
    this.id = id;
  }
}

@EntityRepository(TestEntity)
class TestRepository extends BaseRepository<TestEntity> {}

class TestService {
  public constructor(private readonly repository: TestRepository) {}

  public async findById(id: number): Promise<TestEntity | undefined> {
    return this.repository.findOne(id);
  }

  @Transactional({
    connectionName: CONNECTION_NAME,
    isolationLevel: IsolationLevel.SERIALIZABLE,
  })
  public async getAll(): Promise<TestEntity[]> {
    return this.repository.find({ order: { id: 'ASC' } });
  }

  @Transactional({ connectionName: CONNECTION_NAME })
  public async createTest(id: number): Promise<void> {
    await this.repository.insert(new TestEntity(id));
  }

  @Transactional({ connectionName: CONNECTION_NAME })
  public async createTestBulk(...ids: number[]): Promise<void> {
    await Promise.all(
      ids.map((id) => this.repository.insert(new TestEntity(id))),
    );
  }
}

class NestedTestService {
  public constructor(private readonly testService: TestService) {}

  @Transactional({ connectionName: CONNECTION_NAME })
  public async insertOneByOne(...ids: number[]): Promise<void> {
    for (const id of ids) {
      await this.testService.createTest(id);
    }
  }

  @Transactional({
    connectionName: CONNECTION_NAME,
    propagationLevel: PropagationLevel.MANDATORY,
  })
  public async insertTwo(a: number, b: number): Promise<void> {
    await this.testService.createTestBulk(a, b);
  }

  @Transactional({ connectionName: CONNECTION_NAME })
  public async insertInGroupsOfTwo(...ids: number[]): Promise<void> {
    for (let i = 0; i < ids.length; i += 2) {
      await this.insertTwo(ids[i], ids[i + 1]);
    }
  }
}

describe('transactional.decorator', () => {
  let connection!: Connection;
  let service!: TestService;
  let nestedService!: NestedTestService;

  beforeAll(() => {
    initializeTransactionalContext();
  });

  afterAll(() => {
    destroyTransactionalContext();
  });

  beforeEach(async () => {
    connection = await createConnection({
      type: 'sqlite',
      name: CONNECTION_NAME,
      database: DATABASE_NAME,
      entities: [TestEntity],
    });
    await connection.dropDatabase();
    await connection.synchronize();
    service = new TestService(
      getCustomRepository(TestRepository, CONNECTION_NAME),
    );
    nestedService = new NestedTestService(service);
  });

  afterEach(async () => {
    await connection.close();
  });

  describe('simple transactions', () => {
    it('should persist a new test entity', async () => {
      await service.createTest(1);

      const result = await service.findById(1);
      expect(result).toEqual(new TestEntity(1));
    });

    it('should three entities in bulk', async () => {
      await service.createTestBulk(1, 2, 3, 4);

      const result = await service.getAll();
      expect(result).toEqual([
        new TestEntity(1),
        new TestEntity(2),
        new TestEntity(3),
        new TestEntity(4),
      ]);
    });

    it('should not insert any items if an error occurred while inserting in bulk', async () => {
      await expect(service.createTestBulk(1, 2, 2, 4)).rejects.toThrow();

      const result = await service.getAll();
      expect(result).toEqual([]);
    });
  });

  describe('nested transactions', () => {
    it('should insert all values sequentially', async () => {
      await nestedService.insertOneByOne(1, 2, 3, 4);

      const result = await service.getAll();
      expect(result).toEqual([
        new TestEntity(1),
        new TestEntity(2),
        new TestEntity(3),
        new TestEntity(4),
      ]);
    });

    it('should not insert any values if error occurs', async () => {
      await expect(nestedService.insertOneByOne(1, 2, 2, 4)).rejects.toThrow();

      const result = await service.getAll();
      expect(result).toEqual([]);
    });
  });

  describe('transaction isolation levels', () => {
    it('should invoke a transaction with SERIALIZABLE isolation level', async () => {
      const fn = jest.fn();
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const typeorm = require('typeorm');
      jest
        .spyOn(typeorm, 'getManager')
        .mockImplementationOnce(() => ({ transaction: fn }));

      await service.getAll();

      expect(fn).toHaveBeenCalledWith(
        IsolationLevel.SERIALIZABLE,
        expect.any(Function),
      );
    });
  });

  describe('transaction propagation levels', () => {
    it('should execute MANDATORY method if wrapped in parent transaction', async () => {
      await nestedService.insertInGroupsOfTwo(1, 2, 3, 4);

      const all = await service.getAll();
      expect(all).toEqual([
        new TestEntity(1),
        new TestEntity(2),
        new TestEntity(3),
        new TestEntity(4),
      ]);
    });

    it('should throw an error if MANDATORY trx method not called from within a transaction', async () => {
      await expect(nestedService.insertTwo(1, 2)).rejects.toThrow(
        new Error(
          'Function marked to have MANDATORY transaction is missing one.',
        ),
      );
    });
  });
});
