/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityManager } from 'typeorm';

import { getEntityManagerOrDefault } from './transactional.util';
//#endregion

export function CLSManager(): PropertyDecorator {
  let _connectionName: string | undefined;
  let _entityManager: EntityManager | undefined;

  return (target: any, propertyKey: string | symbol) => {
    Object.defineProperty(target, propertyKey, {
      set: (entityManager: EntityManager) => {
        _entityManager = entityManager;
        _connectionName = entityManager.connection.name;
      },
      get: () => {
        return getEntityManagerOrDefault(_connectionName!, _entityManager);
      },
    });
  };
}
