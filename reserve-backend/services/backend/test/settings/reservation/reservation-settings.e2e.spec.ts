/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// tslint:disable:no-implicit-dependencies
// tslint:disable:no-var-requires
require('../../util/load-dotenv');

//#region imports
import { RootModule } from '../../../src/root.module';

import request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';

import { signEmailVerification } from '../../../src/edge/verfication/services/sign-email-verification';
import { initializeEventBroker } from '../../../libs/commons/src/event-broker';

import { dropDatabase } from '../../util/drop-database';
import { generateWorkspaceAccessToken } from '../../util/generate-workspace-access-token';
import { sleep } from '../../util/sleep';
import { TypeormReservationTemplateRepository } from '../../../src/resource-reservation/application/reservation-template/repositories/typeorm/typeorm-reservation-template.repository';
import { User, Identity, UserRole } from '../../../src/user-access/domain/user';
import { ReservationTemplateDTO } from '../../../src/edge/api/settings/reservation/dto/reservation-template.dto';
import { initializeTransactionalContext } from '../../../libs/commons/src/typeorm';
import { RESERVATION_TEMPLATE_REPOSITORY } from '../../../src/resource-reservation/domain/reservation-template/constants';
//#endregion

jest.setTimeout(10000);

describe('console auth controller', () => {
  let app: INestApplication;
  let templateRepository: TypeormReservationTemplateRepository;

  beforeAll(async () => {
    initializeTransactionalContext();
    await dropDatabase();
    const moduleRef = await Test.createTestingModule({
      imports: [RootModule],
    }).compile();

    templateRepository = moduleRef.get<TypeormReservationTemplateRepository>(
      RESERVATION_TEMPLATE_REPOSITORY,
    );

    app = moduleRef.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    initializeEventBroker(app);
    await app.init();
    await sleep(1000);

    await effect_createTestWorkspace();
    await sleep(1000);
  });

  beforeEach(async () => {
    await templateRepository.clear();
  });

  afterAll(async () => {
    await app.close();
  });

  async function loginConsole(
    username: string,
    password: string,
  ): Promise<string> {
    const response = await request(app.getHttpServer())
      .post('/api/v1/console/auth/login')
      .set('Content-Type', 'application/json')
      .send(JSON.stringify({ username, password }));

    expect(response.status).toBe(200);
    return response.body.access_token;
  }

  async function effect_createTestWorkspace() {
    // tslint:disable-next-line:no-bitwise
    const expiry = ((Date.now() / 1000) >>> 0) + 300; // 5 minute period
    const signature = signEmailVerification('1', 'test@test.com', expiry);

    // Open workspace request
    await request(app.getHttpServer())
      .post('/api/v1/setup/complete/email')
      .set('Content-Type', 'application/json')
      .send(
        JSON.stringify({
          workspace_id: '1',
          email: 'test@test.com',
          expiry,
          signature,
          workspace: 'test-workspace',
          password: '12345',
        }),
      );

    const at = await loginConsole('root', 'root');
    // Conform workspace request
    const response = await request(app.getHttpServer())
      .post('/api/v1/console/workspace/request/1/confirm')
      .set('Authorization', `Bearer ${at}`);

    expect(response.status).toBe(200);
  }

  describe('reservation templates', () => {
    it('should retrieve an empty template', async () => {
      const user = new User(
        '99',
        'test',
        new Identity('99', 'test', ''),
        UserRole.admin,
      );
      const at = generateWorkspaceAccessToken('test-workspace', user);

      const response = await request(app.getHttpServer())
        .get('/api/v1/settings/reservation/template')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(200);
      expect(response.body).toEqual(new ReservationTemplateDTO());
    });

    it('should return 401 Unauthorized if user is not logged in', async () => {
      const response = await request(app.getHttpServer()).get(
        '/api/v1/settings/reservation/template',
      );

      expect(response.status).toBe(401);
    });

    it('should create a reservation template', async () => {
      const user = new User(
        '99',
        'test',
        new Identity('99', 'test', ''),
        UserRole.admin,
      );
      const at = generateWorkspaceAccessToken('test-workspace', user);

      const response = await request(app.getHttpServer())
        .put('/api/v1/settings/reservation/template')
        .set('Authorization', `Bearer ${at}`)
        .set('Content-Type', 'application/json')
        .send({ template: 'some template' });

      expect(response.status).toBe(200);
      expect(response.body).toEqual(
        new ReservationTemplateDTO('some template'),
      );
    });

    it('should override an existing reservation template', async () => {
      const user = new User(
        '99',
        'test',
        new Identity('99', 'test', ''),
        UserRole.maintainer,
      );
      const at = generateWorkspaceAccessToken('test-workspace', user);

      await request(app.getHttpServer())
        .put('/api/v1/settings/reservation/template')
        .set('Authorization', `Bearer ${at}`)
        .set('Content-Type', 'application/json')
        .send({ template: 'some template' });
      const response = await request(app.getHttpServer())
        .put('/api/v1/settings/reservation/template')
        .set('Authorization', `Bearer ${at}`)
        .set('Content-Type', 'application/json')
        .send({ template: 'some other template' });

      expect(response.status).toBe(200);
      expect(response.body).toEqual(
        new ReservationTemplateDTO('some other template'),
      );
    });

    it('should repond with 403 Forbidden if not priviledged user tries to edit template', async () => {
      const user = new User(
        '99',
        'test',
        new Identity('99', 'test', ''),
        UserRole.user,
      );
      const at = generateWorkspaceAccessToken('test-workspace', user);

      const response = await request(app.getHttpServer())
        .put('/api/v1/settings/reservation/template')
        .set('Authorization', `Bearer ${at}`)
        .set('Content-Type', 'application/json')
        .send({ template: 'some template' });

      expect(response.status).toBe(403);
    });

    it('should delete an existing reservation template', async () => {
      const user = new User(
        '99',
        'test',
        new Identity('99', 'test', ''),
        UserRole.admin,
      );
      const at = generateWorkspaceAccessToken('test-workspace', user);

      await request(app.getHttpServer())
        .put('/api/v1/settings/reservation/template')
        .set('Authorization', `Bearer ${at}`)
        .set('Content-Type', 'application/json')
        .send({ template: 'some template' });
      const response = await request(app.getHttpServer())
        .delete('/api/v1/settings/reservation/template')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(204);
    });

    it('should repond with 403 Forbidden if not priviledged user tries to delete template', async () => {
      const user = new User(
        '99',
        'test',
        new Identity('99', 'test', ''),
        UserRole.user,
      );
      const at = generateWorkspaceAccessToken('test-workspace', user);

      const response = await request(app.getHttpServer())
        .delete('/api/v1/settings/reservation/template')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(403);
    });
  });
});
