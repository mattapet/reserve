/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// tslint:disable:no-implicit-dependencies
// tslint:disable:no-var-requires
require('./util/load-dotenv');

//#region imports
import { RootModule } from '../src/root.module';

import request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';

import { signEmailVerification } from '../src/edge/verfication/services/sign-email-verification';

import { dropDatabase } from './util/drop-database';
import { sleep } from './util/sleep';
import { initializeEventBroker } from '../libs/commons/src/event-broker';
import { initializeTransactionalContext } from '../libs/commons/src/typeorm';
//#endregion

jest.mock('@rsaas/util/uuid.util', () => {
  let uuid = 0;
  return { default: () => `${uuid++}` };
});

describe('console auth controller', () => {
  let app: INestApplication;

  const resourceIds: string[] = [];
  const reservationIds: string[] = [];

  beforeAll(async () => {
    await dropDatabase();
    initializeTransactionalContext();
    const moduleRef = await Test.createTestingModule({
      imports: [RootModule],
    }).compile();

    app = moduleRef.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    initializeEventBroker(app);
    await app.init();
    await sleep(500);
  });

  afterAll(async () => {
    await app.close();
  });

  async function loginConsole(
    username: string,
    password: string,
  ): Promise<string> {
    const response = await request(app.getHttpServer())
      .post('/api/v1/console/auth/login')
      .set('Content-Type', 'application/json')
      .send(JSON.stringify({ username, password }));

    expect(response.status).toBe(200);
    return response.body.access_token;
  }

  async function loginWorkspace(
    workspace: string,
    email: string,
    password: string,
  ): Promise<string> {
    const response = await request(app.getHttpServer())
      .post('/api/v1/oauth2/token')
      .set('Content-Type', 'application/json')
      .send(
        JSON.stringify({
          workspace,
          username: email,
          password,
          grant_type: 'password',
        }),
      );

    expect(response.status).toBe(200);
    return response.body.access_token;
  }

  it('should request the workspace', async () => {
    const response = await request(app.getHttpServer())
      .post('/api/v1/setup/verify/email')
      .set('Content-Type', 'application/json')
      .send({
        email: 'test@test.com',
        redirect_uri: 'http://example.com/verify/email',
      });

    expect(response.status).toBe(202);
  });

  it('should verify the user creating a workspace request', async () => {
    // tslint:disable-next-line:no-bitwise
    const expiry = ((Date.now() / 1000) >>> 0) + 300; // 5 minute period
    const signature = signEmailVerification('1', 'test@test.com', expiry);

    const response = await request(app.getHttpServer())
      .post('/api/v1/setup/complete/email')
      .set('Content-Type', 'application/json')
      .send(
        JSON.stringify({
          workspace_id: '1',
          email: 'test@test.com',
          expiry,
          signature,
          workspace: 'test-workspace',
          password: '12345',
        }),
      );

    expect(response.status).toBe(201);
    await sleep(500);
  });

  it('should confirm workspace request', async () => {
    const at = await loginConsole('root', 'root');

    const response = await request(app.getHttpServer())
      .post('/api/v1/console/workspace/request/1/confirm')
      .set('Authorization', `Bearer ${at}`);

    await sleep(500);
    expect(response.status).toBe(200);
  });

  it('should create a resource', async () => {
    const at = await loginWorkspace('test-workspace', 'test@test.com', '12345');

    const response = await request(app.getHttpServer())
      .post('/api/v1/resource')
      .set('Authorization', `Bearer ${at}`)
      .set('Content-Type', 'application/json')
      .send(
        JSON.stringify({
          name: 'test resource',
          description: 'test description',
        }),
      );

    expect(response.status).toBe(201);
    resourceIds.push(response.body.id);
  });

  it('should create a another resource', async () => {
    const at = await loginWorkspace('test-workspace', 'test@test.com', '12345');

    const response = await request(app.getHttpServer())
      .post('/api/v1/resource')
      .set('Authorization', `Bearer ${at}`)
      .set('Content-Type', 'application/json')
      .send(
        JSON.stringify({
          name: 'another test resource',
          description: 'another test description',
        }),
      );

    expect(response.status).toBe(201);
    resourceIds.push(response.body.id);
  });

  it('should create a reservation for two resources', async () => {
    const at = await loginWorkspace('test-workspace', 'test@test.com', '12345');

    const response = await request(app.getHttpServer())
      .post('/api/v1/reservation')
      .set('Authorization', `Bearer ${at}`)
      .set('Content-Type', 'application/json')
      .send(
        JSON.stringify({
          date_start: new Date('2030-05-10'),
          date_end: new Date('2030-05-15'),
          resource_ids: resourceIds,
        }),
      );

    await sleep(500);
    expect(response.status).toBe(201);
    reservationIds.push(response.body.id);
  });

  it('should create a another reservation reservation for a single resource', async () => {
    const at = await loginWorkspace('test-workspace', 'test@test.com', '12345');

    const response = await request(app.getHttpServer())
      .post('/api/v1/reservation')
      .set('Authorization', `Bearer ${at}`)
      .set('Content-Type', 'application/json')
      .send(
        JSON.stringify({
          date_start: new Date('2030-05-10'),
          date_end: new Date('2030-05-15'),
          resource_ids: [resourceIds[0]],
        }),
      );

    await sleep(500);
    expect(response.status).toBe(201);
    reservationIds.push(response.body.id);
  });

  it('should confirm first reservation', async () => {
    const at = await loginWorkspace('test-workspace', 'test@test.com', '12345');

    const response = await request(app.getHttpServer())
      .patch(`/api/v1/reservation/${reservationIds[0]}/confirm`)
      .set('Authorization', `Bearer ${at}`);

    await sleep(500);
    expect(response.status).toBe(200);
  });

  it('should reject reject reservation', async () => {
    const at = await loginWorkspace('test-workspace', 'test@test.com', '12345');

    const response = await request(app.getHttpServer())
      .patch(`/api/v1/reservation/${reservationIds[1]}/reject`)
      .set('Authorization', `Bearer ${at}`)
      .set('Content-Type', 'application/json')
      .send(
        JSON.stringify({
          reason: 'some meaningful reson',
        }),
      );

    await sleep(500);
    expect(response.status).toBe(200);
  });

  it('should delete the workspace', async () => {
    const at = await loginWorkspace('test-workspace', 'test@test.com', '12345');

    const response = await request(app.getHttpServer())
      .delete('/api/v1/workspace')
      .set('Authorization', `Bearer ${at}`);

    await sleep(500);
    expect(response.status).toBe(200);
  });

  it('should match the event log', async () => {
    await sleep(1000);
    const events = await eventRepository.dumpEvents();
    // strip the timestamps from the events
    const eventTypes = events.map(({ timestamp, ...event }) => event);

    expect(eventTypes).toMatchSnapshot();
  });
});
