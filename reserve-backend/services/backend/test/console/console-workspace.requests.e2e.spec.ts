/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// tslint:disable:no-implicit-dependencies
// tslint:disable:no-var-requires
require('../util/load-dotenv');

//#region imports
import { RootModule } from '../../src/root.module';

import request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { EntityManager } from 'typeorm';

import { TypeormEventRepository } from '@rsaas/commons/libs/events/repositories/typeorm/typeorm-event.repository';
import { UserService } from '../../src/user-access/domain/user';
import { TypeormIdentityRepository } from '../../src/user-access/application/identity/repositories/typeorm';

import { ConsoleAccount } from '../../src/console/domain/console-account';
import { signEmailVerification } from '../../src/edge/verfication/services/sign-email-verification';
import { WorkspaceRequestDTO } from '../../src/edge/api/console/console-workspace/dto/workspace-request.dto';
import { WorkspaceRequestState } from '../../src/workspace-management';
import { WorkspaceDTO } from '../../src/edge/api/console/console-workspace/dto/workspace.dto';
import { TypeormConsoleWorkspaceProjectionRepository } from '../../src/edge/api/console/console-workspace/projections/console-workspace/repositories/typeorm/typeorm-console-workspace.projection.repository';
import { TypeormConsoleWorkspaceRequestProjectionRepository } from '../../src/edge/api/console/console-workspace/projections/console-workspace-request/repositories/typeorm/typeorm-console-workspace-request.projection.repository';

import { sleep } from '../util/sleep';
import { generateConsoleAccessToken } from '../util/generate-console-access-token';
import { initializeEventBroker } from '@rsaas/commons/libs/event-broker';
import { InboxMeta } from '@rsaas/commons/libs/event-broker/inbox/inbox-meta.entity';
import { dropDatabase } from '../util/drop-database';
import { initializeTransactionalContext } from '@rsaas/commons/libs/typeorm';
import { IDENTITY_REPOSITORY } from '../../src/user-access/domain/identity/constants';
//#endregion

jest.setTimeout(10000);
jest.mock('@rsaas/util/uuid.util', () => ({ default: () => '99' }));

describe('console auth controller', () => {
  let app: INestApplication;

  let eventRepository!: TypeormEventRepository<any>;
  let identityRepository!: TypeormIdentityRepository;
  let workspaceRepository!: TypeormConsoleWorkspaceProjectionRepository;
  let requestRepository!: TypeormConsoleWorkspaceRequestProjectionRepository;
  let userService!: UserService;
  let manager!: EntityManager;

  beforeAll(async () => {
    initializeTransactionalContext();
    await dropDatabase();
    const moduleRef = await Test.createTestingModule({
      imports: [RootModule],
    }).compile();

    eventRepository = moduleRef.get<TypeormEventRepository<any>>(
      TypeormEventRepository,
    );
    identityRepository = moduleRef.get<TypeormIdentityRepository>(
      IDENTITY_REPOSITORY,
    );
    workspaceRepository = moduleRef.get<
      TypeormConsoleWorkspaceProjectionRepository
    >(TypeormConsoleWorkspaceProjectionRepository);
    requestRepository = moduleRef.get<
      TypeormConsoleWorkspaceRequestProjectionRepository
    >(TypeormConsoleWorkspaceRequestProjectionRepository);
    manager = moduleRef.get<EntityManager>(EntityManager);
    userService = moduleRef.get<UserService>(UserService);
    app = moduleRef.createNestApplication();
    initializeEventBroker(app);
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    await sleep(1000);
  });

  afterAll(async () => {
    await app.close();
  });

  beforeEach(async () => {
    await identityRepository.clear();
    await eventRepository.clear();
    await workspaceRepository.clear();
    await requestRepository.clear();
    await manager.clear(InboxMeta);
  });

  async function effect_openRequest(id: string, name: string, ownerId: string) {
    // tslint:disable-next-line:no-bitwise
    const expiry = ((Date.now() / 1000) >>> 0) + 300;
    const signature = signEmailVerification(id, 'test@test.com', expiry);

    await request(app.getHttpServer())
      .post('/api/v1/setup/complete/email')
      .set('Content-Type', 'application/json')
      .send(
        JSON.stringify({
          workspace_id: id,
          email: 'test@test.com',
          expiry,
          signature,
          workspace: name,
          password: '12345',
        }),
      );
  }

  describe('retrieving workspace requests', () => {
    it('should return an empty array if no requests are present', async () => {
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));

      const response = await request(app.getHttpServer())
        .get('/api/v1/console/workspace/request')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(200);
      expect(response.body).toEqual([]);
    });

    it('should 401 Unauthorized if access token is missing', async () => {
      const response = await request(app.getHttpServer()).get(
        '/api/v1/console/workspace/request',
      );

      expect(response.status).toBe(401);
    });
  });

  describe('opening workspace requests', () => {
    it('should open a workspace request', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 1000);
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));

      // tslint:disable-next-line:no-bitwise
      const expiry = ((Date.now() / 1000) >>> 0) + 300;
      const signature = signEmailVerification('test', 'test@test.com', expiry);

      await request(app.getHttpServer())
        .post('/api/v1/setup/complete/email')
        .set('Content-Type', 'application/json')
        .send(
          JSON.stringify({
            workspace_id: 'test',
            email: 'test@test.com',
            expiry,
            signature,
            workspace: 'test-name',
            password: '12345',
          }),
        );
      await sleep(1000);

      const response = await request(app.getHttpServer())
        .get('/api/v1/console/workspace/request')
        .set('Authorization', `Bearer ${at}`);
      expect(response.status).toBe(200);
      expect(response.body).toEqual([
        new WorkspaceRequestDTO(
          'test',
          'test-name',
          '99',
          'test@test.com',
          WorkspaceRequestState.opened,
          new Date(1000).toISOString(),
        ),
      ]);
    });
  });

  describe('confirming workspace requests', () => {
    it('should confirm opened workspace request', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 1000);
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));
      await effect_openRequest('test', 'test-name', '99');

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/workspace/request/test/confirm')
        .set('Authorization', `Bearer ${at}`);
      await sleep(1000);

      expect(response.status).toBe(200);
      expect(response.body).toEqual(
        new WorkspaceRequestDTO(
          'test',
          'test-name',
          '99',
          'test@test.com',
          WorkspaceRequestState.confirmed,
          new Date(1000).toISOString(),
          new Date(1000).toISOString(),
        ),
      );
    });

    it('should asynchronously create a workspace', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 1000);
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));
      await effect_openRequest('test', 'test-name', '99');

      await request(app.getHttpServer())
        .post('/api/v1/console/workspace/request/test/confirm')
        .set('Authorization', `Bearer ${at}`);
      await sleep(1000);

      const response = await request(app.getHttpServer())
        .get('/api/v1/console/workspace')
        .set('Authorization', `Bearer ${at}`);
      expect(response.status).toBe(200);
      expect(response.body).toEqual([
        new WorkspaceDTO('test', 'test-name', '99', 'test@test.com'),
      ]);
    });

    it('should return 400 BadRequest if confirming already closed request', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 1000);
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));
      await effect_openRequest('test', 'test-name', '99');

      await request(app.getHttpServer())
        .post('/api/v1/console/workspace/request/test/confirm')
        .set('Authorization', `Bearer ${at}`);
      await sleep(1000);
      const response = await request(app.getHttpServer())
        .post('/api/v1/console/workspace/request/test/confirm')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(400);
    });

    it('should return 404 NotFound if requests does not exists', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 1000);
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/workspace/request/test/confirm')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(404);
    });
  });

  describe('declining workspace requests', () => {
    it('should decline opened workspace request', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 1000);
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));
      await effect_openRequest('test', 'test-name', '99');

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/workspace/request/test/decline')
        .set('Authorization', `Bearer ${at}`);
      await sleep(1000);

      expect(response.status).toBe(200);
      expect(response.body).toEqual(
        new WorkspaceRequestDTO(
          'test',
          'test-name',
          '99',
          null,
          WorkspaceRequestState.declined,
          new Date(1000).toISOString(),
          new Date(1000).toISOString(),
        ),
      );
    });

    it('should asynchronously delete requester information', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 1000);
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));
      await effect_openRequest('test', 'test-name', '99');

      await request(app.getHttpServer())
        .post('/api/v1/console/workspace/request/test/decline')
        .set('Authorization', `Bearer ${at}`);
      await sleep(1000);

      const user = await userService.getById('test', '99');
      expect(user.getIdentity().isAnonymized()).toBe(true);
    });

    it('should return 400 BadRequest if declining already closed request', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 1000);
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));
      await effect_openRequest('test', 'test-name', '99');

      await request(app.getHttpServer())
        .post('/api/v1/console/workspace/request/test/decline')
        .set('Authorization', `Bearer ${at}`);
      await sleep(1000);
      const response = await request(app.getHttpServer())
        .post('/api/v1/console/workspace/request/test/decline')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(400);
    });

    it('should return 404 NotFound if requests does not exists', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 1000);
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/workspace/request/test/cancel')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(404);
    });
  });
});
