/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// tslint:disable:no-implicit-dependencies
// tslint:disable:no-var-requires
require('../util/load-dotenv');

//#region imports
import { RootModule } from '../../src/root.module';

import request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { EntityManager } from 'typeorm';

import { TypeormEventRepository } from '../../libs/commons/src/events/repositories/typeorm/typeorm-event.repository';
import {
  WorkspaceService,
  Workspace,
  WorkspaceName,
  WorkspaceOwner,
} from '../../src/workspace-management';
import { UserService, User, Identity } from '../../src/user-access/domain/user';
import { WorkspaceDTO } from '../../src/edge/api/console/console-workspace/dto/workspace.dto';
import { UserDTO } from '../../src/edge/api/console/console-workspace/dto/user.dto';
import { TypeormIdentityRepository } from '../../src/user-access/application/identity/repositories/typeorm';
import {
  ConsoleAccount,
  ConsoleAccountService,
} from '../../src/console/domain/console-account';
import { TypeormConsoleWorkspaceProjectionRepository } from '../../src/edge/api/console/console-workspace/projections/console-workspace/repositories/typeorm/typeorm-console-workspace.projection.repository';

import { generateConsoleAccessToken } from '../util/generate-console-access-token';
import { sleep } from '../util/sleep';
import { initializeEventBroker } from '../../libs/commons/src/event-broker';
import { InboxMeta } from '../../libs/commons/src/event-broker/inbox/inbox-meta.entity';
import { OutboxEvent } from '../../libs/commons/src/outbox';
import { dropDatabase } from '../util/drop-database';
import { initializeTransactionalContext } from '../../libs/commons/src/typeorm';
import { IDENTITY_REPOSITORY } from '../../src/user-access/domain/identity/constants';
//#endregion

jest.setTimeout(10000);

describe('console auth controller', () => {
  let app: INestApplication;

  let eventRepository!: TypeormEventRepository<any>;
  let identityRepository!: TypeormIdentityRepository;
  let projectionRepository!: TypeormConsoleWorkspaceProjectionRepository;
  let workspaceService!: WorkspaceService;
  let userService!: UserService;
  let manager!: EntityManager;
  let accountService!: ConsoleAccountService;

  beforeAll(async () => {
    initializeTransactionalContext();
    await dropDatabase();
    const moduleRef = await Test.createTestingModule({
      imports: [RootModule],
    }).compile();

    eventRepository = moduleRef.get<TypeormEventRepository<any>>(
      TypeormEventRepository,
    );
    identityRepository = moduleRef.get<TypeormIdentityRepository>(
      IDENTITY_REPOSITORY,
    );
    projectionRepository = moduleRef.get<
      TypeormConsoleWorkspaceProjectionRepository
    >(TypeormConsoleWorkspaceProjectionRepository);
    workspaceService = moduleRef.get<WorkspaceService>(WorkspaceService);
    userService = moduleRef.get<UserService>(UserService);
    accountService = moduleRef.get<ConsoleAccountService>(
      ConsoleAccountService,
    );
    manager = moduleRef.get<EntityManager>(EntityManager);
    app = moduleRef.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    initializeEventBroker(app);
    await app.init();
    await sleep(1000);

    await accountService.create(new ConsoleAccount('root', 'root'));
  });

  afterAll(async () => {
    await app.close();
  });

  beforeEach(async () => {
    await identityRepository.clear();
    await eventRepository.clear();
    await projectionRepository.clear();
    await manager.clear(InboxMeta);
    await manager.clear(OutboxEvent);
  });

  async function effect_createOwner(workspaceId: string): Promise<User> {
    const identity = new Identity('99', workspaceId, 'test@test.com');
    const owner = new User();
    await userService.createOwner(owner, identity);
    return owner;
  }

  async function effect_createUser(identity: Identity): Promise<User> {
    const user = new User();
    await userService.create(user, identity);
    return user;
  }

  async function effect_createWorkspace(workspace: Workspace) {
    const owner = await effect_createOwner(workspace.getId());
    await workspaceService.create(
      new Workspace(),
      workspace.getId(),
      workspace.getName(),
      WorkspaceOwner.from(owner),
    );
  }

  async function effect_createWorkspaceWithOwner(
    workspace: Workspace,
    owner: User,
  ) {
    await workspaceService.create(
      new Workspace(),
      workspace.getId(),
      workspace.getName(),
      WorkspaceOwner.from(owner),
    );
  }

  describe('retrieving workspaces', () => {
    it('should respond with an empty array if no workspaces are present', async () => {
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));

      const response = await request(app.getHttpServer())
        .get('/api/v1/console/workspace')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(200);
      expect(response.body).toEqual([]);
    });

    it('should retrieve all workspaces', async () => {
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));
      await effect_createWorkspace(
        new Workspace('test1', new WorkspaceName('test1')),
      );
      await effect_createWorkspace(
        new Workspace('test2', new WorkspaceName('test2')),
      );
      await effect_createWorkspace(
        new Workspace('test3', new WorkspaceName('test3')),
      );
      await sleep(1000);

      const response = await request(app.getHttpServer())
        .get('/api/v1/console/workspace')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(200);
      expect(response.body).toEqual(
        expect.arrayContaining([
          new WorkspaceDTO('test1', 'test1', '99', 'test@test.com'),
          new WorkspaceDTO('test2', 'test2', '99', 'test@test.com'),
          new WorkspaceDTO('test3', 'test3', '99', 'test@test.com'),
        ]),
      );
    });

    it('should respond with 401 Unauthorized if no access token is passed', async () => {
      const response = await request(app.getHttpServer()).get(
        '/api/v1/console/workspace',
      );

      expect(response.status).toBe(401);
    });
  });

  describe('retrieving workspace requests', () => {
    it('should return an empty array if no requests are present', async () => {
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));

      const response = await request(app.getHttpServer())
        .get('/api/v1/console/workspace/request')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(200);
      expect(response.body).toEqual([]);
    });

    it('should 401 Unauthorized if access token is missing', async () => {
      const response = await request(app.getHttpServer()).get(
        '/api/v1/console/workspace/request',
      );

      expect(response.status).toBe(401);
    });

    it('should repond with workspace matching id', async () => {
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));
      await effect_createWorkspace(
        new Workspace('test', new WorkspaceName('test')),
      );
      await sleep(1000);

      const response = await request(app.getHttpServer())
        .get('/api/v1/console/workspace/test/details')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(200);
      expect(response.body).toEqual(
        new WorkspaceDTO('test', 'test', '99', 'test@test.com'),
      );
    });
  });

  describe('retrieving workspace users', () => {
    it('should respond with workspace owner', async () => {
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));
      const owner = await effect_createOwner('test');
      await effect_createWorkspaceWithOwner(
        new Workspace('test', new WorkspaceName('test')),
        owner,
      );
      await sleep(1000);

      const response = await request(app.getHttpServer())
        .get('/api/v1/console/workspace/test/user')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(200);
      expect(response.body).toEqual([
        new UserDTO('99', 'test', 'test@test.com', '', '', true),
      ]);
    });

    it('should respond with all workspace users', async () => {
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));
      const owner = await effect_createOwner('test');
      await effect_createUser(new Identity('88', 'test', 'test1@test.com'));
      await effect_createWorkspaceWithOwner(
        new Workspace('test', new WorkspaceName('test')),
        owner,
      );
      await sleep(1000);

      const response = await request(app.getHttpServer())
        .get('/api/v1/console/workspace/test/user')
        .set('Authorization', `Bearer ${at}`);

      expect(response.status).toBe(200);
      expect(response.body).toEqual(
        expect.arrayContaining([
          new UserDTO('99', 'test', 'test@test.com', '', '', true),
          new UserDTO('88', 'test', 'test1@test.com', '', '', false),
        ]),
      );
    });
  });

  describe('transferring workspace ownership', () => {
    it('should transfer user ownership', async () => {
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));
      const owner = await effect_createOwner('test');
      await effect_createUser(new Identity('88', 'test', 'test1@test.com'));
      await effect_createWorkspaceWithOwner(
        new Workspace('test', new WorkspaceName('test')),
        owner,
      );
      await sleep(1000);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/workspace/test/transfer')
        .set('Authorization', `Bearer ${at}`)
        .set('Content-Type', 'application/json')
        .send({ to: '88' });
      await sleep(1000);

      expect(response.status).toBe(200);
      expect(response.body).toEqual(
        new WorkspaceDTO('test', 'test', '88', 'test1@test.com'),
      );
    });

    it('should respond with 400 BadRequest if no `to` is specified', async () => {
      const at = generateConsoleAccessToken(new ConsoleAccount('root', 'root'));

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/workspace/test/transfer')
        .set('Authorization', `Bearer ${at}`)
        .set('Content-Type', 'application/json')
        .send({});

      expect(response.status).toBe(400);
    });
  });
});
