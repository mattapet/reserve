/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// tslint:disable:no-implicit-dependencies
// tslint:disable:no-var-requires
require('../util/load-dotenv');

//#region imports
import { RootModule } from '../../src/root.module';

import request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';

import { TypeormConsoleAccountRepository } from '../../src/console/application/console-account/repositories/typeorm/typeorm-console-account.repository';
import { ConsoleAccount } from '../../src/console/domain/console-account';

import { generateConsoleAccessToken } from '../util/generate-console-access-token';
import { ConsoleAccountDTO } from '../../src/edge/api/console/console-account/dto/console-account.dto';
import { initializeEventBroker } from '../../libs/commons/src/event-broker';
import { dropDatabase } from '../util/drop-database';
import { initializeTransactionalContext } from '../../libs/commons/src/typeorm';
import { CONSOLE_ACCOUNT_REPOSITORY } from '../../src/console/domain/console-account/constants';
//#endregion

jest.setTimeout(10000);

describe('console auth controller', () => {
  let app: INestApplication;

  let accountRepository!: TypeormConsoleAccountRepository;

  beforeAll(async () => {
    initializeTransactionalContext();
    await dropDatabase();
    const moduleRef = await Test.createTestingModule({
      imports: [RootModule],
    }).compile();

    accountRepository = moduleRef.get<TypeormConsoleAccountRepository>(
      CONSOLE_ACCOUNT_REPOSITORY,
    );
    app = moduleRef.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    initializeEventBroker(app);
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  beforeEach(async () => {
    await accountRepository.clear();
  });

  function make_rootAccount() {
    return new ConsoleAccount('root', 'root');
  }

  async function effect_registerAccount(account: ConsoleAccount) {
    const accessToken = generateConsoleAccessToken(account);

    await request(app.getHttpServer())
      .post('/api/v1/console/auth/register')
      .set('Authorization', `Bearer ${accessToken}`)
      .send({
        username: account.getUsername(),
        password: account.getPassword(),
      });
  }

  describe('account retrieval', () => {
    it('should return an empty array if no accounts are created', async () => {
      const account = make_rootAccount();

      const accessToken = generateConsoleAccessToken(account);
      const response = await request(app.getHttpServer())
        .get('/api/v1/console/account')
        .set('Authorization', `Bearer ${accessToken}`);

      expect(response.status).toBe(200);
      expect(response.body).toEqual([]);
    });

    it('should single registered user', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);

      const accessToken = generateConsoleAccessToken(account);
      const response = await request(app.getHttpServer())
        .get('/api/v1/console/account')
        .set('Authorization', `Bearer ${accessToken}`);

      expect(response.status).toBe(200);
      expect(response.body).toEqual([new ConsoleAccountDTO('root')]);
    });

    it('should respond with 401 Unauthorized if request if not authorized', async () => {
      const response = await request(app.getHttpServer()).get(
        '/api/v1/console/account',
      );

      expect(response.status).toBe(401);
    });
  });

  describe('account removal', () => {
    it('should respond with 204 NoContent', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);
      await effect_registerAccount(new ConsoleAccount('admin', 'admin'));

      const accessToken = generateConsoleAccessToken(account);
      const response = await request(app.getHttpServer())
        .delete('/api/v1/console/account/admin')
        .set('Authorization', `Bearer ${accessToken}`);

      expect(response.status).toBe(204);
    });

    it('should remove account', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);
      await effect_registerAccount(new ConsoleAccount('admin', 'admin'));

      const accessToken = generateConsoleAccessToken(account);
      await request(app.getHttpServer())
        .delete('/api/v1/console/account/admin')
        .set('Authorization', `Bearer ${accessToken}`);

      const response = await request(app.getHttpServer())
        .get('/api/v1/console/account')
        .set('Authorization', `Bearer ${accessToken}`);
      expect(response.body).toEqual([new ConsoleAccountDTO('root')]);
    });

    it('should respond with 404 NotFound if account to be removed does not exist', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);

      const accessToken = generateConsoleAccessToken(account);
      const response = await request(app.getHttpServer())
        .delete('/api/v1/console/account/admin')
        .set('Authorization', `Bearer ${accessToken}`);

      expect(response.status).toBe(404);
    });

    it('should respond with 400 badrequest if trying to remove youtself', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);

      const accessToken = generateConsoleAccessToken(account);
      const response = await request(app.getHttpServer())
        .delete('/api/v1/console/account/root')
        .set('Authorization', `Bearer ${accessToken}`);

      expect(response.status).toBe(403);
    });
  });
});
