/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// tslint:disable:no-implicit-dependencies
// tslint:disable:no-var-requires
require('../util/load-dotenv');

//#region imports
import { RootModule } from '../../src/root.module';

import request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';

import { TypeormConsoleTokenRepository } from '../../src/console/application/console-token/repositories/typeorm/typeorm-console-token.repository';
import { TypeormConsoleAccountRepository } from '../../src/console/application/console-account/repositories/typeorm/typeorm-console-account.repository';
import { ConsoleAccount } from '../../src/console/domain/console-account';

import { generateConsoleAccessToken } from '../util/generate-console-access-token';
import { initializeEventBroker } from '../../libs/commons/src/event-broker';
import { dropDatabase } from '../util/drop-database';
import { initializeTransactionalContext } from '../../libs/commons/src/typeorm';
import { CONSOLE_TOKEN_REPOSITORY } from '../../src/console/domain/console-token/constants';
import { CONSOLE_ACCOUNT_REPOSITORY } from '../../src/console/domain/console-account/constants';
//#endregion

jest.setTimeout(10000);

describe('console auth controller', () => {
  let app: INestApplication;

  let tokenRepository!: TypeormConsoleTokenRepository;
  let accountRepository!: TypeormConsoleAccountRepository;

  beforeAll(async () => {
    initializeTransactionalContext();
    await dropDatabase();
    const moduleRef = await Test.createTestingModule({
      imports: [RootModule],
    }).compile();

    tokenRepository = moduleRef.get<TypeormConsoleTokenRepository>(
      CONSOLE_TOKEN_REPOSITORY,
    );
    accountRepository = moduleRef.get<TypeormConsoleAccountRepository>(
      CONSOLE_ACCOUNT_REPOSITORY,
    );
    app = moduleRef.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    initializeEventBroker(app);
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  beforeEach(async () => {
    await tokenRepository.clear();
    await accountRepository.clear();
  });

  function make_rootAccount() {
    return new ConsoleAccount('root', 'root');
  }

  async function effect_registerAccount(account: ConsoleAccount) {
    const accessToken = generateConsoleAccessToken(account);

    await request(app.getHttpServer())
      .post('/api/v1/console/auth/register')
      .set('Authorization', `Bearer ${accessToken}`)
      .send({
        username: account.getUsername(),
        password: account.getPassword(),
      });
  }

  async function effect_loginAccount(account: ConsoleAccount) {
    const response = await request(app.getHttpServer())
      .post('/api/v1/console/auth/login')
      .send({
        username: account.getUsername(),
        password: account.getPassword(),
      });

    return [response.body.access_token, response.body.refresh_token];
  }

  describe('user registration', () => {
    it('should return 401 UnauthorizedRequest on invalid access token', async () => {
      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/register')
        .send({ username: 'admin', password: 'admin' });

      expect(response.status).toBe(401);
    });

    it('should return 201 Created on successful registration', async () => {
      const account = make_rootAccount();
      const accessToken = generateConsoleAccessToken(account);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/register')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ username: 'admin', password: 'admin' });

      expect(response.status).toBe(201);
      expect(response.body).toEqual({ username: 'admin' });
    });

    it('should return 400 BadRequest if password is missing', async () => {
      const account = make_rootAccount();
      const accessToken = generateConsoleAccessToken(account);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/register')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ username: 'admin' });

      expect(response.status).toBe(400);
    });

    it('should return 400 BadRequest if username is an empty string', async () => {
      const account = make_rootAccount();
      const accessToken = generateConsoleAccessToken(account);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/register')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ username: '', password: 'test' });

      expect(response.status).toBe(400);
    });

    it('should return 400 BadRequest if username is already registered', async () => {
      const account = make_rootAccount();
      const accessToken = generateConsoleAccessToken(account);
      await effect_registerAccount(new ConsoleAccount('test', 'test'));

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/register')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({ username: 'test', password: 'test' });

      expect(response.status).toBe(400);
    });
  });

  describe('user login', () => {
    it('should respond with token pair', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/login')
        .send({
          username: 'root',
          password: 'root',
        });

      expect(response.status).toBe(200);
      expect(response.body).toEqual(
        expect.objectContaining({
          expires_in: 300,
          scope: [],
          token_type: 'Bearer',
          access_token: expect.stringMatching(/\w+/),
          refresh_token: expect.stringMatching(/\w+/),
        }),
      );
    });

    it('should respond 400 BadRequest if password does not match', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/login')
        .send({
          username: 'root',
          password: 'some-other-password',
        });

      expect(response.status).toBe(400);
    });

    it('should respond 400 BadRequest if user is not registered', async () => {
      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/login')
        .send({
          username: 'root',
          password: 'some-other-password',
        });

      expect(response.status).toBe(400);
    });
  });

  describe('refreshing access tokens', () => {
    it('should refresh access token', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);
      const [, rt] = await effect_loginAccount(account);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/refresh')
        .send({ refresh_token: rt });

      expect(response.status).toBe(200);
      expect(response.body).toEqual(
        expect.objectContaining({
          expires_in: 300,
          scope: [],
          token_type: 'Bearer',
          access_token: expect.stringMatching(/\w+/),
          refresh_token: rt,
        }),
      );
    });

    it('should respond with 400 BadRequest malformed token', async () => {
      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/refresh')
        .send({ refresh_token: 'some-random-token' });

      expect(response.status).toBe(400);
    });

    it('should respond with 400 BadRequest missing token', async () => {
      const account = make_rootAccount();
      const at = generateConsoleAccessToken(account);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/refresh')
        .send({ refresh_token: at });

      expect(response.status).toBe(400);
    });

    it('should respond with 400 BadRequest if token is expired', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);
      const [, rt] = await effect_loginAccount(account);
      const farFarFuture = Date.now() + 200_000_000;
      jest.spyOn(Date, 'now').mockImplementation(() => farFarFuture);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/refresh')
        .send({ refresh_token: rt });

      expect(response.status).toBe(400);
    });
  });

  describe('token revocation', () => {
    it('should respond with 204 NoContent', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);
      const [, rt] = await effect_loginAccount(account);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/revoke')
        .send({ refresh_token: rt });

      expect(response.status).toBe(204);
    });

    it('should respond with 400 BadRequest when trying to refresh with revoked token', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);
      const [, rt] = await effect_loginAccount(account);

      await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/revoke')
        .send({ refresh_token: rt });
      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/refresh')
        .send({ refresh_token: rt });

      expect(response.status).toBe(400);
    });

    it('should respond with 400 BadRequest when trying to revoke missing token', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);
      const [at] = await effect_loginAccount(account);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/revoke')
        .send({ refresh_token: at });

      expect(response.status).toBe(400);
    });

    it('should respond with 400 BadRequest when trying to revoke malformed', async () => {
      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/revoke')
        .send({ refresh_token: 'some-random-token' });

      expect(response.status).toBe(400);
    });

    it('should respond with 400 BadRequest when trying to revoke expired token', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);
      const [, rt] = await effect_loginAccount(account);
      const farFarFuture = Date.now() + 200_000_000;
      jest.spyOn(Date, 'now').mockImplementation(() => farFarFuture);

      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/revoke')
        .send({ refresh_token: rt });

      expect(response.status).toBe(400);
    });

    it('should respond with 400 BadRequest when trying to revoke already revoked token', async () => {
      const account = make_rootAccount();
      await effect_registerAccount(account);
      const [, rt] = await effect_loginAccount(account);

      await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/revoke')
        .send({ refresh_token: rt });
      const response = await request(app.getHttpServer())
        .post('/api/v1/console/auth/token/revoke')
        .send({ refresh_token: rt });

      expect(response.status).toBe(400);
    });
  });
});
