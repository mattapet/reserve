/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
// tslint:disable-next-line:no-implicit-dependencies
import jwt from 'jsonwebtoken';
import { generateToken } from '../../libs/util/src/generate-token';
import { TokenKind } from '../../libs/util/src/interface/jwt-payload.interface';
import { User } from '../../src/user-access/domain/user';
//#endregion

const SERVICE = process.env.SERVICE_NAME;
const JWT_ALG = process.env.JWT_ALG;
const JWT_SECRET = process.env.JWT_SECRET;

export function generateWorkspaceAccessToken(
  workspace: string,
  user: User,
): string {
  const token = generateToken();
  return jwt.sign(
    {
      jit: token,
      sub: user.getEmail(),
      // tslint:disable-next-line:no-bitwise
      exp: ((Date.now() / 1000) >>> 0) + 300,
      // tslint:disable-next-line:no-bitwise
      iat: (Date.now() / 1000) >>> 0,
      scope: [],
      kind: TokenKind.accessToken,
      user: {
        id: user.getId(),
        workspace,
        role: user.getRole(),
      },
    },
    JWT_SECRET!,
    {
      algorithm: JWT_ALG,
      issuer: SERVICE,
    },
  );
}
