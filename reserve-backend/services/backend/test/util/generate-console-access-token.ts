/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
// tslint:disable-next-line:no-implicit-dependencies
import jwt from 'jsonwebtoken';
import { ConsoleAccount } from '../../src/console/domain/console-account';
import { generateToken } from '../../libs/util/src/generate-token';
import { TokenKind } from '../../libs/util/src/interface/jwt-payload.interface';
//#endregion

const SERVICE = process.env.SERVICE_NAME;
const JWT_ALG = process.env.JWT_ALG;
const JWT_SECRET = process.env.JWT_SECRET;

export function generateConsoleAccessToken(account: ConsoleAccount): string {
  const token = generateToken();
  return jwt.sign(
    {
      jit: token,
      sub: account.getUsername(),
      // tslint:disable-next-line:no-bitwise
      exp: ((Date.now() / 1000) >>> 0) + 300,
      // tslint:disable-next-line:no-bitwise
      iat: (Date.now() / 1000) >>> 0,
      scope: [],
      kind: TokenKind.accessToken,
      user: account.getUsername(),
    },
    JWT_SECRET!,
    {
      algorithm: JWT_ALG,
      issuer: SERVICE,
    },
  );
}
