/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonsModule } from '@rsaas/commons';

import { ConsoleAccessToken } from '../../domain/console-token/console-access-token.entity';
import { ConsoleRefreshToken } from '../../domain/console-token/console-refresh-token.entity';
import { ConsoleTokenService } from '../../domain/console-token/console-token.service';
import { CONSOLE_TOKEN_REPOSITORY } from '../../domain/console-token/constants';

import { TypeormConsoleTokenRepository } from '../../application/console-token/repositories/typeorm/typeorm-console-token.repository';
import { ConsoleAccessTokenRepository } from '../../application/console-token/repositories/typeorm/console-refresh-token.repository';
import { ConsoleRefreshTokenRepository } from '../../application/console-token/repositories/typeorm/console-access-token.repository';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ConsoleAccessToken,
      ConsoleRefreshToken,
      ConsoleAccessTokenRepository,
      ConsoleRefreshTokenRepository,
    ]),
    CommonsModule,
  ],
  providers: [
    ConsoleTokenService,
    {
      provide: CONSOLE_TOKEN_REPOSITORY,
      useClass: TypeormConsoleTokenRepository,
    },
  ],
  exports: [ConsoleTokenService],
})
export class ConsoleTokenModule {}
