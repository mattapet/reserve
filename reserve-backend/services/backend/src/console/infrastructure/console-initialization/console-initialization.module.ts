/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import {
  INITIAL_ROOT_PASSWORD,
  INITIAL_ROOT_USERNAME,
} from '../../domain/console-initialization/constants';
import { ConsoleInitializationService } from '../../domain/console-initialization/console-initialization.service';

import { InitializeRootUserOnModuleInit } from '../../application/console-initialization/initialize-root-user.on-module-init';

import { ConsoleAuthModule } from '../console-auth';
//#endregion

const CONSOLE_ROOT_USERNAME = process.env.CONSOLE_ROOT_USERNAME;
const CONSOLE_ROOT_PASSWORD = process.env.CONSOLE_ROOT_PASSWORD;

@Module({
  imports: [ConsoleAuthModule],
  providers: [
    {
      provide: INITIAL_ROOT_USERNAME,
      useValue: CONSOLE_ROOT_USERNAME,
    },
    {
      provide: INITIAL_ROOT_PASSWORD,
      useValue: CONSOLE_ROOT_PASSWORD,
    },
    ConsoleInitializationService,
    InitializeRootUserOnModuleInit,
  ],
})
export class ConsoleInitializationModule {}
