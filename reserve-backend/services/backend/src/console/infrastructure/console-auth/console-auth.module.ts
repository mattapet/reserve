/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { ConsoleAuthService } from '../../domain/console-auth/console-auth.service';

import { ConsoleAccountModule } from '../console-account';
//#endregion

@Module({
  imports: [ConsoleAccountModule],
  providers: [ConsoleAuthService],
  exports: [ConsoleAuthService],
})
export class ConsoleAuthModule {}
