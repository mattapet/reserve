/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ConsoleAccount } from '../../domain/console-account/console-account.entity';
import { ConsoleAccountService } from '../../domain/console-account/console-account.service';
import { CONSOLE_ACCOUNT_REPOSITORY } from '../../domain/console-account/constants';

import { TypeormConsoleAccountRepository } from '../../application/console-account/repositories/typeorm/typeorm-console-account.repository';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([ConsoleAccount, TypeormConsoleAccountRepository]),
  ],
  providers: [
    ConsoleAccountService,
    {
      provide: CONSOLE_ACCOUNT_REPOSITORY,
      useFactory: (repository) => repository,
      inject: [TypeormConsoleAccountRepository],
    },
  ],
  exports: [ConsoleAccountService],
})
export class ConsoleAccountModule {}
