/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { ConsoleAccountModule } from './console-account';
import { ConsoleAuthModule } from './console-auth';
import { ConsoleTokenModule } from './console-token';
import { ConsoleInitializationModule } from './console-initialization';
//#endregion

@Module({
  imports: [
    ConsoleAccountModule,
    ConsoleAuthModule,
    ConsoleInitializationModule,
    ConsoleTokenModule,
  ],
  exports: [ConsoleAccountModule, ConsoleAuthModule, ConsoleTokenModule],
})
export class ConsoleModule {}
