/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleAuthService } from '../../domain/console-auth/console-auth.service';
import {
  ConsoleAccount,
  ConsoleAccountService,
} from '../../domain/console-account';
import {
  InvalidUsernameOrPasswordError,
  UsernameAlreadyExistsError,
} from '../../domain/console-auth/console-auth.errors';
import { InMemoryConsoleAccountRepository } from '../../application/console-account/repositories/in-memory/in-memory-console-account.repository';
//#endregion

jest.mock('bcryptjs', () => ({
  compare: (plain: string, hashed: string) =>
    Promise.resolve(hashed === `${plain}+hash`),
  hash: (password: string) => Promise.resolve(`${password}+hash`),
}));

describe('console-auth.service', () => {
  let service!: ConsoleAuthService;
  let accountService!: ConsoleAccountService;

  beforeEach(() => {
    accountService = new ConsoleAccountService(
      new InMemoryConsoleAccountRepository(),
    );
    service = new ConsoleAuthService(accountService);
  });

  describe('authenticating in users', () => {
    it('should return authorized account if usernmae and password match', async () => {
      const account = new ConsoleAccount('root', 'root');
      await service.register('root', 'root');

      const loggedInAccount = await service.authenticate('root', 'root');

      expect(loggedInAccount).toEqual(account);
    });

    it('should throw InvalidUsernameOrPasswordError if account for specifiec username is not found', async () => {
      const fn = service.authenticate('root', 'root');

      await expect(fn).rejects.toThrow(InvalidUsernameOrPasswordError);
    });

    it('should throw InvalidUsernameOrPasswordError if password does not match', async () => {
      await service.register('root', 'password');
      const fn = service.authenticate('root', 'root');

      await expect(fn).rejects.toThrow(InvalidUsernameOrPasswordError);
    });
  });

  describe('registering accoutns', () => {
    it('should create a new account with hashed password', async () => {
      await service.register('root', 'root');

      const account = await accountService.findByUsername('root');
      expect(account).toEqual(new ConsoleAccount('root', 'root+hash'));
    });

    it('should throw UsernameAlreadyExistsError if account with given username already exists', async () => {
      await service.register('root', 'root');

      const fn = service.register('root', 'root');

      expect(fn).rejects.toThrow(UsernameAlreadyExistsError);
    });
  });
});
