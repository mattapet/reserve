/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region improts
import { ConsoleInitializationService } from '../../domain/console-initialization/console-initialization.service';

import {
  ConsoleAuthService,
  InvalidUsernameOrPasswordError,
} from '../../domain/console-auth';
import { ConsoleAccountService } from '../../domain/console-account';
import { InMemoryConsoleAccountRepository } from '../../application/console-account/repositories/in-memory/in-memory-console-account.repository';
//#endregion

describe('console initialization service', () => {
  let authService!: ConsoleAuthService;

  beforeEach(() => {
    authService = new ConsoleAuthService(
      new ConsoleAccountService(new InMemoryConsoleAccountRepository()),
    );
  });

  function make_initializationServiceWithCredentials(
    username: string,
    password: string,
  ) {
    return new ConsoleInitializationService(authService, username, password);
  }

  describe('initial account initialization', () => {
    it('should be able to authenticate with initial credentials', async () => {
      const service = make_initializationServiceWithCredentials('root', 'root');

      await service.initializeRootUser();

      const account = await authService.authenticate('root', 'root');
      expect(account.getUsername()).toBe('root');
    });

    it('should throw InvalidUsernameOrPassword when authenticating with initial credentials if an account already exists', async () => {
      const service = make_initializationServiceWithCredentials('root', 'root');
      await authService.register('admin', 'admin');

      await service.initializeRootUser();

      const fn = authService.authenticate('root', 'root');
      expect(fn).rejects.toThrow(InvalidUsernameOrPasswordError);
    });
  });
});
