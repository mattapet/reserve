/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleTokenService } from '../../domain/console-token/console-token.service';

import { ConsoleAccount } from '../../domain/console-account';
import { ConsoleRefreshToken } from '../../domain/console-token/console-refresh-token.entity';
import {
  InvalidTokenError,
  TokenNotFoundError,
} from '../../domain/console-token/console-token.errors';
import { InMemoryConsoleTokenRepository } from '../../application/console-token/repositories/in-memory/in-memory-console-token.repository';
//#endregion

describe('console-token.service', () => {
  let service!: ConsoleTokenService;

  beforeEach(() => {
    service = new ConsoleTokenService(new InMemoryConsoleTokenRepository());
  });

  describe('token generation', () => {
    it('should generate valid token pair for account', async () => {
      const account = new ConsoleAccount('root', 'root');

      const [at, rt] = await service.generateTokenPair(account);

      expect(at.isValid()).toBe(true);
      expect(rt.isValid()).toBe(true);
    });

    it('should generate a valid access token for refresh token', async () => {
      const account = new ConsoleAccount('root', 'root');
      const [, rt] = await service.generateTokenPair(account);

      const at = await service.refreshAccessToken(rt);

      expect(at.isValid()).toBe(true);
    });

    it('should throw InvalidTokenError if refresh token is expired', async () => {
      const rt = new ConsoleRefreshToken('token', 84600, 'root');
      rt.created = new Date(0);

      const fn = service.refreshAccessToken(rt);

      await expect(fn).rejects.toThrow(InvalidTokenError);
    });

    it('should throw InvalidTokenError if refresh token is revoked', async () => {
      const rt = new ConsoleRefreshToken('token', 84600, 'root');
      rt.created = new Date(Date.now());
      rt.revoke();

      const fn = service.refreshAccessToken(rt);

      await expect(fn).rejects.toThrow(InvalidTokenError);
    });
  });

  describe('token querying', () => {
    it('should retrieve access token', async () => {
      const account = new ConsoleAccount('root', 'root');
      const [at] = await service.generateTokenPair(account);

      const accessToken = await service.getAccessToken(at.value);

      expect(accessToken).toEqual(at);
    });

    it('should throw TokenNotFoundError if requested access token is not found', async () => {
      const fn = service.getAccessToken('some random token');

      await expect(fn).rejects.toThrow(TokenNotFoundError);
    });

    it('should retrieve access token', async () => {
      const account = new ConsoleAccount('root', 'root');
      const [, rt] = await service.generateTokenPair(account);

      const refreshToken = await service.getRefreshToken(rt.value);

      expect(refreshToken).toEqual(rt);
    });

    it('should throw TokenNotFoundError if requested refresh token is not found', async () => {
      const fn = service.getRefreshToken('some random token');

      await expect(fn).rejects.toThrow(TokenNotFoundError);
    });
  });

  describe('token revokation', () => {
    it('should should revoke reresh token', async () => {
      const rt = new ConsoleRefreshToken('token', 84600, 'root');
      rt.created = new Date(Date.now());

      await service.revokeRefreshToken(rt);

      expect(rt.isRevoked()).toBe(true);
    });

    it('should throw InvalidTokenError if token is already revoked', async () => {
      const rt = new ConsoleRefreshToken('token', 84600, 'root');
      rt.created = new Date(Date.now());
      rt.revoke();

      const fn = service.revokeRefreshToken(rt);

      await expect(fn).rejects.toThrow(InvalidTokenError);
    });
  });
});
