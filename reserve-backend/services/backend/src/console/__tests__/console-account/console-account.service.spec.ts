/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleAccountService } from '../../domain/console-account/console-account.service';

import { ConsoleAccount } from '../../domain/console-account/console-account.entity';
import {
  InvalidOperationError,
  ConsoleAccountNotFoundError,
} from '../../domain/console-account/console-account.errors';

import { InMemoryConsoleAccountRepository } from '../../application/console-account/repositories/in-memory/in-memory-console-account.repository';
//#endregion

describe('console-account.service', () => {
  let service!: ConsoleAccountService;

  beforeEach(() => {
    service = new ConsoleAccountService(new InMemoryConsoleAccountRepository());
  });

  describe('find by username', () => {
    it('should return undefined if no accounts are present', async () => {
      const result = await service.findByUsername('some-username');

      expect(result).toBeUndefined();
    });

    it('should return account matching requested username', async () => {
      const account = new ConsoleAccount('username', 'password');
      await service.create(account);

      const result = await service.findByUsername('username');

      expect(result).toEqual(account);
    });

    it('should return account matching requested username', async () => {
      const account = new ConsoleAccount('username', 'password');
      await service.create(account);

      const result = await service.findByUsername('other-username');

      expect(result).toBeUndefined();
    });
  });

  describe('get by username', () => {
    it('should retrive account by its username', async () => {
      const account = new ConsoleAccount('root', 'root');
      await service.create(account);

      const result = await service.getByUsername('root');

      expect(result).toEqual(new ConsoleAccount('root', 'root'));
    });

    it('should throw ConsoleAccountNotFoundError if account for given username is not found', async () => {
      const fn = service.getByUsername('root');

      await expect(fn).rejects.toThrow(ConsoleAccountNotFoundError);
    });
  });

  describe('account removal', () => {
    it('should be able to remove an existing user', async () => {
      const root = new ConsoleAccount('root', 'root');
      const admin = new ConsoleAccount('admin', 'admin');
      await service.create(root);

      await service.remove(root, admin);

      const result = await service.findByUsername('root');
      expect(result).toBeUndefined();
    });

    it('should throw InvalidOperationError when account is trying to remove itself', async () => {
      const account = new ConsoleAccount('root', 'root');

      const fn = service.remove(account, account);

      await expect(fn).rejects.toThrow(InvalidOperationError);
    });
  });
});
