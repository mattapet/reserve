/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleAccountRepository } from '../../../../domain/console-account/console-account.repository';
import { ConsoleAccount } from '../../../../domain/console-account/console-account.entity';
import { ConsoleAccountNotFoundError } from '../../../../domain/console-account/console-account.errors';
//#endregion

export class InMemoryConsoleAccountRepository
  implements ConsoleAccountRepository {
  public constructor(
    private storage: { [username: string]: ConsoleAccount } = {},
  ) {}

  public clear(): void {
    this.storage = {};
  }

  public async getAll(): Promise<ConsoleAccount[]> {
    return Object.values(this.storage);
  }

  public async findByUsername(
    username: string,
  ): Promise<ConsoleAccount | undefined> {
    return this.storage[username];
  }

  public async getByUsername(username: string): Promise<ConsoleAccount> {
    const account = this.storage[username];
    if (!account) {
      throw new ConsoleAccountNotFoundError(
        `Account with username '${username}' does not exist`,
      );
    }
    return account;
  }

  public async save(account: ConsoleAccount): Promise<ConsoleAccount> {
    return (this.storage[account.getUsername()] = account);
  }

  public async remove(account: ConsoleAccount): Promise<ConsoleAccount> {
    delete this.storage[account.getUsername()];
    return account;
  }
}
