/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository } from 'typeorm';
import { BaseRepository } from '@rsaas/commons/lib/typeorm';

import { ConsoleAccount } from '../../../../domain/console-account/console-account.entity';
import { ConsoleAccountRepository } from '../../../../domain/console-account/console-account.repository';
import { ConsoleAccountNotFoundError } from '../../../../domain/console-account/console-account.errors';
//#endregion

@EntityRepository(ConsoleAccount)
export class TypeormConsoleAccountRepository
  extends BaseRepository<ConsoleAccount>
  implements ConsoleAccountRepository {
  public async getAll(): Promise<ConsoleAccount[]> {
    return this.find();
  }

  public async findByUsername(
    username: string,
  ): Promise<ConsoleAccount | undefined> {
    return this.findOne({ username } as any);
  }

  public async getByUsername(username: string): Promise<ConsoleAccount> {
    const item = await this.findOne({ username } as any);
    if (!item) {
      throw new ConsoleAccountNotFoundError(
        `Account with id '${username}' does not exist`,
      );
    }
    return item;
  }
}
