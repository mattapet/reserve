/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { Transactional } from '@rsaas/commons/lib/typeorm';

import {
  ConsoleTokenRepository,
  ConsoleToken,
} from '../../../../domain/console-token/console-token.repository';
import { ConsoleRefreshToken } from '../../../../domain/console-token/console-refresh-token.entity';
import { ConsoleAccessToken } from '../../../../domain/console-token/console-access-token.entity';
import { ConsoleAccessTokenRepository } from './console-refresh-token.repository';
import { ConsoleRefreshTokenRepository } from './console-access-token.repository';
//#endregion

@Injectable()
export class TypeormConsoleTokenRepository implements ConsoleTokenRepository {
  public constructor(
    private readonly atRepository: ConsoleAccessTokenRepository,
    private readonly rtRepository: ConsoleRefreshTokenRepository,
  ) {}

  @Transactional()
  public async clear(): Promise<void> {
    await this.atRepository.clear();
    await this.rtRepository.clear();
  }

  public async findAccessToken(
    value: string,
  ): Promise<ConsoleAccessToken | undefined> {
    return this.atRepository.findOne(value);
  }

  public async findRefreshToken(
    value: string,
  ): Promise<ConsoleRefreshToken | undefined> {
    return this.rtRepository.findOne(value);
  }

  @Transactional()
  public async save(...tokens: ConsoleToken[]): Promise<void> {
    await Promise.all(tokens.map((token) => this.saveToken(token)));
  }

  private async saveToken(token: ConsoleToken): Promise<void> {
    if (token instanceof ConsoleAccessToken) {
      await this.atRepository.save(token);
    } else {
      await this.rtRepository.save(token);
    }
  }
}
