/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  ConsoleTokenRepository,
  ConsoleToken,
} from '../../../../domain/console-token/console-token.repository';
import { ConsoleAccessToken } from '../../../../domain/console-token/console-access-token.entity';
import { ConsoleRefreshToken } from '../../../../domain/console-token/console-refresh-token.entity';
//#endregion

export class InMemoryConsoleTokenRepository implements ConsoleTokenRepository {
  public constructor(
    private atStorage: { [token: string]: ConsoleAccessToken } = {},
    private rtStorage: { [token: string]: ConsoleRefreshToken } = {},
  ) {}

  public clear(): void {
    this.atStorage = {};
    this.rtStorage = {};
  }

  public async findAccessToken(
    token: string,
  ): Promise<ConsoleAccessToken | undefined> {
    return this.atStorage[token];
  }

  public async findRefreshToken(
    token: string,
  ): Promise<ConsoleRefreshToken | undefined> {
    return this.rtStorage[token];
  }

  public async save(...tokens: ConsoleToken[]): Promise<void> {
    await Promise.all(tokens.map((token) => this.saveToken(token)));
  }

  private async saveToken(token: ConsoleToken): Promise<void> {
    if (token instanceof ConsoleAccessToken) {
      await this.saveAccessToken(token);
    } else {
      await this.saveRefreshToken(token);
    }
  }

  private async saveAccessToken(at: ConsoleAccessToken): Promise<void> {
    at.created = new Date(Date.now());
    this.atStorage[at.value] = at;
  }

  private async saveRefreshToken(rt: ConsoleRefreshToken): Promise<void> {
    rt.created = new Date(Date.now());
    this.rtStorage[rt.value] = rt;
  }
}
