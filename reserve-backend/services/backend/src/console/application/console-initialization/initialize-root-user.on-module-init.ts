/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { OnModuleInit, Injectable } from '@nestjs/common';
import { Transactional } from '@rsaas/commons/lib/typeorm';

import { ConsoleInitializationService } from '../../domain/console-initialization/console-initialization.service';
//#endregion

@Injectable()
export class InitializeRootUserOnModuleInit implements OnModuleInit {
  public constructor(
    private readonly initializationService: ConsoleInitializationService,
  ) {}

  @Transactional()
  public async onModuleInit(): Promise<void> {
    await this.initializationService.initializeRootUser();
  }
}
