/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import bcryptjs from 'bcryptjs';
import { Injectable } from '@nestjs/common';

import { ConsoleAccountService, ConsoleAccount } from '../console-account';

import {
  InvalidUsernameOrPasswordError,
  UsernameAlreadyExistsError,
} from './console-auth.errors';
//#endregion

@Injectable()
export class ConsoleAuthService {
  public constructor(private readonly accountService: ConsoleAccountService) {}

  public async hasNoRegisteredAccounts(): Promise<boolean> {
    const accounts = await this.accountService.getAll();
    return accounts.length === 0;
  }

  public async authenticate(
    username: string,
    password: string,
  ): Promise<ConsoleAccount> {
    const account = await this.accountService.findByUsername(username);
    if (!account) {
      throw new InvalidUsernameOrPasswordError('Invalid username or password');
    }
    if (!(await bcryptjs.compare(password, account.getPassword()))) {
      throw new InvalidUsernameOrPasswordError('Invalid username or password');
    }
    return new ConsoleAccount(username, password);
  }

  public async register(username: string, password: string): Promise<void> {
    const account = await this.accountService.findByUsername(username);
    if (account != null) {
      throw new UsernameAlreadyExistsError(
        `Account with username '${username}' already exsits`,
      );
    }

    const hashed = await bcryptjs.hash(password, 10);
    await this.accountService.create(new ConsoleAccount(username, hashed));
  }
}
