/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region improts
import { Injectable, Inject } from '@nestjs/common';

import { ConsoleAuthService } from '../console-auth';
import { INITIAL_ROOT_USERNAME, INITIAL_ROOT_PASSWORD } from './constants';
//#endregion

@Injectable()
export class ConsoleInitializationService {
  public constructor(
    private readonly authService: ConsoleAuthService,
    @Inject(INITIAL_ROOT_USERNAME)
    private readonly initialUsername: string,
    @Inject(INITIAL_ROOT_PASSWORD)
    private readonly initialPassword: string,
  ) {}

  public async initializeRootUser(): Promise<void> {
    if (await this.authService.hasNoRegisteredAccounts()) {
      await this.authService.register(
        this.initialUsername,
        this.initialPassword,
      );
    }
  }
}
