/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';
import { generateToken } from '@rsaas/util/lib/generate-token';

import { ConsoleAccount } from '../console-account';

import { ConsoleTokenRepository } from './console-token.repository';
import { ConsoleAccessToken } from './console-access-token.entity';
import { TokenNotFoundError, InvalidTokenError } from './console-token.errors';
import { ConsoleRefreshToken } from './console-refresh-token.entity';
import { CONSOLE_TOKEN_REPOSITORY } from './constants';
//#endregion

@Injectable()
export class ConsoleTokenService {
  public constructor(
    @Inject(CONSOLE_TOKEN_REPOSITORY)
    private readonly repository: ConsoleTokenRepository,
  ) {}

  public async getAccessToken(value: string): Promise<ConsoleAccessToken> {
    const token = await this.repository.findAccessToken(value);
    if (!token) {
      throw new TokenNotFoundError();
    }
    return token;
  }

  public async getRefreshToken(value: string): Promise<ConsoleRefreshToken> {
    const token = await this.repository.findRefreshToken(value);
    if (!token) {
      throw new TokenNotFoundError();
    }
    return token;
  }

  public async generateTokenPair(
    account: ConsoleAccount,
  ): Promise<[ConsoleAccessToken, ConsoleRefreshToken]> {
    const at = new ConsoleAccessToken(
      generateToken(),
      300,
      account.getUsername(),
    );
    const rt = new ConsoleRefreshToken(
      generateToken(),
      84600,
      account.getUsername(),
    );
    await this.repository.save(at, rt);
    return [at, rt];
  }

  public async refreshAccessToken(
    refreshToken: ConsoleRefreshToken,
  ): Promise<ConsoleAccessToken> {
    if (refreshToken.isExpired()) {
      throw new InvalidTokenError('Refresh token is expired');
    }

    if (refreshToken.isRevoked()) {
      throw new InvalidTokenError('Refresh token is revoked');
    }

    const accountUsername = refreshToken.accountUsername;
    const at = new ConsoleAccessToken(generateToken(), 300, accountUsername);
    await this.repository.save(at);
    return at;
  }

  public async revokeRefreshToken(
    refreshToken: ConsoleRefreshToken,
  ): Promise<void> {
    if (refreshToken.isRevoked()) {
      throw new InvalidTokenError('Refresh token is revoked');
    }
    refreshToken.revoke();
    await this.repository.save(refreshToken);
  }
}
