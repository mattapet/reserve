/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleAccessToken } from './console-access-token.entity';
import { ConsoleRefreshToken } from './console-refresh-token.entity';
//#endregion

export type ConsoleToken = ConsoleAccessToken | ConsoleRefreshToken;

export interface ConsoleTokenRepository {
  findAccessToken(token: string): Promise<ConsoleAccessToken | undefined>;
  findRefreshToken(token: string): Promise<ConsoleRefreshToken | undefined>;
  save(...token: ConsoleToken[]): Promise<void>;
}
