/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity, PrimaryColumn, Column, CreateDateColumn } from 'typeorm';
//#endregion

@Entity({ name: 'console_access_token' })
export class ConsoleAccessToken {
  @PrimaryColumn({ length: 64, charset: 'utf8' })
  public value: string;

  @Column()
  public expiry: number;

  @CreateDateColumn({
    transformer: {
      to: (d) => d ?? new Date(Date.now()).toISOString(),
      from: (d: Date) => d,
    },
  })
  public created!: Date;

  @Column({ name: 'console_account_username', charset: 'utf8' })
  public accountUsername: string;

  public constructor(value: string, expiry: number, accountUsername: string) {
    this.value = value;
    this.expiry = expiry;
    this.accountUsername = accountUsername;
  }

  public isValid(): boolean {
    return Date.now() < this.created.valueOf() + this.expiry * 1000;
  }
}
