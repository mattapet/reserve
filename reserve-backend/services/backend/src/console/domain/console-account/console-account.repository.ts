/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleAccount } from './console-account.entity';
//#endregion

export interface ConsoleAccountRepository {
  getAll(): Promise<ConsoleAccount[]>;
  getByUsername(id: string): Promise<ConsoleAccount>;
  findByUsername(username: string): Promise<ConsoleAccount | undefined>;
  save(account: ConsoleAccount): Promise<ConsoleAccount>;
  remove(account: ConsoleAccount): Promise<ConsoleAccount>;
}
