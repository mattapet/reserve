/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity, Column, PrimaryColumn } from 'typeorm';
//#endregion

@Entity({ name: 'console_account' })
export class ConsoleAccount {
  @PrimaryColumn({ name: 'username', charset: 'utf8' })
  private username: string;

  @Column({ name: 'password', charset: 'utf8' })
  private password: string;

  public constructor(username: string, password: string) {
    this.username = username;
    this.password = password;
  }

  public getUsername(): string {
    return this.username;
  }

  public getPassword(): string {
    return this.password;
  }

  public equals(other: ConsoleAccount): boolean {
    return this.username === other.getUsername();
  }
}
