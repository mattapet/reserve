/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';

import { ConsoleAccount } from './console-account.entity';
import { ConsoleAccountRepository } from './console-account.repository';
import {
  InvalidOperationError,
  ConsoleAccountNotFoundError,
} from './console-account.errors';
import { CONSOLE_ACCOUNT_REPOSITORY } from './constants';
//#endregion

@Injectable()
export class ConsoleAccountService {
  public constructor(
    @Inject(CONSOLE_ACCOUNT_REPOSITORY)
    private readonly repository: ConsoleAccountRepository,
  ) {}

  public async getAll(): Promise<ConsoleAccount[]> {
    return this.repository.getAll();
  }

  public async findByUsername(
    username: string,
  ): Promise<ConsoleAccount | undefined> {
    return this.repository.findByUsername(username);
  }

  public async getByUsername(username: string): Promise<ConsoleAccount> {
    const account = await this.repository.findByUsername(username);
    if (!account) {
      throw new ConsoleAccountNotFoundError(
        `Account for username '${username}' was not found`,
      );
    }
    return account!;
  }

  public async create(account: ConsoleAccount): Promise<void> {
    await this.repository.save(account);
  }

  public async remove(
    account: ConsoleAccount,
    removedBy: ConsoleAccount,
  ): Promise<void> {
    if (removedBy.equals(account)) {
      throw new InvalidOperationError('Cannot remove your own account.');
    }
    await this.repository.remove(account);
  }
}
