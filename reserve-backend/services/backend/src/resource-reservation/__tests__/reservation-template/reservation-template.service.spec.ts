/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationTemplateService } from '../../domain/reservation-template/reservation-template.service';

import { Workspace, WorkspaceName } from '../../../workspace-management';

import { ReservationTemplate } from '../../domain/reservation-template/reservation-template.entity';
import { InMemoryReservationTemplateRepository } from '../../application/reservation-template/repositories/in-memory/in-memory-reservation-template.repository';
//#endregion

describe('reservation-template.service', () => {
  let service!: ReservationTemplateService;

  beforeEach(() => {
    service = new ReservationTemplateService(
      new InMemoryReservationTemplateRepository(),
    );
  });

  describe('retrieving templates', () => {
    it('should return undefined if there is no template for given workspace', async () => {
      const workspace = new Workspace('test', new WorkspaceName('test'));

      const result = await service.findByWorkspace(workspace);

      expect(result).toBeUndefined();
    });

    it('should return template', async () => {
      const workspace = new Workspace('test', new WorkspaceName('test'));
      await service.save(new ReservationTemplate('test', 'some template'));

      const result = await service.findByWorkspace(workspace);

      expect(result).toEqual(new ReservationTemplate('test', 'some template'));
    });

    it('should return template for the given workspace', async () => {
      const workspace = new Workspace('test', new WorkspaceName('test'));
      await service.save(new ReservationTemplate('test', 'some template'));
      await service.save(new ReservationTemplate('test1', 'some template'));

      const result = await service.findByWorkspace(workspace);

      expect(result).toEqual(new ReservationTemplate('test', 'some template'));
    });
  });

  describe('template management', () => {
    it('should override existing template', async () => {
      const workspace = new Workspace('test', new WorkspaceName('test'));

      await service.save(new ReservationTemplate('test', 'some template'));
      await service.save(new ReservationTemplate('test', 'another template'));

      const result = await service.findByWorkspace(workspace);
      expect(result).toEqual(
        new ReservationTemplate('test', 'another template'),
      );
    });

    it('should delete workspace for given workspace', async () => {
      const workspace = new Workspace('test', new WorkspaceName('test'));

      await service.save(new ReservationTemplate('test', 'some template'));
      await service.remove(workspace);

      const result = await service.findByWorkspace(workspace);
      expect(result).toBeUndefined();
    });
  });
});
