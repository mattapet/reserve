/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ResourceCommandHandler } from '../../../domain/resource/handlers/resource.command-handler';
import {
  ResourceCommandType,
  CreateResourceCommand,
  ToggleActiveResourceCommand,
  DeleteResourceCommand,
  UpdateResourceCommand,
} from '../../../domain/resource/resource.command';
import { Resource } from '../../../domain/resource/resource.entity';
import { ResourceEventType } from '../../../domain/resource/resource.event';
import { ResourceManager } from '../../../domain/resource/entities';
//#endregion

describe('resource.command-handler', () => {
  const resourceCommandHandler = new ResourceCommandHandler();

  function mock_dateNow(now: number = 100) {
    jest.spyOn(Date, 'now').mockImplementation(() => now);
  }

  function make_defaultResource(): Resource {
    return new Resource('99', 'test', 'test resource', 'test description');
  }

  describe('create command', () => {
    it('should produce a created, nameSet and descriptionSet events', () => {
      mock_dateNow(100);
      const command: CreateResourceCommand = {
        type: ResourceCommandType.create,
        rowId: 'ResourceAggregate:test',
        aggregateId: '99',
        payload: {
          workspaceId: 'test',
          name: 'test resource',
          description: 'description',
          createdBy: new ResourceManager('44'),
        },
      };

      const result = resourceCommandHandler.execute(new Resource(), command);

      const baseEvent = {
        rowId: 'ResourceAggregate:test',
        aggregateId: '99',
        timestamp: new Date(100),
      };
      expect(result).toEqual([
        {
          ...baseEvent,
          type: ResourceEventType.created,
          payload: {
            workspaceId: 'test',
            createdBy: '44',
          },
        },
        {
          ...baseEvent,
          type: ResourceEventType.nameSet,
          payload: {
            name: 'test resource',
            changedBy: '44',
          },
        },
        {
          ...baseEvent,
          type: ResourceEventType.descriptionSet,
          payload: {
            description: 'description',
            changedBy: '44',
          },
        },
      ]);
    });
  });

  describe('update command', () => {
    it('should produce no events if nothing changed', () => {
      mock_dateNow(100);
      const resource = make_defaultResource();
      const command: UpdateResourceCommand = {
        type: ResourceCommandType.update,
        rowId: 'ResourceAggregate:test',
        aggregateId: '99',
        payload: {
          name: 'test resource',
          description: 'test description',
          changedBy: new ResourceManager('44'),
        },
      };

      const result = resourceCommandHandler.execute(resource, command);

      expect(result).toEqual([]);
    });

    it('should produce nameSet event', () => {
      mock_dateNow(100);
      const resource = make_defaultResource();
      const command: UpdateResourceCommand = {
        type: ResourceCommandType.update,
        rowId: 'ResourceAggregate:test',
        aggregateId: '99',
        payload: {
          name: 'new name',
          description: 'test description',
          changedBy: new ResourceManager('44'),
        },
      };

      const result = resourceCommandHandler.execute(resource, command);

      expect(result).toEqual([
        {
          type: ResourceEventType.nameSet,
          rowId: 'ResourceAggregate:test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            name: 'new name',
            changedBy: '44',
          },
        },
      ]);
    });

    it('should produce descriptionSet event', () => {
      mock_dateNow(100);
      const resource = make_defaultResource();
      const command: UpdateResourceCommand = {
        type: ResourceCommandType.update,
        rowId: 'ResourceAggregate:test',
        aggregateId: '99',
        payload: {
          name: 'test resource',
          description: 'new description',
          changedBy: new ResourceManager('44'),
        },
      };

      const result = resourceCommandHandler.execute(resource, command);

      expect(result).toEqual([
        {
          type: ResourceEventType.descriptionSet,
          rowId: 'ResourceAggregate:test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            description: 'new description',
            changedBy: '44',
          },
        },
      ]);
    });

    it('should produce nameSet and descriptionSet events', () => {
      mock_dateNow(100);
      const resource = make_defaultResource();
      const command: UpdateResourceCommand = {
        type: ResourceCommandType.update,
        rowId: 'ResourceAggregate:test',
        aggregateId: '99',
        payload: {
          name: 'new name',
          description: 'new description',
          changedBy: new ResourceManager('44'),
        },
      };

      const result = resourceCommandHandler.execute(resource, command);

      expect(result).toEqual([
        {
          type: ResourceEventType.nameSet,
          rowId: 'ResourceAggregate:test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            name: 'new name',
            changedBy: '44',
          },
        },
        {
          type: ResourceEventType.descriptionSet,
          rowId: 'ResourceAggregate:test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            description: 'new description',
            changedBy: '44',
          },
        },
      ]);
    });
  });

  describe('toggle command', () => {
    it('should produce a deactivated event is resource is active', () => {
      mock_dateNow(100);
      const command: ToggleActiveResourceCommand = {
        type: ResourceCommandType.toggleActive,
        rowId: 'ResourceAggregate:test',
        aggregateId: '99',
        payload: {
          changedBy: new ResourceManager('44'),
        },
      };
      const resource = make_defaultResource();

      const result = resourceCommandHandler.execute(resource, command);

      expect(result).toEqual([
        {
          type: ResourceEventType.deactivated,
          rowId: 'ResourceAggregate:test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            changedBy: '44',
          },
        },
      ]);
    });

    it('should produce a activated event if resource is inactive', () => {
      mock_dateNow(100);
      const command: ToggleActiveResourceCommand = {
        type: ResourceCommandType.toggleActive,
        rowId: 'ResourceAggregate:test',
        aggregateId: '99',
        payload: {
          changedBy: new ResourceManager('44'),
        },
      };
      const resource = make_defaultResource();
      resource.deactivate();

      const result = resourceCommandHandler.execute(resource, command);

      expect(result).toEqual([
        {
          type: ResourceEventType.activated,
          rowId: 'ResourceAggregate:test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            changedBy: '44',
          },
        },
      ]);
    });
  });

  describe('delete command', () => {
    it('should produce a deleted event', () => {
      mock_dateNow(100);
      const command: DeleteResourceCommand = {
        type: ResourceCommandType.delete,
        rowId: 'ResourceAggregate:test',
        aggregateId: '99',
        payload: {
          deletedBy: new ResourceManager('44'),
        },
      };
      const resource = make_defaultResource();

      const result = resourceCommandHandler.execute(resource, command);

      expect(result).toEqual([
        {
          type: ResourceEventType.deleted,
          rowId: 'ResourceAggregate:test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            deletedBy: '44',
          },
        },
      ]);
    });

    it('should produce no events if resource is deleted', () => {
      mock_dateNow(100);
      const command: DeleteResourceCommand = {
        type: ResourceCommandType.delete,
        rowId: 'ResourceAggregate:test',
        aggregateId: '99',
        payload: {
          deletedBy: new ResourceManager('44'),
        },
      };
      const resource = make_defaultResource();
      resource.delete();

      const result = resourceCommandHandler.execute(resource, command);

      expect(result).toEqual([]);
    });
  });
});
