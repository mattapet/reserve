/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ResourceEventHandler } from '../../../domain/resource/handlers/resource.event-handler';
import {
  ResourceEventType,
  ResourceDeactivatedEvent,
  ResourceActivatedEvent,
  ResourceDeletedEvent,
  ResourceEvent,
} from '../../../domain/resource/resource.event';
import { Resource } from '../../../domain/resource/resource.entity';
//#endregion

describe('resource.event-handler', () => {
  const resourceEvenHandler = new ResourceEventHandler();

  function make_defaultResource(): Resource {
    return new Resource('99', 'test', 'test resource', 'test description');
  }

  it('should initialize newly created resource', () => {
    const baseEvent = {
      rowId: 'ResourceAggregate:test',
      aggregateId: '99',
      timestamp: new Date(100),
    };
    const events: ResourceEvent[] = [
      {
        ...baseEvent,
        type: ResourceEventType.created,
        payload: {
          workspaceId: 'test',
          createdBy: '44',
        },
      },
      {
        ...baseEvent,
        type: ResourceEventType.nameSet,
        payload: {
          name: 'test resource',
          changedBy: '44',
        },
      },
      {
        ...baseEvent,
        type: ResourceEventType.descriptionSet,
        payload: {
          description: 'test description',
          changedBy: '44',
        },
      },
    ];
    const resource = new Resource();

    const result = resourceEvenHandler.apply(resource, events);

    expect(result).toEqual(
      new Resource('99', 'test', 'test resource', 'test description'),
    );
  });

  it('should deactivate active resource', () => {
    const event: ResourceDeactivatedEvent = {
      type: ResourceEventType.deactivated,
      rowId: 'ResourceAggregate:test',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        changedBy: '44',
      },
    };
    const resource = make_defaultResource();

    const result = resourceEvenHandler.apply(resource, [event]);

    expect(result.isActive()).toBe(false);
  });

  it('should activate inactive resource', () => {
    const event: ResourceActivatedEvent = {
      type: ResourceEventType.activated,
      rowId: 'ResourceAggregate:test',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        changedBy: '44',
      },
    };
    const resource = make_defaultResource();
    resource.deactivate();

    const result = resourceEvenHandler.apply(resource, [event]);

    expect(result.isActive()).toBe(true);
  });

  it('should delete a resource', () => {
    const event: ResourceDeletedEvent = {
      type: ResourceEventType.deleted,
      rowId: 'ResourceAggregate:test',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        deletedBy: '44',
      },
    };
    const resource = make_defaultResource();

    const result = resourceEvenHandler.apply(resource, [event]);

    expect(result.isDeleted()).toBe(true);
  });
});
