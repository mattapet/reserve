/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';
import { EventRepository, SnapshotRepository } from '@rsaas/commons/lib/events';
import { InMemoryEventRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-event-repository';
import { InMemorySnapshotRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-snapshot.repository';

import { ResourceEvent } from '../../../domain/resource/resource.event';
import {
  ResourceCommandHandler,
  ResourceEventHandler,
} from '../../../domain/resource/handlers';

import { ResourceRepository } from '../../../application/resource/resource.repository';
//#endregion

export class ResourceRepositoryBuilder {
  private eventRepository?: EventRepository<ResourceEvent>;
  private snapshotRepository?: SnapshotRepository;

  public withEventRepository(
    eventRepository: EventRepository<ResourceEvent>,
  ): ResourceRepositoryBuilder {
    this.eventRepository = eventRepository;
    return this;
  }

  public withSnapshotRepository(
    snapshotRepository: SnapshotRepository,
  ): ResourceRepositoryBuilder {
    this.snapshotRepository = snapshotRepository;
    return this;
  }

  public build(): ResourceRepository {
    return new ResourceRepository(
      notNull(this.eventRepository, 'eventRepository'),
      notNull(this.snapshotRepository, 'snapshotRepository'),
      new ResourceCommandHandler(),
      new ResourceEventHandler(),
    );
  }

  public static inMemory(): ResourceRepository {
    const eventsRepo = new InMemoryEventRepository<ResourceEvent>();
    return new ResourceRepositoryBuilder()
      .withEventRepository(eventsRepo)
      .withSnapshotRepository(new InMemorySnapshotRepository())
      .build();
  }
}
