/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole, User, Identity } from '../../../../user-access';

import { ResourceManager } from '../../../domain/resource/entities';
import { InsufficientRightsError } from '../../../domain/resource/resource.errors';
//#endregion

describe('resource-manager.entity', () => {
  it('should create a resource manager from admin', () => {
    const result = ResourceManager.from(
      new User(
        '99',
        'test',
        new Identity('99', 'test', 'test@test.com'),
        UserRole.admin,
      ),
    );

    expect(result).toEqual(new ResourceManager('99'));
  });

  it('should throw InsufficientRightsError if user tries to become ResourceManager', () => {
    const fn = () =>
      ResourceManager.from(
        new User(
          '99',
          'test',
          new Identity('99', 'test', 'test@test.com'),
          UserRole.user,
        ),
      );

    expect(fn).toThrow(InsufficientRightsError);
  });
});
