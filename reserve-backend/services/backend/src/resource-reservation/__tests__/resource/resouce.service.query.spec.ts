/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ResourceService } from '../../domain/resource/resource.service';
import { ResourceRepositoryBuilder } from './builders/resource.repository.builder';
import { Resource, ResourceId } from '../../domain/resource/resource.entity';
import { ResourceRepository } from '../../application/resource/resource.repository';
import { ResourceNotFoundError } from '../../domain/resource/resource.errors';
import { CreateResource } from '../../domain/resource/values';
import { ResourceManager } from '../../domain/resource/entities';
//#endregion

describe('resource.service query', () => {
  let repository!: ResourceRepository;
  let service!: ResourceService;

  beforeEach(() => {
    repository = ResourceRepositoryBuilder.inMemory();
    service = new ResourceService(repository);
  });

  function make_resourceWithId(id: ResourceId) {
    return new Resource(id, 'test', 'test resource', 'test description');
  }

  function make_createParamsWithId(
    id: string,
    createdBy: ResourceManager,
  ): CreateResource {
    return new CreateResource(
      id,
      'test',
      'test resource',
      'test description',
      createdBy,
    );
  }

  describe('retrieving all resources', () => {
    it('should return an empty array if there are not resources', async () => {
      const result = await service.getAll('test');

      expect(result).toEqual([]);
    });

    it('should retrieve all resources, excluding deleted ones', async () => {
      const manager = new ResourceManager('99');
      const resource = new Resource();
      await Promise.all([
        service.create(new Resource(), make_createParamsWithId('77', manager)),
        service.create(new Resource(), make_createParamsWithId('88', manager)),
        service.create(resource, make_createParamsWithId('99', manager)),
      ]);
      await service.delete(resource, manager);

      const result = await service.getAll('test');

      expect(result.length).toBe(2);
      expect(result).toEqual(
        expect.arrayContaining([
          make_resourceWithId('77'),
          make_resourceWithId('88'),
        ]),
      );
    });
  });

  describe('retrieving resources by id', () => {
    it('should retrieve resource by its ID', async () => {
      const manager = new ResourceManager('99');
      await Promise.all([
        service.create(new Resource(), make_createParamsWithId('77', manager)),
        service.create(new Resource(), make_createParamsWithId('88', manager)),
        service.create(new Resource(), make_createParamsWithId('99', manager)),
      ]);

      const result = await service.getById('test', '88');

      expect(result).toEqual(make_resourceWithId('88'));
    });

    it('should retrieve resources by their IDs', async () => {
      const manager = new ResourceManager('99');
      await Promise.all([
        service.create(new Resource(), make_createParamsWithId('77', manager)),
        service.create(new Resource(), make_createParamsWithId('88', manager)),
        service.create(new Resource(), make_createParamsWithId('99', manager)),
      ]);

      const result = await service.getByIds('test', ['77', '88', '99']);

      expect(result).toEqual([
        make_resourceWithId('77'),
        make_resourceWithId('88'),
        make_resourceWithId('99'),
      ]);
    });

    it('should throw ResourceNotFoundError if trying to get resource that does not exist', async () => {
      const fn = service.getById('test', '99');

      await expect(fn).rejects.toThrow(ResourceNotFoundError);
    });

    it('should throw ResourceNotFoundError if trying to get resource that has been deleted', async () => {
      const manager = new ResourceManager('99');
      const resource = new Resource();
      await service.create(resource, make_createParamsWithId('99', manager));
      await service.delete(resource, manager);

      const fn = service.getById('test', '99');

      await expect(fn).rejects.toThrow(ResourceNotFoundError);
    });
  });
});
