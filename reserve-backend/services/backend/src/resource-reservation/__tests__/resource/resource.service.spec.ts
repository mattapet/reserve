/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ResourceService } from '../../domain/resource/resource.service';
import { ResourceRepositoryBuilder } from './builders/resource.repository.builder';
import { Resource } from '../../domain/resource/resource.entity';
import { ResourceCommandType } from '../../domain/resource/resource.command';
import { ResourceRepository } from '../../application/resource/resource.repository';
import { ResourceManager } from '../../domain/resource/entities';
//#endregion

describe('resource.service', () => {
  let repository!: ResourceRepository;
  let service!: ResourceService;

  beforeEach(() => {
    repository = ResourceRepositoryBuilder.inMemory();
    service = new ResourceService(repository);
  });

  function make_defaultResource() {
    return new Resource('99', 'test', 'test resource', 'test description');
  }

  function spy_repositoryExecute() {
    return jest.spyOn(repository, 'execute');
  }

  describe('resource creation', () => {
    it('should produce a create command', async () => {
      const resource = new Resource();
      const createdBy = new ResourceManager('99');
      const createParams = {
        id: '99',
        workspaceId: 'test',
        name: 'test resource',
        description: 'test description',
        createdBy,
      };
      const execute = spy_repositoryExecute();

      await service.create(resource, createParams);

      expect(execute).toHaveBeenCalledWith(
        {
          type: ResourceCommandType.create,
          rowId: 'ResourceAggregate:test',
          aggregateId: '99',
          payload: {
            workspaceId: 'test',
            name: 'test resource',
            description: 'test description',
            createdBy,
          },
        },
        resource,
      );
    });

    it('should create a resource', async () => {
      const resource = new Resource();
      const createdBy = new ResourceManager('99');
      const createParams = {
        id: '99',
        workspaceId: 'test',
        name: 'test resource',
        description: 'test description',
        createdBy,
      };

      await service.create(resource, createParams);

      expect(resource).toEqual(
        new Resource('99', 'test', 'test resource', 'test description'),
      );
    });
  });

  describe('updating resources', () => {
    it('should produce an update command', async () => {
      const resource = make_defaultResource();
      const updatedBy = new ResourceManager('99');
      const updateParams = {
        name: 'new name',
        description: 'new description',
        updatedBy,
      };
      const execute = spy_repositoryExecute();

      await service.update(resource, updateParams);

      expect(execute).toBeCalledWith(
        {
          type: ResourceCommandType.update,
          rowId: 'ResourceAggregate:test',
          aggregateId: '99',
          payload: {
            name: 'new name',
            description: 'new description',
            changedBy: updatedBy,
          },
        },
        resource,
      );
    });

    it('should update resource details', async () => {
      const resource = make_defaultResource();
      const updatedBy = new ResourceManager('99');
      const updateParams = {
        name: 'new name',
        description: 'new description',
        updatedBy,
      };

      await service.update(resource, updateParams);

      expect(resource.getName()).toBe('new name');
      expect(resource.getDescription()).toBe('new description');
    });
  });

  describe('resource activation toggling', () => {
    it('should produce toggleActive command', async () => {
      const resource = make_defaultResource();
      const changedBy = new ResourceManager('99');
      const execute = spy_repositoryExecute();

      await service.toggle(resource, changedBy);

      expect(execute).toBeCalledWith(
        {
          type: ResourceCommandType.toggleActive,
          rowId: 'ResourceAggregate:test',
          aggregateId: '99',
          payload: { changedBy },
        },
        resource,
      );
    });

    it('should deactivate active resource', async () => {
      const resource = make_defaultResource();
      const manager = new ResourceManager('99');

      await service.toggle(resource, manager);

      expect(resource.isActive()).toBe(false);
    });

    it('should activate inactive resource', async () => {
      const resource = make_defaultResource();
      const manager = new ResourceManager('99');
      resource.deactivate();

      await service.toggle(resource, manager);

      expect(resource.isActive()).toBe(true);
    });
  });

  describe('deleting a resource', () => {
    it('should produce a delete command', async () => {
      const resource = make_defaultResource();
      const deletedBy = new ResourceManager('99');
      const execute = spy_repositoryExecute();

      await service.delete(resource, deletedBy);

      expect(execute).toHaveBeenCalledWith(
        {
          type: ResourceCommandType.delete,
          rowId: 'ResourceAggregate:test',
          aggregateId: '99',
          payload: { deletedBy },
        },
        resource,
      );
    });

    it('should delete a resource', async () => {
      const resource = make_defaultResource();
      const manager = new ResourceManager('99');

      await service.delete(resource, manager);

      expect(resource.isDeleted()).toBe(true);
    });
  });
});
