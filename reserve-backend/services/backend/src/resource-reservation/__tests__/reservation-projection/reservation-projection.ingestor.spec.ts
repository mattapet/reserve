/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationEventType } from '../../domain/reservation/reservation.event';
import { ReservationState } from '../../domain/reservation/reservation-state.type';
import { ReservationProjectionIngestor } from '../../application/reservation-projection/reservation-projection.ingestor';
import { InMemoryReservationProjectionRepository } from '../../application/reservation-projection/repositories/in-memory/in-memory-reservation-projection.repository';
import { ReservationProjectionRepository } from '../../domain/reservation-projection/reservation-projection.repository';
import { ReservationNotFoundError } from '../../domain/reservation-projection/reservation-projection.errors';
//#endregion

describe('reservation projection injestor', () => {
  let repository!: ReservationProjectionRepository;
  let ingestor!: ReservationProjectionIngestor;

  beforeEach(() => {
    repository = new InMemoryReservationProjectionRepository();
    ingestor = new ReservationProjectionIngestor(repository);
  });

  describe('consuming create event', () => {
    it('should create a reservation projection', async () => {
      await ingestor.consumeCreated({
        type: ReservationEventType.created,
        rowId: 'ReservationAggregate:test',
        aggregateId: '99',
        timestamp: new Date(100),
        payload: {
          workspaceId: 'test',
          dateStart: new Date(200).toISOString(),
          dateEnd: new Date(300).toISOString(),
          createdBy: '99',
        },
      });

      const reservation = await repository.getById('test', '99');
      expect(reservation).toEqual({
        id: '99',
        workspaceId: 'test',
        dateStart: new Date(200),
        dateEnd: new Date(300),
        state: ReservationState.requested,
        resourceIds: [],
        userId: '',
        createdAt: new Date(100),
      });
    });
  });

  describe('consuming assigned event', () => {
    it('should update user id', async () => {
      await repository.save({
        id: '99',
        workspaceId: 'test',
        dateStart: new Date(200),
        dateEnd: new Date(300),
        state: ReservationState.requested,
        resourceIds: [],
        userId: '',
        createdAt: new Date(100),
      });

      await ingestor.consumeAssigneeChanged({
        type: ReservationEventType.assigned,
        rowId: 'ReservationAggregate:test',
        aggregateId: '99',
        timestamp: new Date(100),
        payload: {
          assignedTo: '88',
          changedBy: '99',
        },
      });

      const reservation = await repository.getById('test', '99');
      expect(reservation).toEqual({
        id: '99',
        workspaceId: 'test',
        dateStart: new Date(200),
        dateEnd: new Date(300),
        state: ReservationState.requested,
        resourceIds: [],
        userId: '88',
        createdAt: new Date(100),
      });
    });

    it('should throw ReservationNotFoundError if projection does not exists', async () => {
      const fn = ingestor.consumeAssigneeChanged({
        type: ReservationEventType.assigned,
        rowId: 'ReservationAggregate:test',
        aggregateId: '99',
        timestamp: new Date(100),
        payload: {
          assignedTo: '88',
          changedBy: '99',
        },
      });

      await expect(fn).rejects.toThrow(ReservationNotFoundError);
    });
  });

  describe('consuming resource added', () => {
    it('should add a resource to the reservation', async () => {
      await repository.save({
        id: '99',
        workspaceId: 'test',
        dateStart: new Date(200),
        dateEnd: new Date(300),
        state: ReservationState.requested,
        resourceIds: ['1', '2', '3'],
        userId: '',
        createdAt: new Date(100),
      });

      await ingestor.consumeResourceAdded({
        type: ReservationEventType.resourceAdded,
        rowId: 'ReservationAggregate:test',
        aggregateId: '99',
        timestamp: new Date(100),
        payload: {
          resource: '99',
          changedBy: '99',
        },
      });

      const reservation = await repository.getById('test', '99');
      expect(reservation).toEqual({
        id: '99',
        workspaceId: 'test',
        dateStart: new Date(200),
        dateEnd: new Date(300),
        state: ReservationState.requested,
        resourceIds: ['1', '2', '3', '99'],
        userId: '',
        createdAt: new Date(100),
      });
    });
  });

  describe('consuming resource removed', () => {
    it('should add a resource to the reservation', async () => {
      await repository.save({
        id: '99',
        workspaceId: 'test',
        dateStart: new Date(200),
        dateEnd: new Date(300),
        state: ReservationState.requested,
        resourceIds: ['1', '99', '3'],
        userId: '',
        createdAt: new Date(100),
      });

      await ingestor.consumeResourceRemoved({
        type: ReservationEventType.resourceRemoved,
        rowId: 'ReservationAggregate:test',
        aggregateId: '99',
        timestamp: new Date(100),
        payload: {
          resource: '99',
          changedBy: '99',
        },
      });

      const reservation = await repository.getById('test', '99');
      expect(reservation).toEqual({
        id: '99',
        workspaceId: 'test',
        dateStart: new Date(200),
        dateEnd: new Date(300),
        state: ReservationState.requested,
        resourceIds: ['1', '3'],
        userId: '',
        createdAt: new Date(100),
      });
    });
  });

  describe('consuming state update', () => {
    async function effect_insertProjection() {
      await repository.save({
        id: '99',
        workspaceId: 'test',
        dateStart: new Date(200),
        dateEnd: new Date(300),
        state: ReservationState.requested,
        resourceIds: [],
        userId: '',
        createdAt: new Date(100),
      });
    }

    it('should update reservation state to confirmed', async () => {
      await effect_insertProjection();

      await ingestor.consumeConfirmed({
        type: ReservationEventType.confirmed,
        rowId: 'ReservationAggregate:test',
        aggregateId: '99',
        timestamp: new Date(100),
        payload: {
          changedBy: '99',
        },
      });

      const reservation = await repository.getById('test', '99');
      expect(reservation.state).toBe(ReservationState.confirmed);
    });

    it('should update reservation state to rejected', async () => {
      await effect_insertProjection();

      await ingestor.consumeRejected({
        type: ReservationEventType.rejected,
        rowId: 'ReservationAggregate:test',
        aggregateId: '99',
        timestamp: new Date(100),
        payload: {
          reason: 'some reason',
          changedBy: '99',
        },
      });

      const reservation = await repository.getById('test', '99');
      expect(reservation.state).toBe(ReservationState.rejected);
    });

    it('should update reservation state to canceled', async () => {
      await effect_insertProjection();

      await ingestor.consumeCanceled({
        type: ReservationEventType.canceled,
        rowId: 'ReservationAggregate:test',
        aggregateId: '99',
        timestamp: new Date(100),
        payload: {
          changedBy: '99',
        },
      });

      const reservation = await repository.getById('test', '99');
      expect(reservation.state).toBe(ReservationState.canceled);
    });

    it('should update reservation state to completed', async () => {
      await effect_insertProjection();

      await ingestor.consumeCompleted({
        type: ReservationEventType.completed,
        rowId: 'ReservationAggregate:test',
        aggregateId: '99',
        timestamp: new Date(100),
        payload: {},
      });

      const reservation = await repository.getById('test', '99');
      expect(reservation.state).toBe(ReservationState.completed);
    });
  });
});
