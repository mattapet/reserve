/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { CommentAuthor } from '../../domain/reservation-comment/comment-author.entity';
import { CommentAuthorId } from '../../domain/reservation-comment/values';
import { InsufficientRightsError } from '../../domain/reservation-comment/reservation-comment.errors';
import { UserRole, User, Identity } from '../../../user-access';
//#endregion

describe('comment-author.entity', () => {
  it('should create a comment author from maintainer', () => {
    const user = new User(
      '99',
      'test',
      new Identity('99', 'test', 'test@test.com'),
      UserRole.maintainer,
    );

    const author = CommentAuthor.from(user);

    expect(author).toEqual(new CommentAuthor(new CommentAuthorId('99')));
  });

  it('should throw InsufficientRightsError if user is trying to become author', () => {
    const user = new User(
      '99',
      'test',
      new Identity('99', 'test', 'test@test.com'),
      UserRole.user,
    );

    const fn = () => CommentAuthor.from(user);

    expect(fn).toThrow(InsufficientRightsError);
  });
});
