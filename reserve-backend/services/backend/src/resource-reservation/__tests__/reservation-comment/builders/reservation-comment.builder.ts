/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';

import { ReservationComment } from '../../../domain/reservation-comment/reservation-comment.entity';
import { CommentAuthor } from '../../../domain/reservation-comment/comment-author.entity';
//#endregion

export class ReservationCommentBuilder {
  private id?: string;
  private workspaceId?: string;
  private reservationId?: string;
  private comment?: string;
  private timestamp: Date = new Date(Date.now());
  private author?: CommentAuthor;

  public withId(id: string): ReservationCommentBuilder {
    this.id = id;
    return this;
  }

  public withWorkspaceId(workspaceId: string): ReservationCommentBuilder {
    this.workspaceId = workspaceId;
    return this;
  }

  public withReservationId(reservationId: string): ReservationCommentBuilder {
    this.reservationId = reservationId;
    return this;
  }

  public withComment(comment: string): ReservationCommentBuilder {
    this.comment = comment;
    return this;
  }

  public withTimestamp(timestamp: Date): ReservationCommentBuilder {
    this.timestamp = timestamp;
    return this;
  }

  public withAuthor(author: CommentAuthor): ReservationCommentBuilder {
    this.author = author;
    return this;
  }

  public build(): ReservationComment {
    return new ReservationComment(
      notNull(this.id, 'id'),
      notNull(this.workspaceId, 'workspaceId'),
      notNull(this.reservationId, 'reservationId'),
      notNull(this.comment, 'comment'),
      this.timestamp,
      notNull(this.author, 'authorId'),
    );
  }
}
