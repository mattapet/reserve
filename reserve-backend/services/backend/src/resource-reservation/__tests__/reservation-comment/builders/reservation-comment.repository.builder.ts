/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';
import { EventRepository } from '@rsaas/commons/lib/events/event.repository';
import { InMemoryEventRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-event-repository';
import { InMemorySnapshotRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-snapshot.repository';
import { SnapshotRepository } from '@rsaas/commons/lib/events/snapshot.repository';

import { ReservationCommentEvent } from '../../../domain/reservation-comment/reservation-comment.event';
import { ReservationCommentRepository } from '../../../application/reservation-comment/reservation-comment.repository';
import {
  ReservationCommentCommandHandler,
  ReservationCommentEventHandler,
} from '../../../domain/reservation-comment/handlers';
//#endregion

export class ReservationCommentRepositoryBuilder {
  private eventRepository?: EventRepository<ReservationCommentEvent>;
  private snapshotRepository?: SnapshotRepository;

  public withEventRepository(
    eventRepository: EventRepository<ReservationCommentEvent>,
  ): ReservationCommentRepositoryBuilder {
    this.eventRepository = eventRepository;
    return this;
  }

  public withSnapshotRepository(
    snapshotRepository: SnapshotRepository,
  ): ReservationCommentRepositoryBuilder {
    this.snapshotRepository = snapshotRepository;
    return this;
  }

  public build(): ReservationCommentRepository {
    return new ReservationCommentRepository(
      notNull(this.eventRepository, 'eventRepository'),
      notNull(this.snapshotRepository, 'snapshotRepository'),
      new ReservationCommentCommandHandler(),
      new ReservationCommentEventHandler(),
    );
  }

  public static inMemory(): ReservationCommentRepository {
    const eventsRepo = new InMemoryEventRepository<ReservationCommentEvent>();
    return new ReservationCommentRepositoryBuilder()
      .withEventRepository(eventsRepo)
      .withSnapshotRepository(new InMemorySnapshotRepository())
      .build();
  }
}
