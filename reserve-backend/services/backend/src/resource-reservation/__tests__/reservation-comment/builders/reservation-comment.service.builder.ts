/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';

import { ReservationCommentRepository } from '../../../application/reservation-comment/reservation-comment.repository';
import { ReservationCommentRepositoryBuilder } from './reservation-comment.repository.builder';
import { ReservationCommentService } from '../../../domain/reservation-comment/reservation-comment.service';
//#endregion

export class ReservationCommentServiceBuilder {
  private idenityRepository?: ReservationCommentRepository;

  public withRepository(
    idenityRepository: ReservationCommentRepository,
  ): ReservationCommentServiceBuilder {
    this.idenityRepository = idenityRepository;
    return this;
  }

  public buildingRepository(
    buildUsing: (
      builder: ReservationCommentRepositoryBuilder,
    ) => ReservationCommentRepository,
  ): ReservationCommentServiceBuilder {
    return this.withRepository(
      buildUsing(new ReservationCommentRepositoryBuilder()),
    );
  }

  public build(): ReservationCommentService {
    return new ReservationCommentService(
      notNull(this.idenityRepository, 'identityRepository'),
    );
  }

  public static inMemory(): ReservationCommentService {
    return new ReservationCommentServiceBuilder()
      .withRepository(ReservationCommentRepositoryBuilder.inMemory())
      .build();
  }
}
