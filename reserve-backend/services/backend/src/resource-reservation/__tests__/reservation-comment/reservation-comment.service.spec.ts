/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationCommentService } from '../../domain/reservation-comment/reservation-comment.service';

import { ReservationCommentRepository } from '../../application/reservation-comment/reservation-comment.repository';
import { ReservationCommentCommandType } from '../../domain/reservation-comment/reservation-comment.command';
import { ReservationComment } from '../../domain/reservation-comment/reservation-comment.entity';
import {
  ReservationCommentNotFoundError,
  InsufficientRightsError,
} from '../../domain/reservation-comment/reservation-comment.errors';

import { ReservationCommentRepositoryBuilder } from './builders/reservation-comment.repository.builder';
import { ReservationCommentServiceBuilder } from './builders/reservation-comment.service.builder';
import { ReservationCommentBuilder } from './builders/reservation-comment.builder';
import { CommentAuthor } from '../../domain/reservation-comment/comment-author.entity';
import { CommentAuthorId } from '../../domain/reservation-comment/values';
import { Reservation } from '../../domain/reservation/entities/reservation.entity';
//#endregion

describe('reservation-comment.service', () => {
  let repository!: ReservationCommentRepository;
  let service!: ReservationCommentService;

  beforeEach(() => {
    repository = ReservationCommentRepositoryBuilder.inMemory();
    service = new ReservationCommentServiceBuilder()
      .withRepository(repository)
      .build();
  });

  async function effect_createCommentWithAuthor(
    comment: ReservationComment,
    author: CommentAuthor,
  ) {
    const createArgs = {
      id: '99',
      reservation: new Reservation('49', 'test'),
      comment: 'some random comment',
      author,
    };
    await service.create(comment, createArgs);
  }

  describe('#getByReservationId()', () => {
    it('should return all reservation comments', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      await service.create(new ReservationComment(), {
        id: '99',
        reservation: new Reservation('49', 'test'),
        comment: 'some random comment',
        author: new CommentAuthor(new CommentAuthorId('44')),
      });

      const result = await service.getByReservation(
        new Reservation('49', 'test'),
      );

      expect(result).toEqual([
        new ReservationCommentBuilder()
          .withId('99')
          .withWorkspaceId('test')
          .withReservationId('49')
          .withComment('some random comment')
          .withAuthor(new CommentAuthor(new CommentAuthorId('44')))
          .build(),
      ]);
    });

    it('should omit deleted reservation comments', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const comment = new ReservationComment();
      const author = new CommentAuthor(new CommentAuthorId('44'));
      await service.create(comment, {
        id: '99',
        reservation: new Reservation('49', 'test'),
        comment: 'some random comment',
        author,
      });
      await service.delete(comment, author);

      const result = await service.getByReservation(
        new Reservation('49', 'test'),
      );

      expect(result).toEqual([]);
    });

    it('should throw ReservationCommentNotFoundError if comment has not been created yet', async () => {
      const fn = service.getById(new Reservation('test', '99'), '88');

      await expect(fn).rejects.toThrow(ReservationCommentNotFoundError);
    });
  });

  describe('#getById()', () => {
    it('should get comment from repository', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const author = new CommentAuthor(new CommentAuthorId('44'));
      await effect_createCommentWithAuthor(new ReservationComment(), author);

      const result = await service.getById(new Reservation('49', 'test'), '99');

      expect(result).toEqual(
        new ReservationCommentBuilder()
          .withId('99')
          .withWorkspaceId('test')
          .withReservationId('49')
          .withComment('some random comment')
          .withAuthor(new CommentAuthor(new CommentAuthorId('44')))
          .build(),
      );
    });
  });

  describe('creating a comment', () => {
    it('should execute a `create` command', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const execute = jest.spyOn(repository, 'execute');
      const comment = new ReservationComment();
      const args = {
        id: '99',
        reservation: new Reservation('49', 'test'),
        comment: 'some random comment',
        author: new CommentAuthor(new CommentAuthorId('44')),
      };

      await service.create(comment, args);

      expect(execute).toHaveBeenCalledWith(
        {
          type: ReservationCommentCommandType.create,
          rowId: `${service.aggregateName}:test:49`,
          aggregateId: '99',
          payload: {
            workspaceId: 'test',
            reservationId: '49',
            author: new CommentAuthor(new CommentAuthorId('44')),
            comment: 'some random comment',
          },
        },
        comment,
      );
    });
  });

  describe('editing an existing comment', () => {
    it('should execute a `edit` command', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const comment = new ReservationComment();
      const author = new CommentAuthor(new CommentAuthorId('44'));
      await effect_createCommentWithAuthor(comment, author);
      const execute = jest.spyOn(repository, 'execute');

      await service.edit(comment, 'some other comment', author);

      expect(execute).toHaveBeenCalledWith(
        {
          type: ReservationCommentCommandType.edit,
          rowId: `${service.aggregateName}:test:49`,
          aggregateId: '99',
          payload: {
            comment: 'some other comment',
            changedBy: new CommentAuthor(new CommentAuthorId('44')),
          },
        },
        comment,
      );
    });

    it('should edit the comment', async () => {
      const comment = new ReservationComment();
      const author = new CommentAuthor(new CommentAuthorId('44'));
      await effect_createCommentWithAuthor(comment, author);

      await service.edit(comment, 'new comment', author);

      expect(comment.getComment()).toBe('new comment');
    });

    it(`should throw an error when editing someone elses comment`, async () => {
      const comment = new ReservationComment();
      const author = new CommentAuthor(new CommentAuthorId('44'));
      await effect_createCommentWithAuthor(comment, author);

      await expect(
        service.edit(
          comment,
          'some other comment',
          new CommentAuthor(new CommentAuthorId('77')),
        ),
      ).rejects.toThrow(InsufficientRightsError);
    });
  });

  describe('deleting comments', () => {
    it('should delete the comment', async () => {
      const comment = new ReservationComment();
      const author = new CommentAuthor(new CommentAuthorId('44'));
      await effect_createCommentWithAuthor(comment, author);

      await service.delete(comment, author);

      expect(comment.isDeleted()).toBe(true);
      expect(comment.getComment()).toBe('');
    });

    it(`should throw an error when deleting someone else comment`, async () => {
      const comment = new ReservationComment();
      const author = new CommentAuthor(new CommentAuthorId('44'));
      await effect_createCommentWithAuthor(comment, author);

      await expect(
        service.delete(
          new ReservationCommentBuilder()
            .withId('99')
            .withWorkspaceId('test')
            .withReservationId('49')
            .withComment('some random comment')
            .withAuthor(new CommentAuthor(new CommentAuthorId('44')))
            .build(),
          new CommentAuthor(new CommentAuthorId('77')),
        ),
      ).rejects.toThrow(InsufficientRightsError);
    });
  });
});
