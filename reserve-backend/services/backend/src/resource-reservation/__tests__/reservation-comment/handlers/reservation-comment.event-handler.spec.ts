/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationCommentEventHandler } from '../../../domain/reservation-comment/handlers/reservation-comment.event-handler';
import {
  ReservationCommentEventType,
  ReservationCommentEvent,
} from '../../../domain/reservation-comment/reservation-comment.event';
import { ReservationComment } from '../../../domain/reservation-comment/reservation-comment.entity';
import { CommentAuthor } from '../../../domain/reservation-comment/comment-author.entity';
import { CommentAuthorId } from '../../../domain/reservation-comment/values';
//#endregion

describe('reservation-comment.event-handler', () => {
  const reservationCommentEventHandler = new ReservationCommentEventHandler();

  it('should correctly populate all fields', () => {
    jest.spyOn(Date, 'now').mockImplementation(() => 100);
    const event: ReservationCommentEvent = {
      type: ReservationCommentEventType.created,
      rowId: 'test:88',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        workspaceId: 'test',
        reservationId: '88',
        author: '55',
        comment: 'test comment',
      },
    };

    const aggregate = reservationCommentEventHandler.apply(
      new ReservationComment(),
      [event],
    );

    expect(aggregate).toEqual(
      new ReservationComment(
        '99',
        'test',
        '88',
        'test comment',
        new Date(100),
        new CommentAuthor(new CommentAuthorId('55')),
      ),
    );
  });

  it('should correctly update aggregate', () => {
    const event: ReservationCommentEvent = {
      type: ReservationCommentEventType.edited,
      rowId: 'test:88',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        changedBy: '55',
        comment: 'new test comment',
      },
    };
    const comment = new ReservationComment(
      '99',
      'test',
      '88',
      'test comment',
      new Date(100),
      new CommentAuthor(new CommentAuthorId('55')),
    );

    const aggregate = reservationCommentEventHandler.apply(comment, [event]);

    expect(aggregate.getComment()).toBe('new test comment');
  });

  it('should set aggregate to deleted', () => {
    const event: ReservationCommentEvent = {
      type: ReservationCommentEventType.deleted,
      rowId: 'test:88',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        deletedBy: '55',
      },
    };
    const comment = new ReservationComment(
      '99',
      'test',
      '88',
      'test comment',
      new Date(100),
      new CommentAuthor(new CommentAuthorId('55')),
    );

    const aggregate = reservationCommentEventHandler.apply(comment, [event]);

    expect(aggregate.isDeleted()).toBe(true);
    expect(aggregate.getComment()).toBe('');
  });
});
