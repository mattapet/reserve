/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationCommentCommandHandler } from '../../../domain/reservation-comment/handlers/reservation-comment.command-handler';
import {
  ReservationCommentCommandType,
  ReservationCommentCommand,
} from '../../../domain/reservation-comment/reservation-comment.command';
import { ReservationCommentEventType } from '../../../domain/reservation-comment/reservation-comment.event';
import { ReservationComment } from '../../../domain/reservation-comment/reservation-comment.entity';
import { CommentAuthorId } from '../../../domain/reservation-comment/values';
import { CommentAuthor } from '../../../domain/reservation-comment/comment-author.entity';
//#endregion

describe('reservation-comment.command-handler', () => {
  // tslint:disable-next-line:max-line-length
  const reservationCommentCommandHandler = new ReservationCommentCommandHandler();

  function mock_dateNow(now: number) {
    jest.spyOn(Date, 'now').mockImplementation(() => now);
  }

  describe('`create` command', () => {
    it('should produce a `created` event', () => {
      mock_dateNow(100);
      const command: ReservationCommentCommand = {
        type: ReservationCommentCommandType.create,
        rowId: 'test:88',
        aggregateId: '99',
        payload: {
          workspaceId: 'test',
          reservationId: '88',
          author: new CommentAuthor(new CommentAuthorId('44')),
          comment: 'some command',
        },
      };

      const result = reservationCommentCommandHandler.execute(
        new ReservationComment(),
        command,
      );

      expect(result).toEqual([
        {
          type: ReservationCommentEventType.created,
          rowId: 'test:88',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            workspaceId: 'test',
            reservationId: '88',
            author: '44',
            comment: 'some command',
          },
        },
      ]);
    });
  });

  describe('`edit` command', () => {
    it('should produce a `edited` event', () => {
      mock_dateNow(100);
      const command: ReservationCommentCommand = {
        type: ReservationCommentCommandType.edit,
        rowId: 'test:88',
        aggregateId: '99',
        payload: {
          changedBy: new CommentAuthor(new CommentAuthorId('55')),
          comment: 'modified comment',
        },
      };
      const comment = new ReservationComment(
        '99',
        'test',
        '88',
        'test comment',
        new Date(100),
        new CommentAuthor(new CommentAuthorId('55')),
      );

      const result = reservationCommentCommandHandler.execute(comment, command);

      expect(result).toEqual([
        {
          type: ReservationCommentEventType.edited,
          rowId: 'test:88',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            comment: 'modified comment',
            changedBy: '55',
          },
        },
      ]);
    });

    it('should throw an error when deleting already deleted comment', () => {
      mock_dateNow(100);
      const command: ReservationCommentCommand = {
        type: ReservationCommentCommandType.edit,
        rowId: 'test:88',
        aggregateId: '99',
        payload: {
          changedBy: new CommentAuthor(new CommentAuthorId('55')),
          comment: 'modified comment',
        },
      };
      const comment = new ReservationComment(
        '99',
        'test',
        '88',
        'test comment',
        new Date(100),
        new CommentAuthor(new CommentAuthorId('55')),
      );
      comment.delete();

      const fn = () =>
        reservationCommentCommandHandler.execute(comment, command);

      expect(fn).toThrow(new Error('Cannot edit deleted comment.'));
    });
  });

  describe('`delete` command', () => {
    it('should produce a `deleted` event', () => {
      mock_dateNow(100);
      const command: ReservationCommentCommand = {
        type: ReservationCommentCommandType.delete,
        rowId: 'test:88',
        aggregateId: '99',
        payload: {
          deletedBy: new CommentAuthor(new CommentAuthorId('55')),
        },
      };
      const comment = new ReservationComment(
        '99',
        'test',
        '88',
        'test comment',
        new Date(100),
        new CommentAuthor(new CommentAuthorId('55')),
      );

      const result = reservationCommentCommandHandler.execute(comment, command);

      expect(result).toEqual([
        {
          type: ReservationCommentEventType.deleted,
          rowId: 'test:88',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            deletedBy: '55',
          },
        },
      ]);
    });

    it('should not produce any events when deleting already deleted event', () => {
      mock_dateNow(100);
      const command: ReservationCommentCommand = {
        type: ReservationCommentCommandType.delete,
        rowId: 'test:88',
        aggregateId: '99',
        payload: {
          deletedBy: new CommentAuthor(new CommentAuthorId('55')),
        },
      };
      const comment = new ReservationComment(
        '99',
        'test',
        '88',
        'test comment',
        new Date(100),
        new CommentAuthor(new CommentAuthorId('55')),
      );
      comment.delete();

      const result = reservationCommentCommandHandler.execute(comment, command);

      expect(result).toEqual([]);
    });
  });
});
