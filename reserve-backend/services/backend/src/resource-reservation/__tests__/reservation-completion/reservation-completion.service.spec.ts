/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationCompletionService } from '../../domain/reservation-completion/reservation-completion.service';

import { ReservationState } from '../../domain/reservation/reservation-state.type';
import { ReservationService } from '../../domain/reservation/reservation.service';
import { ReservationBuilder } from '../reservation/builders/reservation.builder';
import { Reservation } from '../../domain/reservation/entities/reservation.entity';
import { CreateReservation } from '../../domain/reservation/values';
import {
  Assignee,
  ReservationManager,
} from '../../domain/reservation/entities';

import { ReservationRepositoryBuilder } from '../reservation/builders/reservation.repository.builder';
import { ReservationServiceBuilder } from '../reservation/builders/reservation.service.builder';
import { ReservationProjectionRepository } from '../../domain/reservation-projection';
import { InMemoryReservationProjectionRepository } from '../../application/reservation-projection/repositories/in-memory/in-memory-reservation-projection.repository';

import { UserRole } from '../../../user-access';
//#endregion

describe('reservation-completion.service', () => {
  let searchRepo!: ReservationProjectionRepository;
  let reservationService!: ReservationService;
  let completionService!: ReservationCompletionService;

  beforeEach(async () => {
    searchRepo = new InMemoryReservationProjectionRepository();
    const reservationRepository = ReservationRepositoryBuilder.inMemory();
    reservationService = new ReservationServiceBuilder()
      .withRepository(reservationRepository)
      .withSearchRepo(searchRepo)
      .build();
    completionService = new ReservationCompletionService(
      reservationService,
      searchRepo,
    );
  });

  function make_defaultReservationBuilder(): ReservationBuilder {
    return new ReservationBuilder()
      .withId('99')
      .withWorkspaceId('test')
      .withDateStart(new Date(Date.now() + 100_000))
      .withDateEnd(new Date(Date.now() + 200_000))
      .withResourceIds(['1', '2', '3'])
      .withAssignee(new Assignee('99'))
      .withCreatedAt(new Date(0))
      .withNotes('additional notes');
  }

  function make_createParams(
    id: string,
    assignee: Assignee,
    createdBy: ReservationManager,
  ): CreateReservation {
    return new CreateReservation(
      id,
      'test',
      new Date(100),
      new Date(200),
      ['1', '2', '3'],
      'additional notes',
      assignee,
      createdBy,
    );
  }

  async function effect_createReservation(
    reservation: Reservation,
    params: CreateReservation,
  ) {
    await reservationService.create(reservation, params);
    await searchRepo.save({
      id: reservation.getId(),
      workspaceId: reservation.getWorkspaceId(),
      dateStart: reservation.getDateStart(),
      dateEnd: reservation.getDateEnd(),
      state: reservation.getState(),
      userId: reservation.getAssignee().getId(),
      notes: reservation.getNotes(),
      resourceIds: reservation.getResourceIds(),
      createdAt: reservation.getCreatedAt(),
    });
  }

  async function effect_confirmReservation(
    reservation: Reservation,
    confirmedBy: ReservationManager,
  ) {
    await reservationService.confirm(reservation, confirmedBy);
    const item = await searchRepo.getById(
      reservation.getWorkspaceId(),
      reservation.getId(),
    );
    item.state = ReservationState.confirmed;
    await searchRepo.save(item);
  }

  describe('having uncomplete reservations', () => {
    it('should complete uncompelte reservations', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const assignee = new Assignee('99');
      const changedBy = new ReservationManager('99', UserRole.maintainer);
      const reservation = make_defaultReservationBuilder()
        .withId('99')
        .withDateStart(new Date(100))
        .withDateEnd(new Date(200))
        .withAssignee(assignee)
        .build();
      await effect_createReservation(
        reservation,
        make_createParams('99', assignee, changedBy),
      );
      await effect_confirmReservation(reservation, changedBy);

      jest.spyOn(Date, 'now').mockImplementation(() => 1000);

      await completionService.completeReservations();

      const result = await reservationService.getById('test', '99');
      expect(result.getState()).toBe(ReservationState.completed);
    });

    it('should not complete no confirmed reservations', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const assignee = new Assignee('99');
      const changedBy = new ReservationManager('99', UserRole.maintainer);
      const reservation = make_defaultReservationBuilder()
        .withId('99')
        .withDateStart(new Date(100))
        .withDateEnd(new Date(200))
        .withAssignee(assignee)
        .build();
      await effect_createReservation(
        reservation,
        make_createParams('99', assignee, changedBy),
      );
      jest.spyOn(Date, 'now').mockImplementation(() => 100000);

      await completionService.completeReservations();

      const result = await reservationService.getById('test', '99');
      expect(result.getState()).toBe(ReservationState.requested);
    });
  });
});
