/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationService } from '../../domain/reservation/reservation.service';

import {
  ReservationCommandType,
  ReservationCommand,
} from '../../domain/reservation/reservation.command';
import {
  InvalidDateRangeError,
  ReservationNotFoundError,
  InsufficientRightsError,
} from '../../domain/reservation/reservation.errors';
import { ReservationBuilder } from './builders/reservation.builder';
import { Reservation } from '../../domain/reservation/entities/reservation.entity';
import { ReservationEventType } from '../../domain/reservation/reservation.event';
import { ReservationServiceBuilder } from './builders/reservation.service.builder';
import { ReservationRepository } from '../../application/reservation/reservation.repository';
import { ReservationRepositoryBuilder } from './builders/reservation.repository.builder';
import {
  UpdateReservation,
  CreateReservation,
} from '../../domain/reservation/values';
import { InMemoryReservationProjectionRepository } from '../../application/reservation-projection/repositories/in-memory/in-memory-reservation-projection.repository';
import {
  Assignee,
  ReservationManager,
} from '../../domain/reservation/entities';
import { UserRole } from '../../../user-access';
//#endregion

describe('reservation.service', () => {
  let reservationService!: ReservationService;
  let reservationRepository!: ReservationRepository;

  beforeEach(async () => {
    const searchService = new InMemoryReservationProjectionRepository();
    reservationRepository = ReservationRepositoryBuilder.inMemory();
    reservationService = new ReservationServiceBuilder()
      .withRepository(reservationRepository)
      .withSearchRepo(searchService)
      .build();
  });

  function make_defaultReservationBuilder(): ReservationBuilder {
    return new ReservationBuilder()
      .withId('99')
      .withWorkspaceId('test')
      .withDateStart(new Date(Date.now() + 100_000))
      .withDateEnd(new Date(Date.now() + 200_000))
      .withResourceIds(['1', '2', '3'])
      .withAssignee(new Assignee('99'))
      .withNotes('additional notes')
      .withCreatedAt(new Date(100));
  }

  function make_createParams(
    id: string,
    assignee: Assignee,
    createdBy: ReservationManager,
  ): CreateReservation {
    return new CreateReservation(
      id,
      'test',
      new Date(Date.now() + 100_000),
      new Date(Date.now() + 200_000),
      ['1', '2', '3'],
      'additional notes',
      assignee,
      createdBy,
    );
  }

  function make_updateParams(
    assignee: Assignee,
    updatedBy: ReservationManager,
  ): UpdateReservation {
    return new UpdateReservation(
      'test',
      new Date(Date.now() + 100_000),
      new Date(Date.now() + 200_000),
      ['1', '2', '3'],
      'additional notes',
      assignee,
      updatedBy,
    );
  }

  function make_reservationWithAssignee(assignee: Assignee): Reservation {
    return make_defaultReservationBuilder().withAssignee(assignee).build();
  }

  describe('#getAllEvents()', () => {
    it('should return an empty list if no events have been processed', async () => {
      const result = await reservationService.getAllEvents('test', '99');

      expect(result).toEqual([]);
    });

    it('should return created, assigned to and resources added events', async () => {
      const assignee = new Assignee('99');
      const createdBy = new ReservationManager('99', UserRole.user);
      const reservation = new Reservation();
      const newReservation = make_createParams('99', assignee, createdBy);
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      await reservationService.create(reservation, newReservation);

      const result = await reservationService.getAllEvents('test', '99');

      const commonEvent = {
        rowId: `${reservationService.aggregateName}:test`,
        aggregateId: '99',
        timestamp: new Date(100),
      };
      expect(result).toEqual([
        {
          type: ReservationEventType.created,
          eventId: 1,
          ...commonEvent,
          payload: {
            workspaceId: 'test',
            dateStart: newReservation.dateStart.toISOString(),
            dateEnd: newReservation.dateEnd.toISOString(),
            createdBy: '99',
            notes: newReservation.notes,
          },
        },
        {
          type: ReservationEventType.assigned,
          eventId: 2,
          ...commonEvent,
          payload: {
            assignedTo: '99',
            changedBy: '99',
          },
        },
        {
          type: ReservationEventType.resourceAdded,
          eventId: 3,
          ...commonEvent,
          payload: {
            resource: '1',
            changedBy: '99',
          },
        },
        {
          type: ReservationEventType.resourceAdded,
          eventId: 4,
          ...commonEvent,
          payload: {
            resource: '2',
            changedBy: '99',
          },
        },
        {
          type: ReservationEventType.resourceAdded,
          eventId: 5,
          ...commonEvent,
          payload: {
            resource: '3',
            changedBy: '99',
          },
        },
      ]);
    });
  });

  describe('retrieving reservations', () => {
    it('shoudl throw ReservationNotFoundError if reservation has not been created yet', async () => {
      const fn = reservationService.getById('test', '99');

      await expect(fn).rejects.toThrow(ReservationNotFoundError);
    });
  });

  describe('#create()', () => {
    it('should execute a `create` command', async () => {
      const assignee = new Assignee('99');
      const createdBy = new ReservationManager('99', UserRole.user);
      const reservation = new Reservation();
      const newReservation = make_createParams('99', assignee, createdBy);
      const execute = jest.spyOn(reservationRepository, 'execute');

      await reservationService.create(reservation, newReservation);

      expect(execute).toHaveBeenCalledWith(
        {
          type: ReservationCommandType.create,
          rowId: `${reservationService.aggregateName}:test`,
          aggregateId: '99',
          payload: {
            workspaceId: newReservation.workspaceId,
            dateStart: newReservation.dateStart,
            dateEnd: newReservation.dateEnd,
            assignee: new Assignee('99'),
            createdBy,
            resources: newReservation.resources,
            notes: newReservation.notes,
          },
        },
        reservation,
      );
    });

    it('should create a new reservation', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const assignee = new Assignee('99');
      const createdBy = new ReservationManager('99', UserRole.user);
      const reservation = new Reservation();
      const newReservation = make_createParams('99', assignee, createdBy);

      await reservationService.create(reservation, newReservation);

      expect(reservation).toEqual(
        make_defaultReservationBuilder().withAssignee(assignee).build(),
      );
    });

    it('should throw InsufficientRightsError when trying to assign to someone else', async () => {
      const assignee = new Assignee('99');
      const createdBy = new ReservationManager('88', UserRole.user);
      const reservation = new Reservation();
      const newReservation = make_createParams('99', assignee, createdBy);

      const fn = reservationService.create(reservation, newReservation);

      await expect(fn).rejects.toThrow(InsufficientRightsError);
    });
  });

  describe('#editReservation()', () => {
    function make_editCommandWithAssigneeAndChangedBy(
      reservation: Reservation,
      assignee: Assignee,
      changedBy: ReservationManager,
    ): ReservationCommand {
      return {
        type: ReservationCommandType.edit,
        rowId: `${reservationService.aggregateName}:test`,
        aggregateId: reservation.getId(),
        payload: {
          dateStart: reservation.getDateStart(),
          dateEnd: reservation.getDateEnd(),
          resources: reservation.getResourceIds(),
          notes: reservation.getNotes(),
          assignee,
          changedBy,
        },
      };
    }

    it('should produce a `edit` command when editing your own reservation', async () => {
      const assignee = new Assignee('88');
      const changedBy = new ReservationManager('88', UserRole.user);
      const reservation = make_reservationWithAssignee(assignee);
      const newReservation = make_updateParams(assignee, changedBy);
      const execute = jest.spyOn(reservationRepository, 'execute');

      await reservationService.editReservation(reservation, newReservation);

      expect(execute).toHaveBeenCalledWith(
        make_editCommandWithAssigneeAndChangedBy(
          reservation,
          assignee,
          changedBy,
        ),
        reservation,
      );
    });

    it('should maintainer edits someone elses reservation', async () => {
      const assignee = new Assignee('99');
      const changedBy = new ReservationManager('88', UserRole.maintainer);
      const reservation = make_reservationWithAssignee(assignee);
      const newReservation = make_updateParams(assignee, changedBy);
      const execute = jest.spyOn(reservationRepository, 'execute');

      await reservationService.editReservation(reservation, newReservation);

      expect(execute).toHaveBeenCalledWith(
        make_editCommandWithAssigneeAndChangedBy(
          reservation,
          assignee,
          changedBy,
        ),
        reservation,
      );
    });

    it('should throw an InvalidDateRangeError if `dateStart` is in the past', async () => {
      const assignee = new Assignee('99');
      const changedBy = new ReservationManager('99', UserRole.admin);
      const reservation = make_defaultReservationBuilder()
        .withDateStart(new Date(0))
        .build();
      const newReservation = make_updateParams(assignee, changedBy);

      const fn = reservationService.editReservation(
        reservation,
        newReservation,
      );

      await expect(fn).rejects.toThrow(InvalidDateRangeError);
    });

    it('should throw an InsufficientRightsError if non-maintainer tries to edit', async () => {
      const assignee = new Assignee('99');
      const reservation = make_defaultReservationBuilder().build();
      const changedBy = new ReservationManager('88', UserRole.user);
      const newReservation = make_updateParams(assignee, changedBy);

      const fn = reservationService.editReservation(
        reservation,
        newReservation,
      );

      await expect(fn).rejects.toThrow(InsufficientRightsError);
    });

    it('should throw an InsufficientRightsError if non-maintainer tries to reassign', async () => {
      const assignee = new Assignee('99');
      const changedBy = new ReservationManager('88', UserRole.user);
      const originalAssignee = new Assignee('44');
      const reservation = make_reservationWithAssignee(originalAssignee);
      const newReservation = make_updateParams(assignee, changedBy);

      const fn = reservationService.editReservation(
        reservation,
        newReservation,
      );

      await expect(fn).rejects.toThrow(InsufficientRightsError);
    });

    it('should throw an InsufficientRightsError if non-maintainer tries to reassign their own reservation', async () => {
      const assignee = new Assignee('99');
      const originalAssignee = new Assignee('88');
      const changedBy = new ReservationManager('88', UserRole.user);
      const reservation = make_reservationWithAssignee(originalAssignee);
      const newReservation = make_updateParams(assignee, changedBy);

      const fn = reservationService.editReservation(
        reservation,
        newReservation,
      );

      await expect(fn).rejects.toThrow(InsufficientRightsError);
    });
  });
});
