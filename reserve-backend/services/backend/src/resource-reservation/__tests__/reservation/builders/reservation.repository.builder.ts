/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';

import { EventRepository } from '@rsaas/commons/lib/events/event.repository';
import { InMemoryEventRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-event-repository';
import { InMemorySnapshotRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-snapshot.repository';
import { SnapshotRepository } from '@rsaas/commons/lib/events/snapshot.repository';

import { ReservationEvent } from '../../../domain/reservation/reservation.event';
import {
  ReservationCommandHandler,
  ReservationEventHandler,
} from '../../../domain/reservation/handlers';

import { ReservationRepository } from '../../../application/reservation/reservation.repository';
//#endregion

export class ReservationRepositoryBuilder {
  private eventRepository?: EventRepository<ReservationEvent>;
  private snapshotRepository?: SnapshotRepository;

  public withEventRepository(
    eventRepository: EventRepository<ReservationEvent>,
  ): ReservationRepositoryBuilder {
    this.eventRepository = eventRepository;
    return this;
  }

  public withSnapshotRepository(
    snapshotRepository: SnapshotRepository,
  ): ReservationRepositoryBuilder {
    this.snapshotRepository = snapshotRepository;
    return this;
  }

  public build(): ReservationRepository {
    return new ReservationRepository(
      notNull(this.eventRepository, 'eventRepository'),
      notNull(this.snapshotRepository, 'snapshotRepository'),
      new ReservationCommandHandler(),
      new ReservationEventHandler(),
    );
  }

  public static inMemory(): ReservationRepository {
    const eventsRepo = new InMemoryEventRepository<ReservationEvent>();
    return new ReservationRepositoryBuilder()
      .withEventRepository(eventsRepo)
      .withSnapshotRepository(new InMemorySnapshotRepository())
      .build();
  }
}
