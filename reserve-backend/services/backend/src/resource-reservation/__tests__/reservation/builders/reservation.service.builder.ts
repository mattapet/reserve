/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';
import { InMemoryEventRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-event-repository';
import { InMemorySnapshotRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-snapshot.repository';

import { ReservationService } from '../../../domain/reservation/reservation.service';
import { ReservationRepository } from '../../../application/reservation/reservation.repository';
import { ReservationEvent } from '../../../domain/reservation/reservation.event';
import { ReservationRepositoryBuilder } from './reservation.repository.builder';
import { ReservationProjectionRepository } from '../../../domain/reservation-projection';
import { InMemoryReservationProjectionRepository } from '../../../application/reservation-projection/repositories/in-memory/in-memory-reservation-projection.repository';
//#endregion

export class ReservationServiceBuilder {
  private repository?: ReservationRepository;
  private searchRepo?: ReservationProjectionRepository;

  public withRepository(
    repository?: ReservationRepository,
  ): ReservationServiceBuilder {
    this.repository = repository;
    return this;
  }

  public buildingRepository(
    buildUsing: (
      builder: ReservationRepositoryBuilder,
    ) => ReservationRepository,
  ): ReservationServiceBuilder {
    return this.withRepository(buildUsing(new ReservationRepositoryBuilder()));
  }

  public withSearchRepo(
    searchService?: ReservationProjectionRepository,
  ): ReservationServiceBuilder {
    this.searchRepo = searchService;
    return this;
  }

  public build(): ReservationService {
    return new ReservationService(
      notNull(this.repository),
      notNull(this.searchRepo),
    );
  }

  public static inMemory(): ReservationService {
    const eventRepo = new InMemoryEventRepository<ReservationEvent>();
    const searchRepo = new InMemoryReservationProjectionRepository();

    return new ReservationServiceBuilder()
      .buildingRepository((builder) =>
        builder
          .withEventRepository(eventRepo)
          .withSnapshotRepository(new InMemorySnapshotRepository())
          .build(),
      )
      .withSearchRepo(searchRepo)
      .build();
  }
}
