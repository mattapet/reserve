/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';

import { ResourceId } from '../../../domain/resource/resource.entity';
import { ReservationState } from '../../../domain/reservation/reservation-state.type';
import { Reservation } from '../../../domain/reservation/entities/reservation.entity';
import { Assignee } from '../../../domain/reservation/entities';
//#endregion

export class ReservationBuilder {
  private id?: string;
  private workspaceId?: string;
  private dateStart?: Date;
  private dateEnd?: Date;
  private assignee?: Assignee;
  private createdAt?: Date;
  private state: ReservationState = ReservationState.requested;
  private resourceIds: ResourceId[] = [];
  private notes: string = '';

  public withId(id?: string): ReservationBuilder {
    this.id = id;
    return this;
  }

  public withWorkspaceId(workspaceId?: string): ReservationBuilder {
    this.workspaceId = workspaceId;
    return this;
  }

  public withDateStart(dateStart?: Date): ReservationBuilder {
    this.dateStart = dateStart;
    return this;
  }

  public withDateEnd(dateEnd?: Date): ReservationBuilder {
    this.dateEnd = dateEnd;
    return this;
  }

  public withAssignee(assignee: Assignee): ReservationBuilder {
    this.assignee = assignee;
    return this;
  }

  public withState(state: ReservationState): ReservationBuilder {
    this.state = state;
    return this;
  }

  public withResourceIds(resourceIds: ResourceId[]): ReservationBuilder {
    this.resourceIds = resourceIds;
    return this;
  }

  public withCreatedAt(createdAt: Date): ReservationBuilder {
    this.createdAt = createdAt;
    return this;
  }

  public withNotes(notes?: string): ReservationBuilder {
    this.notes = notes ?? '';
    return this;
  }

  public static fromReservation(reservation: Reservation): ReservationBuilder {
    return new ReservationBuilder()
      .withId(reservation.getId())
      .withDateStart(reservation.getDateStart())
      .withDateEnd(reservation.getDateEnd())
      .withState(reservation.getState())
      .withResourceIds(reservation.getResourceIds())
      .withAssignee(reservation.getAssignee())
      .withCreatedAt(reservation.getCreatedAt())
      .withNotes(reservation.getNotes());
  }

  public build(): Reservation {
    return new Reservation(
      notNull(this.id),
      notNull(this.workspaceId),
      notNull(this.dateStart),
      notNull(this.dateEnd),
      notNull(this.assignee),
      this.state,
      this.resourceIds,
      notNull(this.createdAt),
      this.notes,
    );
  }
}
