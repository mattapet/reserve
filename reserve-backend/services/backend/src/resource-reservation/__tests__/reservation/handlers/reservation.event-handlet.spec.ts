/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationEventHandler } from '../../../domain/reservation/handlers/reservation.event-handler';
import {
  ReservationEventType,
  ReservationEvent,
} from '../../../domain/reservation/reservation.event';
import { ReservationState } from '../../../domain/reservation/reservation-state.type';
import { Reservation } from '../../../domain/reservation/entities/reservation.entity';
import { Assignee } from '../../../domain/reservation/entities';
//#endregion

describe('reservation.event-handler', () => {
  const reservationEventHandler = new ReservationEventHandler();
  const rowId = 'test';
  const aggregateId = '49';
  const dateStart = new Date(100);
  const dateEnd = new Date(200);
  const timestamp = new Date(0);

  const makeEventCreator = <T extends ReservationEventType>(type: T) => <
    Value extends Record<string, any>
  >(
    value: Value,
  ) => ({
    type,
    rowId,
    aggregateId,
    timestamp,
    payload: value,
  });

  function make_defaultReservation(): Reservation {
    return new Reservation(
      '99',
      'test',
      new Date(0),
      new Date(100),
      new Assignee('88'),
    );
  }

  it('should set all values from a new reservation', () => {
    const event: ReservationEvent = makeEventCreator(
      ReservationEventType.created,
    )({
      workspaceId: 'test',
      dateStart: dateStart.toISOString(),
      dateEnd: dateEnd.toISOString(),
      notes: 'additional notes',
      createdBy: '40',
    });

    const aggregate = reservationEventHandler.apply(new Reservation(), [
      {
        ...event,
        timestamp: new Date(100),
      },
    ]);

    expect(aggregate.getId()).toEqual(aggregateId);
    expect(aggregate.getDateStart()).toEqual(dateStart);
    expect(aggregate.getDateEnd()).toEqual(dateEnd);
    expect(aggregate.getNotes()).toEqual('additional notes');
    expect(aggregate.getCreatedAt()).toEqual(new Date(100));
  });

  it('should set date range', () => {
    const event = makeEventCreator(ReservationEventType.dateChanged)({
      dateStart: dateStart.toISOString(),
      dateEnd: dateEnd.toISOString(),
      changedBy: '40',
    });
    const reservation = make_defaultReservation();

    const aggregate = reservationEventHandler.apply(reservation, [event]);
    expect(aggregate.getDateStart()).toEqual(dateStart);
    expect(aggregate.getDateEnd()).toEqual(dateEnd);
  });

  it('should set assigned user', () => {
    const userId = '22';
    const event = makeEventCreator(ReservationEventType.assigned)({
      assignedTo: userId,
      changedBy: '40',
    });
    const reservation = make_defaultReservation();

    const aggregate = reservationEventHandler.apply(reservation, [event]);
    expect(aggregate.getAssignee()).toEqual(new Assignee(userId));
  });

  it('add a resource to the reservation', () => {
    const event = makeEventCreator(ReservationEventType.resourceAdded)({
      resource: '94',
      changedBy: '40',
    });

    const aggregate = reservationEventHandler.apply(make_defaultReservation(), [
      event,
    ]);
    expect(aggregate.getResourceIds()).toEqual(['94']);
  });

  it('add a resource to the reservation', () => {
    const event = makeEventCreator(ReservationEventType.resourceAdded)({
      resource: '94',
      changedBy: '40',
    });
    const reservation = make_defaultReservation();
    reservation.addResource('1');
    reservation.addResource('2');

    const aggregate = reservationEventHandler.apply(reservation, [event]);
    expect(aggregate.getResourceIds()).toEqual(['1', '2', '94']);
  });

  it('remove a resource to the reservation', () => {
    const event = makeEventCreator(ReservationEventType.resourceRemoved)({
      resource: '94',
      changedBy: '40',
    });
    const reservation = make_defaultReservation();
    reservation.addResource('94');

    const aggregate = reservationEventHandler.apply(reservation, [event]);

    expect(aggregate.getResourceIds()).toEqual([]);
  });

  it('remove a resource to the reservation', () => {
    const event = makeEventCreator(ReservationEventType.resourceRemoved)({
      resource: '94',
      changedBy: '40',
    });
    const reservation = make_defaultReservation();
    reservation.addResource('1');
    reservation.addResource('94');
    reservation.addResource('2');

    const aggregate = reservationEventHandler.apply(reservation, [event]);

    expect(aggregate.getResourceIds()).toEqual(['1', '2']);
  });

  describe('state change', () => {
    it.each`
      eventType                         | state
      ${ReservationEventType.confirmed} | ${ReservationState.confirmed}
      ${ReservationEventType.canceled}  | ${ReservationState.canceled}
      ${ReservationEventType.rejected}  | ${ReservationState.rejected}
      ${ReservationEventType.completed} | ${ReservationState.completed}
    `(
      'should consume $eventType to produce $state state',
      ({ eventType, state }) => {
        const event = {
          type: eventType,
          rowId: 'test',
          aggregateId: '99',
          timestamp: new Date(0),
          payload: {},
        };
        const reservation = make_defaultReservation();

        const result = reservationEventHandler.apply(reservation, [event]);

        expect(result.getState()).toBe(state);
      },
    );
  });
});
