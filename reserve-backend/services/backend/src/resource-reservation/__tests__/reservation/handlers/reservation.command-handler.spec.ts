/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationCommandHandler } from '../../../domain/reservation/handlers/reservation.command-handler';
import {
  ReservationCommandType,
  ReservationCommand,
} from '../../../domain/reservation/reservation.command';
import { ReservationEventType } from '../../../domain/reservation/reservation.event';
import { InvalidReservationStateError } from '../../../domain/reservation/reservation.errors';

import {
  Assignee,
  Reservation,
  ReservationManager,
} from '../../../domain/reservation/entities';

import { UserRole } from '../../../../user-access';
//#endregion

describe('reservation.command-handler', () => {
  const reservationCommandHandler = new ReservationCommandHandler();
  const workspaceId = 'test';
  const aggregateId = '49';

  const makeCommandCreator = <T extends ReservationCommandType>(type: T) => <
    Value extends Record<string, any>
  >(
    value: Value,
  ) => ({
    type,
    aggregateId,
    rowId: workspaceId,
    payload: value,
  });

  describe('create command', () => {
    const dateStart = new Date(0);
    const dateEnd = new Date(0);
    const createdBy = new ReservationManager('44', UserRole.admin);
    const notes = 'additional notes';
    const makeCommand = <Value extends Record<string, any>>(value: Value) =>
      makeCommandCreator(ReservationCommandType.create)({
        ...value,
        createdBy,
      });

    it('should produce `created`, `assigned` and  addedResource` events', () => {
      const assignee = new Assignee('40');
      const resources = ['1', '2', '3'];
      const command = makeCommand({
        workspaceId: 'test',
        dateStart,
        dateEnd,
        resources,
        assignee,
        notes,
      });

      const [
        created,
        assigned,
        ...resourcesAdded
      ] = reservationCommandHandler.execute(new Reservation(), command);

      expect(created.type).toEqual(ReservationEventType.created);
      expect(created.payload).toEqual({
        workspaceId: 'test',
        dateStart: dateStart.toISOString(),
        dateEnd: dateEnd.toISOString(),
        notes,
        createdBy: createdBy.getId(),
      });
      expect(assigned.type).toEqual(ReservationEventType.assigned);
      expect(assigned.payload).toEqual({
        assignedTo: assignee.getId(),
        changedBy: createdBy.getId(),
      });
      resourcesAdded.forEach((resource, idx) => {
        expect(resource.type).toEqual(ReservationEventType.resourceAdded);
        expect(resource.payload).toEqual({
          resource: resources[idx],
          changedBy: createdBy.getId(),
        });
      });
    });
  });

  function make_defaultReservation(): Reservation {
    return new Reservation('99', 'test', new Date(0), new Date(100));
  }

  describe('`edit` command', () => {
    const makeCommand = makeCommandCreator(ReservationCommandType.edit);

    it('should return an empty list if nothing changed', () => {
      const command: ReservationCommand = makeCommand({
        dateStart: new Date(0),
        dateEnd: new Date(100),
        resources: ['1'],
        assignee: new Assignee('44'),
        changedBy: new ReservationManager('44', UserRole.admin),
      });
      const reservation = make_defaultReservation();
      reservation.addResource('1');
      reservation.assignTo(new Assignee('44'));

      const result = reservationCommandHandler.execute(reservation, command);

      expect(result).toEqual([]);
    });

    it('should return `dateChanged` event if date changed', () => {
      const command: ReservationCommand = makeCommand({
        dateStart: new Date(100),
        dateEnd: new Date(200),
        resources: ['1'],
        assignee: new Assignee('44'),
        changedBy: new ReservationManager('44', UserRole.admin),
      });
      const reservation = make_defaultReservation();
      reservation.addResource('1');
      reservation.assignTo(new Assignee('44'));

      const [result] = reservationCommandHandler.execute(reservation, command);

      expect(result.type).toEqual(ReservationEventType.dateChanged);
      expect(result.payload).toEqual({
        dateStart: new Date(100).toISOString(),
        dateEnd: new Date(200).toISOString(),
        changedBy: '44',
      });
    });

    it('should return `assigned` event if userId changed', () => {
      const command: ReservationCommand = makeCommand({
        dateStart: new Date(0),
        dateEnd: new Date(100),
        resources: ['1'],
        assignee: new Assignee('44'),
        changedBy: new ReservationManager('44', UserRole.admin),
      });
      const reservation = make_defaultReservation();
      reservation.addResource('1');
      reservation.assignTo(new Assignee('99'));

      const [result] = reservationCommandHandler.execute(reservation, command);

      expect(result.type).toEqual(ReservationEventType.assigned);
      expect(result.payload).toEqual({ assignedTo: '44', changedBy: '44' });
    });

    it('should return `resourceAdded` event if a new resource got added', () => {
      const command: ReservationCommand = makeCommand({
        dateStart: new Date(0),
        dateEnd: new Date(100),
        resources: ['1', '99'],
        assignee: new Assignee('44'),
        changedBy: new ReservationManager('44', UserRole.admin),
      });
      const reservation = make_defaultReservation();
      reservation.addResource('1');
      reservation.assignTo(new Assignee('44'));

      const [result] = reservationCommandHandler.execute(reservation, command);

      expect(result.type).toEqual(ReservationEventType.resourceAdded);
      expect(result.payload).toEqual({ resource: '99', changedBy: '44' });
    });

    it('should return `resourceAdded` event for each new resource that got added', () => {
      const command: ReservationCommand = makeCommand({
        dateStart: new Date(0),
        dateEnd: new Date(100),
        resources: ['1', '88', '99'],
        assignee: new Assignee('44'),
        changedBy: new ReservationManager('44', UserRole.admin),
      });
      const reservation = make_defaultReservation();
      reservation.addResource('1');
      reservation.assignTo(new Assignee('44'));

      const result = reservationCommandHandler.execute(reservation, command);

      expect(result[0].type).toEqual(ReservationEventType.resourceAdded);
      expect(result[0].payload).toEqual({ resource: '88', changedBy: '44' });
      expect(result[1].type).toEqual(ReservationEventType.resourceAdded);
      expect(result[1].payload).toEqual({ resource: '99', changedBy: '44' });
    });

    it('should return `resourceRemoved` event if a new resource got removed', () => {
      const command: ReservationCommand = makeCommand({
        dateStart: new Date(0),
        dateEnd: new Date(100),
        resources: ['1'],
        assignee: new Assignee('44'),
        changedBy: new ReservationManager('44', UserRole.admin),
      });
      const reservation = make_defaultReservation();
      reservation.addResource('1');
      reservation.addResource('99');
      reservation.assignTo(new Assignee('44'));

      const [result] = reservationCommandHandler.execute(reservation, command);

      expect(result.type).toEqual(ReservationEventType.resourceRemoved);
      expect(result.payload).toEqual({ resource: '99', changedBy: '44' });
    });

    it('should return `resourceRemoved` event for each new resource that got added', () => {
      const command: ReservationCommand = makeCommand({
        dateStart: new Date(0),
        dateEnd: new Date(100),
        resources: ['1'],
        assignee: new Assignee('44'),
        changedBy: new ReservationManager('44', UserRole.admin),
      });
      const reservation = make_defaultReservation();
      reservation.addResource('1');
      reservation.addResource('88');
      reservation.addResource('99');
      reservation.assignTo(new Assignee('44'));

      const result = reservationCommandHandler.execute(reservation, command);

      expect(result[0].type).toEqual(ReservationEventType.resourceRemoved);
      expect(result[0].payload).toEqual({ resource: '88', changedBy: '44' });
      expect(result[1].type).toEqual(ReservationEventType.resourceRemoved);
      expect(result[1].payload).toEqual({ resource: '99', changedBy: '44' });
    });

    it('should return multiple events on a simple edit command', () => {
      const command: ReservationCommand = makeCommand({
        dateStart: new Date(100),
        dateEnd: new Date(200),
        resources: ['1', '99'],
        assignee: new Assignee('99'),
        changedBy: new ReservationManager('44', UserRole.admin),
      });
      const reservation = make_defaultReservation();
      reservation.addResource('1');
      reservation.addResource('88');
      reservation.assignTo(new Assignee('44'));

      const result = reservationCommandHandler.execute(reservation, command);

      expect(result[0].type).toEqual(ReservationEventType.dateChanged);
      expect(result[0].payload).toEqual({
        dateStart: new Date(100).toISOString(),
        dateEnd: new Date(200).toISOString(),
        changedBy: '44',
      });
      expect(result[1].type).toEqual(ReservationEventType.assigned);
      expect(result[1].payload).toEqual({ assignedTo: '99', changedBy: '44' });
      expect(result[2].type).toEqual(ReservationEventType.resourceAdded);
      expect(result[2].payload).toEqual({ resource: '99', changedBy: '44' });
      expect(result[3].type).toEqual(ReservationEventType.resourceRemoved);
      expect(result[3].payload).toEqual({ resource: '88', changedBy: '44' });
    });

    it('should throw an error if editing complete reservation', () => {
      const command: ReservationCommand = makeCommand({
        dateStart: new Date(100),
        dateEnd: new Date(200),
        resources: ['1'],
        assignee: new Assignee('44'),
        changedBy: new ReservationManager('44', UserRole.admin),
      });
      const reservation = make_defaultReservation();
      reservation.complete();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrow(InvalidReservationStateError);
    });

    it('should throw an error if editing canceled reservation', () => {
      const command: ReservationCommand = makeCommand({
        dateStart: new Date(100),
        dateEnd: new Date(200),
        resources: ['1'],
        assignee: new Assignee('44'),
        changedBy: new ReservationManager('44', UserRole.admin),
      });
      const reservation = make_defaultReservation();
      reservation.cancel();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrow(InvalidReservationStateError);
    });

    it('should throw an error if editing rejected reservation', () => {
      const command: ReservationCommand = makeCommand({
        dateStart: new Date(100),
        dateEnd: new Date(200),
        resources: ['1'],
        assignee: new Assignee('44'),
        changedBy: new ReservationManager('44', UserRole.admin),
      });
      const reservation = make_defaultReservation();
      reservation.reject();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrow(InvalidReservationStateError);
    });
  });
});
