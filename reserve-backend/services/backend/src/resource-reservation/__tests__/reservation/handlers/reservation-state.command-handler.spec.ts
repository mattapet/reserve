/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationCommandHandler } from '../../../domain/reservation/handlers/reservation.command-handler';
import { ReservationCommandType } from '../../../domain/reservation/reservation.command';
import { ReservationEventType } from '../../../domain/reservation/reservation.event';
import { Reservation } from '../../../domain/reservation/entities/reservation.entity';
import { ReservationManager } from '../../../domain/reservation/entities';

import { UserRole } from '../../../../user-access';
//#endregion

describe('state.command-handler', () => {
  const reservationCommandHandler = new ReservationCommandHandler();
  const workspaceId = 'test';
  const aggregateId = '49';
  const changedBy = new ReservationManager('45', UserRole.admin);

  const make_commandCreator = <T extends ReservationCommandType>(type: T) => <
    Value extends Record<string, any> = Record<string, any>
  >(
    value: Value,
  ) => ({
    type,
    rowId: workspaceId,
    aggregateId,
    payload: {
      ...value,
      changedBy,
    },
  });

  function make_defaultReservation(): Reservation {
    return new Reservation('99', 'test', new Date(0), new Date(100));
  }

  describe('`confirm` command', () => {
    const make_command = make_commandCreator(ReservationCommandType.confirm);

    it('should produce `confirmed` event', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();

      const [result] = reservationCommandHandler.execute(reservation, command);

      expect(result.type).toEqual(ReservationEventType.confirmed);
      expect(result.payload).toEqual({ changedBy: changedBy.getId() });
    });

    it('should produce `confirmed` event', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.cancel();

      const [result] = reservationCommandHandler.execute(reservation, command);

      expect(result.type).toEqual(ReservationEventType.confirmed);
      expect(result.payload).toEqual({ changedBy: changedBy.getId() });
    });

    it('should produce `confirmed` event', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.reject();

      const [result] = reservationCommandHandler.execute(reservation, command);

      expect(result.type).toEqual(ReservationEventType.confirmed);
      expect(result.payload).toEqual({ changedBy: changedBy.getId() });
    });

    it('should no events if no state change', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.confirm();

      const results = reservationCommandHandler.execute(reservation, command);

      expect(results).toEqual([]);
    });

    it('should produce Invalid state change error', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.complete();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrowError('Invalid state change');
    });
  });

  describe('`cancel` command', () => {
    const make_command = make_commandCreator(ReservationCommandType.cancel);

    it('should produce `canceled` event', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.confirm();

      const [result] = reservationCommandHandler.execute(reservation, command);

      expect(result.type).toEqual(ReservationEventType.canceled);
      expect(result.payload).toEqual({ changedBy: changedBy.getId() });
    });

    it('should no events if no state change', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.cancel();

      const results = reservationCommandHandler.execute(reservation, command);

      expect(results).toEqual([]);
    });

    it('should produce `canceled` event', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();

      const [result] = reservationCommandHandler.execute(reservation, command);

      expect(result.type).toEqual(ReservationEventType.canceled);
      expect(result.payload).toEqual({ changedBy: changedBy.getId() });
    });

    it('should produce Invalid state change error', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.reject();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrowError('Invalid state change');
    });

    it('should produce Invalid state change error', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.complete();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrowError('Invalid state change');
    });
  });

  describe('`reject` command', () => {
    const reason = 'because we need to test!';
    const make_command = make_commandCreator(ReservationCommandType.reject);

    it('should produce `rejected` event', () => {
      const command = make_command({ reason });
      const reservation = make_defaultReservation();

      const [result] = reservationCommandHandler.execute(reservation, command);

      expect(result.type).toEqual(ReservationEventType.rejected);
      expect(result.payload).toEqual({ reason, changedBy: changedBy.getId() });
    });

    it('should no events if no state change', () => {
      const command = make_command({ reason });
      const reservation = make_defaultReservation();
      reservation.reject();

      const results = reservationCommandHandler.execute(reservation, command);

      expect(results).toEqual([]);
    });

    it('should produce Invalid state change error', () => {
      const command = make_command({ reason });
      const reservation = make_defaultReservation();
      reservation.confirm();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrowError('Invalid state change');
    });

    it('should produce Invalid state change error', () => {
      const command = make_command({ reason });
      const reservation = make_defaultReservation();
      reservation.confirm();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrowError('Invalid state change');
    });

    it('should produce Invalid state change error', () => {
      const command = make_command({ reason });
      const reservation = make_defaultReservation();
      reservation.complete();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrowError('Invalid state change');
    });
  });

  describe('`complete` command', () => {
    const make_command = make_commandCreator(ReservationCommandType.complete);

    it('should produce `completed` event', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.confirm();

      const [result] = reservationCommandHandler.execute(reservation, command);

      expect(result.type).toEqual(ReservationEventType.completed);
      expect(result.payload).toEqual({});
    });

    it('should no events if no state change', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.complete();

      const results = reservationCommandHandler.execute(reservation, command);

      expect(results).toEqual([]);
    });

    it('should produce Invalid state change error', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.cancel();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrowError('Invalid state change');
    });

    it('should produce Invalid state change error', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();
      reservation.reject();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrowError('Invalid state change');
    });

    it('should produce Invalid state change error', () => {
      const command = make_command({});
      const reservation = make_defaultReservation();

      const fn = () => reservationCommandHandler.execute(reservation, command);

      expect(fn).toThrowError('Invalid state change');
    });
  });
});
