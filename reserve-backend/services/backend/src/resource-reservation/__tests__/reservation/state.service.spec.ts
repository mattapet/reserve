/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationService } from '../../domain/reservation/reservation.service';
import { ReservationState } from '../../domain/reservation/reservation-state.type';
import { ReservationBuilder } from './builders/reservation.builder';
import { Reservation } from '../../domain/reservation/entities/reservation.entity';
import { ReservationServiceBuilder } from './builders/reservation.service.builder';
import { CreateReservation } from '../../domain/reservation/values';
import {
  ResourcesUnavailableError,
  IllegalOperationError,
  InsufficientRightsError,
} from '../../domain/reservation/reservation.errors';
import { ReservationRepositoryBuilder } from './builders/reservation.repository.builder';
import { ReservationProjectionRepository } from '../../domain/reservation-projection';
import { InMemoryReservationProjectionRepository } from '../../application/reservation-projection/repositories/in-memory/in-memory-reservation-projection.repository';
import {
  Assignee,
  ReservationManager,
} from '../../domain/reservation/entities';
import { UserRole } from '../../../user-access';
import { ResourceId } from '../../domain/resource/resource.entity';
//#endregion

describe('state.service', () => {
  let searchRepo!: ReservationProjectionRepository;
  let reservationService!: ReservationService;

  beforeEach(async () => {
    searchRepo = new InMemoryReservationProjectionRepository();
    reservationService = new ReservationServiceBuilder()
      .withRepository(ReservationRepositoryBuilder.inMemory())
      .withSearchRepo(searchRepo)
      .build();
  });

  function make_defaultReservationBuilder(): ReservationBuilder {
    return new ReservationBuilder()
      .withId('99')
      .withWorkspaceId('test')
      .withDateStart(new Date(Date.now() + 100_000))
      .withDateEnd(new Date(Date.now() + 200_000))
      .withResourceIds(['1', '2', '3'])
      .withAssignee(new Assignee('99'))
      .withCreatedAt(new Date(0))
      .withNotes('additional notes');
  }

  function make_reservationWithAssignee(assignee: Assignee): Reservation {
    return make_defaultReservationBuilder().withAssignee(assignee).build();
  }

  function make_createParams(
    id: string,
    assignee: Assignee,
    createdBy: ReservationManager,
  ): CreateReservation {
    return new CreateReservation(
      id,
      'test',
      new Date(Date.now() + 100_000),
      new Date(Date.now() + 200_000),
      ['1', '2', '3'],
      'additional notes',
      assignee,
      createdBy,
    );
  }

  function make_createParamsWithResources(
    id: string,
    assignee: Assignee,
    createdBy: ReservationManager,
    resources: ResourceId[],
  ): CreateReservation {
    return new CreateReservation(
      id,
      'test',
      new Date(Date.now() + 100_000),
      new Date(Date.now() + 200_000),
      resources,
      'additional notes',
      assignee,
      createdBy,
    );
  }

  function make_createParamsWithDateRange(
    id: string,
    assignee: Assignee,
    createdBy: ReservationManager,
    dateStart: Date,
    dateEnd: Date,
  ): CreateReservation {
    return new CreateReservation(
      id,
      'test',
      dateStart,
      dateEnd,
      ['1', '2', '3'],
      'additional notes',
      assignee,
      createdBy,
    );
  }

  async function effect_createReservation(
    reservation: Reservation,
    params: CreateReservation,
  ) {
    await reservationService.create(reservation, params);
    await searchRepo.save({
      id: reservation.getId(),
      workspaceId: reservation.getWorkspaceId(),
      dateStart: reservation.getDateStart(),
      dateEnd: reservation.getDateEnd(),
      state: reservation.getState(),
      userId: reservation.getAssignee().getId(),
      notes: reservation.getNotes(),
      resourceIds: reservation.getResourceIds(),
      createdAt: reservation.getCreatedAt(),
    });
  }

  async function effect_confirmReservation(
    reservation: Reservation,
    confirmedBy: ReservationManager,
  ) {
    await reservationService.confirm(reservation, confirmedBy);
    const item = await searchRepo.getById(
      reservation.getWorkspaceId(),
      reservation.getId(),
    );
    item.state = ReservationState.confirmed;
    await searchRepo.save(item);
  }

  async function effect_insertConfirmedReservations(
    params: CreateReservation[],
  ) {
    await params.reduce(async (prev, param) => {
      await prev;
      const user = new ReservationManager('99', UserRole.maintainer);
      const reservation = new Reservation();
      await effect_createReservation(reservation, param);
      await effect_confirmReservation(reservation, user);
    }, Promise.resolve());
  }

  describe('#confirm()', () => {
    it('should confirm the reservation', async () => {
      const reservation = make_defaultReservationBuilder().build();
      const assignee = new Assignee('99');
      const changedBy = new ReservationManager('99', UserRole.maintainer);
      await effect_createReservation(
        reservation,
        make_createParams('99', assignee, changedBy),
      );

      await reservationService.confirm(reservation, changedBy);

      const result = await reservationService.getById('test', '99');
      expect(result.getState()).toBe(ReservationState.confirmed);
    });

    it('should throw ResourcesUnavailableError if some resources are not available', async () => {
      const assignee = new Assignee('99');
      const changedBy = new ReservationManager('99', UserRole.maintainer);
      await effect_insertConfirmedReservations([
        make_createParamsWithResources('99', assignee, changedBy, ['44']),
      ]);
      const reservation = make_defaultReservationBuilder()
        .withResourceIds(['44'])
        .build();

      const fn = reservationService.confirm(reservation, changedBy);

      await expect(fn).rejects.toThrow(ResourcesUnavailableError);
    });

    it('should throw an error that users cannot confirm', async () => {
      effect_insertConfirmedReservations([]);
      const reservation = make_defaultReservationBuilder().build();
      const changedBy = new ReservationManager('99', UserRole.user);

      const fn = reservationService.confirm(reservation, changedBy);

      await expect(fn).rejects.toThrow(InsufficientRightsError);
    });
  });

  describe('#cancel()', () => {
    it('should cancel the reservation if maintainer canceling', async () => {
      const assignee = new Assignee('88');
      const createdBy = new ReservationManager('88', UserRole.maintainer);
      const reservation = make_reservationWithAssignee(assignee);
      const changedBy = new ReservationManager('99', UserRole.maintainer);
      await effect_createReservation(
        reservation,
        make_createParams('99', assignee, createdBy),
      );

      await reservationService.cancel(reservation, changedBy);

      const result = await reservationService.getById('test', '99');
      expect(result.getState()).toBe(ReservationState.canceled);
    });

    it('should cancel the reservation if canceling assignees reservation', async () => {
      const assignee = new Assignee('99');
      const createdBy = new ReservationManager('99', UserRole.user);
      const reservation = make_reservationWithAssignee(assignee);
      await effect_createReservation(
        reservation,
        make_createParams('99', assignee, createdBy),
      );

      await reservationService.cancel(reservation, createdBy);

      const result = await reservationService.getById('test', '99');
      expect(result.getState()).toBe(ReservationState.canceled);
    });

    it('should produce an error when user tries to cancel different reservation', async () => {
      const assignee = new Assignee('88');
      const reservation = make_reservationWithAssignee(assignee);
      const changedBy = new ReservationManager('99', UserRole.user);

      const fn = reservationService.cancel(reservation, changedBy);

      await expect(fn).rejects.toThrow(InsufficientRightsError);
    });
  });

  describe('#reject()', () => {
    const reason = 'some-random-reason';

    it('should reject the reservation', async () => {
      const reservation = make_defaultReservationBuilder().build();
      const assignee = new Assignee('99');
      const changedBy = new ReservationManager('99', UserRole.admin);
      await effect_createReservation(
        reservation,
        make_createParams('99', assignee, changedBy),
      );

      await reservationService.reject(reservation, changedBy, reason);

      const result = await reservationService.getById('test', '99');
      expect(result.getState()).toBe(ReservationState.rejected);
    });

    it('should throw an error that users cannot reject', async () => {
      const reservation = make_defaultReservationBuilder().build();
      const changedBy = new ReservationManager('99', UserRole.user);

      await expect(
        reservationService.reject(reservation, changedBy, reason),
      ).rejects.toEqual(
        new Error('Only Maintainers or admins can reject reservations.'),
      );
    });

    it('should throw an error if reservation in past', async () => {
      const reservation = make_defaultReservationBuilder()
        .withDateStart(new Date(0))
        .withDateEnd(new Date(100))
        .build();
      const changedBy = new ReservationManager('99', UserRole.maintainer);

      await expect(
        reservationService.reject(reservation, changedBy, reason),
      ).rejects.toThrow(IllegalOperationError);
    });
  });

  describe('#complete()', () => {
    it('should complete the reservation', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const reservation = new Reservation();
      const assignee = new Assignee('99');
      const createBy = new ReservationManager('99', UserRole.admin);
      await effect_createReservation(
        reservation,
        make_createParamsWithDateRange(
          '99',
          assignee,
          createBy,
          new Date(0),
          new Date(100),
        ),
      );
      await reservationService.confirm(reservation, createBy);
      jest.spyOn(Date, 'now').mockImplementation(() => 200);

      await reservationService.complete(reservation);

      const result = await reservationService.getById('test', '99');
      expect(result.getState()).toBe(ReservationState.completed);
    });

    it('should produce an error when resrvation in future', async () => {
      const reservation = make_defaultReservationBuilder()
        .withState(ReservationState.confirmed)
        .build();

      const fn = reservationService.complete(reservation);

      await expect(fn).rejects.toThrow(IllegalOperationError);
    });
  });
});
