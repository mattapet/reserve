/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AvailabilityService } from '../../domain/reservation/services/availability.service';

import { ReservationProjectionRepository } from '../../domain/reservation-projection';
import { InMemoryReservationProjectionRepository } from '../../application/reservation-projection/repositories/in-memory/in-memory-reservation-projection.repository';

import { ReservationBuilder } from './builders/reservation.builder';

import { ReservationState } from '../../domain/reservation/reservation-state.type';
import { Reservation } from '../../domain/reservation/entities/reservation.entity';
import { Assignee } from '../../domain/reservation/entities';
//#endregion

describe('availability.service', () => {
  let searchRepo!: ReservationProjectionRepository;
  let availabilityService!: AvailabilityService;

  beforeEach(() => {
    searchRepo = new InMemoryReservationProjectionRepository();
    availabilityService = new AvailabilityService(searchRepo);
  });

  const workspaceId = 'test';

  function make_defaultReservationBuilder(): ReservationBuilder {
    return new ReservationBuilder()
      .withId('99')
      .withAssignee(new Assignee('88'))
      .withState(ReservationState.requested)
      .withWorkspaceId(workspaceId)
      .withCreatedAt(new Date(0));
  }

  function mock_reservations(reservations: Reservation[]) {
    jest.spyOn(searchRepo, 'getAllByDateRange').mockImplementation(() =>
      Promise.resolve(
        reservations.map((reservation) => ({
          id: reservation.getId(),
          workspaceId: reservation.getId(),
          dateStart: reservation.getDateStart(),
          dateEnd: reservation.getDateEnd(),
          state: reservation.getState(),
          userId: reservation.getAssignee().getId(),
          resourceIds: reservation.getResourceIds(),
          createdAt: reservation.getCreatedAt(),
          notes: reservation.getNotes(),
        })),
      ),
    );
  }

  it('should return `true` if no reservation with given resource intersect within given date range', async () => {
    const dateStart = new Date(0);
    const dateEnd = new Date(0);
    mock_reservations([]);

    const result = await availabilityService.isAvailable(
      workspaceId,
      dateStart,
      dateEnd,
      '1',
    );

    expect(result).toBe(true);
  });

  it('should return `true` if reservation intersecting given range are not confirmed', async () => {
    const dateStart = new Date(0);
    const dateEnd = new Date(100);
    const reservations = [
      make_defaultReservationBuilder()
        .withDateStart(new Date(200))
        .withDateEnd(new Date(300))
        .withResourceIds(['45'])
        .build(),
    ];
    mock_reservations(reservations);

    const result = await availabilityService.isAvailable(
      workspaceId,
      dateStart,
      dateEnd,
      '45',
    );

    expect(result).toBe(true);
  });

  it('should return `false` if a reservation with given resource intersect within given date range', async () => {
    const dateStart = new Date(100);
    const dateEnd = new Date(200);
    const reservations = [
      make_defaultReservationBuilder()
        .withDateStart(new Date(0))
        .withDateEnd(new Date(300))
        .withState(ReservationState.confirmed)
        .withResourceIds(['44', '45', '47'])
        .build(),
    ];
    mock_reservations(reservations);

    const result = await availabilityService.isAvailable(
      workspaceId,
      dateStart,
      dateEnd,
      '45',
    );

    expect(result).toBe(false);
  });

  it('should return `true` if a reservation with other resource intersect within given date range', async () => {
    const dateStart = new Date(100);
    const dateEnd = new Date(200);
    const reservations = [
      make_defaultReservationBuilder()
        .withDateStart(new Date(0))
        .withDateEnd(new Date(300))
        .withState(ReservationState.confirmed)
        .withResourceIds(['47'])
        .build(),
    ];
    mock_reservations(reservations);

    const result = await availabilityService.isAvailable(
      workspaceId,
      dateStart,
      dateEnd,
      '45',
    );

    expect(result).toBe(true);
  });
});
