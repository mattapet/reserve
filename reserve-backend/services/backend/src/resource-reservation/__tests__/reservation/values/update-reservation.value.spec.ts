/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from '../../../../user-access';

import {
  Assignee,
  ReservationManager,
} from '../../../domain/reservation/entities';
import { UpdateReservation } from '../../../domain/reservation/values/update-reservation.value';
import { InvalidDateRangeError } from '../../../domain/reservation/reservation.errors';
//#endregion

describe('update-reservaiton.value', () => {
  it('should throw InvalidDateRangeError if date starts in the past', () => {
    const fn = () =>
      new UpdateReservation(
        'test',
        new Date(Date.now() - 100_000),
        new Date(Date.now() + 200_000),
        ['1', '2', '3'],
        'additional notes',
        new Assignee('99'),
        new ReservationManager('99', UserRole.user),
      );

    expect(fn).toThrow(InvalidDateRangeError);
  });

  it('should throw InvalidDateRangeError if date end is before date start', () => {
    const fn = () =>
      new UpdateReservation(
        'test',
        new Date(Date.now() + 200_000),
        new Date(Date.now() + 100_000),
        ['1', '2', '3'],
        'additional notes',
        new Assignee('99'),
        new ReservationManager('99', UserRole.user),
      );

    expect(fn).toThrow(InvalidDateRangeError);
  });
});
