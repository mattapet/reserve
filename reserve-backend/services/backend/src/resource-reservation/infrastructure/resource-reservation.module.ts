/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { ResourceModule } from './resource';
import { ReservationModule } from './reservation';
import { ReservationProjectionModule } from './reservation-projection';
import { ReservationCommentModule } from './reservation-comment';
import { ReservationCompletionModule } from './reservation-completion';
import { ReservationTemplateModule } from './reservation-template';
//#endregion

@Module({
  imports: [
    ResourceModule,
    ReservationModule,
    ReservationProjectionModule,
    ReservationCommentModule,
    ReservationCompletionModule,
    ReservationTemplateModule,
  ],
  exports: [
    ResourceModule,
    ReservationModule,
    ReservationProjectionModule,
    ReservationCommentModule,
    ReservationCompletionModule,
    ReservationTemplateModule,
  ],
})
export class ResourceReservationModule {}
