/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons';

import {
  ResourceCommandHandler,
  ResourceEventHandler,
} from '../../domain/resource/handlers';
import { ResourceService } from '../../domain/resource/resource.service';
import { RESOURCE_REPOSITORY } from '../../domain/resource/constants';

import { ResourceRepository } from '../../application/resource/resource.repository';
//#endregion

@Module({
  imports: [CommonsModule],
  providers: [
    ResourceService,
    {
      provide: RESOURCE_REPOSITORY,
      useClass: ResourceRepository,
    },
    ResourceCommandHandler,
    ResourceEventHandler,
  ],
  exports: [ResourceService],
})
export class ResourceModule {}
