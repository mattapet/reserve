/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonsModule } from '@rsaas/commons';

import { TypeormReservationTemplateRepository } from '../../application/reservation-template/repositories/typeorm/typeorm-reservation-template.repository';
import { ReservationTemplate } from '../../domain/reservation-template/reservation-template.entity';
import { ReservationTemplateService } from '../../domain/reservation-template/reservation-template.service';
import { RESERVATION_TEMPLATE_REPOSITORY } from '../../domain/reservation-template/constants';
//#endregion

@Module({
  imports: [
    CommonsModule,
    TypeOrmModule.forFeature([
      TypeormReservationTemplateRepository,
      ReservationTemplate,
    ]),
  ],
  providers: [
    ReservationTemplateService,
    {
      provide: RESERVATION_TEMPLATE_REPOSITORY,
      useFactory: (repo) => repo,
      inject: [TypeormReservationTemplateRepository],
    },
  ],
  exports: [ReservationTemplateService],
})
export class ReservationTemplateModule {}
