/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons/lib/commons.module';

import { ReservationCommentService } from '../../domain/reservation-comment/reservation-comment.service';
import {
  ReservationCommentCommandHandler,
  ReservationCommentEventHandler,
} from '../../domain/reservation-comment/handlers';
import { RESERVATION_COMMENT_REPOSITORY } from '../../domain/reservation-comment/constants';

import { ReservationCommentRepository } from '../../application/reservation-comment/reservation-comment.repository';
//#endregion

@Module({
  imports: [CommonsModule],
  providers: [
    ReservationCommentService,
    ReservationCommentCommandHandler,
    ReservationCommentEventHandler,
    {
      provide: RESERVATION_COMMENT_REPOSITORY,
      useClass: ReservationCommentRepository,
    },
  ],
  exports: [ReservationCommentService],
})
export class ReservationCommentModule {}
