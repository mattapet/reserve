/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons';

import { ReservationProjectionModule } from '../reservation-projection';

import { ReservationCompletionService } from '../../domain/reservation-completion';
import { ReservationService } from '../../domain/reservation/reservation.service';
import {
  ReservationCommandHandler,
  ReservationEventHandler,
} from '../../domain/reservation/handlers';
import { RESERVATION_REPOSITORY } from '../../domain/reservation';

import { ReservationRepository } from '../../application/reservation/reservation.repository';
//#endregion

@Module({
  imports: [CommonsModule, ReservationProjectionModule],
  providers: [
    ReservationService,
    {
      provide: RESERVATION_REPOSITORY,
      useClass: ReservationRepository,
    },
    ReservationCompletionService,
    ReservationCommandHandler,
    ReservationEventHandler,
  ],
  exports: [ReservationService, ReservationProjectionModule],
})
export class ReservationModule {}
