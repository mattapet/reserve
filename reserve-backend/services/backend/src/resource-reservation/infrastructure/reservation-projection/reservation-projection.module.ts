/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonsModule } from '@rsaas/commons';

import {
  TypeormReservationProjection,
  TypeormReservationProjectionRepository,
} from '../../application/reservation-projection/repositories/typeorm';
import { ReservationProjectionIngestor } from '../../application/reservation-projection/reservation-projection.ingestor';
import { RESERVATION_PROJECTION_REPOSITORY } from '../../domain/reservation-projection/constants';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([
      TypeormReservationProjection,
      TypeormReservationProjectionRepository,
    ]),
    CommonsModule,
  ],
  providers: [
    ReservationProjectionIngestor,
    {
      provide: RESERVATION_PROJECTION_REPOSITORY,
      useFactory: (repo) => repo,
      inject: [TypeormReservationProjectionRepository],
    },
  ],
  exports: [TypeOrmModule, RESERVATION_PROJECTION_REPOSITORY],
})
export class ReservationProjectionModule {}
