/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { ReservationTemplate } from '../../../../domain/reservation-template/reservation-template.entity';
import { ReservationTemplateRepository } from '../../../../domain/reservation-template/reservation-template.repository';

export class InMemoryReservationTemplateRepository
  implements ReservationTemplateRepository {
  public constructor(
    private readonly storage: {
      [workspaceId: string]: ReservationTemplate;
    } = {},
  ) {}

  public async findByWorkspaceId(
    workspaceId: string,
  ): Promise<ReservationTemplate | undefined> {
    return this.storage[workspaceId];
  }

  public async save(
    template: ReservationTemplate,
  ): Promise<ReservationTemplate> {
    return (this.storage[template.workspaceId] = template);
  }

  public async remove(
    template: ReservationTemplate,
  ): Promise<ReservationTemplate> {
    delete this.storage[template.workspaceId];
    return template;
  }
}
