/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository } from 'typeorm';
import { BaseRepository } from '@rsaas/commons/lib/typeorm';

import { ReservationTemplate } from '../../../../domain/reservation-template/reservation-template.entity';
import { ReservationTemplateRepository } from '../../../../domain/reservation-template/reservation-template.repository';
//#endregion

@EntityRepository(ReservationTemplate)
export class TypeormReservationTemplateRepository
  extends BaseRepository<ReservationTemplate>
  implements ReservationTemplateRepository {
  public async findByWorkspaceId(
    workspaceId: string,
  ): Promise<ReservationTemplate | undefined> {
    return this.findOne({ workspaceId });
  }
}
