/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Inject, Injectable } from '@nestjs/common';
import { Log } from '@rsaas/commons/lib/logger';
import { EventPattern, EventConsumer } from '@rsaas/commons/lib/event-broker';
import { Transactional } from '@rsaas/commons/lib/typeorm';

import {
  ReservationEventType,
  ReservationAssignedEvent,
  ReservationResourceAddedEvent,
  ReservationResourceRemovedEvent,
  ReservationConfirmedEvent,
  ReservationRejectedEvent,
  ReservationCanceledEvent,
  ReservationCompletedEvent,
  ReservationCreatedEvent,
} from '../../domain/reservation/reservation.event';

import { ReservationProjectionRepository } from '../../domain/reservation-projection/reservation-projection.repository';
import {
  TypeormReservationProjection,
  TypeormReservationProjectionRepository,
} from './repositories/typeorm';
import { ReservationState } from '../../domain/reservation/reservation-state.type';
//#endregion

@Injectable()
@EventConsumer()
export class ReservationProjectionIngestor {
  public constructor(
    @Inject(TypeormReservationProjectionRepository)
    private readonly repository: ReservationProjectionRepository,
  ) {}

  @Log({ format: 'Consuming {0}' })
  @Transactional()
  @EventPattern(ReservationEventType.created)
  public async consumeCreated(event: ReservationCreatedEvent): Promise<void> {
    const projection = new TypeormReservationProjection(
      event.payload.workspaceId,
      event.aggregateId,
      new Date(event.payload.dateStart),
      new Date(event.payload.dateEnd),
      event.timestamp,
      ReservationState.requested,
      [],
      '',
      event.payload.notes,
    );
    await this.repository.save(projection);
  }

  @Log({ format: 'Consuming {0}' })
  @Transactional()
  @EventPattern(ReservationEventType.assigned)
  public async consumeAssigneeChanged(
    event: ReservationAssignedEvent,
  ): Promise<void> {
    const item = await this.repository.getById(
      event.rowId.split(':')[1],
      event.aggregateId,
    );
    item.userId = event.payload.assignedTo;
    await this.repository.save(item);
  }

  @Log({ format: 'Consuming {0}' })
  @Transactional()
  @EventPattern(ReservationEventType.resourceAdded)
  public async consumeResourceAdded(
    event: ReservationResourceAddedEvent,
  ): Promise<void> {
    const item = await this.repository.getById(
      event.rowId.split(':')[1],
      event.aggregateId,
    );
    item.resourceIds = [...item.resourceIds, event.payload.resource];
    await this.repository.save(item);
  }

  @Log({ format: 'Consuming {0}' })
  @Transactional()
  @EventPattern(ReservationEventType.resourceRemoved)
  public async consumeResourceRemoved(
    event: ReservationResourceRemovedEvent,
  ): Promise<void> {
    const item = await this.repository.getById(
      event.rowId.split(':')[1],
      event.aggregateId,
    );
    item.resourceIds = item.resourceIds.filter(
      (id) => id !== event.payload.resource,
    );
    await this.repository.save(item);
  }

  @Log({ format: 'Consuming {0}' })
  @Transactional()
  @EventPattern(ReservationEventType.confirmed)
  public async consumeConfirmed(
    event: ReservationConfirmedEvent,
  ): Promise<void> {
    const item = await this.repository.getById(
      event.rowId.split(':')[1],
      event.aggregateId,
    );
    item.state = ReservationState.confirmed;
    await this.repository.save(item);
  }

  @Log({ format: 'Consuming {0}' })
  @Transactional()
  @EventPattern(ReservationEventType.rejected)
  public async consumeRejected(event: ReservationRejectedEvent): Promise<void> {
    const item = await this.repository.getById(
      event.rowId.split(':')[1],
      event.aggregateId,
    );
    item.state = ReservationState.rejected;
    await this.repository.save(item);
  }

  @Log({ format: 'Consuming {0}' })
  @Transactional()
  @EventPattern(ReservationEventType.canceled)
  public async consumeCanceled(event: ReservationCanceledEvent): Promise<void> {
    const item = await this.repository.getById(
      event.rowId.split(':')[1],
      event.aggregateId,
    );
    item.state = ReservationState.canceled;
    await this.repository.save(item);
  }

  @Log({ format: 'Consuming {0}' })
  @Transactional()
  @EventPattern(ReservationEventType.completed)
  public async consumeCompleted(
    event: ReservationCompletedEvent,
  ): Promise<void> {
    const item = await this.repository.getById(
      event.rowId.split(':')[1],
      event.aggregateId,
    );
    item.state = ReservationState.completed;
    await this.repository.save(item);
  }
}
