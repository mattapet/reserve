/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  LessThanOrEqual,
  MoreThanOrEqual,
  LessThan,
  EntityRepository,
} from 'typeorm';
import { BaseRepository } from '@rsaas/commons/lib/typeorm';
import { TypeormReservationProjection } from './reservation-projection.entity';
import { ReservationProjection } from '../../../../domain/reservation-projection/reservation-projection.interface';
import { ReservationState } from '../../../../domain/reservation/reservation-state.type';
import { ReservationProjectionRepository } from '../../../../domain/reservation-projection/reservation-projection.repository';
import { ReservationNotFoundError } from '../../../../domain/reservation-projection/reservation-projection.errors';
//#endregion

@EntityRepository(TypeormReservationProjection)
export class TypeormReservationProjectionRepository
  extends BaseRepository<TypeormReservationProjection>
  implements ReservationProjectionRepository {
  public async getById(
    workspaceId: string,
    reservationId: string,
  ): Promise<ReservationProjection> {
    const item = await this.findOne({
      where: { workspaceId, id: reservationId },
    });

    if (!item) {
      throw new ReservationNotFoundError(
        `Reservation with id '${reservationId}' not found`,
      );
    }

    return item;
  }

  public async getAll(workspaceId: string): Promise<ReservationProjection[]> {
    return this.find({ where: { workspaceId } });
  }

  public async getAllByDateRange(
    workspaceId: string,
    dateStart: Date,
    dateEnd: Date,
  ): Promise<ReservationProjection[]> {
    return this.find({
      where: {
        workspaceId,
        dateStart: LessThanOrEqual(dateEnd),
        dateEnd: MoreThanOrEqual(dateStart),
      },
    });
  }

  public async getAllSince(
    workspaceId: string,
    since: Date,
  ): Promise<ReservationProjection[]> {
    return this.find({
      where: {
        workspaceId,
        dateEnd: MoreThanOrEqual(since),
      },
    });
  }

  public async getAllUntil(
    workspaceId: string,
    until: Date,
  ): Promise<ReservationProjection[]> {
    return this.find({
      where: {
        workspaceId,
        dateEnd: MoreThanOrEqual(until),
      },
    });
  }

  public async getByUserId(
    workspaceId: string,
    userId: string,
  ): Promise<ReservationProjection[]> {
    return this.find({
      where: {
        workspaceId,
        userId,
      },
    });
  }

  public async getAllToComplete(): Promise<ReservationProjection[]> {
    return this.find({
      where: {
        dateEnd: LessThan(new Date(Date.now())),
        state: ReservationState.confirmed,
      },
    });
  }
}
