/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Column, PrimaryColumn, Index, Entity } from 'typeorm';
import { ReservationProjection } from '../../../../domain/reservation-projection/reservation-projection.interface';
import { ReservationState } from '../../../../domain/reservation/reservation-state.type';
import { ResourceId } from '../../../../domain/resource/resource.entity';
//#endregion

@Entity({ name: 'search_projection' })
export class TypeormReservationProjection implements ReservationProjection {
  @PrimaryColumn({ name: 'workspace_id', charset: 'utf8' })
  public workspaceId: string;

  @PrimaryColumn({ name: 'reservation_id', charset: 'utf8' })
  public id: string;

  @Index()
  @Column({
    transformer: {
      to: (d) => d ?? new Date(Date.now()).toISOString(),
      from: (d: Date) => d,
    },
    type: 'datetime',
    name: 'date_start',
  })
  public dateStart: Date;

  @Index()
  @Column({
    transformer: {
      to: (d) => d ?? new Date(Date.now()).toISOString(),
      from: (d: Date) => d,
    },
    type: 'datetime',
    name: 'date_end',
  })
  public dateEnd: Date;

  @Index()
  @Column({ name: 'user_id', charset: 'utf8' })
  public userId: string;

  @Column({ name: 'reservation_state', charset: 'utf8' })
  public state: ReservationState;

  @Column({ name: 'resource_ids', type: 'simple-json' })
  public resourceIds: ResourceId[];

  @Column({
    transformer: {
      to: (d) => d ?? new Date(Date.now()).toISOString(),
      from: (d: Date) => d,
    },
    name: 'created_at',
  })
  public createdAt: Date;

  @Column({ name: 'additional_notes', type: 'text', nullable: true })
  public notes?: string;

  public constructor(
    workspaceId: string,
    reservationId: string,
    dateStart: Date,
    dateEnd: Date,
    createdAt: Date,
    state: ReservationState = ReservationState.requested,
    resourceIds: ResourceId[] = [],
    userId: string = '',
    notes?: string,
  ) {
    this.id = reservationId;
    this.workspaceId = workspaceId;
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
    this.createdAt = createdAt;
    this.state = state;
    this.resourceIds = resourceIds;
    this.userId = userId;
    this.notes = notes;
  }
}
