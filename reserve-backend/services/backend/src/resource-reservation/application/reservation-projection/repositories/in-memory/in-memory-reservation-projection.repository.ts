/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationProjection } from '../../../../domain/reservation-projection/reservation-projection.interface';
import { ReservationState } from '../../../../domain/reservation/reservation-state.type';
import { ReservationProjectionRepository } from '../../../../domain/reservation-projection/reservation-projection.repository';
import { ReservationNotFoundError } from '../../../../domain/reservation-projection/reservation-projection.errors';
//#endregion

export class InMemoryReservationProjectionRepository
  implements ReservationProjectionRepository {
  private readonly storage: { [id: string]: ReservationProjection } = {};

  public async getById(
    workspaceId: string,
    reservationId: string,
  ): Promise<ReservationProjection> {
    const item = this.storage[reservationId];
    if (!item) {
      throw new ReservationNotFoundError(
        `Reservation with id '${reservationId}' not found`,
      );
    }
    return item;
  }

  public async getAll(workspaceId: string): Promise<ReservationProjection[]> {
    return Object.values(this.storage).filter(
      (r) => r.workspaceId === workspaceId,
    );
  }

  public async getAllByDateRange(
    workspaceId: string,
    dateStart: Date,
    dateEnd: Date,
  ): Promise<ReservationProjection[]> {
    return Object.values(this.storage)
      .filter((r) => r.workspaceId === workspaceId)
      .filter((r) => +r.dateStart <= +dateEnd && +r.dateEnd >= +dateStart);
  }

  public async getAllSince(
    workspaceId: string,
    since: Date,
  ): Promise<ReservationProjection[]> {
    return Object.values(this.storage)
      .filter((r) => r.workspaceId === workspaceId)
      .filter((r) => +r.dateEnd >= +since);
  }

  public async getAllUntil(
    workspaceId: string,
    until: Date,
  ): Promise<ReservationProjection[]> {
    return Object.values(this.storage)
      .filter((r) => r.workspaceId === workspaceId)
      .filter((r) => +r.dateStart <= +until);
  }

  public async getByUserId(
    workspaceId: string,
    userId: string,
  ): Promise<ReservationProjection[]> {
    return Object.values(this.storage).filter(
      (r) => r.workspaceId === workspaceId && r.userId === userId,
    );
  }

  public async getAllToComplete(): Promise<ReservationProjection[]> {
    return Object.values(this.storage)
      .filter(({ state }) => state === ReservationState.confirmed)
      .filter(({ dateEnd }) => +dateEnd < Date.now());
  }

  public async save(
    projection: ReservationProjection,
  ): Promise<ReservationProjection> {
    return (this.storage[projection.id] = projection);
  }
}
