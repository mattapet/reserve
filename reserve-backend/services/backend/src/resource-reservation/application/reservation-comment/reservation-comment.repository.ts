/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import {
  AggregateRepository,
  EventRepository,
  SnapshotRepository,
  InjectEventRepository,
  InjectSnapshotRepository,
} from '@rsaas/commons/lib/events';

import { ReservationComment } from '../../domain/reservation-comment/reservation-comment.entity';
import { ReservationCommentCommand } from '../../domain/reservation-comment/reservation-comment.command';
import { ReservationCommentEvent } from '../../domain/reservation-comment/reservation-comment.event';
import {
  ReservationCommentCommandHandler,
  ReservationCommentEventHandler,
} from '../../domain/reservation-comment/handlers';
//#endregion

@Injectable()
export class ReservationCommentRepository extends AggregateRepository<
  ReservationCommentCommand,
  ReservationCommentEvent,
  ReservationComment
> {
  public constructor(
    @InjectEventRepository()
    repository: EventRepository<ReservationCommentEvent>,
    @InjectSnapshotRepository()
    snapshotRepository: SnapshotRepository,
    commandHandler: ReservationCommentCommandHandler,
    eventHandler: ReservationCommentEventHandler,
  ) {
    super(
      repository,
      snapshotRepository,
      commandHandler,
      eventHandler,
      ReservationComment,
    );
  }
}
