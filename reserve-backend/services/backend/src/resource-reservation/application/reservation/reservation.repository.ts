/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import {
  AggregateRepository,
  EventRepository,
  SnapshotRepository,
  InjectEventRepository,
  InjectSnapshotRepository,
} from '@rsaas/commons/lib/events';

import { ReservationCommand } from '../../domain/reservation/reservation.command';
import { ReservationEvent } from '../../domain/reservation/reservation.event';
import { Reservation } from '../../domain/reservation/entities/reservation.entity';
import {
  ReservationCommandHandler,
  ReservationEventHandler,
} from '../../domain/reservation/handlers';
//#endregion

@Injectable()
export class ReservationRepository extends AggregateRepository<
  ReservationCommand,
  ReservationEvent,
  Reservation
> {
  public constructor(
    @InjectEventRepository()
    eventRepository: EventRepository<ReservationEvent>,
    @InjectSnapshotRepository()
    snapshotRepository: SnapshotRepository,
    commandHandler: ReservationCommandHandler,
    eventHandler: ReservationEventHandler,
  ) {
    super(
      eventRepository,
      snapshotRepository,
      commandHandler,
      eventHandler,
      Reservation,
    );
  }
}
