/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { AggregateRepository } from '@rsaas/commons/lib/events/repositories/aggregate-repository';
import {
  InjectEventRepository,
  InjectSnapshotRepository,
} from '@rsaas/commons/lib/events';
import { SnapshotRepository } from '@rsaas/commons/lib/events/snapshot.repository';
import { EventRepository } from '@rsaas/commons/lib/events/event.repository';

import { ResourceCommand } from '../../domain/resource/resource.command';
import { ResourceEvent } from '../../domain/resource/resource.event';
import { Resource } from '../../domain/resource/resource.entity';
import {
  ResourceCommandHandler,
  ResourceEventHandler,
} from '../../domain/resource/handlers';
//#endregion

@Injectable()
export class ResourceRepository extends AggregateRepository<
  ResourceCommand,
  ResourceEvent,
  Resource
> {
  public constructor(
    @InjectEventRepository()
    eventRepository: EventRepository<ResourceEvent>,
    @InjectSnapshotRepository()
    snapshotRepository: SnapshotRepository,
    commandHandler: ResourceCommandHandler,
    eventHandler: ResourceEventHandler,
  ) {
    super(
      eventRepository,
      snapshotRepository,
      commandHandler,
      eventHandler,
      Resource,
    );
  }
}
