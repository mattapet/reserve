/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Cron } from '@nestjs/schedule';
import { Injectable } from '@nestjs/common';
import { Transactional } from '@rsaas/commons/lib/typeorm';

import { ReservationCompletionService } from '../../domain/reservation-completion';
//#endregion

const CRON_COMPLETION_SCHEDULE = process.env.CRON_COMPLETION_SCHEDULE;

@Injectable()
export class ReservationCompletionScheduler {
  public constructor(
    private readonly completionService: ReservationCompletionService,
  ) {}

  @Cron(CRON_COMPLETION_SCHEDULE!)
  @Transactional()
  public async scheduleTask(): Promise<void> {
    await this.completionService.completeReservations();
  }
}
