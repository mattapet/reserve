/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Type } from 'class-transformer';
import { User } from '../../../user-access';

import { CommentAuthorId } from './values';
import { InsufficientRightsError } from './reservation-comment.errors';
//#endregion

export class CommentAuthor {
  @Type(() => CommentAuthorId)
  private readonly id: CommentAuthorId;

  public constructor(id: CommentAuthorId) {
    this.id = id;
  }

  public getId(): CommentAuthorId {
    return this.id;
  }

  public equals(other: CommentAuthor): boolean {
    return this.id.equals(other.id);
  }

  public static from(user: User): CommentAuthor {
    if (user.isUser()) {
      throw new InsufficientRightsError(
        'Only workspace maintainer or admin can issue a comment',
      );
    }
    return new CommentAuthor(new CommentAuthorId(user.getId()));
  }
}
