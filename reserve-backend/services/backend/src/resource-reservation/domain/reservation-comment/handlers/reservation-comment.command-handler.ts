/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import {
  AggregateCommandHandler,
  CommandHandler,
} from '@rsaas/commons/lib/events';

import { ReservationComment } from '../reservation-comment.entity';
import {
  ReservationCommentCommand,
  ReservationCommentCommandType,
  CreateReservationCommentCommand,
  EditReservationCommentCommand,
  DeleteReservationCommentCommand,
} from '../reservation-comment.command';
import {
  ReservationCommentEvent,
  ReservationCommentEventType,
} from '../reservation-comment.event';
//#endregion

@Injectable()
export class ReservationCommentCommandHandler extends AggregateCommandHandler<
  ReservationComment,
  ReservationCommentEvent,
  ReservationCommentCommand
> {
  @CommandHandler(ReservationCommentCommandType.create)
  public executeCreate(
    aggregate: ReservationComment,
    { rowId, aggregateId, payload }: CreateReservationCommentCommand,
  ): ReservationCommentEvent[] {
    return [
      {
        type: ReservationCommentEventType.created,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          ...payload,
          author: payload.author.getId().get(),
        },
      },
    ];
  }

  @CommandHandler(ReservationCommentCommandType.edit)
  public executeEdit(
    aggregate: ReservationComment,
    { rowId, aggregateId, payload }: EditReservationCommentCommand,
  ): ReservationCommentEvent[] {
    if (aggregate.deleted) {
      throw new Error('Cannot edit deleted comment.');
    }
    return [
      {
        type: ReservationCommentEventType.edited,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          ...payload,
          changedBy: payload.changedBy.getId().get(),
        },
      },
    ];
  }

  @CommandHandler(ReservationCommentCommandType.delete)
  public executeDelete(
    aggregate: ReservationComment,
    { rowId, aggregateId, payload }: DeleteReservationCommentCommand,
  ): ReservationCommentEvent[] {
    if (aggregate.deleted) {
      return [];
    }
    return [
      {
        type: ReservationCommentEventType.deleted,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          ...payload,
          deletedBy: payload.deletedBy.getId().get(),
        },
      },
    ];
  }
}
