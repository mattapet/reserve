/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { AggregateEventHandler, EventHandler } from '@rsaas/commons/lib/events';

import { ReservationComment } from '../reservation-comment.entity';
import {
  ReservationCommentEvent,
  ReservationCommentEventType,
  ReservationCommentCreateEvent,
  ReservationCommentEditEvent,
} from '../reservation-comment.event';
import { CommentAuthor } from '../comment-author.entity';
import { CommentAuthorId } from '../values';
//#endregion

@Injectable()
export class ReservationCommentEventHandler extends AggregateEventHandler<
  ReservationComment,
  ReservationCommentEvent
> {
  @EventHandler(ReservationCommentEventType.created)
  public applyCreated(
    aggregate: ReservationComment,
    event: ReservationCommentCreateEvent,
  ): ReservationComment {
    aggregate.setId(event.aggregateId);
    aggregate.setWorkspaceId(event.payload.workspaceId);
    aggregate.setReservationId(event.payload.reservationId);
    aggregate.setComment(event.payload.comment);
    aggregate.setAuthor(
      new CommentAuthor(new CommentAuthorId(event.payload.author)),
    );
    aggregate.setTimestamp(event.timestamp);
    return aggregate;
  }

  @EventHandler(ReservationCommentEventType.edited)
  public applyEdited(
    aggregate: ReservationComment,
    event: ReservationCommentEditEvent,
  ): ReservationComment {
    aggregate.editComment(event.payload.comment);
    return aggregate;
  }

  @EventHandler(ReservationCommentEventType.deleted)
  public applyDeleted(aggregate: ReservationComment): ReservationComment {
    aggregate.delete();
    return aggregate;
  }
}
