/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';
import { AggregateRepository } from '@rsaas/commons/lib/events';

import { ReservationComment } from './reservation-comment.entity';
import {
  ReservationCommentCommandType,
  ReservationCommentCommand,
} from './reservation-comment.command';
import {
  InsufficientRightsError,
  ReservationCommentNotFoundError,
} from './reservation-comment.errors';
import { CreateReservationComment } from './values';
import { CommentAuthor } from './comment-author.entity';
import { Reservation } from '../reservation/entities/reservation.entity';
import { AGGREGATE_NAME, RESERVATION_COMMENT_REPOSITORY } from './constants';
import { ReservationCommentEvent } from './reservation-comment.event';
//#endregion

@Injectable()
export class ReservationCommentService {
  public constructor(
    @Inject(RESERVATION_COMMENT_REPOSITORY)
    private readonly repository: AggregateRepository<
      ReservationCommentCommand,
      ReservationCommentEvent,
      ReservationComment
    >,
  ) {}

  public get aggregateName(): string {
    return AGGREGATE_NAME;
  }

  // - MARK: Queries

  public async getById(
    reservation: Reservation,
    commentId: string,
  ): Promise<ReservationComment> {
    const comment = await this.repository.getById(
      `${
        this.aggregateName
      }:${reservation.getWorkspaceId()}:${reservation.getId()}`,
      commentId,
    );

    if (comment.hasNotBeenCreated() || comment.isDeleted()) {
      throw new ReservationCommentNotFoundError(
        `Reservation comment for reservation '${reservation.getId()}' with id ${commentId} was not found`,
      );
    }

    return comment;
  }

  public async getByReservation(
    reservation: Reservation,
  ): Promise<ReservationComment[]> {
    const comments = await this.repository.getAll(
      `${
        this.aggregateName
      }:${reservation.getWorkspaceId()}:${reservation.getId()}`,
    );

    return comments.filter((comment) => comment.isNotDeleted());
  }

  // - MARK: Commands

  public async create(
    comment: ReservationComment,
    createArgs: CreateReservationComment,
  ): Promise<void> {
    await this.repository.execute(this.makeCreateCommand(createArgs), comment);
  }

  public async edit(
    comment: ReservationComment,
    newComment: string,
    changedBy: CommentAuthor,
  ): Promise<void> {
    if (!comment.wasCreatedBy(changedBy)) {
      throw new InsufficientRightsError(
        'Only author of the comment can edit it.',
      );
    }

    await this.repository.execute(
      this.makeEditCommand(comment, newComment, changedBy),
      comment,
    );
  }

  public async delete(
    comment: ReservationComment,
    deletedBy: CommentAuthor,
  ): Promise<void> {
    if (!comment.wasCreatedBy(deletedBy)) {
      throw new InsufficientRightsError(
        'Only author of the comment can delete it.',
      );
    }

    await this.repository.execute(
      this.makeDeleteCommand(comment, deletedBy),
      comment,
    );
  }

  private makeCreateCommand({
    id,
    reservation,
    comment,
    author,
  }: CreateReservationComment): ReservationCommentCommand {
    return {
      type: ReservationCommentCommandType.create,
      rowId: this.getCommentRowId(
        reservation.getWorkspaceId(),
        reservation.getId(),
      ),
      aggregateId: id,
      payload: {
        reservationId: reservation.getId(),
        workspaceId: reservation.getWorkspaceId(),
        comment,
        author,
      },
    };
  }

  private makeEditCommand(
    comment: ReservationComment,
    newComment: string,
    changedBy: CommentAuthor,
  ): ReservationCommentCommand {
    return {
      type: ReservationCommentCommandType.edit,
      rowId: this.getCommentRowId(
        comment.getWorkspaceId(),
        comment.getReservationId(),
      ),
      aggregateId: comment.getId(),
      payload: { comment: newComment, changedBy },
    };
  }

  private makeDeleteCommand(
    comment: ReservationComment,
    deletedBy: CommentAuthor,
  ): ReservationCommentCommand {
    return {
      type: ReservationCommentCommandType.delete,
      rowId: this.getCommentRowId(
        comment.getWorkspaceId(),
        comment.getReservationId(),
      ),
      aggregateId: comment.getId(),
      payload: { deletedBy },
    };
  }

  private getCommentRowId(workspaceId: string, reservationId: string): string {
    return `${this.aggregateName}:${workspaceId}:${reservationId}`;
  }
}
