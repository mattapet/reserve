/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '@rsaas/commons/lib/events/command';
import { CommentAuthor } from './comment-author.entity';
//#endregion

export enum ReservationCommentCommandType {
  create = 'reservation-comment.create',
  edit = 'reservation-comment.edit',
  delete = 'reservation-comment.delete',
}

export interface CreateReservationCommentCommand extends Command {
  readonly type: ReservationCommentCommandType.create;
  readonly payload: {
    readonly workspaceId: string;
    readonly reservationId: string;
    readonly author: CommentAuthor;
    readonly comment: string;
  };
}

export interface EditReservationCommentCommand extends Command {
  readonly type: ReservationCommentCommandType.edit;
  readonly payload: {
    readonly comment: string;
    readonly changedBy: CommentAuthor;
  };
}

export interface DeleteReservationCommentCommand extends Command {
  readonly type: ReservationCommentCommandType.delete;
  readonly payload: {
    readonly deletedBy: CommentAuthor;
  };
}

export type ReservationCommentCommand =
  | CreateReservationCommentCommand
  | EditReservationCommentCommand
  | DeleteReservationCommentCommand;
