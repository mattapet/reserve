/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { CommentAuthor } from '../comment-author.entity';
import { Reservation } from '../../reservation/entities/reservation.entity';
//#endregion

export class CreateReservationComment {
  public constructor(
    public readonly id: string,
    public readonly reservation: Reservation,
    public readonly comment: string,
    public readonly author: CommentAuthor,
  ) {}
}
