/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserId } from '../../../../user-access';
//#endregion

export class CommentAuthorId {
  public constructor(private readonly id: UserId) {}

  public get(): UserId {
    return this.id;
  }

  public equals(other: CommentAuthorId): boolean {
    return this.id === other.id;
  }
}
