/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '@rsaas/commons/lib/events/event.entity';
//#endregion

export enum ReservationCommentEventType {
  created = 'reservation-comment.created',
  edited = 'reservation-comment.edited',
  deleted = 'reservation-comment.deleted',
}

export interface ReservationCommentCreateEvent extends Event {
  readonly type: ReservationCommentEventType.created;
  readonly payload: {
    readonly workspaceId: string;
    readonly reservationId: string;
    readonly author: string;
    readonly comment: string;
  };
}

export interface ReservationCommentEditEvent extends Event {
  readonly type: ReservationCommentEventType.edited;
  readonly payload: {
    readonly comment: string;
    readonly changedBy: string;
  };
}

export interface ReservationCommentDeleteEvent extends Event {
  readonly type: ReservationCommentEventType.deleted;
  readonly payload: {
    readonly deletedBy: string;
  };
}

export type ReservationCommentEvent =
  | ReservationCommentCreateEvent
  | ReservationCommentEditEvent
  | ReservationCommentDeleteEvent;
