/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';

import { Type } from 'class-transformer';
import { CommentAuthor } from './comment-author.entity';
//#endregion

export class ReservationComment {
  @Type(() => CommentAuthor)
  private author?: CommentAuthor;

  @Type(() => Date)
  private timestamp?: Date;

  public constructor(
    private id?: string,
    private workspaceId?: string,
    private reservationId?: string,
    private comment?: string,
    timestamp?: Date,
    author?: CommentAuthor,
  ) {
    this.timestamp = timestamp;
    this.author = author;
  }

  public deleted: boolean = false;

  public getId(): string {
    return notNull(this.id, 'Unexpected uninitialized reservation comment');
  }

  public getWorkspaceId(): string {
    return notNull(
      this.workspaceId,
      'Unexpected uninitialized reservation comment',
    );
  }

  public getReservationId(): string {
    return notNull(
      this.reservationId,
      'Unexpected uninitialized reservation comment',
    );
  }

  public getComment(): string {
    return notNull(
      this.comment,
      'Unexpected uninitialized reservation comment',
    );
  }

  public getTimestamp(): Date {
    return notNull(
      this.timestamp,
      'Unexpected uninitialized reservation comment',
    );
  }

  public getAuthor(): CommentAuthor {
    return notNull(this.author, 'Unexpected uninitialized reservation comment');
  }

  public wasCreatedBy(other: CommentAuthor): boolean {
    const author = notNull(
      this.author,
      'Unexpected uninitialized reservation comment',
    );
    return author.equals(other);
  }

  public hasBeenCreated(): boolean {
    return (
      !!this.id &&
      !!this.workspaceId &&
      !!this.reservationId &&
      !!this.comment &&
      !!this.timestamp &&
      !!this.author
    );
  }

  public hasNotBeenCreated(): boolean {
    return !this.hasBeenCreated();
  }

  public isDeleted(): boolean {
    return this.deleted;
  }

  public isNotDeleted(): boolean {
    return !this.isDeleted();
  }

  public setId(id: string): void {
    this.id = id;
  }

  public setWorkspaceId(workspaceId: string): void {
    this.workspaceId = workspaceId;
  }

  public setReservationId(reservationId: string): void {
    this.reservationId = reservationId;
  }

  public setComment(comment: string): void {
    this.comment = comment;
  }

  public setAuthor(author: CommentAuthor): void {
    this.author = author;
  }

  public editComment(comment: string): void {
    this.comment = comment;
  }

  public setTimestamp(timestamp: Date): void {
    this.timestamp = timestamp;
  }

  public delete(): void {
    this.deleted = true;
    this.comment = '';
  }
}
