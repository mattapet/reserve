/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';

import { ReservationService } from '../reservation';

import {
  ReservationProjectionRepository,
  ReservationProjection,
} from '../reservation-projection';
import { RESERVATION_PROJECTION_REPOSITORY } from '../reservation-projection/constants';
//#endregion

@Injectable()
export class ReservationCompletionService {
  public constructor(
    private readonly reservationService: ReservationService,
    @Inject(RESERVATION_PROJECTION_REPOSITORY)
    private readonly repository: ReservationProjectionRepository,
  ) {}

  public async completeReservations(): Promise<void> {
    await this.forEachReservationToComplete(
      this.completeReservation.bind(this),
    );
  }

  private async forEachReservationToComplete(
    fn: (reservation: ReservationProjection) => Promise<void>,
  ): Promise<void> {
    const reservationsToComplete = await this.repository.getAllToComplete();
    await reservationsToComplete.reduce(
      async (prev, nextReservationToComplete) => {
        await prev;
        await fn(nextReservationToComplete);
      },
      Promise.resolve(),
    );
  }

  private async completeReservation(
    reservation: ReservationProjection,
  ): Promise<void> {
    const { workspaceId, id } = reservation;
    const toComplete = await this.reservationService.getById(workspaceId, id);
    await this.reservationService.complete(toComplete);
  }
}
