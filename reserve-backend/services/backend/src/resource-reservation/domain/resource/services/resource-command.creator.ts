/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  CreateResourceCommand,
  ResourceCommandType,
  UpdateResourceCommand,
  ToggleActiveResourceCommand,
  DeleteResourceCommand,
} from '../resource.command';
import { Resource } from '../resource.entity';
import { UpdateResource, CreateResource } from '../values';
import { ResourceManager } from '../entities';
//#endregion

export class ResourceCommandCreator {
  private constructor() {}

  public static makeCreateResourceCommand({
    id,
    workspaceId,
    name,
    description,
    createdBy,
  }: CreateResource): CreateResourceCommand {
    return {
      type: ResourceCommandType.create,
      rowId: ResourceCommandCreator.getRowId(workspaceId),
      aggregateId: id,
      payload: {
        workspaceId,
        name,
        description,
        createdBy,
      },
    };
  }

  public static makeUpdateCommand(
    resource: Resource,
    { name, description, updatedBy }: UpdateResource,
  ): UpdateResourceCommand {
    return {
      type: ResourceCommandType.update,
      rowId: ResourceCommandCreator.getRowId(resource.getWorkspaceId()),
      aggregateId: resource.getId(),
      payload: { name, description, changedBy: updatedBy },
    };
  }

  public static makeToggleActiveCommand(
    resource: Resource,
    changedBy: ResourceManager,
  ): ToggleActiveResourceCommand {
    return {
      type: ResourceCommandType.toggleActive,
      rowId: ResourceCommandCreator.getRowId(resource.getWorkspaceId()),
      aggregateId: resource.getId(),
      payload: { changedBy },
    };
  }

  public static makeDeleteCommand(
    resource: Resource,
    deletedBy: ResourceManager,
  ): DeleteResourceCommand {
    return {
      type: ResourceCommandType.delete,
      rowId: ResourceCommandCreator.getRowId(resource.getWorkspaceId()),
      aggregateId: resource.getId(),
      payload: { deletedBy },
    };
  }

  private static getRowId(workspaceId: string): string {
    return `ResourceAggregate:${workspaceId}`;
  }
}
