/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '@rsaas/commons/lib/events/command';
import { ResourceManager } from './entities';
//#endregion

export enum ResourceCommandType {
  create = 'resource.create',
  update = 'resource.update',
  toggleActive = 'resource.toggle_active',
  delete = 'resource.delete',
}

export interface CreateResourceCommand extends Command {
  readonly type: ResourceCommandType.create;
  readonly payload: {
    readonly workspaceId: string;
    readonly name: string;
    readonly description: string;
    readonly createdBy: ResourceManager;
  };
}

export interface UpdateResourceCommand extends Command {
  readonly type: ResourceCommandType.update;
  readonly payload: {
    readonly name: string;
    readonly description: string;
    readonly changedBy: ResourceManager;
  };
}

export interface ToggleActiveResourceCommand extends Command {
  readonly type: ResourceCommandType.toggleActive;
  readonly payload: {
    readonly changedBy: ResourceManager;
  };
}

export interface DeleteResourceCommand extends Command {
  readonly type: ResourceCommandType.delete;
  readonly payload: {
    readonly deletedBy: ResourceManager;
  };
}

export type ResourceCommand =
  | CreateResourceCommand
  | UpdateResourceCommand
  | ToggleActiveResourceCommand
  | DeleteResourceCommand;
