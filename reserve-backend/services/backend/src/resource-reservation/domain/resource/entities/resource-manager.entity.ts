/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { User } from '../../../../user-access';

import { InsufficientRightsError } from '../resource.errors';
//#endregion

export class ResourceManager {
  public constructor(private readonly userId: string) {}

  public getId(): string {
    return this.userId;
  }

  public static from(user: User): ResourceManager {
    if (user.isUser()) {
      throw new InsufficientRightsError('User cannot manage resources');
    }
    return new ResourceManager(user.getId());
  }
}
