/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Resource } from '../../resource.entity';
import { UpdateResourceCommand } from '../../resource.command';
import { ResourceEvent, ResourceEventType } from '../../resource.event';
//#endregion

export function resourceUpdateCommandHandler(
  aggregate: Resource,
  command: UpdateResourceCommand,
): ResourceEvent[] {
  const result: ResourceEvent[] = [];

  if (nameChanged(aggregate, command)) {
    result.push({
      type: ResourceEventType.nameSet,
      rowId: command.rowId,
      aggregateId: command.aggregateId,
      timestamp: new Date(Date.now()),
      payload: {
        name: command.payload.name,
        changedBy: command.payload.changedBy.getId(),
      },
    });
  }

  if (descriptionChanged(aggregate, command)) {
    result.push({
      type: ResourceEventType.descriptionSet,
      rowId: command.rowId,
      aggregateId: command.aggregateId,
      timestamp: new Date(Date.now()),
      payload: {
        description: command.payload.description,
        changedBy: command.payload.changedBy.getId(),
      },
    });
  }

  return result;
}

function descriptionChanged(
  aggregate: Resource,
  command: UpdateResourceCommand,
) {
  return aggregate.getDescription() !== command.payload.description;
}

function nameChanged(aggregate: Resource, command: UpdateResourceCommand) {
  return aggregate.getName() !== command.payload.name;
}
