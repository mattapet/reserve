/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { AggregateEventHandler, EventHandler } from '@rsaas/commons/lib/events';

import { Resource } from '../resource.entity';
import {
  ResourceEvent,
  ResourceEventType,
  ResourceDescriptionSetEvent,
  ResourceNameSetEvent,
  ResourceCreatedEvent,
} from '../resource.event';
//#endregion

@Injectable()
export class ResourceEventHandler extends AggregateEventHandler<
  Resource,
  ResourceEvent
> {
  @EventHandler(ResourceEventType.created)
  public applyCreated(
    aggregate: Resource,
    event: ResourceCreatedEvent,
  ): Resource {
    aggregate.setId(event.aggregateId);
    aggregate.setWorkspaceId(event.payload.workspaceId);
    return aggregate;
  }

  @EventHandler(ResourceEventType.nameSet)
  public applyNameSet(
    aggregate: Resource,
    event: ResourceNameSetEvent,
  ): Resource {
    aggregate.setName(event.payload.name);
    return aggregate;
  }

  @EventHandler(ResourceEventType.descriptionSet)
  public applyDescriptionSet(
    aggregate: Resource,
    event: ResourceDescriptionSetEvent,
  ): Resource {
    aggregate.setDescription(event.payload.description);
    return aggregate;
  }

  @EventHandler(ResourceEventType.activated)
  public applyActivated(aggregate: Resource): Resource {
    aggregate.activate();
    return aggregate;
  }

  @EventHandler(ResourceEventType.deactivated)
  public applyDeactivated(aggregate: Resource): Resource {
    aggregate.deactivate();
    return aggregate;
  }

  @EventHandler(ResourceEventType.deleted)
  public applyDeleted(aggregate: Resource): Resource {
    aggregate.delete();
    return aggregate;
  }
}
