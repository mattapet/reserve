/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import {
  AggregateCommandHandler,
  CommandHandler,
} from '@rsaas/commons/lib/events';

import { Resource } from '../../resource.entity';
import {
  ResourceCommand,
  ResourceCommandType,
  CreateResourceCommand,
  DeleteResourceCommand,
  ToggleActiveResourceCommand,
  UpdateResourceCommand,
} from '../../resource.command';
import { ResourceEvent, ResourceEventType } from '../../resource.event';
import { resourceUpdateCommandHandler } from './resource.update-command-handler';
//#endregion

@Injectable()
export class ResourceCommandHandler extends AggregateCommandHandler<
  Resource,
  ResourceEvent,
  ResourceCommand
> {
  @CommandHandler(ResourceCommandType.create)
  public executeCreate(
    aggregate: Resource,
    { rowId, aggregateId, payload }: CreateResourceCommand,
  ): ResourceEvent[] {
    return [
      {
        type: ResourceEventType.created,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          workspaceId: payload.workspaceId,
          createdBy: payload.createdBy.getId(),
        },
      },
      {
        type: ResourceEventType.nameSet,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          name: payload.name,
          changedBy: payload.createdBy.getId(),
        },
      },
      {
        type: ResourceEventType.descriptionSet,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          description: payload.description,
          changedBy: payload.createdBy.getId(),
        },
      },
    ];
  }

  @CommandHandler(ResourceCommandType.update)
  public executeUpdate(
    aggregate: Resource,
    command: UpdateResourceCommand,
  ): ResourceEvent[] {
    return resourceUpdateCommandHandler(aggregate, command);
  }

  @CommandHandler(ResourceCommandType.toggleActive)
  public executeToggleActive(
    aggregate: Resource,
    { rowId, aggregateId, payload }: ToggleActiveResourceCommand,
  ): ResourceEvent[] {
    if (aggregate.isActive()) {
      return [
        {
          type: ResourceEventType.deactivated,
          rowId,
          aggregateId,
          timestamp: new Date(Date.now()),
          payload: {
            ...payload,
            changedBy: payload.changedBy.getId(),
          },
        },
      ];
    } else {
      return [
        {
          type: ResourceEventType.activated,
          rowId,
          aggregateId,
          timestamp: new Date(Date.now()),
          payload: {
            ...payload,
            changedBy: payload.changedBy.getId(),
          },
        },
      ];
    }
  }

  @CommandHandler(ResourceCommandType.delete)
  public executeDelete(
    aggregate: Resource,
    { rowId, aggregateId, payload }: DeleteResourceCommand,
  ): ResourceEvent[] {
    if (aggregate.isDeleted()) {
      return [];
    }
    return [
      {
        type: ResourceEventType.deleted,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          ...payload,
          deletedBy: payload.deletedBy.getId(),
        },
      },
    ];
  }
}
