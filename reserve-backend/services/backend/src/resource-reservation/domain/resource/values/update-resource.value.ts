/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ResourceManager } from '../entities';
//#endregion

export class UpdateResource {
  public constructor(
    public readonly name: string,
    public readonly description: string,
    public readonly updatedBy: ResourceManager,
  ) {}
}
