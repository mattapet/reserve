/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';
//#endregion

export type ResourceId = string;

export class Resource {
  private active: boolean = true;

  private deleted: boolean = false;

  public constructor(
    private id?: ResourceId,
    private workspaceId?: string,
    private name?: string,
    private description?: string,
  ) {}

  public getId(): ResourceId {
    return notNull(this.id, 'Uninitialized resource');
  }

  public getWorkspaceId(): string {
    return notNull(this.workspaceId, 'Uninitialized resource');
  }

  public getName(): string {
    return notNull(this.name, 'Uninitialized resource');
  }

  public getDescription(): string {
    return notNull(this.description, 'Uninitialized resource');
  }

  public isActive(): boolean {
    return this.active;
  }

  public hasBeenCreated(): boolean {
    return !!this.id && !!this.workspaceId && !!this.name && !!this.description;
  }

  public hasNotBeenCreated(): boolean {
    return !this.hasBeenCreated();
  }

  public isDeleted(): boolean {
    return this.deleted;
  }

  public isNotDeleted(): boolean {
    return !this.isDeleted();
  }

  public setId(id: ResourceId): void {
    this.id = id;
  }

  public setWorkspaceId(workspaceId: string): void {
    this.workspaceId = workspaceId;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public setDescription(description: string): void {
    this.description = description;
  }

  public activate(): void {
    this.active = true;
  }

  public deactivate(): void {
    this.active = false;
  }

  public delete(): void {
    this.deleted = true;
  }
}
