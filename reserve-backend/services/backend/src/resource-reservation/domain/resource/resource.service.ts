/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';
import { AggregateRepository } from '@rsaas/commons/lib/events';

import { Resource, ResourceId } from './resource.entity';
import { CreateResource, UpdateResource } from './values';
import { ResourceNotFoundError } from './resource.errors';
import { ResourceCommandCreator } from './services/resource-command.creator';
import { ResourceManager } from './entities';
import { RESOURCE_REPOSITORY } from './constants';
import { ResourceCommand } from './resource.command';
import { ResourceEvent } from './resource.event';
//#endregion

@Injectable()
export class ResourceService {
  public constructor(
    @Inject(RESOURCE_REPOSITORY)
    private readonly repository: AggregateRepository<
      ResourceCommand,
      ResourceEvent,
      Resource
    >,
  ) {}

  // Queries

  public async getAll(workspaceId: string): Promise<Resource[]> {
    const resources = await this.repository.getAll(
      this.getRowIdForWorkspaceId(workspaceId),
    );
    return resources.filter((r) => r.isNotDeleted());
  }

  public async getById(workspaceId: string, id: ResourceId): Promise<Resource> {
    const resource = await this.repository.getById(
      this.getRowIdForWorkspaceId(workspaceId),
      id,
    );

    if (resource.hasNotBeenCreated() || resource.isDeleted()) {
      throw new ResourceNotFoundError(`Resource with id ${id} does not exist`);
    }

    return resource;
  }

  public async getByIds(
    workspaceId: string,
    ids: ResourceId[],
  ): Promise<Resource[]> {
    return Promise.all(ids.map((id) => this.getById(workspaceId, id)));
  }

  // Commands

  public async create(
    resource: Resource,
    createParams: CreateResource,
  ): Promise<void> {
    await this.repository.execute(
      ResourceCommandCreator.makeCreateResourceCommand(createParams),
      resource,
    );
  }

  public async update(
    resource: Resource,
    updateParams: UpdateResource,
  ): Promise<void> {
    await this.repository.execute(
      ResourceCommandCreator.makeUpdateCommand(resource, updateParams),
      resource,
    );
  }

  public async toggle(
    resource: Resource,
    manager: ResourceManager,
  ): Promise<void> {
    await this.repository.execute(
      ResourceCommandCreator.makeToggleActiveCommand(resource, manager),
      resource,
    );
  }

  public async delete(
    resource: Resource,
    manager: ResourceManager,
  ): Promise<void> {
    await this.repository.execute(
      ResourceCommandCreator.makeDeleteCommand(resource, manager),
      resource,
    );
  }

  private getRowIdForWorkspaceId(workspaceId: string): string {
    return `ResourceAggregate:${workspaceId}`;
  }
}
