/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '@rsaas/commons/lib/events/event.entity';
import { UserId } from '../../../user-access';
//#endregion

export enum ResourceEventType {
  created = 'resource.created',
  nameSet = 'resource.name_set',
  descriptionSet = 'resource.description_set',
  activated = 'resource.activated',
  deactivated = 'resource.deactivated',
  deleted = 'resource.deleted',
}

export interface ResourceCreatedEvent extends Event {
  readonly type: ResourceEventType.created;
  readonly payload: {
    readonly workspaceId: string;
    readonly createdBy: UserId;
  };
}

export interface ResourceNameSetEvent extends Event {
  readonly type: ResourceEventType.nameSet;
  readonly payload: {
    readonly name: string;
    readonly changedBy: UserId;
  };
}

export interface ResourceDescriptionSetEvent extends Event {
  readonly type: ResourceEventType.descriptionSet;
  readonly payload: {
    readonly description: string;
    readonly changedBy: UserId;
  };
}

export interface ResourceActivatedEvent extends Event {
  readonly type: ResourceEventType.activated;
  readonly payload: {
    readonly changedBy: UserId;
  };
}

export interface ResourceDeactivatedEvent extends Event {
  readonly type: ResourceEventType.deactivated;
  readonly payload: {
    readonly changedBy: UserId;
  };
}

export interface ResourceDeletedEvent extends Event {
  readonly type: ResourceEventType.deleted;
  readonly payload: {
    readonly deletedBy: UserId;
  };
}

export type ResourceEvent =
  | ResourceCreatedEvent
  | ResourceNameSetEvent
  | ResourceDescriptionSetEvent
  | ResourceActivatedEvent
  | ResourceDeactivatedEvent
  | ResourceDeletedEvent;
