/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '@rsaas/commons/lib/events/event.entity';
import { UserId } from '../../../user-access';
import { ResourceId } from '../resource/resource.entity';
//#endregion

export enum ReservationEventType {
  created = 'reservation.created',
  dateChanged = 'reservation.date_changed',
  assigned = 'reservation.assigned',
  resourceAdded = 'reservation.resource_added',
  resourceRemoved = 'reservation.resource_removed',

  confirmed = 'reservation.state_changed.confirmed',
  canceled = 'reservation.state_changed.canceled',
  rejected = 'reservation.state_changed.rejected',
  completed = 'reservation.state_changed.completed',
}

export interface ReservationCreatedEvent extends Event {
  readonly type: ReservationEventType.created;
  readonly payload: {
    readonly workspaceId: string;
    readonly dateStart: string;
    readonly dateEnd: string;
    readonly createdBy: UserId;
    readonly notes?: string;
  };
}

export interface ReservationDateChangedEvent extends Event {
  readonly type: ReservationEventType.dateChanged;
  readonly payload: {
    readonly dateStart: string;
    readonly dateEnd: string;
    readonly changedBy: UserId;
  };
}

export interface ReservationAssignedEvent extends Event {
  readonly type: ReservationEventType.assigned;
  readonly payload: {
    readonly assignedTo: UserId;
    readonly changedBy: UserId;
  };
}

export interface ReservationResourceAddedEvent extends Event {
  readonly type: ReservationEventType.resourceAdded;
  readonly payload: {
    readonly resource: ResourceId;
    readonly changedBy: UserId;
  };
}

export interface ReservationResourceRemovedEvent extends Event {
  readonly type: ReservationEventType.resourceRemoved;
  readonly payload: {
    readonly resource: ResourceId;
    readonly changedBy: UserId;
  };
}

export interface ReservationConfirmedEvent extends Event {
  readonly type: ReservationEventType.confirmed;
  readonly payload: {
    readonly changedBy: UserId;
  };
}

export interface ReservationCanceledEvent extends Event {
  readonly type: ReservationEventType.canceled;
  readonly payload: {
    readonly changedBy: UserId;
  };
}

export interface ReservationRejectedEvent extends Event {
  readonly type: ReservationEventType.rejected;
  readonly payload: {
    readonly reason: string;
    readonly changedBy: UserId;
  };
}

export interface ReservationCompletedEvent extends Event {
  readonly type: ReservationEventType.completed;
}

export type ReservationStateEvent =
  | ReservationConfirmedEvent
  | ReservationCanceledEvent
  | ReservationRejectedEvent
  | ReservationCompletedEvent;

export type ReservationEvent =
  | ReservationCreatedEvent
  | ReservationDateChangedEvent
  | ReservationAssignedEvent
  | ReservationResourceAddedEvent
  | ReservationResourceRemovedEvent
  | ReservationStateEvent;
