/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import {
  CommandHandler,
  AggregateCommandHandler,
} from '@rsaas/commons/lib/events';

import {
  ReservationCommand,
  ReservationCommandType,
  ReservationCreateCommand,
  ReservationEditCommand,
  ReservationStateCommand,
} from '../../reservation.command';
import { ReservationEvent } from '../../reservation.event';

import { ReservationCreateCommandHandler } from './reservation.create-command-handler';
import { ReservationEditCommandHandler } from './reservation.edit-command-handler';
import { ReservationStateCommandHandler } from './reservation.state-command-handler';
import { Reservation } from '../../entities/reservation.entity';
//#endregion

@Injectable()
export class ReservationCommandHandler extends AggregateCommandHandler<
  Reservation,
  ReservationEvent,
  ReservationCommand
> {
  private readonly createHandler = new ReservationCreateCommandHandler();
  private readonly editHandler = new ReservationEditCommandHandler();
  private readonly stateHandler = new ReservationStateCommandHandler();

  @CommandHandler(ReservationCommandType.create)
  public executeCreate(
    aggregate: Reservation,
    command: ReservationCreateCommand,
  ): ReservationEvent[] {
    return this.createHandler.execute(aggregate, command);
  }

  @CommandHandler(ReservationCommandType.edit)
  public executeEdit(
    aggregate: Reservation,
    command: ReservationEditCommand,
  ): ReservationEvent[] {
    return this.editHandler.execute(aggregate, command);
  }

  @CommandHandler(ReservationCommandType.confirm)
  @CommandHandler(ReservationCommandType.cancel)
  @CommandHandler(ReservationCommandType.reject)
  @CommandHandler(ReservationCommandType.complete)
  public executeStateChange(
    aggregate: Reservation,
    command: ReservationStateCommand,
  ): ReservationEvent[] {
    return this.stateHandler.execute(aggregate, command);
  }
}
