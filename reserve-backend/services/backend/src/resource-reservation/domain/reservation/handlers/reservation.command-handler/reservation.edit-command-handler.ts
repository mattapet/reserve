/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationEditCommand } from '../../reservation.command';
import {
  ReservationEvent,
  ReservationEventType,
} from '../../reservation.event';
import { InvalidReservationStateError } from '../../reservation.errors';
import { Reservation } from '../../entities';
import { ResourceId } from '../../../resource/resource.entity';
//#endregion

export class ReservationEditCommandHandler {
  public execute(
    aggregate: Reservation,
    command: ReservationEditCommand,
  ): ReservationEvent[] {
    const resultEvents: ReservationEvent[] = [];

    if (!aggregate.isEditable()) {
      throw new InvalidReservationStateError(
        'Only requested reservations can be edited.',
      );
    }

    if (this.shouldDateChange(aggregate, command)) {
      resultEvents.push(this.produceDateChange(command));
    }

    if (this.shouldUserChange(aggregate, command)) {
      resultEvents.push(this.produceUserChange(command));
    }

    this.produceResourcesAdded(
      this.resourceDifference(
        command.payload.resources,
        aggregate.getResourceIds(),
      ),
      command,
    ).forEach((event) => resultEvents.push(event));

    this.produceResourcesRemoved(
      this.resourceDifference(
        aggregate.getResourceIds(),
        command.payload.resources,
      ),
      command,
    ).forEach((event) => resultEvents.push(event));

    return resultEvents;
  }

  private resourceDifference(
    updated: ResourceId[],
    original: ResourceId[],
  ): ResourceId[] {
    return updated.filter((resource) => !original.includes(resource));
  }

  private shouldDateChange(
    aggregate: Reservation,
    command: ReservationEditCommand,
  ): boolean {
    return (
      +aggregate.getDateStart() !== +command.payload.dateStart ||
      +aggregate.getDateEnd() !== +command.payload.dateEnd
    );
  }

  private produceDateChange(command: ReservationEditCommand): ReservationEvent {
    return {
      type: ReservationEventType.dateChanged,
      rowId: command.rowId,
      aggregateId: command.aggregateId,
      timestamp: new Date(Date.now()),
      payload: {
        dateStart: command.payload.dateStart.toISOString(),
        dateEnd: command.payload.dateEnd.toISOString(),
        changedBy: command.payload.changedBy.getId(),
      },
    };
  }

  private shouldUserChange(
    aggregate: Reservation,
    command: ReservationEditCommand,
  ): boolean {
    return !aggregate.isAssignedTo(command.payload.assignee);
  }

  private produceUserChange(command: ReservationEditCommand): ReservationEvent {
    return {
      type: ReservationEventType.assigned,
      rowId: command.rowId,
      aggregateId: command.aggregateId,
      timestamp: new Date(Date.now()),
      payload: {
        assignedTo: command.payload.assignee.getId(),
        changedBy: command.payload.changedBy.getId(),
      },
    };
  }

  private produceResourcesAdded(
    resources: ResourceId[],
    command: ReservationEditCommand,
  ): ReservationEvent[] {
    return resources.map((resource) => ({
      type: ReservationEventType.resourceAdded,
      rowId: command.rowId,
      aggregateId: command.aggregateId,
      timestamp: new Date(Date.now()),
      payload: {
        resource,
        changedBy: command.payload.changedBy.getId(),
      },
    }));
  }

  private produceResourcesRemoved(
    resources: ResourceId[],
    command: ReservationEditCommand,
  ): ReservationEvent[] {
    return resources.map((resource) => ({
      type: ReservationEventType.resourceRemoved,
      rowId: command.rowId,
      aggregateId: command.aggregateId,
      timestamp: new Date(Date.now()),
      payload: {
        resource,
        changedBy: command.payload.changedBy.getId(),
      },
    }));
  }
}
