/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationCreateCommand } from '../../reservation.command';
import {
  ReservationEvent,
  ReservationEventType,
} from '../../reservation.event';
import { Reservation } from '../../entities/reservation.entity';
//#endregion

export class ReservationCreateCommandHandler {
  public execute(
    aggregate: Reservation,
    command: ReservationCreateCommand,
  ): ReservationEvent[] {
    return [
      this.produceCreate(command),
      this.produceUserAssign(command),
      ...this.produceResourcesAdded(command),
    ];
  }

  private produceCreate(command: ReservationCreateCommand): ReservationEvent {
    return {
      type: ReservationEventType.created,
      rowId: command.rowId,
      aggregateId: command.aggregateId,
      timestamp: new Date(Date.now()),
      payload: {
        workspaceId: command.payload.workspaceId,
        dateStart: command.payload.dateStart.toISOString(),
        dateEnd: command.payload.dateEnd.toISOString(),
        notes: command.payload.notes,
        createdBy: command.payload.createdBy.getId(),
      },
    };
  }

  private produceUserAssign(
    command: ReservationCreateCommand,
  ): ReservationEvent {
    return {
      type: ReservationEventType.assigned,
      rowId: command.rowId,
      aggregateId: command.aggregateId,
      timestamp: new Date(Date.now()),
      payload: {
        assignedTo: command.payload.assignee.getId(),
        changedBy: command.payload.createdBy.getId(),
      },
    };
  }

  private produceResourcesAdded(
    command: ReservationCreateCommand,
  ): ReservationEvent[] {
    return command.payload.resources.map((resource) => ({
      type: ReservationEventType.resourceAdded,
      rowId: command.rowId,
      aggregateId: command.aggregateId,
      timestamp: new Date(Date.now()),
      payload: {
        resource,
        changedBy: command.payload.createdBy.getId(),
      },
    }));
  }
}

// Private helpers implementation
