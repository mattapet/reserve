/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { AggregateEventHandler, EventHandler } from '@rsaas/commons/lib/events';

import {
  ReservationEvent,
  ReservationEventType,
  ReservationCreatedEvent,
  ReservationAssignedEvent,
  ReservationDateChangedEvent,
  ReservationResourceAddedEvent,
  ReservationResourceRemovedEvent,
} from '../reservation.event';
import { Reservation } from '../entities/reservation.entity';
import { Assignee } from '../entities/assignee.entity';
//#endregion

@Injectable()
export class ReservationEventHandler extends AggregateEventHandler<
  Reservation,
  ReservationEvent
> {
  @EventHandler(ReservationEventType.created)
  public applyCreated(
    aggregate: Reservation,
    event: ReservationCreatedEvent,
  ): Reservation {
    aggregate.setId(event.aggregateId);
    aggregate.setWorkspaceId(event.payload.workspaceId);
    aggregate.setDateStart(new Date(event.payload.dateStart));
    aggregate.setDateEnd(new Date(event.payload.dateEnd));
    aggregate.setCreatedAt(event.timestamp);
    event.payload.notes && aggregate.setNotes(event.payload.notes);
    return aggregate;
  }

  @EventHandler(ReservationEventType.dateChanged)
  public applyDateChanged(
    aggregate: Reservation,
    event: ReservationDateChangedEvent,
  ): Reservation {
    aggregate.setDateStart(new Date(event.payload.dateStart));
    aggregate.setDateEnd(new Date(event.payload.dateEnd));
    return aggregate;
  }

  @EventHandler(ReservationEventType.assigned)
  public applyAssigned(
    aggregate: Reservation,
    event: ReservationAssignedEvent,
  ): Reservation {
    aggregate.assignTo(new Assignee(event.payload.assignedTo));
    return aggregate;
  }

  @EventHandler(ReservationEventType.resourceAdded)
  public applyResourceAdded(
    aggregate: Reservation,
    event: ReservationResourceAddedEvent,
  ): Reservation {
    aggregate.addResource(event.payload.resource);
    return aggregate;
  }

  @EventHandler(ReservationEventType.resourceRemoved)
  public applyResourceRemoved(
    aggregate: Reservation,
    event: ReservationResourceRemovedEvent,
  ): Reservation {
    aggregate.removeResource(event.payload.resource);
    return aggregate;
  }

  @EventHandler(ReservationEventType.confirmed)
  public applyConfirmed(aggregate: Reservation): Reservation {
    aggregate.confirm();
    return aggregate;
  }

  @EventHandler(ReservationEventType.canceled)
  public applyCanceled(aggregate: Reservation): Reservation {
    aggregate.cancel();
    return aggregate;
  }

  @EventHandler(ReservationEventType.rejected)
  public applyRejected(aggregate: Reservation): Reservation {
    aggregate.reject();
    return aggregate;
  }

  @EventHandler(ReservationEventType.completed)
  public applyCompleted(aggregate: Reservation): Reservation {
    aggregate.complete();
    return aggregate;
  }
}
