/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  AggregateCommandHandler,
  CommandHandler,
} from '@rsaas/commons/lib/events';

import { ReservationState } from '../../reservation-state.type';
import { Reservation } from '../../entities/reservation.entity';
import {
  ReservationCommandType,
  ReservationCommand,
  ReservationCancelCommand,
  ReservationCompleteCommand,
  ReservationConfirmCommand,
  ReservationRejectCommand,
} from '../../reservation.command';
import {
  ReservationEvent,
  ReservationEventType,
} from '../../reservation.event';
//#endregion

export class ReservationStateCommandHandler extends AggregateCommandHandler<
  Reservation,
  ReservationEvent,
  ReservationCommand
> {
  @CommandHandler(ReservationCommandType.confirm)
  public executeConfirm(
    aggregate: Reservation,
    { rowId, aggregateId, payload }: ReservationConfirmCommand,
  ): ReservationEvent[] {
    switch (aggregate.getState()) {
      case ReservationState.confirmed:
        return [];
      case ReservationState.requested:
      case ReservationState.canceled:
      case ReservationState.rejected:
        return [
          {
            type: ReservationEventType.confirmed,
            rowId,
            aggregateId,
            timestamp: new Date(Date.now()),
            payload: {
              ...payload,
              changedBy: payload.changedBy.getId(),
            },
          },
        ];
      default:
        throw new Error('Invalid state change');
    }
  }

  @CommandHandler(ReservationCommandType.cancel)
  public executeCancel(
    aggregate: Reservation,
    { rowId, aggregateId, payload }: ReservationCancelCommand,
  ): ReservationEvent[] {
    switch (aggregate.getState()) {
      case ReservationState.canceled:
        return [];
      case ReservationState.requested:
      case ReservationState.confirmed:
        return [
          {
            type: ReservationEventType.canceled,
            rowId,
            aggregateId,
            timestamp: new Date(Date.now()),
            payload: {
              ...payload,
              changedBy: payload.changedBy.getId(),
            },
          },
        ];
      default:
        throw new Error('Invalid state change');
    }
  }

  @CommandHandler(ReservationCommandType.reject)
  public executeReject(
    aggregate: Reservation,
    { rowId, aggregateId, payload }: ReservationRejectCommand,
  ): ReservationEvent[] {
    switch (aggregate.getState()) {
      case ReservationState.rejected:
        return [];
      case ReservationState.requested:
        return [
          {
            type: ReservationEventType.rejected,
            rowId,
            aggregateId,
            timestamp: new Date(Date.now()),
            payload: {
              ...payload,
              changedBy: payload.changedBy.getId(),
            },
          },
        ];
      default:
        throw new Error('Invalid state change');
    }
  }

  @CommandHandler(ReservationCommandType.complete)
  public executeComplete(
    aggregate: Reservation,
    { rowId, aggregateId }: ReservationCompleteCommand,
  ): ReservationEvent[] {
    switch (aggregate.getState()) {
      case ReservationState.completed:
        return [];
      case ReservationState.confirmed:
        return [
          {
            type: ReservationEventType.completed,
            rowId,
            aggregateId,
            timestamp: new Date(Date.now()),
            payload: {},
          },
        ];
      default:
        throw new Error('Invalid state change');
    }
  }
}
