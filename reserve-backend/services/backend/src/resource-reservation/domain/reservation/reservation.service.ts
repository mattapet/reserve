/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';
import { AggregateRepository } from '@rsaas/commons/lib/events';

import {
  ReservationCommandType,
  ReservationCommand,
} from './reservation.command';
import {
  InvalidDateRangeError,
  ReservationNotFoundError,
  InsufficientRightsError,
} from './reservation.errors';
import { ReservationEvent } from './reservation.event';
import { Reservation } from './entities/reservation.entity';
import { StateService } from './services/state.service';
import { ReservationManager } from './entities';
import { CreateReservation, UpdateReservation } from './values';
import { ReservationProjectionRepository } from '../reservation-projection';

import { RESERVATION_REPOSITORY, AGGREGATE_NAME } from './constants';
import { RESERVATION_PROJECTION_REPOSITORY } from '../reservation-projection/constants';
//#endregion

@Injectable()
export class ReservationService {
  private readonly stateService: StateService;

  public constructor(
    @Inject(RESERVATION_REPOSITORY)
    private readonly repository: AggregateRepository<
      ReservationCommand,
      ReservationEvent,
      Reservation
    >,
    @Inject(RESERVATION_PROJECTION_REPOSITORY)
    private readonly searchRepo: ReservationProjectionRepository,
  ) {
    this.stateService = new StateService(this.repository, this.searchRepo);
  }

  public get aggregateName(): string {
    return AGGREGATE_NAME;
  }

  // - MARK: Queries

  public async getAllEvents(
    workspaceId: string,
    reservationId: string,
  ): Promise<ReservationEvent[]> {
    return this.repository.dumpEvents(
      this.getRowIdForWorkspace(workspaceId),
      reservationId,
    );
  }

  public async getById(
    workspaceId: string,
    reservationId: string,
  ): Promise<Reservation> {
    const reservation = await this.repository.getById(
      this.getRowIdForWorkspace(workspaceId),
      reservationId,
    );

    if (reservation.hasNotBeenCreated()) {
      throw new ReservationNotFoundError(
        `Reservation with id ${reservationId} was not found`,
      );
    }
    return reservation;
  }

  // - MARK: Commands

  public async create(
    aggregate: Reservation,
    params: CreateReservation,
  ): Promise<void> {
    const { assignee, createdBy } = params;
    if (!assignee.equals(createdBy) && createdBy.cannotReassignReservations()) {
      throw new InsufficientRightsError(
        `Only Maintainers or admins can assign else's` +
          'reservation to someone else.',
      );
    }

    await this.repository.execute(this.makeCreateCommand(params), aggregate);
  }

  public async editReservation(
    reservation: Reservation,
    params: UpdateReservation,
  ): Promise<void> {
    const { assignee, updatedBy } = params;

    if (
      reservation.isNotAssignedTo(updatedBy) &&
      updatedBy.cannotEditUnownedReservations()
    ) {
      throw new InsufficientRightsError(
        `Only Maintainers or admins can edit someone else's reservations`,
      );
    }

    if (
      reservation.isNotAssignedTo(assignee) &&
      updatedBy.cannotReassignReservations()
    ) {
      throw new InsufficientRightsError(
        `Only Maintainers or admins can edit someone else's reservations`,
      );
    }

    if (reservation.isReservationStartInPast()) {
      throw new InvalidDateRangeError('Date start cannot be in the past');
    }

    await this.repository.execute(
      this.makeEditReservationCommand(reservation, params),
      reservation,
    );
  }

  private getRowIdForWorkspace(workspaceId: string): string {
    return `${this.aggregateName}:${workspaceId}`;
  }

  public async confirm(
    reservation: Reservation,
    changedBy: ReservationManager,
  ): Promise<void> {
    await this.stateService.confirm(reservation, changedBy);
  }

  public async cancel(
    reservation: Reservation,
    changedBy: ReservationManager,
  ): Promise<void> {
    await this.stateService.cancel(reservation, changedBy);
  }

  public async reject(
    reservation: Reservation,
    changedBy: ReservationManager,
    reason: string,
  ): Promise<void> {
    await this.stateService.reject(reservation, changedBy, reason);
  }

  public async complete(reservation: Reservation): Promise<void> {
    await this.stateService.complete(reservation);
  }

  private makeCreateCommand(params: CreateReservation): ReservationCommand {
    return {
      type: ReservationCommandType.create,
      rowId: this.getRowIdForWorkspace(params.workspaceId),
      aggregateId: params.id,
      payload: {
        workspaceId: params.workspaceId,
        dateStart: params.dateStart,
        dateEnd: params.dateEnd,
        assignee: params.assignee,
        createdBy: params.createdBy,
        resources: params.resources,
        notes: params.notes,
      },
    };
  }

  private makeEditReservationCommand(
    reservation: Reservation,
    params: UpdateReservation,
  ): ReservationCommand {
    return {
      type: ReservationCommandType.edit,
      rowId: this.getRowIdForWorkspace(params.workspaceId),
      aggregateId: reservation.getId(),
      payload: {
        dateStart: params.dateStart,
        dateEnd: params.dateEnd,
        resources: params.resources,
        notes: params.notes,
        assignee: params.assignee,
        changedBy: params.updatedBy,
      },
    };
  }
}
