/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ResourceId } from '../../resource/resource.entity';
import { Assignee, ReservationManager } from '../entities';
import { InvalidDateRangeError } from '../reservation.errors';
//#endregion

export class CreateReservation {
  public constructor(
    public readonly id: string,
    public readonly workspaceId: string,
    public readonly dateStart: Date,
    public readonly dateEnd: Date,
    public readonly resources: ResourceId[],
    public readonly notes: string | undefined,
    public readonly assignee: Assignee,
    public readonly createdBy: ReservationManager,
  ) {
    this.validate();
  }

  private validate() {
    if (+this.dateStart < Date.now()) {
      throw new InvalidDateRangeError('Reservation cannot start in the past.');
    }

    if (+this.dateEnd < +this.dateStart) {
      throw new InvalidDateRangeError(
        'Reservation cannot end before it starts.',
      );
    }
  }
}
