/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { zip } from '@rsaas/util';
import { AggregateRepository } from '@rsaas/commons/lib/events';

import { Reservation, ReservationManager } from '../entities';
import { ReservationProjectionRepository } from '../../reservation-projection';
import { AvailabilityService } from './availability.service';
import {
  ReservationCommandType,
  ReservationCommand,
} from '../reservation.command';
import {
  ResourcesUnavailableError,
  IllegalOperationError,
  InsufficientRightsError,
} from '../reservation.errors';
import { ReservationEvent } from '../reservation.event';
import { AGGREGATE_NAME } from '../constants';
//#endregion

export class StateService {
  private readonly availability: AvailabilityService;

  public constructor(
    private readonly repository: AggregateRepository<
      ReservationCommand,
      ReservationEvent,
      Reservation
    >,
    private readonly searchRepo: ReservationProjectionRepository,
  ) {
    this.availability = new AvailabilityService(this.searchRepo);
  }

  public get aggregateName(): string {
    return AGGREGATE_NAME;
  }

  // - MARK: Commands

  public async confirm(
    reservation: Reservation,
    changedBy: ReservationManager,
  ): Promise<void> {
    if (changedBy.cannotConfirmReservations()) {
      throw new InsufficientRightsError(
        'Only Maintainers or admins can confirm reservations.',
      );
    }

    if (reservation.isReservationStartInPast()) {
      throw new IllegalOperationError(
        'Cannot confirm reservation that starts in past.',
      );
    }

    const availabilities = await Promise.all(
      reservation
        .getResourceIds()
        .map((resourceId) =>
          this.availability.isAvailable(
            reservation.getWorkspaceId(),
            reservation.getDateStart(),
            reservation.getDateEnd(),
            resourceId,
          ),
        ),
    );

    const unavailable = zip(availabilities, reservation.getResourceIds())
      .filter(([availability]) => !availability)
      .map(([, resourceId]) => resourceId);

    if (unavailable.length) {
      throw new ResourcesUnavailableError(
        `Resources with ids ${unavailable.join(', ')} are already reserved.`,
      );
    }

    await this.repository.execute(
      this.makeConfirmCommand(reservation, changedBy),
      reservation,
    );
  }

  public async cancel(
    reservation: Reservation,
    changedBy: ReservationManager,
  ): Promise<void> {
    if (
      reservation.isNotAssignedTo(changedBy) &&
      changedBy.cannotEditUnownedReservations()
    ) {
      throw new InsufficientRightsError(
        `Only Maintainers or admins can modify someone else's reservations.`,
      );
    }

    if (reservation.isReservationStartInPast()) {
      throw new IllegalOperationError(
        'Cannot cancel reservation that starts in past.',
      );
    }

    await this.repository.execute(
      this.makeCancelCommand(reservation, changedBy),
      reservation,
    );
  }

  public async reject(
    reservation: Reservation,
    changedBy: ReservationManager,
    reason: string,
  ): Promise<void> {
    if (changedBy.cannotRejectReservations()) {
      throw new InsufficientRightsError(
        'Only Maintainers or admins can reject reservations.',
      );
    }

    if (reservation.isReservationStartInPast()) {
      throw new IllegalOperationError(
        'Cannot reject reservation that starts in past.',
      );
    }

    await this.repository.execute(
      this.makeRejectCommand(reservation, reason, changedBy),
      reservation,
    );
  }

  public async complete(reservation: Reservation): Promise<void> {
    if (reservation.isReservationEndInFuture()) {
      throw new IllegalOperationError(
        'Cannot complete reservation has not ended yet.',
      );
    }

    await this.repository.execute(
      this.makeCompleteCommand(reservation),
      reservation,
    );
  }

  private makeCancelCommand(
    reservation: Reservation,
    changedBy: ReservationManager,
  ): ReservationCommand {
    return {
      type: ReservationCommandType.cancel,
      rowId: `${this.aggregateName}:${reservation.getWorkspaceId()}`,
      aggregateId: reservation.getId(),
      payload: { changedBy },
    };
  }

  private makeConfirmCommand(
    reservation: Reservation,
    changedBy: ReservationManager,
  ): ReservationCommand {
    return {
      type: ReservationCommandType.confirm,
      rowId: `${this.aggregateName}:${reservation.getWorkspaceId()}`,
      aggregateId: reservation.getId(),
      payload: { changedBy },
    };
  }

  private makeRejectCommand(
    reservation: Reservation,
    reason: string,
    changedBy: ReservationManager,
  ): ReservationCommand {
    return {
      type: ReservationCommandType.reject,
      rowId: `${this.aggregateName}:${reservation.getWorkspaceId()}`,
      aggregateId: reservation.getId(),
      payload: { reason, changedBy },
    };
  }

  private makeCompleteCommand(reservation: Reservation): ReservationCommand {
    return {
      type: ReservationCommandType.complete,
      rowId: `${this.aggregateName}:${reservation.getWorkspaceId()}`,
      aggregateId: reservation.getId(),
      payload: {},
    };
  }
}
