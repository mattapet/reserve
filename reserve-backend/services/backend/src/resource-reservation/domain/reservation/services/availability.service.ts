/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { flatMap } from '@rsaas/util';

import {
  ReservationProjection,
  ReservationProjectionRepository,
} from '../../reservation-projection';
import { ReservationState } from '../reservation-state.type';
import { ResourceId } from '../../resource/resource.entity';
//#endregion

export class AvailabilityService {
  public constructor(
    private readonly searchRepo: ReservationProjectionRepository,
  ) {}

  public async isAvailable(
    workspaceId: string,
    dateStart: Date,
    dateEnd: Date,
    resource: ResourceId,
  ): Promise<boolean> {
    const matchingReservations = await this.getMatchingReservations(
      workspaceId,
      dateStart,
      dateEnd,
    );

    return !this.reservationsIncludeResourceWithId(
      matchingReservations,
      resource,
    );
  }

  private async getMatchingReservations(
    workspaceId: string,
    dateStart: Date,
    dateEnd: Date,
  ): Promise<ReservationProjection[]> {
    const matchingProjections = await this.searchRepo.getAllByDateRange(
      workspaceId,
      dateStart,
      dateEnd,
    );
    return matchingProjections.filter(this.getConfirmedProjectionFilter());
  }

  private getConfirmedProjectionFilter(): (
    projection: ReservationProjection,
  ) => boolean {
    return (projection) => projection.state === ReservationState.confirmed;
  }

  private reservationsIncludeResourceWithId(
    reservations: ReservationProjection[],
    resourceId: ResourceId,
  ): boolean {
    return this.getResourcesFromReservations(reservations).includes(resourceId);
  }

  private getResourcesFromReservations(
    reservations: ReservationProjection[],
  ): ResourceId[] {
    return flatMap(reservations, ({ resourceIds }) => resourceIds);
  }
}
