/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '@rsaas/commons/lib/events/command';
import { ResourceId } from '../resource/resource.entity';
import { Assignee, ReservationManager } from './entities';
//#endregion

export enum ReservationCommandType {
  create = 'reservation.create',
  edit = 'reservation.edit',

  confirm = 'reservation.state_change.confirm',
  cancel = 'reservation.state_change.cancel',
  reject = 'reservation.state_change.reject',
  complete = 'reservation.state_change.complete',
}

export interface ReservationCreateCommand extends Command {
  readonly type: ReservationCommandType.create;
  readonly payload: {
    readonly workspaceId: string;
    readonly dateStart: Date;
    readonly dateEnd: Date;
    readonly resources: ResourceId[];
    readonly assignee: Assignee;
    readonly createdBy: ReservationManager;
    readonly notes?: string;
  };
}

export interface ReservationEditCommand extends Command {
  readonly type: ReservationCommandType.edit;
  readonly payload: {
    readonly dateStart: Date;
    readonly dateEnd: Date;
    readonly resources: ResourceId[];
    readonly assignee: Assignee;
    readonly notes?: string;
    readonly changedBy: ReservationManager;
  };
}

export interface ReservationConfirmCommand extends Command {
  readonly type: ReservationCommandType.confirm;
  readonly payload: {
    readonly changedBy: ReservationManager;
  };
}

export interface ReservationCancelCommand extends Command {
  readonly type: ReservationCommandType.cancel;
  readonly payload: {
    readonly changedBy: ReservationManager;
  };
}

export interface ReservationRejectCommand extends Command {
  readonly type: ReservationCommandType.reject;
  readonly payload: {
    readonly reason: string;
    readonly changedBy: ReservationManager;
  };
}

export interface ReservationCompleteCommand extends Command {
  readonly type: ReservationCommandType.complete;
}

export type ReservationStateCommand =
  | ReservationConfirmCommand
  | ReservationCancelCommand
  | ReservationRejectCommand
  | ReservationCompleteCommand;

export type ReservationCommand =
  | ReservationCreateCommand
  | ReservationEditCommand
  | ReservationStateCommand;
