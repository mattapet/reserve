/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole, UserId, User } from '../../../../user-access';
import { Assignee } from './assignee.entity';
//#endregion

export { UserId } from '../../../../user-access';

export class ReservationManager {
  public constructor(private userId: UserId, private role: UserRole) {}

  public getId(): UserId {
    return this.userId;
  }

  public canReassignReservations(): boolean {
    return this.role !== UserRole.user;
  }

  public cannotReassignReservations(): boolean {
    return !this.canReassignReservations();
  }

  public canEditUnownedReservations(): boolean {
    return this.role !== UserRole.user;
  }

  public cannotEditUnownedReservations(): boolean {
    return !this.canEditUnownedReservations();
  }

  public canConfirmReservations(): boolean {
    return this.role !== UserRole.user;
  }

  public cannotConfirmReservations(): boolean {
    return !this.canConfirmReservations();
  }

  public canRejectReservations(): boolean {
    return this.role !== UserRole.user;
  }

  public cannotRejectReservations(): boolean {
    return !this.canRejectReservations();
  }

  public equals(other: ReservationManager | Assignee): boolean {
    return this.userId === other.getId();
  }

  public static from(user: User): ReservationManager {
    return new ReservationManager(user.getId(), user.getRole());
  }
}
