/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Type } from 'class-transformer';
import { notNull } from '@rsaas/util';

import { Assignee, ReservationManager } from '.';
import { ReservationState } from '../reservation-state.type';
import { InvalidDateRangeError } from '../reservation.errors';
import { ResourceId } from '../../resource/resource.entity';
//#endregion

export class Reservation {
  @Type(() => Date)
  private dateStart?: Date;

  @Type(() => Date)
  private dateEnd?: Date;

  @Type(() => Assignee)
  private assignee?: Assignee;

  @Type(() => Date)
  private createdAt: Date;

  public constructor(
    private id?: string,
    private workspaceId?: string,
    dateStart?: Date,
    dateEnd?: Date,
    assignee?: Assignee,
    private state: ReservationState = ReservationState.requested,
    private resourceIds: ResourceId[] = [],
    createdAt: Date = new Date(Date.now()),
    private notes: string = '',
  ) {
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
    this.assignee = assignee;
    this.createdAt = createdAt;
    this.validate();
  }

  public getId(): string {
    return notNull(this.id, 'Expected initialized Reservation entity');
  }

  public getWorkspaceId(): string {
    return notNull(this.workspaceId, 'Expected initialized Reservation entity');
  }

  public getDateStart(): Date {
    return notNull(this.dateStart, 'Expected initialized Reservation entity');
  }

  public getDateEnd(): Date {
    return notNull(this.dateEnd, 'Expected initialized Reservation entity');
  }

  public getResourceIds(): ResourceId[] {
    return this.resourceIds;
  }

  public getAssignee(): Assignee {
    return notNull(this.assignee, 'Expected initialized Reservation entity');
  }

  public isReservationStartInPast(): boolean {
    return (
      +notNull(this.dateStart, 'Expected initialized Reservation entity') <
      Date.now()
    );
  }

  public isReservationEndInFuture(): boolean {
    return (
      +notNull(this.dateEnd, 'Expected initialized Reservation entity') >=
      Date.now()
    );
  }

  public isAssignedTo(assignee: Assignee | ReservationManager): boolean {
    return notNull(
      this.assignee,
      'Expected initialized Reservation entity',
    ).equals(assignee);
  }

  public isNotAssignedTo(assignee: Assignee | ReservationManager): boolean {
    return !this.isAssignedTo(assignee);
  }

  public getState(): ReservationState {
    return this.state;
  }

  public getCreatedAt(): Date {
    return this.createdAt;
  }

  public getNotes(): string {
    return this.notes;
  }

  public isEditable(): boolean {
    return (
      this.state === ReservationState.requested ||
      this.state === ReservationState.confirmed
    );
  }

  public hasBeenCreated(): boolean {
    return (
      !!this.id && !!this.workspaceId && !!this.dateStart && !!this.dateEnd
    );
  }

  public hasNotBeenCreated(): boolean {
    return !this.hasBeenCreated();
  }

  public setId(id: string): void {
    this.id = id;
  }

  public setWorkspaceId(workspaceId: string): void {
    this.workspaceId = workspaceId;
  }

  public setCreatedAt(createdAt: Date): void {
    this.createdAt = createdAt;
  }

  public setDateStart(dateStart: Date): void {
    this.dateStart = dateStart;
    this.validate();
  }

  public setDateEnd(dateEnd: Date): void {
    this.dateEnd = dateEnd;
    this.validate();
  }

  public assignTo(assignee: Assignee): void {
    this.assignee = assignee;
  }

  public addResource(resourceId: ResourceId): void {
    this.resourceIds = [...this.resourceIds, resourceId];
  }

  public removeResource(resourceId: ResourceId): void {
    this.resourceIds = this.resourceIds.filter((id) => id !== resourceId);
  }

  public setNotes(notes: string): void {
    this.notes = notes;
  }

  public confirm(): void {
    this.state = ReservationState.confirmed;
  }

  public cancel(): void {
    this.state = ReservationState.canceled;
  }

  public reject(): void {
    this.state = ReservationState.rejected;
  }

  public complete(): void {
    this.state = ReservationState.completed;
  }

  private validate() {
    if (this.dateStart && this.dateEnd && this.dateStart > this.dateEnd) {
      throw new InvalidDateRangeError(
        'Reservation cannot end before it starts.',
      );
    }
  }
}
