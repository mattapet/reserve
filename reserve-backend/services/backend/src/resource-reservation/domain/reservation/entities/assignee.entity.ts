/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { User, UserId } from '../../../../user-access';
import { ReservationManager } from './reservation-manager.entity';
//#endregion

export class Assignee {
  public constructor(private readonly userId: UserId) {}

  public getId(): UserId {
    return this.userId;
  }

  public equals(other: Assignee | ReservationManager): boolean {
    return this.userId === other.getId();
  }

  public static from(user: User): Assignee {
    return new Assignee(user.getId());
  }
}
