/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationProjection } from './reservation-projection.interface';
//#endregion

export interface ReservationProjectionRepository {
  getById(
    workspaceId: string,
    reservationId: string,
  ): Promise<ReservationProjection>;

  getAll(workspaceId: string): Promise<ReservationProjection[]>;

  getAllSince(
    workspaceId: string,
    since: Date,
  ): Promise<ReservationProjection[]>;

  getAllUntil(
    workspaceId: string,
    until: Date,
  ): Promise<ReservationProjection[]>;

  getAllByDateRange(
    workspaceId: string,
    dateStart: Date,
    dateEnd: Date,
  ): Promise<ReservationProjection[]>;

  getByUserId(
    workspaceId: string,
    userId: string,
  ): Promise<ReservationProjection[]>;

  getAllToComplete(): Promise<ReservationProjection[]>;

  save(projection: ReservationProjection): Promise<ReservationProjection>;
}
