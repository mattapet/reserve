/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationState } from '../reservation/reservation-state.type';
import { ResourceId } from '../resource/resource.entity';
//#endregion

export interface ReservationProjection {
  id: string;
  workspaceId: string;
  dateStart: Date;
  dateEnd: Date;
  userId: string;
  state: ReservationState;
  resourceIds: ResourceId[];
  createdAt: Date;
  notes?: string;
}
