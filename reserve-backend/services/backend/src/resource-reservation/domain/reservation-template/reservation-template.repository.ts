/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { ReservationTemplate } from './reservation-template.entity';

export interface ReservationTemplateRepository {
  findByWorkspaceId(
    workspaceId: string,
  ): Promise<ReservationTemplate | undefined>;
  save(template: ReservationTemplate): Promise<ReservationTemplate>;
  remove(template: ReservationTemplate): Promise<ReservationTemplate>;
}
