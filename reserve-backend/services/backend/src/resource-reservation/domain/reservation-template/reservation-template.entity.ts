/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { PrimaryColumn, Column, Entity } from 'typeorm';
//#endregion

@Entity({ name: 'reservation_template' })
export class ReservationTemplate {
  @PrimaryColumn({ name: 'workspace_id' })
  public workspaceId: string;

  @Column({ charset: 'utf8' })
  public template: string;

  public constructor(workspaceId: string, template: string) {
    this.workspaceId = workspaceId;
    this.template = template;
  }
}
