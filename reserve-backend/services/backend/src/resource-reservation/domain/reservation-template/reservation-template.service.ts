/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';

import { Workspace } from '../../../workspace-management';

import { ReservationTemplateRepository } from './reservation-template.repository';
import { ReservationTemplate } from './reservation-template.entity';
import { RESERVATION_TEMPLATE_REPOSITORY } from './constants';
//#endregion

@Injectable()
export class ReservationTemplateService {
  public constructor(
    @Inject(RESERVATION_TEMPLATE_REPOSITORY)
    private readonly repository: ReservationTemplateRepository,
  ) {}

  public async findByWorkspace(
    workspace: Workspace,
  ): Promise<ReservationTemplate | undefined> {
    return this.repository.findByWorkspaceId(workspace.getId());
  }

  public async save(template: ReservationTemplate): Promise<void> {
    await this.repository.save(template);
  }

  public async remove(workspace: Workspace): Promise<void> {
    const template = await this.repository.findByWorkspaceId(workspace.getId());
    template && (await this.repository.remove(template));
  }
}
