/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons/lib/commons.module';

import { WebhookModule } from '../webhook';
import { WebhookDispatcherModule } from '../webhook-dispatcher';

import { WebhookEventIngestorService } from './webhook-event-ingestor.service';
//#endregion

@Module({
  imports: [CommonsModule, WebhookModule, WebhookDispatcherModule],
  providers: [WebhookEventIngestorService],
})
export class WebhookEventIngestorModule {}
