/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { Event } from '@rsaas/commons/lib/events/event.entity';
import { EventConsumer, EventPattern } from '@rsaas/commons/lib/event-broker';

import {
  ReservationCanceledEvent,
  ReservationRejectedEvent,
  ReservationConfirmedEvent,
  ReservationCreatedEvent,
  ReservationEventType,
} from '../../resource-reservation';

import { WebhookDispatcherService } from '../webhook-dispatcher';
import { Webhook, WebhookService, EventType } from '../webhook';
//#endregion

@Injectable()
@EventConsumer()
export class WebhookEventIngestorService {
  public constructor(
    private readonly dispatcher: WebhookDispatcherService,
    private readonly webhookService: WebhookService,
  ) {}

  @EventPattern(ReservationEventType.created)
  public async consumeCreated(event: ReservationCreatedEvent): Promise<void> {
    const workspaceId = event.rowId.split(':')[1];
    const webhooks = await this.webhookService.getAllApplicable(
      workspaceId,
      EventType.reservationCreated,
    );
    await this.disaptchWebhooks(webhooks, event);
  }

  @EventPattern(ReservationEventType.confirmed)
  public async consumeConfirmed(
    event: ReservationConfirmedEvent,
  ): Promise<void> {
    const workspaceId = event.rowId.split(':')[1];
    const webhooks = await this.webhookService.getAllApplicable(
      workspaceId,
      EventType.reservationConfirmed,
    );
    await this.disaptchWebhooks(webhooks, event);
  }

  @EventPattern(ReservationEventType.rejected)
  public async consumeRejected(event: ReservationRejectedEvent): Promise<void> {
    const workspaceId = event.rowId.split(':')[1];
    const webhooks = await this.webhookService.getAllApplicable(
      workspaceId,
      EventType.reservationRejected,
    );
    await this.disaptchWebhooks(webhooks, event);
  }

  @EventPattern(ReservationEventType.canceled)
  public async consumeCanceled(event: ReservationCanceledEvent): Promise<void> {
    const workspaceId = event.rowId.split(':')[1];
    const webhooks = await this.webhookService.getAllApplicable(
      workspaceId,
      EventType.reservationCanceled,
    );
    await this.disaptchWebhooks(webhooks, event);
  }

  private async disaptchWebhooks(
    webhooks: Webhook[],
    event: Event,
  ): Promise<void> {
    for (const webhook of webhooks) {
      try {
        await this.dispatcher.dispatch(webhook, event);
      } catch {}
    }
  }
}
