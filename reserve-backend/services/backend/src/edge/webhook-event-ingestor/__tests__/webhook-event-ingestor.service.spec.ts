/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imporst
import { WebhookEventIngestorService } from '../webhook-event-ingestor.service';

import { HttpService } from '@rsaas/commons/lib/http';
import { ConsoleLogger } from '@rsaas/commons/lib/logger/console/logger.service';

import { ReservationEventType } from '../../../resource-reservation';

import { WebhookService } from '../../webhook/webhook.service';
import { Webhook, EncodingType, EventType } from '../../webhook';
import { InMemoryWebhookRepository } from '../../webhook/repositories/in-memory/in-memory-webhook.repository';

import { WebhookDispatcherService } from '../../webhook-dispatcher';
import { WebhookConfig } from '../../webhook-dispatcher/services/webhook.config';
import { SignatureService } from '../../webhook-dispatcher/services/signature.service';
//#endregion

describe('webhook-event-ingestor.service', () => {
  let webhookService!: WebhookService;

  beforeEach(() => {
    webhookService = new WebhookService(new InMemoryWebhookRepository());
  });

  function mock_successfullFetch() {
    return jest.fn().mockImplementation(() =>
      Promise.resolve({
        status: 200,
        headers: [],
        text: () => Promise.resolve(),
      }),
    );
  }

  function mock_errornousFetch() {
    return jest.fn().mockImplementation(() => Promise.reject(new Error()));
  }

  function make_dispatcherWithFetchAndLogger(
    fetch: any,
  ): WebhookDispatcherService {
    const signer = new SignatureService();
    jest.spyOn(signer, 'sign').mockImplementation((a, b) => a + b);
    return new WebhookDispatcherService(
      new WebhookConfig('X-Reserve-Signature', 2),
      new ConsoleLogger(),
      new HttpService(fetch),
      signer,
    );
  }

  function make_serviceWithFetch(fetch: any): WebhookEventIngestorService {
    const dispatcher = make_dispatcherWithFetchAndLogger(fetch);
    return new WebhookEventIngestorService(dispatcher, webhookService);
  }

  async function effect_createWebhook(eventType: EventType) {
    await webhookService.create(new Webhook(), {
      encoding: EncodingType.urlencoded,
      events: [eventType],
      payloadUrl: 'test.com/webhook',
      secret: 'shh',
      workspaceId: 'test',
    });
  }

  it('should ignore event if there are no associated webhook', async () => {
    const fetch = mock_successfullFetch();
    const service = make_serviceWithFetch(fetch);

    await service.consumeCreated({
      type: ReservationEventType.created,
      rowId: 'ReservationAggregate:test',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        workspaceId: 'test',
        dateStart: new Date(200).toISOString(),
        dateEnd: new Date(200).toISOString(),
        createdBy: '88',
      },
    });

    expect(fetch).not.toHaveBeenCalled();
  });

  it('should execute a matching webhook', async () => {
    const fetch = mock_successfullFetch();
    const service = make_serviceWithFetch(fetch);
    await effect_createWebhook(EventType.reservationCreated);

    await service.consumeCreated({
      type: ReservationEventType.created,
      rowId: 'ReservationAggregate:test',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        workspaceId: 'test',
        dateStart: new Date(200).toISOString(),
        dateEnd: new Date(200).toISOString(),
        createdBy: '88',
      },
    });

    expect(fetch).toHaveBeenCalled();
  });

  it('should execute a matching webhooks', async () => {
    const fetch = mock_successfullFetch();
    await effect_createWebhook(EventType.reservationCreated);
    await effect_createWebhook(EventType.reservationCreated);
    const service = make_serviceWithFetch(fetch);

    await service.consumeCreated({
      type: ReservationEventType.created,
      rowId: 'ReservationAggregate:test',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        workspaceId: 'test',
        dateStart: new Date(200).toISOString(),
        dateEnd: new Date(200).toISOString(),
        createdBy: '88',
      },
    });

    expect(fetch).toHaveBeenCalledTimes(2);
  });

  it('should ignore the failure in webhook dispatch', async () => {
    const fetch = mock_errornousFetch();
    await effect_createWebhook(EventType.reservationCanceled);
    const service = make_serviceWithFetch(fetch);

    await service.consumeCanceled({
      type: ReservationEventType.canceled,
      rowId: 'ReservationAggregate:test',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        changedBy: '88',
      },
    });

    expect(fetch).toHaveBeenCalled();
  });

  it('should execute a matching confirmed webhook', async () => {
    const fetch = mock_successfullFetch();
    await effect_createWebhook(EventType.reservationConfirmed);
    const service = make_serviceWithFetch(fetch);

    await service.consumeConfirmed({
      type: ReservationEventType.confirmed,
      rowId: 'ReservationAggregate:test',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        changedBy: '88',
      },
    });

    expect(fetch).toHaveBeenCalled();
  });

  it('should execute a matching rejected webhook', async () => {
    const fetch = mock_successfullFetch();
    await effect_createWebhook(EventType.reservationRejected);
    const service = make_serviceWithFetch(fetch);

    await service.consumeRejected({
      type: ReservationEventType.rejected,
      rowId: 'ReservationAggregate:test',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        reason: 'some reason',
        changedBy: '88',
      },
    });

    expect(fetch).toHaveBeenCalled();
  });

  it('should execute a matching canceled webhook', async () => {
    const fetch = mock_successfullFetch();
    await effect_createWebhook(EventType.reservationCanceled);
    const service = make_serviceWithFetch(fetch);

    await service.consumeCanceled({
      type: ReservationEventType.canceled,
      rowId: 'ReservationAggregate:test',
      aggregateId: '99',
      timestamp: new Date(100),
      payload: {
        changedBy: '88',
      },
    });

    expect(fetch).toHaveBeenCalled();
  });
});
