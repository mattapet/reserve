/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

import { WorkspaceManagementModule } from '../../../workspace-management';

import { JwtStrategy } from './jwt.strategy';
import { JwtValidator } from './jwt-validator';
//#endregion

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    WorkspaceManagementModule,
  ],
  providers: [JwtStrategy, JwtValidator],
  exports: [JwtStrategy],
})
export class JwtModule {}
