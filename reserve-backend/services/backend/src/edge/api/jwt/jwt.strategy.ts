/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import * as fs from 'fs';
import {
  Injectable,
  UnauthorizedException,
  InternalServerErrorException,
  BadRequestException,
} from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-jwt';

import { match } from '@rsaas/util';
import { JwtPayload } from '@rsaas/commons/lib/interface/jwt-payload.interface';
import { AuthorizedUser } from '@rsaas/commons/lib/interface/authorized-user.interface';

import { WorkspaceNotFoundError } from '../../../workspace-management';
import { UserBannedError } from '../../../user-access';

import { JwtValidator } from './jwt-validator';
import { ExtractToken } from './extract-token';
import { InvalidTokenError } from './jwt.errors';
//#endregion

const JWT_SECRET = process.env.JWT_SECRET;
const JWT_PRIVK = process.env.JWT_PRIVK;

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  public constructor(private readonly jwtValidator: JwtValidator) {
    super({
      jwtFromRequest: ExtractToken(),
      secretOrKey: JWT_PRIVK ? fs.readFileSync(JWT_PRIVK) : JWT_SECRET,
    });
  }

  public async validate(payload: JwtPayload): Promise<AuthorizedUser> {
    try {
      return this.jwtValidator.validate(payload);
    } catch (error) {
      throw match(error)
        .whenType(InvalidTokenError)
        .then(() => new BadRequestException(error.message))
        .whenType(WorkspaceNotFoundError)
        .then(() => new UnauthorizedException(error.message))
        .whenType(UserBannedError)
        .then(() => new UnauthorizedException(error.message))
        .getOrDefault(() => new InternalServerErrorException(error));
    }
  }
}
