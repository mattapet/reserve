/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';

import {
  JwtPayload,
  TokenKind,
  ConsoleJwtPayload,
  WorkspaceJwtPayload,
} from '@rsaas/commons/lib/interface/jwt-payload.interface';
import {
  WorkspaceAuthorizedUser,
  ConsoleAuthorizedUser,
  AuthorizedUser,
} from '@rsaas/commons/lib/interface/authorized-user.interface';

import { WorkspaceService, WorkspaceName } from '../../../workspace-management';
import { InvalidTokenError } from './jwt.errors';
//#endregion

@Injectable()
export class JwtValidator {
  public constructor(private readonly workspaceService: WorkspaceService) {}

  public async validate(jwtPayload: JwtPayload): Promise<AuthorizedUser> {
    if (!this.isAccessToken(jwtPayload)) {
      throw new InvalidTokenError('Malformed access token.');
    }

    if (typeof jwtPayload.user === 'string') {
      return this.validateConsoleToken(jwtPayload as ConsoleJwtPayload);
    } else {
      return this.validateWorkspaceToken(jwtPayload as WorkspaceJwtPayload);
    }
  }

  private async validateConsoleToken(
    jwtPayload: ConsoleJwtPayload,
  ): Promise<ConsoleAuthorizedUser> {
    return {
      username: jwtPayload.user,
      scope: jwtPayload.scope,
    };
  }

  private async validateWorkspaceToken(
    jwtPayload: WorkspaceJwtPayload,
  ): Promise<WorkspaceAuthorizedUser> {
    const workspace = await this.workspaceService.getByName(
      new WorkspaceName(jwtPayload.user.workspace),
    );
    const workspaceId = workspace.getId();
    return {
      ...jwtPayload.user,
      workspace: workspaceId,
      scope: jwtPayload.scope,
    };
  }

  private isAccessToken(jwtPayload: JwtPayload): boolean {
    return jwtPayload.kind === TokenKind.accessToken;
  }
}
