/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Get,
  Put,
  Delete,
  UseGuards,
  Req,
  Body,
  UseInterceptors,
  ClassSerializerInterceptor,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOkResponse,
  ApiNoContentResponse,
} from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';
import { JwtGuard } from '@rsaas/commons/lib/guards/jwt.guard';
import { RoleGuard } from '@rsaas/commons/lib/guards/role.guard';

import { WorkspaceService } from '../../../../workspace-management';
import { UserRole } from '../../../../user-access';
import {
  ReservationTemplateService,
  ReservationTemplate,
} from '../../../../resource-reservation';

import { CreateReservationTemplateDTO } from './dto/create-reservation-template.dto';
import { ReservationTemplateInterceptor } from './interceptors/reservation-tempate-dto.interceptor';
import { ReservationTemplateDTO } from './dto/reservation-template.dto';
//#endregion

@Controller('api/v1/settings/reservation')
@UseGuards(JwtGuard)
@ApiTags('settings', 'reservation')
@ApiBearerAuth()
export class ReservationSettingsController {
  public constructor(
    private readonly workspaceService: WorkspaceService,
    private readonly templateService: ReservationTemplateService,
  ) {}

  @Get('template')
  @UseInterceptors(ReservationTemplateInterceptor)
  @ApiOkResponse({ type: ReservationTemplateDTO })
  public async getTemplate(
    @Req()
    req: WorkspaceAuthorizedRequest,
  ): Promise<ReservationTemplate | undefined> {
    const workspace = await this.workspaceService.getById(req.user.workspace);
    return this.templateService.findByWorkspace(workspace);
  }

  @Put('template')
  @Transactional()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UseInterceptors(ClassSerializerInterceptor, ReservationTemplateInterceptor)
  @ApiOkResponse({ type: ReservationTemplateDTO })
  public async createTemplate(
    @Req()
    req: WorkspaceAuthorizedRequest,
    @Body()
    templateDto: CreateReservationTemplateDTO,
  ): Promise<ReservationTemplate> {
    const workspace = await this.workspaceService.getById(req.user.workspace);
    const template = new ReservationTemplate(
      workspace.getId(),
      templateDto.template,
    );

    await this.templateService.save(template);
    return template;
  }

  @Delete('template')
  @Transactional()
  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @ApiNoContentResponse({ description: 'Template was successfully removed.' })
  public async deleteTemplate(
    @Req()
    req: WorkspaceAuthorizedRequest,
  ): Promise<void> {
    const workspace = await this.workspaceService.getById(req.user.workspace);
    await this.templateService.remove(workspace);
  }
}
