/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationSettingsController } from '../reservation-settings.controller';

import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';

import { UserRole } from '../../../../../user-access';
import {
  ReservationTemplate,
  ReservationTemplateService,
} from '../../../../../resource-reservation';
import {
  Workspace,
  WorkspaceService,
  WorkspaceName,
  WorkspaceOwner,
} from '../../../../../workspace-management';
import { InMemoryReservationTemplateRepository } from '../../../../../resource-reservation/application/reservation-template/repositories/in-memory/in-memory-reservation-template.repository';
import { WorkspaceServiceBuilder } from '../../../../../workspace-management/__tests__/workspace/builders/workspace.service.builder';
//#endregion

describe('reservation.settings.controller', () => {
  let templateService!: ReservationTemplateService;
  let workspaceService!: WorkspaceService;
  let controller!: ReservationSettingsController;

  beforeEach(() => {
    templateService = new ReservationTemplateService(
      new InMemoryReservationTemplateRepository(),
    );
    workspaceService = WorkspaceServiceBuilder.inMemory();
    controller = new ReservationSettingsController(
      workspaceService,
      templateService,
    );
  });

  function make_adminUserRequest(): WorkspaceAuthorizedRequest {
    return {
      user: {
        id: '99',
        workspace: 'test',
        role: UserRole.admin,
        isOwner: true,
      },
    } as any;
  }

  async function effect_createTestWorkspace() {
    const owner = new WorkspaceOwner('99');
    await workspaceService.create(
      new Workspace(),
      'test',
      new WorkspaceName('test'),
      owner,
    );
  }

  describe('template', () => {
    it('should return undefined if no template exists', async () => {
      await effect_createTestWorkspace();

      const result = await controller.getTemplate(make_adminUserRequest());

      expect(result).toBeUndefined();
    });

    it('should return template if template exists', async () => {
      await effect_createTestWorkspace();
      await templateService.save(
        new ReservationTemplate('test', 'some template'),
      );

      const result = await controller.getTemplate(make_adminUserRequest());

      expect(result).toEqual(new ReservationTemplate('test', 'some template'));
    });

    it('should create a workspace template', async () => {
      await effect_createTestWorkspace();

      const result = await controller.createTemplate(make_adminUserRequest(), {
        template: 'some template',
      });

      expect(result).toEqual(new ReservationTemplate('test', 'some template'));
    });

    it('should delete workspace template', async () => {
      await effect_createTestWorkspace();

      await controller.deleteTemplate(make_adminUserRequest());

      const result = await controller.getTemplate(make_adminUserRequest());
      expect(result).toBeUndefined();
    });
  });
});
