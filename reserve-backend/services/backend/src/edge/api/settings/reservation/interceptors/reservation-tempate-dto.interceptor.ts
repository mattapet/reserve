/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ReservationTemplate } from '../../../../../resource-reservation';

import { ReservationTemplateDTO } from '../dto/reservation-template.dto';
//#endregion

@Injectable()
export class ReservationTemplateInterceptor
  implements
    NestInterceptor<ReservationTemplate | undefined, ReservationTemplateDTO> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<ReservationTemplate>,
  ): Observable<ReservationTemplateDTO> {
    return call$.handle().pipe(map(this.encode));
  }

  private encode = (
    template: ReservationTemplate | undefined,
  ): ReservationTemplateDTO => ({
    template: template?.template,
  });
}
