/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class ResourceDTO {
  @ApiProperty()
  public readonly id!: string;

  @ApiProperty({ minLength: 1 })
  public readonly name!: string;

  @ApiProperty({ minLength: 1 })
  public readonly description!: string;
}
