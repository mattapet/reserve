/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IsString, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class UpdateResourceDTO {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ minLength: 1 })
  public readonly name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ minLength: 1 })
  public readonly description: string;

  public constructor(name: string, description: string) {
    this.name = name;
    this.description = description;
  }
}
