/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons/lib/commons.module';

import { ResourceReservationModule } from '../../../resource-reservation';
import { UserAccessModule } from '../../../user-access';

import { ResourceController } from './resource.controller';
//#endregion

@Module({
  imports: [CommonsModule, ResourceReservationModule, UserAccessModule],
  controllers: [ResourceController],
})
export class ResourceApiModule {}
