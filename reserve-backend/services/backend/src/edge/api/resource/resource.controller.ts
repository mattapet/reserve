/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  UseGuards,
  Post,
  Req,
  Body,
  Get,
  Param,
  Put,
  Delete,
  Patch,
  UseInterceptors,
  ClassSerializerInterceptor,
  NotFoundException,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
} from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { uuid, match } from '@rsaas/util';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';
import { JwtGuard } from '@rsaas/commons/lib/guards/jwt.guard';
import { RoleGuard } from '@rsaas/commons/lib/guards/role.guard';

import {
  ResourceService,
  Resource,
} from '../../../resource-reservation/domain/resource';
import { UserService, UserRole } from '../../../user-access';

import {
  CreateResource,
  ResourceManager,
  UpdateResource,
} from '../../../resource-reservation';
import { ResourceNotFoundError } from '../../../resource-reservation/domain/resource/resource.errors';

import { CreateResourceDTO } from './dto/create-resource.dto';
import { UpdateResourceDTO } from './dto/update-resource.dto';
import { ResourceDTO } from './dto/resource.dto';
//#endregion

@Controller('api/v1/resource')
@UseGuards(JwtGuard)
@ApiTags('resource')
@ApiBearerAuth()
export class ResourceController {
  public constructor(
    private readonly resourceService: ResourceService,
    private readonly userService: UserService,
  ) {}

  @Get()
  @ApiOkResponse({ type: [ResourceDTO] })
  public async getAll(
    @Req() req: WorkspaceAuthorizedRequest,
  ): Promise<Resource[]> {
    return this.resourceService.getAll(req.user.workspace);
  }

  @Get(':id')
  @ApiOkResponse({ type: ResourceDTO })
  @ApiNotFoundResponse({
    description: 'Resource with the given `id` does not exist.',
  })
  public async getById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') resourceId: string,
  ): Promise<Resource> {
    try {
      return await this.resourceService.getById(req.user.workspace, resourceId);
    } catch (error) {
      throw match(error)
        .whenType(ResourceNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Post()
  @Transactional()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiCreatedResponse({ type: ResourceDTO })
  public async create(
    @Req() req: WorkspaceAuthorizedRequest,
    @Body() { name, description }: CreateResourceDTO,
  ): Promise<Resource> {
    const workspaceId = req.user.workspace;
    const user = await this.userService.getById(workspaceId, req.user.id);
    const resource = new Resource();

    await this.resourceService.create(
      resource,
      new CreateResource(
        uuid(),
        workspaceId,
        name,
        description,
        ResourceManager.from(user),
      ),
    );
    return resource;
  }

  @Put(':id')
  @Transactional()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiOkResponse({ type: ResourceDTO })
  @ApiNotFoundResponse({
    description: 'Resource with the given `id` does not exist.',
  })
  public async updateById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') resourceId: string,
    @Body() { name, description }: UpdateResourceDTO,
  ): Promise<Resource> {
    try {
      const workspaceId = req.user.workspace;
      const user = await this.userService.getById(workspaceId, req.user.id);
      const resource = await this.resourceService.getById(
        workspaceId,
        resourceId,
      );
      const updateParams = new UpdateResource(
        name,
        description,
        ResourceManager.from(user),
      );

      await this.resourceService.update(resource, updateParams);

      return resource;
    } catch (error) {
      throw match(error)
        .whenType(ResourceNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Patch(':id/toggle')
  @Transactional()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiOkResponse({ type: ResourceDTO })
  @ApiNotFoundResponse({
    description: 'Resource with the given `id` does not exist.',
  })
  public async toggleActiveById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') resourceId: string,
  ): Promise<Resource> {
    try {
      const workspaceId = req.user.workspace;
      const user = await this.userService.getById(workspaceId, req.user.id);
      const resource = await this.resourceService.getById(
        workspaceId,
        resourceId,
      );

      await this.resourceService.toggle(resource, ResourceManager.from(user));

      return resource;
    } catch (error) {
      throw match(error)
        .whenType(ResourceNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Delete(':id')
  @Transactional()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiNoContentResponse({ description: 'Resource was successfully deleted.' })
  @ApiNotFoundResponse({
    description: 'Resource with the given `id` does not exist.',
  })
  public async removeById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') resourceId: string,
  ): Promise<void> {
    try {
      const workspaceId = req.user.workspace;
      const user = await this.userService.getById(workspaceId, req.user.id);
      const resource = await this.resourceService.getById(
        workspaceId,
        resourceId,
      );
      await this.resourceService.delete(resource, ResourceManager.from(user));
    } catch (error) {
      throw match(error)
        .whenType(ResourceNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }
}
