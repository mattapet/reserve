/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ResourceController } from '../resource.controller';

import { NotFoundException } from '@nestjs/common';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';

import { ResourceService, Resource } from '../../../../resource-reservation';
import { User, Identity, UserRole, UserService } from '../../../../user-access';

import { ResourceRepositoryBuilder } from '../../../../resource-reservation/__tests__/resource/builders/resource.repository.builder';
import { UserServiceBuilder } from '../../../../user-access/__tests__/user/builders/user.service.builder';

import { CreateResourceDTO } from '../dto/create-resource.dto';
import { UpdateResourceDTO } from '../dto/update-resource.dto';
//#endregion

describe('resource.controller.command', () => {
  let userService: UserService;
  let controller: ResourceController;

  beforeEach(() => {
    userService = UserServiceBuilder.inMemory();
    controller = new ResourceController(
      new ResourceService(ResourceRepositoryBuilder.inMemory()),
      userService,
    );
  });

  async function effect_createOwnerWithId(id: string) {
    await userService.createOwner(
      new User(),
      new Identity(id, 'test', 'test@test.com'),
    );
  }

  function make_ownerAuthorizedRequestWithId(id: string) {
    return ({
      user: { id, workspace: 'test', role: UserRole.admin },
    } as any) as WorkspaceAuthorizedRequest;
  }

  describe('querying resources', () => {
    it('should return an empty array if no resources are present', async () => {
      await effect_createOwnerWithId('99');

      const result = await controller.getAll(
        make_ownerAuthorizedRequestWithId('99'),
      );

      expect(result).toEqual([]);
    });

    it('should return an array with a single resource', async () => {
      await effect_createOwnerWithId('99');
      const resource = await controller.create(
        make_ownerAuthorizedRequestWithId('99'),
        new CreateResourceDTO('test resource', 'test description'),
      );

      const result = await controller.getAll(
        make_ownerAuthorizedRequestWithId('99'),
      );

      expect(result).toEqual([
        new Resource(
          resource.getId(),
          'test',
          'test resource',
          'test description',
        ),
      ]);
    });

    it('should return a resource queried by its id', async () => {
      await effect_createOwnerWithId('99');
      const resource = await controller.create(
        make_ownerAuthorizedRequestWithId('99'),
        new CreateResourceDTO('test resource', 'test description'),
      );

      const result = await controller.getById(
        make_ownerAuthorizedRequestWithId('99'),
        resource.getId(),
      );

      expect(result).toEqual(
        new Resource(
          resource.getId(),
          'test',
          'test resource',
          'test description',
        ),
      );
    });

    it('should throw NotFoundException if resource does not exists', async () => {
      await effect_createOwnerWithId('99');

      const fn = controller.getById(
        make_ownerAuthorizedRequestWithId('99'),
        'some-weird-id',
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('creating a new resource', () => {
    it('should create a new resource', async () => {
      await effect_createOwnerWithId('99');

      const result = await controller.create(
        make_ownerAuthorizedRequestWithId('99'),
        new CreateResourceDTO('test resource', 'test description'),
      );

      expect(result).toEqual(
        new Resource(
          expect.any(String),
          'test',
          'test resource',
          'test description',
        ),
      );
    });
  });

  describe('updating a new resource', () => {
    it('should update an existing resource', async () => {
      await effect_createOwnerWithId('99');
      const resource = await controller.create(
        make_ownerAuthorizedRequestWithId('99'),
        new CreateResourceDTO('test resource', 'test description'),
      );

      const result = await controller.updateById(
        make_ownerAuthorizedRequestWithId('99'),
        resource.getId(),
        new UpdateResourceDTO('another name', 'test description'),
      );

      expect(result).toEqual(
        new Resource(
          resource.getId(),
          'test',
          'another name',
          'test description',
        ),
      );
    });

    it('should throw NotFoundException if resource does not exists', async () => {
      await effect_createOwnerWithId('99');

      const fn = controller.updateById(
        make_ownerAuthorizedRequestWithId('99'),
        'some-weird-id',
        new UpdateResourceDTO('another name', 'test description'),
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('toggling resources', () => {
    it('should make a resource inactive', async () => {
      await effect_createOwnerWithId('99');
      const resource = await controller.create(
        make_ownerAuthorizedRequestWithId('99'),
        new CreateResourceDTO('test resource', 'test description'),
      );

      const result = await controller.toggleActiveById(
        make_ownerAuthorizedRequestWithId('99'),
        resource.getId(),
      );

      expect(result.isActive()).toBe(false);
    });

    it('should throw NotFoundException if the resource does not exists', async () => {
      await effect_createOwnerWithId('99');

      const fn = controller.toggleActiveById(
        make_ownerAuthorizedRequestWithId('99'),
        'some-weird-id',
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('deleting resources', () => {
    it('should return no resources after the resource is deleted', async () => {
      await effect_createOwnerWithId('99');
      const resource = await controller.create(
        make_ownerAuthorizedRequestWithId('99'),
        new CreateResourceDTO('test resource', 'test description'),
      );

      await controller.removeById(
        make_ownerAuthorizedRequestWithId('99'),
        resource.getId(),
      );

      const resources = await controller.getAll(
        make_ownerAuthorizedRequestWithId('99'),
      );
      expect(resources).toEqual([]);
    });

    it('should throw NotFoundException if the resource does not exists', async () => {
      await effect_createOwnerWithId('99');

      const fn = controller.removeById(
        make_ownerAuthorizedRequestWithId('99'),
        'some-weird-id',
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });
});
