/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  BadRequestException,
  NotFoundException,
  Controller,
  Get,
  Req,
  UseGuards,
  Post,
  Body,
  UseInterceptors,
  ClassSerializerInterceptor,
  Param,
  Delete,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOkResponse,
  ApiExtraModels,
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger';
import { Transactional } from '@rsaas/commons/lib/typeorm';

import { match } from '@rsaas/util';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';
import { JwtGuard } from '@rsaas/commons/lib/guards/jwt.guard';
import { RoleGuard } from '@rsaas/commons/lib/guards/role.guard';

import {
  WorkspaceAnnouncementService,
  WorkspaceAnnouncement,
  WorkspaceAnnouncementNameCollisionError,
  WorkspaceAnnouncementNotFoundError,
} from '../../../workspace-management';

import { CreateWorkspaceAnnouncementDTO } from './dto/create-workspace-announcement.dto';
import { UserRole } from '../../../user-access';
import { WorkspaceAnnouncementDTO } from './dto/workspace-announcement.dto';
//#endregion

@Controller('api/v1/workspace/announcement')
@UseGuards(JwtGuard)
@ApiTags('workspace')
@ApiBearerAuth()
@ApiExtraModels(WorkspaceAnnouncementDTO)
export class WorkspaceAnnouncementController {
  public constructor(private readonly service: WorkspaceAnnouncementService) {}

  // Queries

  @Get()
  @ApiOkResponse({ type: [WorkspaceAnnouncementDTO] })
  public async getAll(
    @Req()
    req: WorkspaceAuthorizedRequest,
  ): Promise<WorkspaceAnnouncement[]> {
    return this.service.getAll(req.user.workspace);
  }

  @Get('active')
  @ApiOkResponse({ type: WorkspaceAnnouncementDTO })
  public async getActive(
    @Req()
    req: WorkspaceAuthorizedRequest,
  ): Promise<WorkspaceAnnouncement | undefined> {
    return this.service.findActive(req.user.workspace);
  }

  // Commands

  @Post()
  @Transactional()
  @UseGuards(RoleGuard(UserRole.admin))
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiCreatedResponse({ type: WorkspaceAnnouncementDTO })
  @ApiBadRequestResponse({
    description: 'An announcement with the given `name` already exists.',
  })
  public async create(
    @Req()
    req: WorkspaceAuthorizedRequest,
    @Body()
    params: CreateWorkspaceAnnouncementDTO,
  ): Promise<WorkspaceAnnouncement> {
    const announcement = new WorkspaceAnnouncement(
      req.user.workspace,
      params.name,
      params.text,
    );

    try {
      await this.service.create(announcement);

      return announcement;
    } catch (error) {
      throw match(error)
        .whenType(WorkspaceAnnouncementNameCollisionError)
        .then(() => new BadRequestException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Post(':name/toggle')
  @Transactional()
  @UseGuards(RoleGuard(UserRole.admin))
  @ApiCreatedResponse({ type: WorkspaceAnnouncementDTO })
  @ApiNotFoundResponse({
    description: 'An announcement with the given `name` does not exist.',
  })
  public async toggle(
    @Req()
    req: WorkspaceAuthorizedRequest,
    @Param('name')
    name: string,
  ): Promise<WorkspaceAnnouncement> {
    try {
      const announcement = await this.service.getByName(
        req.user.workspace,
        name,
      );

      await this.service.toggle(announcement);
      return announcement;
    } catch (error) {
      throw match(error)
        .whenType(WorkspaceAnnouncementNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Delete(':name')
  @Transactional()
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiCreatedResponse({ type: WorkspaceAnnouncementDTO })
  @ApiNotFoundResponse({
    description: 'An announcement with the given `name` does not exist.',
  })
  public async remove(
    @Req()
    req: WorkspaceAuthorizedRequest,
    @Param('name')
    name: string,
  ): Promise<void> {
    try {
      const announcement = await this.service.getByName(
        req.user.workspace,
        name,
      );
      await this.service.remove(announcement);
    } catch (error) {
      throw match(error)
        .whenType(WorkspaceAnnouncementNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }
}
