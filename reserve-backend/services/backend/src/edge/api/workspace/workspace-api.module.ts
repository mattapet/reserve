/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { WorkspaceManagementModule } from '../../../workspace-management';
import { UserAccessModule } from '../../../user-access';

import { WorkspaceController } from './workspace.controller';
import { WorkspaceAnnouncementController } from './workspace-announcement.controller';
//#endregion

@Module({
  imports: [WorkspaceManagementModule, UserAccessModule],
  controllers: [WorkspaceController, WorkspaceAnnouncementController],
})
export class WorkspaceApiModule {}
