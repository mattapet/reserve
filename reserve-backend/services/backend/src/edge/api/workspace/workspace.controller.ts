/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Get,
  UseGuards,
  Req,
  Delete,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
  NotFoundException,
  Post,
  Body,
  UnauthorizedException,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { match } from '@rsaas/util';
import { UserRole } from '@rsaas/util/lib/user-role.type';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';
import { RoleGuard } from '@rsaas/commons/lib/guards/role.guard';
import { JwtGuard } from '@rsaas/commons/lib/guards/jwt.guard';
import {
  ApiTags,
  ApiOkResponse,
  getSchemaPath,
  ApiExtraModels,
  ApiNotFoundResponse,
  ApiBearerAuth,
  ApiUnauthorizedResponse,
  ApiNoContentResponse,
} from '@nestjs/swagger';
import { Transactional } from '@rsaas/commons/lib/typeorm';

import { UserService } from '../../../user-access';

import {
  WorkspaceNotFoundError,
  IllegalOperationError,
  IllegalWorkspaceNameError,
} from '../../../workspace-management/domain/workspace/workspace.errors';
import {
  WorkspaceService,
  Workspace,
  WorkspaceName,
  WorkspaceOwner,
  WorkspaceUser,
} from '../../../workspace-management';

import { TransformerInterceptor } from './transformer.interceptor';
import { TransferOwnershipDTO } from './dto/transfer-ownership-dto';
import { WorkspaceDTO } from './dto/workspace.dto';
import { UserNotFoundError } from '../../../user-access/domain/user/user.errors';

//#endregion

@ApiTags('workspace')
@Controller('api/v1/workspace')
export class WorkspaceController {
  public constructor(
    private readonly service: WorkspaceService,
    private readonly userService: UserService,
  ) {}

  @Get()
  @Transactional()
  @UseInterceptors(ClassSerializerInterceptor, TransformerInterceptor)
  @ApiExtraModels(WorkspaceDTO)
  @ApiOkResponse({
    schema: {
      oneOf: [
        { $ref: getSchemaPath(WorkspaceDTO) },
        { type: 'array', items: { $ref: getSchemaPath(WorkspaceDTO) } },
      ],
    },
  })
  @ApiNotFoundResponse({
    description: 'A workspace with the queried `name` does not exist.',
  })
  public async get(
    @Query('name')
    name?: string,
  ): Promise<Workspace | Workspace[]> {
    if (name) {
      return this.getByName(name);
    } else {
      return this.getAll();
    }
  }

  private async getAll(): Promise<Workspace[]> {
    return this.service.getAll();
  }

  private async getByName(name: string): Promise<Workspace> {
    try {
      return await this.service.getByName(new WorkspaceName(name));
    } catch (error) {
      throw match(error)
        .whenType(IllegalWorkspaceNameError)
        .then(() => new NotFoundException(error.message))
        .whenType(WorkspaceNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Post('transfer')
  @Transactional()
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtGuard)
  @UseInterceptors(ClassSerializerInterceptor, TransformerInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: WorkspaceDTO,
    description: 'The workspace ownership was successfully transferred.',
  })
  @ApiUnauthorizedResponse({
    description:
      'User trying to transfer the ownership of the workspace is not its owner. ',
  })
  @ApiNotFoundResponse({
    description:
      'User trying to transfer the workspace ownership to does not exist.',
  })
  public async transferOwnership(
    @Req() req: WorkspaceAuthorizedRequest,
    @Body() dto: TransferOwnershipDTO,
  ): Promise<Workspace> {
    try {
      const [workspace, from, to] = await Promise.all([
        this.service.getById(req.user.workspace),
        this.userService.getById(req.user.workspace, req.user.id),
        this.userService.getById(req.user.workspace, dto.to),
      ]);

      await this.service.transferOwnership(
        workspace,
        WorkspaceUser.from(to),
        WorkspaceOwner.from(from),
      );

      return workspace;
    } catch (error) {
      throw match(error)
        .whenType(UserNotFoundError)
        .then(() => new NotFoundException(error.message))
        .whenType(IllegalOperationError)
        .then(() => new UnauthorizedException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Delete()
  @Transactional()
  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtGuard, RoleGuard(UserRole.admin))
  @ApiBearerAuth()
  @ApiNoContentResponse({
    description: 'The workspace was successfully deleted.',
  })
  @ApiUnauthorizedResponse({
    description: 'User trying to delete the workspace is not its owner.',
  })
  public async deleteWorkspace(
    @Req() req: WorkspaceAuthorizedRequest,
  ): Promise<void> {
    try {
      const { workspace: workspaceId, id } = req.user;
      const user = await this.userService.getById(workspaceId, id);
      const workspace = await this.service.getById(workspaceId);

      await this.service.delete(workspace, WorkspaceOwner.from(user));
    } catch (error) {
      throw match(error)
        .whenType(IllegalOperationError)
        .then(() => new UnauthorizedException(error.message))
        .getOrDefault(() => error);
    }
  }
}
