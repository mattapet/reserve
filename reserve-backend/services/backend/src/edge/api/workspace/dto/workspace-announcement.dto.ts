/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class WorkspaceAnnouncementDTO {
  @ApiProperty()
  public readonly workspaceId: string;

  @ApiProperty()
  public readonly name: string;

  @ApiProperty()
  public readonly text: string;

  @ApiProperty()
  public readonly active: boolean;

  public constructor(
    workspaceId: string,
    name: string,
    text: string,
    active: boolean,
  ) {
    this.workspaceId = workspaceId;
    this.name = name;
    this.text = text;
    this.active = active;
  }
}
