/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class TransferOwnershipDTO {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public to: string;

  public constructor(to: string) {
    this.to = to;
  }
}
