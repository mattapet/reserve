/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class CreateWorkspaceAnnouncementDTO {
  @ApiProperty()
  public readonly name: string;

  @ApiProperty()
  public readonly text: string;

  public constructor(name: string, text: string) {
    this.name = name;
    this.text = text;
  }
}
