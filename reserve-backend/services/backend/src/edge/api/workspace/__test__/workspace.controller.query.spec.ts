/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceController } from '../workspace.controller';
import { NotFoundException } from '@nestjs/common';

import { UserService, User, Identity } from '../../../../user-access';
import { UserServiceBuilder } from '../../../../user-access/__tests__/user/builders/user.service.builder';

import {
  WorkspaceService,
  Workspace,
  WorkspaceName,
  WorkspaceOwner,
} from '../../../../workspace-management';
import { WorkspaceServiceBuilder } from '../../../../workspace-management/__tests__/workspace/builders/workspace.service.builder';
//#endregion

describe('workspace controller', () => {
  let controller!: WorkspaceController;
  let workspaceService!: WorkspaceService;
  let userService!: UserService;

  beforeEach(() => {
    userService = UserServiceBuilder.inMemory();
    workspaceService = WorkspaceServiceBuilder.inMemory();
    controller = new WorkspaceController(workspaceService, userService);
  });

  async function effect_createOwnerWithId(id: string) {
    const owner = new User();
    await userService.createOwner(
      owner,
      new Identity(id, 'test', 'test@test.com'),
    );
    return owner;
  }

  async function effect_createWorkspaceWithOwnerAndName(
    owner: User,
    name: WorkspaceName,
  ) {
    await workspaceService.create(
      new Workspace(),
      'test',
      name,
      WorkspaceOwner.from(owner),
    );
  }

  describe('retrieving all workspaces', () => {
    it('should return an empty array, if no workspaces are present', async () => {
      const result = await controller.get();

      expect(result).toEqual([]);
    });

    it('should return an array with a single workspace', async () => {
      const owner = await effect_createOwnerWithId('99');
      const name = new WorkspaceName('test-workspace');
      await effect_createWorkspaceWithOwnerAndName(owner, name);

      const result = await controller.get();

      expect(result).toEqual([
        new Workspace('test', name, WorkspaceOwner.from(owner)),
      ]);
    });
  });

  describe('retrieving workspaces by names', () => {
    it('should return workspace with matching name', async () => {
      const owner = await effect_createOwnerWithId('99');
      const name = new WorkspaceName('test-workspace');
      await effect_createWorkspaceWithOwnerAndName(owner, name);

      const result = await controller.get('test-workspace');

      expect(result).toEqual(
        new Workspace('test', name, WorkspaceOwner.from(owner)),
      );
    });

    it('should throw NotFoundException if invalid workspace name passed', async () => {
      const owner = await effect_createOwnerWithId('99');
      const name = new WorkspaceName('test-workspace');
      await effect_createWorkspaceWithOwnerAndName(owner, name);

      const fn = controller.get('t');

      await expect(fn).rejects.toThrow(NotFoundException);
    });

    it('should throw NotFoundException if workspace does not exist', async () => {
      const owner = await effect_createOwnerWithId('99');
      const name = new WorkspaceName('test-workspace');
      await effect_createWorkspaceWithOwnerAndName(owner, name);

      const fn = controller.get('some-other-workspace-name');

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });
});
