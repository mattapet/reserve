/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceController } from '../workspace.controller';
import { UnauthorizedException, NotFoundException } from '@nestjs/common';

import { UserService, User, Identity } from '../../../../user-access';
import { UserServiceBuilder } from '../../../../user-access/__tests__/user/builders/user.service.builder';

import {
  WorkspaceService,
  Workspace,
  WorkspaceName,
  WorkspaceOwner,
} from '../../../../workspace-management';
import { WorkspaceServiceBuilder } from '../../../../workspace-management/__tests__/workspace/builders/workspace.service.builder';
//#endregion

describe('workspace controller', () => {
  let controller!: WorkspaceController;
  let workspaceService!: WorkspaceService;
  let userService!: UserService;

  beforeEach(() => {
    userService = UserServiceBuilder.inMemory();
    workspaceService = WorkspaceServiceBuilder.inMemory();
    controller = new WorkspaceController(workspaceService, userService);
  });

  async function effect_createOwnerWithId(id: string) {
    const owner = new User();
    await userService.createOwner(
      owner,
      new Identity(id, 'test', 'test@test.com'),
    );
    return owner;
  }

  async function effect_createUserWithId(id: string) {
    const owner = new User();
    await userService.create(owner, new Identity(id, 'test', 'user@test.com'));
    return owner;
  }

  async function effect_createWorkspaceWithOwnerAndName(
    owner: User,
    name: WorkspaceName,
  ) {
    await workspaceService.create(
      new Workspace(),
      'test',
      name,
      WorkspaceOwner.from(owner),
    );
  }

  function make_authorizedRequestWithUserId(id: string) {
    return { user: { id, workspace: 'test' } } as any;
  }

  describe('deleting a workspace', () => {
    it('should delete a workspace if owner deletes it', async () => {
      const owner = await effect_createOwnerWithId('99');
      const name = new WorkspaceName('test-workspace');
      await effect_createWorkspaceWithOwnerAndName(owner, name);

      await controller.deleteWorkspace(make_authorizedRequestWithUserId('99'));

      const deletedWorkspace = await workspaceService.getById('test');
      expect(deletedWorkspace.isDeleted()).toBe(true);
    });

    it('should throw UnauthorizedException if non-owner tries to delete a workspace', async () => {
      const owner = await effect_createOwnerWithId('99');
      const name = new WorkspaceName('test-workspace');
      await effect_createWorkspaceWithOwnerAndName(owner, name);
      await effect_createUserWithId('88');

      const fn = controller.deleteWorkspace(
        make_authorizedRequestWithUserId('88'),
      );

      await expect(fn).rejects.toThrow(UnauthorizedException);
    });
  });

  describe('transferring ownership', () => {
    it('should transfer workspace ownership', async () => {
      const owner = await effect_createOwnerWithId('99');
      const name = new WorkspaceName('test-workspace');
      await effect_createWorkspaceWithOwnerAndName(owner, name);
      await effect_createUserWithId('88');

      const result = await controller.transferOwnership(
        make_authorizedRequestWithUserId('99'),
        { to: '88' },
      );

      expect(result.getOwner()).toEqual(new WorkspaceOwner('88'));
    });

    it('should throw UnauthorizedException if non-owner tries to transfer workspace', async () => {
      const owner = await effect_createOwnerWithId('99');
      const name = new WorkspaceName('test-workspace');
      await effect_createWorkspaceWithOwnerAndName(owner, name);
      await effect_createUserWithId('88');

      const fn = controller.transferOwnership(
        make_authorizedRequestWithUserId('88'),
        { to: '88' },
      );

      await expect(fn).rejects.toThrow(UnauthorizedException);
    });

    it('should throw NotFoundException if transferring workspace to non-existing user', async () => {
      const owner = await effect_createOwnerWithId('99');
      const name = new WorkspaceName('test-workspace');
      await effect_createWorkspaceWithOwnerAndName(owner, name);

      const fn = controller.transferOwnership(
        make_authorizedRequestWithUserId('99'),
        { to: '88' },
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });
});
