/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceAnnouncementController } from '../workspace-announcement.controller';

import { BadRequestException, NotFoundException } from '@nestjs/common';

import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';

import {
  WorkspaceAnnouncement,
  WorkspaceAnnouncementService,
} from '../../../../workspace-management';
import { InMemoryWorkspaceAccouncementRepository } from '../../../../workspace-management/application/workspace-announcement/repositories/in-memory/in-memory-workspace-announcement.repository';
//#endregion

describe('workspace-announcement.controller', () => {
  let service!: WorkspaceAnnouncementService;
  let controller!: WorkspaceAnnouncementController;
  const req: WorkspaceAuthorizedRequest = {
    user: { workspace: 'test' },
  } as any;

  beforeEach(() => {
    service = new WorkspaceAnnouncementService(
      new InMemoryWorkspaceAccouncementRepository(),
    );
    controller = new WorkspaceAnnouncementController(service);
  });

  describe('announcement retrieval', () => {
    it('should return an empty array if no announcements are present', async () => {
      const result = await controller.getAll(req);

      expect(result).toEqual([]);
    });
  });

  describe('creating announcements', () => {
    it('should create a new workspace', async () => {
      await controller.create(req, {
        name: 'test-name',
        text: 'content',
      });

      const result = await controller.getAll(req);
      expect(result).toEqual([
        new WorkspaceAnnouncement('test', 'test-name', 'content'),
      ]);
    });

    it('should throw a BadRequestException if announcement with given name already exists', async () => {
      await controller.create(req, {
        name: 'test-name',
        text: 'content',
      });

      const fn = controller.create(req, {
        name: 'test-name',
        text: 'content',
      });

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should return undefined if there is no active announcement', async () => {
      const result = await controller.getActive(req);

      expect(result).toBeUndefined();
    });

    it('should return an active announcement', async () => {
      await controller.create(req, {
        name: 'test-name',
        text: 'content',
      });
      await controller.toggle(req, 'test-name');

      const result = await controller.getActive(req);

      expect(result!.getName()).toBe('test-name');
      expect(result!.isActive()).toBe(true);
    });
  });

  describe('toggling announcements', () => {
    it('should activate existing announcement', async () => {
      await controller.create(req, {
        name: 'test-name',
        text: 'content',
      });

      await controller.toggle(req, 'test-name');

      const [result] = await controller.getAll(req);
      expect(result.isActive()).toBe(true);
    });

    it('should deactivate existing announcement', async () => {
      await controller.create(req, {
        name: 'test-name',
        text: 'content',
      });

      await controller.toggle(req, 'test-name');
      await controller.toggle(req, 'test-name');

      const [result] = await controller.getAll(req);
      expect(result.isActive()).toBe(false);
    });

    it('should throw NotFoundException if toggling non-existing announcement', async () => {
      const fn = controller.toggle(req, 'test-name');

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('removing announcements', () => {
    it('should remove existing announcement', async () => {
      await controller.create(req, {
        name: 'test-name',
        text: 'content',
      });

      await controller.remove(req, 'test-name');

      const results = await controller.getAll(req);
      expect(results).toEqual([]);
    });

    it('should throw NotFoundException if removing non-existing announcement', async () => {
      const fn = controller.remove(req, 'test-name');

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });
});
