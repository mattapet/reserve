/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Workspace } from '../../../workspace-management';
import { WorkspaceDTO } from './dto/workspace.dto';
//#endregion

@Injectable()
export class TransformerInterceptor
  implements NestInterceptor<Workspace | Workspace[]> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<Workspace | Workspace[]>,
  ): Observable<WorkspaceDTO | WorkspaceDTO[]> {
    return call$
      .handle()
      .pipe(
        map((payload: Workspace | Workspace[]) =>
          Array.isArray(payload)
            ? payload.map(this.encode)
            : this.encode(payload),
        ),
      );
  }

  private encode = (workspace: Workspace) => ({
    id: workspace.getId(),
    name: workspace.getName().get(),
    owner_id: workspace.getOwner().getId(),
  });
}
