/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Webhook } from '../../../webhook';

import { WebhookDTO } from '../dto/webhook.dto';
//#endregion

@Injectable()
export class TransformerInterceptor
  implements NestInterceptor<Webhook | Webhook[], WebhookDTO | WebhookDTO[]> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<Webhook | Webhook[]>,
  ): Observable<WebhookDTO | WebhookDTO[]> {
    return call$
      .handle()
      .pipe(
        map((payload: Webhook | Webhook[]) =>
          Array.isArray(payload)
            ? payload.map(this.encode)
            : this.encode(payload),
        ),
      );
  }

  private encode = (webhook: Webhook): WebhookDTO => ({
    id: webhook.getId(),
    active: webhook.isActive(),
    encoding: webhook.getEncoding(),
    events: webhook.getEvents(),
    payload_url: webhook.getPayloadUrl(),
    secret: webhook.getSecret(),
  });
}
