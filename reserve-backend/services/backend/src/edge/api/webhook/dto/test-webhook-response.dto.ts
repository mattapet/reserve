/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { ApiProperty } from '@nestjs/swagger';

export class SuccessfullWebhookResponseDTO {
  @ApiProperty()
  public readonly status: number;

  @ApiProperty()
  public readonly headers?: { [key: string]: string };

  @ApiProperty()
  public readonly body?: string | Buffer;

  public constructor(
    status: number,
    headers?: { [key: string]: string },
    body?: string | Buffer,
  ) {
    this.status = status;
    this.headers = headers;
    this.body = body;
  }
}

export class ErrorWebhookResponseDTO {
  @ApiProperty()
  public readonly message: string;

  public constructor(message: string) {
    this.message = message;
  }
}

export type WebhookResponseDTO =
  | SuccessfullWebhookResponseDTO
  | ErrorWebhookResponseDTO;
