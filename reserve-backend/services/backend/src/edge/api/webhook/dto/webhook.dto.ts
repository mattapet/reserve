/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EventType, EncodingType } from '../../../webhook';
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class WebhookDTO {
  @ApiProperty()
  public readonly id!: number;

  @ApiProperty()
  public readonly payload_url!: string;

  @ApiProperty()
  public readonly secret!: string;

  @ApiProperty({ enum: EncodingType })
  public readonly encoding!: EncodingType;

  @ApiProperty({ enum: EventType, isArray: true, minItems: 1 })
  public readonly events!: EventType[];

  @ApiProperty()
  public readonly active!: boolean;
}
