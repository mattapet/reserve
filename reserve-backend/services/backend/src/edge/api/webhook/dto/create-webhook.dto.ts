/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  IsString,
  IsNotEmpty,
  IsUrl,
  IsArray,
  IsEnum,
  IsOptional,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { EncodingType, EventType } from '../../../webhook';
//#endregion

export class CreateWebhookDTO {
  @IsUrl({ require_tld: false })
  @IsNotEmpty()
  @ApiProperty()
  public readonly payload_url!: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public readonly secret!: string;

  @IsArray()
  @IsNotEmpty()
  @IsEnum(EventType, { each: true })
  @ApiProperty({ enum: EventType, isArray: true, minItems: 1 })
  public readonly events!: EventType[];

  @IsOptional()
  @IsEnum(EncodingType)
  @ApiProperty({
    enum: EncodingType,
    required: false,
    default: EncodingType.urlencoded,
  })
  public readonly encoding?: EncodingType;
}
