/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WebhookController } from '../webhook.controller';
import { NotFoundException } from '@nestjs/common';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';

import { UserRole } from '../../../../user-access';
import { WebhookService, EventType, EncodingType } from '../../../webhook';

import { InMemoryWebhookRepository } from '../../../webhook/repositories/in-memory/in-memory-webhook.repository';
import { WebhookBuilder } from '../../../webhook/builders/webhook.builder';
//#endregion

describe('webhook.controller', () => {
  let controller!: WebhookController;

  beforeEach(() => {
    controller = new WebhookController(
      new WebhookService(new InMemoryWebhookRepository()),
      jest.fn() as any,
    );
  });

  function make_authorizedRequest(
    role: UserRole = UserRole.admin,
  ): WorkspaceAuthorizedRequest {
    return {
      user: {
        id: '99',
        workspace: 'test',
        isOwner: true,
        role,
      },
    } as any;
  }

  describe('retrieving webhooks', () => {
    it('should respond with an empty array if there are no webhooks', async () => {
      const result = await controller.getAll(make_authorizedRequest());

      expect(result).toEqual([]);
    });

    it('should respond a single webhook', async () => {
      const webhook = await controller.create(make_authorizedRequest(), {
        payload_url: 'test.com',
        events: [EventType.reservationConfirmed],
        secret: '12345',
      });

      const result = await controller.getAll(
        make_authorizedRequest(UserRole.admin),
      );

      expect(result).toEqual([webhook]);
    });

    it('should retrieve webhook by its id', async () => {
      const webhook = await controller.create(make_authorizedRequest(), {
        payload_url: 'test.com',
        events: [EventType.reservationConfirmed],
        secret: '12345',
      });

      const result = await controller.getById(
        make_authorizedRequest(UserRole.admin),
        1,
      );

      expect(result).toEqual(webhook);
    });

    it('should throw NotFoundException if webhook is does not exists', async () => {
      const fn = controller.getById(make_authorizedRequest(UserRole.admin), 1);

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('creating webhooks', () => {
    it('should create a new webhook', async () => {
      const result = await controller.create(make_authorizedRequest(), {
        payload_url: 'test.com',
        events: [EventType.reservationConfirmed],
        secret: '12345',
      });

      expect(result).toEqual(
        new WebhookBuilder()
          .withId(1)
          .withWorkspaceId('test')
          .withSecret('12345')
          .withPayloadUrl('test.com')
          .withEvent(EventType.reservationConfirmed)
          .withEncoding(EncodingType.urlencoded)
          .activated()
          .build(),
      );
    });
  });

  describe('updating webhooks', () => {
    it('should update already created webhook', async () => {
      await controller.create(make_authorizedRequest(), {
        payload_url: 'test.com',
        events: [EventType.reservationConfirmed],
        secret: '12345',
      });

      const result = await controller.updateById(make_authorizedRequest(), 1, {
        payload_url: 'another-test.com',
        events: [EventType.reservationCanceled],
        encoding: EncodingType.urlencoded,
        secret: 'shh',
      });

      expect(result).toEqual(
        new WebhookBuilder()
          .withId(1)
          .withWorkspaceId('test')
          .withSecret('shh')
          .withPayloadUrl('another-test.com')
          .withEvent(EventType.reservationCanceled)
          .withEncoding(EncodingType.urlencoded)
          .activated()
          .build(),
      );
    });

    it('should throw NotFoundException if webhook to be updated does not exist', async () => {
      const fn = controller.updateById(make_authorizedRequest(), 1, {
        payload_url: 'another-test.com',
        events: [EventType.reservationCanceled],
        encoding: EncodingType.urlencoded,
        secret: 'shh',
      });

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('toggling webhook activity', () => {
    it('should deactivate active webhook', async () => {
      await controller.create(make_authorizedRequest(), {
        payload_url: 'test.com',
        events: [EventType.reservationConfirmed],
        secret: '12345',
      });

      const result = await controller.toggleActiveById(
        make_authorizedRequest(),
        1,
      );

      expect(result.isInactive()).toBe(true);
    });

    it('should activate active webhook', async () => {
      await controller.create(make_authorizedRequest(), {
        payload_url: 'test.com',
        events: [EventType.reservationConfirmed],
        secret: '12345',
      });
      await controller.toggleActiveById(make_authorizedRequest(), 1);

      const result = await controller.toggleActiveById(
        make_authorizedRequest(),
        1,
      );

      expect(result.isActive()).toBe(true);
    });

    it('should throw NotFoundException if webhook to be toggled does not exist', async () => {
      const fn = controller.toggleActiveById(make_authorizedRequest(), 1);

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('removing webhooks', () => {
    it('should remove existing webhook', async () => {
      await controller.create(make_authorizedRequest(), {
        payload_url: 'test.com',
        events: [EventType.reservationConfirmed],
        secret: '12345',
      });

      await controller.removeById(make_authorizedRequest(), 1);

      const result = await controller.getAll(make_authorizedRequest());
      expect(result).toEqual([]);
    });

    it('should throw NotFoundException if webhook does not exist', async () => {
      const fn = controller.removeById(make_authorizedRequest(), 1);

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });
});
