/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WebhookController } from '../webhook.controller';

import { NotFoundException } from '@nestjs/common';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';
import { ConsoleLogger } from '@rsaas/commons/lib/logger/console/logger.service';
import { HttpService } from '@rsaas/commons/lib/http';

import { UserRole } from '../../../../user-access';
import { WebhookService, EventType } from '../../../webhook';

import { InMemoryWebhookRepository } from '../../../webhook/repositories/in-memory/in-memory-webhook.repository';
import {
  WebhookDispatcherService,
  SuccessfullWebhookResponse,
  ErrorWebhookResponse,
} from '../../../webhook-dispatcher';
import { SignatureService } from '../../../webhook-dispatcher/services/signature.service';
import { WebhookConfig } from '../../../webhook-dispatcher/services/webhook.config';
//#endregion

describe('webhook.controller', () => {
  let fetch!: jest.Mock<any, any>;
  let dispatcher!: WebhookDispatcherService;
  let controller!: WebhookController;

  beforeEach(() => {
    fetch = jest.fn();
    const signer = new SignatureService();
    jest.spyOn(signer, 'sign').mockImplementation((a, b) => a + b);
    dispatcher = new WebhookDispatcherService(
      new WebhookConfig('X-Reserve-Signature', 2),
      new ConsoleLogger(),
      new HttpService(fetch),
      signer,
    );
    controller = new WebhookController(
      new WebhookService(new InMemoryWebhookRepository()),
      dispatcher,
    );
  });

  function make_authorizedRequest(
    role: UserRole = UserRole.admin,
  ): WorkspaceAuthorizedRequest {
    return {
      user: {
        id: '99',
        workspace: 'test',
        isOwner: true,
        role,
      },
    } as any;
  }

  it('should pass successfull response of remote reserver back to the user', async () => {
    fetch.mockImplementation(() => ({
      status: 200,
      headers: new Map(),
      text: () => Promise.resolve('successfull response'),
    }));
    await controller.create(make_authorizedRequest(), {
      payload_url: 'test.com',
      events: [EventType.reservationConfirmed],
      secret: '12345',
    });

    const response = await controller.testWebhook(
      make_authorizedRequest(),
      1,
      {},
    );

    expect(response).toEqual(
      new SuccessfullWebhookResponse(200, {}, 'successfull response'),
    );
  });

  it('should pass error response of remote reserver back to the user', async () => {
    fetch.mockImplementation(() => ({
      status: 400,
      headers: new Map(),
      text: () => Promise.resolve('error response'),
    }));
    await controller.create(make_authorizedRequest(), {
      payload_url: 'test.com',
      events: [EventType.reservationConfirmed],
      secret: '12345',
    });

    const response = await controller.testWebhook(
      make_authorizedRequest(),
      1,
      {},
    );

    expect(response).toEqual(
      new SuccessfullWebhookResponse(400, {}, 'error response'),
    );
  });

  it('should throw NotFoundExcpetion if missing webhook selected', async () => {
    fetch.mockImplementation(() => ({
      status: 400,
      headers: new Map(),
      text: () => Promise.resolve('error response'),
    }));

    const fn = controller.testWebhook(make_authorizedRequest(), 1, {});

    await expect(fn).rejects.toThrow(NotFoundException);
  });

  it('should repond with error response', async () => {
    fetch.mockImplementation(() => Promise.reject(new Error('Failure')));
    await controller.create(make_authorizedRequest(), {
      payload_url: 'test.com',
      events: [EventType.reservationConfirmed],
      secret: '12345',
    });

    const response = await controller.testWebhook(
      make_authorizedRequest(),
      1,
      {},
    );

    expect(response).toEqual(new ErrorWebhookResponse('Error: Failure'));
  });
});
