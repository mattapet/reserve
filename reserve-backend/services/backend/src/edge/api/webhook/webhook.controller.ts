/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  UseGuards,
  Get,
  Req,
  Param,
  Post,
  Body,
  Put,
  Patch,
  Delete,
  UseInterceptors,
  NotFoundException,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  getSchemaPath,
  ApiExtraModels,
} from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { match } from '@rsaas/util';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';
import { JwtGuard } from '@rsaas/commons/lib/guards/jwt.guard';
import { RoleGuard } from '@rsaas/commons/lib/guards/role.guard';
import { UserRole } from '@rsaas/util/lib/user-role.type';
import { AnyObject } from '@rsaas/util/lib/AnyObject';

import {
  EncodingType,
  WebhookNotFoundError,
  WebhookService,
  Webhook,
} from '../../webhook';

import { TransformerInterceptor } from './interceptors/transformer.interceptor';
import { CreateWebhookDTO } from './dto/create-webhook.dto';
import { UpdateWebhookDTO } from './dto/update-webhook.dto';
import {
  SuccessfullWebhookResponse,
  ErrorWebhookResponse,
} from '../../webhook-dispatcher/webhook-response.value';
import { WebhookDispatcherService } from '../../webhook-dispatcher';
import { WenhookCouldNotBeDispatchError } from '../../webhook-dispatcher/webhook-dispatcher.errors';
import { WebhookDTO } from './dto/webhook.dto';
import {
  SuccessfullWebhookResponseDTO,
  ErrorWebhookResponseDTO,
} from './dto/test-webhook-response.dto';
//#endregion

@ApiTags('webhook')
@ApiBearerAuth()
@Controller('api/v1/webhook')
@UseGuards(JwtGuard, RoleGuard(UserRole.admin))
export class WebhookController {
  public constructor(
    private readonly service: WebhookService,
    private readonly dispatcher: WebhookDispatcherService,
  ) {}

  @Get()
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({
    type: [WebhookDTO],
    description: 'List of configured webhooks.',
  })
  public async getAll(
    @Req()
    req: WorkspaceAuthorizedRequest,
  ): Promise<Webhook[]> {
    const workspaceId = req.user.workspace;
    return this.service.getAll(workspaceId);
  }

  @Get(':id')
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({ type: WebhookDTO })
  @ApiNotFoundResponse({
    description: 'Webhook with the given `id` does not exist.',
  })
  public async getById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') webhookId: number,
  ): Promise<Webhook> {
    try {
      const workspaceId = req.user.workspace;
      return await this.service.getById(workspaceId, webhookId);
    } catch (error) {
      throw match(error)
        .whenType(WebhookNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Post(':id/test')
  @HttpCode(HttpStatus.OK)
  @ApiExtraModels(SuccessfullWebhookResponseDTO, ErrorWebhookResponseDTO)
  @ApiOkResponse({
    schema: {
      oneOf: [
        { $ref: getSchemaPath(SuccessfullWebhookResponseDTO) },
        { $ref: getSchemaPath(ErrorWebhookResponseDTO) },
      ],
    },
    description:
      'Forwards the response of the remote service, or returns an error encountered.',
  })
  @ApiNotFoundResponse({
    description: 'Webhook with the given `id` does not exist.',
  })
  public async testWebhook(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') webhookId: number,
    @Body() payload: AnyObject,
  ): Promise<SuccessfullWebhookResponse | ErrorWebhookResponse> {
    try {
      const workspaceId = req.user.workspace;
      const webhook = await this.service.getById(workspaceId, webhookId);
      return await this.dispatcher.dispatch(webhook, payload);
    } catch (error) {
      return match(error)
        .whenType(WenhookCouldNotBeDispatchError)
        .then(() => new ErrorWebhookResponse(error.message))
        .whenType(WebhookNotFoundError)
        .then(() => {
          throw new NotFoundException(error.message);
        })
        .getOrDefault(() => {
          throw error;
        });
    }
  }

  @Post()
  @Transactional()
  @UseInterceptors(TransformerInterceptor)
  @ApiCreatedResponse({ type: WebhookDTO })
  public async create(
    @Req() req: WorkspaceAuthorizedRequest,
    @Body()
    {
      payload_url: payloadUrl,
      secret,
      events,
      encoding = EncodingType.urlencoded,
    }: CreateWebhookDTO,
  ): Promise<Webhook> {
    const workspaceId = req.user.workspace;
    const webhook = new Webhook();

    await this.service.create(webhook, {
      workspaceId,
      payloadUrl,
      secret,
      events,
      encoding,
    });

    return webhook;
  }

  @Put(':id')
  @Transactional()
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({ type: WebhookDTO })
  @ApiNotFoundResponse({
    description: 'Webhook with the given `id` does not exist',
  })
  public async updateById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') webhookId: number,
    @Body()
    { payload_url: payloadUrl, secret, events, encoding }: UpdateWebhookDTO,
  ): Promise<Webhook> {
    try {
      const workspaceId = req.user.workspace;
      const webhook = await this.service.getById(workspaceId, webhookId);

      await this.service.update(webhook, {
        payloadUrl,
        secret,
        events,
        encoding,
      });

      return webhook;
    } catch (error) {
      throw match(error)
        .whenType(WebhookNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Patch(':id/toggle')
  @Transactional()
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({ type: WebhookDTO })
  @ApiNotFoundResponse({
    description: 'Webhook with the given `id` does not exist',
  })
  public async toggleActiveById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') webhookId: number,
  ): Promise<Webhook> {
    try {
      const workspaceId = req.user.workspace;
      const webhook = await this.service.getById(workspaceId, webhookId);

      await this.service.toggleActive(webhook);

      return webhook;
    } catch (error) {
      throw match(error)
        .whenType(WebhookNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Delete(':id')
  @Transactional()
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiNoContentResponse()
  @ApiNotFoundResponse({
    description: 'Webhook with the given `id` does not exist',
  })
  public async removeById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') webhookId: number,
  ): Promise<void> {
    try {
      const workspaceId = req.user.workspace;
      const webhook = await this.service.getById(workspaceId, webhookId);

      await this.service.remove(webhook);
    } catch (error) {
      throw match(error)
        .whenType(WebhookNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }
}
