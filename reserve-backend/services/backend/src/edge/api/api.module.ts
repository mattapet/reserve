/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { AuthApiModule } from './auth';
import { ReservationApiModule } from './reservation';
import { UserApiModule } from './user';
import { WorkspaceApiModule } from './workspace';
import { ResourceApiModule } from './resource';
import { ConsoleApiModule } from './console';
import { SetupApiModule } from './setup';
import { VersionApiModule } from './version';
import { SettingsApiModule } from './settings';
import { WebhookApiModule } from './webhook';
import { JwtModule } from './jwt/jwt.module';
import { OAuth2ApiModule } from './oauth2/oauth2-api.module';
//#endregion

@Module({
  imports: [
    JwtModule,
    AuthApiModule,
    OAuth2ApiModule,
    ReservationApiModule,
    ResourceApiModule,
    UserApiModule,
    WorkspaceApiModule,
    ConsoleApiModule,
    SetupApiModule,
    WebhookApiModule,
    SettingsApiModule,
    VersionApiModule,
  ],
})
export class ApiModule {}
