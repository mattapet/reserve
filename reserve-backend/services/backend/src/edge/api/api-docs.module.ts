/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module, INestApplication, Global } from '@nestjs/common';
import { AuthApiModule } from './auth';
import { ReservationApiModule } from './reservation';
import { UserApiModule } from './user';
import { WorkspaceApiModule } from './workspace';
import { ResourceApiModule } from './resource';
import { ConsoleApiModule } from './console';
import { SetupApiModule } from './setup';
import { VersionApiModule } from './version';
import { SettingsApiModule } from './settings';
import { WebhookApiModule } from './webhook';
import { OAuth2ApiModule } from './oauth2/oauth2-api.module';
import { OpenAPIObject, SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
//#endregion

@Module({})
@Global()
export class ApiDocsModule {
  public static createWorkspaceDocument(app: INestApplication): OpenAPIObject {
    const options = new DocumentBuilder()
      .setTitle('Reservation as a Service Workspace API')
      .setDescription(
        'This API is used for interaction with a specific Workspace.',
      )
      .setVersion('1.0')
      .build();

    return SwaggerModule.createDocument(app, options, {
      include: [
        AuthApiModule,
        OAuth2ApiModule,
        ReservationApiModule,
        ResourceApiModule,
        UserApiModule,
        WorkspaceApiModule,
        WebhookApiModule,
        SettingsApiModule,
        VersionApiModule,
      ],
      deepScanRoutes: true,
      ignoreGlobalPrefix: false,
    });
  }

  public static createSetupDocument(app: INestApplication): OpenAPIObject {
    const options = new DocumentBuilder()
      .setTitle('Reservation as a Service Setup API')
      .setDescription('This API is used for opening Workspace Requests.')
      .setVersion('1.0')
      .build();

    return SwaggerModule.createDocument(app, options, {
      include: [SetupApiModule],
      deepScanRoutes: true,
      ignoreGlobalPrefix: false,
    });
  }

  public static createConsoleDocument(app: INestApplication): OpenAPIObject {
    const options = new DocumentBuilder()
      .setTitle('Reservation as a Service Console API')
      .setDescription('This API is used for system administration.')
      .setVersion('1.0')
      .build();

    return SwaggerModule.createDocument(app, options, {
      include: [ConsoleApiModule],
      deepScanRoutes: true,
      ignoreGlobalPrefix: false,
    });
  }
}
