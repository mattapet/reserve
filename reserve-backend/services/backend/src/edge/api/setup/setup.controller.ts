/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import qs from 'qs';
import {
  Controller,
  Post,
  Body,
  BadRequestException,
  ClassSerializerInterceptor,
  UseInterceptors,
  Param,
  Query,
  Res,
  Get,
  HttpCode,
  HttpStatus,
  UnauthorizedException,
} from '@nestjs/common';
// tslint:disable:no-implicit-dependencies
import { Response } from 'express';
import {
  ApiTags,
  ApiResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
  ApiAcceptedResponse,
  ApiBadRequestResponse,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { uuid, match } from '@rsaas/util';

import { VerificationService } from '../../verfication';
import { SetupService } from '../../setup/setup.service';
import {
  WorkspaceRequest,
  IllegalWorkspaceNameError,
  WorkspaceRequestNameCollisionError,
  WorkspaceName,
} from '../../../workspace-management';
import {
  AuthorityService,
  AuthorityAuthorizationService,
} from '../../../user-access';

import {
  AuthorityNotFoundError,
  AuthorizationCodeError,
  AuthorizedUserFetchError,
} from '../../../user-access/domain/authority/authority.errors';
import { TransformerInterceptor } from './interceptors/transformer.interceptor';
import { WorkspaceRequestDTO } from './dto/workspace-request.dto';
import { EmailVerificationCompletionDTO } from './dto/email-verification-completion.dto';
import { AuthorityVerificationCompletionDTO } from './dto/authority-verification-completion.dto';
//#endregion

@Controller('api/v1')
export class SetupController {
  public constructor(
    private readonly service: SetupService,
    private readonly verificationService: VerificationService,
    private readonly authorityService: AuthorityService,
    private readonly authorizationService: AuthorityAuthorizationService,
  ) {}

  @Post('setup/verify/email')
  @HttpCode(HttpStatus.ACCEPTED)
  @ApiTags('email-verification')
  @ApiAcceptedResponse({ description: 'A verification email was sent.' })
  public async verifyOwnerByEmail(
    @Body('email') email: string,
    @Body('redirect_uri') redirectUri: string,
  ): Promise<void> {
    await this.service.verify(redirectUri, uuid(), email);
  }

  @Get('setup/verify/authority/:authority')
  @ApiTags('email-verification')
  @ApiResponse({
    status: 301,
    description:
      'Redirects user to the authorization url of the external `authority`.',
  })
  @ApiNotFoundResponse({
    description:
      'The `authority` does not exist. Currently the only registered is `silicon_hill`.',
  })
  public async verifyOwnerByAuthority(
    @Param('authority') name: string,
    @Query('state') state: string,
    @Res() res: Response,
  ): Promise<void> {
    try {
      const authority = this.authorityService.getByName(name);
      const query = this.authorizationService.getAuthorizationQuery(
        state,
        authority,
      );
      res.redirect(
        `${authority.getAuthorizationUrl()}${
          authority.getAuthorizationUrl().indexOf('?') < 0 ? '?' : '&'
        }${qs.stringify(query)}`,
      );
    } catch (error) {
      throw match(error)
        .whenType(AuthorityNotFoundError)
        .then(() => new BadRequestException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Post('setup/complete/email')
  @Transactional()
  @UseInterceptors(ClassSerializerInterceptor, TransformerInterceptor)
  @ApiTags('completion')
  @ApiCreatedResponse({
    type: WorkspaceRequestDTO,
    description:
      'Verification completed successfully and the workspace request have been opened',
  })
  @ApiBadRequestResponse({
    description:
      'Invalid payload or a name collision with already existing workspace.',
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid email verification information.',
  })
  public async completeSetupByEmail(
    @Body()
    {
      workspace_id: workspaceId,
      email: email,
      expiry: expiry,
      signature: signature,
      workspace: name,
      password: password,
    }: EmailVerificationCompletionDTO,
  ): Promise<WorkspaceRequest> {
    try {
      const valid = await this.verificationService.confirm(
        workspaceId,
        email,
        expiry,
        signature,
      );
      if (!valid) throw new UnauthorizedException('Corrupted token.');

      const request = new WorkspaceRequest();
      await this.service.completeEmailVerification(
        request,
        workspaceId,
        new WorkspaceName(name),
        email,
        password,
      );

      return request;
    } catch (error) {
      throw match(error)
        .whenType(IllegalWorkspaceNameError)
        .orWhenType(WorkspaceRequestNameCollisionError)
        .then(() => new BadRequestException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Post('setup/complete/authority')
  @UseInterceptors(ClassSerializerInterceptor, TransformerInterceptor)
  @Transactional()
  @ApiTags('completion')
  @ApiCreatedResponse({
    type: WorkspaceRequestDTO,
    description:
      'Verification completed successfully and the workspace request have been opened',
  })
  @ApiBadRequestResponse({
    description:
      'Invalid payload or a name collision with already existing workspace, or an invalid authority identifier provided.',
  })
  @ApiUnauthorizedResponse({
    description:
      'An error happened while completing OAuth2.0 communication with the authority.',
  })
  public async completeSetupByAuthority(
    @Body()
    {
      authority: authorityName,
      workspace: name,
      state: state,
      code: code,
    }: AuthorityVerificationCompletionDTO,
  ): Promise<WorkspaceRequest> {
    try {
      const workspaceId = uuid();
      const authority = this.authorityService.getByName(authorityName);

      const request = new WorkspaceRequest();
      await this.service.completeAuthorityAuthentication(
        request,
        workspaceId,
        new WorkspaceName(name),
        state,
        code,
        authority,
      );

      return request;
    } catch (error) {
      throw match(error)
        .whenType(IllegalWorkspaceNameError)
        .orWhenType(WorkspaceRequestNameCollisionError)
        .orWhenType(AuthorityNotFoundError)
        .then(() => new BadRequestException(error.message))
        .whenType(AuthorizationCodeError)
        .orWhenType(AuthorizedUserFetchError)
        .then(() => new UnauthorizedException(error.message))
        .getOrDefault(() => error);
    }
  }
}
