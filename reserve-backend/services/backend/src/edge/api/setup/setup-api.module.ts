/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { SetupController } from './setup.controller';
import { SetupModule } from '../../setup';
import { VerificationModule } from '../../verfication';
import { UserAccessModule } from '../../../user-access';
//#endregion

@Module({
  imports: [SetupModule, VerificationModule, UserAccessModule],
  controllers: [SetupController],
})
export class SetupApiModule {}
