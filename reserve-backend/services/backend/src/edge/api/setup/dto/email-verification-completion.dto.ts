/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IsNotEmpty, IsString, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class EmailVerificationCompletionDTO {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public readonly workspace_id: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public readonly email: string;

  @IsNumber()
  @ApiProperty()
  public readonly expiry: number;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public readonly signature: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public readonly workspace: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public readonly password: string;

  public constructor(
    workspace_id: string,
    email: string,
    expiry: number,
    signature: string,
    name: string,
    password: string,
  ) {
    this.workspace_id = workspace_id;
    this.email = email;
    this.expiry = expiry;
    this.signature = signature;
    this.workspace = name;
    this.password = password;
  }
}
