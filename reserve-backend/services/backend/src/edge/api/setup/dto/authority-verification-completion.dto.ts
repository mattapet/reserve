/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IsString, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class AuthorityVerificationCompletionDTO {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  authority: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  workspace: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  state: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  code: string;

  public constructor(
    authority: string,
    workspace: string,
    state: string,
    code: string,
  ) {
    this.authority = authority;
    this.workspace = workspace;
    this.state = state;
    this.code = code;
  }
}
