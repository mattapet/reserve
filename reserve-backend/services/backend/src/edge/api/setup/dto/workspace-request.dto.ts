/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
//#region imports
import { ApiProperty } from '@nestjs/swagger';

import { WorkspaceRequestState } from '../../../../workspace-management';
//#endregion

export class WorkspaceRequestDTO {
  @ApiProperty()
  public readonly workspace_id: string;
  @ApiProperty()
  public readonly workspace_name: string;
  @ApiProperty()
  public readonly owner_id: string;
  @ApiProperty()
  public readonly state: WorkspaceRequestState;
  @ApiProperty()
  public readonly opened_at: string;

  @ApiProperty({ required: false })
  public readonly closed_at?: string;

  public constructor(
    workspace_id: string,
    workspace_name: string,
    owner_id: string,
    state: WorkspaceRequestState,
    opened_at: string,
    closed_at?: string,
  ) {
    this.workspace_id = workspace_id;
    this.workspace_name = workspace_name;
    this.owner_id = owner_id;
    this.state = state;
    this.opened_at = opened_at;
    this.closed_at = closed_at;
  }
}
