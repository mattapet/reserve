/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { WorkspaceRequest } from '../../../../workspace-management';
import { WorkspaceRequestDTO } from '../dto/workspace-request.dto';
//#endregion

@Injectable()
export class TransformerInterceptor
  implements NestInterceptor<WorkspaceRequest | WorkspaceRequest[]> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<WorkspaceRequest | WorkspaceRequest[]>,
  ): Observable<WorkspaceRequestDTO | WorkspaceRequestDTO[]> {
    return call$
      .handle()
      .pipe(
        map((payload: WorkspaceRequest | WorkspaceRequest[]) =>
          Array.isArray(payload)
            ? payload.map(this.encode)
            : this.encode(payload),
        ),
      );
  }

  private encode = (workspaceRequest: WorkspaceRequest) =>
    new WorkspaceRequestDTO(
      workspaceRequest.getId(),
      workspaceRequest.getWorkspaceName().get(),
      workspaceRequest.getOwner().getId(),
      workspaceRequest.getState(),
      workspaceRequest.getOpenedAt().toISOString(),
      workspaceRequest.isClosed()
        ? workspaceRequest.getClosedAt().toISOString()
        : undefined,
    );
}
