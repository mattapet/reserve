/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IsString, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class RefreshTokenDTO {
  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  public readonly refresh_token: string;

  public constructor(refresh_token: string) {
    this.refresh_token = refresh_token;
  }
}
