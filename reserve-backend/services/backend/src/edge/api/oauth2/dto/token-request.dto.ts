/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { GrantType } from '@rsaas/util/lib/oauth2/grant-type.type';
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class TokenRequestAuthorizationCode {
  @ApiProperty({ enum: [GrantType.authorizationCode] })
  public readonly grant_type = GrantType.authorizationCode;

  @ApiProperty()
  public readonly code: string;

  @ApiProperty()
  public readonly client_id: string;

  @ApiProperty()
  public readonly client_secret: string;

  @ApiProperty()
  public readonly state?: string;

  public constructor(
    code: string,
    client_id: string,
    client_secret: string,
    state?: string,
  ) {
    this.code = code;
    this.client_id = client_id;
    this.client_secret = client_secret;
    this.state = state;
  }
}

export class TokenRequestClientCredentials {
  @ApiProperty({ enum: [GrantType.clientCredentials] })
  public readonly grant_type = GrantType.clientCredentials;

  @ApiProperty()
  public readonly client_id: string;

  @ApiProperty()
  public readonly client_secret: string;

  @ApiProperty({ required: false })
  public readonly scope?: string;

  public constructor(client_id: string, client_secret: string, scope?: string) {
    this.client_id = client_id;
    this.client_secret = client_secret;
    this.scope = scope;
  }
}

export class TokenRequestPassword {
  @ApiProperty({ enum: [GrantType.password] })
  public readonly grant_type = GrantType.password;

  @ApiProperty()
  public readonly username: string;

  @ApiProperty()
  public readonly password: string;

  @ApiProperty()
  public readonly workspace: string;

  @ApiProperty({ required: false })
  public readonly scope?: string;

  public constructor(
    username: string,
    password: string,
    workspace: string,
    scope?: string,
  ) {
    this.username = username;
    this.password = password;
    this.workspace = workspace;
    this.scope = scope;
  }
}

export class TokenRequestRefreshToken {
  @ApiProperty({ enum: [GrantType.refreshToken] })
  public readonly grant_type = GrantType.refreshToken;

  @ApiProperty()
  public readonly refresh_token: string;

  @ApiProperty({ required: false })
  public readonly scope?: string;

  public constructor(refresh_token: string, scope?: string) {
    this.refresh_token = refresh_token;
    this.scope = scope;
  }
}

export type TokenRequestDTO =
  | TokenRequestAuthorizationCode
  | TokenRequestClientCredentials
  | TokenRequestPassword
  | TokenRequestRefreshToken;
