/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TokenResponseDTO } from '../../auth/dto/token-response.dto';
//#endregion

export interface AuthorizeResponseCodeDTO {
  readonly code: string;
  readonly workspace: string;
  readonly state?: string;
}

export type AuthorizeResponseDTO = AuthorizeResponseCodeDTO | TokenResponseDTO;
