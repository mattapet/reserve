/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { registerJWTServiceModule } from '@rsaas/commons/lib/jwt-service.module';

import { WorkspaceManagementModule } from '../../../workspace-management';
import { UserAccessModule } from '../../../user-access';

import { JwtModule } from '../jwt';

import { WorkspaceJWTService } from '../auth/services/workspace-jwt.service';
import { TokenController } from './token.controller';
//#endregion

@Module({
  imports: [
    JwtModule,
    registerJWTServiceModule(),
    WorkspaceManagementModule,
    UserAccessModule,
  ],
  controllers: [TokenController],
  providers: [WorkspaceJWTService],
})
export class OAuth2ApiModule {}
