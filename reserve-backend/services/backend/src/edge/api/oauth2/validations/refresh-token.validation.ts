/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, PipeTransform } from '@nestjs/common';

import { WorkspaceJWTService } from '../../auth/services/workspace-jwt.service';
//#endregion

@Injectable()
export class RefreshTokenValidation
  implements PipeTransform<string | undefined, Promise<string | undefined>> {
  public constructor(private readonly jwtService: WorkspaceJWTService) {}

  public async transform(value?: string): Promise<string | undefined> {
    if (!value) {
      return;
    }

    const { jit } = await this.jwtService.verify(value);
    return jit;
  }
}
