/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TokenController } from '../token.controller';

import { BadRequestException } from '@nestjs/common';

import {
  WorkspaceService,
  Workspace,
  WorkspaceName,
  WorkspaceOwner,
} from '../../../../workspace-management';
import {
  User,
  Identity,
  UserService,
  AuthService,
  TokenService,
  AuthorityAuthorizationService,
} from '../../../../user-access';

import { UserServiceBuilder } from '../../../../user-access/__tests__/user/builders/user.service.builder';
import { WorkspaceServiceBuilder } from '../../../../workspace-management/__tests__/workspace/builders/workspace.service.builder';
import { InMemoryTokenRepository } from '../../../../user-access/application/authentication/token/repositories/in-memory/in-memory-console-token.repository';

import { TokenRequestPassword } from '../dto/token-request.dto';
//#endregion

describe('password-grant-type.token.controller', () => {
  let userService: UserService;
  let authService: AuthService;
  let workspaceService: WorkspaceService;
  let tokenService: TokenService;
  let controller: TokenController;

  beforeEach(() => {
    userService = UserServiceBuilder.inMemory();
    authService = new AuthService(
      userService,
      new AuthorityAuthorizationService(),
    );
    workspaceService = WorkspaceServiceBuilder.inMemory();
    tokenService = new TokenService(new InMemoryTokenRepository());
    controller = new TokenController(
      workspaceService,
      authService,
      tokenService,
    );
  });

  async function effect_createWorkspaceWithOwner(user: User, userId: string) {
    await userService.createOwner(
      user,
      new Identity(userId, 'test', 'test@test.com'),
    );
    const name = new WorkspaceName('test');
    const owner = WorkspaceOwner.from(user);
    await workspaceService.create(new Workspace(), 'test', name, owner);
  }

  describe('password grant type', () => {
    it('should respond with valid token pair', async () => {
      const user = new User();
      effect_createWorkspaceWithOwner(user, '99');
      await authService.setPassword(user, '12345');

      const response = await controller.getToken(
        new TokenRequestPassword('test@test.com', '12345', 'test'),
      );

      expect(response.accessToken.isValid()).toBe(true);
      expect(response.refreshToken.isValid()).toBe(true);
    });

    it('should throw BadRequestException if password does not match', async () => {
      const user = new User();
      effect_createWorkspaceWithOwner(user, '99');
      await authService.setPassword(user, 'password');

      const fn = controller.getToken(
        new TokenRequestPassword('test@test.com', '12345', 'test'),
      );

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException if workspace does not exist', async () => {
      const user = new User();
      await userService.createOwner(
        user,
        new Identity('99', 'test', 'test@test.com'),
      );
      await authService.setPassword(user, 'password');

      const fn = controller.getToken(
        new TokenRequestPassword('test@test.com', '12345', 'test'),
      );

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException if invalid workspace name passed', async () => {
      const user = new User();
      effect_createWorkspaceWithOwner(user, '99');
      await authService.setPassword(user, '12345');

      const fn = controller.getToken(
        new TokenRequestPassword('test@test.com', '12345', 't'),
      );

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException if user does not exist', async () => {
      const user = new User();
      effect_createWorkspaceWithOwner(user, '99');

      const fn = controller.getToken(
        new TokenRequestPassword('james@example.com', '12345', 'test'),
      );

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException if user is banned', async () => {
      const owner = new User();
      effect_createWorkspaceWithOwner(owner, '99');
      const user = new User();
      await userService.create(
        user,
        new Identity('88', 'test', 'james@example.com'),
      );
      await userService.toggleBanned(user, owner);
      await authService.setPassword(user, '12345');

      const fn = controller.getToken(
        new TokenRequestPassword('james@example.com', '12345', 'test'),
      );

      await expect(fn).rejects.toThrow(BadRequestException);
    });
  });
});
