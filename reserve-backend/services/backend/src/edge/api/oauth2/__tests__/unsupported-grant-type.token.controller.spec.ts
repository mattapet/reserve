/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TokenController } from '../token.controller';

import { NotImplementedException } from '@nestjs/common';

import { WorkspaceService } from '../../../../workspace-management';
import {
  UserService,
  AuthService,
  TokenService,
  AuthorityAuthorizationService,
} from '../../../../user-access';

import { UserServiceBuilder } from '../../../../user-access/__tests__/user/builders/user.service.builder';
import { WorkspaceServiceBuilder } from '../../../../workspace-management/__tests__/workspace/builders/workspace.service.builder';
import { InMemoryTokenRepository } from '../../../../user-access/application/authentication/token/repositories/in-memory/in-memory-console-token.repository';

import {
  TokenRequestAuthorizationCode,
  TokenRequestClientCredentials,
} from '../dto/token-request.dto';
//#endregion

describe('password-grant-type.token.controller', () => {
  let userService: UserService;
  let authService: AuthService;
  let workspaceService: WorkspaceService;
  let tokenService: TokenService;
  let controller: TokenController;

  beforeEach(() => {
    userService = UserServiceBuilder.inMemory();
    authService = new AuthService(
      userService,
      new AuthorityAuthorizationService(),
    );
    workspaceService = WorkspaceServiceBuilder.inMemory();
    tokenService = new TokenService(new InMemoryTokenRepository());
    controller = new TokenController(
      workspaceService,
      authService,
      tokenService,
    );
  });

  describe('authorization_code grant type', () => {
    it('should throw NotImplementedException when grant type set to authorization code', async () => {
      const fn = controller.getToken(
        new TokenRequestAuthorizationCode('code', 'client-id', 'client-secret'),
      );

      await expect(fn).rejects.toThrow(NotImplementedException);
    });
  });

  describe('client_credentials grant type', () => {
    it('should throw NotImplementedException when grant type set to authorization code', async () => {
      const fn = controller.getToken(
        new TokenRequestClientCredentials('client-id', 'client-secret'),
      );

      await expect(fn).rejects.toThrow(NotImplementedException);
    });
  });
});
