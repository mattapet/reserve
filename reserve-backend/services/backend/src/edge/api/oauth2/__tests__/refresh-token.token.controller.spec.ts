/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TokenController } from '../token.controller';
import { BadRequestException } from '@nestjs/common';

import {
  WorkspaceService,
  Workspace,
  WorkspaceName,
  WorkspaceOwner,
} from '../../../../workspace-management';
import {
  User,
  Identity,
  UserService,
  TokenService,
  AuthService,
  AuthorityAuthorizationService,
} from '../../../../user-access';

import { UserServiceBuilder } from '../../../../user-access/__tests__/user/builders/user.service.builder';
import { WorkspaceServiceBuilder } from '../../../../workspace-management/__tests__/workspace/builders/workspace.service.builder';
import { InMemoryTokenRepository } from '../../../../user-access/application/authentication/token/repositories/in-memory/in-memory-console-token.repository';

import {
  TokenRequestPassword,
  TokenRequestRefreshToken,
} from '../dto/token-request.dto';
//#endregion

describe('password-grant-type.token.controller', () => {
  let userService: UserService;
  let authService: AuthService;
  let workspaceService: WorkspaceService;
  let tokenService: TokenService;
  let controller: TokenController;

  beforeEach(() => {
    userService = UserServiceBuilder.inMemory();
    authService = new AuthService(
      userService,
      new AuthorityAuthorizationService(),
    );
    workspaceService = WorkspaceServiceBuilder.inMemory();
    tokenService = new TokenService(new InMemoryTokenRepository());
    controller = new TokenController(
      workspaceService,
      authService,
      tokenService,
    );
  });

  async function effect_createWorkspaceWithOwner(user: User, userId: string) {
    await userService.createOwner(
      user,
      new Identity(userId, 'test', 'test@test.com'),
    );
    const name = new WorkspaceName('test');
    const owner = WorkspaceOwner.from(user);
    await workspaceService.create(new Workspace(), 'test', name, owner);
  }

  describe('refresh_token grant type', () => {
    it('should refresh access token', async () => {
      const user = new User();
      await effect_createWorkspaceWithOwner(user, '99');
      await authService.setPassword(user, '12345');

      const { refreshToken } = await controller.getToken(
        new TokenRequestPassword('test@test.com', '12345', 'test'),
      );
      const response = await controller.getToken(
        new TokenRequestRefreshToken(refreshToken.value),
      );

      expect(response.accessToken.isValid()).toBe(true);
      expect(response.refreshToken.isValid()).toBe(true);
    });

    it('should throw BadRequestException if token is not found', async () => {
      const user = new User();
      await effect_createWorkspaceWithOwner(user, '99');
      await authService.setPassword(user, '12345');

      const fn = controller.getToken(
        new TokenRequestRefreshToken('some-random-token'),
      );

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException if token is expired', async () => {
      const user = new User();
      await effect_createWorkspaceWithOwner(user, '99');
      await authService.setPassword(user, '12345');

      const { refreshToken } = await controller.getToken(
        new TokenRequestPassword('test@test.com', '12345', 'test'),
      );
      jest.spyOn(Date, 'now').mockImplementation(() => Infinity);
      const fn = controller.getToken(
        new TokenRequestRefreshToken(refreshToken.value),
      );

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException if token is revoked', async () => {
      const user = new User();
      await effect_createWorkspaceWithOwner(user, '99');
      await authService.setPassword(user, '12345');

      const { refreshToken } = await controller.getToken(
        new TokenRequestPassword('test@test.com', '12345', 'test'),
      );
      await tokenService.revokeRefreshToken(refreshToken);
      const fn = controller.getToken(
        new TokenRequestRefreshToken(refreshToken.value),
      );

      await expect(fn).rejects.toThrow(BadRequestException);
    });
  });

  describe('token revocation', () => {
    it('should be able to revoke the refresh token passed in body', async () => {
      const user = new User();
      await effect_createWorkspaceWithOwner(user, '99');
      await authService.setPassword(user, '12345');

      const { refreshToken } = await controller.getToken(
        new TokenRequestPassword('test@test.com', '12345', 'test'),
      );
      await controller.revoke(refreshToken.value);

      const rt = await tokenService.getRefreshToken(refreshToken.value);
      expect(rt.isRevoked()).toBe(true);
    });

    it('should be able to revoke the refresh token passed in query', async () => {
      const user = new User();
      await effect_createWorkspaceWithOwner(user, '99');
      await authService.setPassword(user, '12345');

      const { refreshToken } = await controller.getToken(
        new TokenRequestPassword('test@test.com', '12345', 'test'),
      );
      await controller.revoke(undefined, refreshToken.value);

      const rt = await tokenService.getRefreshToken(refreshToken.value);
      expect(rt.isRevoked()).toBe(true);
    });

    it('should throw BadRequestError when revoking invalid refresh token', async () => {
      const user = new User();
      await effect_createWorkspaceWithOwner(user, '99');
      await authService.setPassword(user, '12345');

      const { refreshToken } = await controller.getToken(
        new TokenRequestPassword('test@test.com', '12345', 'test'),
      );
      await controller.revoke(refreshToken.value);
      const fn = controller.revoke(refreshToken.value);

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestError when revoking missing', async () => {
      const fn = controller.revoke('some-random-token');

      await expect(fn).rejects.toThrow(BadRequestException);
    });
  });
});
