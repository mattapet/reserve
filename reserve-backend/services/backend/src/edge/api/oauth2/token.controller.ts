/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  NotImplementedException,
  BadRequestException,
  Controller,
  Post,
  UsePipes,
  HttpCode,
  HttpStatus,
  Body,
  UseInterceptors,
  Query,
} from '@nestjs/common';
import {
  ApiTags,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiExtraModels,
  ApiBody,
  getSchemaPath,
  ApiBadRequestResponse,
  ApiNotImplementedResponse,
  ApiQuery,
} from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { match } from '@rsaas/util';
import { Scope } from '@rsaas/util/lib/oauth2/scope.type';
import { GrantType } from '@rsaas/util/lib/oauth2/grant-type.type';

import {
  WorkspaceService,
  WorkspaceNotFoundError,
  WorkspaceName,
  IllegalWorkspaceNameError,
} from '../../../workspace-management';
import {
  AuthService,
  InvalidEmailOrPasswordError,
  UserBannedError,
  TokenService,
  AccessToken,
  RefreshToken,
  TokenNotFoundError,
  InvalidTokenError,
} from '../../../user-access';

import { TokenResponse } from '../auth/token-response.value';
import {
  TokenRequestDTO,
  TokenRequestRefreshToken,
  TokenRequestPassword,
  TokenRequestAuthorizationCode,
  TokenRequestClientCredentials,
} from './dto/token-request.dto';
import { TokenValidation } from './validations/token.validation';
import { TokenResponseInterceptor } from '../auth/interceptors/token-response.interceptor';
import { RefreshTokenValidation } from './validations/refresh-token.validation';
import { TokenResponseDTO } from '../auth/dto/token-response.dto';
import { RefreshTokenDTO } from './dto/refresh-token.dto';
//#endregion

@ApiTags('oauth2', 'auth')
@Controller('api/v1/oauth2/token')
export class TokenController {
  public constructor(
    private readonly workspaceService: WorkspaceService,
    private readonly authService: AuthService,
    private readonly tokenService: TokenService,
  ) {}

  @Post('revoke')
  @Transactional()
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiExtraModels(RefreshTokenDTO)
  @ApiBody({
    required: false,
    type: RefreshTokenDTO,
    description:
      'The request is expected to contain the `refresh_token` property either in the body or passed as the query parameter.',
  })
  @ApiQuery({
    required: false,
    name: 'refresh_token',
    type: String,
    description:
      'The request is expected to contain the `refresh_token` property either in the body or passed as the query parameter.',
  })
  @ApiNoContentResponse({
    description: 'The refresh token was successfully revoked.',
  })
  @ApiBadRequestResponse({ description: 'Invalid refresh token provided.' })
  public async revoke(
    @Body('refresh_token', RefreshTokenValidation) refreshToken1?: string,
    @Query('refresh_token', RefreshTokenValidation) refreshToken2?: string,
  ): Promise<void> {
    try {
      const rt = refreshToken1 ?? refreshToken2 ?? '';
      const refreshToken = await this.tokenService.getRefreshToken(rt);

      await this.tokenService.revokeRefreshToken(refreshToken);
    } catch (error) {
      throw match(error)
        .whenType(TokenNotFoundError)
        .orWhenType(InvalidTokenError)
        .then(() => new BadRequestException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Post()
  @Transactional()
  @UsePipes(TokenValidation)
  @UseInterceptors(TokenResponseInterceptor)
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ type: TokenResponseDTO })
  @ApiExtraModels(
    TokenRequestAuthorizationCode,
    TokenRequestClientCredentials,
    TokenRequestPassword,
    TokenRequestRefreshToken,
  )
  @ApiBody({
    schema: {
      oneOf: [
        { $ref: getSchemaPath(TokenRequestAuthorizationCode) },
        { $ref: getSchemaPath(TokenRequestClientCredentials) },
        { $ref: getSchemaPath(TokenRequestPassword) },
        { $ref: getSchemaPath(TokenRequestRefreshToken) },
      ],
      example: {
        grant_type: 'password',
        username: 'username',
        password: 'super-secret-password',
        workspace: 'test-workspace',
      },
    },
  })
  @ApiOkResponse({ type: TokenResponseDTO })
  @ApiBadRequestResponse({
    description:
      '`password` grant type:\n' +
      '  - invalid workspace name\n' +
      '  - invalid username-password combination\n' +
      '  - user have been banned\n' +
      '\n' +
      '`refresh_token` grant type:\n' +
      '  - invalid refresh token provided\n',
  })
  @ApiNotImplementedResponse({
    description:
      'Currently the API supports only `password` and `refresh_token` grant types. If a `authorization_code` or `client_credentials` is selected, the API responds with `NotImplemented`.',
  })
  public async getToken(
    @Body()
    tokenRequest: TokenRequestDTO,
  ): Promise<TokenResponse> {
    const [at, rt] = await this.getTokenPair(tokenRequest);
    const workspace = await this.workspaceService.getById(at.workspaceId);
    return new TokenResponse(at, rt, workspace);
  }

  private async getTokenPair(
    tokenRequest: TokenRequestDTO,
  ): Promise<[AccessToken, RefreshToken]> {
    switch (tokenRequest.grant_type) {
      case GrantType.authorizationCode:
      case GrantType.clientCredentials:
        throw new NotImplementedException();
      case GrantType.refreshToken:
        return this.refreshAccessToken(tokenRequest);
      case GrantType.password:
        return this.authorizeUserWithPassword(tokenRequest);
    }
  }

  public async refreshAccessToken({
    refresh_token,
  }: TokenRequestRefreshToken): Promise<[AccessToken, RefreshToken]> {
    try {
      const rt = await this.tokenService.getRefreshToken(refresh_token);
      const at = await this.tokenService.refreshAccessToken(rt);
      return [at, rt];
    } catch (error) {
      throw match(error)
        .whenType(TokenNotFoundError)
        .orWhenType(InvalidTokenError)
        .then(() => new BadRequestException(error.message))
        .getOrDefault(() => error);
    }
  }

  private async authorizeUserWithPassword({
    username,
    password,
    workspace: workspaceName,
    scope,
  }: TokenRequestPassword): Promise<[AccessToken, RefreshToken]> {
    try {
      const workspace = await this.workspaceService.getByName(
        new WorkspaceName(workspaceName),
      );

      const user = await this.authService.loginPassword(
        workspace,
        username,
        password,
      );

      const requestedScope = (scope?.split(' ') ?? []) as Scope[];
      return await this.tokenService.generateTokenPair(user, requestedScope);
    } catch (error) {
      throw match(error)
        .whenType(IllegalWorkspaceNameError)
        .orWhenType(InvalidEmailOrPasswordError)
        .orWhenType(WorkspaceNotFoundError)
        .orWhenType(UserBannedError)
        .then(() => new BadRequestException(error.message))
        .getOrDefault(() => error);
    }
  }
}
