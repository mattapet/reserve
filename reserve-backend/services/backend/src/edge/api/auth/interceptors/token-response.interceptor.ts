/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable, from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { WorkspaceJWTService } from '../services/workspace-jwt.service';
import { TokenResponse } from '../token-response.value';
import { TokenResponseDTO } from '../dto/token-response.dto';
//#endregion

@Injectable()
export class TokenResponseInterceptor
  implements NestInterceptor<TokenResponse, TokenResponseDTO> {
  public constructor(private readonly jwtService: WorkspaceJWTService) {}

  public intercept(
    context: ExecutionContext,
    next: CallHandler<TokenResponse>,
  ): Observable<TokenResponseDTO> {
    return next
      .handle()
      .pipe(mergeMap((payload: TokenResponse) => from(this.encode(payload))));
  }

  private async encode({
    accessToken,
    refreshToken,
    workspace,
  }: TokenResponse): Promise<TokenResponseDTO> {
    return {
      access_token: await this.jwtService.sign(accessToken, workspace),
      expires_in: accessToken.expiry,
      token_type: 'Bearer',
      refresh_token: await this.jwtService.sign(refreshToken, workspace),
      scope: accessToken.scope,
      workspace: workspace.getName().get(),
    };
  }
}
