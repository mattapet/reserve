/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import qs from 'querystring';
import { Response } from 'express';
import {
  NotFoundException,
  Controller,
  Get,
  Param,
  Query,
  Res,
} from '@nestjs/common';
import { ApiTags, ApiResponse, ApiNotFoundResponse } from '@nestjs/swagger';

import { match } from '@rsaas/util';

import {
  AuthorityService,
  AuthorityAuthorizationService,
  Authority,
} from '../../../../user-access';
import { AuthorityNotFoundError } from '../../../../user-access/domain/authority/authority.errors';
import { AuthorizationQuery } from '../../../../user-access/domain/authority/authorization-query.interface';

//#endregion

@Controller('api/v1')
@ApiTags('auth')
export class AuthorizationController {
  public constructor(
    private readonly authorityService: AuthorityService,
    private readonly authorizationService: AuthorityAuthorizationService,
  ) {}

  @Get('login/:authority')
  @ApiResponse({
    status: 301,
    description: 'Redirects user to OAuth2.0 authority authorization url.',
  })
  @ApiNotFoundResponse({
    description:
      'Invalid `authority` identifier passed. Currently the system supports only `silicon_hill` authority.',
  })
  public async authorizeWithAuthority(
    @Param('authority')
    name: string,
    @Query('state')
    state: string,
    @Res()
    res: Response,
  ): Promise<void> {
    try {
      const authority = this.authorityService.getByName(name);
      const query = this.authorizationService.getAuthorizationQuery(
        state,
        authority,
      );

      const authorizationUrl = this.getAuthorizationUrl(authority, query);
      res.redirect(authorizationUrl);
    } catch (error) {
      throw match(error)
        .whenType(AuthorityNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Get('auth/:authority')
  @ApiResponse({
    status: 301,
    description:
      'Redirects user to to the client depending on the redirect uri passed in the `state` parameter.',
  })
  public async redirectAuthorityCallback(
    @Param('authority')
    name: string,
    @Query('code')
    code: string,
    @Query('state')
    state: string,
    @Res()
    res: Response,
  ): Promise<void> {
    const { clientUrl } = JSON.parse(state);
    res.redirect(
      `${clientUrl}/auth/${name}/callback?code=${code}&state=${state}`,
    );
  }

  private getAuthorizationUrl(
    authority: Authority,
    query: AuthorizationQuery,
  ): string {
    return `${authority.getAuthorizationUrl()}${
      authority.getAuthorizationUrl().indexOf('?') < 0 ? '?' : '&'
    }${qs.stringify(query as any)}`;
  }
}
