/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Post,
  Body,
  Req,
  UseInterceptors,
  ClassSerializerInterceptor,
  HttpCode,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { ApiTags, ApiNoContentResponse, ApiBearerAuth } from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';
import { JwtGuard } from '@rsaas/commons/lib/guards/jwt.guard';

import { AuthService, UserService } from '../../../../user-access';

import { SetPasswordDTO } from '../dto/set-password.dto';
//#endregion

@Controller('api/v1')
@UseGuards(JwtGuard)
@ApiTags('auth')
export class PasswordController {
  public constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Post('password')
  @Transactional()
  @HttpCode(HttpStatus.NO_CONTENT)
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @ApiNoContentResponse({ description: 'Password was set' })
  public async setPassword(
    @Req()
    req: WorkspaceAuthorizedRequest,
    @Body()
    { password }: SetPasswordDTO,
  ): Promise<void> {
    const workspaceId = req.user.workspace;
    const userId = req.user.id;
    const user = await this.userService.getById(workspaceId, userId);

    await this.authService.setPassword(user, password);
  }
}
