/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  NotFoundException,
  BadRequestException,
  Post,
  Body,
  UseInterceptors,
  Controller,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiBadRequestResponse,
} from '@nestjs/swagger';

import { match } from '@rsaas/util';
import { Transactional } from '@rsaas/commons/lib/typeorm';

import {
  WorkspaceService,
  WorkspaceNotFoundError,
} from '../../../../workspace-management';
import {
  AuthorityService,
  AuthService,
  TokenService,
  UserBannedError,
} from '../../../../user-access';
import { AuthorityNotFoundError } from '../../../../user-access/domain/authority/authority.errors';

import { AuthorityLoginDTO } from '../dto/authority-login.dto';
import { TokenResponse } from '../token-response.value';
import { TokenResponseInterceptor } from '../interceptors/token-response.interceptor';
import { TokenResponseDTO } from '../dto/token-response.dto';
//#endregion

@Controller('api/v1')
@ApiTags('auth')
export class AuthorityLoginController {
  public constructor(
    private readonly authorityService: AuthorityService,
    private readonly authService: AuthService,
    private readonly workspaceService: WorkspaceService,
    private readonly tokenService: TokenService,
  ) {}

  @Post('login/authority')
  @Transactional()
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(TokenResponseInterceptor)
  @ApiOkResponse({
    type: TokenResponseDTO,
    description:
      'User was successfully logged in with external authority. If its their first interaction with the workspace, their account is created as well.',
  })
  @ApiNotFoundResponse({
    description:
      'Authority referenced with `authority` identifier or workspace does not exist',
  })
  @ApiBadRequestResponse({ description: 'User is banned.' })
  public async loginWithAuthority(
    @Body() { authority: name, code, state }: AuthorityLoginDTO,
  ): Promise<TokenResponse> {
    try {
      const { workspaceId } = JSON.parse(state);
      const workspace = await this.workspaceService.getById(workspaceId);
      const authority = this.authorityService.getByName(name);

      const user = await this.authService.loginAuthority(
        workspace,
        authority,
        code,
      );

      const [at, rt] = await this.tokenService.generateTokenPair(user, []);
      return new TokenResponse(at, rt, workspace);
    } catch (error) {
      throw match(error)
        .whenType(WorkspaceNotFoundError)
        .orWhenType(AuthorityNotFoundError)
        .then(() => new NotFoundException(error.message))
        .whenType(UserBannedError)
        .then(() => new BadRequestException(error.message))
        .getOrDefault(() => error);
    }
  }
}
