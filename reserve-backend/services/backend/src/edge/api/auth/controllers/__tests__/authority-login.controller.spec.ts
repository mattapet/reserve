/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AuthorityLoginController } from '../authority-login.controller';

import { NotFoundException, BadRequestException } from '@nestjs/common';

import {
  WorkspaceService,
  Workspace,
  WorkspaceName,
  WorkspaceOwner,
} from '../../../../../workspace-management';
import {
  Identity,
  UserRole,
  User,
  UserService,
  AuthorityService,
  AuthService,
  TokenService,
  AuthorityAuthorizationService,
} from '../../../../../user-access';

import { UserServiceBuilder } from '../../../../../user-access/__tests__/user/builders/user.service.builder';
import { WorkspaceServiceBuilder } from '../../../../../workspace-management/__tests__/workspace/builders/workspace.service.builder';
import { InMemoryTokenRepository } from '../../../../../user-access/application/authentication/token/repositories/in-memory/in-memory-console-token.repository';
import { AuthorityMockBuilder } from '../../../../../user-access/__tests__/authority/authority-mock.builder';

import { AuthorityLoginDTO } from '../../dto/authority-login.dto';
//#endregion

describe('authority-login.controller', () => {
  let authorityService!: AuthorityService;
  let authService!: AuthService;
  let workspaceService!: WorkspaceService;
  let userService!: UserService;
  let tokenService!: TokenService;
  let controller!: AuthorityLoginController;

  beforeEach(() => {
    authorityService = new AuthorityService();
    userService = UserServiceBuilder.inMemory();
    authService = new AuthService(
      userService,
      new AuthorityAuthorizationService(),
    );
    workspaceService = WorkspaceServiceBuilder.inMemory();
    tokenService = new TokenService(new InMemoryTokenRepository());
    controller = new AuthorityLoginController(
      authorityService,
      authService,
      workspaceService,
      tokenService,
    );
  });

  function make_adminUser(): User {
    return new User(
      '99',
      'test',
      new Identity('99', 'test', 'test@test.com'),
      UserRole.admin,
    );
  }

  async function effect_createWorkspaceWithId(id: string) {
    await workspaceService.create(
      new Workspace(),
      id,
      new WorkspaceName('test'),
      new WorkspaceOwner('99'),
    );
  }

  function make_identityWithEmail(id: string, email: string): Identity {
    return new Identity(id, 'test', email);
  }

  it('should login authorized user', async () => {
    const state = { workspaceId: 'test', clientUrl: 'localhost:3000/cb' };
    await effect_createWorkspaceWithId('test');
    authorityService.register(
      new AuthorityMockBuilder()
        .withName('mocked-authority')
        .mockingFetchAuthorizedUser(() =>
          Promise.resolve({
            email: 'james@example.com',
            firstName: 'James',
            lastName: 'Example',
          }),
        )
        .build(),
    );

    const response = await controller.loginWithAuthority(
      new AuthorityLoginDTO('mocked-authority', '12345', JSON.stringify(state)),
    );

    expect(response.accessToken.isValid()).toBe(true);
    expect(response.refreshToken.isValid()).toBe(true);
  });

  it('should create a new user', async () => {
    const state = { workspaceId: 'test', clientUrl: 'localhost:3000/cb' };
    await effect_createWorkspaceWithId('test');
    authorityService.register(
      new AuthorityMockBuilder()
        .withName('mocked-authority')
        .mockingFetchAuthorizedUser(() =>
          Promise.resolve({
            email: 'james@example.com',
            firstName: 'James',
            lastName: 'Example',
          }),
        )
        .build(),
    );

    await controller.loginWithAuthority(
      new AuthorityLoginDTO('mocked-authority', '12345', JSON.stringify(state)),
    );

    const user = await userService.getByEmail('test', 'james@example.com');
    expect(user.getIdentity().getFirstName()).toBe('James');
    expect(user.getIdentity().getLastName()).toBe('Example');
    expect(user.getEmail()).toBe('james@example.com');
  });

  it('should throw NotFoundException if workspace does not exists', async () => {
    const state = { workspaceId: 'test', clientUrl: 'localhost:3000/cb' };
    authorityService.register(
      new AuthorityMockBuilder()
        .withName('mocked-authority')
        .mockingFetchAuthorizedUser(() =>
          Promise.resolve({
            email: 'james@example.com',
            firstName: 'James',
            lastName: 'Example',
          }),
        )
        .build(),
    );

    const fn = controller.loginWithAuthority(
      new AuthorityLoginDTO('mocked-authority', '12345', JSON.stringify(state)),
    );

    await expect(fn).rejects.toThrow(NotFoundException);
  });

  it('should throw NotFoundException if authority not found', async () => {
    const state = { workspaceId: 'test', clientUrl: 'localhost:3000/cb' };
    await effect_createWorkspaceWithId('test');

    const fn = controller.loginWithAuthority(
      new AuthorityLoginDTO('mocked-authority', '12345', JSON.stringify(state)),
    );

    await expect(fn).rejects.toThrow(NotFoundException);
  });

  it('should throw BadRequestError if user is banned', async () => {
    const state = { workspaceId: 'test', clientUrl: 'localhost:3000/cb' };
    await effect_createWorkspaceWithId('test');
    authorityService.register(
      new AuthorityMockBuilder()
        .withName('mocked-authority')
        .mockingFetchAuthorizedUser(() =>
          Promise.resolve({
            email: 'james@example.com',
            firstName: 'James',
            lastName: 'Example',
          }),
        )
        .build(),
    );
    const user = new User();
    await userService.create(
      user,
      make_identityWithEmail('88', 'james@example.com'),
    );
    await userService.toggleBanned(user, make_adminUser());

    const fn = controller.loginWithAuthority(
      new AuthorityLoginDTO('mocked-authority', '12345', JSON.stringify(state)),
    );

    await expect(fn).rejects.toThrow(BadRequestException);
  });
});
