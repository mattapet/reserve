/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AuthorizationController } from '../authorization.controller';

import qs from 'querystring';
import { NotFoundException } from '@nestjs/common';

import {
  AuthorityService,
  AuthorityAuthorizationService,
} from '../../../../../user-access';
import { AuthorityMockBuilder } from '../../../../../user-access/__tests__/authority/authority-mock.builder';
//#endregion

describe('auth.controller', () => {
  let authorityService: AuthorityService;
  let authorizationService: AuthorityAuthorizationService;
  let controller: AuthorizationController;

  beforeEach(() => {
    authorityService = new AuthorityService();
    authorizationService = new AuthorityAuthorizationService();
    controller = new AuthorizationController(
      authorityService,
      authorizationService,
    );
  });

  describe('authority authorization', () => {
    it('should redirect user to auhority authorization route', async () => {
      const state = { workspaceId: 'test', clientUrl: 'localhost:3000/cb' };
      authorityService.register(
        new AuthorityMockBuilder()
          .withName('mocked-authority')
          .withClientId('mocked-client')
          .withRedirectUri('http://localhost:3000/mocked-authority/cb')
          .withScope('')
          .withAuthorizationUrl(`http://mocked-authority.com/oauth2/authoritze`)
          .build(),
      );
      const res = { redirect: jest.fn() };

      await controller.authorizeWithAuthority(
        'mocked-authority',
        JSON.stringify(state),
        res as any,
      );

      const query = {
        client_id: 'mocked-client',
        redirect_uri: 'http://localhost:3000/mocked-authority/cb',
        scope: '',
        state: JSON.stringify(state),
        response_type: 'code',
      };
      expect(res.redirect).toHaveBeenCalledWith(
        `http://mocked-authority.com/oauth2/authoritze?${qs.stringify(query)}`,
      );
    });

    it('should throw NotFoundError if invalid authority provided', async () => {
      const state = { workspaceId: 'test', clientUrl: 'localhost:3000/cb' };
      const res = { redirect: jest.fn() };

      const fn = controller.authorizeWithAuthority(
        'mocked-authority',
        JSON.stringify(state),
        res as any,
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('authority authorization redirect', () => {
    it('should redirect authorization callback to the client', async () => {
      const state = { workspaceId: 'test', clientUrl: 'localhost:3000/cb' };
      const code = '12345';
      const authority = 'mocked-authority';
      const res = { redirect: jest.fn() };

      await controller.redirectAuthorityCallback(
        authority,
        code,
        JSON.stringify(state),
        res as any,
      );

      expect(res.redirect).toHaveBeenCalledWith(
        `localhost:3000/cb/auth/${authority}/callback?code=${code}&state=${JSON.stringify(
          state,
        )}`,
      );
    });
  });
});
