/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { PasswordController } from '../password.controller';

import {
  UserRole,
  UserService,
  Identity,
  User,
  AuthService,
  AuthorityAuthorizationService,
} from '../../../../../user-access';
import { Workspace, WorkspaceName } from '../../../../../workspace-management';
import { UserServiceBuilder } from '../../../../../user-access/__tests__/user/builders/user.service.builder';

import { SetPasswordDTO } from '../../dto/set-password.dto';
//#endregion

describe('password-controller', () => {
  let userService: UserService;
  let authService: AuthService;
  let controller: PasswordController;

  beforeEach(() => {
    userService = UserServiceBuilder.inMemory();
    authService = new AuthService(
      userService,
      new AuthorityAuthorizationService(),
    );
    controller = new PasswordController(authService, userService);
  });

  it('should set users password', async () => {
    jest.spyOn(Date, 'now').mockImplementation(() => 200);
    const identity = new Identity('99', 'test', 'test@test.com');
    await userService.createOwner(new User(), identity);
    const req = {
      user: {
        id: '99',
        workspace: 'test',
        role: UserRole.admin,
        isOwner: true,
      },
    };

    await controller.setPassword(req as any, new SetPasswordDTO('12345'));

    const workspace = new Workspace('test', new WorkspaceName('test'));
    const user = await authService.loginPassword(
      workspace,
      'test@test.com',
      '12345',
    );
    expect(user.getLastLogin()).toEqual(new Date(200));
  });
});
