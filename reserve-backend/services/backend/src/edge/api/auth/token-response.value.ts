/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AccessToken, RefreshToken } from '../../../user-access';
import { Workspace } from '../../../workspace-management';
//#endregion

export class TokenResponse {
  public constructor(
    public readonly accessToken: AccessToken,
    public readonly refreshToken: RefreshToken,
    public readonly workspace: Workspace,
  ) {}
}
