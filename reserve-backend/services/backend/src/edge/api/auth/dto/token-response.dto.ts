/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';
import { Scope } from '@rsaas/util/lib/oauth2/scope.type';
//#endregion

export class TokenResponseDTO {
  @ApiProperty()
  readonly access_token!: string;

  @ApiProperty()
  readonly expires_in!: number;

  @ApiProperty()
  readonly token_type!: 'Bearer';

  @ApiProperty()
  readonly refresh_token!: string;

  @ApiProperty({ enum: Scope })
  readonly scope!: Scope[];

  @ApiProperty()
  readonly workspace!: string;
}
