/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IsString, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class AuthorityLoginDTO {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public readonly authority: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public readonly code: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  public readonly state: string;

  public constructor(authority: string, code: string, state: string) {
    this.authority = authority;
    this.code = code;
    this.state = state;
  }
}
