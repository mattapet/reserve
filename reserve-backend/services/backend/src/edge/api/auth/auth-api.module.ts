/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { registerJWTServiceModule } from '@rsaas/commons/lib/jwt-service.module';

import { UserAccessModule } from '../../../user-access';
import { WorkspaceManagementModule } from '../../../workspace-management';

import { AuthorityLoginController } from './controllers/authority-login.controller';
import { AuthorizationController } from './controllers/authorization.controller';
import { PasswordController } from './controllers/password.controller';

import { WorkspaceJWTService } from './services/workspace-jwt.service';
import { JwtModule } from '../jwt';
//#endregion

@Module({
  imports: [
    JwtModule,
    registerJWTServiceModule(),
    UserAccessModule,
    WorkspaceManagementModule,
  ],
  controllers: [
    AuthorityLoginController,
    AuthorizationController,
    PasswordController,
  ],
  providers: [WorkspaceJWTService],
})
export class AuthApiModule {}
