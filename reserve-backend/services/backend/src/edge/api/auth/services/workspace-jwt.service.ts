/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, BadRequestException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import {
  TokenKind,
  WorkspaceJwtPayload,
} from '@rsaas/commons/lib/interface/jwt-payload.interface';

import { Token } from '../../../../user-access/domain/authentication/token/token.repository';
import { UserService, AccessToken } from '../../../../user-access';
import { Workspace } from '../../../../workspace-management';
//#endregion

@Injectable()
export class WorkspaceJWTService {
  public constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) {}

  public async verify(token: string): Promise<WorkspaceJwtPayload> {
    try {
      return this.jwtService.verify<WorkspaceJwtPayload>(token);
    } catch (error) {
      throw new BadRequestException('Invalid refresh token');
    }
  }

  public async sign(token: Token, workspace: Workspace): Promise<string> {
    const user = await this.userService.getById(
      token.workspaceId,
      token.userId,
    );
    return this.jwtService.signAsync({
      jit: token.value,
      sub: token.userId,
      exp: this.getExpirationUnixTimestamp(token),
      iat: this.getIssuedAtUnixTimestamp(token),
      scope: [],
      kind: this.getTokenKind(token),
      user: {
        id: token.userId,
        workspace: workspace.getName().get(),
        role: user.getRole(),
        isOwner: user.isOwner(),
      },
    });
  }

  private getTokenKind(token: Token): TokenKind {
    return token instanceof AccessToken
      ? TokenKind.accessToken
      : TokenKind.refreshToken;
  }

  private getExpirationUnixTimestamp(token: Token): number {
    return this.getIssuedAtUnixTimestamp(token) + token.expiry;
  }

  private getIssuedAtUnixTimestamp(token: Token): number {
    return Math.floor(token.created.valueOf() / 1000);
  }
}
