/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from '@rsaas/util/lib/user-role.type';
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class ProfileDTO {
  @ApiProperty()
  public readonly first_name!: string;

  @ApiProperty()
  public readonly last_name!: string;

  @ApiProperty({ required: false })
  public readonly phone?: string;
}

export class UserDTO {
  @ApiProperty()
  public readonly id!: string;

  @ApiProperty()
  public readonly role!: UserRole;

  @ApiProperty()
  public readonly workspace!: string;

  @ApiProperty()
  public readonly is_owner!: boolean;

  @ApiProperty({ required: false })
  public readonly last_login?: string;

  @ApiProperty()
  public readonly email!: string;

  @ApiProperty({ type: ProfileDTO })
  public readonly profile!: ProfileDTO;

  @ApiProperty()
  public readonly banned!: boolean;
}
