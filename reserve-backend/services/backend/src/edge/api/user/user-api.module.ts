/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { UserAccessModule } from '../../../user-access';
import { WorkspaceManagementModule } from '../../../workspace-management';
import { UserController } from './user.controller';
//#endregion

@Module({
  imports: [UserAccessModule, WorkspaceManagementModule],
  controllers: [UserController],
})
export class UserApiModule {}
