/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserController } from '../user.controller';

import {
  NotFoundException,
  UnauthorizedException,
  ForbiddenException,
} from '@nestjs/common';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';

import { User, Identity, UserService, UserRole } from '../../../../user-access';
import { UserServiceBuilder } from '../../../../user-access/__tests__/user/builders/user.service.builder';
import { WorkspaceServiceBuilder } from '../../../../workspace-management/__tests__/workspace/builders/workspace.service.builder';
import {
  Workspace,
  WorkspaceName,
  WorkspaceOwner,
  WorkspaceService,
} from '../../../../workspace-management';
import { ProfileDTO } from '../dto/profile.dto';
//#endregion

describe('user.controller', () => {
  let userService!: UserService;
  let workspaceService!: WorkspaceService;
  let controller!: UserController;

  beforeEach(() => {
    userService = UserServiceBuilder.inMemory();
    workspaceService = WorkspaceServiceBuilder.inMemory();
    controller = new UserController(userService, workspaceService);
  });

  async function effect_createTestWorkspaceWithOwnerId(ownerId: string) {
    await workspaceService.create(
      new Workspace(),
      'test',
      new WorkspaceName('test'),
      new WorkspaceOwner(ownerId),
    );
  }

  function make_userWithIdentity(identity: Identity): User {
    const user = new User();
    user.setIdentity(identity);
    return user;
  }

  function make_ownerWithIdentity(identity: Identity): User {
    const user = new User();
    user.setIdentity(identity);
    user.claimOwnership();
    return user;
  }

  function make_authorizedRequestWithUserId(
    userId: string,
  ): WorkspaceAuthorizedRequest {
    return { user: { id: userId, workspace: 'test' } } as any;
  }

  describe('retrieving users', () => {
    it('should just a single workspace user -- its owner', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );

      const result = await controller.getAll(
        make_authorizedRequestWithUserId('99'),
      );

      expect(result).toEqual([
        make_ownerWithIdentity(new Identity('99', 'test', 'test@test.com')),
      ]);
    });

    it('should two workspace users', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );
      await userService.create(
        new User(),
        new Identity('88', 'test', 'test2@test.com'),
      );

      const result = await controller.getAll(
        make_authorizedRequestWithUserId('99'),
      );

      expect(result.length).toBe(2);
      expect(result).toEqual(
        expect.arrayContaining([
          make_ownerWithIdentity(new Identity('99', 'test', 'test@test.com')),
          make_userWithIdentity(new Identity('88', 'test', 'test2@test.com')),
        ]),
      );
    });

    it('should return logged in user', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );

      const result = await controller.getMe(
        make_authorizedRequestWithUserId('99'),
      );

      expect(result).toEqual(
        make_ownerWithIdentity(new Identity('99', 'test', 'test@test.com')),
      );
    });

    it('should retrieve user by its id', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );
      await userService.create(
        new User(),
        new Identity('88', 'test', 'test2@test.com'),
      );

      const result = await controller.getById(
        make_authorizedRequestWithUserId('99'),
        '88',
      );

      expect(result).toEqual(
        make_userWithIdentity(new Identity('88', 'test', 'test2@test.com')),
      );
    });

    it('should throw NotFoundException if user does not exist', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );

      const fn = controller.getById(
        make_authorizedRequestWithUserId('99'),
        'some-random-id',
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('updating user profile', () => {
    it('should update user profile', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );

      const result = await controller.updateProfile(
        make_authorizedRequestWithUserId('99'),
        new ProfileDTO('James', 'Example'),
      );

      expect(result).toEqual(
        make_ownerWithIdentity(
          new Identity('99', 'test', 'test@test.com', 'James', 'Example'),
        ),
      );
    });
  });

  describe('updating user role', () => {
    it('should make user admin', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );
      await userService.create(
        new User(),
        new Identity('88', 'test', 'test2@test.com'),
      );

      const result = await controller.updateRole(
        make_authorizedRequestWithUserId('99'),
        '88',
        UserRole.admin,
      );

      expect(result.getRole()).toBe(UserRole.admin);
    });

    it('should throw UnauthorizedException when a user is trying to demote admin', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );
      await userService.create(
        new User(),
        new Identity('88', 'test', 'test2@test.com'),
      );

      const fn = controller.updateRole(
        make_authorizedRequestWithUserId('88'),
        '99',
        UserRole.maintainer,
      );

      await expect(fn).rejects.toThrow(UnauthorizedException);
    });

    it('should throw ForbiddenException when a user is trying to change their own role', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );

      const fn = controller.updateRole(
        make_authorizedRequestWithUserId('99'),
        '99',
        UserRole.maintainer,
      );

      await expect(fn).rejects.toThrow(ForbiddenException);
    });

    it('should throw NotFoundException referenced user does not exist', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );

      const fn = controller.updateRole(
        make_authorizedRequestWithUserId('99'),
        'some-other-user',
        UserRole.maintainer,
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('banning users', () => {
    it('should place ban on user', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );
      await userService.create(
        new User(),
        new Identity('88', 'test', 'test2@test.com'),
      );

      const result = await controller.toggleBan(
        make_authorizedRequestWithUserId('99'),
        '88',
      );

      expect(result.isBanned()).toBe(true);
    });

    it('should lift ban from user', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );
      await userService.create(
        new User(),
        new Identity('88', 'test', 'test2@test.com'),
      );

      await controller.toggleBan(make_authorizedRequestWithUserId('99'), '88');
      const result = await controller.toggleBan(
        make_authorizedRequestWithUserId('99'),
        '88',
      );

      expect(result.isBanned()).toBe(false);
    });

    it('should throw UnauthorizedException when a user is trying to ban an admin', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );
      await userService.create(
        new User(),
        new Identity('88', 'test', 'test2@test.com'),
      );

      const fn = controller.toggleBan(
        make_authorizedRequestWithUserId('88'),
        '99',
      );

      await expect(fn).rejects.toThrow(UnauthorizedException);
    });

    it('should throw ForbiddenException when a user is trying to ban themselves', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );

      const fn = controller.toggleBan(
        make_authorizedRequestWithUserId('99'),
        '99',
      );

      await expect(fn).rejects.toThrow(ForbiddenException);
    });

    it('should throw NotFoundException referenced user does not exist', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );

      const fn = controller.toggleBan(
        make_authorizedRequestWithUserId('99'),
        'some-other-user',
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('deleting users', () => {
    it('should allow user to delete themselves', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );
      await userService.create(
        new User(),
        new Identity('88', 'test', 'test2@test.com'),
      );

      await controller.deleteUser(make_authorizedRequestWithUserId('88'));

      const fn = controller.getById(
        make_authorizedRequestWithUserId('99'),
        '88',
      );
      await expect(fn).rejects.toThrow(NotFoundException);
    });

    it('should throw ForbiddenException when the owner of the workspace is trying to delete themselves', async () => {
      await effect_createTestWorkspaceWithOwnerId('99');
      await userService.createOwner(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );

      const fn = controller.deleteUser(make_authorizedRequestWithUserId('99'));

      await expect(fn).rejects.toThrow(ForbiddenException);
    });
  });
});
