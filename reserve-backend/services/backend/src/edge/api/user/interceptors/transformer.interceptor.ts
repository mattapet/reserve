/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../../../user-access';
import { UserDTO } from '../dto/user.dto';
//#endregion

@Injectable()
export class TransformerInterceptor
  implements NestInterceptor<User | User[], UserDTO | UserDTO[]> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<User | User[]>,
  ): Observable<UserDTO | UserDTO[]> {
    return call$
      .handle()
      .pipe(
        map((payload: User | User[]) =>
          Array.isArray(payload)
            ? payload.map(this.encode)
            : this.encode(payload),
        ),
      );
  }

  private encode = (user: User) => ({
    id: user.getId(),
    workspace: user.getWorkspaceId(),
    role: user.getRole(),
    email: user.getEmail(),
    is_owner: user.isOwner(),
    profile: {
      first_name: user.getIdentity().getFirstName(),
      last_name: user.getIdentity().getLastName(),
      phone: user.getIdentity().getPhone(),
    },
    banned: user.isBanned(),
    last_login: user.getLastLogin()?.toISOString(),
  });
}
