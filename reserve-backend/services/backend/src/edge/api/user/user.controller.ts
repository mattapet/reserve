/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Get,
  UseGuards,
  Req,
  Param,
  UsePipes,
  UseInterceptors,
  Patch,
  Body,
  Put,
  ClassSerializerInterceptor,
  Delete,
  UnauthorizedException,
  ForbiddenException,
  NotFoundException,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiNoContentResponse,
  ApiForbiddenResponse,
} from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { match } from '@rsaas/util';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';
import { JwtGuard } from '@rsaas/commons/lib/guards/jwt.guard';
import { RoleGuard } from '@rsaas/commons/lib/guards/role.guard';
import { UserRole } from '@rsaas/util/lib/user-role.type';

import { User, UserService } from '../../../user-access';
import {
  IllegalOperationError,
  InsufficientRightsError,
  UserNotFoundError,
} from '../../../user-access/domain/user/user.errors';
import { WorkspaceService } from '../../../workspace-management';

import { TransformerInterceptor } from './interceptors/transformer.interceptor';
import { GetUserValidation } from './validation/id.validation';
import { ProfileDTO } from './dto/profile.dto';
import { UserDTO } from './dto/user.dto';
//#endregion

@Controller('api/v1/user')
@UseGuards(JwtGuard)
@ApiTags('user')
@ApiBearerAuth()
export class UserController {
  public constructor(
    private readonly service: UserService,
    private readonly workspace: WorkspaceService,
  ) {}

  @Get()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({ type: [UserDTO] })
  public async getAll(@Req() req: WorkspaceAuthorizedRequest): Promise<User[]> {
    const workspace = await this.workspace.getById(req.user.workspace);
    return this.service.getAll(workspace.getId());
  }

  @Get('me')
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({
    type: UserDTO,
    description: 'Returns currently logged in user.',
  })
  public async getMe(@Req() req: WorkspaceAuthorizedRequest): Promise<User> {
    const workspace = await this.workspace.getById(req.user.workspace);
    return this.service.getById(workspace.getId(), req.user.id);
  }

  @Put('me/profile')
  @Transactional()
  @UseInterceptors(ClassSerializerInterceptor, TransformerInterceptor)
  @ApiOkResponse({ type: UserDTO })
  public async updateProfile(
    @Req() req: WorkspaceAuthorizedRequest,
    @Body() { first_name: firstName, last_name: lastName, phone }: ProfileDTO,
  ): Promise<User> {
    const user = await this.service.getById(req.user.workspace, req.user.id);
    const identity = user.getIdentity();
    identity.setFirstName(firstName);
    identity.setLastName(lastName);
    identity.setPhone(phone);
    await this.service.updateProfile(user, identity);
    return user;
  }

  @Get(':id')
  @Transactional()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UsePipes(GetUserValidation)
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({ type: UserDTO })
  @ApiNotFoundResponse({
    description: 'User with the given `id` does not exist.',
  })
  public async getById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') userId: string,
  ): Promise<User> {
    try {
      const workspace = await this.workspace.getById(req.user.workspace);
      return await this.service.getById(workspace.getId(), userId);
    } catch (error) {
      throw match(error)
        .whenType(UserNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Patch(':id/role')
  @Transactional()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UsePipes(GetUserValidation)
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({ type: UserDTO })
  @ApiNotFoundResponse({
    description: 'User with the given `id` does not exist.',
  })
  public async updateRole(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') userId: string,
    @Body('role') role: UserRole,
  ): Promise<User> {
    const workspaceId = req.user.workspace;

    try {
      const user = await this.service.getById(workspaceId, userId);
      const changedBy = await this.service.getById(workspaceId, req.user.id);

      await this.service.updateUserRole(user, role, changedBy);

      return user;
    } catch (error) {
      throw match(error)
        .whenType(InsufficientRightsError)
        .then(() => new UnauthorizedException(error.message))
        .whenType(IllegalOperationError)
        .then(() => new ForbiddenException(error.message))
        .whenType(UserNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Patch(':id/ban')
  @Transactional()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UsePipes(GetUserValidation)
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({ type: UserDTO })
  @ApiNotFoundResponse({
    description: 'User with the given `id` does not exist.',
  })
  public async toggleBan(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') userId: string,
  ): Promise<User> {
    const workspaceId = req.user.workspace;

    try {
      const user = await this.service.getById(workspaceId, userId);
      const toggledBy = await this.service.getById(workspaceId, req.user.id);

      await this.service.toggleBanned(user, toggledBy);

      return user;
    } catch (error) {
      throw match(error)
        .whenType(InsufficientRightsError)
        .then(() => new UnauthorizedException(error.message))
        .whenType(IllegalOperationError)
        .then(() => new ForbiddenException(error.message))
        .whenType(UserNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Delete('me')
  @Transactional()
  @ApiNoContentResponse({
    // tslint:disable-next-line:quotemark
    description: "User's account have been successfully deleted.",
  })
  @ApiForbiddenResponse({
    description: 'Workspace owner cannot delete themselves.',
  })
  public async deleteUser(
    @Req() req: WorkspaceAuthorizedRequest,
  ): Promise<void> {
    try {
      const user = await this.service.getById(req.user.workspace, req.user.id);
      await this.service.delete(user);
    } catch (error) {
      throw match(error)
        .whenType(IllegalOperationError)
        .then(() => new ForbiddenException(error.message))
        .getOrDefault(() => error);
    }
  }
}
