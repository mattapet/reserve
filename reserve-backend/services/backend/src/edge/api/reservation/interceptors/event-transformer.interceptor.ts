/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ReservationEventDTO } from '../dto/reservation-event.dto';
import { ReservationEvent } from '../../../../resource-reservation/domain/reservation';
//#endregion

@Injectable()
export class EventTransformerInterceptor
  implements NestInterceptor<ReservationEvent[], ReservationEventDTO[]> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<ReservationEvent[]>,
  ): Observable<ReservationEventDTO[]> {
    return call$
      .handle()
      .pipe(map((payload: ReservationEvent[]) => payload.map(this.encode)));
  }

  private encode = (event: ReservationEvent): ReservationEventDTO => ({
    type: event.type,
    reservation_id: event.aggregateId,
    timestamp: event.timestamp.toISOString(),
    payload: event.payload,
  });
}
