/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Reservation } from '../../../../resource-reservation/domain/reservation';
import { ReservationProjection } from '../../../../resource-reservation/domain/reservation-projection';
import { ReservationDTO } from '../dto/reservation.dto';
//#endregion

type ReservationType = Reservation | ReservationProjection;

@Injectable()
export class TransformerInterceptor
  implements
    NestInterceptor<
      Reservation | Reservation[],
      ReservationDTO | ReservationDTO[]
    > {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<ReservationType | ReservationType[]>,
  ): Observable<ReservationDTO | ReservationDTO[]> {
    return call$
      .handle()
      .pipe(
        map((payload: ReservationType | ReservationType[]) =>
          Array.isArray(payload)
            ? payload.map(this.encode)
            : this.encode(payload),
        ),
      );
  }

  private encode = (reservation: ReservationType): ReservationDTO =>
    reservation instanceof Reservation
      ? {
          id: reservation.getId(),
          date_start: reservation.getDateStart().toISOString(),
          date_end: reservation.getDateEnd().toISOString(),
          resource_ids: reservation.getResourceIds(),
          state: reservation.getState(),
          user_id: reservation.getAssignee().getId(),
          created_at: reservation.getCreatedAt().toISOString(),
          notes: reservation.getNotes(),
        }
      : {
          id: reservation.id,
          date_start: reservation.dateStart.toISOString(),
          date_end: reservation.dateEnd.toISOString(),
          resource_ids: reservation.resourceIds,
          state: reservation.state,
          user_id: reservation.userId,
          created_at: reservation.createdAt.toISOString(),
          notes: reservation.notes,
        };
}
