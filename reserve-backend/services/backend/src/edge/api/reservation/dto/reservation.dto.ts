/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';

import { ReservationState } from '../../../../resource-reservation/domain/reservation/reservation-state.type';
import { ResourceId } from '../../../../resource-reservation/domain/resource/resource.entity';
//#endregion

export class ReservationDTO {
  @ApiProperty()
  public readonly id!: string;

  @ApiProperty()
  public readonly date_start!: string;

  @ApiProperty()
  public readonly date_end!: string;

  @ApiProperty()
  public readonly user_id!: string;

  @ApiProperty({ enum: ReservationState })
  public readonly state!: ReservationState;

  @ApiProperty({ type: [String] })
  public readonly resource_ids!: ResourceId[];

  @ApiProperty()
  public readonly created_at!: string;

  @ApiProperty({ required: false })
  public readonly notes?: string;
}
