/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  IsDateString,
  IsUUID,
  IsArray,
  IsOptional,
  IsString,
  IsNotEmpty,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { ResourceId } from '../../../../resource-reservation/domain/resource/resource.entity';
//#endregion

export class CreateReservationDTO {
  @IsDateString()
  @ApiProperty()
  public readonly date_start!: string;

  @IsDateString()
  @ApiProperty()
  public readonly date_end!: string;

  @IsArray()
  @IsString({ each: true })
  @IsNotEmpty()
  @ApiProperty({ type: [String], minLength: 1 })
  public readonly resource_ids!: ResourceId[];

  @IsUUID()
  @IsOptional()
  @ApiProperty({ required: false })
  public readonly user_id?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  public readonly notes?: string;
}
