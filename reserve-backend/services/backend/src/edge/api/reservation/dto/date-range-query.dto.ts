/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IsDateString, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class DateRangeQuery {
  @IsDateString()
  @IsOptional()
  @ApiProperty({ required: false })
  public readonly date_start?: string;

  @IsDateString()
  @IsOptional()
  @ApiProperty({ required: false })
  public readonly date_end?: string;
}
