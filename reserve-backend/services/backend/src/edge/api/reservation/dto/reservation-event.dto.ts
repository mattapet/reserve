/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EventDTO } from '@rsaas/commons/lib/event.dto';
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class ReservationEventDTO extends EventDTO {
  @ApiProperty()
  readonly reservation_id!: string;
}
