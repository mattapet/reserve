/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationCommentController } from '../reservation-comment.controller';

import { NotFoundException, UnauthorizedException } from '@nestjs/common';

import { UserServiceBuilder } from '../../../../../user-access/__tests__/user/builders/user.service.builder';
import { ReservationCommentServiceBuilder } from '../../../../../resource-reservation/__tests__/reservation-comment/builders/reservation-comment.service.builder';
import { ReservationServiceBuilder } from '../../../../../resource-reservation/__tests__/reservation/builders/reservation.service.builder';
import {
  UserService,
  Identity,
  User,
  UserRole,
} from '../../../../../user-access';
import {
  ReservationService,
  Reservation,
  Assignee,
  ReservationManager,
  CreateReservation,
  ReservationComment,
  CommentAuthor,
  CommentAuthorId,
} from '../../../../../resource-reservation';
//#endregion

describe('reservation-comment.controller', () => {
  let userService!: UserService;
  let reservationService!: ReservationService;
  let controller!: ReservationCommentController;

  beforeEach(() => {
    userService = UserServiceBuilder.inMemory();
    reservationService = ReservationServiceBuilder.inMemory();
    controller = new ReservationCommentController(
      ReservationCommentServiceBuilder.inMemory(),
      reservationService,
      userService,
    );
  });

  function mock_dateNow(now: number) {
    jest.spyOn(Date, 'now').mockImplementation(() => now);
  }

  async function effect_createOwnerWithId(id: string) {
    const owner = new User();
    await userService.createOwner(
      owner,
      new Identity(id, 'test', 'test@test.com'),
    );
    return owner;
  }

  async function effect_createAdminWithId(id: string, owner: User) {
    const user = new User();
    await userService.create(user, new Identity(id, 'test', 'admin@test.com'));
    await userService.updateUserRole(user, UserRole.admin, owner);
    return user;
  }

  async function effect_createReservationWithId(
    id: string,
    assigneeId: string,
    createdById: string,
  ) {
    await reservationService.create(
      new Reservation(),
      new CreateReservation(
        id,
        'test',
        new Date(100),
        new Date(200),
        ['1'],
        '',
        new Assignee(assigneeId),
        new ReservationManager(createdById, UserRole.admin),
      ),
    );
  }

  function make_authorizedRequestWithUserId(id: string) {
    return { user: { id, workspace: 'test' } } as any;
  }

  describe('comment querying', () => {
    it('should return an empty array if reservation does not have any comments', async () => {
      mock_dateNow(0);
      await effect_createOwnerWithId('99');
      await effect_createReservationWithId('44', '99', '99');

      const result = await controller.getReservationComments(
        make_authorizedRequestWithUserId('99'),
        '44',
      );

      expect(result).toEqual([]);
    });

    it('should return a single created comment', async () => {
      mock_dateNow(0);
      await effect_createOwnerWithId('99');
      await effect_createReservationWithId('44', '99', '99');
      await controller.create(make_authorizedRequestWithUserId('99'), '44', {
        comment: 'some comment',
      });

      const result = await controller.getReservationComments(
        make_authorizedRequestWithUserId('99'),
        '44',
      );

      expect(result).toEqual([
        new ReservationComment(
          expect.any(String),
          'test',
          '44',
          'some comment',
          new Date(0),
          new CommentAuthor(new CommentAuthorId('99')),
        ),
      ]);
    });

    it('should throw NotFoundException if reservation does not exist', async () => {
      const fn = controller.getReservationComments(
        { user: { id: '99' } } as any,
        '44',
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('creating comments', () => {
    it('should create a reservation comment', async () => {
      mock_dateNow(0);
      await effect_createOwnerWithId('99');
      await effect_createReservationWithId('44', '99', '99');

      const result = await controller.create(
        make_authorizedRequestWithUserId('99'),
        '44',
        { comment: 'some comment' },
      );

      expect(result).toEqual(
        new ReservationComment(
          expect.any(String),
          'test',
          '44',
          'some comment',
          new Date(0),
          new CommentAuthor(new CommentAuthorId('99')),
        ),
      );
    });

    it('should throw NotFoundException if reservation does not exist', async () => {
      mock_dateNow(0);
      await effect_createOwnerWithId('99');

      const fn = controller.create(
        make_authorizedRequestWithUserId('99'),
        '44',
        { comment: 'some comment' },
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('updating comments', () => {
    it('should update a reservation comment', async () => {
      mock_dateNow(0);
      await effect_createOwnerWithId('99');
      await effect_createReservationWithId('44', '99', '99');
      const comment = await controller.create(
        make_authorizedRequestWithUserId('99'),
        '44',
        { comment: 'some comment' },
      );

      const result = await controller.edit(
        make_authorizedRequestWithUserId('99'),
        '44',
        comment.getId(),
        { comment: 'some other comment' },
      );

      expect(result).toEqual(
        new ReservationComment(
          expect.any(String),
          'test',
          '44',
          'some other comment',
          new Date(0),
          new CommentAuthor(new CommentAuthorId('99')),
        ),
      );
    });

    it('should throw NotFoundException if reservation does not exist', async () => {
      mock_dateNow(0);
      await effect_createOwnerWithId('99');

      const fn = controller.edit(
        make_authorizedRequestWithUserId('99'),
        '44',
        '55',
        { comment: 'some comment' },
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });

    it('should throw NotFoundException if comment does not exist', async () => {
      mock_dateNow(0);
      await effect_createOwnerWithId('99');
      await effect_createReservationWithId('44', '99', '99');
      await controller.create(make_authorizedRequestWithUserId('99'), '44', {
        comment: 'some comment',
      });

      const fn = controller.edit(
        make_authorizedRequestWithUserId('99'),
        '44',
        '55',
        { comment: 'some comment' },
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });

    it('should throw UnauthorizedException if non-author tries to edit comment', async () => {
      mock_dateNow(0);
      const owner = await effect_createOwnerWithId('99');
      await effect_createAdminWithId('88', owner);
      await effect_createReservationWithId('44', '99', '99');
      const comment = await controller.create(
        make_authorizedRequestWithUserId('99'),
        '44',
        { comment: 'some comment' },
      );

      const fn = controller.edit(
        make_authorizedRequestWithUserId('88'),
        '44',
        comment.getId(),
        { comment: 'some comment' },
      );

      await expect(fn).rejects.toThrow(UnauthorizedException);
    });
  });

  describe('deleting comments', () => {
    it('should delete a reservation comment', async () => {
      mock_dateNow(0);
      await effect_createOwnerWithId('99');
      await effect_createReservationWithId('44', '99', '99');
      const comment = await controller.create(
        make_authorizedRequestWithUserId('99'),
        '44',
        { comment: 'some comment' },
      );

      await controller.delete(
        make_authorizedRequestWithUserId('99'),
        '44',
        comment.getId(),
      );

      const result = await controller.getReservationComments(
        make_authorizedRequestWithUserId('99'),
        '44',
      );
      expect(result).toEqual([]);
    });

    it('should throw NotFoundException if reservation does not exist', async () => {
      mock_dateNow(0);
      await effect_createOwnerWithId('99');

      const fn = controller.delete(
        make_authorizedRequestWithUserId('99'),
        '44',
        '55',
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });

    it('should throw NotFoundException if comment does not exist', async () => {
      mock_dateNow(0);
      await effect_createOwnerWithId('99');
      await effect_createReservationWithId('44', '99', '99');
      await controller.create(make_authorizedRequestWithUserId('99'), '44', {
        comment: 'some comment',
      });

      const fn = controller.delete(
        make_authorizedRequestWithUserId('99'),
        '44',
        '55',
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });

    it('should throw UnauthorizedException if non-author tries to edit comment', async () => {
      mock_dateNow(0);
      const owner = await effect_createOwnerWithId('99');
      await effect_createAdminWithId('88', owner);
      await effect_createReservationWithId('44', '99', '99');
      const comment = await controller.create(
        make_authorizedRequestWithUserId('99'),
        '44',
        { comment: 'some comment' },
      );

      const fn = controller.delete(
        make_authorizedRequestWithUserId('88'),
        '44',
        comment.getId(),
      );

      await expect(fn).rejects.toThrow(UnauthorizedException);
    });
  });
});
