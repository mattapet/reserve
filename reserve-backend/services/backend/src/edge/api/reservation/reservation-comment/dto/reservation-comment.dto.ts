/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class ReservationCommentDTO {
  @ApiProperty()
  public readonly id: string;

  @ApiProperty()
  public readonly reservation_id: string;

  @ApiProperty()
  public readonly timestamp: string;

  @ApiProperty()
  public readonly author: string;

  @ApiProperty()
  public readonly comment: string;

  @ApiProperty()
  public readonly deleted: boolean;

  public constructor(
    id: string,
    reservation_id: string,
    timestamp: string,
    author: string,
    comment: string,
    deleted: boolean,
  ) {
    this.id = id;
    this.reservation_id = reservation_id;
    this.timestamp = timestamp;
    this.author = author;
    this.comment = comment;
    this.deleted = deleted;
  }
}
