/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Get,
  UseGuards,
  Req,
  Param,
  Post,
  UseInterceptors,
  ClassSerializerInterceptor,
  Body,
  Put,
  Delete,
  NotFoundException,
  UnauthorizedException,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
  ApiUnauthorizedResponse,
  ApiNoContentResponse,
} from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { uuid, match } from '@rsaas/util';
import { JwtGuard } from '@rsaas/commons/lib/guards/jwt.guard';
import { RoleGuard } from '@rsaas/commons/lib/guards/role.guard';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';

import {
  ReservationCommentService,
  InsufficientRightsError,
  ReservationCommentNotFoundError,
} from '../../../../resource-reservation/domain/reservation-comment';
import { ReservationComment } from '../../../../resource-reservation/domain/reservation-comment/reservation-comment.entity';
import { CreateReservationComment } from '../../../../resource-reservation/domain/reservation-comment/values';
import { CommentAuthor } from '../../../../resource-reservation/domain/reservation-comment/comment-author.entity';
import { ReservationService } from '../../../../resource-reservation';
import { ReservationNotFoundError } from '../../../../resource-reservation/domain/reservation/reservation.errors';
import { UserService, UserRole } from '../../../../user-access';

import { CreateCommentDTO } from './dto/create-comment.dto';
import { TransformerInterceptor } from './interceptors/transformer.interceptor';
import { ReservationCommentDTO } from './dto/reservation-comment.dto';
//#endregion

@Controller('api/v1/reservation')
@UseGuards(JwtGuard)
@ApiTags('reservation', 'reservation-comment')
@ApiBearerAuth()
export class ReservationCommentController {
  public constructor(
    private readonly service: ReservationCommentService,
    private readonly reservationService: ReservationService,
    private readonly userService: UserService,
  ) {}

  @Get(':reservation_id/comment')
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({ type: [ReservationCommentDTO] })
  @ApiNotFoundResponse({
    description: 'Reservation with the given `reservation_id` does not exist.',
  })
  public async getReservationComments(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('reservation_id') reservationId: string,
  ): Promise<ReservationComment[]> {
    try {
      const workspaceId = req.user.workspace;
      const reservation = await this.reservationService.getById(
        workspaceId,
        reservationId,
      );

      return await this.service.getByReservation(reservation);
    } catch (error) {
      throw match(error)
        .whenType(ReservationNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Post(':reservation_id/comment')
  @Transactional()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UseInterceptors(ClassSerializerInterceptor, TransformerInterceptor)
  @ApiCreatedResponse({ type: ReservationCommentDTO })
  @ApiNotFoundResponse({
    description: 'Reservation with the given `reservation_id` does not exist.',
  })
  public async create(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('reservation_id') reservationId: string,
    @Body() { comment }: CreateCommentDTO,
  ): Promise<ReservationComment> {
    try {
      const workspaceId = req.user.workspace;
      const [author, reservation] = await Promise.all([
        this.userService.getById(workspaceId, req.user.id),
        this.reservationService.getById(workspaceId, reservationId),
      ]);

      const reservationComment = new ReservationComment();
      const args = new CreateReservationComment(
        uuid(),
        reservation,
        comment,
        CommentAuthor.from(author),
      );

      await this.service.create(reservationComment, args);

      return reservationComment;
    } catch (error) {
      throw match(error)
        .whenType(ReservationNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Put(':reservation_id/comment/:comment_id')
  @Transactional()
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UseInterceptors(ClassSerializerInterceptor, TransformerInterceptor)
  @ApiOkResponse({ type: ReservationCommentDTO })
  @ApiNotFoundResponse({
    description:
      'Reservation with the given `reservation_id`, or comment with the given `comment_id` does not exist.',
  })
  @ApiUnauthorizedResponse({
    description: 'user trying to modify the comment is not its author.',
  })
  public async edit(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('reservation_id') reservationId: string,
    @Param('comment_id') commentId: string,
    @Body() { comment }: CreateCommentDTO,
  ): Promise<ReservationComment> {
    const workspaceId = req.user.workspace;

    try {
      const [changedBy, reservation] = await Promise.all([
        this.userService.getById(workspaceId, req.user.id),
        this.reservationService.getById(workspaceId, reservationId),
      ]);
      const reservationComment = await this.service.getById(
        reservation,
        commentId,
      );

      await this.service.edit(
        reservationComment,
        comment,
        CommentAuthor.from(changedBy),
      );

      return reservationComment;
    } catch (error) {
      throw match(error)
        .whenType(InsufficientRightsError)
        .then(() => new UnauthorizedException(error.message))
        .whenType(ReservationNotFoundError)
        .orWhenType(ReservationCommentNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error.message);
    }
  }

  @Delete(':reservation_id/comment/:comment_id')
  @Transactional()
  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(RoleGuard(UserRole.maintainer, UserRole.admin))
  @UseInterceptors(ClassSerializerInterceptor, TransformerInterceptor)
  @ApiNoContentResponse({
    description: 'The comment was successfully deleted.',
  })
  @ApiNotFoundResponse({
    description:
      'Reservation with the given `reservation_id`, or comment with the given `comment_id` does not exist.',
  })
  @ApiUnauthorizedResponse({
    description: 'user trying to modify the comment is not its author.',
  })
  public async delete(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('reservation_id') reservationId: string,
    @Param('comment_id') commentId: string,
  ): Promise<void> {
    const workspaceId = req.user.workspace;

    try {
      const [deletedBy, reservation] = await Promise.all([
        this.userService.getById(workspaceId, req.user.id),
        this.reservationService.getById(workspaceId, reservationId),
      ]);

      const comment = await this.service.getById(reservation, commentId);
      await this.service.delete(comment, CommentAuthor.from(deletedBy));
    } catch (error) {
      throw match(error)
        .whenType(InsufficientRightsError)
        .then(() => new UnauthorizedException(error.message))
        .whenType(ReservationNotFoundError)
        .orWhenType(ReservationCommentNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error.message);
    }
  }
}
