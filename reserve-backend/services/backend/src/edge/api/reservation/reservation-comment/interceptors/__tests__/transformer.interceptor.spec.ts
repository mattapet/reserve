/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TransformerInterceptor } from '../transformer.interceptor';

import { CallHandler } from '@nestjs/common';

import { of } from 'rxjs';
import {
  ReservationComment,
  CommentAuthor,
  CommentAuthorId,
} from '../../../../../../resource-reservation';
//#endregion

describe('reservation-comment transformer.interceptor', () => {
  const interceptor = new TransformerInterceptor();

  function make_callHandlerReturning(
    payload: ReservationComment | ReservationComment[],
  ): CallHandler<ReservationComment | ReservationComment[]> {
    return { handle: () => of(payload) };
  }

  it('should encode a single reservation comment', async () => {
    const comment = new ReservationComment(
      '99',
      'test',
      '44',
      'some comment',
      new Date(0),
      new CommentAuthor(new CommentAuthorId('88')),
    );
    const call$ = make_callHandlerReturning(comment);

    const result = await interceptor.intercept({} as any, call$).toPromise();

    expect(result).toEqual({
      id: '99',
      reservation_id: '44',
      timestamp: new Date(0).toISOString(),
      author: '88',
      comment: 'some comment',
      deleted: false,
    });
  });

  it('should encode an empty array', async () => {
    const call$ = make_callHandlerReturning([]);

    const result = await interceptor.intercept({} as any, call$).toPromise();

    expect(result).toEqual([]);
  });

  it('should encode an array of a single comment', async () => {
    const comment = new ReservationComment(
      '99',
      'test',
      '44',
      'some comment',
      new Date(0),
      new CommentAuthor(new CommentAuthorId('88')),
    );
    const call$ = make_callHandlerReturning([comment]);

    const result = await interceptor.intercept({} as any, call$).toPromise();

    expect(result).toEqual([
      {
        id: '99',
        reservation_id: '44',
        timestamp: new Date(0).toISOString(),
        author: '88',
        comment: 'some comment',
        deleted: false,
      },
    ]);
  });
});
