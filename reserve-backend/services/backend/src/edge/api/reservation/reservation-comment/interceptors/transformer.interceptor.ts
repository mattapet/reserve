/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ReservationComment } from '../../../../../resource-reservation/domain/reservation-comment/reservation-comment.entity';
import { CommentAuthor } from '../../../../../resource-reservation/domain/reservation-comment/comment-author.entity';

import { ReservationCommentDTO } from '../dto/reservation-comment.dto';
//#endregion

type ReservationCommentType = ReservationComment | ReservationComment[];

type ReservationCommentDTOType =
  | ReservationCommentDTO
  | ReservationCommentDTO[];

@Injectable()
export class TransformerInterceptor
  implements
    NestInterceptor<ReservationCommentType, ReservationCommentDTOType> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<ReservationCommentType>,
  ): Observable<ReservationCommentDTOType> {
    return call$
      .handle()
      .pipe(
        map((payload: ReservationCommentType) =>
          Array.isArray(payload)
            ? payload.map(this.encode)
            : this.encode(payload),
        ),
      );
  }

  private encode = (comment: ReservationComment) => ({
    id: comment.getId(),
    reservation_id: comment.getReservationId(),
    timestamp: comment.getTimestamp().toISOString(),
    author: this.encodeAuthor(comment.getAuthor()),
    comment: comment.getComment(),
    deleted: comment.isDeleted(),
  });

  private encodeAuthor = (author: CommentAuthor) => author.getId().get();
}
