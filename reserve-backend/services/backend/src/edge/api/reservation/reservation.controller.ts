/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  UseGuards,
  Req,
  Body,
  UnauthorizedException,
  BadRequestException,
  Post,
  UseInterceptors,
  ClassSerializerInterceptor,
  Put,
  Param,
  ForbiddenException,
  Patch,
  Get,
  Query,
  NotFoundException,
  Inject,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiUnauthorizedResponse,
  ApiForbiddenResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';

import { uuid, match } from '@rsaas/util';
import { Transactional } from '@rsaas/commons/lib/typeorm';
import { JwtGuard } from '@rsaas/commons/lib/guards/jwt.guard';
import { WorkspaceAuthorizedRequest } from '@rsaas/commons/lib/interface/workspace-authorized-request.interface';

import {
  Assignee,
  Reservation,
  ReservationEvent,
  ReservationService,
  CreateReservation,
  UpdateReservation,
  ReservationManager,
} from '../../../resource-reservation/domain/reservation';
import { UserService, User } from '../../../user-access';
import {
  ReservationProjectionRepository,
  ReservationProjection,
} from '../../../resource-reservation/domain/reservation-projection';
import { TypeormReservationProjectionRepository } from '../../../resource-reservation/application/reservation-projection/repositories/typeorm';
import {
  IllegalOperationError,
  InvalidDateRangeError,
  InvalidReservationStateError,
  ResourcesUnavailableError,
  InsufficientRightsError,
} from '../../../resource-reservation/domain/reservation/reservation.errors';
import { ReservationNotFoundError } from '../../../resource-reservation/domain/reservation-projection/reservation-projection.errors';

import { TransformerInterceptor } from './interceptors/transformer.interceptor';
import { EventTransformerInterceptor } from './interceptors/event-transformer.interceptor';
import { CreateReservationDTO } from './dto/create-reservation.dto';
import { UpdateReservationDTO } from './dto/update-reservation.dto';
import { ReservationRejectionDTO } from './dto/reservation-rejection.dto';
import { DateRangeQuery } from './dto/date-range-query.dto';
import { ReservationDTO } from './dto/reservation.dto';
import { ReservationEventDTO } from './dto/reservation-event.dto';
//#endregion

@ApiTags('reservation')
@ApiBearerAuth()
@Controller('api/v1/reservation')
@UseGuards(JwtGuard)
export class ReservationController {
  public constructor(
    private readonly service: ReservationService,
    @Inject(TypeormReservationProjectionRepository)
    private readonly search: ReservationProjectionRepository,
    private readonly user: UserService,
  ) {}

  // Queries

  @Get()
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({
    type: [ReservationDTO],
    description:
      'Returns list of reservations. The route also provides options for filtering based on the reservation time period. A requester can provide `date_start`, `date_end`, or both dates as query parameters forming a interval. If filtering parameters are provided, the API responds only with reservations, which intervals intersect with the one formed by the query parameters.',
  })
  public async getAll(
    @Req() req: WorkspaceAuthorizedRequest,
    @Query() { date_start: dateStart, date_end: dateEnd }: DateRangeQuery,
  ): Promise<ReservationProjection[]> {
    if (dateStart && dateEnd) {
      return this.search.getAllByDateRange(
        req.user.workspace,
        new Date(dateStart),
        new Date(dateEnd),
      );
    } else if (dateStart) {
      return this.search.getAllSince(req.user.workspace, new Date(dateStart));
    } else if (dateEnd) {
      return this.search.getAllUntil(req.user.workspace, new Date(dateEnd));
    } else {
      return this.search.getAll(req.user.workspace);
    }
  }

  @Get(':id')
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({
    type: ReservationDTO,
    description: 'Returns a reservation queried by its `id`.',
  })
  @ApiNotFoundResponse({
    description: 'Resource with given `id` does not exist',
  })
  public async getById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') reservationId: string,
  ): Promise<ReservationProjection> {
    try {
      return await this.search.getById(req.user.workspace, reservationId);
    } catch (error) {
      throw match(error)
        .whenType(ReservationNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Get(':id/events')
  @UseInterceptors(new EventTransformerInterceptor())
  @ApiOkResponse({
    type: [ReservationEventDTO],
    description: 'Returns a list of reservation events in ascending order.',
  })
  @ApiNotFoundResponse({
    description: 'Resource with given `id` does not exist',
  })
  public async getEventsById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') reservationId: string,
  ): Promise<ReservationEvent[]> {
    const events = await this.service.getAllEvents(
      req.user.workspace,
      reservationId,
    );
    if (events.length === 0) {
      throw new NotFoundException(
        `Reservation with id ${reservationId} does not exists.`,
      );
    }
    return events;
  }

  // Commands

  @Post()
  @Transactional()
  @UseInterceptors(TransformerInterceptor, ClassSerializerInterceptor)
  @ApiCreatedResponse({ type: ReservationDTO })
  @ApiBadRequestResponse({ description: 'Error in the payload.' })
  @ApiUnauthorizedResponse({
    description:
      'A workspace user is trying to assign a reservation someone else.',
  })
  public async create(
    @Req() req: WorkspaceAuthorizedRequest,
    @Body()
    {
      date_start: dateStart,
      date_end: dateEnd,
      resource_ids: resourceIds,
      user_id: assigneeId = req.user.id,
      notes = '',
    }: CreateReservationDTO,
  ): Promise<Reservation> {
    try {
      const [assignee, createdBy] = await Promise.all([
        this.user.getById(req.user.workspace, assigneeId),
        this.user.getById(req.user.workspace, req.user.id),
      ]);
      const reservation = new Reservation();
      const newReservation = new CreateReservation(
        uuid(),
        req.user.workspace,
        new Date(dateStart),
        new Date(dateEnd),
        resourceIds,
        notes,
        Assignee.from(assignee),
        ReservationManager.from(createdBy),
      );

      await this.service.create(reservation, newReservation);

      return reservation;
    } catch (error) {
      throw match(error)
        .whenType(InvalidDateRangeError)
        .then(() => new BadRequestException(error.message))
        .whenType(InsufficientRightsError)
        .then(() => new UnauthorizedException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Put(':id')
  @Transactional()
  @UseInterceptors(TransformerInterceptor, ClassSerializerInterceptor)
  @ApiOkResponse({ type: ReservationDTO })
  @ApiBadRequestResponse({ description: 'Error in the payload.' })
  @ApiForbiddenResponse({
    description:
      'Reservation starts in the past or it has already been canceled, rejected, or completed.',
  })
  @ApiUnauthorizedResponse({
    description:
      'A workspace user is trying to assign a reservation someone else.',
  })
  public async update(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') reservationId: string,
    @Body()
    {
      date_start: dateStart,
      date_end: dateEnd,
      resource_ids: resourceIds = [],
      notes = '',
      user_id: userId = req.user.id,
    }: UpdateReservationDTO,
  ): Promise<Reservation> {
    try {
      const reservation = await this.service.getById(
        req.user.workspace,
        reservationId,
      );
      const assignee = await this.user.getById(req.user.workspace, userId);
      const changedBy = await this.user.getById(
        req.user.workspace,
        req.user.id,
      );

      const newReservation = new UpdateReservation(
        req.user.workspace,
        new Date(dateStart),
        new Date(dateEnd),
        resourceIds,
        notes,
        Assignee.from(assignee),
        ReservationManager.from(changedBy),
      );

      await this.service.editReservation(reservation, newReservation);

      return reservation;
    } catch (error) {
      throw match(error)
        .whenType(IllegalOperationError)
        .orWhenType(InvalidReservationStateError)
        .then(() => new ForbiddenException(error.message))
        .whenType(InvalidDateRangeError)
        .then(() => new BadRequestException(error.message))
        .whenType(InsufficientRightsError)
        .then(() => new UnauthorizedException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Patch(':id/confirm')
  @Transactional()
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({ type: ReservationDTO })
  @ApiBadRequestResponse({ description: 'Error in the payload.' })
  @ApiForbiddenResponse({
    description:
      'Reservation starts in the past, some of the resources are unavailable,  or it has already been canceled, rejected, or completed.',
  })
  @ApiUnauthorizedResponse({
    description:
      'A workspace user is trying to assign a reservation someone else.',
  })
  public async confirmById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') reservationId: string,
  ): Promise<Reservation> {
    try {
      const [reservation, changedBy] = await this.getReservationAndChangedBy(
        req,
        reservationId,
      );
      await this.service.confirm(
        reservation,
        ReservationManager.from(changedBy),
      );
      return reservation;
    } catch (error) {
      throw match(error)
        .whenType(InsufficientRightsError)
        .then(() => new UnauthorizedException(error.message))
        .whenType(ResourcesUnavailableError)
        .orWhenType(IllegalOperationError)
        .orWhenType(InvalidReservationStateError)
        .then(() => new ForbiddenException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Patch(':id/cancel')
  @Transactional()
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({ type: ReservationDTO })
  @ApiBadRequestResponse({ description: 'Error in the payload.' })
  @ApiForbiddenResponse({
    description:
      'Reservation starts in the past or it has already been canceled, rejected, or completed.',
  })
  @ApiUnauthorizedResponse({
    description:
      'A workspace user is trying to assign a reservation someone else.',
  })
  public async cancelById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') reservationId: string,
  ): Promise<Reservation> {
    try {
      const [reservation, changedBy] = await this.getReservationAndChangedBy(
        req,
        reservationId,
      );
      await this.service.cancel(
        reservation,
        ReservationManager.from(changedBy),
      );
      return reservation;
    } catch (error) {
      throw match(error)
        .whenType(InsufficientRightsError)
        .then(() => new UnauthorizedException(error.message))
        .whenType(IllegalOperationError)
        .orWhenType(InvalidReservationStateError)
        .then(() => new ForbiddenException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Patch(':id/reject')
  @Transactional()
  @UseInterceptors(TransformerInterceptor)
  @ApiOkResponse({ type: ReservationDTO })
  @ApiBadRequestResponse({ description: 'Error in the payload.' })
  @ApiForbiddenResponse({
    description:
      'Reservation starts in the past or it has already been canceled, rejected, or completed.',
  })
  @ApiUnauthorizedResponse({
    description:
      'A workspace user is trying to assign a reservation someone else.',
  })
  public async rejectById(
    @Req() req: WorkspaceAuthorizedRequest,
    @Param('id') reservationId: string,
    @Body() { reason = '' }: ReservationRejectionDTO,
  ): Promise<Reservation> {
    try {
      const [reservation, changedBy] = await this.getReservationAndChangedBy(
        req,
        reservationId,
      );
      await this.service.reject(
        reservation,
        ReservationManager.from(changedBy),
        reason,
      );
      return reservation;
    } catch (error) {
      throw match(error)
        .whenType(InsufficientRightsError)
        .then(() => new UnauthorizedException(error.message))
        .whenType(IllegalOperationError)
        .orWhenType(InvalidReservationStateError)
        .then(() => new ForbiddenException(error.message))
        .getOrDefault(() => error);
    }
  }

  private async getReservationAndChangedBy(
    req: WorkspaceAuthorizedRequest,
    reservationId: string,
  ): Promise<[Reservation, User]> {
    return Promise.all([
      this.service.getById(req.user.workspace, reservationId),
      this.user.getById(req.user.workspace, req.user.id),
    ]);
  }
}
