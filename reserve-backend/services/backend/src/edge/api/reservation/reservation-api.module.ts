/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons/lib/commons.module';

import { ReservationController } from './reservation.controller';
import { ReservationCommentApiModule } from './reservation-comment';
import { ResourceReservationModule } from '../../../resource-reservation';
import { UserAccessModule } from '../../../user-access';
//#endregion

@Module({
  imports: [
    CommonsModule,
    UserAccessModule,
    ResourceReservationModule,
    ReservationCommentApiModule,
  ],
  controllers: [ReservationController],
})
export class ReservationApiModule {}
