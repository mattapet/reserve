/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class VersionDTO {
  @ApiProperty({
    description: 'The commit hash of the currently deployed system commit.',
  })
  public readonly version: string;

  public constructor(version: string) {
    this.version = version;
  }
}
