/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Controller, Get } from '@nestjs/common';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';

import { VersionDTO } from './dto/version.dto';
//#endregion

const RESERVE_BACKEND_VERSION = process.env.RESERVE_BACKEND_VERSION;

@Controller('api/v1')
@ApiTags('system')
export class VersionController {
  @Get('version')
  @ApiOkResponse({ type: VersionDTO })
  public getApiVersion(): VersionDTO {
    return new VersionDTO(RESERVE_BACKEND_VERSION!);
  }
}
