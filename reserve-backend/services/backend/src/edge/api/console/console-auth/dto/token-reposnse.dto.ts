/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class TokenResponseDTO {
  @ApiProperty()
  public readonly access_token: string;

  @ApiProperty()
  public readonly expires_in: number;

  @ApiProperty()
  public readonly token_type: 'Bearer';

  @ApiProperty()
  public readonly refresh_token: string;

  @ApiProperty()
  public readonly scope: [];

  public constructor(
    access_token: string,
    expires_in: number,
    refresh_token: string,
  ) {
    this.access_token = access_token;
    this.expires_in = expires_in;
    this.token_type = 'Bearer';
    this.refresh_token = refresh_token;
    this.scope = [];
  }
}
