/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, PipeTransform } from '@nestjs/common';

import { ConsoleJWTService } from '../services/console-jwt.service';
import { RefreshTokenDTO } from '../dto/refresh-token.dto';
//#endregion

@Injectable()
export class RefreshTokenValidation
  implements PipeTransform<RefreshTokenDTO, Promise<string>> {
  public constructor(private readonly jwtService: ConsoleJWTService) {}

  public async transform(value: RefreshTokenDTO): Promise<string> {
    const { jit } = await this.jwtService.verify(value.refresh_token);
    return jit;
  }
}
