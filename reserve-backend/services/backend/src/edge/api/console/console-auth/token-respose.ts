/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  ConsoleAccessToken,
  ConsoleRefreshToken,
} from '../../../../console/domain/console-token';
//#endregion

export class TokenResponse {
  public constructor(
    public readonly accessToken: ConsoleAccessToken,
    public readonly refreshToken: ConsoleRefreshToken,
  ) {}
}
