/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { registerJWTServiceModule } from '@rsaas/commons/lib/jwt-service.module';

import { ConsoleAuthModule } from '../../../../console/infrastructure/console-auth';
import { ConsoleTokenModule } from '../../../../console/infrastructure/console-token';

import { ConsoleAuthController } from './console-auth.controller';
import { ConsoleJWTService } from './services/console-jwt.service';
//#endregion

@Module({
  imports: [registerJWTServiceModule(), ConsoleAuthModule, ConsoleTokenModule],
  controllers: [ConsoleAuthController],
  providers: [ConsoleJWTService],
})
export class ConsoleAuthApiModule {}
