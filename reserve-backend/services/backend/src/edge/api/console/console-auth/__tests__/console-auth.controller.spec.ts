/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleAuthController } from '../console-auth.controller';

import { BadRequestException } from '@nestjs/common';

import { ConsoleAccountService } from '../../../../../console/domain/console-account';
import { ConsoleAuthService } from '../../../../../console/domain/console-auth';
import { InMemoryConsoleAccountRepository } from '../../../../../console/application/console-account/repositories/in-memory/in-memory-console-account.repository';
import {
  ConsoleTokenService,
  ConsoleAccessToken,
  ConsoleRefreshToken,
} from '../../../../../console/domain/console-token';
import { InMemoryConsoleTokenRepository } from '../../../../../console/application/console-token/repositories/in-memory/in-memory-console-token.repository';

import { RegisterConsoleAccountDTO } from '../dto/register-console-account.dto';
import { ConsoleAccountDTO } from '../dto/console-account.dto';
import { LoginConsoleAccountDTO } from '../dto/login-console-account.dto';
//#endregion

describe('console auth controller', () => {
  let controller!: ConsoleAuthController;

  beforeEach(() => {
    controller = new ConsoleAuthController(
      new ConsoleAuthService(
        new ConsoleAccountService(new InMemoryConsoleAccountRepository()),
      ),
      new ConsoleTokenService(new InMemoryConsoleTokenRepository()),
    );
  });

  describe('user registration', () => {
    it('should register a new account', async () => {
      const registerDTO = new RegisterConsoleAccountDTO('root', 'root');

      const accountDTO = await controller.register(registerDTO);

      expect(accountDTO).toEqual(new ConsoleAccountDTO('root'));
    });

    it('should throw BadRequestException if username conflict found ', async () => {
      const registerDTO = new RegisterConsoleAccountDTO('root', 'root');
      await controller.register(registerDTO);

      const fn = controller.register(registerDTO);

      await expect(fn).rejects.toThrow(BadRequestException);
    });
  });

  describe('user login', () => {
    it('should return token pair', async () => {
      await controller.register(new RegisterConsoleAccountDTO('root', 'root'));
      const payload = new LoginConsoleAccountDTO('root', 'root');

      const tokens = await controller.login(payload);

      expect(tokens.accessToken).toBeInstanceOf(ConsoleAccessToken);
      expect(tokens.refreshToken).toBeInstanceOf(ConsoleRefreshToken);
    });

    it('should throw BadRequestException when password does not match', async () => {
      await controller.register(new RegisterConsoleAccountDTO('root', 'root'));
      const payload = new LoginConsoleAccountDTO('root', 'other password');

      const fn = controller.login(payload);

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException when user is not found', async () => {
      const payload = new LoginConsoleAccountDTO('root', 'other password');

      const fn = controller.login(payload);

      await expect(fn).rejects.toThrow(BadRequestException);
    });
  });

  describe('refreshing access tokens', () => {
    it('should return refreshed token pair', async () => {
      await controller.register(new RegisterConsoleAccountDTO('root', 'root'));
      const payload = new LoginConsoleAccountDTO('root', 'root');
      const { refreshToken } = await controller.login(payload);

      const tokens = await controller.refreshToken(refreshToken.value);

      expect(tokens.accessToken).toBeInstanceOf(ConsoleAccessToken);
      expect(tokens.refreshToken).toEqual(refreshToken);
    });

    it('should throw BadRequestException if invalid token passed', async () => {
      const fn = controller.refreshToken('some-random-token');

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException if expired token passed', async () => {
      await controller.register(new RegisterConsoleAccountDTO('root', 'root'));
      const payload = new LoginConsoleAccountDTO('root', 'root');
      const { refreshToken } = await controller.login(payload);
      const farFarFuture = Date.now() + 200_000_000;
      jest.spyOn(Date, 'now').mockImplementation(() => farFarFuture);

      const fn = controller.refreshToken(refreshToken.value);

      await expect(fn).rejects.toThrow(BadRequestException);
    });
  });

  describe('token revocation', () => {
    it('should should not allow refresh of revoked refresh token', async () => {
      await controller.register(new RegisterConsoleAccountDTO('root', 'root'));
      const payload = new LoginConsoleAccountDTO('root', 'root');
      const { refreshToken } = await controller.login(payload);

      await controller.revokeToken(refreshToken.value);
      const fn = controller.refreshToken(refreshToken.value);

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException if invalid token passed', async () => {
      const fn = controller.refreshToken('some-random-token');

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException if revoked token passed', async () => {
      await controller.register(new RegisterConsoleAccountDTO('root', 'root'));
      const payload = new LoginConsoleAccountDTO('root', 'root');
      const { refreshToken } = await controller.login(payload);

      await controller.revokeToken(refreshToken.value);
      const fn = controller.revokeToken(refreshToken.value);

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw BadRequestException if non-existing token passed', async () => {
      await controller.register(new RegisterConsoleAccountDTO('root', 'root'));

      const fn = controller.revokeToken('some-random-token');

      await expect(fn).rejects.toThrow(BadRequestException);
    });
  });
});
