/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class ConsoleAccountDTO {
  @ApiProperty()
  public username: string;

  public constructor(username: string) {
    this.username = username;
  }
}
