/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, BadRequestException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import {
  TokenKind,
  ConsoleJwtPayload,
} from '@rsaas/commons/lib/interface/jwt-payload.interface';
import { ConsoleToken } from '../../../../../console/domain/console-token/console-token.repository';
import { ConsoleAccessToken } from '../../../../../console/domain/console-token';
//#endregion

@Injectable()
export class ConsoleJWTService {
  public constructor(private readonly jwtService: JwtService) {}

  public async verify(token: string): Promise<ConsoleJwtPayload> {
    try {
      return this.jwtService.verify<ConsoleJwtPayload>(token);
    } catch (error) {
      throw new BadRequestException('Invalid refresh token');
    }
  }

  public async sign(token: ConsoleToken): Promise<string> {
    return this.jwtService.signAsync({
      jit: token.value,
      sub: token.accountUsername,
      exp: this.getExpirationUnixTimestamp(token),
      iat: this.getIssuedAtUnixTimestamp(token),
      scope: [],
      kind: this.getTokenKind(token),
      user: token.accountUsername,
    });
  }

  private getTokenKind(token: ConsoleToken): TokenKind {
    return token instanceof ConsoleAccessToken
      ? TokenKind.accessToken
      : TokenKind.refreshToken;
  }

  private getExpirationUnixTimestamp(token: ConsoleToken): number {
    return this.getIssuedAtUnixTimestamp(token) + token.expiry;
  }

  private getIssuedAtUnixTimestamp(token: ConsoleToken): number {
    return Math.floor(token.created.valueOf() / 1000);
  }
}
