/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { TokenResponse } from '../token-respose';
import { ConsoleJWTService } from '../services/console-jwt.service';
import { TokenResponseDTO } from '../dto/token-reposnse.dto';
//#endregion

@Injectable()
export class TokenResponseInterceptor
  implements NestInterceptor<TokenResponse, TokenResponseDTO> {
  public constructor(private readonly jwtService: ConsoleJWTService) {}

  public intercept(
    context: ExecutionContext,
    call$: CallHandler<TokenResponse>,
  ): Observable<TokenResponseDTO> {
    return call$
      .handle()
      .pipe(flatMap((payload: TokenResponse) => from(this.encode(payload))));
  }

  private async encode({
    accessToken,
    refreshToken,
  }: TokenResponse): Promise<TokenResponseDTO> {
    return new TokenResponseDTO(
      await this.jwtService.sign(accessToken),
      accessToken.expiry,
      await this.jwtService.sign(refreshToken),
    );
  }
}
