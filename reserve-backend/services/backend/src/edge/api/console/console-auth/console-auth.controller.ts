/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Post,
  Body,
  UseGuards,
  UseInterceptors,
  ClassSerializerInterceptor,
  BadRequestException,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiBody,
  ApiNoContentResponse,
  ApiTags,
  ApiBearerAuth,
} from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { match } from '@rsaas/util';
import { ConsoleJwtGuard } from '@rsaas/commons/lib/guards/console-jwt.guard';

import {
  ConsoleAuthService,
  UsernameAlreadyExistsError,
  InvalidUsernameOrPasswordError,
} from '../../../../console/domain/console-auth';
import {
  ConsoleTokenService,
  InvalidTokenError,
  TokenNotFoundError,
} from '../../../../console/domain/console-token';

import { RegisterConsoleAccountDTO } from './dto/register-console-account.dto';
import { ConsoleAccountDTO } from './dto/console-account.dto';
import { LoginConsoleAccountDTO } from './dto/login-console-account.dto';
import { TokenResponseInterceptor } from './interceptors/token-response.interceptor';
import { TokenResponse } from './token-respose';
import { RefreshTokenValidation } from './interceptors/refresh-token-validator.interceptor';
import { TokenResponseDTO } from './dto/token-reposnse.dto';
import { RefreshTokenDTO } from './dto/refresh-token.dto';
//#endregion

@Controller('api/v1/console/auth')
@ApiTags('auth')
export class ConsoleAuthController {
  public constructor(
    private readonly authService: ConsoleAuthService,
    private readonly tokenService: ConsoleTokenService,
  ) {}

  @Post('/register')
  @Transactional()
  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(ConsoleJwtGuard)
  @ApiBearerAuth()
  @ApiCreatedResponse({ type: ConsoleAccountDTO })
  @ApiBadRequestResponse({
    description:
      'The `username` selected collides with already existing console account.',
  })
  public async register(
    @Body() { username, password }: RegisterConsoleAccountDTO,
  ): Promise<ConsoleAccountDTO> {
    try {
      await this.authService.register(username, password);
      return new ConsoleAccountDTO(username);
    } catch (error) {
      throw match(error)
        .whenType(UsernameAlreadyExistsError)
        .then(() => new BadRequestException('Username is already taken'))
        .getOrDefault(() => error);
    }
  }

  @Post('/login')
  @Transactional()
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(ClassSerializerInterceptor, TokenResponseInterceptor)
  @ApiOkResponse({ type: TokenResponseDTO })
  @ApiBadRequestResponse({ description: 'Invalid username or password.' })
  public async login(
    @Body() { username, password }: LoginConsoleAccountDTO,
  ): Promise<TokenResponse> {
    try {
      const account = await this.authService.authenticate(username, password);
      const [at, rt] = await this.tokenService.generateTokenPair(account);
      return new TokenResponse(at, rt);
    } catch (error) {
      throw match(error)
        .whenType(InvalidUsernameOrPasswordError)
        .then(() => new BadRequestException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Post('/token/refresh')
  @Transactional()
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(ClassSerializerInterceptor, TokenResponseInterceptor)
  @ApiBody({ type: RefreshTokenDTO })
  @ApiOkResponse({ type: TokenResponseDTO })
  @ApiBadRequestResponse({ description: 'Invalid refresh token.' })
  public async refreshToken(
    @Body(RefreshTokenValidation)
    refreshToken: string,
  ): Promise<TokenResponse> {
    try {
      const rt = await this.tokenService.getRefreshToken(refreshToken);
      const at = await this.tokenService.refreshAccessToken(rt);
      return new TokenResponse(at, rt);
    } catch (error) {
      throw match(error)
        .whenType(TokenNotFoundError)
        .orWhenType(InvalidTokenError)
        .then(() => new BadRequestException('Invalid refresh token'))
        .getOrDefault(() => error);
    }
  }

  @Post('/token/revoke')
  @Transactional()
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiBody({ type: RefreshTokenDTO })
  @ApiNoContentResponse({
    description: 'Refresh token was successfully revoked.',
  })
  @ApiBadRequestResponse({ description: 'Invalid refresh token.' })
  public async revokeToken(
    @Body(RefreshTokenValidation)
    refreshToken: string,
  ): Promise<void> {
    try {
      const rt = await this.tokenService.getRefreshToken(refreshToken);
      await this.tokenService.revokeRefreshToken(rt);
    } catch (error) {
      throw match(error)
        .whenType(TokenNotFoundError)
        .orWhenType(InvalidTokenError)
        .then(() => new BadRequestException('Invalid refresh token'))
        .getOrDefault(() => error);
    }
  }
}
