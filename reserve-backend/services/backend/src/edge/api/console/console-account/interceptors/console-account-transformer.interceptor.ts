/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ConsoleAccount } from '../../../../../console/domain/console-account';
import { ConsoleAccountDTO } from '../dto/console-account.dto';
//#endregion

@Injectable()
export class ConsoleAccountTransformer
  implements
    NestInterceptor<
      ConsoleAccount | ConsoleAccount[],
      ConsoleAccountDTO | ConsoleAccountDTO[]
    > {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<ConsoleAccount | ConsoleAccount[]>,
  ): Observable<ConsoleAccountDTO | ConsoleAccountDTO[]> {
    return call$
      .handle()
      .pipe(
        map((data) =>
          Array.isArray(data) ? data.map(this.encode) : this.encode(data),
        ),
      );
  }

  private encode = (account: ConsoleAccount): ConsoleAccountDTO =>
    new ConsoleAccountDTO(account.getUsername());
}
