/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Get,
  UseInterceptors,
  UseGuards,
  NotFoundException,
  Delete,
  Param,
  Req,
  HttpCode,
  HttpStatus,
  ForbiddenException,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiForbiddenResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { match } from '@rsaas/util';
import { ConsoleJwtGuard } from '@rsaas/commons/lib/guards/console-jwt.guard';
import { ConsoleAuthorizedRequest } from '@rsaas/commons/lib/interface/console-authorized-request.interface';

import {
  ConsoleAccountService,
  ConsoleAccount,
  ConsoleAccountNotFoundError,
  InvalidOperationError,
} from '../../../../console/domain/console-account';

import { ConsoleAccountTransformer } from './interceptors/console-account-transformer.interceptor';
import { ConsoleAccountDTO } from './dto/console-account.dto';
//#endregion

@Controller('api/v1/console/account')
@ApiTags('account')
@ApiBearerAuth()
export class ConsoleAccountController {
  public constructor(private readonly accountService: ConsoleAccountService) {}

  @Get()
  @UseGuards(ConsoleJwtGuard)
  @UseInterceptors(ConsoleAccountTransformer)
  @ApiOkResponse({ type: [ConsoleAccountDTO] })
  public async getAll(): Promise<ConsoleAccount[]> {
    return this.accountService.getAll();
  }

  @Delete(':username')
  @Transactional()
  @UseGuards(ConsoleJwtGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiNoContentResponse({
    description:
      'The account with the given `username` have been successfully deleted.',
  })
  @ApiNotFoundResponse({
    description: 'Account with given `username` does not exist.',
  })
  @ApiForbiddenResponse({
    description: 'User trying to remove their own account.',
  })
  public async removeByUsername(
    @Req()
    req: ConsoleAuthorizedRequest,
    @Param('username')
    username: string,
  ): Promise<void> {
    try {
      const accountToRemove = await this.accountService.getByUsername(username);
      const removedBy = await this.accountService.getByUsername(
        req.user.username,
      );

      await this.accountService.remove(accountToRemove, removedBy);
    } catch (error) {
      throw match(error)
        .whenType(ConsoleAccountNotFoundError)
        .then(() => new NotFoundException())
        .whenType(InvalidOperationError)
        .then(() => new ForbiddenException(error.message))
        .getOrDefault(() => error);
    }
  }
}
