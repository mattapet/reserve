/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleAccountController } from '../console-account.controller';

import { NotFoundException, ForbiddenException } from '@nestjs/common';

import {
  ConsoleAccount,
  ConsoleAccountService,
} from '../../../../../console/domain/console-account';
import { InMemoryConsoleAccountRepository } from '../../../../../console/application/console-account/repositories/in-memory/in-memory-console-account.repository';
import { ConsoleAuthorizedRequest } from '@rsaas/commons/lib/interface/console-authorized-request.interface';
//#endregion

describe('console account controller', () => {
  let accountService!: ConsoleAccountService;
  let controller!: ConsoleAccountController;

  beforeEach(() => {
    accountService = new ConsoleAccountService(
      new InMemoryConsoleAccountRepository(),
    );
    controller = new ConsoleAccountController(accountService);
  });

  describe('listing accounts', () => {
    it('should return an empty array if no accounts are created', async () => {
      const result = await controller.getAll();

      expect(result).toEqual([]);
    });

    it('should return a single created account', async () => {
      await accountService.create(new ConsoleAccount('root', 'root'));

      const result = await controller.getAll();

      expect(result).toEqual([new ConsoleAccount('root', 'root')]);
    });

    it('should return multiple created accounts', async () => {
      await accountService.create(new ConsoleAccount('root', 'root'));
      await accountService.create(new ConsoleAccount('admin', 'admin'));

      const result = await controller.getAll();

      expect(result).toEqual(
        expect.arrayContaining([new ConsoleAccount('root', 'root')]),
      );
    });
  });

  describe('account removal', () => {
    it('should remove account by its username', async () => {
      await accountService.create(new ConsoleAccount('root', 'root'));
      await accountService.create(new ConsoleAccount('admin', 'admin'));
      const request: ConsoleAuthorizedRequest = {
        user: { username: 'root' },
      } as any;

      await controller.removeByUsername(request, 'admin');

      const accounts = await controller.getAll();
      expect(accounts).toEqual([new ConsoleAccount('root', 'root')]);
    });

    it('should throw NotFoundException if account to be removed does not exists', async () => {
      await accountService.create(new ConsoleAccount('root', 'root'));
      const request: ConsoleAuthorizedRequest = {
        user: { username: 'root' },
      } as any;

      const fn = controller.removeByUsername(request, 'admin');

      await expect(fn).rejects.toThrow(NotFoundException);
    });

    it('should throw ForbiddenException if trying to remove yourself', async () => {
      await accountService.create(new ConsoleAccount('root', 'root'));
      const request: ConsoleAuthorizedRequest = {
        user: { username: 'root' },
      } as any;

      const fn = controller.removeByUsername(request, 'root');

      await expect(fn).rejects.toThrow(ForbiddenException);
    });
  });
});
