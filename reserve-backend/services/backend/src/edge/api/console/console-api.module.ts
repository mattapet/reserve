/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { ConsoleAuthApiModule } from './console-auth/console-auth-api.module';
import { ConsoleWorkspaceApiModule } from './console-workspace/console-workspace-api.module';
import { ConsoleAccountApiModule } from './console-account';
//#endregion

@Module({
  imports: [
    ConsoleAuthApiModule,
    ConsoleWorkspaceApiModule,
    ConsoleAccountApiModule,
  ],
})
export class ConsoleApiModule {}
