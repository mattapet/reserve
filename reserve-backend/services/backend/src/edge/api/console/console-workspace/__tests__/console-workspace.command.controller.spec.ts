/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleWorkspaceCommandController } from '../console-workspace.command.controller';
import { NotFoundException } from '@nestjs/common';

import { UserServiceBuilder } from '../../../../../user-access/__tests__/user/builders/user.service.builder';
import { WorkspaceServiceBuilder } from '../../../../../workspace-management/__tests__/workspace/builders/workspace.service.builder';
import {
  Workspace,
  WorkspaceService,
  WorkspaceName,
  WorkspaceOwner,
} from '../../../../../workspace-management';
import { User, Identity, UserService } from '../../../../../user-access';
import {
  ConsoleAccountService,
  ConsoleAccount,
} from '../../../../../console/domain/console-account';
import { InMemoryConsoleAccountRepository } from '../../../../../console/application/console-account/repositories/in-memory/in-memory-console-account.repository';
//#endregion

describe('console-workspace.command.controller', () => {
  let accountService: ConsoleAccountService;
  let workspaceService: WorkspaceService;
  let userService: UserService;
  let controller: ConsoleWorkspaceCommandController;

  beforeEach(() => {
    accountService = new ConsoleAccountService(
      new InMemoryConsoleAccountRepository(),
    );
    workspaceService = WorkspaceServiceBuilder.inMemory();
    userService = UserServiceBuilder.inMemory();
    controller = new ConsoleWorkspaceCommandController(
      accountService,
      workspaceService,
      userService,
    );
  });

  async function effect_createOwner(
    identity: Identity,
  ): Promise<WorkspaceOwner> {
    const owner = new User();
    await userService.createOwner(owner, identity);
    return WorkspaceOwner.from(owner);
  }

  async function effect_createUser(identity: Identity): Promise<User> {
    const user = new User();
    await userService.create(user, identity);
    return user;
  }

  describe('transferring ownership', () => {
    it('should transfer workspace ownership', async () => {
      await accountService.create(new ConsoleAccount('root', 'root'));
      const owner = await effect_createOwner(
        new Identity('99', 'test', 'test@test.com'),
      );
      await effect_createUser(new Identity('88', 'test', 'test1@test1.com'));
      const name = new WorkspaceName('test');
      await workspaceService.create(new Workspace(), 'test', name, owner);

      const result = await controller.transferOwnership(
        { user: { username: 'root' } } as any,
        'test',
        { to: '88' },
      );

      expect(result.getOwner()).toEqual(new WorkspaceOwner('88'));
    });

    it('should throw NotFoundException if workspace does not exists', async () => {
      await accountService.create(new ConsoleAccount('root', 'root'));
      await effect_createOwner(new Identity('99', 'test', 'test@test.com'));
      await effect_createUser(new Identity('88', 'test', 'test1@test1.com'));

      const fn = controller.transferOwnership(
        { user: { username: 'root' } } as any,
        'test',
        { to: '88' },
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });

    it('should throw NotFoundException if user transferring to does not exist', async () => {
      await accountService.create(new ConsoleAccount('root', 'root'));
      const owner = await effect_createOwner(
        new Identity('99', 'test', 'test@test.com'),
      );
      const name = new WorkspaceName('test');
      await workspaceService.create(new Workspace(), 'test', name, owner);

      const fn = controller.transferOwnership(
        { user: { username: 'root' } } as any,
        'test',
        { to: '88' },
      );

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });
});
