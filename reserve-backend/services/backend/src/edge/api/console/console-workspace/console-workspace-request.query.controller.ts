/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Inject,
  Controller,
  Get,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOkResponse } from '@nestjs/swagger';

import { ConsoleJwtGuard } from '@rsaas/commons/lib/guards/console-jwt.guard';

import {
  ConsoleWorkspaceRequestProjectionRepository,
  ConsoleWorkspaceRequestProjection,
} from './projections';
import { TypeormConsoleWorkspaceRequestProjectionRepository } from './projections/console-workspace-request/repositories/typeorm/typeorm-console-workspace-request.projection.repository';
import { WorkspaceRequestDTOInterceptor } from './interceptors/workspace-request-dto.interceptor';
import { WorkspaceRequestDTO } from './dto/workspace-request.dto';
//#endregion

@UseGuards(ConsoleJwtGuard)
@Controller('api/v1/console/workspace/request')
@ApiTags('workspace')
@ApiBearerAuth()
export class ConsoleWorkspaceRequestQueryController {
  public constructor(
    @Inject(TypeormConsoleWorkspaceRequestProjectionRepository)
    private readonly repository: ConsoleWorkspaceRequestProjectionRepository,
  ) {}

  @Get()
  @UseInterceptors(WorkspaceRequestDTOInterceptor)
  @ApiOkResponse({ type: [WorkspaceRequestDTO] })
  public async getAll(): Promise<ConsoleWorkspaceRequestProjection[]> {
    return this.repository.getAll();
  }
}
