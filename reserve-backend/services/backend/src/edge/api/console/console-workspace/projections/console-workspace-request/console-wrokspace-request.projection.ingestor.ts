/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Log } from '@rsaas/commons/lib/logger';
import { Injectable, Inject } from '@nestjs/common';
import { Transactional } from '@rsaas/commons/lib/typeorm';
import { EventConsumer, EventPattern } from '@rsaas/commons/lib/event-broker';

import { UserService } from '../../../../../../user-access';
import {
  WorkspaceRequestOpenedEvent,
  WorkspaceRequestState,
  WorkspaceRequestConfirmedEvent,
  WorkspaceRequestDeclinedEvent,
  WorkspaceRequestEventType,
  WorkspaceEventType,
  WorkspaceDeletedEvent,
} from '../../../../../../workspace-management';

import { ConsoleWorkspaceRequestProjectionRepository } from './console-workspace-request.projection.repository';
import { ConsoleWorkspaceRequestProjection } from './console-workspace-request.projection.entity';
import { TypeormConsoleWorkspaceRequestProjectionRepository } from './repositories/typeorm/typeorm-console-workspace-request.projection.repository';
//#endregion

@Injectable()
@EventConsumer()
export class ConsoleWorkspaceRequestProjectionIngestor {
  public constructor(
    @Inject(TypeormConsoleWorkspaceRequestProjectionRepository)
    private readonly repository: ConsoleWorkspaceRequestProjectionRepository,
    private readonly userService: UserService,
  ) {}

  @Log({ format: 'Consumed event {0}' })
  @Transactional()
  @EventPattern(WorkspaceRequestEventType.opened)
  public async consumeOpened(
    event: WorkspaceRequestOpenedEvent,
  ): Promise<void> {
    const owner = await this.userService.getById(
      event.aggregateId,
      event.payload.requestedBy,
    );

    const projection = new ConsoleWorkspaceRequestProjection(
      event.aggregateId,
      event.payload.workspaceName,
      owner.getId(),
      owner.getEmail(),
      WorkspaceRequestState.opened,
      event.timestamp,
      null,
    );

    await this.repository.save(projection);
  }

  @Log({ format: 'Consumed event {0}' })
  @Transactional()
  @EventPattern(WorkspaceRequestEventType.confirmed)
  public async consumeConfirmed(
    event: WorkspaceRequestConfirmedEvent,
  ): Promise<void> {
    const request = await this.repository.findByWorkspaceId(event.aggregateId);
    if (!request) {
      return;
    }

    request.confirmAt(event.timestamp);
    await this.repository.save(request);
  }

  @Log({ format: 'Consumed event {0}' })
  @Transactional()
  @EventPattern(WorkspaceRequestEventType.declined)
  public async consumeDeclined(
    event: WorkspaceRequestDeclinedEvent,
  ): Promise<void> {
    const request = await this.repository.findByWorkspaceId(event.aggregateId);
    if (!request) {
      return;
    }

    request.declineAt(event.timestamp);
    await this.repository.save(request);
  }

  @Log({ format: 'Consumed event {0}' })
  @Transactional()
  @EventPattern(WorkspaceEventType.deleted)
  public async consumeDeleted(event: WorkspaceDeletedEvent): Promise<void> {
    const request = await this.repository.findByWorkspaceId(event.aggregateId);
    request && (await this.repository.remove(request));
  }
}
