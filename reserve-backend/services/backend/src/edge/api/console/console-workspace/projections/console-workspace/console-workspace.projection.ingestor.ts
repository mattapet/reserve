/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';
import { Log } from '@rsaas/commons/lib/logger';
import { Transactional } from '@rsaas/commons/lib/typeorm';
import { EventConsumer, EventPattern } from '@rsaas/commons/lib/event-broker';

import {
  WorkspaceEventType,
  WorkspaceCreatedEvent,
  WorkspaceDeletedEvent,
  WorkspaceOwnershipTransferredEvent,
} from '../../../../../../workspace-management';
import { UserService } from '../../../../../../user-access';

import { ConsoleWorkspaceProjectionRepository } from './console-workspace.projection.repository';
import { TypeormConsoleWorkspaceProjectionRepository } from './repositories/typeorm/typeorm-console-workspace.projection.repository';
import { ConsoleWorkspaceProjection } from './console-workspace.projection.entity';
//#endregion

@Injectable()
@EventConsumer()
export class ConsoleWorkspaceProjectionIngestor {
  public constructor(
    @Inject(TypeormConsoleWorkspaceProjectionRepository)
    private readonly repository: ConsoleWorkspaceProjectionRepository,
    private readonly userService: UserService,
  ) {}

  @Log({ format: 'Consumed event {0} ' })
  @Transactional()
  @EventPattern(WorkspaceEventType.created)
  public async consumeCreated(event: WorkspaceCreatedEvent): Promise<void> {
    const owner = await this.userService.getById(
      event.aggregateId,
      event.payload.createdBy,
    );

    const projection = new ConsoleWorkspaceProjection(
      event.aggregateId,
      event.payload.name,
      owner.getId(),
      owner.getEmail(),
    );

    await this.repository.save(projection);
  }

  @Log({ format: 'Consumed event {0} ' })
  @Transactional()
  @EventPattern(WorkspaceEventType.ownershipTransferred)
  public async consumeOwnershipTransferred(
    event: WorkspaceOwnershipTransferredEvent,
  ): Promise<void> {
    const [projection, owner] = await Promise.all([
      this.repository.findById(event.aggregateId),
      this.userService.getById(event.aggregateId, event.payload.transferredTo),
    ]);

    projection && (projection.ownerId = owner!.getId());
    projection && (projection.ownerEmail = owner!.getEmail());
    projection && (await this.repository.save(projection));
  }

  @Log({ format: 'Consumed event {0} ' })
  @Transactional()
  @EventPattern(WorkspaceEventType.deleted)
  public async consumeDeleted(event: WorkspaceDeletedEvent): Promise<void> {
    const projection = await this.repository.findById(event.aggregateId);
    projection && (await this.repository.remove(projection));
  }
}
