/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  Get,
  Inject,
  UseGuards,
  UseInterceptors,
  Param,
  NotFoundException,
} from '@nestjs/common';
import {
  ApiOkResponse,
  ApiExcludeEndpoint,
  ApiNotFoundResponse,
  ApiTags,
  ApiBearerAuth,
} from '@nestjs/swagger';

import { ConsoleJwtGuard } from '@rsaas/commons/lib/guards/console-jwt.guard';

import { User, UserService } from '../../../../user-access';

import {
  ConsoleWorkspaceProjection,
  ConsoleWorkspaceProjectionRepository,
} from './projections';
import { TypeormConsoleWorkspaceProjectionRepository } from './projections/console-workspace/repositories/typeorm/typeorm-console-workspace.projection.repository';

import { WorkspaceDTOInterceptor } from './interceptors/workspace-dto.interceptor';
import { UserDTOInterceptor } from './interceptors/user-dto.interceptor';
import { WorkspaceRequestDTO } from './dto/workspace-request.dto';
import { UserDTO } from './dto/user.dto';
//#endregion

@Controller('api/v1/console/workspace')
@UseGuards(ConsoleJwtGuard)
@ApiTags('workspace')
@ApiBearerAuth()
export class ConsoleWorkspaceQueryController {
  public constructor(
    private readonly userService: UserService,
    @Inject(TypeormConsoleWorkspaceProjectionRepository)
    private readonly repository: ConsoleWorkspaceProjectionRepository,
  ) {}

  @Get()
  @UseInterceptors(WorkspaceDTOInterceptor)
  @ApiOkResponse({ type: [WorkspaceRequestDTO] })
  public async getAll(): Promise<ConsoleWorkspaceProjection[]> {
    return this.repository.getAll();
  }

  @Get(':id/details')
  @UseInterceptors(WorkspaceDTOInterceptor)
  @ApiExcludeEndpoint()
  public async getById(
    @Param('id') id: string,
  ): Promise<ConsoleWorkspaceProjection> {
    const item = await this.repository.findById(id);
    if (!item) {
      throw new NotFoundException(`Workspace with id '${id}' was not found`);
    }
    return item!;
  }

  @Get(':id/user')
  @UseInterceptors(UserDTOInterceptor)
  @ApiOkResponse({ type: [UserDTO] })
  @ApiNotFoundResponse({
    description: 'Workspace with given `id` does not exist.',
  })
  public async getWorkspaceUsers(
    @Param('id') workspaceId: string,
  ): Promise<User[]> {
    const item = await this.repository.findById(workspaceId);
    if (!item) {
      throw new NotFoundException(
        `Workspace with id '${workspaceId}' was not found`,
      );
    }
    return this.userService.getAll(workspaceId);
  }
}
