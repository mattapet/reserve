/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class WorkspaceDTO {
  @ApiProperty()
  public readonly id: string;

  @ApiProperty()
  public readonly name: string;

  @ApiProperty()
  public readonly owner_id: string;

  @ApiProperty({ oneOf: [{ type: 'string' }, { type: 'null' }] })
  public readonly owner_email: string | null;

  public constructor(
    id: string,
    name: string,
    owner_id: string,
    owner_email: string | null,
  ) {
    this.id = id;
    this.name = name;
    this.owner_id = owner_id;
    this.owner_email = owner_email;
  }
}
