/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonsModule } from '@rsaas/commons';

import { UserAccessModule } from '../../../../../../user-access';

import { ConsoleWorkspaceRequestProjection } from './console-workspace-request.projection.entity';
import { ConsoleWorkspaceRequestProjectionIngestor } from './console-wrokspace-request.projection.ingestor';
import { TypeormConsoleWorkspaceRequestProjectionRepository } from './repositories/typeorm/typeorm-console-workspace-request.projection.repository';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ConsoleWorkspaceRequestProjection,
      TypeormConsoleWorkspaceRequestProjectionRepository,
    ]),
    CommonsModule,
    UserAccessModule,
  ],
  providers: [ConsoleWorkspaceRequestProjectionIngestor],
  exports: [TypeOrmModule],
})
export class ConsoleWorkspaceRequestProjectionModule {}
