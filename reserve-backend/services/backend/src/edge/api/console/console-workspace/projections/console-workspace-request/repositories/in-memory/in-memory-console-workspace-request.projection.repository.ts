/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleWorkspaceRequestProjectionRepository } from '../../console-workspace-request.projection.repository';
import { ConsoleWorkspaceRequestProjection } from '../../console-workspace-request.projection.entity';
//#endregion

type Storage = { [workspaceId: string]: ConsoleWorkspaceRequestProjection };
export class InMemoryConsoleWorkspaceRequestProjectionRepository
  implements ConsoleWorkspaceRequestProjectionRepository {
  private storage: Storage = {};

  public async getAll(): Promise<ConsoleWorkspaceRequestProjection[]> {
    return Object.values(this.storage);
  }

  public async findByWorkspaceId(
    workspaceId: string,
  ): Promise<ConsoleWorkspaceRequestProjection | undefined> {
    return this.storage[workspaceId];
  }

  public async save(
    projection: ConsoleWorkspaceRequestProjection,
  ): Promise<ConsoleWorkspaceRequestProjection> {
    return (this.storage[projection.workspaceId] = projection);
  }

  public async remove(
    projection: ConsoleWorkspaceRequestProjection,
  ): Promise<ConsoleWorkspaceRequestProjection> {
    delete this.storage[projection.workspaceId];
    return projection;
  }
}
