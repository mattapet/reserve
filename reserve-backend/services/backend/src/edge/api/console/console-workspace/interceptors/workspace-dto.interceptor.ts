/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Injectable,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { WorkspaceDTO } from '../dto/workspace.dto';
import { ConsoleWorkspaceProjection } from '../projections';
//#endregion

type T = ConsoleWorkspaceProjection;
@Injectable()
export class WorkspaceDTOInterceptor
  implements NestInterceptor<T | T[], WorkspaceDTO | WorkspaceDTO[]> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<T | T[]>,
  ): Observable<WorkspaceDTO | WorkspaceDTO[]> {
    return call$
      .handle()
      .pipe(
        map((payload: T | T[]) =>
          Array.isArray(payload)
            ? payload.map(this.encode)
            : this.encode(payload),
        ),
      );
  }

  public encode(workspace: T): WorkspaceDTO {
    return new WorkspaceDTO(
      workspace.id,
      workspace.name,
      workspace.ownerId,
      workspace.ownerEmail,
    );
  }
}
