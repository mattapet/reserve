/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Injectable,
  Inject,
} from '@nestjs/common';
import { Observable, from, timer } from 'rxjs';
import { flatMap, map, filter, concatMap, take } from 'rxjs/operators';

import { WorkspaceRequest } from '../../../../../workspace-management';

import {
  ConsoleWorkspaceRequestProjection,
  ConsoleWorkspaceRequestProjectionRepository,
} from '../projections';
import { TypeormConsoleWorkspaceRequestProjectionRepository } from '../projections/console-workspace-request/repositories/typeorm/typeorm-console-workspace-request.projection.repository';
//#endregion

@Injectable()
export class WorkspaceRequestProjectionInterceptor
  implements
    NestInterceptor<WorkspaceRequest, ConsoleWorkspaceRequestProjection> {
  public constructor(
    @Inject(TypeormConsoleWorkspaceRequestProjectionRepository)
    private readonly repository: ConsoleWorkspaceRequestProjectionRepository,
  ) {}

  public intercept(
    context: ExecutionContext,
    call$: CallHandler<WorkspaceRequest>,
  ): Observable<ConsoleWorkspaceRequestProjection> {
    return call$.handle().pipe(
      flatMap((payload: WorkspaceRequest) =>
        timer(0, 200)
          .pipe(
            concatMap(() =>
              from(this.repository.findByWorkspaceId(payload.getId())),
            ),
          )
          .pipe(
            filter((value) => value?.state === payload.getState()),
            map((value) => value as ConsoleWorkspaceRequestProjection),
            take(1),
          ),
      ),
    );
  }
}
