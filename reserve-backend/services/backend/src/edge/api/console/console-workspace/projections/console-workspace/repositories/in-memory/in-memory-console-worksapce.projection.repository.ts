/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { ConsoleWorkspaceProjectionRepository } from '../../console-workspace.projection.repository';
import { ConsoleWorkspaceProjection } from '../../console-workspace.projection.entity';

export class InMemoryConsoleWorkspaceProjectionRepository
  implements ConsoleWorkspaceProjectionRepository {
  private storage: { [id: string]: ConsoleWorkspaceProjection } = {};

  public async clear(): Promise<void> {
    this.storage = {};
  }

  public async getAll(): Promise<ConsoleWorkspaceProjection[]> {
    return Object.values(this.storage);
  }

  public async findById(
    id: string,
  ): Promise<ConsoleWorkspaceProjection | undefined> {
    return this.storage[id];
  }

  public async save(
    projection: ConsoleWorkspaceProjection,
  ): Promise<ConsoleWorkspaceProjection> {
    return (this.storage[projection.id] = projection);
  }

  public async remove(
    projection: ConsoleWorkspaceProjection,
  ): Promise<ConsoleWorkspaceProjection> {
    delete this.storage[projection.id];
    return projection;
  }
}
