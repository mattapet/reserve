/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleWorkspaceRequestQueryController } from '../console-workspace-request.query.controller';

import {
  ConsoleWorkspaceRequestProjection,
  ConsoleWorkspaceRequestProjectionRepository,
} from '../projections';
import { WorkspaceRequestState } from '../../../../../workspace-management';
import { InMemoryConsoleWorkspaceRequestProjectionRepository } from '../projections/console-workspace-request/repositories/in-memory/in-memory-console-workspace-request.projection.repository';
//#endregion

describe('console-workspace-request.read.controller', () => {
  let repository!: ConsoleWorkspaceRequestProjectionRepository;
  let controller!: ConsoleWorkspaceRequestQueryController;

  beforeEach(() => {
    repository = new InMemoryConsoleWorkspaceRequestProjectionRepository();
    controller = new ConsoleWorkspaceRequestQueryController(repository);
  });

  it('should return an empty array if no requests are present', async () => {
    const result = await controller.getAll();

    expect(result).toEqual([]);
  });

  it('should return a single workspace request', async () => {
    await repository.save(
      new ConsoleWorkspaceRequestProjection(
        'test',
        'test-name',
        '99',
        'test@test.com',
        WorkspaceRequestState.opened,
        new Date(100),
        null,
      ),
    );

    const result = await controller.getAll();

    expect(result).toEqual([
      new ConsoleWorkspaceRequestProjection(
        'test',
        'test-name',
        '99',
        'test@test.com',
        WorkspaceRequestState.opened,
        new Date(100),
        null,
      ),
    ]);
  });
});
