/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imporst
import { ConsoleWorkspaceRequestProjectionIngestor } from '../console-wrokspace-request.projection.ingestor';

import {
  WorkspaceRequestEventType,
  WorkspaceRequestState,
  WorkspaceEventType,
} from '../../../../../../../workspace-management';
import { UserService, User, Identity } from '../../../../../../../user-access';
import { UserServiceBuilder } from '../../../../../../../user-access/__tests__/user/builders/user.service.builder';

import { ConsoleWorkspaceRequestProjection } from '../console-workspace-request.projection.entity';
import { ConsoleWorkspaceRequestProjectionRepository } from '../console-workspace-request.projection.repository';
import { InMemoryConsoleWorkspaceRequestProjectionRepository } from '../repositories/in-memory/in-memory-console-workspace-request.projection.repository';
//#endregion

describe('console-workspace-request.projection.ingestor', () => {
  let repository: ConsoleWorkspaceRequestProjectionRepository;
  let userService: UserService;
  let ingestor: ConsoleWorkspaceRequestProjectionIngestor;

  beforeEach(() => {
    repository = new InMemoryConsoleWorkspaceRequestProjectionRepository();
    userService = UserServiceBuilder.inMemory();
    ingestor = new ConsoleWorkspaceRequestProjectionIngestor(
      repository,
      userService,
    );
  });

  async function effect_createOwner(identity: Identity) {
    await userService.createOwner(new User(), identity);
  }

  describe('opened event', () => {
    it('should create a new request projection', async () => {
      await effect_createOwner(new Identity('99', 'test', 'test@test.com'));

      await ingestor.consumeOpened({
        type: WorkspaceRequestEventType.opened,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          workspaceName: 'test-name',
          requestedBy: '99',
        },
      });

      const result = await repository.getAll();
      expect(result).toEqual([
        new ConsoleWorkspaceRequestProjection(
          'test',
          'test-name',
          '99',
          'test@test.com',
          WorkspaceRequestState.opened,
          new Date(100),
          null,
        ),
      ]);
    });
  });

  describe('confirm event', () => {
    it('should close opened request', async () => {
      await effect_createOwner(new Identity('99', 'test', 'test@test.com'));
      await ingestor.consumeOpened({
        type: WorkspaceRequestEventType.opened,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          workspaceName: 'test-name',
          requestedBy: '99',
        },
      });

      await ingestor.consumeConfirmed({
        type: WorkspaceRequestEventType.confirmed,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        timestamp: new Date(200),
        payload: {
          confirmedBy: 'root',
        },
      });

      const result = await repository.getAll();
      expect(result).toEqual([
        new ConsoleWorkspaceRequestProjection(
          'test',
          'test-name',
          '99',
          'test@test.com',
          WorkspaceRequestState.confirmed,
          new Date(100),
          new Date(200),
        ),
      ]);
    });

    it('should do nothing if projection does not exist', async () => {
      await effect_createOwner(new Identity('99', 'test', 'test@test.com'));

      await ingestor.consumeConfirmed({
        type: WorkspaceRequestEventType.confirmed,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        timestamp: new Date(200),
        payload: {
          confirmedBy: 'root',
        },
      });

      const result = await repository.getAll();
      expect(result).toEqual([]);
    });
  });

  describe('decline event', () => {
    it('should close opened request', async () => {
      await effect_createOwner(new Identity('99', 'test', 'test@test.com'));
      await ingestor.consumeOpened({
        type: WorkspaceRequestEventType.opened,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          workspaceName: 'test-name',
          requestedBy: '99',
        },
      });

      await ingestor.consumeDeclined({
        type: WorkspaceRequestEventType.declined,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        timestamp: new Date(200),
        payload: {
          declinedBy: 'root',
        },
      });

      const result = await repository.getAll();
      expect(result).toEqual([
        new ConsoleWorkspaceRequestProjection(
          'test',
          'test-name',
          '99',
          null,
          WorkspaceRequestState.declined,
          new Date(100),
          new Date(200),
        ),
      ]);
    });

    it('should do nothing if projection does not exist', async () => {
      await effect_createOwner(new Identity('99', 'test', 'test@test.com'));

      await ingestor.consumeDeclined({
        type: WorkspaceRequestEventType.declined,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        timestamp: new Date(200),
        payload: {
          declinedBy: 'root',
        },
      });

      const result = await repository.getAll();
      expect(result).toEqual([]);
    });
  });

  describe('workspace deleted event', () => {
    it('should remove the request', async () => {
      await effect_createOwner(new Identity('99', 'test', 'test@test.com'));
      await ingestor.consumeOpened({
        type: WorkspaceRequestEventType.opened,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          workspaceName: 'test-name',
          requestedBy: '99',
        },
      });
      await ingestor.consumeConfirmed({
        type: WorkspaceRequestEventType.confirmed,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        timestamp: new Date(200),
        payload: {
          confirmedBy: 'root',
        },
      });

      await ingestor.consumeDeleted({
        type: WorkspaceEventType.deleted,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(300),
        payload: {
          changedBy: '99',
        },
      });

      const result = await repository.getAll();
      expect(result).toEqual([]);
    });

    it('should do nothing if projection does not exist', async () => {
      await effect_createOwner(new Identity('99', 'test', 'test@test.com'));

      await ingestor.consumeDeleted({
        type: WorkspaceEventType.deleted,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(200),
        payload: {
          changedBy: '99',
        },
      });

      const result = await repository.getAll();
      expect(result).toEqual([]);
    });
  });
});
