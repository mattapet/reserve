/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity, PrimaryColumn, Column } from 'typeorm';
//#endregion

@Entity({ name: 'console_workspace_projection' })
export class ConsoleWorkspaceProjection {
  @PrimaryColumn({ charset: 'utf8' })
  public id: string;

  @Column({ charset: 'utf8' })
  public name: string;

  @Column({ type: 'text', name: 'owner_id', charset: 'utf8' })
  public ownerId: string;

  @Column({
    type: 'text',
    name: 'owner_email',
    charset: 'utf8',
    nullable: true,
  })
  public ownerEmail: string | null;

  public constructor(
    id: string,
    name: string,
    ownerId: string,
    ownerEmail: string | null,
  ) {
    this.id = id;
    this.name = name;
    this.ownerId = ownerId;
    this.ownerEmail = ownerEmail;
  }
}
