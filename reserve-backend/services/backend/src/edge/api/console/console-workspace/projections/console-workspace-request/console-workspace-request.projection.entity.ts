/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Column, Entity, PrimaryColumn } from 'typeorm';

import { WorkspaceRequestState } from '../../../../../../workspace-management';
//#endregion

@Entity({ name: 'console_workspace_request_projection' })
export class ConsoleWorkspaceRequestProjection {
  @PrimaryColumn({ name: 'workspace_id', charset: 'utf8' })
  public workspaceId: string;

  @Column({ name: 'workspace_name', charset: 'utf8' })
  public workspaceName: string;

  @Column({ name: 'owner_id', charset: 'utf8' })
  public ownerId: string;

  @Column({
    type: 'varchar',
    name: 'owner_name',
    charset: 'utf8',
    nullable: true,
  })
  public ownerEmail: string | null;

  @Column({ charset: 'utf8' })
  public state: WorkspaceRequestState;

  @Column({ name: 'opened_at' })
  public openedAt: Date;

  @Column({ type: 'datetime', name: 'closed_at', nullable: true })
  public closedAt?: Date | null;

  public constructor(
    workspaceId: string,
    workspaceName: string,
    ownerId: string,
    ownerEmail: string | null,
    state: WorkspaceRequestState,
    openedAt: Date,
    closedAt: Date | null,
  ) {
    this.workspaceId = workspaceId;
    this.workspaceName = workspaceName;
    this.ownerId = ownerId;
    this.ownerEmail = ownerEmail;
    this.state = state;
    this.openedAt = openedAt;
    this.closedAt = closedAt;
  }

  public confirmAt(openedAt: Date): void {
    this.state = WorkspaceRequestState.confirmed;
    this.closedAt = openedAt;
  }

  public declineAt(declinedAt: Date): void {
    this.state = WorkspaceRequestState.declined;
    this.ownerEmail = null;
    this.closedAt = declinedAt;
  }
}
