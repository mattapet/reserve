/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleWorkspaceProjectionIngestor } from '../console-workspace.projection.ingestor';

import { UserServiceBuilder } from '../../../../../../../user-access/__tests__/user/builders/user.service.builder';
import { User, Identity, UserService } from '../../../../../../../user-access';
import { WorkspaceEventType } from '../../../../../../../workspace-management';

import { ConsoleWorkspaceProjectionRepository } from '../console-workspace.projection.repository';
import { ConsoleWorkspaceProjection } from '../console-workspace.projection.entity';

import { InMemoryConsoleWorkspaceProjectionRepository } from '../repositories/in-memory/in-memory-console-worksapce.projection.repository';
//#endregion

describe('console workspace projection ingestor', () => {
  let repository!: ConsoleWorkspaceProjectionRepository;
  let userService!: UserService;
  let ingestor!: ConsoleWorkspaceProjectionIngestor;

  beforeEach(() => {
    repository = new InMemoryConsoleWorkspaceProjectionRepository();
    userService = UserServiceBuilder.inMemory();
    ingestor = new ConsoleWorkspaceProjectionIngestor(repository, userService);
  });

  async function effect_createUser(identity: Identity) {
    await userService.create(new User(), identity);
  }

  it('should create a new workspace projection', async () => {
    await effect_createUser(new Identity('99', 'test', 'test@test.com'));

    await ingestor.consumeCreated({
      type: WorkspaceEventType.created,
      rowId: 'WorkspaceAggregate',
      aggregateId: 'test',
      timestamp: new Date(100),
      payload: {
        name: 'test-name',
        createdBy: '99',
      },
    });

    const projection = await repository.findById('test');
    expect(projection).toEqual(
      new ConsoleWorkspaceProjection(
        'test',
        'test-name',
        '99',
        'test@test.com',
      ),
    );
  });

  describe('workspace being created', () => {
    it('should delete a workspace projection', async () => {
      await effect_createUser(new Identity('99', 'test', 'test@test.com'));
      await ingestor.consumeCreated({
        type: WorkspaceEventType.created,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          name: 'test-name',
          createdBy: '99',
        },
      });

      await ingestor.consumeDeleted({
        type: WorkspaceEventType.deleted,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          changedBy: '99',
        },
      });

      const projection = await repository.findById('test');
      expect(projection).toBeUndefined();
    });
  });

  describe('workspace ownership being transfered', () => {
    it('should reassign workspace', async () => {
      await effect_createUser(new Identity('99', 'test', 'test1@test1.com'));
      await effect_createUser(new Identity('88', 'test', 'test2@test2.com'));
      await ingestor.consumeCreated({
        type: WorkspaceEventType.created,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          name: 'test-name',
          createdBy: '99',
        },
      });

      await ingestor.consumeOwnershipTransferred({
        type: WorkspaceEventType.ownershipTransferred,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          transferredFrom: '99',
          transferredTo: '88',
          changedByOwner: '99',
        },
      });

      const projection = await repository.findById('test');
      expect(projection).toEqual(
        new ConsoleWorkspaceProjection(
          'test',
          'test-name',
          '88',
          'test2@test2.com',
        ),
      );
    });
  });

  describe('workspace being deleted', () => {
    it('should delete the workspace', async () => {
      await effect_createUser(new Identity('99', 'test', 'test@test.com'));
      await ingestor.consumeCreated({
        type: WorkspaceEventType.created,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          name: 'test-name',
          createdBy: '99',
        },
      });

      await ingestor.consumeDeleted({
        type: WorkspaceEventType.deleted,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          changedBy: '99',
        },
      });

      const projection = await repository.findById('test');
      expect(projection).toBeUndefined();
    });

    it('should do nothing if workspace is already deleted', async () => {
      await effect_createUser(new Identity('99', 'test', 'test@test.com'));

      await ingestor.consumeDeleted({
        type: WorkspaceEventType.deleted,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          changedBy: '99',
        },
      });

      const projection = await repository.findById('test');
      expect(projection).toBeUndefined();
    });
  });
});
