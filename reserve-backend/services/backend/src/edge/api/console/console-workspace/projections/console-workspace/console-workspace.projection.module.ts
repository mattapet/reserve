/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonsModule } from '@rsaas/commons';

import { ConsoleWorkspaceProjection } from './console-workspace.projection.entity';
import { TypeormConsoleWorkspaceProjectionRepository } from './repositories/typeorm/typeorm-console-workspace.projection.repository';
import { ConsoleWorkspaceProjectionIngestor } from './console-workspace.projection.ingestor';
import { UserAccessModule } from '../../../../../../user-access';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ConsoleWorkspaceProjection,
      TypeormConsoleWorkspaceProjectionRepository,
    ]),
    CommonsModule,
    UserAccessModule,
  ],
  providers: [ConsoleWorkspaceProjectionIngestor],
  exports: [TypeOrmModule],
})
export class ConsoleWorkspaceProjectionModule {}
