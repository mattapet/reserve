/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  NotFoundException,
  Post,
  Controller,
  Req,
  Param,
  Body,
  UseGuards,
  UseInterceptors,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOkResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { match } from '@rsaas/util';
import { ConsoleAuthorizedRequest } from '@rsaas/commons/lib/interface/console-authorized-request.interface';
import { ConsoleJwtGuard } from '@rsaas/commons/lib/guards/console-jwt.guard';

import { ConsoleAccountService } from '../../../../console/domain/console-account';
import {
  WorkspaceService,
  Workspace,
  WorkspaceNotFoundError,
  SystemAdministrator,
  WorkspaceUser,
} from '../../../../workspace-management';
import { UserService } from '../../../../user-access';

import { UserNotFoundError } from '../../../../user-access/domain/user/user.errors';

import { TransferOwnershipDTO } from './dto/transfer-ownership.dto';
import { WorkspaceProjectionInterceptor } from './interceptors/workspace-request.interceptor';
import { WorkspaceDTOInterceptor } from './interceptors/workspace-dto.interceptor';
import { WorkspaceDTO } from './dto/workspace.dto';
//#endregion

@UseGuards(ConsoleJwtGuard)
@Controller('api/v1/console/workspace')
@ApiTags('workspace')
@ApiBearerAuth()
export class ConsoleWorkspaceCommandController {
  public constructor(
    private readonly accountService: ConsoleAccountService,
    private readonly workspaceService: WorkspaceService,
    private readonly userService: UserService,
  ) {}

  @Post(':id/transfer')
  @Transactional()
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(WorkspaceDTOInterceptor, WorkspaceProjectionInterceptor)
  @ApiOkResponse({ type: WorkspaceDTO })
  @ApiNotFoundResponse({
    description:
      'Workspace referenced by the `id` or user transferring the workspace to does not exist.',
  })
  public async transferOwnership(
    @Req()
    req: ConsoleAuthorizedRequest,
    @Param('id')
    workspaceId: string,
    @Body()
    dto: TransferOwnershipDTO,
  ): Promise<Workspace> {
    try {
      const [account, workspace, to] = await Promise.all([
        this.accountService.getByUsername(req.user.username),
        this.workspaceService.getById(workspaceId),
        this.userService.getById(workspaceId, dto.to),
      ]);

      await this.workspaceService.transferOwnership(
        workspace,
        WorkspaceUser.from(to),
        SystemAdministrator.from(account),
      );

      return workspace;
    } catch (error) {
      throw match(error)
        .whenType(WorkspaceNotFoundError)
        .orWhenType(UserNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }
}
