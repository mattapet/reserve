/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { ConsoleAccountModule } from '../../../../console/infrastructure/console-account';
import { WorkspaceManagementModule } from '../../../../workspace-management';
import { UserAccessModule } from '../../../../user-access';

import {
  ConsoleWorkspaceProjectionModule,
  ConsoleWorkspaceRequestProjectionModule,
} from './projections';
import { ConsoleWorkspaceQueryController } from './console-workspace.query.controller';
import { ConsoleWorkspaceCommandController } from './console-workspace.command.controller';
import { ConsoleWorkspaceRequestQueryController } from './console-workspace-request.query.controller';
import { ConsoleWorkspaceRequestCommandController } from './console-wrokspace-request.command.controller';
//#endregion

@Module({
  imports: [
    ConsoleWorkspaceProjectionModule,
    ConsoleWorkspaceRequestProjectionModule,
    WorkspaceManagementModule,
    ConsoleAccountModule,
    UserAccessModule,
  ],
  controllers: [
    ConsoleWorkspaceQueryController,
    ConsoleWorkspaceCommandController,
    ConsoleWorkspaceRequestQueryController,
    ConsoleWorkspaceRequestCommandController,
  ],
})
export class ConsoleWorkspaceApiModule {}
