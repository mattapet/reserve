/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleWorkspaceRequestCommandController } from '../console-wrokspace-request.command.controller';
import { BadRequestException, NotFoundException } from '@nestjs/common';

import { ConsoleAuthorizedRequest } from '@rsaas/commons/lib/interface/console-authorized-request.interface';

import {
  WorkspaceRequest,
  WorkspaceService,
  WorkspaceRequestService,
  WorkspaceName,
  WorkspaceOwner,
} from '../../../../../workspace-management';
import { WorkspaceRepositoryBuilder } from '../../../../../workspace-management/__tests__/workspace/builders/workspace.repository.builder';
import { WorkspaceRequestRepositoryBuilder } from '../../../../../workspace-management/__tests__/workspace-request/builders/workspace-request.repository.builder';
import {
  ConsoleAccount,
  ConsoleAccountService,
} from '../../../../../console/domain/console-account';
import { InMemoryConsoleAccountRepository } from '../../../../../console/application/console-account/repositories/in-memory/in-memory-console-account.repository';
//#endregion

describe('console workspace controller -- workspace requests', () => {
  let accountService!: ConsoleAccountService;
  let workspaceService!: WorkspaceService;
  let requestService!: WorkspaceRequestService;
  let controller!: ConsoleWorkspaceRequestCommandController;

  beforeEach(() => {
    accountService = new ConsoleAccountService(
      new InMemoryConsoleAccountRepository(),
    );
    workspaceService = new WorkspaceService(
      WorkspaceRepositoryBuilder.inMemory(),
    );
    requestService = new WorkspaceRequestService(
      WorkspaceRequestRepositoryBuilder.inMemory(),
      workspaceService,
    );
    controller = new ConsoleWorkspaceRequestCommandController(
      requestService,
      accountService,
    );
  });

  async function effect_openWorkspaceRequest(
    id: string,
    name: string,
    userId: string,
  ) {
    const owner = new WorkspaceOwner(userId);
    await requestService.open(new WorkspaceRequest(), {
      workspaceId: id,
      workspaceName: new WorkspaceName(name),
      requestedBy: owner,
    });
  }

  describe('Confirmation of workspace requests', () => {
    it('should confirm workspace request', async () => {
      await effect_openWorkspaceRequest('test', 'test-name', '99');
      await accountService.create(new ConsoleAccount('root', 'root'));
      const request: ConsoleAuthorizedRequest = {
        user: { username: 'root' },
      } as any;

      const result = await controller.confirmRequest(request, 'test');

      expect(result.isConfirmed()).toBe(true);
    });

    it('should throw BadRequestException if request is already confirmed', async () => {
      await effect_openWorkspaceRequest('test', 'test-name', '99');
      await accountService.create(new ConsoleAccount('root', 'root'));
      const request: ConsoleAuthorizedRequest = {
        user: { username: 'root' },
      } as any;

      await controller.confirmRequest(request, 'test');
      const fn = controller.confirmRequest(request, 'test');

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw NotFoundException if request does not exists', async () => {
      await accountService.create(new ConsoleAccount('root', 'root'));
      const request: ConsoleAuthorizedRequest = {
        user: { username: 'root' },
      } as any;

      const fn = controller.confirmRequest(request, 'test');

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('declining workspace requests', () => {
    it('should decline workspace request', async () => {
      await effect_openWorkspaceRequest('test', 'test-name', '99');
      await accountService.create(new ConsoleAccount('root', 'root'));
      const request: ConsoleAuthorizedRequest = {
        user: { username: 'root' },
      } as any;

      const result = await controller.declineRequest(request, 'test');

      expect(result.isDeclined()).toBe(true);
    });

    it('should throw BadRequestException if request is already declined', async () => {
      await effect_openWorkspaceRequest('test', 'test-name', '99');
      await accountService.create(new ConsoleAccount('root', 'root'));
      const request: ConsoleAuthorizedRequest = {
        user: { username: 'root' },
      } as any;

      await controller.declineRequest(request, 'test');
      const fn = controller.declineRequest(request, 'test');

      await expect(fn).rejects.toThrow(BadRequestException);
    });

    it('should throw NotFoundException if request does not exists', async () => {
      await accountService.create(new ConsoleAccount('root', 'root'));
      const request: ConsoleAuthorizedRequest = {
        user: { username: 'root' },
      } as any;

      const fn = controller.declineRequest(request, 'test');

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });
});
