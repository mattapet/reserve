/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository } from 'typeorm';
import { BaseRepository } from '@rsaas/commons/lib/typeorm';

import { ConsoleWorkspaceRequestProjection } from '../../console-workspace-request.projection.entity';
import { ConsoleWorkspaceRequestProjectionRepository } from '../../console-workspace-request.projection.repository';
//#endregion

@EntityRepository(ConsoleWorkspaceRequestProjection)
export class TypeormConsoleWorkspaceRequestProjectionRepository
  extends BaseRepository<ConsoleWorkspaceRequestProjection>
  implements ConsoleWorkspaceRequestProjectionRepository {
  public async getAll(): Promise<ConsoleWorkspaceRequestProjection[]> {
    return this.find();
  }

  public async findByWorkspaceId(
    workspaceId: string,
  ): Promise<ConsoleWorkspaceRequestProjection | undefined> {
    return this.findOne({ workspaceId });
  }
}
