/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Injectable,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { WorkspaceRequestDTO } from '../dto/workspace-request.dto';
import { ConsoleWorkspaceRequestProjection } from '../projections';
//#endregion

type T = ConsoleWorkspaceRequestProjection;
@Injectable()
export class WorkspaceRequestDTOInterceptor
  implements
    NestInterceptor<T | T[], WorkspaceRequestDTO | WorkspaceRequestDTO[]> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<T | T[]>,
  ): Observable<WorkspaceRequestDTO | WorkspaceRequestDTO[]> {
    return call$
      .handle()
      .pipe(
        map((payload: T | T[]) =>
          Array.isArray(payload)
            ? payload.map(this.encode)
            : this.encode(payload),
        ),
      );
  }

  public encode(workspaceRequest: T): WorkspaceRequestDTO {
    return new WorkspaceRequestDTO(
      workspaceRequest.workspaceId,
      workspaceRequest.workspaceName,
      workspaceRequest.ownerId,
      workspaceRequest.ownerEmail,
      workspaceRequest.state,
      workspaceRequest.openedAt.toISOString(),
      workspaceRequest.closedAt?.toISOString(),
    );
  }
}
