/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Injectable,
  Inject,
} from '@nestjs/common';
import { Observable, from, timer } from 'rxjs';
import { flatMap, map, filter, concatMap, take } from 'rxjs/operators';

import { Workspace } from '../../../../../workspace-management';

import {
  ConsoleWorkspaceProjection,
  ConsoleWorkspaceProjectionRepository,
} from '../projections';
import { TypeormConsoleWorkspaceProjectionRepository } from '../projections/console-workspace/repositories/typeorm/typeorm-console-workspace.projection.repository';
//#endregion

@Injectable()
export class WorkspaceProjectionInterceptor
  implements NestInterceptor<Workspace, ConsoleWorkspaceProjection> {
  public constructor(
    @Inject(TypeormConsoleWorkspaceProjectionRepository)
    private readonly repository: ConsoleWorkspaceProjectionRepository,
  ) {}

  public intercept(
    context: ExecutionContext,
    call$: CallHandler<Workspace>,
  ): Observable<ConsoleWorkspaceProjection> {
    return call$.handle().pipe(
      flatMap((payload: Workspace) =>
        timer(0, 200)
          .pipe(
            concatMap(() => from(this.repository.findById(payload.getId()))),
          )
          .pipe(
            filter((value) => value?.ownerId === payload.getOwner().getId()),
            map((value) => value as ConsoleWorkspaceProjection),
            take(1),
          ),
      ),
    );
  }
}
