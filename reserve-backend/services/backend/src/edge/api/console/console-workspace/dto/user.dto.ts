/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ApiProperty } from '@nestjs/swagger';
//#endregion

export class UserDTO {
  @ApiProperty()
  public readonly id: string;

  @ApiProperty()
  public readonly workspace_id: string;

  @ApiProperty()
  public readonly email: string;

  @ApiProperty()
  public readonly first_name: string;

  @ApiProperty()
  public readonly last_name: string;

  @ApiProperty()
  public readonly is_owner: boolean;

  public constructor(
    id: string,
    workspace_id: string,
    email: string,
    first_name: string,
    last_name: string,
    is_owner: boolean = false,
  ) {
    this.id = id;
    this.workspace_id = workspace_id;
    this.email = email;
    this.first_name = first_name;
    this.last_name = last_name;
    this.is_owner = is_owner;
  }
}
