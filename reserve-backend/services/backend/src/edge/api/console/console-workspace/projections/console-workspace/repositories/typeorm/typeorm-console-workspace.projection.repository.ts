/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository } from 'typeorm';

import { ConsoleWorkspaceProjectionRepository } from '../../console-workspace.projection.repository';
import { ConsoleWorkspaceProjection } from '../../console-workspace.projection.entity';
import { BaseRepository } from '@rsaas/commons/lib/typeorm';
//#endregion

@EntityRepository(ConsoleWorkspaceProjection)
export class TypeormConsoleWorkspaceProjectionRepository
  extends BaseRepository<ConsoleWorkspaceProjection>
  implements ConsoleWorkspaceProjectionRepository {
  public async getAll(): Promise<ConsoleWorkspaceProjection[]> {
    return this.find();
  }

  public async findById(
    id: string,
  ): Promise<ConsoleWorkspaceProjection | undefined> {
    return this.findOne({ id });
  }
}
