/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { ConsoleWorkspaceRequestProjection } from './console-workspace-request.projection.entity';

export interface ConsoleWorkspaceRequestProjectionRepository {
  getAll(): Promise<ConsoleWorkspaceRequestProjection[]>;

  findByWorkspaceId(
    id: string,
  ): Promise<ConsoleWorkspaceRequestProjection | undefined>;

  save(
    projection: ConsoleWorkspaceRequestProjection,
  ): Promise<ConsoleWorkspaceRequestProjection>;

  remove(
    projection: ConsoleWorkspaceRequestProjection,
  ): Promise<ConsoleWorkspaceRequestProjection>;
}
