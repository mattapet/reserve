/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Injectable,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../../../../../user-access';

import { UserDTO } from '../dto/user.dto';
//#endregion

@Injectable()
export class UserDTOInterceptor implements NestInterceptor<User[], UserDTO[]> {
  public intercept(
    context: ExecutionContext,
    call$: CallHandler<User[]>,
  ): Observable<UserDTO[]> {
    return call$
      .handle()
      .pipe(map((payload: User[]) => payload.map(this.encode)));
  }

  public encode(user: User): UserDTO {
    return new UserDTO(
      user.getId(),
      user.getWorkspaceId(),
      user.getEmail(),
      user.getIdentity().getFirstName(),
      user.getIdentity().getLastName(),
      user.isOwner(),
    );
  }
}
