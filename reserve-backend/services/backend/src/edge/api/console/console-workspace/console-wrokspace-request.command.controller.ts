/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Controller,
  UseInterceptors,
  UseGuards,
  BadRequestException,
  NotFoundException,
  Req,
  Param,
  HttpStatus,
  HttpCode,
  Post,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiOkResponse,
  ApiBadRequestResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger';

import { Transactional } from '@rsaas/commons/lib/typeorm';
import { match } from '@rsaas/util';
import { ConsoleJwtGuard } from '@rsaas/commons/lib/guards/console-jwt.guard';
import { ConsoleAuthorizedRequest } from '@rsaas/commons/lib/interface/console-authorized-request.interface';

import {
  WorkspaceRequestService,
  InvalidOperationError,
  WorkspaceRequestNotFoundError,
  SystemAdministrator,
} from '../../../../workspace-management';
import { WorkspaceRequest } from '../../../../workspace-management/domain/workspace-request/workspace-request.entity';
import { ConsoleAccountService } from '../../../../console/domain/console-account';

import { WorkspaceRequestDTOInterceptor } from './interceptors/workspace-request-dto.interceptor';
import { WorkspaceRequestProjectionInterceptor } from './interceptors/workspace-request-projection.interceptor';
import { WorkspaceRequestDTO } from './dto/workspace-request.dto';
//#endregion

@Controller('api/v1/console/workspace/request')
@UseGuards(ConsoleJwtGuard)
@ApiTags('workspace-request')
@ApiBearerAuth()
export class ConsoleWorkspaceRequestCommandController {
  public constructor(
    private readonly workspaceRequestService: WorkspaceRequestService,
    private readonly accountService: ConsoleAccountService,
  ) {}

  @Post(':workspace_id/confirm')
  @Transactional()
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(
    WorkspaceRequestDTOInterceptor,
    WorkspaceRequestProjectionInterceptor,
  )
  @ApiOkResponse({ type: WorkspaceRequestDTO })
  @ApiBadRequestResponse({ description: 'Request is already closed' })
  @ApiNotFoundResponse({
    description: 'Workspace request with given `workspace_id` does not exist.',
  })
  public async confirmRequest(
    @Req()
    req: ConsoleAuthorizedRequest,
    @Param('workspace_id')
    workspaceId: string,
  ): Promise<WorkspaceRequest> {
    try {
      const request = await this.workspaceRequestService.getById(workspaceId);
      const account = await this.accountService.getByUsername(
        req.user.username,
      );
      await this.workspaceRequestService.confirm(
        request,
        SystemAdministrator.from(account),
      );
      return request;
    } catch (error) {
      throw match(error)
        .whenType(InvalidOperationError)
        .then(() => new BadRequestException(error.message))
        .whenType(WorkspaceRequestNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }

  @Post(':workspace_id/decline')
  @Transactional()
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(
    WorkspaceRequestDTOInterceptor,
    WorkspaceRequestProjectionInterceptor,
  )
  @ApiOkResponse({ type: WorkspaceRequestDTO })
  @ApiBadRequestResponse({ description: 'Request is already closed' })
  @ApiNotFoundResponse({
    description: 'Workspace request with given `workspace_id` does not exist.',
  })
  public async declineRequest(
    @Req()
    req: ConsoleAuthorizedRequest,
    @Param('workspace_id')
    workspaceId: string,
  ): Promise<WorkspaceRequest> {
    try {
      const request = await this.workspaceRequestService.getById(workspaceId);
      const account = await this.accountService.getByUsername(
        req.user.username,
      );

      await this.workspaceRequestService.decline(
        request,
        SystemAdministrator.from(account),
      );
      return request;
    } catch (error) {
      throw match(error)
        .whenType(InvalidOperationError)
        .then(() => new BadRequestException(error.message))
        .whenType(WorkspaceRequestNotFoundError)
        .then(() => new NotFoundException(error.message))
        .getOrDefault(() => error);
    }
  }
}
