/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleWorkspaceQueryController } from '../console-workspace.query.controller';

import { NotFoundException } from '@nestjs/common';

import { UserService, User, Identity } from '../../../../../user-access';
import { UserServiceBuilder } from '../../../../../user-access/__tests__/user/builders/user.service.builder';

import {
  ConsoleWorkspaceProjection,
  ConsoleWorkspaceProjectionRepository,
} from '../projections';
import { InMemoryConsoleWorkspaceProjectionRepository } from '../projections/console-workspace/repositories/in-memory/in-memory-console-worksapce.projection.repository';
//#endregion

describe('console-workspace.read.controller', () => {
  let repository!: ConsoleWorkspaceProjectionRepository;
  let controller!: ConsoleWorkspaceQueryController;
  let userService!: UserService;

  beforeEach(() => {
    repository = new InMemoryConsoleWorkspaceProjectionRepository();
    userService = UserServiceBuilder.inMemory();
    controller = new ConsoleWorkspaceQueryController(userService, repository);
  });

  async function effect_createTestWorkspace() {
    await repository.save(
      new ConsoleWorkspaceProjection('test', 'test-name', '99', null),
    );
  }

  describe('retrieving workspaces', () => {
    it('should return an empty array if no workspaces are created', async () => {
      const result = await controller.getAll();

      expect(result).toEqual([]);
    });

    it('should return a single workspace if one workspace is stored', async () => {
      await repository.save(
        new ConsoleWorkspaceProjection(
          'test',
          'test-name',
          '99',
          'test@test.com',
        ),
      );

      const result = await controller.getAll();

      expect(result).toEqual([
        new ConsoleWorkspaceProjection(
          'test',
          'test-name',
          '99',
          'test@test.com',
        ),
      ]);
    });

    it('should return a single workspace with nullified user information', async () => {
      await effect_createTestWorkspace();

      const result = await controller.getAll();

      expect(result).toEqual([
        new ConsoleWorkspaceProjection('test', 'test-name', '99', null),
      ]);
    });

    it('should respond with console workspace projection matching given id', async () => {
      await effect_createTestWorkspace();

      const result = await controller.getById('test');

      expect(result).toEqual(
        new ConsoleWorkspaceProjection('test', 'test-name', '99', null),
      );
    });

    it('should throw NotFoundException if request workspace is not created', async () => {
      const fn = controller.getById('test');

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });

  describe('listing workspace users', () => {
    it('should respond with workspace owner', async () => {
      await effect_createTestWorkspace();
      const owner = new User();
      userService.createOwner(
        owner,
        new Identity('99', 'test', 'test@test.com'),
      );

      const result = await controller.getWorkspaceUsers('test');

      expect(result).toEqual([owner]);
    });

    it('should respond with all workspace users', async () => {
      await effect_createTestWorkspace();
      const owner = new User();
      userService.createOwner(
        owner,
        new Identity('99', 'test', 'test1@test1.com'),
      );
      const user = new User();
      userService.create(user, new Identity('88', 'test', 'test2@test2.com'));

      const result = await controller.getWorkspaceUsers('test');

      expect(result).toEqual(expect.arrayContaining([owner, user]));
    });

    it('should throw NotFoundException if workspace does not exist', async () => {
      const fn = controller.getWorkspaceUsers('test');

      await expect(fn).rejects.toThrow(NotFoundException);
    });
  });
});
