/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { ConsoleWorkspaceProjection } from './console-workspace.projection.entity';

export interface ConsoleWorkspaceProjectionRepository {
  getAll(): Promise<ConsoleWorkspaceProjection[]>;

  findById(id: string): Promise<ConsoleWorkspaceProjection | undefined>;

  save(
    projection: ConsoleWorkspaceProjection,
  ): Promise<ConsoleWorkspaceProjection>;

  remove(
    projection: ConsoleWorkspaceProjection,
  ): Promise<ConsoleWorkspaceProjection>;
}
