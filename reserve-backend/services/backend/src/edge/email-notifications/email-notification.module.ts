/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons/lib/commons.module';

import { EmailNotificationService } from './email-notification.service';
import { EmailModule } from '../email/email.module';
import { ReservationAggregateModule } from '../aggregation-services';

import { EMAIL_SENDER_CONFIG } from './constants';
import { ReservationEmailNotificationService } from './reservation/reservation-email-notification.service';
import { UserAccessModule } from '../../user-access';
//#endregion

const EMAIL_SENDER = process.env.EMAIL_SENDER;

@Module({
  imports: [
    CommonsModule,
    EmailModule,
    ReservationAggregateModule,
    UserAccessModule,
  ],
  providers: [
    {
      provide: EMAIL_SENDER_CONFIG,
      useValue: EMAIL_SENDER,
    },
    ReservationEmailNotificationService,
    EmailNotificationService,
  ],
  exports: [EmailNotificationService],
})
export class EmailNotificaitonModule {}
