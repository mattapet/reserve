/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserService, User } from '../../../../user-access';
import { ReservationAggregate } from '../../../aggregation-services';
import { EmailBuilder } from '../../../email/builders/email.builder';
//#endregion

export class ReservationEmailAddresserService {
  public constructor(private readonly userService: UserService) {}

  public async addressEmail(
    aggregate: ReservationAggregate,
    emailBuilder: EmailBuilder,
  ): Promise<EmailBuilder[]> {
    const assignee = aggregate.getAssignee();
    const maintainers = await this.userService.getAllPrivileged(
      aggregate.getWorkspaceId(),
    );

    if (assignee.isAdmin() || assignee.isMaintainer()) {
      return this.aggressEmailToUsers(emailBuilder, maintainers);
    } else {
      return this.aggressEmailToUsers(emailBuilder, [assignee, ...maintainers]);
    }
  }

  private aggressEmailToUsers(
    emailBuilder: EmailBuilder,
    users: User[],
  ): EmailBuilder[] {
    return users.map((user) =>
      emailBuilder.addingAddressee(user.getIdentity()),
    );
  }
}
