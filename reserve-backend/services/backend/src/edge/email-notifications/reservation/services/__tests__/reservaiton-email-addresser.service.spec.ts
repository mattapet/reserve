/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReservationEmailAddresserService } from '../reservation-email-addresser.service';
import { Workspace, WorkspaceName } from '../../../../../workspace-management';
import { Resource } from '../../../../../resource-reservation/domain/resource/resource.entity';
import { Assignee } from '../../../../../resource-reservation/domain/reservation';
import {
  UserBuilder,
  UserService,
  User,
  UserRole,
} from '../../../../../user-access';
import { UserServiceBuilder } from '../../../../../user-access/__tests__/user/builders/user.service.builder';
import { Identity } from '../../../../../user-access/domain/identity';
import { ReservationAggregate } from '../../../../aggregation-services';
import { EmailBuilder } from '../../../../email/builders/email.builder';
import { ReservationBuilder } from '../../../../../resource-reservation/__tests__/reservation/builders/reservation.builder';
//#endregion

describe('reservation email aggresser', () => {
  let service!: ReservationEmailAddresserService;
  let userService!: UserService;

  beforeEach(() => {
    userService = UserServiceBuilder.inMemory();
    service = new ReservationEmailAddresserService(userService);
  });

  const assignee = new UserBuilder()
    .withId('44')
    .withWorkspaceId('test')
    .withEmail('test@test.com')
    .withFirstName('Test')
    .withLastName('Testovic')
    .build();
  const reservation = new ReservationBuilder()
    .withId('99')
    .withWorkspaceId('test')
    .withDateStart(new Date(100))
    .withDateEnd(new Date(100))
    .withAssignee(new Assignee('44'))
    .withResourceIds(['1', '2'])
    .withCreatedAt(new Date(0))
    .build();
  const resources = [
    new Resource('1', 'test', 'resource1', 'descr'),
    new Resource('2', 'test', 'resource2', 'descr'),
  ];
  const workspace = new Workspace('test', new WorkspaceName('test'));

  function make_userWithEmailAndRole(
    id: string,
    email: string,
    role: UserRole,
  ): User {
    return new User(id, 'test', new Identity(id, 'test', email), role);
  }

  function make_reservationAggregate(): ReservationAggregate {
    return new ReservationAggregate(
      workspace,
      reservation,
      assignee,
      resources,
    );
  }

  function make_reservationAggregateWithAssignee(
    user: User,
  ): ReservationAggregate {
    return new ReservationAggregate(workspace, reservation, user, resources);
  }

  async function effect_storeUser(user: User) {
    const role = user.getRole();
    await userService.create(user, user.getIdentity());
    await userService.updateUserRole(
      user,
      role,
      make_userWithEmailAndRole('100', 'test@test.com', UserRole.admin),
    );
  }

  it('should address email to assignee', async () => {
    await effect_storeUser(assignee);
    const aggregate = make_reservationAggregate();

    const result = await service.addressEmail(aggregate, new EmailBuilder());

    expect(result).toEqual([
      new EmailBuilder().addingAddressee(assignee.getIdentity()),
    ]);
  });

  it('should address email to assignee and mainatainer', async () => {
    const mainatainer = make_userWithEmailAndRole(
      '99',
      'maintainer@test.com',
      UserRole.maintainer,
    );
    await effect_storeUser(mainatainer);
    await effect_storeUser(assignee);
    const aggregate = make_reservationAggregate();

    const result = await service.addressEmail(aggregate, new EmailBuilder());

    expect(result).toEqual([
      new EmailBuilder().addingAddressee(assignee.getIdentity()),
      new EmailBuilder().addingAddressee(mainatainer.getIdentity()),
    ]);
  });

  it('should address email to assignee and mainatainer and admin', async () => {
    const maintainer = make_userWithEmailAndRole(
      '99',
      'maintainer@test.com',
      UserRole.maintainer,
    );
    await effect_storeUser(maintainer);
    const admin = make_userWithEmailAndRole(
      '88',
      'admin@test.com',
      UserRole.admin,
    );
    await effect_storeUser(admin);
    await effect_storeUser(assignee);
    const aggregate = make_reservationAggregate();

    const result = await service.addressEmail(aggregate, new EmailBuilder());

    expect(result).toEqual(
      expect.arrayContaining([
        new EmailBuilder().addingAddressee(assignee.getIdentity()),
        new EmailBuilder().addingAddressee(maintainer.getIdentity()),
        new EmailBuilder().addingAddressee(admin.getIdentity()),
      ]),
    );
  });

  it('should not include assignee twice is they are priviged user', async () => {
    const maintainer = make_userWithEmailAndRole(
      '99',
      'maintainer@test.com',
      UserRole.maintainer,
    );
    await effect_storeUser(maintainer);
    const admin = make_userWithEmailAndRole(
      '88',
      'admin@test.com',
      UserRole.admin,
    );
    await effect_storeUser(admin);
    await effect_storeUser(assignee);
    const aggregate = make_reservationAggregateWithAssignee(admin);

    const result = await service.addressEmail(aggregate, new EmailBuilder());

    expect(result.length).toBe(2);
    expect(result).toEqual(
      expect.arrayContaining([
        new EmailBuilder().addingAddressee(maintainer.getIdentity()),
        new EmailBuilder().addingAddressee(admin.getIdentity()),
      ]),
    );
  });
});
