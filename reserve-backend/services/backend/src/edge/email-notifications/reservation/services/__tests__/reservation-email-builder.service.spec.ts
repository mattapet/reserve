/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Assignee } from '../../../../../resource-reservation/domain/reservation';
import { UserBuilder } from '../../../../../user-access';
import { Resource } from '../../../../../resource-reservation/domain/resource/resource.entity';
import { EmailBuilder } from '../../../../email/builders/email.builder';
import { Workspace, WorkspaceName } from '../../../../../workspace-management';
import { ReservationEmailBuilderService } from '../reservation-email-builder.service';
import { ReservationAggregate } from '../../../../aggregation-services/reservation-aggregate/reservation-aggregate.entity';
import { ReservationBuilder } from '../../../../../resource-reservation/__tests__/reservation/builders/reservation.builder';
//#endregion

describe('reservation email notification service', () => {
  let service!: ReservationEmailBuilderService;

  beforeEach(() => {
    service = new ReservationEmailBuilderService();
  });

  const assignee = new UserBuilder()
    .withId('44')
    .withWorkspaceId('test')
    .withEmail('test@test.com')
    .withFirstName('Test')
    .withLastName('Testovic')
    .build();
  const reservation = new ReservationBuilder()
    .withId('99')
    .withWorkspaceId('test')
    .withDateStart(new Date(100))
    .withDateEnd(new Date(100))
    .withAssignee(new Assignee('44'))
    .withResourceIds(['1', '2'])
    .withCreatedAt(new Date(0))
    .build();
  const resources = [
    new Resource('1', 'test', 'resource1', 'descr'),
    new Resource('2', 'test', 'resource2', 'descr'),
  ];
  const workspace = new Workspace('test', new WorkspaceName('test'));

  it('should produce a created reservation email', () => {
    const aggregate = new ReservationAggregate(
      workspace,
      reservation,
      assignee,
      resources,
    );

    const result = service.prepareCreatedReservation(aggregate);

    expect(result).toEqual(
      new EmailBuilder()
        .withSubject('[test] Reservation Created')
        .withMessageId(reservation.getId())
        .appendingContent('Reservation for dates ')
        .appendingContent('1970-01-01 - 1970-01-01 ')
        .appendingContent('was successfully created.\n')
        .appendingContent('Assignee: Test Testovic\n')
        .appendingContent('Resources:\n')
        .appendingContent('- resource1\n')
        .appendingContent('- resource2\n'),
    );
  });

  it('should produce a updated reservation email', () => {
    const aggregate = new ReservationAggregate(
      workspace,
      reservation,
      assignee,
      resources,
    );

    const result = service.prepareUpdatedReservation(aggregate);

    expect(result).toEqual(
      new EmailBuilder()
        .withSubject('[test] Reservation Updated')
        .withInReplyTo(reservation.getId())
        .appendingContent('Reservation for dates ')
        .appendingContent('1970-01-01 - 1970-01-01 ')
        .appendingContent('was updated.\n')
        .appendingContent('Assignee: Test Testovic\n')
        .appendingContent('Resources:\n')
        .appendingContent('- resource1\n')
        .appendingContent('- resource2\n'),
    );
  });

  it('should produce a confirmed reservation email', () => {
    const aggregate = new ReservationAggregate(
      workspace,
      reservation,
      assignee,
      resources,
    );

    const result = service.prepareConfirmedReservation(aggregate);

    expect(result).toEqual(
      new EmailBuilder()
        .withSubject('[test] Reservation Confirmed')
        .withInReplyTo(reservation.getId())
        .appendingContent('Reservation for dates ')
        .appendingContent('1970-01-01 - 1970-01-01 ')
        .appendingContent('was confirmed.\n')
        .appendingContent('Assignee: Test Testovic\n')
        .appendingContent('Resources:\n')
        .appendingContent('- resource1\n')
        .appendingContent('- resource2\n'),
    );
  });

  it('should produce a rejected reservation email', () => {
    const aggregate = new ReservationAggregate(
      workspace,
      reservation,
      assignee,
      resources,
    );

    const result = service.prepareRejectedReservation(aggregate, 'some reason');

    expect(result).toEqual(
      new EmailBuilder()
        .withSubject('[test] Reservation Rejected')
        .withInReplyTo(reservation.getId())
        .appendingContent('Reservation for dates ')
        .appendingContent('1970-01-01 - 1970-01-01 ')
        .appendingContent('was rejected with following reason:\n')
        .appendingContent('some reason\n')
        .appendingContent('Assignee: Test Testovic\n')
        .appendingContent('Resources:\n')
        .appendingContent('- resource1\n')
        .appendingContent('- resource2\n'),
    );
  });

  it('should produce a canceled reservation email', () => {
    const aggregate = new ReservationAggregate(
      workspace,
      reservation,
      assignee,
      resources,
    );

    const result = service.prepareCanceledReservation(aggregate);

    expect(result).toEqual(
      new EmailBuilder()
        .withSubject('[test] Reservation Canceled')
        .withInReplyTo(reservation.getId())
        .appendingContent('Reservation for dates ')
        .appendingContent('1970-01-01 - 1970-01-01 ')
        .appendingContent('was canceled.\n')
        .appendingContent('Assignee: Test Testovic\n')
        .appendingContent('Resources:\n')
        .appendingContent('- resource1\n')
        .appendingContent('- resource2\n'),
    );
  });
});
