/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import moment from 'moment';
import { pipe } from '@rsaas/util/lib/pipe';
import { EmailBuilder } from '../../../email/builders/email.builder';
import { ReservationAggregate } from '../../../aggregation-services/reservation-aggregate/reservation-aggregate.entity';
//#endregion

type EmailMutator = (builder: EmailBuilder) => EmailBuilder;
export class ReservationEmailBuilderService {
  public prepareCreatedReservation(
    reservation: ReservationAggregate,
  ): EmailBuilder {
    return this.createReservationEmail(
      reservation,
      'Reservation Created',
      'was successfully created.',
    ).withMessageId(reservation.getReservationId());
  }

  public prepareUpdatedReservation(
    reservation: ReservationAggregate,
  ): EmailBuilder {
    return this.createReservationEmail(
      reservation,
      'Reservation Updated',
      'was updated.',
    ).withInReplyTo(reservation.getReservationId());
  }

  public prepareConfirmedReservation(
    reservation: ReservationAggregate,
  ): EmailBuilder {
    return this.createReservationEmail(
      reservation,
      'Reservation Confirmed',
      'was confirmed.',
    ).withInReplyTo(reservation.getReservationId());
  }

  public prepareRejectedReservation(
    reservation: ReservationAggregate,
    reason: string,
  ): EmailBuilder {
    return this.createReservationEmail(
      reservation,
      'Reservation Rejected',
      `was rejected with following reason:\n${reason}`,
    ).withInReplyTo(reservation.getReservationId());
  }

  public prepareCanceledReservation(
    reservation: ReservationAggregate,
  ): EmailBuilder {
    return this.createReservationEmail(
      reservation,
      'Reservation Canceled',
      `was canceled.`,
    ).withInReplyTo(reservation.getReservationId());
  }

  private createReservationEmail(
    reservation: ReservationAggregate,
    subject: string,
    message: string,
  ): EmailBuilder {
    return pipe(
      new EmailBuilder(),
      this.getSubjectApplier(reservation, subject),
      this.getDatesApplier(reservation),
      (builder) => builder.appendingContent(` ${message}\n`),
      this.getAssigneeApplier(reservation),
      this.getResourcesApplier(reservation),
    );
  }

  private getSubjectApplier(
    reservation: ReservationAggregate,
    subject: string,
  ): EmailMutator {
    return (builder) =>
      builder.withSubject(
        `[${reservation.getWorkspaceName().get()}] ${subject}`,
      );
  }

  private getDatesApplier(reservation: ReservationAggregate): EmailMutator {
    const dateStart = reservation.getDateStart();
    const dateEnd = reservation.getDateEnd();
    return (builder) =>
      builder
        .appendingContent('Reservation for dates ')
        .appendingContent(
          `${this.formatDate(dateStart)} - ${this.formatDate(dateEnd)}`,
        );
  }

  private getAssigneeApplier(reservation: ReservationAggregate): EmailMutator {
    return (builder) =>
      builder.appendingContent(
        `Assignee: ${reservation.getAssigneeFullName()}\n`,
      );
  }

  private getResourcesApplier(reservation: ReservationAggregate): EmailMutator {
    return (builder) =>
      reservation
        .getResourceNames()
        .reduce(
          (email, next) => email.appendingContent(`- ${next}\n`),
          builder.appendingContent('Resources:\n'),
        );
  }

  private formatDate(date: Date): string {
    return moment(date).format('YYYY-MM-DD');
  }
}
