/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';

import {
  ReservationAggregateService,
  ReservationAggregate,
} from '../../aggregation-services';
import { UserService } from '../../../user-access';
import {
  ReservationCreatedEvent,
  ReservationEvent,
  ReservationConfirmedEvent,
  ReservationRejectedEvent,
  ReservationCanceledEvent,
} from '../../../resource-reservation';
import { EmailBuilder } from '../../email/builders/email.builder';

import { ReservationEmailBuilderService } from './services/reservation-email-builder.service';
import { ReservationEmailAddresserService } from './services/reservation-email-addresser.service';
//#endregion

@Injectable()
export class ReservationEmailNotificationService {
  private readonly emailBuilder: ReservationEmailBuilderService;
  private readonly emailAddresser: ReservationEmailAddresserService;

  public constructor(
    private readonly reservationAggregateService: ReservationAggregateService,
    userService: UserService,
  ) {
    this.emailBuilder = new ReservationEmailBuilderService();
    this.emailAddresser = new ReservationEmailAddresserService(userService);
  }

  public async consumeCreatedEvent(
    event: ReservationCreatedEvent,
  ): Promise<EmailBuilder[]> {
    const aggregate = await this.getReservationAggregateForEvent(event);
    const baseBuilder = this.emailBuilder.prepareCreatedReservation(aggregate);
    return this.emailAddresser.addressEmail(aggregate, baseBuilder);
  }

  public async consumeConfirmedEvent(
    event: ReservationConfirmedEvent,
  ): Promise<EmailBuilder[]> {
    const aggregate = await this.getReservationAggregateForEvent(event);
    const baseBuilder = this.emailBuilder.prepareConfirmedReservation(
      aggregate,
    );
    return this.emailAddresser.addressEmail(aggregate, baseBuilder);
  }

  public async consumeRejectedEvent(
    event: ReservationRejectedEvent,
  ): Promise<EmailBuilder[]> {
    const aggregate = await this.getReservationAggregateForEvent(event);
    const baseBuilder = this.emailBuilder.prepareRejectedReservation(
      aggregate,
      event.payload.reason,
    );
    return this.emailAddresser.addressEmail(aggregate, baseBuilder);
  }

  public async consumeCanceledEvent(
    event: ReservationCanceledEvent,
  ): Promise<EmailBuilder[]> {
    const aggregate = await this.getReservationAggregateForEvent(event);
    const baseBuilder = this.emailBuilder.prepareCanceledReservation(aggregate);
    return this.emailAddresser.addressEmail(aggregate, baseBuilder);
  }

  private async getReservationAggregateForEvent(
    event: ReservationEvent,
  ): Promise<ReservationAggregate> {
    const workspaceId = event.rowId.split(':')[1];
    const reservationId = event.aggregateId;
    return this.reservationAggregateService.getById(workspaceId, reservationId);
  }
}
