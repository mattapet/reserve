/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region
import { Injectable, Inject } from '@nestjs/common';
import { Event } from '@rsaas/commons/lib/events/event.entity';
import { EventConsumer } from '@rsaas/commons/lib/event-broker/decorators/event-consumer.decorator';
import { EventPattern } from '@rsaas/commons/lib/event-broker/decorators/event-pattern.decorator';

import { ReservationEmailNotificationService } from './reservation/reservation-email-notification.service';
import { EmailService } from '../email/email.service';
import { EMAIL_SENDER_CONFIG } from './constants';
import { ReservationEventType } from '../../resource-reservation';
import { EmailBuilder } from '../email/builders/email.builder';
//#endregion

@Injectable()
@EventConsumer()
export class EmailNotificationService {
  public constructor(
    @Inject(EMAIL_SENDER_CONFIG)
    private readonly emailSender: string,
    private readonly emailService: EmailService,
    private readonly reservationService: ReservationEmailNotificationService,
  ) {}

  @EventPattern(ReservationEventType.created)
  public async consumeReservationCreated(event: Event): Promise<void> {
    await this.sendEmails(
      await this.reservationService.consumeCreatedEvent(event as any),
    );
  }

  @EventPattern(ReservationEventType.confirmed)
  public async consumeReservationConfirmed(event: Event): Promise<void> {
    await this.sendEmails(
      await this.reservationService.consumeConfirmedEvent(event as any),
    );
  }

  @EventPattern(ReservationEventType.rejected)
  public async consumeReservationRejected(event: Event): Promise<void> {
    await this.sendEmails(
      await this.reservationService.consumeRejectedEvent(event as any),
    );
  }

  @EventPattern(ReservationEventType.canceled)
  public async consumeReservationCanceled(event: Event): Promise<void> {
    await this.sendEmails(
      await this.reservationService.consumeCanceledEvent(event as any),
    );
  }

  private async sendEmails(emailBuilders: EmailBuilder[]): Promise<void> {
    await emailBuilders
      .map((builder) => builder.withSender(this.emailSender))
      .reduce(async (prev, next) => {
        await prev;
        this.emailService.sendPlainText(next.build());
      }, Promise.resolve());
  }
}
