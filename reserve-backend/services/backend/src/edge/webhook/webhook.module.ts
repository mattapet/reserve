/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons';

import { Webhook } from './webhook.entity';
import { WebhookService } from './webhook.service';
import { TypeormWebhookRepository } from './repositories/typeorm/typeorm-webhook.repository';
//#endregions

@Module({
  imports: [
    CommonsModule,
    TypeOrmModule.forFeature([Webhook, TypeormWebhookRepository]),
  ],
  providers: [WebhookService],
  exports: [WebhookService, TypeOrmModule],
})
export class WebhookModule {}
