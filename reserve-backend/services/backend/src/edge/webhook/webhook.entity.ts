/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  AfterLoad,
  PrimaryColumn,
} from 'typeorm';
import { notNull } from '@rsaas/util';

import { EncodingType, EventType } from './types';
import { InvalidNumberOfEventsError } from './webhook.errors';
//#endregion

@Entity()
export class Webhook {
  @PrimaryGeneratedColumn()
  private id?: number;

  @PrimaryColumn({ name: 'workspace_id', charset: 'utf8' })
  private workspaceId?: string;

  @Column({ name: 'payload_url', charset: 'utf8' })
  private payloadUrl?: string;

  @Column({ charset: 'utf8' })
  private secret?: string;

  @Column({ charset: 'utf8', default: EncodingType.urlencoded })
  private encoding?: EncodingType;

  @Column({ type: 'simple-json', charset: 'utf8' })
  private events: EventType[] = [];

  @Column({ default: true })
  private active: boolean = true;

  public constructor(
    workspaceId?: string,
    payloadUrl?: string,
    secret?: string,
    encoding?: EncodingType,
    events: EventType[] = [],
  ) {
    this.workspaceId = workspaceId;
    this.payloadUrl = payloadUrl;
    this.secret = secret;
    this.encoding = encoding;
    this.events = events;
  }

  @AfterLoad()
  // @ts-ignore
  private normalieze() {
    this.active = !!this.active;
  }

  public getId(): number {
    return notNull(this.id, 'Unexpected null found');
  }

  public getWorkspaceId(): string {
    return notNull(this.workspaceId, 'Unexpected null found');
  }

  public getPayloadUrl(): string {
    return notNull(this.payloadUrl, 'Unexpected null found');
  }

  public getSecret(): string {
    return notNull(this.secret, 'Unexpected null found');
  }

  public getEncoding(): EncodingType {
    return notNull(this.encoding, 'Unexpected null found');
  }

  public getEvents(): EventType[] {
    return this.events;
  }

  public isActive(): boolean {
    return this.active;
  }

  public isInactive(): boolean {
    return !this.isActive();
  }

  public hasBeenCreated(): boolean {
    return !!this.id;
  }

  public hasNotBeenCreated(): boolean {
    return !this.hasBeenCreated();
  }

  public setId(id: number): void {
    this.id = id;
  }

  public setWorkspaceId(workspaceId: string): void {
    this.workspaceId = workspaceId;
  }

  public setPayloadUrl(payloadUrl: string): void {
    this.payloadUrl = payloadUrl;
  }

  public setSecret(secret: string): void {
    this.secret = secret;
  }

  public setEncoding(encoding: EncodingType): void {
    this.encoding = encoding;
  }

  public setEvents(events: EventType[] = []): void {
    if (events.length === 0) {
      throw new InvalidNumberOfEventsError(
        'Webhook must specify at least one dispatch event.',
      );
    }
    this.events = events;
  }

  public matchesEventType(eventType: EventType): boolean {
    return this.events.indexOf(eventType) >= 0;
  }

  public activate(): void {
    this.active = true;
  }

  public deactivate(): void {
    this.active = false;
  }
}
