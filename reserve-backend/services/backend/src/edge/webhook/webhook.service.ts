/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';

import { Webhook } from './webhook.entity';
import { WebhookRepository } from './webhook.repository';
import { TypeormWebhookRepository } from './repositories/typeorm/typeorm-webhook.repository';
import { CreateWebhook, UpdateWebhook } from './interfaces';
import { EventType } from './types';
//#endregion

@Injectable()
export class WebhookService {
  public constructor(
    @Inject(TypeormWebhookRepository)
    private readonly repository: WebhookRepository,
  ) {}

  public async getAll(workspaceId: string): Promise<Webhook[]> {
    return this.repository.getAll(workspaceId);
  }

  public async getAllApplicable(
    workspaceId: string,
    eventType: EventType,
  ): Promise<Webhook[]> {
    const webhooks = await this.repository.getAll(workspaceId);
    return webhooks
      .filter((webhook) => webhook.isActive())
      .filter((webhook) => webhook.matchesEventType(eventType));
  }

  public async getById(workspaceId: string, id: number): Promise<Webhook> {
    return this.repository.getById(workspaceId, id);
  }

  public async create(webhook: Webhook, params: CreateWebhook): Promise<void> {
    webhook.setWorkspaceId(params.workspaceId);
    webhook.setPayloadUrl(params.payloadUrl);
    webhook.setSecret(params.secret);
    webhook.setEncoding(params.encoding);
    webhook.setEvents(params.events);
    await this.repository.save(webhook);
  }

  public async update(webhook: Webhook, params: UpdateWebhook): Promise<void> {
    webhook.setPayloadUrl(params.payloadUrl);
    webhook.setSecret(params.secret);
    webhook.setEncoding(params.encoding);
    webhook.setEvents(params.events);
    await this.repository.save(webhook);
  }

  public async toggleActive(webhook: Webhook): Promise<void> {
    if (webhook.isActive()) {
      webhook.deactivate();
    } else {
      webhook.activate();
    }
    await this.repository.save(webhook);
  }

  public async remove(webhook: Webhook): Promise<void> {
    await this.repository.remove(webhook);
  }
}
