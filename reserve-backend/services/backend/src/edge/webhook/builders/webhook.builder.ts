/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EncodingType, EventType } from '../types';
import { Webhook } from '../webhook.entity';
//#endregion

export class WebhookBuilder {
  private id?: number;
  private workspaceId?: string;
  private payloadUrl?: string;
  private secret?: string;
  private encoding: EncodingType = EncodingType.urlencoded;
  private events: EventType[] = [];
  private active: boolean = true;

  public withId(id: number): this {
    this.id = id;
    return this;
  }

  public withWorkspaceId(workspaceId: string): this {
    this.workspaceId = workspaceId;
    return this;
  }

  public withPayloadUrl(payloadUrl: string): this {
    this.payloadUrl = payloadUrl;
    return this;
  }

  public withSecret(secret: string): this {
    this.secret = secret;
    return this;
  }

  public withEncoding(encoding: EncodingType): this {
    this.encoding = encoding;
    return this;
  }

  public withEvent(event: EventType): this {
    this.events.push(event);
    return this;
  }

  public activated(): this {
    this.active = true;
    return this;
  }

  public deactivated(): this {
    this.active = false;
    return this;
  }

  public build(): Webhook {
    const webhook = new Webhook(
      this.workspaceId,
      this.payloadUrl,
      this.secret,
      this.encoding,
      this.events,
    );
    this.id && webhook.setId(this.id);
    this.active ? webhook.activate() : webhook.deactivate();
    return webhook;
  }
}
