/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository } from 'typeorm';
import { BaseRepository } from '@rsaas/commons/lib/typeorm';

import { Webhook } from '../../webhook.entity';
import { WebhookRepository } from '../../webhook.repository';
import { WebhookNotFoundError } from '../../webhook.errors';
//#endregion

@EntityRepository(Webhook)
export class TypeormWebhookRepository
  extends BaseRepository<Webhook>
  implements WebhookRepository {
  public async getAll(workspaceId: string): Promise<Webhook[]> {
    return this.find({ workspaceId } as any);
  }

  public async getById(workspaceId: string, id: number): Promise<Webhook> {
    const item = await this.findOne({ workspaceId, id } as any);
    if (!item) {
      throw new WebhookNotFoundError(`Webhook with id '${id}' was not found`);
    }
    return item;
  }
}
