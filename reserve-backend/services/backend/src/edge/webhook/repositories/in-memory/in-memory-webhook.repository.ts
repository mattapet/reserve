/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WebhookRepository } from '../../webhook.repository';
import { Webhook } from '../../webhook.entity';
import { WebhookNotFoundError } from '../../webhook.errors';
//#endregion

export class InMemoryWebhookRepository implements WebhookRepository {
  private nextId: number = 1;
  public constructor(
    private readonly storage: { [id: number]: Webhook } = {},
  ) {}

  public async getAll(workspaceId: string): Promise<Webhook[]> {
    return Object.values(this.storage).filter(
      (webhook) => webhook.getWorkspaceId() === workspaceId,
    );
  }

  public async getById(workspaceId: string, id: number): Promise<Webhook> {
    const item = this.storage[id];
    if (!item || item.getWorkspaceId() !== workspaceId) {
      throw new WebhookNotFoundError(`Webhook with id '${id}' was not found`);
    }
    return item;
  }

  public async save(webhook: Webhook): Promise<Webhook> {
    if (webhook.hasNotBeenCreated()) {
      webhook.setId(this.nextId++);
    }
    return (this.storage[webhook.getId()] = webhook);
  }

  public async remove(webhook: Webhook): Promise<Webhook> {
    webhook.getId() && delete this.storage[webhook.getId()];
    return webhook;
  }
}
