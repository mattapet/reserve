/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WebhookService } from '../webhook.service';

import { EventType, EncodingType } from '../types';
import { WebhookBuilder } from '../builders/webhook.builder';
import { InMemoryWebhookRepository } from '../repositories/in-memory/in-memory-webhook.repository';
import {
  WebhookNotFoundError,
  InvalidNumberOfEventsError,
} from '../webhook.errors';
import { Webhook } from '../webhook.entity';
import { CreateWebhook, UpdateWebhook } from '../interfaces';
//#endregion

describe('webhook.service', () => {
  let service!: WebhookService;

  beforeEach(() => {
    service = new WebhookService(new InMemoryWebhookRepository());
  });
  describe('retrieving webhooks', () => {
    it('should return an empty array if there are no webhooks present', async () => {
      const results = await service.getAll('test');

      expect(results).toEqual([]);
    });

    it('should return a single webhook for given workspace ID', async () => {
      await service.create(
        new Webhook(),
        new CreateWebhook('test', 'test.com/webhook', 'secret', [
          EventType.reservationConfirmed,
        ]),
      );

      const results = await service.getAll('test');

      expect(results).toEqual([
        new WebhookBuilder()
          .withWorkspaceId('test')
          .withPayloadUrl('test.com/webhook')
          .withSecret('secret')
          .withEvent(EventType.reservationConfirmed)
          .withId(1)
          .activated()
          .build(),
      ]);
    });

    it('should return a webhook matching its ID', async () => {
      await service.create(
        new Webhook(),
        new CreateWebhook('test', 'test.com/webhook', 'secret', [
          EventType.reservationConfirmed,
        ]),
      );

      const results = await service.getById('test', 1);

      expect(results).toEqual(
        new WebhookBuilder()
          .withWorkspaceId('test')
          .withPayloadUrl('test.com/webhook')
          .withSecret('secret')
          .withEvent(EventType.reservationConfirmed)
          .withId(1)
          .activated()
          .build(),
      );
    });

    it('should throw a WebhookNotFoundError if queried webhook does not exist', async () => {
      const fn = service.getById('test', 1);

      await expect(fn).rejects.toThrow(WebhookNotFoundError);
    });
  });

  describe('managing webhooks', () => {
    it('should override existing webhook', async () => {
      const webhook = new Webhook();
      await service.create(
        webhook,
        new CreateWebhook('test', 'test.com/webhook', 'secret', [
          EventType.reservationConfirmed,
        ]),
      );

      await service.update(
        webhook,
        new UpdateWebhook(
          'another.payload.com/url',
          'secret',
          [EventType.reservationConfirmed],
          EncodingType.json,
        ),
      );

      expect(webhook.getPayloadUrl()).toBe('another.payload.com/url');
      expect(webhook.getEncoding()).toBe(EncodingType.json);
    });

    it('should thow InvalidNumberOfEventsError if no events are specified', async () => {
      const webhook = new Webhook();

      const fn = service.create(
        webhook,
        new CreateWebhook('test', 'test.com/webhook', 'secret', []),
      );

      await expect(fn).rejects.toThrow(InvalidNumberOfEventsError);
    });

    it('should deactivate active webhook', async () => {
      const webhook = new Webhook();
      webhook.activate();

      await service.toggleActive(webhook);

      expect(webhook.isInactive()).toBe(true);
    });

    it('should activate inactive webhook', async () => {
      const webhook = new Webhook();
      webhook.deactivate();

      await service.toggleActive(webhook);

      expect(webhook.isActive()).toBe(true);
    });

    it('should detete existing webhook', async () => {
      const webhook = new Webhook();
      await service.create(
        webhook,
        new CreateWebhook('test', 'test.com/webhook', 'secret', [
          EventType.reservationConfirmed,
        ]),
      );

      await service.remove(webhook);

      const fn = service.getById('test', 1);
      await expect(fn).rejects.toThrow(WebhookNotFoundError);
    });
  });
});
