/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EncodingType, EventType } from '../types';
//#endregion

export class UpdateWebhook {
  public constructor(
    public readonly payloadUrl: string,
    public readonly secret: string,
    public readonly events: EventType[],
    public readonly encoding: EncodingType,
  ) {}
}
