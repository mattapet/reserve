/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Webhook } from './webhook.entity';
//#endregion

export interface WebhookRepository {
  getAll(workspaceId: string): Promise<Webhook[]>;
  getById(workspaceId: string, id: number): Promise<Webhook>;
  save(webhook: Webhook): Promise<Webhook>;
  remove(webhook: Webhook): Promise<Webhook>;
}
