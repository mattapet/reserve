/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { WorkspaceManagementModule } from '../../../workspace-management';
import { ResourceReservationModule } from '../../../resource-reservation';
import { UserAccessModule } from '../../../user-access';
import { ReservationAggregateService } from './reservation-aggregate.service';
//#endregion

@Module({
  imports: [
    WorkspaceManagementModule,
    ResourceReservationModule,
    UserAccessModule,
  ],
  providers: [ReservationAggregateService],
  exports: [ReservationAggregateService],
})
export class ReservationAggregateModule {}
