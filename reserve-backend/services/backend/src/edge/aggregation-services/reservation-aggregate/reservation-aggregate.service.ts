/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { ReservationAggregate } from './reservation-aggregate.entity';
import { WorkspaceService } from '../../../workspace-management';
import {
  ReservationService,
  ResourceService,
} from '../../../resource-reservation';
import { UserService } from '../../../user-access';
//#endregion

@Injectable()
export class ReservationAggregateService {
  public constructor(
    private readonly workspaceService: WorkspaceService,
    private readonly reservationService: ReservationService,
    private readonly resourceService: ResourceService,
    private readonly userService: UserService,
  ) {}

  public async getById(
    workspaceId: string,
    reservationId: string,
  ): Promise<ReservationAggregate> {
    const workspace = await this.workspaceService.getById(workspaceId);
    const reservation = await this.reservationService.getById(
      workspaceId,
      reservationId,
    );
    const resources = await this.resourceService.getByIds(
      workspaceId,
      reservation.getResourceIds(),
    );
    const assignee = await this.userService.getById(
      workspaceId,
      reservation.getAssignee().getId(),
    );

    return new ReservationAggregate(
      workspace,
      reservation,
      assignee,
      resources,
    );
  }
}
