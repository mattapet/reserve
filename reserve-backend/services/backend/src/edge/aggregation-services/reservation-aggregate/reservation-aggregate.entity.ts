/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Workspace, WorkspaceName } from '../../../workspace-management';
import { Reservation } from '../../../resource-reservation';
import { User } from '../../../user-access';
import { Resource } from '../../../resource-reservation/domain/resource/resource.entity';
//#endregion

export class ReservationAggregate {
  public constructor(
    private readonly workspace: Workspace,
    private readonly reservation: Reservation,
    private readonly assignee: User,
    private readonly resources: Resource[],
  ) {}

  public getWorkspaceId(): string {
    return this.workspace.getId();
  }

  public getWorkspaceName(): WorkspaceName {
    return this.workspace.getName();
  }

  public getReservationId(): string {
    return this.reservation.getId();
  }

  public getDateStart(): Date {
    return this.reservation.getDateStart();
  }

  public getDateEnd(): Date {
    return this.reservation.getDateEnd();
  }

  public getAssigneeFullName(): string {
    return this.assignee.getIdentity().getFullName();
  }

  public getAssignee(): User {
    return this.assignee;
  }

  public getAssigneeId(): string {
    return this.assignee.getId();
  }

  public getResourceNames(): string[] {
    return this.resources.map((resource) => resource.getName());
  }
}
