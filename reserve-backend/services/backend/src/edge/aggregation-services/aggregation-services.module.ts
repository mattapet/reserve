/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { ReservationAggregateModule } from './reservation-aggregate';
//#endregion

@Module({
  imports: [ReservationAggregateModule],
  exports: [ReservationAggregateModule],
})
export class AggregationServicesModule {}
