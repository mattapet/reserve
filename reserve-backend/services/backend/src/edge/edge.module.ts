/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { ApiModule } from './api';
import { EmailNotificaitonModule } from './email-notifications';
import { SetupModule } from './setup';
import { EmailService } from './email';
import { WebhookModule } from './webhook';
import { WebhookEventIngestorModule } from './webhook-event-ingestor';
//#endregion

@Module({
  imports: [
    ApiModule,
    EmailService,
    EmailNotificaitonModule,
    SetupModule,
    WebhookModule,
    WebhookEventIngestorModule,
  ],
})
export class EdgeModule {}
