/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import crypto from 'crypto';
//#endregion

const SECRET = process.env.EMAIL_VERIFICATION_SECRET as string;
const ALG = process.env.EMAIL_VERIFICATION_ALG as string;

export function signEmailVerification(
  workspaceId: string,
  email: string,
  expiry: number, // Unix timestamp
): string {
  return crypto
    .createHmac(ALG, SECRET)
    .update(`${workspaceId}:${email}:${expiry}`)
    .digest('hex');
}
