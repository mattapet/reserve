/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { VerificationService } from './verification.service';
import { EmailModule } from '../email';
import { EMAIL_SENDER_CONFIG } from './constants';
//#endregion

const EMAIL_SENDER = process.env.EMAIL_SENDER;

@Module({
  imports: [EmailModule],
  providers: [
    VerificationService,
    {
      provide: EMAIL_SENDER_CONFIG,
      useValue: EMAIL_SENDER,
    },
  ],
  exports: [VerificationService],
})
export class VerificationModule {}
