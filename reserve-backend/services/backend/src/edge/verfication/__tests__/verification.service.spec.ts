/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { VerificationService } from '../verification.service';

import { EmailService } from '../../email';
import { EmailServiceMock } from '../../email/__mocks__';
import { Identity } from '../../../user-access';
import { EmailBuilder } from '../../email/builders/email.builder';
//#endregion

jest.mock('../services/sign-email-verification', () => ({
  signEmailVerification: jest
    .fn()
    .mockImplementation(
      (workspaceId: string, email: string, expiry: number) =>
        `${workspaceId}:${email}:${expiry}:secretsignature`,
    ),
}));

describe('verification.service', () => {
  let emailService!: EmailService;
  let verificationService!: VerificationService;
  const baseUrl = 'http://test.localhost:3000/api/v1/email/confirm';

  beforeEach(() => {
    emailService = new EmailServiceMock();
    verificationService = new VerificationService(
      emailService,
      'no-reply@localhost.com',
    );
  });

  describe('#verify()', () => {
    const workspaceId = 'workspaceId';
    const email = 'email';

    it('should sign workspace and email and create appropriate url', async () => {
      // tslint:disable:no-bitwise
      const expiry = (Date.now() / 1000 + 60 * 15) >>> 0;
      const sendPlainText = jest.spyOn(emailService, 'sendPlainText');
      const identity = new Identity('99', workspaceId, email);

      await verificationService.verify(baseUrl, identity);

      const url =
        `${baseUrl}?workspaceId=${workspaceId}&email=${email}` +
        `&expiry=${expiry}&signature=${workspaceId}%3A${email}%3A${expiry}` +
        '%3Asecretsignature';
      expect(sendPlainText).toHaveBeenCalledWith(
        new EmailBuilder()
          .withSender('no-reply@localhost.com')
          .withContent(url)
          .withSubject('Reserve Email Verify')
          .addingAddressee(identity)
          .build(),
      );
    });
  });

  describe('#confirm()', () => {
    const workspaceId = 'workspaceId';
    const email = 'email';

    it('should return `true` if signature is matches', async () => {
      // tslint:disable:no-bitwise
      const expiry = (Date.now() / 1000 + 60 * 15) >>> 0;
      const signature = `${workspaceId}:${email}:${expiry}:secretsignature`;

      const result = await verificationService.confirm(
        workspaceId,
        email,
        expiry,
        signature,
      );

      expect(result).toBe(true);
    });

    it('should return `false` if signature is matches', async () => {
      // tslint:disable:no-bitwise
      const expiry = (Date.now() / 1000 + 60 * 15) >>> 0;
      const signature = `${workspaceId}:${email}:${expiry}:secretsignatur`;

      const result = await verificationService.confirm(
        workspaceId,
        email,
        expiry,
        signature,
      );

      expect(result).toBe(false);
    });

    it('throw an error if expiry date in past', async () => {
      // tslint:disable:no-bitwise
      const expiry = (Date.now() / 1000 - 60 * 15) >>> 0;
      const signature = `${workspaceId}:${email}:${expiry}:secretsignatur`;

      await expect(
        verificationService.confirm(workspaceId, email, expiry, signature),
      ).rejects.toEqual(new Error('Verification code expired'));
    });
  });
});
