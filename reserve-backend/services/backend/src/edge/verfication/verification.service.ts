/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import qs from 'qs';
import { Injectable, Inject } from '@nestjs/common';

import { EMAIL_SENDER_CONFIG } from './constants';
import { signEmailVerification } from './services/sign-email-verification';
import { EmailService } from '../email';
import { Identity } from '../../user-access/domain/identity';
import { Email } from '../email/email.entity';
import { EmailBuilder } from '../email/builders/email.builder';
//#endregion

@Injectable()
export class VerificationService {
  public constructor(
    private readonly emailService: EmailService,
    @Inject(EMAIL_SENDER_CONFIG)
    private readonly verificationSender: string,
  ) {}

  public async verify(baseUrl: string, identity: Identity): Promise<void> {
    const workspaceId = identity.getWorkspaceId();
    const email = identity.getEmail();
    // Set expiry date 15 minutes into the future
    // tslint:disable:no-bitwise
    const expiry = (Date.now() / 1000 + 60 * 15) >>> 0;
    const signature = signEmailVerification(workspaceId, email, expiry);
    const payload = {
      workspaceId,
      email,
      expiry,
      signature,
    };
    const url = `${baseUrl}?${qs.stringify(payload)}`;
    const verificationEmail = this.createtVerificaitonEmail(identity, url);
    await this.emailService.sendPlainText(verificationEmail);
  }

  private createtVerificaitonEmail(addressee: Identity, url: string): Email {
    return new EmailBuilder()
      .withContent(url)
      .withSubject('Reserve Email Verify')
      .withSender(this.verificationSender)
      .addingAddressee(addressee)
      .build();
  }

  public async confirm(
    workspaceId: string,
    email: string,
    expiry: number,
    signature: string,
  ): Promise<boolean> {
    if (expiry * 1000 < Date.now()) {
      throw new Error('Verification code expired');
    }

    return signature === signEmailVerification(workspaceId, email, expiry);
  }
}
