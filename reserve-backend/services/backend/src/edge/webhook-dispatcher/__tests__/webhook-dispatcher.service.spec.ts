/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { WebhookDispatcherService } from '../webhook-dispatcher.service';

import { Logger } from '@rsaas/commons/lib/logger';
import { HttpService } from '@rsaas/commons/lib/http/http.service';
import { ConsoleLogger } from '@rsaas/commons/lib/logger/console/logger.service';
import { HTTPMethod } from '@rsaas/commons/lib/http/http-method.type';

import { WebhookBuilder } from '../../webhook/builders/webhook.builder';
import { EncodingType } from '../../webhook';

import { SignatureService } from '../services/signature.service';
import { WebhookConfig } from '../services/webhook.config';
import { WenhookCouldNotBeDispatchError } from '../webhook-dispatcher.errors';
import { SuccessfullWebhookResponse } from '../webhook-response.value';

describe('webhook-dispatcher.service', () => {
  function make_dispatcherWithFetchAndLogger(
    fetch: any,
    logger: Logger,
  ): WebhookDispatcherService {
    const signer = new SignatureService();
    jest.spyOn(signer, 'sign').mockImplementation((a, b) => a + b);
    return new WebhookDispatcherService(
      new WebhookConfig('X-Reserve-Signature', 2),
      logger,
      new HttpService(fetch),
      signer,
    );
  }

  function make_dispatcherWithFetch(fetch: any): WebhookDispatcherService {
    return make_dispatcherWithFetchAndLogger(fetch, new ConsoleLogger());
  }

  function make_response(
    status: number,
    headers: [string, string][] = [],
    text: string = '',
  ) {
    return {
      status,
      headers: new Map(headers),
      text: () => Promise.resolve(text),
    };
  }

  describe('forming payload', () => {
    it('should dispatch given webhook with specified payload', async () => {
      const fetch = jest
        .fn()
        .mockImplementation(() => Promise.resolve(make_response(200)));
      const dispatcher = make_dispatcherWithFetch(fetch);

      await dispatcher.dispatch(
        new WebhookBuilder()
          .activated()
          .withId(1)
          .withSecret('shh')
          .withPayloadUrl('http://localhost:300/webhook/test')
          .build(),
        { message: 'some-message' },
      );

      expect(fetch).toHaveBeenCalledWith('http://localhost:300/webhook/test', {
        body: 'message=some-message',
        headers: {
          'Content-Type': EncodingType.urlencoded,
          'X-Reserve-Signature': 'message=some-messageshh',
        },
        method: HTTPMethod.post,
      });
    });

    it('should dispatch given webhook with specified JSON payload', async () => {
      const fetch = jest
        .fn()
        .mockImplementation(() => Promise.resolve(make_response(200)));
      const dispatcher = make_dispatcherWithFetch(fetch);

      await dispatcher.dispatch(
        new WebhookBuilder()
          .activated()
          .withId(1)
          .withSecret('shh')
          .withPayloadUrl('http://localhost:300/webhook/test')
          .withEncoding(EncodingType.json)
          .build(),
        { message: 'some-message' },
      );

      expect(fetch).toHaveBeenCalledWith('http://localhost:300/webhook/test', {
        body: '{"message":"some-message"}',
        headers: {
          'Content-Type': EncodingType.json,
          'X-Reserve-Signature': '{"message":"some-message"}shh',
        },
        method: HTTPMethod.post,
      });
    });
  });

  describe('encoding webhook response', () => {
    it('should pass successfull webhooks reponse', async () => {
      const fetch = jest
        .fn()
        .mockImplementation(() =>
          Promise.resolve(
            make_response(
              200,
              [['Content-Type', 'text']],
              'Successfull response',
            ),
          ),
        );
      const dispatcher = make_dispatcherWithFetch(fetch);

      const response = await dispatcher.dispatch(
        new WebhookBuilder()
          .activated()
          .withId(1)
          .withSecret('shh')
          .withPayloadUrl('http://localhost:300/webhook/test')
          .withEncoding(EncodingType.json)
          .build(),
        { message: 'some-message' },
      );

      expect(response).toEqual(
        new SuccessfullWebhookResponse(
          200,
          { 'Content-Type': 'text' },
          'Successfull response',
        ),
      );
    });

    it('should pass errornous webhooks reponse', async () => {
      const fetch = jest
        .fn()
        .mockImplementation(() =>
          Promise.resolve(
            make_response(
              400,
              [['Content-Type', 'text']],
              'Errornous response',
            ),
          ),
        );
      const dispatcher = make_dispatcherWithFetch(fetch);

      const response = await dispatcher.dispatch(
        new WebhookBuilder()
          .activated()
          .withId(1)
          .withSecret('shh')
          .withPayloadUrl('http://localhost:300/webhook/test')
          .withEncoding(EncodingType.json)
          .build(),
        { message: 'some-message' },
      );

      expect(response).toEqual(
        new SuccessfullWebhookResponse(
          400,
          { 'Content-Type': 'text' },
          'Errornous response',
        ),
      );
    });
  });

  describe('retrying', () => {
    it('should retry if remote service errors', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.reject())
        .mockImplementationOnce(() => Promise.resolve(make_response(200)));
      const dispatcher = make_dispatcherWithFetch(fetch);

      await dispatcher.dispatch(
        new WebhookBuilder()
          .activated()
          .withId(1)
          .withSecret('shh')
          .withPayloadUrl('http://localhost:300/webhook/test')
          .withEncoding(EncodingType.json)
          .build(),
        { message: 'some-message' },
      );

      expect(fetch).toHaveBeenCalledTimes(2);
    });

    it('should retry if remote service respond with error code', async () => {
      const fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.resolve(make_response(400)))
        .mockImplementationOnce(() => Promise.resolve(make_response(200)));
      const dispatcher = make_dispatcherWithFetch(fetch);

      await dispatcher.dispatch(
        new WebhookBuilder()
          .activated()
          .withId(1)
          .withSecret('shh')
          .withPayloadUrl('http://localhost:300/webhook/test')
          .withEncoding(EncodingType.json)
          .build(),
        { message: 'some-message' },
      );

      expect(fetch).toHaveBeenCalledTimes(2);
    });

    it('should throw WenhookCouldNotBeDispatchError', async () => {
      const fetch = jest
        .fn()
        .mockImplementation(() =>
          Promise.reject(new Error('Error reaching remote server')),
        );
      const dispatcher = make_dispatcherWithFetch(fetch);

      const fn = dispatcher.dispatch(
        new WebhookBuilder()
          .activated()
          .withId(1)
          .withSecret('shh')
          .withPayloadUrl('http://localhost:300/webhook/test')
          .withEncoding(EncodingType.json)
          .build(),
        { message: 'some-message' },
      );

      await expect(fn).rejects.toThrow(WenhookCouldNotBeDispatchError);
    });
  });

  describe('logging', () => {
    it('should log successfull webhook execution', async () => {
      const fetch = jest
        .fn()
        .mockImplementation(() => Promise.resolve(make_response(200)));
      const logger = new ConsoleLogger();
      const log = jest.spyOn(logger, 'log');
      const dispatcher = make_dispatcherWithFetchAndLogger(fetch, logger);

      await dispatcher.dispatch(
        new WebhookBuilder()
          .activated()
          .withId(1)
          .withSecret('shh')
          .withPayloadUrl('http://localhost:300/webhook/test')
          .withEncoding(EncodingType.json)
          .build(),
        { message: 'some-message' },
      );

      expect(log).toHaveBeenCalled();
    });

    it('should log the thrown error', async () => {
      const fetch = jest
        .fn()
        .mockImplementation(() =>
          Promise.reject(new Error('Error connectin to remote server')),
        );
      const logger = new ConsoleLogger();
      const logError = jest.spyOn(logger, 'error');
      const dispatcher = make_dispatcherWithFetchAndLogger(fetch, logger);

      try {
        await dispatcher.dispatch(
          new WebhookBuilder()
            .activated()
            .withId(1)
            .withSecret('shh')
            .withPayloadUrl('http://localhost:300/webhook/test')
            .withEncoding(EncodingType.json)
            .build(),
          { message: 'some-message' },
        );
      } catch {}

      expect(logError).toHaveBeenCalled();
    });

    it('should log the status error code', async () => {
      const fetch = jest
        .fn()
        .mockImplementation(() => Promise.resolve(make_response(400)));
      const logger = new ConsoleLogger();
      const logError = jest.spyOn(logger, 'error');
      const dispatcher = make_dispatcherWithFetchAndLogger(fetch, logger);

      try {
        await dispatcher.dispatch(
          new WebhookBuilder()
            .activated()
            .withId(1)
            .withSecret('shh')
            .withPayloadUrl('http://localhost:300/webhook/test')
            .withEncoding(EncodingType.json)
            .build(),
          { message: 'some-message' },
        );
      } catch {}

      expect(logError).toHaveBeenCalled();
    });
  });
});
