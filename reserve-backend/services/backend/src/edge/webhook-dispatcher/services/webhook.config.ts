/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imorts
import { Injectable } from '@nestjs/common';
//#endregion

@Injectable()
export class WebhookConfig {
  public constructor(
    public readonly signatureHeader: string = 'X-Reserve-Signature',
    public readonly retries: number = 2,
  ) {}
}
