/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import crypto from 'crypto';
import { Injectable } from '@nestjs/common';
//#endregion

@Injectable()
export class SignatureService {
  public sign(content: string | Buffer, secret: string): string {
    return crypto.createHmac('sha1', secret).update(content).digest('hex');
  }
}
