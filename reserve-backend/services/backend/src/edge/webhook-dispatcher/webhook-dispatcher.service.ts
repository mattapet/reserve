/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Response } from 'node-fetch';
import { Injectable, Inject } from '@nestjs/common';
import { Logger } from '@rsaas/commons/lib/logger';
import { AnyObject } from '@rsaas/util/lib/AnyObject';
import { HttpService, encodeHttpContent } from '@rsaas/commons/lib/http';
import { Encoding } from '@rsaas/commons/lib/http/types/encoding.type';
import { NestLogger } from '@rsaas/commons/lib/logger/nest/logger.service';

import { EncodingType, Webhook } from '../webhook';

import { SignatureService } from './services/signature.service';
import { WebhookConfig } from './services/webhook.config';
import { WenhookCouldNotBeDispatchError } from './webhook-dispatcher.errors';
import { SuccessfullWebhookResponse } from './webhook-response.value';
//#endregion

@Injectable()
export class WebhookDispatcherService {
  public constructor(
    private readonly config: WebhookConfig,
    @Inject(NestLogger)
    private readonly logger: Logger,
    private readonly httpService: HttpService,
    private readonly signer: SignatureService,
  ) {
    this.logger.setContext('WebhookDispatcherService');
  }

  public async dispatch<Payload extends AnyObject>(
    webhook: Webhook,
    payload: Payload,
  ): Promise<SuccessfullWebhookResponse> {
    try {
      const encoding = this.getHttpEncoding(webhook.getEncoding());
      const content = encodeHttpContent(payload, encoding);
      const signature = this.signer.sign(content, webhook.getSecret());

      const response = await this.httpService
        .post(webhook.getPayloadUrl())
        .set('Content-Type', encoding)
        .retry(this.config.retries)
        .set(this.config.signatureHeader, signature)
        .send(content);

      this.logResponse(response, webhook);
      return this.translateResponse(response);
    } catch (error) {
      this.logger.error(
        `'Error dispatching webhook with id '${webhook.getId()}' to url '${webhook.getPayloadUrl()}'`,
        error.stack,
      );

      throw new WenhookCouldNotBeDispatchError(error);
    }
  }

  private logResponse(response: Response, webhook: Webhook): void {
    if (response.status < 400) {
      this.logger.log(
        `Webhook with id '${webhook.getId()}' and url ${webhook.getPayloadUrl()} responded ${
          response.status
        }`,
      );
    } else {
      this.logger.error(
        `Webhook with id '${webhook.getId()}' and url ${webhook.getPayloadUrl()} responded ${
          response.status
        }`,
      );
    }
  }

  private getHttpEncoding(encoding: EncodingType): Encoding {
    switch (encoding) {
      case EncodingType.json:
        return Encoding.json;
      case EncodingType.urlencoded:
        return Encoding.urlencoded;
    }
  }

  private async translateResponse(
    response: Response,
  ): Promise<SuccessfullWebhookResponse> {
    const headers = {};
    // @ts-ignore
    for (const [key, value] of response.headers.entries()) {
      headers[key] = value;
    }

    return new SuccessfullWebhookResponse(
      response.status,
      headers,
      await response.text(),
    );
  }
}
