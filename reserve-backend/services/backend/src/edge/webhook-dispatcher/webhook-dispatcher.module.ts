/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons/lib/commons.module';

import { WebhookModule } from '../webhook/webhook.module';

import { WebhookDispatcherService } from './webhook-dispatcher.service';
import { WebhookConfig } from './services/webhook.config';
import { SignatureService } from './services/signature.service';
//#endregion

@Module({
  imports: [CommonsModule, WebhookModule],
  providers: [
    WebhookDispatcherService,
    {
      provide: WebhookConfig,
      useValue: new WebhookConfig(),
    },
    SignatureService,
  ],
  exports: [WebhookDispatcherService],
})
export class WebhookDispatcherModule {}
