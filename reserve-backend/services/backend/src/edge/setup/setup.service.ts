/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { uuid } from '@rsaas/util';

import { Identity } from '../../user-access/domain/identity';
import {
  UserService,
  User,
  AuthorityAuthorizationService,
  AuthService,
  Authority,
} from '../../user-access';
import {
  WorkspaceRequest,
  WorkspaceRequestService,
  WorkspaceName,
  WorkspaceOwner,
} from '../../workspace-management';
import { VerificationService } from '../verfication';
import { IdentityBuilder } from '../../user-access/__tests__/identity/builders/identity.builder';
//#endregion

@Injectable()
export class SetupService {
  public constructor(
    private readonly requestService: WorkspaceRequestService,
    private readonly userService: UserService,
    private readonly authService: AuthService,
    private readonly authorizationService: AuthorityAuthorizationService,
    private readonly verificationService: VerificationService,
  ) {}

  public async verify(
    redirectUri: string,
    workspaceId: string,
    email: string,
  ): Promise<void> {
    const identity = new Identity(uuid(), workspaceId, email);
    await this.verificationService.verify(redirectUri, identity);
  }

  public async completeEmailVerification(
    request: WorkspaceRequest,
    workspaceId: string,
    workspaceName: WorkspaceName,
    email: string,
    password: string,
  ): Promise<void> {
    const owner = new User();
    const identity = new Identity(uuid(), workspaceId, email);
    await this.userService.createOwner(owner, identity);

    await this.authService.setPassword(owner, password);
    await this.requestService.open(request, {
      workspaceId,
      workspaceName,
      requestedBy: WorkspaceOwner.from(owner),
    });
  }

  public async completeAuthorityAuthentication(
    request: WorkspaceRequest,
    workspaceId: string,
    workspaceName: WorkspaceName,
    state: string,
    code: string,
    authority: Authority,
  ): Promise<void> {
    const authorizedUser = await this.authorizationService.authorizeUser(
      state,
      code,
      authority,
    );
    const identity = new IdentityBuilder()
      .withUserId(uuid())
      .withWorkspaceId(workspaceId)
      .withEmail(authorizedUser.email)
      .withFirstName(authorizedUser.firstName)
      .withLastName(authorizedUser.lastName)
      .build();
    const owner = new User();
    await this.userService.createOwner(owner, identity);
    await this.requestService.open(request, {
      workspaceId,
      workspaceName,
      requestedBy: WorkspaceOwner.from(owner),
    });
  }
}
