/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Address } from 'nodemailer/lib/mailer';
import { Identity } from '../../user-access/domain/identity';
import { InvalidEmailError } from './email.errors';
//#endregion

export class Email {
  public constructor(
    private readonly to: Identity[],
    private readonly cc: Identity[],
    private readonly bcc: Identity[],
    private readonly from: string,
    private readonly subject: string,
    private readonly content: string,
    private readonly messageId?: string,
    private readonly inReplyTo?: string,
  ) {
    this.validate();
  }

  public getFrom(): string {
    return this.from;
  }

  public getSubject(): string {
    return this.subject;
  }

  public getTo(): Address[] {
    return this.to.map(this.mapIdentityToAddress);
  }

  public getCC(): Address[] {
    return this.cc.map(this.mapIdentityToAddress);
  }

  public getBCC(): Address[] {
    return this.bcc.map(this.mapIdentityToAddress);
  }

  public getContent(): string {
    return this.content;
  }

  public getMessageId(): string | undefined {
    return this.messageId;
  }

  public getInReplyTo(): string | undefined {
    return this.inReplyTo;
  }

  private mapIdentityToAddress(identity: Identity): Address {
    return { address: identity.getEmail(), name: identity.getFullName() };
  }

  private validate() {
    if (this.to.length === 0) {
      throw new InvalidEmailError('Email must have at least one addresee');
    }

    if (this.from.length === 0) {
      throw new InvalidEmailError('From address cannot be empty');
    }

    if (this.content.length === 0) {
      throw new InvalidEmailError('Content cannot be empty');
    }
  }
}
