/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import nodemailer, { Transporter } from 'nodemailer';
import { Email } from './email.entity';
//#endregion

const EMAIL_HOST = process.env.EMAIL_HOST;
const EMAIL_PORT = process.env.EMAIL_PORT
  ? parseInt(process.env.EMAIL_PORT, 10)
  : 25;

export class EmailService {
  public constructor(
    private readonly transporter: Transporter = nodemailer.createTransport({
      host: EMAIL_HOST,
      port: EMAIL_PORT,
    }),
  ) {}

  public async sendPlainText(email: Email): Promise<void> {
    if (process.env.NODE_ENV === 'test') {
      return;
    }

    const info = await this.transporter.sendMail({
      messageId: email.getMessageId(),
      inReplyTo: email.getInReplyTo(),
      text: email.getContent(),
      from: email.getFrom(),
      to: email.getTo(),
      subject: email.getSubject(),
      cc: email.getCC(),
      bcc: email.getBCC(),
    });

    if (process.env.NODE_ENV !== 'production') {
      // tslint:disable:no-console
      console.info(nodemailer.getTestMessageUrl(info));
    }
  }
}
