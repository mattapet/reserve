/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';

import { Email } from '../email.entity';
import { Identity } from '../../../user-access/domain/identity';
//#endregion

interface EmailProperties {
  readonly messageId?: string;
  readonly inReplyTo?: string;
  readonly to: Identity[];
  readonly cc: Identity[];
  readonly bcc: Identity[];
  readonly from: string;
  readonly subject: string;
  readonly content: string;
}

export class EmailBuilder {
  private messageId?: string;
  private inReplyTo?: string;
  private to: Identity[] = [];
  private cc: Identity[] = [];
  private bcc: Identity[] = [];
  private subject?: string;
  private from?: string;
  private content?: string;

  public withMessageId(messageId: string): EmailBuilder {
    return this.copy({ messageId });
  }

  public withInReplyTo(inReplyTo: string): EmailBuilder {
    return this.copy({ inReplyTo });
  }

  public withSubject(subject: string): EmailBuilder {
    this.subject = subject;
    return this;
  }

  public withSender(sender: string): EmailBuilder {
    return this.copy({ from: sender });
  }

  public addingAddressee(addressee: Identity): EmailBuilder {
    return this.copy({ to: [...this.to, addressee] });
  }

  public addingCopyAddressee(copyAddressee: Identity): EmailBuilder {
    return this.copy({ cc: [...this.cc, copyAddressee] });
  }

  public addingBCopyAddressee(bCopyAddressee: Identity): EmailBuilder {
    return this.copy({ bcc: [...this.bcc, bCopyAddressee] });
  }

  public appendingContent(content: string): EmailBuilder {
    return this.copy({ content: (this.content ?? '') + content });
  }

  public withContent(content: string): EmailBuilder {
    return this.copy({ content });
  }

  public copy(newValues?: Partial<EmailProperties>): EmailBuilder {
    const builder = new EmailBuilder();
    builder.messageId = newValues?.messageId ?? this.messageId;
    builder.inReplyTo = newValues?.inReplyTo ?? this.inReplyTo;
    builder.to = newValues?.to ?? this.to;
    builder.cc = newValues?.cc ?? this.cc;
    builder.bcc = newValues?.bcc ?? this.bcc;
    builder.subject = newValues?.subject ?? this.subject;
    builder.from = newValues?.from ?? this.from;
    builder.content = newValues?.content ?? this.content;
    return builder;
  }

  public build(): Email {
    return new Email(
      this.to,
      this.cc,
      this.bcc,
      notNull(this.from, 'sender missing'),
      notNull(this.subject, 'Empty subject'),
      notNull(this.content, 'email content missing'),
      this.messageId,
      this.inReplyTo,
    );
  }
}
