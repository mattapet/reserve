/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserId, User } from '../../../user-access';
import { IllegalOperationError } from '../workspace/workspace.errors';
//#endregion

export class WorkspaceUser {
  public constructor(private readonly userId: string) {}

  public getId(): UserId {
    return this.userId;
  }

  public equals(other: WorkspaceUser): boolean {
    return this.userId === other.userId;
  }

  public static from(user: User): WorkspaceUser {
    if (user.isDeleted()) {
      throw new IllegalOperationError(
        `Workspace user with id '${user.getId()}' is deleted.`,
      );
    }
    return new WorkspaceUser(user.getId());
  }
}
