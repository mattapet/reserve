/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { User } from '../../../user-access';
import { IllegalOperationError } from '../workspace/workspace.errors';
import { WorkspaceUser } from './workspace-user.entity';
//#endregion

export class WorkspaceOwner extends WorkspaceUser {
  public static from(user: User): WorkspaceOwner {
    if (user.isNotOwner()) {
      throw new IllegalOperationError(
        `User with id '${user.getId()}' is not a workspace owner.`,
      );
    }

    return new WorkspaceOwner(user.getId());
  }
}
