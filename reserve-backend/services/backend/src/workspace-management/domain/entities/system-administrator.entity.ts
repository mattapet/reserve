/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ConsoleAccount } from '../../../console/domain/console-account';
//#endregion

export class SystemAdministrator {
  public constructor(private readonly username: string) {}

  public getUsername(): string {
    return this.username;
  }

  public equals(other: SystemAdministrator): boolean {
    return this.username === other.username;
  }

  public static from(account: ConsoleAccount): SystemAdministrator {
    return new SystemAdministrator(account.getUsername());
  }
}
