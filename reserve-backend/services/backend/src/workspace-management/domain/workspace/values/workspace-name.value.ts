/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IllegalWorkspaceNameError } from '../workspace.errors';
//#endregion

export class WorkspaceName {
  public constructor(private readonly name: string) {
    this.validate();
  }

  public get(): string {
    return this.name;
  }

  public equals(other: WorkspaceName): boolean {
    return this.name === other.name;
  }

  private validate() {
    if (!/^[a-z0-9_-]{4,32}$/.test(this.name)) {
      throw new IllegalWorkspaceNameError(
        `Workspace name '${this.name}' does not match allowed format`,
      );
    }
  }
}
