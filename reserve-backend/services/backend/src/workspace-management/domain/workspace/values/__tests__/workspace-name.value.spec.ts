/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceName } from '../workspace-name.value';
import { IllegalWorkspaceNameError } from '../../workspace.errors';
//#endregion

describe('worksapce-name.value', () => {
  it('should create a valid workspace name', () => {
    const name = new WorkspaceName('test');

    expect(name.get()).toBe('test');
  });

  it('should throw IllegalWorkspaceNameError if workspace contains invalid characaters', () => {
    const fn = () => new WorkspaceName('test?');

    expect(fn).toThrow(IllegalWorkspaceNameError);
  });

  it('should throw IllegalWorkspaceNameError if name is too short', () => {
    const fn = () => new WorkspaceName('api');

    expect(fn).toThrow(IllegalWorkspaceNameError);
  });

  it('should throw IllegalWorkspaceNameError if name is too long', () => {
    const fn = () =>
      new WorkspaceName('abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz');

    expect(fn).toThrow(IllegalWorkspaceNameError);
  });
});
