/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '@rsaas/commons/lib/events/event.entity';
//#endregion

export enum WorkspaceEventType {
  created = 'workspace.created',
  nameSet = 'workspace.name_set',
  ownershipTransferred = 'workspace.ownership_transferred',
  deleted = 'workspace.deleted',
}

export interface WorkspaceCreatedEvent extends Event {
  readonly type: WorkspaceEventType.created;
  readonly payload: {
    readonly name: string;
    readonly createdBy: string;
  };
}

export interface WorkspaceNameSetEvent extends Event {
  readonly type: WorkspaceEventType.nameSet;
  readonly payload: {
    readonly name: string;
    readonly changedBy: string;
  };
}

export interface WorkspaceOwnershipTransferredEvent extends Event {
  readonly type: WorkspaceEventType.ownershipTransferred;
  readonly payload:
    | {
        readonly transferredTo: string;
        readonly transferredFrom: string;
        readonly changedByOwner: string;
      }
    | {
        readonly transferredTo: string;
        readonly transferredFrom: string;
        readonly changedByAdministrator: string;
      };
}

export interface WorkspaceDeletedEvent extends Event {
  readonly type: WorkspaceEventType.deleted;
  readonly payload: {
    readonly changedBy: string;
  };
}

export type WorkspaceEvent =
  | WorkspaceCreatedEvent
  | WorkspaceNameSetEvent
  | WorkspaceOwnershipTransferredEvent
  | WorkspaceDeletedEvent;
