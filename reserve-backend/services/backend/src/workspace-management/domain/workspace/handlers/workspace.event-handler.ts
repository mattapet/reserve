/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EventHandler, AggregateEventHandler } from '@rsaas/commons/lib/events';
import {
  WorkspaceEvent,
  WorkspaceEventType,
  WorkspaceCreatedEvent,
  WorkspaceNameSetEvent,
  WorkspaceOwnershipTransferredEvent,
} from '../workspace.event';
import { Workspace } from '../workspace.entity';
import { WorkspaceName } from '../values';
import { WorkspaceOwner } from '../../entities';
//#endregion

export class WorkspaceEventHandler extends AggregateEventHandler<
  Workspace,
  WorkspaceEvent
> {
  @EventHandler(WorkspaceEventType.created)
  public handleCreated(
    aggregate: Workspace,
    event: WorkspaceCreatedEvent,
  ): Workspace {
    aggregate.setId(event.aggregateId);
    aggregate.setName(new WorkspaceName(event.payload.name));
    aggregate.setOwner(new WorkspaceOwner(event.payload.createdBy));
    return aggregate;
  }

  @EventHandler(WorkspaceEventType.nameSet)
  public handleNameSet(
    aggregate: Workspace,
    event: WorkspaceNameSetEvent,
  ): Workspace {
    aggregate.setName(new WorkspaceName(event.payload.name));
    return aggregate;
  }

  @EventHandler(WorkspaceEventType.ownershipTransferred)
  public handleOwnershipTransferred(
    aggregate: Workspace,
    event: WorkspaceOwnershipTransferredEvent,
  ): Workspace {
    aggregate.setOwner(new WorkspaceOwner(event.payload.transferredTo));
    return aggregate;
  }

  @EventHandler(WorkspaceEventType.deleted)
  public handleDeleted(aggregate: Workspace): Workspace {
    aggregate.delete();
    return aggregate;
  }
}
