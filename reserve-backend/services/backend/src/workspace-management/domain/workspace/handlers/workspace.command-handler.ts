/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import {
  AggregateCommandHandler,
  CommandHandler,
} from '@rsaas/commons/lib/events';
import {
  WorkspaceCommand,
  WorkspaceCommandType,
  DeleteWorkspaceCommand,
  TransferOwnershipWorkspaceCommand,
  SetNameWorkspaceCommand,
  CreateWorkspaceCommand,
} from '../workspace.command';
import { WorkspaceEvent, WorkspaceEventType } from '../workspace.event';
import { Workspace } from '../workspace.entity';
import { IllegalOperationError } from '../workspace.errors';
import { WorkspaceOwner } from '../../entities';
//#endregion

@Injectable()
export class WorkspaceCommandHandler extends AggregateCommandHandler<
  Workspace,
  WorkspaceEvent,
  WorkspaceCommand
> {
  @CommandHandler(WorkspaceCommandType.create)
  public executeCreate(
    aggregate: Workspace,
    { rowId, aggregateId, payload }: CreateWorkspaceCommand,
  ): WorkspaceEvent[] {
    if (aggregate.hasBeenCreated()) {
      return [];
    }
    return [
      {
        type: WorkspaceEventType.created,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          name: payload.name.get(),
          createdBy: payload.createdBy.getId(),
        },
      },
    ];
  }

  @CommandHandler(WorkspaceCommandType.setName)
  public executeSetName(
    aggregate: Workspace,
    { rowId, aggregateId, payload }: SetNameWorkspaceCommand,
  ): WorkspaceEvent[] {
    if (aggregate.isDeleted()) {
      throw new IllegalOperationError('Cannot modify deleted workspace.');
    }
    return [
      {
        type: WorkspaceEventType.nameSet,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          name: payload.name.get(),
          changedBy: payload.changedBy.getId(),
        },
      },
    ];
  }

  @CommandHandler(WorkspaceCommandType.transferOwnership)
  public executeTransferOwnership(
    aggregate: Workspace,
    { rowId, aggregateId, payload }: TransferOwnershipWorkspaceCommand,
  ): WorkspaceEvent[] {
    if (aggregate.isDeleted()) {
      throw new IllegalOperationError('Cannot modify deleted workspace.');
    }
    if (aggregate.isOwnedBy(payload.transferredTo)) {
      return [];
    }

    return [
      {
        type: WorkspaceEventType.ownershipTransferred,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload:
          payload.changedBy instanceof WorkspaceOwner
            ? {
                transferredFrom: payload.transferredFrom.getId(),
                transferredTo: payload.transferredTo.getId(),
                changedByOwner: payload.changedBy.getId(),
              }
            : {
                transferredFrom: payload.transferredFrom.getId(),
                transferredTo: payload.transferredTo.getId(),
                // tslint:disable-next-line:max-line-length
                changedByAdministrator: payload.changedBy.getUsername(),
              },
      },
    ];
  }

  @CommandHandler(WorkspaceCommandType.delete)
  public executeDelete(
    aggregate: Workspace,
    { rowId, aggregateId, payload }: DeleteWorkspaceCommand,
  ): WorkspaceEvent[] {
    if (aggregate.isDeleted()) {
      return [];
    }
    return [
      {
        type: WorkspaceEventType.deleted,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          changedBy: payload.changedBy.getId(),
        },
      },
    ];
  }
}
