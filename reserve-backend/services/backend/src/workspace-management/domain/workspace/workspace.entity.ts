/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';

import { WorkspaceName } from './values';
import { WorkspaceOwner, WorkspaceUser } from '../entities';
import { Type } from 'class-transformer';
//#endregion

export class Workspace {
  @Type(() => WorkspaceName)
  private name?: WorkspaceName;

  @Type(() => WorkspaceOwner)
  private owner?: WorkspaceOwner;

  public constructor(
    private id?: string,
    name?: WorkspaceName,
    owner?: WorkspaceOwner,
  ) {
    this.name = name;
    this.owner = owner;
  }

  private deleted: boolean = false;

  public getId(): string {
    return notNull(this.id, 'Cannot access ID of uninitialized workspace');
  }

  public getName(): WorkspaceName {
    return notNull(this.name, 'Cannot access name of uninitialized workspace');
  }

  public getOwner(): WorkspaceOwner {
    return notNull(this.owner, 'Cannot access name of uninitialized workspace');
  }

  public hasBeenCreated(): boolean {
    return !!this.id && !!this.name && !!this.owner;
  }

  public hasNotBeenCreated(): boolean {
    return !this.hasBeenCreated();
  }

  public isOwnedBy(owner: WorkspaceOwner | WorkspaceUser): boolean {
    return this.getOwner().equals(owner);
  }

  public isNotOwnedBy(user: WorkspaceOwner): boolean {
    return !this.isOwnedBy(user);
  }

  public isDeleted(): boolean {
    return this.deleted;
  }

  public isNotDeleted(): boolean {
    return !this.deleted;
  }

  public setId(id: string): void {
    this.id = id;
  }

  public setName(name: WorkspaceName): void {
    this.name = name;
  }

  public setOwner(owner: WorkspaceOwner): void {
    this.owner = owner;
  }

  public delete(): void {
    this.deleted = true;
  }
}
