/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';
import { AggregateRepository } from '@rsaas/commons/lib/events';

import { Workspace } from './workspace.entity';
import { OwnershipTransferService } from './services/ownership-transfer.service';
import { WorkspaceCommandType, WorkspaceCommand } from './workspace.command';
import {
  WorkspaceNotFoundError,
  WorkspaceNameCollisionError,
  IllegalOperationError,
} from './workspace.errors';
import { WorkspaceName } from './values';
import {
  SystemAdministrator,
  WorkspaceOwner,
  WorkspaceUser,
} from '../entities';
import { AGGREGATE_NAME, WORKSPACE_REPOSITORY } from './constants';
import { WorkspaceEvent } from './workspace.event';
//#endregion

@Injectable()
export class WorkspaceService {
  private readonly ownershipTransferService: OwnershipTransferService;
  public constructor(
    @Inject(WORKSPACE_REPOSITORY)
    private readonly repository: AggregateRepository<
      WorkspaceCommand,
      WorkspaceEvent,
      Workspace
    >,
  ) {
    this.ownershipTransferService = new OwnershipTransferService(
      this.repository,
    );
  }

  public get aggregateName(): string {
    return AGGREGATE_NAME;
  }

  // - MARK: Queries

  public async getAll(): Promise<Workspace[]> {
    const workspaces = await this.repository.getAll(this.aggregateName);
    return workspaces.filter((workspace) => workspace.isNotDeleted());
  }

  public async getById(workspaceId: string): Promise<Workspace> {
    const workspace = await this.repository.getById(
      this.aggregateName,
      workspaceId,
    );
    if (workspace.hasNotBeenCreated()) {
      throw new WorkspaceNotFoundError();
    }
    return workspace;
  }

  public async getByName(name: WorkspaceName): Promise<Workspace> {
    const item = await this.findByName(name);
    if (!item || item.isDeleted()) {
      throw new WorkspaceNotFoundError(
        `Workspace with name ${name} does not exist.`,
      );
    }
    return item;
  }

  public async findByName(name: WorkspaceName): Promise<Workspace | undefined> {
    const workspaces = await this.repository.getAll(this.aggregateName);
    const result = workspaces
      .filter((workspace) => workspace.isNotDeleted())
      .filter((workspace) => workspace.getName().equals(name));
    return result.length ? result[0]! : undefined;
  }

  // - MARK: Commands

  public async create(
    workspace: Workspace,
    workspaceId: string,
    name: WorkspaceName,
    createdBy: WorkspaceOwner,
  ): Promise<void> {
    if ((await this.findByName(name)) != null) {
      throw new WorkspaceNameCollisionError(
        `Workspace with name '${name.get()}' already exists.`,
      );
    }

    await this.repository.execute(
      this.makeCreateCommand(workspaceId, name, createdBy),
      workspace,
    );
  }

  public async transferOwnership(
    workspace: Workspace,
    to: WorkspaceUser,
    changedBy: WorkspaceOwner | SystemAdministrator,
  ): Promise<void> {
    await this.ownershipTransferService.transfer(workspace, to, changedBy);
  }

  public async delete(
    workspace: Workspace,
    changedBy: WorkspaceOwner,
  ): Promise<void> {
    if (workspace.isNotOwnedBy(changedBy)) {
      throw new IllegalOperationError(
        'Only workspace owner can delete the workspace',
      );
    }

    await this.repository.execute(
      this.makeDeleteCommand(workspace, changedBy),
      workspace,
    );
  }

  private makeCreateCommand(
    workspaceId: string,
    name: WorkspaceName,
    createdBy: WorkspaceOwner,
  ): WorkspaceCommand {
    return {
      type: WorkspaceCommandType.create,
      rowId: this.aggregateName,
      aggregateId: workspaceId,
      payload: { name, createdBy },
    };
  }

  private makeDeleteCommand(
    workspace: Workspace,
    changedBy: WorkspaceOwner,
  ): WorkspaceCommand {
    return {
      type: WorkspaceCommandType.delete,
      rowId: this.aggregateName,
      aggregateId: workspace.getId(),
      payload: { changedBy },
    };
  }
}
