/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  SystemAdministrator,
  WorkspaceOwner,
  WorkspaceUser,
} from '../../entities';
import { WorkspaceRepository } from '../../../application/workspace/workspace.repository';
import { Workspace } from '../workspace.entity';
import { IllegalOperationError } from '../workspace.errors';
import { WorkspaceCommand, WorkspaceCommandType } from '../workspace.command';
//#endregion

export class OwnershipTransferService {
  public constructor(private readonly repository: WorkspaceRepository) {}

  public async transfer(
    workspace: Workspace,
    to: WorkspaceUser,
    changedBy: WorkspaceOwner | SystemAdministrator,
  ): Promise<void> {
    if (changedBy instanceof WorkspaceOwner) {
      await this.giveUpOwnership(workspace, changedBy, to);
    } else {
      await this.reassignOwnership(workspace, to, changedBy);
    }
  }

  private async giveUpOwnership(
    workspace: Workspace,
    changedBy: WorkspaceOwner,
    to: WorkspaceUser,
  ) {
    if (workspace.isNotOwnedBy(changedBy)) {
      throw new IllegalOperationError(
        'Only owner can give up ownership of the workspace',
      );
    }
    await this.repository.execute(
      this.makeTransferOwnershipCommand(workspace, to, changedBy),
      workspace,
    );
  }

  private async reassignOwnership(
    workspace: Workspace,
    to: WorkspaceUser,
    changedBy: SystemAdministrator,
  ) {
    await this.repository.execute(
      this.makeTransferOwnershipCommand(workspace, to, changedBy),
      workspace,
    );
  }

  private makeTransferOwnershipCommand(
    workspace: Workspace,
    to: WorkspaceUser,
    changedBy: WorkspaceOwner | SystemAdministrator,
  ): WorkspaceCommand {
    return {
      type: WorkspaceCommandType.transferOwnership,
      aggregateId: workspace.getId(),
      rowId: 'WorkspaceAggregate',
      payload: {
        transferredFrom: workspace.getOwner(),
        transferredTo: to,
        changedBy,
      },
    };
  }
}
