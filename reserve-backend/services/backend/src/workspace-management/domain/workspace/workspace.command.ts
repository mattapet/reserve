/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '@rsaas/commons/lib/events/command';
import { WorkspaceName } from './values';
import {
  WorkspaceOwner,
  SystemAdministrator,
  WorkspaceUser,
} from '../entities';
//#endregion

export enum WorkspaceCommandType {
  create = 'create',
  setName = 'setName',
  transferOwnership = 'transfer_ownership',
  delete = 'delete',
}

export interface CreateWorkspaceCommand extends Command {
  readonly type: WorkspaceCommandType.create;
  readonly payload: {
    readonly name: WorkspaceName;
    readonly createdBy: WorkspaceOwner;
  };
}

export interface SetNameWorkspaceCommand extends Command {
  readonly type: WorkspaceCommandType.setName;
  readonly payload: {
    readonly name: WorkspaceName;
    readonly changedBy: WorkspaceOwner;
  };
}

export interface TransferOwnershipWorkspaceCommand extends Command {
  readonly type: WorkspaceCommandType.transferOwnership;
  readonly payload: {
    readonly transferredTo: WorkspaceUser;
    readonly transferredFrom: WorkspaceOwner;
    readonly changedBy: WorkspaceOwner | SystemAdministrator;
  };
}

export interface DeleteWorkspaceCommand extends Command {
  readonly type: WorkspaceCommandType.delete;
  readonly payload: {
    readonly changedBy: WorkspaceOwner;
  };
}

export type WorkspaceCommand =
  | CreateWorkspaceCommand
  | SetNameWorkspaceCommand
  | TransferOwnershipWorkspaceCommand
  | DeleteWorkspaceCommand;
