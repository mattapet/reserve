/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceName } from '../../workspace/values';
import { WorkspaceOwner } from '../../entities';
//#endregion

export class OpenWorkspaceRequestParams {
  public constructor(
    public readonly workspaceId: string,
    public readonly workspaceName: WorkspaceName,
    public readonly requestedBy: WorkspaceOwner,
  ) {}
}
