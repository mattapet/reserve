/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '@rsaas/commons/lib/events/command';

import { WorkspaceName } from '../workspace/values';
import { SystemAdministrator, WorkspaceOwner } from '../entities';
//#endregion

export enum WorkspaceRequestCommandType {
  open = 'workspace.request.open',
  confirm = 'workspace.request.confirm',
  decline = 'workspace.request.decline',
}

export interface OpenWorkspaceRequestCommand extends Command {
  readonly type: WorkspaceRequestCommandType.open;
  readonly payload: {
    readonly workspaceName: WorkspaceName;
    readonly requestedBy: WorkspaceOwner;
  };
}

export interface ConfirmWorkspaceRequestCommand extends Command {
  readonly type: WorkspaceRequestCommandType.confirm;
  readonly payload: {
    readonly confirmedBy: SystemAdministrator;
  };
}

export interface DeclinedWorkspaceRequestCommand extends Command {
  readonly type: WorkspaceRequestCommandType.decline;
  readonly payload: {
    readonly declinedBy: SystemAdministrator;
  };
}

export type WorkspaceRequestCommand =
  | OpenWorkspaceRequestCommand
  | ConfirmWorkspaceRequestCommand
  | DeclinedWorkspaceRequestCommand;
