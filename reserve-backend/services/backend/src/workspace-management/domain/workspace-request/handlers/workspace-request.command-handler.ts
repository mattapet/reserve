/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  AggregateCommandHandler,
  CommandHandler,
} from '@rsaas/commons/lib/events';
import { WorkspaceRequest } from '../workspace-request.entity';
import {
  WorkspaceRequestCommand,
  WorkspaceRequestCommandType,
  DeclinedWorkspaceRequestCommand,
  ConfirmWorkspaceRequestCommand,
  OpenWorkspaceRequestCommand,
} from '../workspace-request.command';
import {
  WorkspaceRequestEvent,
  WorkspaceRequestEventType,
} from '../workspace-request.event';
//#endregion

export class WorkspaceRequestCommandHandler extends AggregateCommandHandler<
  WorkspaceRequest,
  WorkspaceRequestEvent,
  WorkspaceRequestCommand
> {
  @CommandHandler(WorkspaceRequestCommandType.open)
  public executeOpen(
    aggregate: WorkspaceRequest,
    { rowId, aggregateId, payload }: OpenWorkspaceRequestCommand,
  ): WorkspaceRequestEvent[] {
    if (aggregate.hasBeenCreated()) {
      return [];
    }
    return [
      {
        type: WorkspaceRequestEventType.opened,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          workspaceName: payload.workspaceName.get(),
          requestedBy: payload.requestedBy.getId(),
        },
      },
    ];
  }

  @CommandHandler(WorkspaceRequestCommandType.confirm)
  public executeConfirm(
    aggregate: WorkspaceRequest,
    { rowId, aggregateId, payload }: ConfirmWorkspaceRequestCommand,
  ): WorkspaceRequestEvent[] {
    if (aggregate.isConfirmed()) {
      return [];
    }
    return [
      {
        type: WorkspaceRequestEventType.confirmed,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          confirmedBy: payload.confirmedBy.getUsername(),
        },
      },
    ];
  }

  @CommandHandler(WorkspaceRequestCommandType.decline)
  public executeDecline(
    aggregate: WorkspaceRequest,
    { rowId, aggregateId, payload }: DeclinedWorkspaceRequestCommand,
  ): WorkspaceRequestEvent[] {
    if (aggregate.isDeclined()) {
      return [];
    }
    return [
      {
        type: WorkspaceRequestEventType.declined,
        rowId,
        aggregateId,
        timestamp: new Date(Date.now()),
        payload: {
          declinedBy: payload.declinedBy.getUsername(),
        },
      },
    ];
  }
}
