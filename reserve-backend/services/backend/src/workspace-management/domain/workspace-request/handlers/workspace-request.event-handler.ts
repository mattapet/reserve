/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AggregateEventHandler, EventHandler } from '@rsaas/commons/lib/events';
import { WorkspaceRequest } from '../workspace-request.entity';
import {
  WorkspaceRequestEvent,
  WorkspaceRequestEventType,
  WorkspaceRequestOpenedEvent,
  WorkspaceRequestConfirmedEvent,
  WorkspaceRequestDeclinedEvent,
} from '../workspace-request.event';

import { WorkspaceName } from '../../workspace/values';
import { WorkspaceOwner } from '../../entities';
//#endregion

export class WorkspaceRequestEventHandler extends AggregateEventHandler<
  WorkspaceRequest,
  WorkspaceRequestEvent
> {
  @EventHandler(WorkspaceRequestEventType.opened)
  public handleWorkspaceOpened(
    aggregate: WorkspaceRequest,
    event: WorkspaceRequestOpenedEvent,
  ): WorkspaceRequest {
    aggregate.setId(event.aggregateId);
    aggregate.setWorkspaceName(new WorkspaceName(event.payload.workspaceName));
    aggregate.setOwner(new WorkspaceOwner(event.payload.requestedBy));
    aggregate.setOpenedAt(event.timestamp);
    aggregate.open();
    return aggregate;
  }

  @EventHandler(WorkspaceRequestEventType.confirmed)
  public handleWorkspaceConfirmed(
    aggregate: WorkspaceRequest,
    event: WorkspaceRequestConfirmedEvent,
  ): WorkspaceRequest {
    aggregate.confirm();
    aggregate.setClosedAt(event.timestamp);
    return aggregate;
  }

  @EventHandler(WorkspaceRequestEventType.declined)
  public handleWorkspaceDeclined(
    aggregate: WorkspaceRequest,
    event: WorkspaceRequestDeclinedEvent,
  ): WorkspaceRequest {
    aggregate.decline();
    aggregate.setClosedAt(event.timestamp);
    return aggregate;
  }
}
