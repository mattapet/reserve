/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '@rsaas/commons/lib/events';

import { UserId } from '../../../user-access';
//#endregion

export enum WorkspaceRequestEventType {
  opened = 'workspace.request.opened',
  confirmed = 'workspace.request.confirmed',
  declined = 'workspace.request.declined',
}

export interface WorkspaceRequestOpenedEvent extends Event {
  readonly type: WorkspaceRequestEventType.opened;
  readonly payload: {
    readonly workspaceName: string;
    readonly requestedBy: UserId;
  };
}

export interface WorkspaceRequestConfirmedEvent extends Event {
  readonly type: WorkspaceRequestEventType.confirmed;
  readonly payload: {
    readonly confirmedBy: string; // Console Account Username
  };
}

export interface WorkspaceRequestDeclinedEvent extends Event {
  readonly type: WorkspaceRequestEventType.declined;
  readonly payload: {
    readonly declinedBy: string; // Console Account Username
  };
}

export type WorkspaceRequestEvent =
  | WorkspaceRequestOpenedEvent
  | WorkspaceRequestConfirmedEvent
  | WorkspaceRequestDeclinedEvent;
