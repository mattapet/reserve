/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';
import { Identifiable } from '@rsaas/util/lib/identifiable.interface';
import { Type } from 'class-transformer';

import { WorkspaceRequestState } from './workspace-request-state.type';
import { WorkspaceName } from '../workspace/values';
import { WorkspaceOwner } from '../entities';
//#endregion

export class WorkspaceRequest implements Identifiable {
  private state?: WorkspaceRequestState;

  @Type(() => Date)
  private openedAt?: Date;

  @Type(() => Date)
  private closedAt?: Date;

  public constructor(
    private workspaceId?: string,
    private workspaceName?: WorkspaceName,
    private owner?: WorkspaceOwner,
  ) {}

  public getId(): string {
    return notNull(
      this.workspaceId,
      'Accessing uninitialized workspace request',
    );
  }

  public getWorkspaceName(): WorkspaceName {
    return notNull(
      this.workspaceName,
      'Accessing uninitialized workspace request',
    );
  }

  public getOwner(): WorkspaceOwner {
    return notNull(this.owner, 'Accessing uninitialized workspace request');
  }

  public getOpenedAt(): Date {
    return notNull(this.openedAt, 'Accessing uninitialized workspace request');
  }

  public getClosedAt(): Date {
    return notNull(
      this.closedAt,
      'Retrieving close time of open workspace request',
    );
  }

  public getState(): WorkspaceRequestState {
    return notNull(this.state, 'Accessing uninitialized workspace request');
  }

  public isOpened(): boolean {
    return this.state === WorkspaceRequestState.opened;
  }

  public isConfirmed(): boolean {
    return this.state === WorkspaceRequestState.confirmed;
  }

  public isDeclined(): boolean {
    return this.state === WorkspaceRequestState.declined;
  }

  public isClosed(): boolean {
    return (
      this.state === WorkspaceRequestState.confirmed ||
      this.state === WorkspaceRequestState.declined
    );
  }

  public hasBeenCreated(): boolean {
    return (
      !!this.state && !!this.workspaceId && !!this.workspaceName && !!this.owner
    );
  }

  public hasNotBeenCreated(): boolean {
    return !this.hasBeenCreated();
  }

  public setId(workspaceId: string): void {
    this.workspaceId = workspaceId;
  }

  public setWorkspaceName(workspaceName: WorkspaceName): void {
    this.workspaceName = workspaceName;
  }

  public setOwner(owner: WorkspaceOwner): void {
    this.owner = owner;
  }

  public setOpenedAt(openedAt: Date): void {
    this.openedAt = openedAt;
  }

  public setClosedAt(closedAt: Date): void {
    this.closedAt = closedAt;
  }

  public open(): void {
    this.state = WorkspaceRequestState.opened;
  }

  public confirm(): void {
    this.state = WorkspaceRequestState.confirmed;
  }

  public decline(): void {
    this.state = WorkspaceRequestState.declined;
  }
}
