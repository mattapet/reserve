/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';
import { AggregateRepository } from '@rsaas/commons/lib/events';

import { WorkspaceRequest } from './workspace-request.entity';
import { WorkspaceService } from '../workspace/workspace.service';

import { SystemAdministrator } from '../entities';
import {
  InvalidOperationError,
  WorkspaceRequestNameCollisionError,
  WorkspaceRequestNotFoundError,
} from './workspace-request.errors';
import {
  WorkspaceRequestCommand,
  WorkspaceRequestCommandType,
} from './workspace-request.command';
import { OpenWorkspaceRequestParams } from './values';
import { AGGREGATE_NAME, WORKSPACE_REQUEST_REPOSITORY } from './constants';
import { WorkspaceRequestEvent } from './workspace-request.event';
//#endregion

@Injectable()
export class WorkspaceRequestService {
  public constructor(
    @Inject(WORKSPACE_REQUEST_REPOSITORY)
    private readonly repository: AggregateRepository<
      WorkspaceRequestCommand,
      WorkspaceRequestEvent,
      WorkspaceRequest
    >,
    private readonly workspaceService: WorkspaceService,
  ) {}

  private get aggregateName(): string {
    return AGGREGATE_NAME;
  }

  // Queries

  public async getById(workspaceId: string): Promise<WorkspaceRequest> {
    const request = await this.repository.getById(
      this.aggregateName,
      workspaceId,
    );

    if (!request.hasBeenCreated()) {
      throw new WorkspaceRequestNotFoundError(
        `Workspace request for workspace with id '${workspaceId}' does not exists`,
      );
    }

    return request;
  }

  // Commands

  public async open(
    request: WorkspaceRequest,
    params: OpenWorkspaceRequestParams,
  ): Promise<void> {
    const { workspaceName } = params;
    const workspace = await this.workspaceService.findByName(workspaceName);

    if (workspace != null) {
      throw new WorkspaceRequestNameCollisionError(
        `Workspace with name '${params.workspaceName.get()}' already exists`,
      );
    }

    await this.repository.execute(this.makeOpenCommand(params), request);
  }

  public async confirm(
    request: WorkspaceRequest,
    confirmedBy: SystemAdministrator,
  ): Promise<void> {
    if (request.isClosed()) {
      throw new InvalidOperationError('Cannot confirm already closed request');
    }

    await this.repository.execute(
      this.makeConfirmCommand(request, confirmedBy),
      request,
    );
  }

  public async decline(
    request: WorkspaceRequest,
    declinedBy: SystemAdministrator,
  ): Promise<void> {
    if (request.isClosed()) {
      throw new InvalidOperationError('Cannot decline already closed request');
    }

    await this.repository.execute(
      this.makeDeclinedCommand(request, declinedBy),
      request,
    );
  }

  private makeOpenCommand({
    workspaceId,
    workspaceName,
    requestedBy,
  }: OpenWorkspaceRequestParams): WorkspaceRequestCommand {
    return {
      type: WorkspaceRequestCommandType.open,
      rowId: this.aggregateName,
      aggregateId: workspaceId,
      payload: {
        workspaceName,
        requestedBy,
      },
    };
  }

  private makeConfirmCommand(
    request: WorkspaceRequest,
    confirmedBy: SystemAdministrator,
  ): WorkspaceRequestCommand {
    return {
      type: WorkspaceRequestCommandType.confirm,
      rowId: this.aggregateName,
      aggregateId: request.getId(),
      payload: { confirmedBy },
    };
  }

  private makeDeclinedCommand(
    request: WorkspaceRequest,
    declinedBy: SystemAdministrator,
  ): WorkspaceRequestCommand {
    return {
      type: WorkspaceRequestCommandType.decline,
      rowId: this.aggregateName,
      aggregateId: request.getId(),
      payload: { declinedBy },
    };
  }
}
