/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Inject, Injectable } from '@nestjs/common';

import { WorkspaceAnnouncementRepository } from './workspace-announcement.repository';
import { WorkspaceAnnouncement } from './workspace-announcement.entity';

import { WorkspaceAnnouncementNameCollisionError } from './workspace-announcement.errors';
import { WORKSPACE_ANNOUNCEMENT_REPOSITORY } from './constants';
//#endregion

@Injectable()
export class WorkspaceAnnouncementService {
  public constructor(
    @Inject(WORKSPACE_ANNOUNCEMENT_REPOSITORY)
    private readonly repository: WorkspaceAnnouncementRepository,
  ) {}

  public async getAll(workspaceId: string): Promise<WorkspaceAnnouncement[]> {
    return this.repository.getAll(workspaceId);
  }

  public async getByName(
    workspaceId: string,
    name: string,
  ): Promise<WorkspaceAnnouncement> {
    return this.repository.getByName(workspaceId, name);
  }

  public async findActive(
    workspaceId: string,
  ): Promise<WorkspaceAnnouncement | undefined> {
    return this.repository.findActive(workspaceId);
  }

  public async create(announcement: WorkspaceAnnouncement): Promise<void> {
    if (await this.containsAnnouncementWithMatchingName(announcement)) {
      throw new WorkspaceAnnouncementNameCollisionError(
        `Announcement with name ${announcement.getName()} already exists`,
      );
    }
    await this.repository.save(announcement);
  }

  private async containsAnnouncementWithMatchingName(
    announcement: WorkspaceAnnouncement,
  ): Promise<boolean> {
    const match = await this.repository.findByName(
      announcement.getWorkspaceId(),
      announcement.getName(),
    );
    return match != null;
  }

  public async toggle(announcement: WorkspaceAnnouncement): Promise<void> {
    if (announcement.isActive()) {
      await this.deactivate(announcement);
    } else {
      await this.activate(announcement);
    }
  }

  private async activate(announcement: WorkspaceAnnouncement): Promise<void> {
    const active = await this.repository.findActive(
      announcement.getWorkspaceId(),
    );

    if (active) {
      await this.activateAccnouncementDeactivatingOldOne(announcement, active);
    } else {
      await this.activateAccnouncement(announcement);
    }
  }

  private async deactivate(announcement: WorkspaceAnnouncement): Promise<void> {
    announcement.deactivate();
    await this.repository.save(announcement);
  }

  public async remove(announcement: WorkspaceAnnouncement): Promise<void> {
    await this.repository.remove(announcement);
  }

  private async activateAccnouncement(
    newAnnouncement: WorkspaceAnnouncement,
  ): Promise<void> {
    newAnnouncement.activate();
    await this.repository.save(newAnnouncement);
  }

  private async activateAccnouncementDeactivatingOldOne(
    newAnnouncement: WorkspaceAnnouncement,
    oldAnnouncement: WorkspaceAnnouncement,
  ): Promise<void> {
    newAnnouncement.activate();
    oldAnnouncement.deactivate();
    await this.repository.save([newAnnouncement, oldAnnouncement]);
  }
}
