/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { Entity, PrimaryColumn, Column, AfterLoad } from 'typeorm';

@Entity({ name: 'workspace_announcement' })
export class WorkspaceAnnouncement {
  @PrimaryColumn({ name: 'workspace_id', charset: 'utf8' })
  private workspaceId: string;

  @PrimaryColumn({ charset: 'utf8' })
  private name: string;

  @Column({ charset: 'utf8' })
  private text: string;

  @Column()
  private active: boolean;

  public constructor(workspaceId: string, name: string, text: string) {
    this.workspaceId = workspaceId;
    this.name = name;
    this.text = text;
    this.active = false;
  }

  @AfterLoad()
  // @ts-ignore
  private normalize() {
    this.active = !!this.active;
  }

  public getWorkspaceId(): string {
    return this.workspaceId;
  }

  public getName(): string {
    return this.name;
  }

  public getText(): string {
    return this.text;
  }

  public isActive(): boolean {
    return this.active;
  }

  public activate(): void {
    this.active = true;
  }

  public deactivate(): void {
    this.active = false;
  }
}
