/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { WorkspaceAnnouncement } from './workspace-announcement.entity';

export interface WorkspaceAnnouncementRepository {
  getAll(workspaceId: string): Promise<WorkspaceAnnouncement[]>;
  findByName(
    workspaceId: string,
    name: string,
  ): Promise<WorkspaceAnnouncement | undefined>;
  getByName(workspaceId: string, name: string): Promise<WorkspaceAnnouncement>;
  findActive(workspaceId: string): Promise<WorkspaceAnnouncement | undefined>;
  save(accouncement: WorkspaceAnnouncement): Promise<WorkspaceAnnouncement>;
  save(accouncement: WorkspaceAnnouncement[]): Promise<WorkspaceAnnouncement[]>;
  remove(accouncement: WorkspaceAnnouncement): Promise<WorkspaceAnnouncement>;
}
