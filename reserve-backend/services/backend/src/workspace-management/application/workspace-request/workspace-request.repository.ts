/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';

import {
  AggregateRepository,
  SnapshotRepository,
  EventRepository,
  InjectEventRepository,
  InjectSnapshotRepository,
} from '@rsaas/commons/lib/events';

import { WorkspaceRequestCommand } from '../../domain/workspace-request/workspace-request.command';
import { WorkspaceRequestEvent } from '../../domain/workspace-request/workspace-request.event';
import { WorkspaceRequest } from '../../domain/workspace-request/workspace-request.entity';
import {
  WorkspaceRequestCommandHandler,
  WorkspaceRequestEventHandler,
} from '../../domain/workspace-request/handlers';
//#endregion

@Injectable()
export class WorkspaceRequestRepository extends AggregateRepository<
  WorkspaceRequestCommand,
  WorkspaceRequestEvent,
  WorkspaceRequest
> {
  public constructor(
    @InjectEventRepository()
    eventRepository: EventRepository<WorkspaceRequestEvent>,
    @InjectSnapshotRepository()
    snapshotRepository: SnapshotRepository,
    commandHandler: WorkspaceRequestCommandHandler,
    eventHandler: WorkspaceRequestEventHandler,
  ) {
    super(
      eventRepository,
      snapshotRepository,
      commandHandler,
      eventHandler,
      WorkspaceRequest,
    );
  }
}
