/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { Log } from '@rsaas/commons/lib/logger';
import { Transactional } from '@rsaas/commons/lib/typeorm';
import { EventConsumer, EventPattern } from '@rsaas/commons/lib/event-broker';

import {
  WorkspaceRequestEventType,
  WorkspaceRequestConfirmedEvent,
} from '../../domain/workspace-request/workspace-request.event';
import { WorkspaceService } from '../../domain/workspace/workspace.service';
import { WorkspaceRequestService } from '../../domain/workspace-request/workspace-request.service';
import { Workspace } from '../../domain/workspace/workspace.entity';
//#endregion

@Injectable()
@EventConsumer()
export class WorkspaceRequestConfirmationIngestorService {
  public constructor(
    private readonly workspaceService: WorkspaceService,
    private readonly workspaceRequestService: WorkspaceRequestService,
  ) {}

  @Log({ format: 'Consuming event {0}' })
  @Transactional()
  @EventPattern(WorkspaceRequestEventType.confirmed)
  public async consumeConfirmed(
    event: WorkspaceRequestConfirmedEvent,
  ): Promise<void> {
    const request = await this.workspaceRequestService.getById(
      event.aggregateId,
    );

    await this.workspaceService.create(
      new Workspace(),
      request.getId(),
      request.getWorkspaceName(),
      request.getOwner(),
    );
  }
}
