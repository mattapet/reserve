/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';

import {
  AggregateRepository,
  EventRepository,
  SnapshotRepository,
  InjectEventRepository,
  InjectSnapshotRepository,
} from '@rsaas/commons/lib/events';

import { Workspace } from '../../domain/workspace/workspace.entity';
import { WorkspaceEvent } from '../../domain/workspace/workspace.event';
import { WorkspaceCommand } from '../../domain/workspace/workspace.command';
import {
  WorkspaceCommandHandler,
  WorkspaceEventHandler,
} from '../../domain/workspace/handlers';
//#endregion

@Injectable()
export class WorkspaceRepository extends AggregateRepository<
  WorkspaceCommand,
  WorkspaceEvent,
  Workspace
> {
  public constructor(
    @InjectEventRepository()
    eventRepository: EventRepository<WorkspaceEvent>,
    @InjectSnapshotRepository()
    snapshotRepository: SnapshotRepository,
    commandHandler: WorkspaceCommandHandler,
    eventHandler: WorkspaceEventHandler,
  ) {
    super(
      eventRepository,
      snapshotRepository,
      commandHandler,
      eventHandler,
      Workspace,
    );
  }
}
