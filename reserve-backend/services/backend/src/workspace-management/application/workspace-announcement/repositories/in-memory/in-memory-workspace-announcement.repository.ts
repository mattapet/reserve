/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceAnnouncement } from '../../../../domain/workspace-announcement/workspace-announcement.entity';
import { WorkspaceAnnouncementNotFoundError } from '../../../../domain/workspace-announcement/workspace-announcement.errors';
import { WorkspaceAnnouncementRepository } from '../../../../domain/workspace-announcement/workspace-announcement.repository';
//#endregion

export class InMemoryWorkspaceAccouncementRepository
  implements WorkspaceAnnouncementRepository {
  private storage: {
    [workspaceId: string]: { [name: string]: WorkspaceAnnouncement };
  } = {};

  public async getAll(workspaceId: string): Promise<WorkspaceAnnouncement[]> {
    return Object.values(this.storage[workspaceId] || {});
  }

  public async findActive(
    workspaceId: string,
  ): Promise<WorkspaceAnnouncement | undefined> {
    return Object.values(
      this.storage[workspaceId] || {},
    ).filter((announcement) => announcement.isActive())[0];
  }

  public async getByName(
    workspaceId: string,
    name: string,
  ): Promise<WorkspaceAnnouncement> {
    const item = (this.storage[workspaceId] ?? {})[name];
    if (!item) {
      throw new WorkspaceAnnouncementNotFoundError();
    }
    return item;
  }

  public async findByName(
    workspaceId: string,
    name: string,
  ): Promise<WorkspaceAnnouncement | undefined> {
    return (this.storage[workspaceId] ?? {})[name];
  }

  public async save(
    announcement: WorkspaceAnnouncement,
  ): Promise<WorkspaceAnnouncement>;
  public async save(
    announcement: WorkspaceAnnouncement[],
  ): Promise<WorkspaceAnnouncement[]>;
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async save(args: any): Promise<any> {
    if (Array.isArray(args)) {
      return this.saveAnnouncements(args);
    } else {
      return this.saveAnnouncement(args);
    }
  }

  public saveAnnouncements(
    announcements: WorkspaceAnnouncement[],
  ): WorkspaceAnnouncement[] {
    return announcements.map((ann) => this.saveAnnouncement(ann));
  }

  public saveAnnouncement(
    announcement: WorkspaceAnnouncement,
  ): WorkspaceAnnouncement {
    const announcements = this.storage[announcement.getWorkspaceId()] ?? {};
    announcements[announcement.getName()] = announcement;
    this.storage[announcement.getWorkspaceId()] = announcements;
    return announcement;
  }

  public async remove(
    announcement: WorkspaceAnnouncement,
  ): Promise<WorkspaceAnnouncement> {
    const announcements = this.storage[announcement.getWorkspaceId()] ?? {};
    delete announcements[announcement.getName()];
    this.storage[announcement.getWorkspaceId()] = announcements;
    return announcement;
  }
}
