/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Repository, EntityRepository } from 'typeorm';

import { WorkspaceAnnouncement } from '../../../../domain/workspace-announcement/workspace-announcement.entity';
import { WorkspaceAnnouncementNotFoundError } from '../../../../domain/workspace-announcement/workspace-announcement.errors';
import { WorkspaceAnnouncementRepository } from '../../../../domain/workspace-announcement/workspace-announcement.repository';
//#endregion

@EntityRepository(WorkspaceAnnouncement)
export class TypeormWorkspaceAnnouncementRepository
  extends Repository<WorkspaceAnnouncement>
  implements WorkspaceAnnouncementRepository {
  public async getAll(workspaceId: string): Promise<WorkspaceAnnouncement[]> {
    return this.find({ workspaceId } as any);
  }

  public async findActive(
    workspaceId: string,
  ): Promise<WorkspaceAnnouncement | undefined> {
    return this.findOne({ workspaceId, active: true } as any);
  }

  public async getByName(
    workspaceId: string,
    name: string,
  ): Promise<WorkspaceAnnouncement> {
    const item = await this.findOne({ workspaceId, name } as any);
    if (!item) {
      throw new WorkspaceAnnouncementNotFoundError();
    }
    return item;
  }

  public async findByName(
    workspaceId: string,
    name: string,
  ): Promise<WorkspaceAnnouncement | undefined> {
    return this.findOne({ workspaceId, name } as any);
  }
}
