/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonsModule } from '@rsaas/commons';

import { WorkspaceAnnouncement } from '../../domain/workspace-announcement/workspace-announcement.entity';
import { WorkspaceAnnouncementService } from '../../domain/workspace-announcement/workspace-announcement.service';
import { WORKSPACE_ANNOUNCEMENT_REPOSITORY } from '../../domain/workspace-announcement/constants';

import { TypeormWorkspaceAnnouncementRepository } from '../../application/workspace-announcement/repositories/typeorm/typeorm-workspace-announcement.repository';
//#endregion

@Module({
  imports: [
    CommonsModule,
    TypeOrmModule.forFeature([
      WorkspaceAnnouncement,
      TypeormWorkspaceAnnouncementRepository,
    ]),
  ],
  providers: [
    WorkspaceAnnouncementService,
    {
      provide: WORKSPACE_ANNOUNCEMENT_REPOSITORY,
      useFactory: (repo) => repo,
      inject: [TypeormWorkspaceAnnouncementRepository],
    },
  ],
  exports: [WorkspaceAnnouncementService],
})
export class WorkspaceAnnouncementModule {}
