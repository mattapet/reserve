/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { CommonsModule } from '@rsaas/commons';

import { WorkspaceModule } from '../workspace';
import { WorkspaceRequestModule } from '../workspace-request';

import { WorkspaceRequestConfirmationIngestorService } from '../../application/workspace-request-confirmation-ingestor/workspace-request-confirmation-ingestor.service';
//#endregion

@Module({
  imports: [CommonsModule, WorkspaceModule, WorkspaceRequestModule],
  providers: [WorkspaceRequestConfirmationIngestorService],
})
export class WorkspaceRequestEventIngestorModule {}
