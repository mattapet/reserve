/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// #region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons';

import { WorkspaceRepository } from '../../application/workspace/workspace.repository';
import { WorkspaceService } from '../../domain/workspace/workspace.service';
import {
  WorkspaceCommandHandler,
  WorkspaceEventHandler,
} from '../../domain/workspace/handlers';
import { WORKSPACE_REPOSITORY } from '../../domain/workspace/constants';
// #endregion

@Module({
  imports: [CommonsModule],
  providers: [
    WorkspaceService,
    { provide: WORKSPACE_REPOSITORY, useClass: WorkspaceRepository },
    WorkspaceCommandHandler,
    WorkspaceEventHandler,
  ],
  exports: [WorkspaceService],
})
export class WorkspaceModule {}
