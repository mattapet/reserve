/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { WorkspaceModule } from './workspace';
import { WorkspaceRequestModule } from './workspace-request';
import { WorkspaceRequestEventIngestorModule } from './workspace-request-confirmation-ingestor';
import { WorkspaceAnnouncementModule } from './workspace-announcement';
//#endregion

@Module({
  imports: [
    WorkspaceModule,
    WorkspaceRequestModule,
    WorkspaceAnnouncementModule,
    WorkspaceRequestEventIngestorModule,
  ],
  exports: [
    WorkspaceModule,
    WorkspaceRequestModule,
    WorkspaceAnnouncementModule,
    WorkspaceRequestEventIngestorModule,
  ],
})
export class WorkspaceManagementModule {}
