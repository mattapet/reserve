/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons';

import { WorkspaceModule } from '../workspace/workspace.module';

import { WorkspaceRequestRepository } from '../../application/workspace-request/workspace-request.repository';
import { WorkspaceRequestService } from '../../domain/workspace-request/workspace-request.service';
import {
  WorkspaceRequestCommandHandler,
  WorkspaceRequestEventHandler,
} from '../../domain/workspace-request/handlers';
import { WORKSPACE_REQUEST_REPOSITORY } from '../../domain/workspace-request/constants';
//#endregion

@Module({
  imports: [CommonsModule, WorkspaceModule],
  providers: [
    WorkspaceRequestService,
    {
      provide: WORKSPACE_REQUEST_REPOSITORY,
      useClass: WorkspaceRequestRepository,
    },
    WorkspaceRequestCommandHandler,
    WorkspaceRequestEventHandler,
  ],
  exports: [WorkspaceRequestService],
})
export class WorkspaceRequestModule {}
