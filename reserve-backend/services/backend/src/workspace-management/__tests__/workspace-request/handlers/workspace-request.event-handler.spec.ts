/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceRequestEventHandler } from '../../../domain/workspace-request/handlers/workspace-request.event-handler';

import {
  WorkspaceRequestOpenedEvent,
  WorkspaceRequestEventType,
  WorkspaceRequestDeclinedEvent,
  WorkspaceRequestConfirmedEvent,
} from '../../../domain/workspace-request/workspace-request.event';
import { WorkspaceRequest } from '../../../domain/workspace-request/workspace-request.entity';
import { WorkspaceName } from '../../../domain/workspace/values';
import { WorkspaceOwner } from '../../../domain/entities';
//#endregion

describe('workspace request event handler', () => {
  const workspaceRequestEventHandler = new WorkspaceRequestEventHandler();

  it('should produce opened workspace request', () => {
    const event: WorkspaceRequestOpenedEvent = {
      type: WorkspaceRequestEventType.opened,
      rowId: 'WorkspaceRequestAggregate',
      aggregateId: 'test',
      timestamp: new Date(100),
      payload: {
        workspaceName: 'test-workspace',
        requestedBy: '99',
      },
    };

    const result = workspaceRequestEventHandler.apply(new WorkspaceRequest(), [
      event,
    ]);

    expect(result.isOpened()).toBe(true);
    expect(result.getOwner()).toEqual(new WorkspaceOwner('99'));
    expect(result.getId()).toBe('test');
    expect(result.getWorkspaceName()).toEqual(
      new WorkspaceName('test-workspace'),
    );
    expect(result.getOpenedAt()).toEqual(new Date(100));
  });

  describe('confirming workspace request', () => {
    const event: WorkspaceRequestConfirmedEvent = {
      type: WorkspaceRequestEventType.confirmed,
      rowId: 'WorkspaceRequestAggregate',
      aggregateId: 'test',
      timestamp: new Date(100),
      payload: {
        confirmedBy: 'root',
      },
    };

    const result = workspaceRequestEventHandler.apply(new WorkspaceRequest(), [
      event,
    ]);

    expect(result.isClosed()).toBe(true);
    expect(result.getClosedAt()).toEqual(new Date(100));
  });

  it('should decline opened workspace request', () => {
    const event: WorkspaceRequestConfirmedEvent = {
      type: WorkspaceRequestEventType.confirmed,
      rowId: 'WorkspaceRequestAggregate',
      aggregateId: 'test',
      timestamp: new Date(100),
      payload: {
        confirmedBy: 'root',
      },
    };

    const result = workspaceRequestEventHandler.apply(new WorkspaceRequest(), [
      event,
    ]);

    expect(result.isConfirmed()).toBe(true);
  });

  describe('declining workspace request', () => {
    it('should close opened workspace request', () => {
      const event: WorkspaceRequestDeclinedEvent = {
        type: WorkspaceRequestEventType.declined,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          declinedBy: 'root',
        },
      };

      const result = workspaceRequestEventHandler.apply(
        new WorkspaceRequest(),
        [event],
      );

      expect(result.isClosed()).toBe(true);
      expect(result.getClosedAt()).toEqual(new Date(100));
    });

    it('should decline opened workspace request', () => {
      const event: WorkspaceRequestDeclinedEvent = {
        type: WorkspaceRequestEventType.declined,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          declinedBy: 'root',
        },
      };

      const result = workspaceRequestEventHandler.apply(
        new WorkspaceRequest(),
        [event],
      );

      expect(result.isDeclined()).toBe(true);
    });
  });
});
