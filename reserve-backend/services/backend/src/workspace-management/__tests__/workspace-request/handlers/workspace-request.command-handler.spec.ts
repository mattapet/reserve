/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceRequestCommandHandler } from '../../../domain/workspace-request/handlers/workspace-request.command-handler';

import { WorkspaceRequest } from '../../../domain/workspace-request/workspace-request.entity';
import { WorkspaceRequestEventType } from '../../../domain/workspace-request/workspace-request.event';
import {
  WorkspaceRequestCommandType,
  OpenWorkspaceRequestCommand,
  ConfirmWorkspaceRequestCommand,
  DeclinedWorkspaceRequestCommand,
} from '../../../domain/workspace-request/workspace-request.command';
import { WorkspaceName } from '../../../domain/workspace/values';
import { WorkspaceOwner, SystemAdministrator } from '../../../domain/entities';
//#endregion

describe('workspace-request.command-handler', () => {
  const workspaceRequestCommandHandler = new WorkspaceRequestCommandHandler();

  function mock_dateNow(now: number) {
    jest.spyOn(Date, 'now').mockImplementation(() => now);
  }

  describe('open command', () => {
    it('should produce workspace request opened event', async () => {
      mock_dateNow(100);
      const command: OpenWorkspaceRequestCommand = {
        type: WorkspaceRequestCommandType.open,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        payload: {
          workspaceName: new WorkspaceName('test-name'),
          requestedBy: new WorkspaceOwner('99'),
        },
      };

      const result = workspaceRequestCommandHandler.execute(
        new WorkspaceRequest(),
        command,
      );

      expect(result).toEqual([
        {
          type: WorkspaceRequestEventType.opened,
          rowId: 'WorkspaceRequestAggregate',
          aggregateId: 'test',
          timestamp: new Date(100),
          payload: {
            workspaceName: 'test-name',
            requestedBy: '99',
          },
        },
      ]);
    });

    it('should produce no events if request is already created', () => {
      mock_dateNow(100);
      const owner = new WorkspaceOwner('99');
      const command: OpenWorkspaceRequestCommand = {
        type: WorkspaceRequestCommandType.open,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        payload: {
          workspaceName: new WorkspaceName('test-name'),
          requestedBy: owner,
        },
      };
      const name = new WorkspaceName('test-name');
      const aggregate = new WorkspaceRequest('test', name, owner);
      aggregate.open();

      const result = workspaceRequestCommandHandler.execute(aggregate, command);

      expect(result).toEqual([]);
    });
  });

  describe('confirm command', () => {
    it('should produce confirmed request event', () => {
      const command: ConfirmWorkspaceRequestCommand = {
        type: WorkspaceRequestCommandType.confirm,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        payload: {
          confirmedBy: new SystemAdministrator('root'),
        },
      };
      const name = new WorkspaceName('test');
      const owner = new WorkspaceOwner('99');
      const aggregate = new WorkspaceRequest('test', name, owner);
      aggregate.open();

      const result = workspaceRequestCommandHandler.execute(aggregate, command);

      expect(result).toEqual([
        {
          type: WorkspaceRequestEventType.confirmed,
          rowId: 'WorkspaceRequestAggregate',
          aggregateId: 'test',
          timestamp: new Date(100),
          payload: {
            confirmedBy: 'root',
          },
        },
      ]);
    });

    it('should produce no events if request is already confirmed', () => {
      const command: ConfirmWorkspaceRequestCommand = {
        type: WorkspaceRequestCommandType.confirm,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        payload: {
          confirmedBy: new SystemAdministrator('root'),
        },
      };
      const name = new WorkspaceName('test');
      const owner = new WorkspaceOwner('99');
      const aggregate = new WorkspaceRequest('test', name, owner);
      aggregate.confirm();

      const result = workspaceRequestCommandHandler.execute(aggregate, command);

      expect(result).toEqual([]);
    });
  });

  describe('decline command', () => {
    it('should produce declined request event', () => {
      const command: DeclinedWorkspaceRequestCommand = {
        type: WorkspaceRequestCommandType.decline,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        payload: {
          declinedBy: new SystemAdministrator('root'),
        },
      };
      const name = new WorkspaceName('test');
      const owner = new WorkspaceOwner('99');
      const aggregate = new WorkspaceRequest('test', name, owner);
      aggregate.open();

      const result = workspaceRequestCommandHandler.execute(aggregate, command);

      expect(result).toEqual([
        {
          type: WorkspaceRequestEventType.declined,
          rowId: 'WorkspaceRequestAggregate',
          aggregateId: 'test',
          timestamp: new Date(100),
          payload: {
            declinedBy: 'root',
          },
        },
      ]);
    });

    it('should produce no events if request is already declined', () => {
      const command: DeclinedWorkspaceRequestCommand = {
        type: WorkspaceRequestCommandType.decline,
        rowId: 'WorkspaceRequestAggregate',
        aggregateId: 'test',
        payload: {
          declinedBy: new SystemAdministrator('root'),
        },
      };
      const name = new WorkspaceName('test');
      const owner = new WorkspaceOwner('99');
      const aggregate = new WorkspaceRequest('test', name, owner);
      aggregate.decline();

      const result = workspaceRequestCommandHandler.execute(aggregate, command);

      expect(result).toEqual([]);
    });
  });
});
