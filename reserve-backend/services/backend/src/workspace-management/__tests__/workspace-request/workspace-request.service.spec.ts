/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceRequestService } from '../../domain/workspace-request/workspace-request.service';

import { WorkspaceServiceBuilder } from '../workspace/builders/workspace.service.builder';
import { WorkspaceRequestRepositoryBuilder } from './builders/workspace-request.repository.builder';

import { WorkspaceRequestRepository } from '../../application/workspace-request/workspace-request.repository';

import { WorkspaceService } from '../../domain/workspace/workspace.service';
import { WorkspaceRequestCommandType } from '../../domain/workspace-request/workspace-request.command';
import { WorkspaceRequest } from '../../domain/workspace-request/workspace-request.entity';
import {
  InvalidOperationError,
  WorkspaceRequestNameCollisionError,
  WorkspaceRequestNotFoundError,
} from '../../domain/workspace-request/workspace-request.errors';
import { Workspace } from '../../domain/workspace/workspace.entity';
import { WorkspaceName } from '../../domain/workspace/values';
import { SystemAdministrator, WorkspaceOwner } from '../../domain/entities';
//#endregion

describe('workspace-request.service', () => {
  let workspaceService!: WorkspaceService;
  let repository!: WorkspaceRequestRepository;
  let service!: WorkspaceRequestService;

  beforeEach(() => {
    workspaceService = WorkspaceServiceBuilder.inMemory();
    repository = WorkspaceRequestRepositoryBuilder.inMemory();
    service = new WorkspaceRequestService(repository, workspaceService);
  });

  describe('retrieving workspace requests', () => {
    it('should workspace request', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const user = new WorkspaceOwner('99');
      await service.open(new WorkspaceRequest(), {
        workspaceId: 'test',
        workspaceName: new WorkspaceName('test-name'),
        requestedBy: user,
      });

      const result = await service.getById('test');
      expect(result.isOpened()).toBe(true);
      expect(result.getOpenedAt()).toEqual(new Date(100));
      expect(result.getOwner()).toEqual(user);
      expect(result.getId()).toEqual('test');
      expect(result.getWorkspaceName()).toEqual(new WorkspaceName('test-name'));
    });

    it('should throw a WorkspaceRequestNotFoundError if workspace is not opened', async () => {
      const fn = service.getById('test');

      await expect(fn).rejects.toThrow(WorkspaceRequestNotFoundError);
    });
  });

  describe('opening workspace requests', () => {
    it('should open a workspace request', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const user = new WorkspaceOwner('99');
      const request = new WorkspaceRequest();

      await service.open(request, {
        workspaceId: 'test',
        workspaceName: new WorkspaceName('test-name'),
        requestedBy: user,
      });

      expect(request.isOpened()).toBe(true);
      expect(request.getOpenedAt()).toEqual(new Date(100));
      expect(request.getOwner()).toEqual(user);
      expect(request.getId()).toEqual('test');
      expect(request.getWorkspaceName()).toEqual(
        new WorkspaceName('test-name'),
      );
    });

    it('should produce a open command', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const user = new WorkspaceOwner('99');
      const request = new WorkspaceRequest();
      const execute = jest.spyOn(repository, 'execute');

      await service.open(request, {
        workspaceId: 'test',
        workspaceName: new WorkspaceName('test-name'),
        requestedBy: user,
      });

      expect(execute).toHaveBeenCalledWith(
        {
          type: WorkspaceRequestCommandType.open,
          rowId: 'WorkspaceRequestAggregate',
          aggregateId: 'test',
          payload: {
            workspaceName: new WorkspaceName('test-name'),
            requestedBy: user,
          },
        },
        request,
      );
    });

    it('should throw WorkspaceRequestNameCollisionError if there is already a workspace with given name', async () => {
      const user = new WorkspaceOwner('99');
      const name = new WorkspaceName('test');
      await workspaceService.create(new Workspace(), 'test', name, user);
      const request = new WorkspaceRequest();

      const fn = service.open(request, {
        workspaceId: 'testing',
        workspaceName: new WorkspaceName('test'),
        requestedBy: user,
      });

      await expect(fn).rejects.toThrow(WorkspaceRequestNameCollisionError);
    });
  });

  describe('confirming workspace requests', () => {
    it('should confirmed the workspace request', async () => {
      const administrator = new SystemAdministrator('root');
      const request = new WorkspaceRequest('test');
      request.open();

      await service.confirm(request, administrator);

      expect(request.isConfirmed()).toBe(true);
      expect(request.isClosed()).toBe(true);
    });

    it('should produce a confirm command', async () => {
      const administrator = new SystemAdministrator('root');
      const request = new WorkspaceRequest('test');
      request.open();
      const execute = jest.spyOn(repository, 'execute');

      await service.confirm(request, administrator);

      expect(execute).toHaveBeenCalledWith(
        {
          type: WorkspaceRequestCommandType.confirm,
          rowId: 'WorkspaceRequestAggregate',
          aggregateId: 'test',
          payload: {
            confirmedBy: administrator,
          },
        },
        request,
      );
    });

    it('should throw InvalidOperationError if request is already closed', async () => {
      const administrator = new SystemAdministrator('root');
      const request = new WorkspaceRequest('test');
      request.confirm();

      const fn = service.confirm(request, administrator);

      await expect(fn).rejects.toThrow(InvalidOperationError);
    });
  });

  describe('declining workspace requests', () => {
    it('should decline workspace request', async () => {
      const administrator = new SystemAdministrator('root');
      const request = new WorkspaceRequest('test');
      request.open();

      await service.decline(request, administrator);

      expect(request.isDeclined()).toBe(true);
      expect(request.isClosed()).toBe(true);
    });

    it('should produce a decline command', async () => {
      const administrator = new SystemAdministrator('root');
      const request = new WorkspaceRequest('test');
      request.open();
      const execute = jest.spyOn(repository, 'execute');

      await service.decline(request, administrator);

      expect(execute).toHaveBeenCalledWith(
        {
          type: WorkspaceRequestCommandType.decline,
          rowId: 'WorkspaceRequestAggregate',
          aggregateId: 'test',
          payload: {
            declinedBy: administrator,
          },
        },
        request,
      );
    });

    it('should throw InvalidOperationError when request is already closed', async () => {
      const administrator = new SystemAdministrator('root');
      const request = new WorkspaceRequest('test');
      request.decline();

      const fn = service.decline(request, administrator);

      await expect(fn).rejects.toThrow(InvalidOperationError);
    });
  });
});
