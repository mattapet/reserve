/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';
import { EventRepository, SnapshotRepository } from '@rsaas/commons/lib/events';
import { InMemoryEventRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-event-repository';
import { InMemorySnapshotRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-snapshot.repository';

import { WorkspaceRequestRepository } from '../../../application/workspace-request/workspace-request.repository';
import { WorkspaceRequestEvent } from '../../../domain/workspace-request/workspace-request.event';
import {
  WorkspaceRequestCommandHandler,
  WorkspaceRequestEventHandler,
} from '../../../domain/workspace-request/handlers';
//#endregion

export class WorkspaceRequestRepositoryBuilder {
  private eventRepository?: EventRepository<WorkspaceRequestEvent>;
  private snapshotRepository?: SnapshotRepository;

  public withEventRepository(
    eventRepository: EventRepository<WorkspaceRequestEvent>,
  ): WorkspaceRequestRepositoryBuilder {
    this.eventRepository = eventRepository;
    return this;
  }

  public withSnapshotRepository(
    snapshotRepository: SnapshotRepository,
  ): WorkspaceRequestRepositoryBuilder {
    this.snapshotRepository = snapshotRepository;
    return this;
  }

  public build(): WorkspaceRequestRepository {
    return new WorkspaceRequestRepository(
      notNull(this.eventRepository, 'eventRepository'),
      notNull(this.snapshotRepository, 'snapshotRepository'),
      new WorkspaceRequestCommandHandler(),
      new WorkspaceRequestEventHandler(),
    );
  }

  public static inMemory(): WorkspaceRequestRepository {
    const eventsRepository = new InMemoryEventRepository<
      WorkspaceRequestEvent
    >();
    return new WorkspaceRequestRepositoryBuilder()
      .withEventRepository(eventsRepository)
      .withSnapshotRepository(new InMemorySnapshotRepository())
      .build();
  }
}
