/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceService } from '../../domain/workspace/workspace.service';

import {
  WorkspaceNotFoundError,
  WorkspaceNameCollisionError,
  IllegalOperationError,
} from '../../domain/workspace/workspace.errors';
import { WorkspaceServiceBuilder } from './builders/workspace.service.builder';
import { Workspace } from '../../domain/workspace/workspace.entity';
import { WorkspaceName } from '../../domain/workspace/values';
import { WorkspaceOwner } from '../../domain/entities';
//#endregion

describe('workspace.service', () => {
  let service!: WorkspaceService;

  beforeEach(() => {
    service = WorkspaceServiceBuilder.inMemory();
  });

  const createdBy = new WorkspaceOwner('99');

  async function effect_createWorkspace(
    id: string,
    name: WorkspaceName,
    created: WorkspaceOwner,
  ) {
    await service.create(new Workspace(), id, name, created);
  }

  async function effect_deleteById(
    workspaceId: string,
    deletedBy: WorkspaceOwner,
  ) {
    const workspace = await service.getById(workspaceId);
    await service.delete(workspace, deletedBy);
  }

  describe('retrieving workspace', () => {
    it('should return an empty array if no workspaces are present', async () => {
      const result = await service.getAll();

      expect(result).toEqual([]);
    });

    it('should return all workspaces', async () => {
      const id1 = '99';
      const id2 = '88';
      const name1 = new WorkspaceName('test1');
      const name2 = new WorkspaceName('test2');
      await effect_createWorkspace(id1, name1, createdBy);
      await effect_createWorkspace(id2, name2, createdBy);
      await effect_deleteById(id2, createdBy);

      const result = await service.getAll();

      expect(result).toEqual([new Workspace(id1, name1, createdBy)]);
    });

    it('should return a workspace with the given name', async () => {
      const id = 'test-id';
      const name = new WorkspaceName('test-name');
      await effect_createWorkspace(id, name, createdBy);

      const result = await service.getByName(name);

      expect(result).toEqual(new Workspace(id, name, createdBy));
    });

    it('should throw an error if `undefined` returned', async () => {
      const name = new WorkspaceName('test-name');
      await expect(service.getByName(name)).rejects.toThrow(
        WorkspaceNotFoundError,
      );
    });
  });

  describe('finding workspaces', () => {
    it('should return workspace with given name', async () => {
      const id = 'test-id';
      const name = new WorkspaceName('test-name');
      await effect_createWorkspace(id, name, createdBy);

      const result = await service.findByName(name);

      expect(result).toEqual(new Workspace(id, name, createdBy));
    });

    it('should return undefined if workspace with given name does not exists', async () => {
      const name = new WorkspaceName('test-name');

      const result = await service.findByName(name);

      expect(result).toBeUndefined();
    });

    it('should return undefined if workspace with given name has been removed', async () => {
      const id = 'test-id';
      const name = new WorkspaceName('test-name');
      await effect_createWorkspace(id, name, createdBy);
      await effect_deleteById(id, createdBy);

      const result = await service.findByName(name);

      expect(result).toBeUndefined();
    });
  });

  describe('creating a new workspace', () => {
    it('should create a new workspace', async () => {
      const id = 'test-id';
      const name = new WorkspaceName('test-name');
      const workspace = new Workspace();

      await service.create(workspace, id, name, createdBy);

      expect(workspace).toEqual(new Workspace(id, name, createdBy));
    });

    it('should throw an error if workspace with given name exists', async () => {
      const id = 'test-id';
      const name = new WorkspaceName('test-name');
      await effect_createWorkspace(id, name, createdBy);

      await expect(
        service.create(new Workspace(), id, name, createdBy),
      ).rejects.toThrow(WorkspaceNameCollisionError);
    });
  });

  describe('removing a workspace', () => {
    it('should produce `delete` command', async () => {
      const id = 'test-id';
      const name = new WorkspaceName('test-name');
      const workspace = new Workspace();

      await service.create(workspace, id, name, createdBy);
      await service.delete(workspace, createdBy);

      expect(workspace.isDeleted()).toBe(true);
    });

    it('should throw IllegalOperationError if owner of different workspace tries to delete a workspace', async () => {
      const changedBy = new WorkspaceOwner('44');
      const id = 'test-id';
      const name = new WorkspaceName('test-name');
      const workspace = new Workspace();

      await service.create(workspace, id, name, createdBy);
      const fn = service.delete(workspace, changedBy);

      await expect(fn).rejects.toThrow(IllegalOperationError);
    });
  });
});
