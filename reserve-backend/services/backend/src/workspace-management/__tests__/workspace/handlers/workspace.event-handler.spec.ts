/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceEventHandler } from '../../../domain/workspace/handlers/workspace.event-handler';
import { WorkspaceEventType } from '../../../domain/workspace/workspace.event';
import { Workspace } from '../../../domain/workspace/workspace.entity';
import { WorkspaceName } from '../../../domain/workspace/values';
import { WorkspaceOwner } from '../../../domain/entities';
//#endregion

describe('workspace.event-handler', () => {
  const workspaceEventHandler = new WorkspaceEventHandler();

  const workspaceId = 'test';
  const aggregateId = 'test';
  const timestamp = new Date(0);
  const userId = '294';
  const makeEventCreator = <T extends WorkspaceEventType>(type: T) => <
    Value extends Record<string, any>
  >(
    value: Value,
  ) => ({
    type,
    rowId: workspaceId,
    aggregateId,
    timestamp,
    payload: value,
  });

  it('should properly apply `created` event', () => {
    const event = makeEventCreator(WorkspaceEventType.created)({
      name: 'test',
      createdBy: userId,
    });
    const workspace = new Workspace();

    const result = workspaceEventHandler.apply(workspace, [event]);

    expect(result).toEqual(
      new Workspace(
        'test',
        new WorkspaceName('test'),
        new WorkspaceOwner(userId),
      ),
    );
  });

  it('should properly apply `nameSet` event', () => {
    const event = makeEventCreator(WorkspaceEventType.nameSet)({
      name: 'new-test',
      changedBy: userId,
    });
    const workspace = new Workspace('test', new WorkspaceName('test'));

    const result = workspaceEventHandler.apply(workspace, [event]);

    expect(result.getName()).toEqual(new WorkspaceName('new-test'));
  });

  it('should properly apply `nameSet` event', () => {
    const event = makeEventCreator(WorkspaceEventType.nameSet)({
      name: 'new-test',
      changedBy: userId,
    });
    const workspace = new Workspace('test', new WorkspaceName('test'));

    const result = workspaceEventHandler.apply(workspace, [event]);

    expect(result.getName()).toEqual(new WorkspaceName('new-test'));
  });

  it('should change owner of the workspace', () => {
    const workspace = new Workspace(
      'test',
      new WorkspaceName('test'),
      new WorkspaceOwner('99'),
    );
    const event = makeEventCreator(WorkspaceEventType.ownershipTransferred)({
      transferredFrom: '99',
      transferredTo: '88',
      changedByOwner: '99',
    });

    const result = workspaceEventHandler.apply(workspace, [event]);

    expect(result.getOwner()).toEqual(new WorkspaceOwner('88'));
  });

  it('should properly apply `deleted` event', () => {
    const event = makeEventCreator(WorkspaceEventType.deleted)({
      changedBy: userId,
    });
    const workspace = new Workspace('test', new WorkspaceName('test'));

    const result = workspaceEventHandler.apply(workspace, [event]);

    expect(result.isDeleted()).toBe(true);
  });
});
