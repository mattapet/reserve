/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceCommandHandler } from '../../../domain/workspace/handlers/workspace.command-handler';
import {
  WorkspaceCommandType,
  TransferOwnershipWorkspaceCommand,
} from '../../../domain/workspace/workspace.command';
import { WorkspaceEventType } from '../../../domain/workspace/workspace.event';
import { Workspace } from '../../../domain/workspace/workspace.entity';
import { IllegalOperationError } from '../../../domain/workspace/workspace.errors';
import { WorkspaceName } from '../../../domain/workspace/values';
import {
  WorkspaceOwner,
  WorkspaceUser,
  SystemAdministrator,
} from '../../../domain/entities';
//#endregion

describe('workspace.command-handler', () => {
  const workspaceCommandHandler = new WorkspaceCommandHandler();

  const workspaceId = 'test';
  const aggregateId = 'test';
  const makeCommandCreator = <T extends WorkspaceCommandType>(type: T) => <
    Value extends Record<string, any>
  >(
    value: Value,
  ) => ({
    type,
    rowId: workspaceId,
    aggregateId,
    payload: value,
  });

  describe('creating workspace', () => {
    it('should produce a `created` event', () => {
      const name = new WorkspaceName('workspace-name');
      const owner = new WorkspaceOwner('99');
      const command = makeCommandCreator(WorkspaceCommandType.create)({
        name,
        createdBy: owner,
      });

      const [result] = workspaceCommandHandler.execute(
        new Workspace(),
        command,
      );

      expect(result.type).toBe(WorkspaceEventType.created);
      expect(result.payload).toEqual({ name: name.get(), createdBy: '99' });
    });

    it('should produce an empty array if workspace already created', async () => {
      const owner = new WorkspaceOwner('99');
      const command = makeCommandCreator(WorkspaceCommandType.create)({
        name: new WorkspaceName('test'),
        createdBy: owner,
      });

      const result = workspaceCommandHandler.execute(
        new Workspace('test', new WorkspaceName('test'), owner),
        command,
      );

      expect(result).toEqual([]);
    });
  });

  describe('workspace change name', () => {
    it('should produce a `nameSet` event', () => {
      const name = new WorkspaceName('workspace-name');
      const changedBy = new WorkspaceOwner('99');
      const command = makeCommandCreator(WorkspaceCommandType.setName)({
        name,
        changedBy,
      });
      const workspace = new Workspace('99', new WorkspaceName('test'));

      const [result] = workspaceCommandHandler.execute(workspace, command);

      expect(result.type).toBe(WorkspaceEventType.nameSet);
      expect(result.payload).toEqual({ name: name.get(), changedBy: '99' });
    });

    it('should throw IllegalOperationError if workspace is deleted', () => {
      const name = new WorkspaceName('workspace-name');
      const changedBy = new WorkspaceOwner('99');
      const command = makeCommandCreator(WorkspaceCommandType.setName)({
        name,
        changedBy,
      });
      const workspace = new Workspace('99', new WorkspaceName('test'));
      workspace.delete();

      const fn = () => workspaceCommandHandler.execute(workspace, command);

      expect(fn).toThrow(IllegalOperationError);
    });
  });

  describe('transferring ownership', () => {
    it('should produce ownership transferred event from user', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const name = new WorkspaceName('test-name');
      const owner = new WorkspaceOwner('99');
      const user = new WorkspaceOwner('88');
      const workspace = new Workspace('test', name, owner);
      const command: TransferOwnershipWorkspaceCommand = {
        type: WorkspaceCommandType.transferOwnership,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        payload: {
          transferredFrom: owner,
          transferredTo: user,
          changedBy: owner,
        },
      };

      const result = workspaceCommandHandler.execute(workspace, command);

      expect(result).toEqual([
        {
          type: WorkspaceEventType.ownershipTransferred,
          rowId: 'WorkspaceAggregate',
          aggregateId: 'test',
          timestamp: new Date(100),
          payload: {
            transferredFrom: '99',
            transferredTo: '88',
            changedByOwner: '99',
          },
        },
      ]);
    });

    it('should produce ownership transferred event from account', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const name = new WorkspaceName('test-name');
      const owner = new WorkspaceOwner('99');
      const user = new WorkspaceOwner('88');
      const workspace = new Workspace('test', name, owner);
      const command: TransferOwnershipWorkspaceCommand = {
        type: WorkspaceCommandType.transferOwnership,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        payload: {
          transferredFrom: owner,
          transferredTo: user,
          changedBy: new SystemAdministrator('root'),
        },
      };

      const result = workspaceCommandHandler.execute(workspace, command);

      expect(result).toEqual([
        {
          type: WorkspaceEventType.ownershipTransferred,
          rowId: 'WorkspaceAggregate',
          aggregateId: 'test',
          timestamp: new Date(100),
          payload: {
            transferredFrom: '99',
            transferredTo: '88',
            changedByAdministrator: 'root',
          },
        },
      ]);
    });

    it('should no events if target owner is already the owner of the workspace', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const name = new WorkspaceName('test-name');
      const owner = new WorkspaceOwner('99');
      const workspace = new Workspace('test', name, owner);
      const command: TransferOwnershipWorkspaceCommand = {
        type: WorkspaceCommandType.transferOwnership,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        payload: {
          transferredFrom: owner,
          transferredTo: owner,
          changedBy: new SystemAdministrator('root'),
        },
      };

      const result = workspaceCommandHandler.execute(workspace, command);

      expect(result).toEqual([]);
    });

    it('should throw an IllegalOperationError if modifying deleted workspace', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const name = new WorkspaceName('test-name');
      const owner = new WorkspaceOwner('99');
      const workspace = new Workspace('test', name, owner);
      workspace.delete();
      const command: TransferOwnershipWorkspaceCommand = {
        type: WorkspaceCommandType.transferOwnership,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        payload: {
          transferredFrom: owner,
          transferredTo: new WorkspaceUser('88'),
          changedBy: new SystemAdministrator('root'),
        },
      };

      const fn = () => workspaceCommandHandler.execute(workspace, command);

      expect(fn).toThrow(IllegalOperationError);
    });
  });

  describe('deleting workspace', () => {
    it('should produce a `deleted` event', () => {
      const owner = new WorkspaceOwner('99');
      const command = makeCommandCreator(WorkspaceCommandType.delete)({
        changedBy: owner,
      });
      const workspace = new Workspace('99', new WorkspaceName('test'));

      const [result] = workspaceCommandHandler.execute(workspace, command);

      expect(result.type).toBe(WorkspaceEventType.deleted);
      expect(result.payload).toEqual({ changedBy: '99' });
    });

    it('should produce no events if workspace is deleted', () => {
      const owner = new WorkspaceOwner('99');
      const command = makeCommandCreator(WorkspaceCommandType.delete)({
        changedBy: owner,
      });
      const workspace = new Workspace('99', new WorkspaceName('test'));
      workspace.delete();

      const results = workspaceCommandHandler.execute(workspace, command);

      expect(results).toEqual([]);
    });
  });
});
