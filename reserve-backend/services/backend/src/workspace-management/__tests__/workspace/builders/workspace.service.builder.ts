/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';

import { WorkspaceService } from '../../../domain/workspace/workspace.service';
import { WorkspaceRepository } from '../../../application/workspace/workspace.repository';
import { WorkspaceRepositoryBuilder } from './workspace.repository.builder';
//#endregion

export class WorkspaceServiceBuilder {
  private repository?: WorkspaceRepository;

  public withRepository(
    repository: WorkspaceRepository,
  ): WorkspaceServiceBuilder {
    this.repository = repository;
    return this;
  }

  public buildingRepository(
    buildUsing: (builder: WorkspaceRepositoryBuilder) => WorkspaceRepository,
  ): WorkspaceServiceBuilder {
    return this.withRepository(buildUsing(new WorkspaceRepositoryBuilder()));
  }

  public build(): WorkspaceService {
    return new WorkspaceService(notNull(this.repository, 'repository'));
  }

  public static inMemory(): WorkspaceService {
    return new WorkspaceServiceBuilder()
      .withRepository(WorkspaceRepositoryBuilder.inMemory())
      .build();
  }
}
