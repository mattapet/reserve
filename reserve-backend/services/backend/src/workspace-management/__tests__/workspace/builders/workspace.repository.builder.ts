/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';
import { EventRepository } from '@rsaas/commons/lib/events/event.repository';
import { InMemoryEventRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-event-repository';
import { SnapshotRepository } from '@rsaas/commons/lib/events/snapshot.repository';
import { InMemorySnapshotRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-snapshot.repository';

import { WorkspaceRepository } from '../../../application/workspace/workspace.repository';
import { WorkspaceEvent } from '../../../domain/workspace/workspace.event';
import {
  WorkspaceEventHandler,
  WorkspaceCommandHandler,
} from '../../../domain/workspace/handlers';
//#endregion

export class WorkspaceRepositoryBuilder {
  private eventRepository?: EventRepository<WorkspaceEvent>;
  private snapshotRepository?: SnapshotRepository;

  public withEventRepository(
    eventRepository: EventRepository<WorkspaceEvent>,
  ): WorkspaceRepositoryBuilder {
    this.eventRepository = eventRepository;
    return this;
  }

  public withSnapshotRepository(
    snapshotRepository: SnapshotRepository,
  ): WorkspaceRepositoryBuilder {
    this.snapshotRepository = snapshotRepository;
    return this;
  }

  public build(): WorkspaceRepository {
    return new WorkspaceRepository(
      notNull(this.eventRepository, 'eventRepository'),
      notNull(this.snapshotRepository, 'snapshotRepository'),
      new WorkspaceCommandHandler(),
      new WorkspaceEventHandler(),
    );
  }

  public static inMemory(): WorkspaceRepository {
    const eventsRepository = new InMemoryEventRepository<WorkspaceEvent>();
    return new WorkspaceRepositoryBuilder()
      .withEventRepository(eventsRepository)
      .withSnapshotRepository(new InMemorySnapshotRepository())
      .build();
  }
}
