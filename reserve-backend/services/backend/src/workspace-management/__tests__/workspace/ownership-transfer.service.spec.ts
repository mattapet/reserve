/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceService } from '../../domain/workspace/workspace.service';

import { IllegalOperationError } from '../../domain/workspace/workspace.errors';
import { WorkspaceServiceBuilder } from './builders/workspace.service.builder';
import { Workspace } from '../../domain/workspace/workspace.entity';
import { WorkspaceName } from '../../domain/workspace/values';
import {
  SystemAdministrator,
  WorkspaceOwner,
  WorkspaceUser,
} from '../../domain/entities';
//#endregion

describe('workpsace.service', () => {
  let service!: WorkspaceService;

  beforeEach(() => {
    service = WorkspaceServiceBuilder.inMemory();
  });

  describe('transferring ownership', () => {
    it('should transfer ownership requested by the owner', async () => {
      const owner = new WorkspaceOwner('99');
      const workspace = new Workspace(
        'test',
        new WorkspaceName('test-name'),
        owner,
      );
      const newOwner = new WorkspaceUser('88');

      await service.transferOwnership(workspace, newOwner, owner);

      expect(workspace.getOwner()).toEqual(new WorkspaceOwner('88'));
    });

    it('should throw IllegalOperationError if owner of different workspace tries to change it', async () => {
      const workspace = new Workspace(
        'test',
        new WorkspaceName('test-name'),
        new WorkspaceOwner('99'),
      );
      const owner = new WorkspaceOwner('77');
      const newOwner = new WorkspaceUser('88');

      const fn = service.transferOwnership(workspace, newOwner, owner);

      await expect(fn).rejects.toThrow(IllegalOperationError);
    });

    it('should transfer ownership requested by a system administrator', async () => {
      const workspace = new Workspace(
        'test',
        new WorkspaceName('test-name'),
        new WorkspaceOwner('99'),
      );
      const administrator = new SystemAdministrator('root');
      const newOwner = new WorkspaceUser('88');

      await service.transferOwnership(workspace, newOwner, administrator);

      expect(workspace.getOwner()).toEqual(new WorkspaceOwner('88'));
    });
  });
});
