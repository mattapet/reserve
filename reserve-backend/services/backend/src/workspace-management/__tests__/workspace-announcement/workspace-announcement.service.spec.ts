/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceAnnouncement } from '../../domain/workspace-announcement/workspace-announcement.entity';
import { WorkspaceAnnouncementService } from '../../domain/workspace-announcement/workspace-announcement.service';
import {
  WorkspaceAnnouncementNotFoundError,
  WorkspaceAnnouncementNameCollisionError,
} from '../../domain/workspace-announcement/workspace-announcement.errors';
import { InMemoryWorkspaceAccouncementRepository } from '../../application/workspace-announcement/repositories/in-memory/in-memory-workspace-announcement.repository';
//#endregion

describe('workspace-announcement.service', () => {
  let service!: WorkspaceAnnouncementService;

  beforeEach(() => {
    service = new WorkspaceAnnouncementService(
      new InMemoryWorkspaceAccouncementRepository(),
    );
  });

  describe('workspace announcement retrieval', () => {
    it('should return an empty array if no announcements are created', async () => {
      const result = await service.getAll('test');

      expect(result).toEqual([]);
    });

    it('should create an inactive workspace announcement', async () => {
      await service.create(
        new WorkspaceAnnouncement(
          'test',
          'test-announcement',
          'some-announcement',
        ),
      );

      const results = await service.getAll('test');
      expect(results).toEqual([
        new WorkspaceAnnouncement(
          'test',
          'test-announcement',
          'some-announcement',
        ),
      ]);
    });

    it('should retrieve workspace by its name', async () => {
      await service.create(
        new WorkspaceAnnouncement(
          'test',
          'test-announcement',
          'some-announcement',
        ),
      );

      const result = await service.getByName('test', 'test-announcement');

      expect(result).toEqual(
        new WorkspaceAnnouncement(
          'test',
          'test-announcement',
          'some-announcement',
        ),
      );
    });

    it('should throw WorkspaceAnnouncementNotFoundError if announcement with given name is not found', async () => {
      await service.create(
        new WorkspaceAnnouncement(
          'test',
          'test-announcement',
          'some-announcement',
        ),
      );

      const fn = service.getByName('test', 'some-name');

      await expect(fn).rejects.toThrow(WorkspaceAnnouncementNotFoundError);
    });

    it('should return undefined if theres no active announcement', async () => {
      await service.create(
        new WorkspaceAnnouncement(
          'test',
          'test-announcement',
          'some-announcement',
        ),
      );

      const result = await service.findActive('test');

      expect(result).toBeUndefined();
    });

    it('should return active announcement', async () => {
      const announcement = new WorkspaceAnnouncement(
        'test',
        'test-announcement',
        'some-announcement',
      );
      await service.create(announcement);
      await service.toggle(announcement);

      const result = await service.findActive('test');

      expect(result).toEqual(announcement);
    });
  });

  describe('crearing announcements', () => {
    it('should throw WorkspaceAnnouncementNameCollisionError announcement with given name already exists', async () => {
      const announcement = new WorkspaceAnnouncement(
        'test',
        'test-announcement',
        'some-announcement',
      );
      await service.create(announcement);

      const fn = service.create(announcement);

      await expect(fn).rejects.toThrow(WorkspaceAnnouncementNameCollisionError);
    });
  });

  describe('toggling announcements', () => {
    it('should activate given announcement', async () => {
      const announcement = new WorkspaceAnnouncement(
        'test',
        'test-announcement',
        'some-announcement',
      );
      await service.create(announcement);

      await service.toggle(announcement);

      expect(announcement.isActive()).toBe(true);
    });

    it('should deactivate currently active announcement', async () => {
      const announcement1 = new WorkspaceAnnouncement(
        'test',
        'test-announcement1',
        'some-announcement',
      );
      await service.create(announcement1);
      const announcement2 = new WorkspaceAnnouncement(
        'test',
        'test-announcement2',
        'some-announcement',
      );
      await service.create(announcement2);
      await service.toggle(announcement1);

      await service.toggle(announcement2);

      const result = await service.getAll('test');
      expect(result).toEqual(
        expect.arrayContaining([
          new WorkspaceAnnouncement(
            'test',
            'test-announcement1',
            'some-announcement',
          ),
          announcement2,
        ]),
      );
    });

    it('should deactivate given announcement', async () => {
      const announcement = new WorkspaceAnnouncement(
        'test',
        'test-announcement',
        'some-announcement',
      );
      await service.create(announcement);
      await service.toggle(announcement);

      await service.toggle(announcement);

      expect(announcement.isActive()).toBe(false);
    });
  });

  describe('removing workspaces', () => {
    it('should be able to remove workspace announcement', async () => {
      const announcement = new WorkspaceAnnouncement(
        'test',
        'test-announcement',
        'some-announcement',
      );
      await service.create(announcement);

      await service.remove(announcement);

      const result = await service.getAll('test');
      expect(result).toEqual([]);
    });
  });
});
