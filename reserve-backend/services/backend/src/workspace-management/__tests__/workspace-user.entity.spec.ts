/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceUser } from '../domain/entities/workspace-user.entity';
import { Identity, User } from '../../user-access';
import { IllegalOperationError } from '../domain/workspace';
//#endregion

describe('workspace-user.entity', () => {
  it('should create a workspace user', () => {
    const user = new User(
      '99',
      'test',
      new Identity('99', 'test', 'test@test.com'),
    );

    const workspaceUser = WorkspaceUser.from(user);

    expect(workspaceUser.getId()).toBe('99');
  });

  it('should throw an IllegalOperationError if user is deleted', () => {
    const user = new User(
      '99',
      'test',
      new Identity('99', 'test', 'test@test.com'),
    );
    user.delete();

    const fn = () => WorkspaceUser.from(user);

    expect(fn).toThrow(IllegalOperationError);
  });
});
