/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import { User, Identity, UserRole } from '../../user-access';
import { WorkspaceOwner } from '../domain/entities/workspace-owner.entity';
import { IllegalOperationError } from '../domain/workspace';

describe('workspace-owner.entity', () => {
  it('should create a workspace owner from a workspace owner', () => {
    const user = new User(
      '99',
      'test',
      new Identity('99', 'test', 'test@test.com'),
      UserRole.admin,
    );
    user.claimOwnership();

    const owner = WorkspaceOwner.from(user);

    expect(owner.getId()).toBe(user.getId());
  });

  it('should throw an IllegalOperationError when user is not a workspace owner', () => {
    const user = new User(
      '99',
      'test',
      new Identity('99', 'test', 'test@test.com'),
      UserRole.admin,
    );

    const fn = () => WorkspaceOwner.from(user);

    expect(fn).toThrow(IllegalOperationError);
  });
});
