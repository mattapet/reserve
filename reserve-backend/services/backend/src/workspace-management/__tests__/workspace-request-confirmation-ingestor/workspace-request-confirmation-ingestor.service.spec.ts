/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { WorkspaceRequestConfirmationIngestorService } from '../../application/workspace-request-confirmation-ingestor/workspace-request-confirmation-ingestor.service';

import { WorkspaceRequestRepositoryBuilder } from '../workspace-request/builders/workspace-request.repository.builder';
import { Workspace } from '../../domain/workspace/workspace.entity';
import { WorkspaceServiceBuilder } from '../workspace/builders/workspace.service.builder';
import { WorkspaceRequest } from '../../domain/workspace-request/workspace-request.entity';
import { WorkspaceRequestEventType } from '../../domain/workspace-request/workspace-request.event';
import { WorkspaceRequestService } from '../../domain/workspace-request/workspace-request.service';
import { WorkspaceName } from '../../domain/workspace/values';
import { WorkspaceOwner } from '../../domain/entities';
//#endregion

describe('workspace-confirmation-ingestor.service', () => {
  const workspaceService = WorkspaceServiceBuilder.inMemory();
  const workspaceRequestService = new WorkspaceRequestService(
    WorkspaceRequestRepositoryBuilder.inMemory(),
    workspaceService,
  );
  const service = new WorkspaceRequestConfirmationIngestorService(
    workspaceService,
    workspaceRequestService,
  );

  it('should create a new workspace on workspace request confirmation', async () => {
    await workspaceRequestService.open(new WorkspaceRequest(), {
      workspaceId: 'test',
      workspaceName: new WorkspaceName('test-name'),
      requestedBy: new WorkspaceOwner('99'),
    });

    await service.consumeConfirmed({
      type: WorkspaceRequestEventType.confirmed,
      rowId: 'WorkspaceRequestAggregate',
      aggregateId: 'test',
      timestamp: new Date(100),
      payload: {
        confirmedBy: 'root',
      },
    });

    const workspace = await workspaceService.getById('test');
    expect(workspace).toEqual(
      new Workspace(
        'test',
        new WorkspaceName('test-name'),
        new WorkspaceOwner('99'),
      ),
    );
  });
});
