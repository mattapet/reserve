/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ModuleRef } from '@nestjs/core';
import { Module, OnModuleInit } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons';

import { AuthorityService } from '../../domain/authority/authority.service';
import { AuthorityAuthorizationService } from '../../domain/authority/authority-authorization.service';

import { SiliconHillAuthority } from '../../application/authority/authorities/silicon-hill/silicon-hill-authority';
//#endregions

@Module({
  imports: [CommonsModule],
  providers: [
    AuthorityService,
    AuthorityAuthorizationService,
    SiliconHillAuthority,
  ],
  exports: [AuthorityService, AuthorityAuthorizationService],
})
export class AuthorityModule implements OnModuleInit {
  public constructor(
    private readonly moduleRef: ModuleRef,
    private readonly siliconHillAuthority: SiliconHillAuthority,
  ) {}

  public onModuleInit(): void {
    const service = this.moduleRef.get(AuthorityService);
    service.register(this.siliconHillAuthority);
  }
}
