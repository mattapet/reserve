/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons';

import { IdentityModule } from '../identity';

import { UserService } from '../../domain/user/user.service';
import {
  UserCommandHandler,
  UserEventHandler,
} from '../../domain/user/handlers';
import { USER_REPOSITORY } from '../../domain/user/constants';

import { UserRepository } from '../../application/user/user.repository';
//#endregion

@Module({
  imports: [CommonsModule, IdentityModule],
  providers: [
    UserService,
    {
      provide: USER_REPOSITORY,
      useClass: UserRepository,
    },
    UserCommandHandler,
    UserEventHandler,
  ],
  exports: [UserService],
})
export class UserModule {}
