/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons';

import { UserModule } from '../user';

import { OwnershipClaimer } from '../../application/transfer-ownership/ownership-claimer.service';
import { OwnershipDenier } from '../../application/transfer-ownership/ownership-denier.service';

//#endregion

@Module({
  imports: [CommonsModule, UserModule],
  providers: [OwnershipClaimer, OwnershipDenier],
})
export class TransferOwnershipModule {}
