/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserModule } from '../../user';

import { AccessToken } from '../../../domain/authentication/token/access-token.entity';
import { RefreshToken } from '../../../domain/authentication/token/refresh-token.entity';
import { TokenService } from '../../../domain/authentication/token/token.service';
import { TOKEN_REPOSITORY } from '../../../domain/authentication/token/constants';

import { TypeormTokenRepository } from '../../../application/authentication/token/repositories/typeorm/typeorm-token.repository';
import { AccessTokenRepository } from '../../../application/authentication/token/repositories/typeorm/access-token.repository';
import { RefreshTokenRepository } from '../../../application/authentication/token/repositories/typeorm/refresh-token.repository';
//#endregion

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AccessToken,
      RefreshToken,
      AccessTokenRepository,
      RefreshTokenRepository,
    ]),
    UserModule,
  ],
  providers: [
    TokenService,
    { provide: TOKEN_REPOSITORY, useClass: TypeormTokenRepository },
  ],
  exports: [TokenService],
})
export class TokenModule {}
