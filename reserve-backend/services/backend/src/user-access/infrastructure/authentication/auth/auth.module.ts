/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { UserModule } from '../../user';
import { AuthorityModule } from '../../authority';

import { AuthService } from '../../../domain/authentication/auth/auth.service';
//#endregion

@Module({
  imports: [UserModule, AuthorityModule],
  providers: [AuthService],
  exports: [AuthService],
})
export class AuthModule {}
