/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Client } from '../../../../domain/authentication/oauth2/client/client.entity';
//#endregion

@Module({
  imports: [TypeOrmModule.forFeature([Client])],
})
export class ClientModule {}
