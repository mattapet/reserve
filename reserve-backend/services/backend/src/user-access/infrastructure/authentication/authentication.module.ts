/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { AuthModule } from './auth';
import { TokenModule } from './token';
import { OAuth2Module } from './oauth2';
//#endregion

@Module({
  imports: [AuthModule, TokenModule, OAuth2Module],
  exports: [AuthModule, TokenModule, OAuth2Module],
})
export class AuthenticationModule {}
