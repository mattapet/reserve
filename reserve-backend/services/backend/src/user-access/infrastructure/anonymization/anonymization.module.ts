/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { CommonsModule } from '@rsaas/commons';

import { IdentityModule } from '../identity';

import { AnonymizationService } from '../../application/anonymization/anonymization.services';
//#endregion

@Module({
  imports: [CommonsModule, IdentityModule],
  providers: [AnonymizationService],
})
export class AnonymizationModule {}
