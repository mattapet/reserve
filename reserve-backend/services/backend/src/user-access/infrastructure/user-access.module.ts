/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';

import { UserModule } from './user';
import { AnonymizationModule } from './anonymization';
import { TransferOwnershipModule } from './transfer-ownership';
import { AuthenticationModule } from './authentication';
import { AuthorityModule } from './authority';
//#endregion

@Module({
  imports: [
    UserModule,
    AuthenticationModule,
    AuthorityModule,
    AnonymizationModule,
    TransferOwnershipModule,
  ],
  exports: [UserModule, AuthenticationModule, AuthorityModule],
})
export class UserAccessModule {}
