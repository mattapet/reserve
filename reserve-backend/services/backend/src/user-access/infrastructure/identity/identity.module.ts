/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Identity, IdentityService } from '../../domain/identity';
import { IDENTITY_REPOSITORY } from '../../domain/identity/constants';

import { TypeormIdentityRepository } from '../../application/identity/repositories/typeorm';
//#endregion

@Module({
  imports: [TypeOrmModule.forFeature([Identity, TypeormIdentityRepository])],
  providers: [
    IdentityService,
    {
      provide: IDENTITY_REPOSITORY,
      useFactory: (repo) => repo,
      inject: [TypeormIdentityRepository],
    },
  ],
  exports: [IdentityService],
})
export class IdentityModule {}
