/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AuthorityService } from '../../domain/authority/authority.service';
import { Authority } from '../../domain/authority/authority.entity';
import {
  AuthorityConflictError,
  AuthorityNotFoundError,
} from '../../domain/authority/authority.errors';
import { AuthorityMockBuilder } from './authority-mock.builder';
//#endregion

describe('authority login', () => {
  let service!: AuthorityService;

  beforeEach(() => {
    service = new AuthorityService();
  });

  function make_authorityWithName(name: string): Authority {
    return new AuthorityMockBuilder().withName(name).build();
  }

  it('should register an authority', () => {
    const authority = make_authorityWithName('mock-authority');

    service.register(authority);

    expect(service.getByName('mock-authority')).toBe(authority);
  });

  it('should return throw AuthorityNotFoundError if theres no authority with specified name', () => {
    const fn = () => service.getByName('mock-authority');
    expect(fn).toThrow(AuthorityNotFoundError);
  });

  it('should throw an error if trying to register two authorities with same name', () => {
    const authority1 = make_authorityWithName('mock-authority');
    const authority2 = make_authorityWithName('mock-authority');

    service.register(authority1);

    expect(() => service.register(authority2)).toThrow(AuthorityConflictError);
  });
});
