/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  AuthorizationCodeError,
  AuthorizedUserFetchError,
} from '../../domain/authority/authority.errors';
import { AuthorityMockBuilder } from './authority-mock.builder';
import { AuthorityAuthorizationService } from '../../domain/authority/authority-authorization.service';
//#endregion

describe('authority login', () => {
  let service!: AuthorityAuthorizationService;

  beforeEach(() => {
    service = new AuthorityAuthorizationService();
  });

  describe('query generation', () => {
    it('should create a query from the given authority', () => {
      const authority = new AuthorityMockBuilder()
        .withClientId('client_id')
        .withRedirectUri('http://localhost:3000/redirect_uri')
        .withScope('read write')
        .build();

      const query = service.getAuthorizationQuery('test', authority);

      expect(query).toEqual({
        client_id: 'client_id',
        redirect_uri: 'http://localhost:3000/redirect_uri',
        scope: 'read write',
        state: 'test',
        response_type: 'code',
      });
    });
  });

  describe('user authorization', () => {
    it('should get authorized user', async () => {
      const authority = new AuthorityMockBuilder()
        .mockingFetchAuthorizedUser(() =>
          Promise.resolve({
            email: 'test@test.com',
            firstName: 'Test',
            lastName: 'Testovic',
          }),
        )
        .build();

      const user = await service.authorizeUser('test', '12345', authority);

      expect(user).toEqual({
        email: 'test@test.com',
        firstName: 'Test',
        lastName: 'Testovic',
      });
    });

    it('should throw an error AuthorizationCodeError', async () => {
      const authority = new AuthorityMockBuilder()
        .mockingExchangeAuthorizationCode(async () => {
          throw new Error();
        })
        .build();

      await expect(
        service.authorizeUser('test', '12345', authority),
      ).rejects.toThrow(AuthorizationCodeError);
    });

    it('should throw an error AuthorizedUserFetchError', async () => {
      const authority = new AuthorityMockBuilder()
        .mockingFetchAuthorizedUser(async () => {
          throw new Error();
        })
        .build();

      await expect(
        service.authorizeUser('test', '12345', authority),
      ).rejects.toThrow(AuthorizedUserFetchError);
    });

    it('should fetch user using fetched access token', async () => {
      let capturedAccessToken!: string;
      const authorizedUser = {
        email: 'test@test.com',
        firstName: 'Test',
        lastName: 'Testovic',
      };
      const authority = new AuthorityMockBuilder()
        .mockingExchangeAuthorizationCode(async () => 'access_token')
        .mockingFetchAuthorizedUser(async (accessToken: string) => {
          capturedAccessToken = accessToken;
          return authorizedUser;
        })
        .build();

      const user = await service.authorizeUser('test', '12345', authority);

      expect(capturedAccessToken).toEqual('access_token');
      expect(user).toEqual(authorizedUser);
    });
  });
});
