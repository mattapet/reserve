/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Authority } from '../../domain/authority/authority.entity';
import { AuthorizedUser } from '../../domain/authority/authorized-user.interface';
//#endregion

const AuthorityMock = jest.fn<Authority, []>(() => ({
  getName: jest.fn(),
  getClientId: jest.fn(),
  getClientSecret: jest.fn(),
  getRedirectUri: jest.fn(),
  getScope: jest.fn(),
  exchangeAuthorizationCode: jest.fn(),
  fetchAuthorizedUser: jest.fn(),
  getAuthorizationUrl: jest.fn(),
}));

export class AuthorityMockBuilder {
  private authorityMock: Authority = new AuthorityMock();

  withName(name: string): AuthorityMockBuilder {
    jest.spyOn(this.authorityMock, 'getName').mockImplementation(() => name);
    return this;
  }

  withClientId(clientId: string): AuthorityMockBuilder {
    jest
      .spyOn(this.authorityMock, 'getClientId')
      .mockImplementation(() => clientId);
    return this;
  }

  withClientSecret(clientSecret: string): AuthorityMockBuilder {
    jest
      .spyOn(this.authorityMock, 'getClientSecret')
      .mockImplementation(() => clientSecret);
    return this;
  }

  withRedirectUri(redirectUri: string): AuthorityMockBuilder {
    jest
      .spyOn(this.authorityMock, 'getRedirectUri')
      .mockImplementation(() => redirectUri);
    return this;
  }

  withScope(scope: string): AuthorityMockBuilder {
    jest.spyOn(this.authorityMock, 'getScope').mockImplementation(() => scope);
    return this;
  }

  withAuthorizationUrl(authorizationUrl: string): AuthorityMockBuilder {
    jest
      .spyOn(this.authorityMock, 'getAuthorizationUrl')
      .mockImplementation(() => authorizationUrl);
    return this;
  }

  mockingExchangeAuthorizationCode(
    fn: (code: string) => Promise<string>,
  ): AuthorityMockBuilder {
    jest
      .spyOn(this.authorityMock, 'exchangeAuthorizationCode')
      .mockImplementation(fn);
    return this;
  }

  mockingFetchAuthorizedUser(
    fn: (code: string) => Promise<AuthorizedUser>,
  ): AuthorityMockBuilder {
    jest
      .spyOn(this.authorityMock, 'fetchAuthorizedUser')
      .mockImplementation(fn);
    return this;
  }

  build(): Authority {
    return this.authorityMock;
  }
}
