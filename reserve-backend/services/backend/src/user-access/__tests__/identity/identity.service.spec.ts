/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IdentityService } from '../../domain/identity/identity.service';

import { AnyObject } from '@rsaas/util/lib/AnyObject';

import { IdentityServiceBuilder } from './builders/identity.service.builder';
import { IdentityBuilder } from './builders/identity.builder';
import { InMemoryIdenittyRepository } from '../../application/identity/repositories/in-memory/in-memory-identity.repository';
import { Identity } from '../../domain/identity/identity.entity';
//#endregion

jest.mock('../../domain/identity/services/encrypt', () => ({
  encrypt: () => 'encrypted',
}));

describe('identity.service', () => {
  let identitySerivce!: IdentityService;

  beforeEach(() => {
    identitySerivce = IdentityServiceBuilder.inMemory();
  });

  function make_identityServiceWithStorage(
    storage: AnyObject,
  ): IdentityService {
    return new IdentityServiceBuilder()
      .withRepository(new InMemoryIdenittyRepository(storage))
      .build();
  }

  function make_defaultIdentity(): Identity {
    return new IdentityBuilder()
      .withUserId('12345')
      .withWorkspaceId('test')
      .withEmail('test@test.com')
      .build();
  }

  async function effect_insertDefaultIdentity(): Promise<void> {
    await identitySerivce.create(make_defaultIdentity());
  }

  describe('creating identities', () => {
    it('should create identity without first or last name', async () => {
      await identitySerivce.create(
        new IdentityBuilder()
          .withUserId('12345')
          .withWorkspaceId('test')
          .withEmail('test@test.com')
          .build(),
      );

      const user = await identitySerivce.getByEmail('test', 'test@test.com');
      expect(user).toEqual(
        new IdentityBuilder()
          .withUserId('12345')
          .withWorkspaceId('test')
          .withEmail('test@test.com')
          .build(),
      );
    });

    it('should create identity with first and last names', async () => {
      await identitySerivce.create(
        new IdentityBuilder()
          .withUserId('12345')
          .withFirstName('Test')
          .withLastName('Testovic')
          .withWorkspaceId('test')
          .withEmail('test@test.com')
          .build(),
      );

      const user = await identitySerivce.getByEmail('test', 'test@test.com');
      expect(user).toEqual(
        new IdentityBuilder()
          .withUserId('12345')
          .withFirstName('Test')
          .withLastName('Testovic')
          .withWorkspaceId('test')
          .withEmail('test@test.com')
          .build(),
      );
    });
  });

  describe('updating identities', () => {
    it('should update users first and last name', async () => {
      await effect_insertDefaultIdentity();

      await identitySerivce.update(
        new IdentityBuilder()
          .withUserId('12345')
          .withFirstName('Test')
          .withLastName('Testovic')
          .withWorkspaceId('test')
          .withEmail('test@test.com')
          .build(),
      );

      const user = await identitySerivce.getByUserId('test', '12345');
      expect(user).toEqual(
        new IdentityBuilder()
          .withUserId('12345')
          .withFirstName('Test')
          .withLastName('Testovic')
          .withWorkspaceId('test')
          .withEmail('test@test.com')
          .build(),
      );
    });

    it('should set users password', async () => {
      const identity = make_defaultIdentity();
      await identitySerivce.create(identity);

      await identitySerivce.setPassword(identity, 'password');

      const user = await identitySerivce.getByUserId('test', '12345');
      expect(user.getPassword()).toBe('password');
    });

    it('should override users password', async () => {
      const identity = make_defaultIdentity();
      await identitySerivce.create(identity);

      await identitySerivce.setPassword(identity, 'password1');
      await identitySerivce.setPassword(identity, 'password2');

      const user = await identitySerivce.getByUserId('test', '12345');
      expect(user.getPassword()).toBe('password2');
    });
  });

  describe('user anonymization', () => {
    it('should not be able to get anonymizd user', async () => {
      const storage: AnyObject = {};
      const service = make_identityServiceWithStorage(storage);
      const identity = make_defaultIdentity();
      await identitySerivce.create(identity);

      await service.anonymize(identity);

      const user = await identitySerivce.getByUserId('test', '12345');
      expect(user).toEqual(
        new IdentityBuilder()
          .withUserId('12345')
          .withWorkspaceId('test')
          .withFirstName('encrypted')
          .withLastName('encrypted')
          .withEmail('encrypted')
          .withAnonymized(true)
          .build(),
      );
    });

    it('should enpcrypt all users from workspace', async () => {
      await effect_insertDefaultIdentity();

      await identitySerivce.anonymizeByWorkspaceId('test');

      const identities = await identitySerivce.getAll('test');
      expect(identities).toEqual([
        new IdentityBuilder()
          .withUserId('12345')
          .withWorkspaceId('test')
          .withFirstName('encrypted')
          .withLastName('encrypted')
          .withEmail('encrypted')
          .withAnonymized(true)
          .build(),
      ]);
    });
  });
});
