/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';

import { IdentityService } from '../../../domain/identity/identity.service';
import { IdentityRepository } from '../../../domain/identity/identity.repository';

import { InMemoryIdenittyRepository } from '../../../application/identity/repositories/in-memory/in-memory-identity.repository';
//#endregion

export class IdentityServiceBuilder {
  private idenityRepository?: IdentityRepository;

  public withRepository(
    idenityRepository: IdentityRepository,
  ): IdentityServiceBuilder {
    this.idenityRepository = idenityRepository;
    return this;
  }

  public build(): IdentityService {
    return new IdentityService(
      notNull(this.idenityRepository, 'identityRepository'),
    );
  }

  public static inMemory(): IdentityService {
    return new IdentityServiceBuilder()
      .withRepository(new InMemoryIdenittyRepository())
      .build();
  }
}
