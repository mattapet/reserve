/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';

import { Identity } from '../../../domain/identity/identity.entity';
//#endregion

export class IdentityBuilder {
  private userId?: string;
  private workspaceId?: string;
  private email?: string;
  private firstName: string = '';
  private lastName: string = '';
  private phone?: string;
  private password?: string;
  private anonymized: boolean = false;

  public withUserId(userId: string): IdentityBuilder {
    this.userId = userId;
    return this;
  }
  public withWorkspaceId(workspaceId: string): IdentityBuilder {
    this.workspaceId = workspaceId;
    return this;
  }
  public withEmail(email: string): IdentityBuilder {
    this.email = email;
    return this;
  }
  public withFirstName(firstName: string): IdentityBuilder {
    this.firstName = firstName;
    return this;
  }
  public withLastName(lastName: string): IdentityBuilder {
    this.lastName = lastName;
    return this;
  }
  public withPhone(phone: string): IdentityBuilder {
    this.phone = phone;
    return this;
  }
  public withPassword(password: string): IdentityBuilder {
    this.password = password;
    return this;
  }
  public withAnonymized(anonymized: boolean): IdentityBuilder {
    this.anonymized = anonymized;
    return this;
  }

  public build(): Identity {
    return new Identity(
      notNull(this.userId, 'userId'),
      notNull(this.workspaceId, 'workspaceId'),
      notNull(this.email, 'email'),
      this.firstName,
      this.lastName,
      this.phone,
      this.password,
      this.anonymized,
    );
  }
}
