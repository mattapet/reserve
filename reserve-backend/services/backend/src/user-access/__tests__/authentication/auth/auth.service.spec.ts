/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AuthService } from '../../../domain/authentication/auth/auth.service';

import { User, UserService, UserRole } from '../../../domain/user';
import { Identity } from '../../../domain/identity';
import { UserServiceBuilder } from '../../user/builders/user.service.builder';

import {
  InvalidEmailOrPasswordError,
  AuthorityAuthorizationError,
  UserBannedError,
} from '../../../domain/authentication/auth/auth.errors';
import { Workspace, WorkspaceName } from '../../../../workspace-management';
import { AuthorityMockBuilder } from '../../authority/authority-mock.builder';
import { AuthorityAuthorizationService } from '../../../domain/authority';
//#endregion

jest.mock('bcryptjs', () => ({
  hash: (pwd: string) => Promise.resolve(`${pwd}_hashed`),
  compare: (pwd: string, hash: string) =>
    Promise.resolve(`${pwd}_hashed` === hash),
}));

describe('auth.service', () => {
  let userService!: UserService;
  let authorityService!: AuthorityAuthorizationService;
  let service!: AuthService;

  beforeEach(() => {
    userService = UserServiceBuilder.inMemory();
    authorityService = new AuthorityAuthorizationService();
    service = new AuthService(userService, authorityService);
  });

  function make_adminUser(): User {
    return new User(
      '44',
      'test',
      new Identity('44', 'test', ''),
      UserRole.admin,
    );
  }

  describe('password login', () => {
    it('should return authentivated user by password', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 200);
      const workspace = new Workspace('test', new WorkspaceName('test'));
      const user = new User();
      await userService.create(
        user,
        new Identity('99', 'test', 'test@test.com'),
      );
      await service.setPassword(user, '12345');
      user.setLastLogin(new Date(200));

      const result = await service.loginPassword(
        workspace,
        'test@test.com',
        '12345',
      );

      expect(result).toEqual(user);
    });

    it('should throw UserBannedError if user is banned', async () => {
      const workspace = new Workspace('test', new WorkspaceName('test'));
      const user = new User();
      await userService.create(
        user,
        new Identity('99', 'test', 'test@test.com'),
      );
      await userService.toggleBanned(user, make_adminUser());
      await service.setPassword(user, '12345');

      const fn = service.loginPassword(workspace, 'test@test.com', '12345');

      await expect(fn).rejects.toThrow(UserBannedError);
    });

    it('should throw InvalidEmailOrPasswordError if passwords do not match', async () => {
      const workspace = new Workspace('test', new WorkspaceName('test'));
      const user = new User();
      await userService.create(
        user,
        new Identity('99', 'test', 'test@test.com'),
      );
      await service.setPassword(user, '12345');

      const fn = service.loginPassword(workspace, 'test@test.com', 'password');

      await expect(fn).rejects.toThrow(InvalidEmailOrPasswordError);
    });

    it('should throw InvalidEmailOrPasswordError if user does not have a password set', async () => {
      const workspace = new Workspace('test', new WorkspaceName('test'));
      const user = new User();
      await userService.create(
        user,
        new Identity('99', 'test', 'test@test.com'),
      );

      const fn = service.loginPassword(workspace, 'test@test.com', 'password');

      await expect(fn).rejects.toThrow(InvalidEmailOrPasswordError);
    });

    it('should throw InvalidEmailOrPasswordError if user does not exists', async () => {
      const workspace = new Workspace('test', new WorkspaceName('test'));

      const fn = service.loginPassword(workspace, 'test@test.com', '12345');

      await expect(fn).rejects.toThrow(InvalidEmailOrPasswordError);
    });
  });

  describe('authority login', () => {
    it('should login a new user with authority extracting all the information', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 200);
      const workspace = new Workspace('test', new WorkspaceName('test'));
      const authority = new AuthorityMockBuilder()
        .mockingFetchAuthorizedUser(() =>
          Promise.resolve({
            email: 'test@test.com',
            firstName: 'Test',
            lastName: 'Testovic',
          }),
        )
        .build();
      const code = '12435';

      const user = await service.loginAuthority(workspace, authority, code);

      expect(user.getEmail()).toBe('test@test.com');
      expect(user.getIdentity().getFirstName()).toBe('Test');
      expect(user.getIdentity().getLastName()).toBe('Testovic');
      expect(user.getLastLogin()).toEqual(new Date(200));
    });

    it('should throw UserBannedError if user is banned', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 200);
      const workspace = new Workspace('test', new WorkspaceName('test'));
      const authority = new AuthorityMockBuilder()
        .mockingFetchAuthorizedUser(() =>
          Promise.resolve({
            email: 'test@test.com',
            firstName: 'Test',
            lastName: 'Testovic',
          }),
        )
        .build();
      const code = '12435';
      const user = new User();
      await userService.create(
        user,
        new Identity('99', 'test', 'test@test.com'),
      );
      await userService.toggleBanned(user, make_adminUser());

      const fn = service.loginAuthority(workspace, authority, code);

      await expect(fn).rejects.toThrow(UserBannedError);
    });

    it('should login an existing user with authority filling missing information', async () => {
      const workspace = new Workspace('test', new WorkspaceName('test'));
      const authority = new AuthorityMockBuilder()
        .mockingFetchAuthorizedUser(() =>
          Promise.resolve({
            email: 'test@test.com',
            firstName: 'Test',
            lastName: 'Testovic',
          }),
        )
        .build();
      const code = '12435';
      await userService.create(
        new User(),
        new Identity('99', 'test', 'test@test.com'),
      );

      const user = await service.loginAuthority(workspace, authority, code);

      expect(user.getId()).toBe('99');
      expect(user.getEmail()).toBe('test@test.com');
      expect(user.getIdentity().getFirstName()).toBe('Test');
      expect(user.getIdentity().getLastName()).toBe('Testovic');
    });

    it('should throw AuthorityAuthorizationError if authority throws', async () => {
      const workspace = new Workspace('test', new WorkspaceName('test'));
      const authority = new AuthorityMockBuilder()
        .mockingFetchAuthorizedUser(() => Promise.reject(new Error()))
        .build();
      const code = '12345';

      const fn = service.loginAuthority(workspace, authority, code);

      await expect(fn).rejects.toThrow(AuthorityAuthorizationError);
    });
  });
});
