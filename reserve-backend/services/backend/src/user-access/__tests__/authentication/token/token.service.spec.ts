/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { TokenService } from '../../../domain/authentication/token/token.service';

import { Scope } from '@rsaas/util/lib/oauth2/scope.type';
import { User } from '../../../domain/user';
import { Identity } from '../../../domain/identity';

import { InMemoryTokenRepository } from '../../../application/authentication/token/repositories/in-memory/in-memory-console-token.repository';
import {
  InvalidTokenError,
  TokenNotFoundError,
} from '../../../domain/authentication/token/token.errors';
//#endregion

describe('token.service', () => {
  let service!: TokenService;

  beforeEach(() => {
    service = new TokenService(new InMemoryTokenRepository());
  });

  function make_userWithId(userId: string): User {
    return new User(userId, 'test', new Identity(userId, 'test', ''));
  }

  describe('token generation', () => {
    it('should generate a pair of valid tokens', async () => {
      const user = make_userWithId('99');

      const [at, rt] = await service.generateTokenPair(user, []);

      expect(at.isValid()).toBe(true);
      expect(rt.isValid()).toBe(true);
    });

    it('should refresh an access token', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const user = make_userWithId('99');
      const [, rt] = await service.generateTokenPair(user, []);

      const at = await service.refreshAccessToken(rt);

      expect(at.isValid()).toBe(true);
    });

    it('should refresh an access token with the same user', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const user = make_userWithId('99');
      const [, rt] = await service.generateTokenPair(user, []);

      const at = await service.refreshAccessToken(rt);

      expect(at.userId).toBe('99');
    });

    it('should refresh an access token with the same scope', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const user = make_userWithId('99');
      const [, rt] = await service.generateTokenPair(user, [
        Scope.reservationRead,
      ]);

      const at = await service.refreshAccessToken(rt);

      expect(at.scope).toEqual([Scope.reservationRead]);
    });

    it('should throw InvlidTokenError if refresh token is expired', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const user = make_userWithId('99');
      const [, rt] = await service.generateTokenPair(user, []);
      jest.spyOn(Date, 'now').mockImplementation(() => 1_000_0000_000);

      const fn = service.refreshAccessToken(rt);

      await expect(fn).rejects.toThrow(InvalidTokenError);
    });

    it('should throw InvlidTokenError if refresh token is revoked', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const user = make_userWithId('99');
      const [, rt] = await service.generateTokenPair(user, []);
      rt.revoke();

      const fn = service.refreshAccessToken(rt);

      await expect(fn).rejects.toThrow(InvalidTokenError);
    });
  });

  describe('token revokation', () => {
    it('should revoke given refresh token', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const user = make_userWithId('99');
      const [, rt] = await service.generateTokenPair(user, []);

      await service.revokeRefreshToken(rt);

      expect(rt.isRevoked()).toBe(true);
    });

    it('should throw InvalidTokenError if token is already revoked', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const user = make_userWithId('99');
      const [, rt] = await service.generateTokenPair(user, []);
      rt.revoke();

      const fn = service.revokeRefreshToken(rt);

      await expect(fn).rejects.toThrow(InvalidTokenError);
    });

    it('should throw InvalidTokenError if token is expired', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const user = make_userWithId('99');
      const [, rt] = await service.generateTokenPair(user, []);
      jest.spyOn(Date, 'now').mockImplementation(() => 1_000_000_000);

      const fn = service.revokeRefreshToken(rt);

      await expect(fn).rejects.toThrow(InvalidTokenError);
    });
  });

  describe('retrieving tokens', () => {
    it('should retrieve an access token', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const user = make_userWithId('99');
      const [at] = await service.generateTokenPair(user, []);

      const result = await service.getAccessToken(at.value);

      expect(result).toEqual(at);
    });

    it('should retrieve a refresh token', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 0);
      const user = make_userWithId('99');
      const [, rt] = await service.generateTokenPair(user, []);

      const result = await service.getRefreshToken(rt.value);

      expect(result).toEqual(rt);
    });

    it('should throw TokenNotFoundError if access token does not exist', async () => {
      const fn = service.getAccessToken('some-random-token-value');

      await expect(fn).rejects.toThrow(TokenNotFoundError);
    });

    it('should throw TokenNotFoundError if refresh token does not exist', async () => {
      const fn = service.getRefreshToken('some-random-token-value');

      await expect(fn).rejects.toThrow(TokenNotFoundError);
    });
  });
});
