/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { OwnershipClaimer } from '../../application/transfer-ownership/ownership-claimer.service';
import { OwnershipDenier } from '../../application/transfer-ownership/ownership-denier.service';

import {
  WorkspaceOwnershipTransferredEvent,
  WorkspaceEventType,
} from '../../../workspace-management';

import { UserServiceBuilder } from '../user/builders/user.service.builder';
import { Identity } from '../../domain/identity';
import { User } from '../../domain/user/user.entity';
//#endregion

describe('ownership transfer', () => {
  describe('ownership-claimer.service', () => {
    it('should make new owner an admin', async () => {
      const userService = UserServiceBuilder.inMemory();
      const service = new OwnershipClaimer(userService);
      const event: WorkspaceOwnershipTransferredEvent = {
        eventId: 1,
        type: WorkspaceEventType.ownershipTransferred,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          transferredFrom: '99',
          transferredTo: '88',
          changedByOwner: '99',
        },
      };
      await userService.create(new User(), new Identity('88', 'test', ''));

      await service.consumeOwnershipTransfered(event);

      const newOwner = await userService.getById('test', '88');
      expect(newOwner.isAdmin()).toBe(true);
    });

    it('should make new owner an owner', async () => {
      const userService = UserServiceBuilder.inMemory();
      const service = new OwnershipClaimer(userService);
      const event: WorkspaceOwnershipTransferredEvent = {
        eventId: 1,
        type: WorkspaceEventType.ownershipTransferred,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          transferredFrom: '99',
          transferredTo: '88',
          changedByOwner: '99',
        },
      };
      await userService.create(new User(), new Identity('88', 'test', ''));

      await service.consumeOwnershipTransfered(event);

      const newOwner = await userService.getById('test', '88');
      expect(newOwner.isOwner()).toBe(true);
    });
  });

  describe('ownership-claimer.service', () => {
    it('should not demote old owner', async () => {
      const userService = UserServiceBuilder.inMemory();
      const service = new OwnershipDenier(userService);
      const event: WorkspaceOwnershipTransferredEvent = {
        eventId: 1,
        type: WorkspaceEventType.ownershipTransferred,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          transferredFrom: '99',
          transferredTo: '88',
          changedByOwner: '99',
        },
      };
      await userService.createOwner(new User(), new Identity('99', 'test', ''));

      await service.consumeOwnershipTransfered(event);

      const oldOwner = await userService.getById('test', '99');
      expect(oldOwner.isAdmin()).toBe(true);
    });

    it('should make old owner not an owner any more', async () => {
      const userService = UserServiceBuilder.inMemory();
      const service = new OwnershipDenier(userService);
      const event: WorkspaceOwnershipTransferredEvent = {
        eventId: 1,
        type: WorkspaceEventType.ownershipTransferred,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          transferredFrom: '99',
          transferredTo: '88',
          changedByOwner: '99',
        },
      };
      await userService.createOwner(new User(), new Identity('99', 'test', ''));

      await service.consumeOwnershipTransfered(event);

      const oldOwner = await userService.getById('test', '99');
      expect(oldOwner.isOwner()).toBe(false);
    });
  });
});
