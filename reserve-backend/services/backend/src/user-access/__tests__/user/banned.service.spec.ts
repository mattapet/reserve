/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from '@rsaas/util/lib/user-role.type';
import {
  IllegalOperationError,
  InsufficientRightsError,
} from '../../domain/user/user.errors';
import { UserService } from '../../domain/user/user.service';
import { User } from '../../domain/user/user.entity';
import { UserBuilder } from './builders/user.builder';
import { UserCommandType, UserCommand } from '../../domain/user/user.command';
import { UserServiceBuilder } from './builders/user.service.builder';
import { UserRepository } from '../../application/user/user.repository';
import { UserRepositoryBuilder } from './builders/user.repository.builder';
import { IdentityServiceBuilder } from '../identity/builders/identity.service.builder';
//#endregion

describe('banned.service', () => {
  let userRepository!: UserRepository;
  let userService!: UserService;

  beforeEach(() => {
    userRepository = UserRepositoryBuilder.inMemory();
    userService = new UserServiceBuilder()
      .withRepository(userRepository)
      .withIdentityService(IdentityServiceBuilder.inMemory())
      .build();
  });

  function make_defaultUserBuilder(): UserBuilder {
    return new UserBuilder()
      .withId('99')
      .withWorkspaceId('test')
      .withEmail('test@test.com')
      .withFirstName('TEST')
      .withLastName('TEST');
  }

  function make_toggleCommand(user: User, toggledBy: User): UserCommand {
    return {
      type: UserCommandType.toggleBan,
      rowId: `${userService.aggregateName}:${user.getWorkspaceId()}`,
      aggregateId: user.getId(),
      payload: {
        toggledBy: toggledBy.getId(),
      },
    };
  }

  describe('#toggleBan()', () => {
    it('should place user ban', async () => {
      const user = make_defaultUserBuilder().build();
      const toggledBy = make_defaultUserBuilder()
        .withId('88')
        .withRole(UserRole.maintainer)
        .build();

      await userService.toggleBanned(user, toggledBy);

      expect(user.isBanned()).toBe(true);
    });

    it('should lift user ban', async () => {
      const user = make_defaultUserBuilder().build();
      const toggledBy = make_defaultUserBuilder()
        .withId('88')
        .withRole(UserRole.maintainer)
        .build();
      user.placeBan();

      await userService.toggleBanned(user, toggledBy);

      expect(user.isBanned()).toBe(false);
    });

    it('should produce a toggle command', async () => {
      const user = make_defaultUserBuilder().build();
      const toggledBy = make_defaultUserBuilder()
        .withId('88')
        .withRole(UserRole.maintainer)
        .build();
      const execute = jest.spyOn(userRepository, 'execute');

      await userService.toggleBanned(user, toggledBy);

      expect(execute).toHaveBeenCalledWith(
        make_toggleCommand(user, toggledBy),
        user,
      );
    });

    it('should throw IllegalOperationError when trying to ban deleted user', async () => {
      const admin = new User('99');
      admin.setRole(UserRole.admin);
      const user = new User('88');
      user.delete();

      const fn = userService.toggleBanned(user, admin);

      await expect(fn).rejects.toThrow(IllegalOperationError);
    });

    it('should throw an error if user tried to ban', async () => {
      const user = make_defaultUserBuilder().build();
      const toggledBy = make_defaultUserBuilder().build();

      await expect(userService.toggleBanned(user, toggledBy)).rejects.toThrow(
        InsufficientRightsError,
      );
    });

    it('should throw an error if trying to toggle yourself', async () => {
      const user = make_defaultUserBuilder().withRole(UserRole.admin).build();

      await expect(userService.toggleBanned(user, user)).rejects.toThrow(
        IllegalOperationError,
      );
    });

    it('should throw an error if trying to ban an admin', async () => {
      const user = make_defaultUserBuilder().withRole(UserRole.admin).build();
      const toggledBy = make_defaultUserBuilder()
        .withId('88')
        .withRole(UserRole.maintainer)
        .build();

      await expect(userService.toggleBanned(user, toggledBy)).rejects.toThrow(
        InsufficientRightsError,
      );
    });
  });
});
