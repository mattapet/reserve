/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from '@rsaas/util/lib/user-role.type';

import { UserEventHandler } from '../../../domain/user/handlers/user.event-handler';
import { UserEvent, UserEventType } from '../../../domain/user/user.event';
import { UserId, User } from '../../../domain/user/user.entity';
import { UserBuilder } from '../builders/user.builder';
//#endregion

describe('user.event-handler', () => {
  const userEventHandler = new UserEventHandler();

  function make_userEvent<
    T extends string,
    P extends Record<string, any> = Record<string, any>
  >(type: T, payload: P) {
    return {
      type,
      aggregateId: '99',
      rowId: 'test',
      timestamp: new Date(0),
      payload,
    };
  }

  function make_ownershipClaimedEvent(): UserEvent {
    return { ...make_userEvent(UserEventType.ownershipClaimed, {}) };
  }

  function make_ownershipGaveUpEvent(): UserEvent {
    return { ...make_userEvent(UserEventType.ownershipGaveUp, {}) };
  }

  function make_loggedInEventWithTimestamp(timestamp: Date): UserEvent {
    return { ...make_userEvent(UserEventType.loggedIn, {}), timestamp };
  }

  function make_loggedOutEventWithTimestamp(timestamp: Date): UserEvent {
    return { ...make_userEvent(UserEventType.loggedOut, {}), timestamp };
  }

  function make_userCreated(
    userId: UserId,
    workspaceId: string,
    timestamp: Date,
  ): UserEvent {
    return {
      ...make_userEvent(UserEventType.created, { workspaceId }),
      aggregateId: userId,
      timestamp,
    };
  }

  describe('user creation', () => {
    it('should set user ID and created at properties', () => {
      const event = make_userCreated('88', 'test', new Date(44));

      const result = userEventHandler.apply(new User(), [event]);

      expect(result).toEqual(
        new UserBuilder()
          .withId('88')
          .withWorkspaceId('test')
          .withRole(UserRole.user)
          .withBanned(false)
          .build(),
      );
    });
  });

  describe('workspace ownership', () => {
    it('should make user an admin when they claim the ownership', () => {
      const event = make_ownershipClaimedEvent();

      const result = userEventHandler.apply(new User(), [event]);

      expect(result.isAdmin()).toBe(true);
    });

    it('should make user an owner', () => {
      const event = make_ownershipClaimedEvent();

      const result = userEventHandler.apply(new User(), [event]);

      expect(result.isOwner()).toBe(true);
    });

    it('should make user not an owner', () => {
      const event = make_ownershipGaveUpEvent();
      const user = new User();
      user.claimOwnership();

      const result = userEventHandler.apply(new User(), [event]);

      expect(result.isOwner()).toBe(false);
    });
  });

  describe('user deletion', () => {
    it('should set user to be deleted', () => {
      const event = make_userEvent(UserEventType.deleted, {});

      const result = userEventHandler.apply(new User('99', 'test'), [event]);

      expect(result.isDeleted()).toBe(true);
    });
  });

  describe('user last login', () => {
    it('should set last login when loggedIn event consumers', () => {
      const event = make_loggedInEventWithTimestamp(new Date(100));

      const result = userEventHandler.apply(new User('99', 'test'), [event]);

      expect(result.getLastLogin()).toEqual(new Date(100));
    });

    it('should override latest logged in time', () => {
      const event = make_loggedInEventWithTimestamp(new Date(100));
      const user = new User('99', 'test');
      user.setLastLogin(new Date(50));

      const result = userEventHandler.apply(new User('99', 'test'), [event]);

      expect(result.getLastLogin()).toEqual(new Date(100));
    });

    it('should ignote logged out event', () => {
      const event = make_loggedOutEventWithTimestamp(new Date(100));

      const result = userEventHandler.apply(new User('99', 'test'), [event]);

      expect(result.getLastLogin()).toBeUndefined();
    });
  });

  describe('user role asssignment', () => {
    it.each`
      role                   | aggregateId | assignedBy
      ${UserRole.admin}      | ${'33'}     | ${'99'}
      ${UserRole.maintainer} | ${'44'}     | ${'88'}
      ${UserRole.user}       | ${'22'}     | ${'77'}
    `(
      'should assign $role to aggregate with id $aggregateId',
      ({ role, aggregateId, assignedBy }) => {
        const event = {
          ...make_userEvent(UserEventType.roleAssigned, { role, assignedBy }),
          aggregateId,
        };

        const result = userEventHandler.apply(new User('99', 'test'), [event]);

        expect(result.getRole()).toBe(role);
      },
    );
  });

  describe('user banned', () => {
    it('should produce a banned user', () => {
      const event = make_userEvent(UserEventType.banPlaced, { placedBy: '88' });

      const result = userEventHandler.apply(new User('99', 'test'), [event]);

      expect(result.isBanned()).toBe(true);
    });

    it('should unban the user', () => {
      const event = make_userEvent(UserEventType.banLifted, { liftedBy: '88' });
      const user = new User('99', 'test');
      user.placeBan();

      const result = userEventHandler.apply(user, [event]);

      expect(result.isBanned()).toBe(false);
    });
  });
});
