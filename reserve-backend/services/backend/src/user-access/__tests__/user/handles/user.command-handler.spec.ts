/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from '@rsaas/util/lib/user-role.type';
import { UserCommandHandler } from '../../../domain/user/handlers/user.command-handler';
import { UserCommandType } from '../../../domain/user/user.command';
import { UserEventType } from '../../../domain/user/user.event';
import { User } from '../../../domain/user/user.entity';
//#endregion

describe('user.command-handler', () => {
  const userCommandHandler = new UserCommandHandler();

  function mock_dateNow(now: number) {
    jest.spyOn(Date, 'now').mockImplementation(() => now);
  }

  function make_userCommand<
    T extends string,
    P extends Record<string, any> = Record<string, any>
  >(type: T, payload: P) {
    return {
      type,
      rowId: 'test',
      aggregateId: '99',
      payload,
    };
  }

  function make_userEvent<
    T extends string,
    P extends Record<string, any> = Record<string, any>
  >(type: T, payload: P) {
    return {
      type,
      rowId: 'test',
      aggregateId: '99',
      timestamp: new Date(Date.now()),
      payload,
    };
  }

  describe('creating user', () => {
    it('should produce a user created event', () => {
      mock_dateNow(100);
      const command = {
        ...make_userCommand(UserCommandType.create, {
          workspaceId: 'test',
        }),
        aggregateId: '88',
      };

      const result = userCommandHandler.execute(new User(), command);

      expect(result).toEqual([
        {
          ...make_userEvent(UserEventType.created, {
            workspaceId: 'test',
          }),
          aggregateId: '88',
          timestamp: new Date(100),
        },
      ]);
    });

    it('should produce a user created and ownership claimed events', () => {
      mock_dateNow(100);
      const command = {
        ...make_userCommand(UserCommandType.createOwner, {
          workspaceId: 'test',
        }),
        aggregateId: '88',
      };

      const result = userCommandHandler.execute(new User(), command);

      expect(result).toEqual([
        {
          ...make_userEvent(UserEventType.created, {
            workspaceId: 'test',
          }),
          aggregateId: '88',
          timestamp: new Date(100),
        },
        {
          ...make_userEvent(UserEventType.ownershipClaimed, {}),
          aggregateId: '88',
          timestamp: new Date(100),
        },
      ]);
    });
  });

  describe('transfering ownership', () => {
    it('should produce ownership claimed and dropped events', async () => {
      mock_dateNow(100);
      const command = {
        ...make_userCommand(UserCommandType.claimOwnership, {}),
        aggregateId: '99',
      };

      const result = userCommandHandler.execute(new User(), command);

      expect(result).toEqual([
        {
          ...make_userEvent(UserEventType.ownershipClaimed, {}),
          aggregateId: '99',
          timestamp: new Date(100),
        },
      ]);
    });

    it('should produce ownership gave up and dropped events', async () => {
      mock_dateNow(100);
      const command = {
        ...make_userCommand(UserCommandType.giveUpOwnership, {}),
        aggregateId: '99',
      };

      const result = userCommandHandler.execute(new User(), command);

      expect(result).toEqual([
        {
          ...make_userEvent(UserEventType.ownershipGaveUp, {}),
          aggregateId: '99',
          timestamp: new Date(100),
        },
      ]);
    });
  });

  describe('deleting a user', () => {
    it('should produce a deleted event', () => {
      mock_dateNow(100);
      const command = make_userCommand(UserCommandType.delete, {});

      const result = userCommandHandler.execute(
        new User('99', 'test'),
        command,
      );

      expect(result).toEqual([
        {
          type: UserEventType.deleted,
          rowId: 'test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {},
        },
      ]);
    });

    it('should produce an empty array if user is deleted', () => {
      mock_dateNow(100);
      const command = make_userCommand(UserCommandType.delete, {});
      const user = new User('99', 'test');
      user.delete();

      const result = userCommandHandler.execute(user, command);

      expect(result).toEqual([]);
    });
  });

  describe('toggling user ban', () => {
    it('should produce a ban placed event', () => {
      mock_dateNow(100);
      const command = make_userCommand(UserCommandType.toggleBan, {
        toggledBy: '88',
      });
      const user = new User('99', 'test');

      const result = userCommandHandler.execute(user, command);

      expect(result).toEqual([
        {
          type: UserEventType.banPlaced,
          rowId: 'test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            placedBy: '88',
          },
        },
      ]);
    });

    it('should produce a ban lifted event', () => {
      mock_dateNow(100);
      const command = make_userCommand(UserCommandType.toggleBan, {
        toggledBy: '88',
      });
      const user = new User('99', 'test');
      user.placeBan();

      const result = userCommandHandler.execute(user, command);

      expect(result).toEqual([
        {
          type: UserEventType.banLifted,
          rowId: 'test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            liftedBy: '88',
          },
        },
      ]);
    });
  });

  describe('role assignment', () => {
    it('should produce a role assigned event', () => {
      mock_dateNow(100);
      const command = make_userCommand(UserCommandType.assignRole, {
        role: UserRole.admin,
        assignedBy: '88',
      });
      const user = new User('99', 'test');

      const result = userCommandHandler.execute(user, command);

      expect(result).toEqual([
        {
          type: UserEventType.roleAssigned,
          rowId: 'test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {
            role: UserRole.admin,
            assignedBy: '88',
          },
        },
      ]);
    });

    it('should produce an empty array if role does not change', () => {
      const command = make_userCommand(UserCommandType.assignRole, {
        role: UserRole.admin,
        assignedBy: '88',
      });
      const user = new User('99', 'test');
      user.setRole(UserRole.admin);

      const result = userCommandHandler.execute(user, command);

      expect(result).toEqual([]);
    });
  });

  describe('user authentication', () => {
    it('should produce a loggedIn event', () => {
      mock_dateNow(100);
      const command = make_userCommand(UserCommandType.login, {});
      const user = new User('99', 'test');

      const result = userCommandHandler.execute(user, command);

      expect(result).toEqual([
        {
          type: UserEventType.loggedIn,
          rowId: 'test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {},
        },
      ]);
    });

    it('should produce a loggedOut event', () => {
      mock_dateNow(100);
      const command = make_userCommand(UserCommandType.logout, {});
      const user = new User('99', 'test');

      const result = userCommandHandler.execute(user, command);

      expect(result).toEqual([
        {
          type: UserEventType.loggedOut,
          rowId: 'test',
          aggregateId: '99',
          timestamp: new Date(100),
          payload: {},
        },
      ]);
    });
  });
});
