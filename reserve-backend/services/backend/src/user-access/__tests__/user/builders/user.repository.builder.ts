/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';
import { EventRepository } from '@rsaas/commons/lib/events/event.repository';
import { InMemoryEventRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-event-repository';
import { InMemorySnapshotRepository } from '@rsaas/commons/lib/events/repositories/in-memory/in-memory-snapshot.repository';
import { SnapshotRepository } from '@rsaas/commons/lib/events/snapshot.repository';

import { UserEvent } from '../../../domain/user/user.event';
import { UserRepository } from '../../../application/user/user.repository';
import {
  UserEventHandler,
  UserCommandHandler,
} from '../../../domain/user/handlers';
//#endregion

export class UserRepositoryBuilder {
  private eventRepository?: EventRepository<UserEvent>;
  private snapshotRepository?: SnapshotRepository;

  public withEventRepository(
    eventRepository: EventRepository<UserEvent>,
  ): UserRepositoryBuilder {
    this.eventRepository = eventRepository;
    return this;
  }

  public withSnapshotRepository(
    snapshotRepository: SnapshotRepository,
  ): UserRepositoryBuilder {
    this.snapshotRepository = snapshotRepository;
    return this;
  }

  public build(): UserRepository {
    return new UserRepository(
      notNull(this.eventRepository, 'eventRepository'),
      notNull(this.snapshotRepository, 'snapshotRepository'),
      new UserCommandHandler(),
      new UserEventHandler(),
    );
  }

  public static inMemory(): UserRepository {
    const eventsRepository = new InMemoryEventRepository<UserEvent>();
    return new UserRepositoryBuilder()
      .withEventRepository(eventsRepository)
      .withSnapshotRepository(new InMemorySnapshotRepository())
      .build();
  }
}
