/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';
import { UserRole } from '@rsaas/util/lib/user-role.type';

import { User, UserId } from '../../../domain/user/user.entity';
import { Identity } from '../../../domain/identity';
//#endregion

interface UserProperties {
  readonly id: UserId;
  readonly workspaceId: string;
  readonly email: string;
  readonly password: string | undefined;
  readonly role: UserRole;
  readonly banned: boolean;
  readonly firstName: string;
  readonly lastName: string;
  readonly phone?: string;
  readonly lastLogin?: Date;
  readonly deleted: boolean;
  readonly owner: boolean;
}

export class UserBuilder {
  private id?: UserId;
  private workspaceId?: string;
  private firstName?: string;
  private lastName?: string;
  private phone?: string;
  private email?: string;
  private password?: string | undefined;
  private lastLogin?: Date | undefined;
  private banned: boolean = false;
  private role: UserRole = UserRole.user;
  private owner: boolean = false;

  public withId(id: UserId): UserBuilder {
    return this.copy({ id });
  }

  public withWorkspaceId(workspaceId: string): UserBuilder {
    return this.copy({ workspaceId });
  }

  public withFirstName(firstName: string): UserBuilder {
    const builder = this.copy();
    builder.firstName = firstName;
    return builder;
  }

  public withLastName(lastName: string): UserBuilder {
    const builder = this.copy();
    builder.lastName = lastName;
    return builder;
  }

  public withPhone(phone?: string): UserBuilder {
    const builder = this.copy();
    builder.phone = phone;
    return builder;
  }

  public withOwner(): UserBuilder {
    return this.copy({ owner: true });
  }

  public withEmail(email: string): UserBuilder {
    return this.copy({ email });
  }

  public withPassword(password?: string): UserBuilder {
    return this.copy({ password });
  }

  public withBanned(banned: boolean): UserBuilder {
    return this.copy({ banned });
  }

  public withRole(role: UserRole): UserBuilder {
    return this.copy({ role });
  }

  public withLastLogin(lastLogin?: Date): UserBuilder {
    return this.copy({ lastLogin });
  }

  public copy(newValues?: Partial<UserProperties>): UserBuilder {
    const builder = new UserBuilder();
    builder.id = newValues?.id ?? this.id;
    builder.workspaceId = newValues?.workspaceId ?? this.workspaceId;
    builder.firstName = newValues?.firstName ?? this.firstName;
    builder.lastName = newValues?.lastName ?? this.lastName;
    builder.phone = newValues?.phone ?? this.phone;
    builder.email = newValues?.email ?? this.email;
    builder.password = newValues?.password ?? this.password;
    builder.lastLogin = newValues?.lastLogin ?? this.lastLogin;
    builder.banned = newValues?.banned ?? this.banned;
    builder.role = newValues?.role ?? this.role;
    builder.owner = newValues?.owner ?? this.owner;
    return builder;
  }

  public static fromIdentity(identity: Identity): UserBuilder {
    return new UserBuilder()
      .withId(identity.getUserId())
      .withEmail(identity.getEmail())
      .withPassword(
        identity.hasPasswordSet() ? identity.getPassword() : undefined,
      )
      .withFirstName(identity.getFirstName())
      .withLastName(identity.getLastName())
      .withPhone(identity.getPhone())
      .withWorkspaceId(identity.getWorkspaceId());
  }

  public build(): User {
    const user = new User(
      notNull(this.id, 'user id'),
      notNull(this.workspaceId, 'workspace id'),
      this.email
        ? new Identity(
            notNull(this.id, 'user id'),
            notNull(this.workspaceId, 'workspace id '),
            notNull(this.email, 'email'),
            this.firstName,
            this.lastName,
            this.phone,
            this.password,
          )
        : undefined,
      this.role,
      this.banned,
    );
    this.lastLogin && user.setLastLogin(this.lastLogin);
    this.owner && user.claimOwnership();
    return user;
  }
}
