/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';

import { UserService } from '../../../domain/user/user.service';
import { UserRepository } from '../../../application/user/user.repository';
import { IdentityService } from '../../../domain/identity';
import { IdentityServiceBuilder } from '../../identity/builders/identity.service.builder';
import { UserRepositoryBuilder } from './user.repository.builder';
//#endregion

export class UserServiceBuilder {
  private repository?: UserRepository;
  private identityService?: IdentityService;

  public withRepository(repository: UserRepository): UserServiceBuilder {
    this.repository = repository;
    return this;
  }

  public withIdentityService(
    identityService: IdentityService,
  ): UserServiceBuilder {
    this.identityService = identityService;
    return this;
  }

  public buildingRepository(
    buildUsing: (builder: UserRepositoryBuilder) => UserRepository,
  ): UserServiceBuilder {
    return this.withRepository(buildUsing(new UserRepositoryBuilder()));
  }

  public buildingIdentityService(
    buildUsing: (builder: IdentityServiceBuilder) => IdentityService,
  ): UserServiceBuilder {
    return this.withIdentityService(buildUsing(new IdentityServiceBuilder()));
  }

  public build(): UserService {
    return new UserService(
      notNull(this.identityService, 'identity service'),
      notNull(this.repository, 'repository'),
    );
  }

  public static inMemory(): UserService {
    return new UserServiceBuilder()
      .withRepository(UserRepositoryBuilder.inMemory())
      .withIdentityService(IdentityServiceBuilder.inMemory())
      .build();
  }
}
