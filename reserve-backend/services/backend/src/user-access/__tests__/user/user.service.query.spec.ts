/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserService } from '../../domain/user/user.service';

import { UserBuilder } from './builders/user.builder';
import { UserRepository } from '../../application/user/user.repository';
import { IdentityServiceBuilder } from '../identity/builders/identity.service.builder';
import { UserRepositoryBuilder } from './builders/user.repository.builder';
import { UserRole } from '@rsaas/util/lib/user-role.type';
import { User } from '../../domain/user/user.entity';
import { UserNotFoundError } from '../../domain/user/user.errors';
//#endregion

describe('user.service', () => {
  let userRepository!: UserRepository;
  let userService!: UserService;

  beforeEach(() => {
    const identityService = IdentityServiceBuilder.inMemory();
    userRepository = UserRepositoryBuilder.inMemory();
    userService = new UserService(identityService, userRepository);
  });

  function make_defaultUserBuilder(): UserBuilder {
    return new UserBuilder()
      .withId('99')
      .withWorkspaceId('test')
      .withEmail('test@test.com');
  }

  async function effect_createUserAndIdentity(user: User): Promise<void> {
    await userService.create(user, user.getIdentity());
  }

  async function effect_createOwnerUserAndIdentity(user: User): Promise<void> {
    await userService.createOwner(user, user.getIdentity());
  }

  describe('user retrieval', () => {
    it('should get all users', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const user = make_defaultUserBuilder().build();
      await effect_createUserAndIdentity(user);

      const result = await userService.getAll('test');

      expect(result).toEqual([
        new UserBuilder()
          .withId('99')
          .withWorkspaceId('test')
          .withEmail('test@test.com')
          .build(),
      ]);
    });

    it('should exclude removed users', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const user = make_defaultUserBuilder().build();
      await effect_createUserAndIdentity(user);
      await userService.delete(user);

      const result = await userService.getAll('test');

      expect(result).toEqual([]);
    });

    it('should get user by its ID', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const user = make_defaultUserBuilder().build();
      await effect_createUserAndIdentity(user);

      const result = await userService.getById('test', '99');

      expect(result).toEqual(
        new UserBuilder()
          .withId('99')
          .withWorkspaceId('test')
          .withEmail('test@test.com')
          .build(),
      );
    });

    it('should get user by its email', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const user = make_defaultUserBuilder().build();
      await effect_createUserAndIdentity(user);

      const result = await userService.getByEmail('test', 'test@test.com');

      expect(result).toEqual(
        new UserBuilder()
          .withId('99')
          .withWorkspaceId('test')
          .withEmail('test@test.com')
          .build(),
      );
    });

    it('should throw UserNotFoundError when user queries by id is deleted', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const user = make_defaultUserBuilder().build();
      await effect_createUserAndIdentity(user);
      await userService.delete(user);

      const fn = userService.getById('test', '99');

      await expect(fn).rejects.toThrow(UserNotFoundError);
    });

    it('should throw UserNotFoundError when user queries by email is deleted', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const user = make_defaultUserBuilder().build();
      await effect_createUserAndIdentity(user);
      await userService.delete(user);

      const fn = userService.getByEmail('test', 'test@test.com');

      await expect(fn).rejects.toThrow(UserNotFoundError);
    });
  });

  describe('maintainer or admin retrieval', () => {
    it('should retrieve admin', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const user = make_defaultUserBuilder().withId('99').build();
      await effect_createUserAndIdentity(user);
      const admin = make_defaultUserBuilder()
        .withId('88')
        .withEmail('admin@test.com')
        .build();
      await effect_createOwnerUserAndIdentity(admin);

      const result = await userService.getAllPrivileged('test');

      expect(result).toEqual([
        new UserBuilder()
          .withId('88')
          .withWorkspaceId('test')
          .withEmail('admin@test.com')
          .withRole(UserRole.admin)
          .withOwner()
          .build(),
      ]);
    });
  });
});
