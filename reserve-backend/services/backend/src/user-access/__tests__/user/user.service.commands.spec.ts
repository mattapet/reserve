/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserService } from '../../domain/user/user.service';

import { IdentityService, Identity } from '../../domain/identity';
import { UserBuilder } from './builders/user.builder';
import { UserCommandType } from '../../domain/user/user.command';
import { UserRepository } from '../../application/user/user.repository';
import { UserRole } from '@rsaas/util/lib/user-role.type';
import { IdentityServiceBuilder } from '../identity/builders/identity.service.builder';
import { UserRepositoryBuilder } from './builders/user.repository.builder';
import { UserServiceBuilder } from './builders/user.service.builder';
import { User } from '../../domain/user/user.entity';
import { IllegalOperationError } from '../../domain/user/user.errors';
//#endregion

describe('user.service', () => {
  let identityService!: IdentityService;
  let userRepository!: UserRepository;
  let userService!: UserService;

  beforeEach(() => {
    identityService = IdentityServiceBuilder.inMemory();
    userRepository = UserRepositoryBuilder.inMemory();
    userService = new UserServiceBuilder()
      .withRepository(userRepository)
      .withIdentityService(identityService)
      .build();
  });

  function make_defaultUserBuilder(): UserBuilder {
    return new UserBuilder()
      .withId('99')
      .withWorkspaceId('test')
      .withEmail('test@test.com')
      .withFirstName('TEST')
      .withLastName('TEST');
  }

  function make_userCommand<T extends string, P extends Record<string, any>>(
    type: T,
    payload: P,
  ) {
    return {
      type,
      rowId: `${userService.aggregateName}:test`,
      aggregateId: `99`,
      payload,
    };
  }

  describe('creating users', () => {
    it('should execute a `createOwner` command', async () => {
      const identity = new Identity('99', 'test', 'test@test.com');
      const user = make_defaultUserBuilder()
        .withRole(UserRole.maintainer)
        .build();
      const execute = jest.spyOn(userRepository, 'execute');

      await userService.createOwner(user, identity);

      expect(execute).toHaveBeenCalledWith(
        make_userCommand(UserCommandType.createOwner, {
          workspaceId: 'test',
        }),
        user,
      );
    });

    it('should execute a `create` command', async () => {
      const identity = new Identity('99', 'test', 'test@test.com');
      const user = make_defaultUserBuilder().build();
      const execute = jest.spyOn(userRepository, 'execute');

      await userService.create(user, identity);

      expect(execute).toHaveBeenCalledWith(
        make_userCommand(UserCommandType.create, {
          workspaceId: 'test',
        }),
        user,
      );
    });
  });

  describe('transfering ownership', () => {
    it('should make new owner an admin', async () => {
      const to = new User();
      await userService.create(to, new Identity('88', 'test', 'to'));

      await userService.claimOwnership(to);

      expect(to.isAdmin()).toBe(true);
    });

    it('should make new owner owner', async () => {
      const to = new User();
      await userService.create(to, new Identity('88', 'test', 'to'));

      await userService.claimOwnership(to);

      expect(to.isOwner()).toBe(true);
    });

    it('should not demote old owner', async () => {
      const from = new User();
      await userService.create(from, new Identity('99', 'test', 'from'));
      await userService.claimOwnership(from);

      await userService.giveUpOwnership(from);

      expect(from.isAdmin()).toBe(true);
    });

    it('should make old owner not an owner any more', async () => {
      const from = new User();
      await userService.create(from, new Identity('99', 'test', 'from'));
      await userService.claimOwnership(from);

      await userService.giveUpOwnership(from);

      expect(from.isOwner()).toBe(false);
    });

    it('should make old owner not an owner any more', async () => {
      const from = new User();
      await userService.create(from, new Identity('99', 'test', 'from'));
      await userService.claimOwnership(from);

      await userService.giveUpOwnership(from);

      expect(from.isOwner()).toBe(false);
    });
  });

  describe('updating user profile', () => {
    it('should update user profile', async () => {
      const user = make_defaultUserBuilder().build();
      const identity = new Identity(
        '99',
        'test',
        'test@test.com',
        'Test',
        'Testovic',
      );

      await userService.updateProfile(user, identity);

      expect(user.getIdentity().getFirstName()).toBe('Test');
      expect(user.getIdentity().getLastName()).toBe('Testovic');
    });
  });

  describe('user deletion', () => {
    it('should anonymize user', async () => {
      const user = make_defaultUserBuilder().build();
      await identityService.create(user.getIdentity());
      const anonymize = jest.spyOn(identityService, 'anonymize');

      await userService.delete(user);

      expect(anonymize).toHaveBeenCalled();
    });

    it('should set user to be deleted', async () => {
      const user = make_defaultUserBuilder().build();
      await identityService.create(user.getIdentity());

      await userService.delete(user);

      expect(user.isDeleted()).toBe(true);
    });

    it('should produce a user delete command', async () => {
      const user = make_defaultUserBuilder().build();
      await identityService.create(user.getIdentity());
      const execute = jest.spyOn(userRepository, 'execute');

      await userService.delete(user);

      expect(execute).toHaveBeenCalledWith(
        make_userCommand(UserCommandType.delete, {}),
        user,
      );
    });

    it('should throw IllegalOperationError if owner tries to delete themselves', async () => {
      const user = new User();
      user.claimOwnership();

      const fn = userService.delete(user);

      await expect(fn).rejects.toThrow(IllegalOperationError);
    });
  });

  describe('user login', () => {
    it('should update user last login date', async () => {
      jest.spyOn(Date, 'now').mockImplementation(() => 100);
      const user = make_defaultUserBuilder().build();

      await userService.login(user);

      expect(user.getLastLogin()).toEqual(new Date(100));
    });

    it('should produce a login command', async () => {
      const user = make_defaultUserBuilder().build();
      const execute = jest.spyOn(userRepository, 'execute');

      await userService.login(user);

      expect(execute).toHaveBeenCalledWith(
        make_userCommand(UserCommandType.login, {}),
        user,
      );
    });
  });

  describe('user logout', () => {
    it('should produce a logout command', async () => {
      const user = make_defaultUserBuilder().build();
      const execute = jest.spyOn(userRepository, 'execute');

      await userService.logout(user);

      expect(execute).toHaveBeenCalledWith(
        make_userCommand(UserCommandType.logout, {}),
        user,
      );
    });
  });
});
