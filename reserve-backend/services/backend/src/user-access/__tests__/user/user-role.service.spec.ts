/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from '@rsaas/util/lib/user-role.type';
import {
  IllegalOperationError,
  InsufficientRightsError,
} from '../../domain/user/user.errors';
import { UserService } from '../../domain/user/user.service';
import { UserBuilder } from './builders/user.builder';
import { User } from '../../domain/user/user.entity';
import { UserRepository } from '../../application/user/user.repository';
import { UserCommandType, UserCommand } from '../../domain/user/user.command';
import { IdentityServiceBuilder } from '../identity/builders/identity.service.builder';
import { UserRepositoryBuilder } from './builders/user.repository.builder';
import { UserServiceBuilder } from './builders/user.service.builder';
//#endregion

describe('user-role.service', () => {
  let userRepository!: UserRepository;
  let userService!: UserService;

  beforeEach(() => {
    userRepository = UserRepositoryBuilder.inMemory();
    userService = new UserServiceBuilder()
      .withRepository(userRepository)
      .withIdentityService(IdentityServiceBuilder.inMemory())
      .build();
  });

  function make_defaultUserBuilder(): UserBuilder {
    return new UserBuilder()
      .withId('99')
      .withWorkspaceId('test')
      .withEmail('test@test.com')
      .withFirstName('TEST')
      .withLastName('TEST');
  }

  function make_userWithIdAndRole(id: string, role: UserRole): User {
    return make_defaultUserBuilder().withId(id).withRole(role).build();
  }

  function make_assignRoleCommand(role: UserRole): UserCommand {
    return {
      type: UserCommandType.assignRole,
      rowId: `${userService.aggregateName}:test`,
      aggregateId: '99',
      payload: {
        role,
        assignedBy: '88',
      },
    };
  }

  describe('#updateUserRole()', () => {
    it.each`
      userRole               | changeByRole           | setRole
      ${UserRole.user}       | ${UserRole.maintainer} | ${UserRole.maintainer}
      ${UserRole.user}       | ${UserRole.admin}      | ${UserRole.maintainer}
      ${UserRole.maintainer} | ${UserRole.admin}      | ${UserRole.admin}
      ${UserRole.admin}      | ${UserRole.admin}      | ${UserRole.maintainer}
      ${UserRole.admin}      | ${UserRole.admin}      | ${UserRole.user}
      ${UserRole.maintainer} | ${UserRole.admin}      | ${UserRole.user}
      ${UserRole.maintainer} | ${UserRole.maintainer} | ${UserRole.user}
    `(
      'should set role $setRole of $userRole changed by $changeByRole',
      async ({ userRole, changeByRole, setRole }) => {
        const user = make_userWithIdAndRole('99', userRole);
        const assignedBy = make_userWithIdAndRole('88', changeByRole);
        const execute = jest.spyOn(userRepository, 'execute');

        await userService.updateUserRole(user, setRole, assignedBy);

        expect(execute).toHaveBeenCalledWith(
          make_assignRoleCommand(setRole),
          user,
        );
      },
    );

    it.each`
      userRole               | changeByRole           | setRole
      ${UserRole.user}       | ${UserRole.maintainer} | ${UserRole.maintainer}
      ${UserRole.user}       | ${UserRole.admin}      | ${UserRole.maintainer}
      ${UserRole.maintainer} | ${UserRole.admin}      | ${UserRole.admin}
      ${UserRole.admin}      | ${UserRole.admin}      | ${UserRole.maintainer}
      ${UserRole.admin}      | ${UserRole.admin}      | ${UserRole.user}
      ${UserRole.maintainer} | ${UserRole.admin}      | ${UserRole.user}
      ${UserRole.maintainer} | ${UserRole.maintainer} | ${UserRole.user}
    `(
      'should set role $setRole of $userRole changed by $changeByRole',
      async ({ userRole, changeByRole, setRole }) => {
        const user = make_userWithIdAndRole('99', userRole);
        const assignedBy = make_userWithIdAndRole('88', changeByRole);

        await userService.updateUserRole(user, setRole, assignedBy);

        expect(user.getRole()).toBe(setRole);
      },
    );

    it('should throw InsufficientRightsError when trying to demote workspace owner', async () => {
      const owner = make_userWithIdAndRole('99', UserRole.admin);
      const assignedBy = make_userWithIdAndRole('88', UserRole.admin);
      owner.claimOwnership();

      const fn = userService.updateUserRole(owner, UserRole.user, assignedBy);

      await expect(fn).rejects.toThrow(InsufficientRightsError);
    });

    it('should throw IllegalOperationError when to change role of deleted user', async () => {
      const user = make_userWithIdAndRole('99', UserRole.admin);
      const assignedBy = make_userWithIdAndRole('88', UserRole.admin);
      user.delete();

      const fn = userService.updateUserRole(user, UserRole.user, assignedBy);

      await expect(fn).rejects.toThrow(IllegalOperationError);
    });

    it('should throw an error if changed by `user`', async () => {
      const user = make_defaultUserBuilder().build();
      const changeBy = make_defaultUserBuilder().withId('88').build();

      await expect(
        userService.updateUserRole(user, UserRole.maintainer, changeBy),
      ).rejects.toThrow(InsufficientRightsError);
    });

    it('should throw an error if changing your own permissions', async () => {
      const user = make_defaultUserBuilder()
        .withRole(UserRole.maintainer)
        .build();

      await expect(
        userService.updateUserRole(user, UserRole.admin, user),
      ).rejects.toThrow(IllegalOperationError);
    });

    it.each`
      userRole               | changeByRole           | setRole
      ${UserRole.maintainer} | ${UserRole.user}       | ${UserRole.user}
      ${UserRole.admin}      | ${UserRole.user}       | ${UserRole.maintainer}
      ${UserRole.admin}      | ${UserRole.maintainer} | ${UserRole.maintainer}
      ${UserRole.admin}      | ${UserRole.maintainer} | ${UserRole.admin}
      ${UserRole.maintainer} | ${UserRole.maintainer} | ${UserRole.admin}
    `(
      'should throw InsufficientRightsError if $userRole changed' +
        'by $changeByRole',
      async ({ userRole, changeByRole, setRole }) => {
        const user = make_userWithIdAndRole('99', userRole);
        const changedBy = make_userWithIdAndRole('88', changeByRole);

        await expect(
          userService.updateUserRole(user, setRole, changedBy),
        ).rejects.toThrow(InsufficientRightsError);
      },
    );
  });
});
