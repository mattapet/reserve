/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AnonymizationService } from '../../application/anonymization/anonymization.services';

import { IdentityService, Identity } from '../../domain/identity';
import { IdentityServiceBuilder } from '../identity/builders/identity.service.builder';
import {
  WorkspaceEventType,
  WorkspaceRequestEventType,
} from '../../../workspace-management';
//#endregion

describe('anonymization service', () => {
  let identityService!: IdentityService;
  let service!: AnonymizationService;

  beforeEach(() => {
    identityService = IdentityServiceBuilder.inMemory();
    service = new AnonymizationService(identityService);
  });

  async function effect_populateIdentities() {
    await Promise.all([
      identityService.create(new Identity('99', 'test', 'test1@test.com')),
      identityService.create(new Identity('88', 'test', 'test2@test.com')),
      identityService.create(new Identity('77', 'test', 'test3@test.com')),
    ]);
  }

  describe('handling workspace deleted event', () => {
    it('should anonymize all workspace users', async () => {
      await effect_populateIdentities();

      await service.consumeWorkspaceDeleted({
        type: WorkspaceEventType.deleted,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          changedBy: '99',
        },
      });

      const identities = await identityService.getAll('test');
      expect(identities.length).not.toBe(0);
      identities.forEach((identity) =>
        expect(identity.isAnonymized()).toBe(true),
      );
    });
  });

  describe('handling workspace request declined event', () => {
    it('should anonymize all workspace users', async () => {
      await effect_populateIdentities();

      await service.consumeWorkspaceRequestDeclined({
        type: WorkspaceRequestEventType.declined,
        rowId: 'WorkspaceAggregate',
        aggregateId: 'test',
        timestamp: new Date(100),
        payload: {
          declinedBy: 'root',
        },
      });

      const identities = await identityService.getAll('test');
      expect(identities.length).not.toBe(0);
      identities.forEach((identity) =>
        expect(identity.isAnonymized()).toBe(true),
      );
    });
  });
});
