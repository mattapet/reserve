/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { ReserveError } from '@rsaas/util/lib/reserve-error';
//#endregion

export class AuthorityConflictError extends ReserveError {}

export class AuthorityNotFoundError extends ReserveError {}

export class AuthorizationCodeError extends ReserveError {}

export class AuthorizedUserFetchError extends ReserveError {}
