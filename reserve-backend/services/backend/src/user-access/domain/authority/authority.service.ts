/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';

import { Authority } from './authority.entity';
import {
  AuthorityConflictError,
  AuthorityNotFoundError,
} from './authority.errors';
//#endregion

@Injectable()
export class AuthorityService {
  private readonly authorities: { [name: string]: Authority } = {};

  public getByName(name: string): Authority {
    if (this.authorityWithNameExists(name)) {
      return this.getAuthorityByName(name);
    } else {
      throw new AuthorityNotFoundError(
        `Authority with name '${name}' does not exist`,
      );
    }
  }

  public register(authority: Authority): void {
    this.validateAuthorityWithNameIsnNotRegistered(authority.getName());
    this.setAuthorityByName(authority.getName(), authority);
  }

  private validateAuthorityWithNameIsnNotRegistered(
    authorityName: string,
  ): void {
    if (this.authorityWithNameExists(authorityName)) {
      throw new AuthorityConflictError(
        `Authority with name '${authorityName}' is already registered`,
      );
    }
  }

  private authorityWithNameExists(authorityName: string): boolean {
    return this.authorities[authorityName] != null;
  }

  private setAuthorityByName(
    authorityName: string,
    authority: Authority,
  ): void {
    this.authorities[authorityName] = authority;
  }

  private getAuthorityByName(authorityName: string): Authority {
    return this.authorities[authorityName];
  }
}
