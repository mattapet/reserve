/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';

import { Authority } from './authority.entity';
import { AuthorizationQuery } from './authorization-query.interface';
import { AuthorizedUser } from './authorized-user.interface';
import {
  AuthorizationCodeError,
  AuthorizedUserFetchError,
} from './authority.errors';
//#endregion

@Injectable()
export class AuthorityAuthorizationService {
  public getAuthorizationQuery(
    state: string,
    authority: Authority,
  ): AuthorizationQuery {
    return {
      client_id: authority.getClientId(),
      redirect_uri: authority.getRedirectUri(),
      scope: authority.getScope(),
      state,
      response_type: 'code',
    };
  }

  public async authorizeUser(
    workspaceId: string,
    authorizationCode: string,
    authority: Authority,
  ): Promise<AuthorizedUser> {
    const accessToken = await this.getAccessToken(authorizationCode, authority);
    return this.getAuthorizedUser(accessToken, authority);
  }

  private async getAccessToken(
    code: string,
    authority: Authority,
  ): Promise<string> {
    try {
      return await authority.exchangeAuthorizationCode(code);
    } catch (error) {
      throw new AuthorizationCodeError(error);
    }
  }

  private async getAuthorizedUser(
    accessToken: string,
    authority: Authority,
  ): Promise<AuthorizedUser> {
    try {
      return await authority.fetchAuthorizedUser(accessToken);
    } catch (error) {
      throw new AuthorizedUserFetchError(error);
    }
  }
}
