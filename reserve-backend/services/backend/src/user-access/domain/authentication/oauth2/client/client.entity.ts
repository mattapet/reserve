/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { GrantType } from '@rsaas/util/lib/oauth2/grant-type.type';
import { Scope } from '@rsaas/util/lib/oauth2/scope.type';

import { AccessToken } from '../../token/access-token.entity';
import { RefreshToken } from '../../token/refresh-token.entity';
//#endregion

@Entity({ name: 'client_credentials' })
export class Client {
  @PrimaryGeneratedColumn('uuid', { name: 'client_id' })
  public clientId!: string;

  @Column({ length: 64, name: 'client_secret', charset: 'utf8' })
  public clientSecret: string;

  @Column({ name: 'user_id', charset: 'utf8' })
  public userId: string;

  @Column({ type: 'simple-json', name: 'redirect_uris', charset: 'utf8' })
  public redirectUris: string[];

  @Column({ type: 'simple-json', name: 'grant_type', charset: 'utf8' })
  public grantType: GrantType[];

  @Column({ type: 'simple-json', charset: 'utf8' })
  public scope: Scope[];

  @OneToMany(() => AccessToken, (item) => item.client)
  public accessTokens?: AccessToken[];

  @OneToMany(() => RefreshToken, (item) => item.client)
  public refreshTokens?: RefreshToken[];

  public constructor(
    clientSecret: string,
    userId: string,
    redirectUris: string[],
    grantType: GrantType[],
    scope: Scope[],
  ) {
    this.clientSecret = clientSecret;
    this.userId = userId;
    this.redirectUris = redirectUris;
    this.grantType = grantType;
    this.scope = scope;
  }
}
