/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Client } from './client.entity';
//#endregion

export interface ClientRepository {
  getById(string: string): Promise<Client>;
  save(client: Client): Promise<Client>;
  remove(client: Client): Promise<Client>;
}
