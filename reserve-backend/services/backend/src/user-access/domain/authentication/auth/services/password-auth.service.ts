/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import bcrypt from 'bcryptjs';

import { User, UserService } from '../../../user';
import { Workspace } from '../../../../../workspace-management';

import { InvalidEmailOrPasswordError, UserBannedError } from '../auth.errors';
//#endregion

export class PasswordAuthService {
  public constructor(private readonly userService: UserService) {}

  public async login(
    workspace: Workspace,
    email: string,
    password: string,
  ): Promise<User> {
    const user = await this.userService.findByEmail(workspace.getId(), email);

    if (!user || !(await this.isUserPasswordValid(user, password))) {
      throw new InvalidEmailOrPasswordError('Invalid email or password');
    }

    if (user.isBanned()) {
      throw new UserBannedError('You have been banned.');
    }

    await this.userService.login(user);
    return user;
  }

  private async isUserPasswordValid(
    user: User,
    password: string,
  ): Promise<boolean> {
    if (user.doesNotHavePasswordSet()) {
      return false;
    }
    return bcrypt.compare(password, user.getPassword());
  }

  public async setPassword(user: User, password: string): Promise<void> {
    const hashedPassword = await bcrypt.hash(password, 10);
    await this.userService.setPassword(user, hashedPassword);
  }
}
