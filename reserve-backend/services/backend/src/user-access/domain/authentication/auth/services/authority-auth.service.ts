/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { uuid } from '@rsaas/util';

import { Workspace } from '../../../../../workspace-management';
import { User, Identity, UserService } from '../../../..';
import {
  Authority,
  AuthorizedUser,
  AuthorityAuthorizationService,
} from '../../../authority';

import { IdentityBuilder } from '../../../../__tests__/identity/builders/identity.builder';

import { AuthorityAuthorizationError, UserBannedError } from '../auth.errors';
//#endregion

export class AuthorityAuthService {
  public constructor(
    private readonly userService: UserService,
    private readonly authorityService: AuthorityAuthorizationService,
  ) {}

  public async login(
    workspace: Workspace,
    authority: Authority,
    code: string,
  ): Promise<User> {
    const authorizedUser = await this.getAuthorizedUser(
      workspace,
      authority,
      code,
    );

    const maybeUser = await this.userService.findByEmail(
      workspace.getId(),
      authorizedUser.email,
    );

    const identity = this.extractIdentityFromAuthorizedUser(
      workspace,
      maybeUser,
      authorizedUser,
    );

    const user = await this.upsertUser(maybeUser, identity);
    return this.loginUser(user);
  }

  private async loginUser(user: User): Promise<User> {
    if (user.isBanned()) {
      throw new UserBannedError('You have been banned.');
    }
    await this.userService.login(user);
    return user;
  }

  private async getAuthorizedUser(
    workspace: Workspace,
    authority: Authority,
    code: string,
  ): Promise<AuthorizedUser> {
    try {
      return await this.authorityService.authorizeUser(
        workspace.getId(),
        code,
        authority,
      );
    } catch (error) {
      throw new AuthorityAuthorizationError(error);
    }
  }

  private async upsertUser(
    user: User | undefined,
    identity: Identity,
  ): Promise<User> {
    if (!user) {
      return this.createNewUser(identity);
    } else {
      return this.updateExsitingUser(user, identity);
    }
  }

  private async updateExsitingUser(
    user: User,
    identity: Identity,
  ): Promise<User> {
    await this.userService.updateProfile(user, identity);
    return user;
  }

  private async createNewUser(identity: Identity): Promise<User> {
    const newUser = new User();
    await this.userService.create(newUser, identity);
    return newUser;
  }

  private extractIdentityFromAuthorizedUser(
    workspace: Workspace,
    user: User | undefined,
    authorizedUser: AuthorizedUser,
  ): Identity {
    return new IdentityBuilder()
      .withUserId(user?.getId() ?? uuid())
      .withWorkspaceId(workspace.getId())
      .withEmail(authorizedUser.email)
      .withFirstName(authorizedUser.firstName)
      .withLastName(authorizedUser.lastName)
      .build();
  }
}
