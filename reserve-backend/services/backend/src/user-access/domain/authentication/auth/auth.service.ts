/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';

import { UserService, User } from '../../..';
import { AuthorityAuthorizationService } from '../../authority';
import { Workspace } from '../../../../workspace-management';

import { Authority } from '../../authority/authority.entity';

import { PasswordAuthService } from './services/password-auth.service';
import { AuthorityAuthService } from './services/authority-auth.service';
//#endregion

@Injectable()
export class AuthService {
  private readonly passwordService: PasswordAuthService;
  private readonly authorityService: AuthorityAuthService;

  public constructor(
    userService: UserService,
    authorityService: AuthorityAuthorizationService,
  ) {
    this.passwordService = new PasswordAuthService(userService);
    this.authorityService = new AuthorityAuthService(
      userService,
      authorityService,
    );
  }

  public async loginPassword(
    workspace: Workspace,
    email: string,
    password: string,
  ): Promise<User> {
    return this.passwordService.login(workspace, email, password);
  }

  public async setPassword(user: User, password: string): Promise<void> {
    return this.passwordService.setPassword(user, password);
  }

  public async loginAuthority(
    workspace: Workspace,
    authority: Authority,
    code: string,
  ): Promise<User> {
    return this.authorityService.login(workspace, authority, code);
  }
}
