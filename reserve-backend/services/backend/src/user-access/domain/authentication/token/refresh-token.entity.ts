/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  Entity,
  PrimaryColumn,
  Column,
  ManyToOne,
  JoinColumn,
  RelationId,
  CreateDateColumn,
  AfterLoad,
} from 'typeorm';
import { Scope } from '@rsaas/util/lib/oauth2/scope.type';

import { Client } from '../oauth2/client/client.entity';
//#endregion

@Entity()
export class RefreshToken {
  @PrimaryColumn({ length: 64, charset: 'utf8' })
  public value: string;

  @Column()
  public expiry: number;

  @Column()
  public revoked: boolean;

  @Column({ type: 'simple-array', charset: 'utf8' })
  public scope: Scope[];

  @CreateDateColumn({
    transformer: {
      to: (d) => d ?? new Date().toISOString(),
      from: (d: Date) => d,
    },
  })
  public created!: Date;

  @Column({ name: 'user_id', charset: 'utf8' })
  public userId: string;

  @Column({ name: 'workspace_id', charset: 'utf8' })
  public workspaceId: string;

  @ManyToOne(() => Client, (item) => item.refreshTokens, {
    nullable: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'client_id', referencedColumnName: 'clientId' })
  public client?: Client;

  @RelationId((item: RefreshToken) => item.client)
  public clientId?: string;

  public constructor(
    value: string,
    expiry: number,
    scope: Scope[],
    userId: string,
    workspaceId: string,
    client?: Client,
  ) {
    this.value = value;
    this.expiry = expiry;
    this.scope = scope;
    this.userId = userId;
    this.workspaceId = workspaceId;
    this.client = client;
    this.revoked = false;
  }

  @AfterLoad()
  // @ts-ignore:line
  private normalize() {
    this.revoked = !!this.revoked;
  }

  public isValid(): boolean {
    return this.isNotRevoked() && this.isNotExpired();
  }

  public isExpired(): boolean {
    return Date.now() > this.created.valueOf() + this.expiry * 1000;
  }

  public isNotExpired(): boolean {
    return !this.isExpired();
  }

  public isRevoked(): boolean {
    return this.revoked;
  }

  public isNotRevoked(): boolean {
    return !this.isRevoked();
  }
  public revoke(): void {
    this.revoked = true;
  }
}
