/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { AccessToken } from './access-token.entity';
import { RefreshToken } from './refresh-token.entity';
//#endregion

export type Token = AccessToken | RefreshToken;

export interface TokenRepository {
  findAccessToken(token: string): Promise<AccessToken | undefined>;
  findRefreshToken(token: string): Promise<RefreshToken | undefined>;
  save(...token: Token[]): Promise<void>;
}
