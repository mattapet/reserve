/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';
import { generateToken } from '@rsaas/util/lib/generate-token';
import { Scope } from '@rsaas/util/lib/oauth2/scope.type';

import { User } from '../../user';

import { TOKEN_REPOSITORY } from './constants';
import { AccessToken } from './access-token.entity';
import { RefreshToken } from './refresh-token.entity';
import { TokenRepository } from './token.repository';
import { TokenNotFoundError, InvalidTokenError } from './token.errors';
//#endregion

@Injectable()
export class TokenService {
  public constructor(
    @Inject(TOKEN_REPOSITORY)
    private readonly repository: TokenRepository,
  ) {}

  public async getAccessToken(value: string): Promise<AccessToken> {
    const token = await this.repository.findAccessToken(value);
    if (!token) {
      throw new TokenNotFoundError();
    }
    return token;
  }

  public async getRefreshToken(value: string): Promise<RefreshToken> {
    const token = await this.repository.findRefreshToken(value);
    if (!token) {
      throw new TokenNotFoundError();
    }
    return token;
  }

  public async generateTokenPair(
    user: User,
    scope: Scope[],
  ): Promise<[AccessToken, RefreshToken]> {
    const at = new AccessToken(
      generateToken(),
      300,
      scope,
      user.getId(),
      user.getWorkspaceId(),
    );
    const rt = new RefreshToken(
      generateToken(),
      84600,
      scope,
      user.getId(),
      user.getWorkspaceId(),
    );
    await this.repository.save(at, rt);
    return [at, rt];
  }

  public async refreshAccessToken(
    refreshToken: RefreshToken,
  ): Promise<AccessToken> {
    if (refreshToken.isExpired()) {
      throw new InvalidTokenError('Refresh token is expired');
    }

    if (refreshToken.isRevoked()) {
      throw new InvalidTokenError('Refresh token is revoked');
    }

    const userId = refreshToken.userId;
    const scope = refreshToken.scope;
    const at = new AccessToken(
      generateToken(),
      300,
      scope,
      userId,
      refreshToken.workspaceId,
    );
    await this.repository.save(at);
    return at;
  }

  public async revokeRefreshToken(refreshToken: RefreshToken): Promise<void> {
    if (refreshToken.isExpired()) {
      throw new InvalidTokenError('Refresh token is expired');
    }
    if (refreshToken.isRevoked()) {
      throw new InvalidTokenError('Refresh token is revoked');
    }
    refreshToken.revoke();
    await this.repository.save(refreshToken);
  }
}
