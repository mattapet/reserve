/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { notNull } from '@rsaas/util';
import { UserRole } from '@rsaas/util/lib/user-role.type';
import { Type } from 'class-transformer';

import { Identity } from '../identity';
//#endregion

export type UserId = string;

export class User {
  public constructor(
    private id?: UserId,
    private workspaceId?: string,
    private identity?: Identity,
    private role: UserRole = UserRole.user,
    private banned: boolean = false,
    private owner: boolean = false,
  ) {}

  @Type(() => Date)
  private lastLogin?: Date;
  private deleted: boolean = false;

  public getId(): UserId {
    return notNull(this.id, 'Unexpected access of uninitialized user entity');
  }

  public getWorkspaceId(): string {
    return notNull(
      this.workspaceId,
      'Unexpected access of uninitialized user entity',
    );
  }

  public getIdentity(): Identity {
    return notNull(this.identity, 'Identity is expected to be set');
  }

  public getEmail(): string {
    return notNull(this.identity?.getEmail(), 'Identity is expect to be set');
  }

  public hasPasswordSet(): boolean {
    const identity = notNull(this.identity, 'Identity is expected to be set');
    return identity.hasPasswordSet();
  }

  public doesNotHavePasswordSet(): boolean {
    return !this.hasPasswordSet();
  }

  public getPassword(): string {
    const identity = notNull(this.identity, 'Identity is expected to be set');
    return identity.getPassword();
  }

  public getLastLogin(): Date | undefined {
    return this.lastLogin;
  }

  public getRole(): UserRole {
    return this.role;
  }

  public isUser(): boolean {
    return this.role === UserRole.user;
  }

  public isMaintainer(): boolean {
    return this.role === UserRole.maintainer;
  }

  public isAdmin(): boolean {
    return this.role === UserRole.admin;
  }

  public isBanned(): boolean {
    return this.banned;
  }

  public isOwner(): boolean {
    return this.owner;
  }

  public isNotOwner(): boolean {
    return !this.owner;
  }

  public equals(other: User): boolean {
    return this.id === other.id;
  }

  public isDeleted(): boolean {
    return this.deleted;
  }

  public isNotDeleted(): boolean {
    return !this.isDeleted();
  }

  public setId(id: string): void {
    this.id = id;
  }

  public setWorkspaceId(workspaceId: string): void {
    this.workspaceId = workspaceId;
  }

  public setIdentity(identity: Identity): void {
    this.id = identity.getUserId();
    this.workspaceId = identity.getWorkspaceId();
    this.identity = identity;
  }

  public claimOwnership(): void {
    this.owner = true;
    this.role = UserRole.admin;
  }

  public giveupOwnership(): void {
    this.owner = false;
  }

  public delete(): void {
    this.deleted = true;
  }

  public setRole(role: UserRole): void {
    this.role = role;
  }

  public placeBan(): void {
    this.banned = true;
  }

  public liftBan(): void {
    this.banned = false;
  }

  public setLastLogin(lastLogin: Date): void {
    this.lastLogin = lastLogin;
  }
}
