/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Command } from '@rsaas/commons/lib/events/command';
import { UserRole } from '@rsaas/util/lib/user-role.type';

import { UserId } from './user.entity';
//#endregion

export enum UserCommandType {
  create = 'user.create',
  createOwner = 'user.create_owner',
  claimOwnership = 'user.claim_ownership',
  giveUpOwnership = 'user.give_up_ownership',
  delete = 'user.delete',
  login = 'user.login',
  logout = 'user.logout',
  toggleBan = 'user.toggle_ban',
  assignRole = 'user.assign_role',
}

export interface UserCreateCommand extends Command {
  readonly type: UserCommandType.create;
  readonly payload: {
    readonly workspaceId: string;
  };
}

export interface UserCreateOwnerCommand extends Command {
  readonly type: UserCommandType.createOwner;
  readonly payload: {
    readonly workspaceId: string;
  };
}

export interface UserClaimOwnershipCommand extends Command {
  readonly type: UserCommandType.claimOwnership;
  readonly payload: Record<string, any>;
}

export interface UserGiveUpOwnershipCommand extends Command {
  readonly type: UserCommandType.giveUpOwnership;
  readonly payload: Record<string, any>;
}

export interface UserDeleteCommand extends Command {
  readonly type: UserCommandType.delete;
}

export interface UserLoginCommand extends Command {
  readonly type: UserCommandType.login;
}

export interface UserLogoutCommand extends Command {
  readonly type: UserCommandType.logout;
}

export interface UserToggleBanCommand extends Command {
  readonly type: UserCommandType.toggleBan;
  readonly payload: {
    readonly toggledBy: UserId;
  };
}

export interface UserAssignRoleCommand extends Command {
  readonly type: UserCommandType.assignRole;
  readonly payload: {
    readonly role: UserRole;
    readonly assignedBy: UserId;
  };
}

export type UserCommand =
  | UserCreateCommand
  | UserDeleteCommand
  | UserLoginCommand
  | UserLogoutCommand
  | UserToggleBanCommand
  | UserCreateOwnerCommand
  | UserClaimOwnershipCommand
  | UserGiveUpOwnershipCommand
  | UserAssignRoleCommand;
