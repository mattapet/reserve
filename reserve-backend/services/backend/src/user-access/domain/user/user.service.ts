/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';
import { UserRole } from '@rsaas/util/lib/user-role.type';
import { keyBy } from '@rsaas/util/lib/key-by';
import { AggregateRepository } from '@rsaas/commons/lib/events';

import { User } from './user.entity';
import { Identity, IdentityService } from '../identity';
import { BannedService } from './services/banned.service';
import { UserRoleService } from './services/user-role.service';
import { UserCommandCreator } from './services/user-command-creator';
import { IllegalOperationError, UserNotFoundError } from './user.errors';
import { USER_REPOSITORY, AGGREGATE_NAME } from './constants';
import { UserCommand } from './user.command';
import { UserEvent } from './user.event';
//#endregion

@Injectable()
export class UserService {
  private readonly bannedService: BannedService;
  private readonly userRoleService: UserRoleService;

  public constructor(
    private readonly identityService: IdentityService,
    @Inject(USER_REPOSITORY)
    private readonly repository: AggregateRepository<
      UserCommand,
      UserEvent,
      User
    >,
  ) {
    this.bannedService = new BannedService(repository);
    this.userRoleService = new UserRoleService(repository);
  }

  public get aggregateName(): string {
    return AGGREGATE_NAME;
  }

  // - MARK: Queries

  public async getAll(workspaceId: string): Promise<User[]> {
    const identities = await this.identityService.getAll(workspaceId);
    const users = await this.repository.getAll(`UserAggregate:${workspaceId}`);
    return this.mapIdentitiesToUsers(identities, users);
  }

  private mapIdentitiesToUsers(identities: Identity[], users: User[]): User[] {
    const keyedIdentities = keyBy(identities, (id) => id.getUserId());
    users.map((user) => user.setIdentity(keyedIdentities[user.getId()]));
    return users.filter((user) => user.isNotDeleted());
  }

  public async getAllPrivileged(workspaceId: string): Promise<User[]> {
    const users = await this.getAll(workspaceId);
    return users.filter((user) => user.isAdmin() || user.isMaintainer());
  }

  public async getById(workspaceId: string, userId: string): Promise<User> {
    const identity = await this.identityService.getByUserId(
      workspaceId,
      userId,
    );
    return this.createUserFromIdentity(identity);
  }

  public async getByEmail(workspaceId: string, email: string): Promise<User> {
    const identity = await this.identityService.getByEmail(workspaceId, email);
    return this.createUserFromIdentity(identity);
  }

  public async findByEmail(
    workspaceId: string,
    email: string,
  ): Promise<User | undefined> {
    const identity = await this.identityService.findByEmail(workspaceId, email);
    return identity && this.createUserFromIdentity(identity);
  }

  private async createUserFromIdentity(identity: Identity): Promise<User> {
    const user = await this.repository.getById(
      `${this.aggregateName}:${identity.getWorkspaceId()}`,
      identity.getUserId(),
    );
    user.setIdentity(identity);

    if (user.isDeleted()) {
      throw new UserNotFoundError(
        `User with id '${user.getId()}' does not exist`,
      );
    }

    return user;
  }

  // - MARK: Commands

  public async create(user: User, identity: Identity): Promise<void> {
    await this.identityService.create(identity);
    await this.repository.execute(
      UserCommandCreator.makeCreateCommand(identity),
      user,
    );
    user.setIdentity(identity);
  }

  public async createOwner(user: User, identity: Identity): Promise<void> {
    await this.identityService.create(identity);
    await this.repository.execute(
      UserCommandCreator.makeCreateOwnerCommand(identity),
      user,
    );
    user.setIdentity(identity);
  }

  public async claimOwnership(newOwner: User): Promise<void> {
    await this.repository.execute(
      UserCommandCreator.makeClaimOwnershipCommand(newOwner),
      newOwner,
    );
  }

  public async giveUpOwnership(oldOwner: User): Promise<void> {
    await this.repository.execute(
      UserCommandCreator.makeGiveUpOwnershipCommand(oldOwner),
      oldOwner,
    );
  }

  public async delete(user: User): Promise<void> {
    if (user.isOwner()) {
      throw new IllegalOperationError('Owner cannot delete themselves');
    }

    await this.identityService.anonymize(user.getIdentity());
    await this.repository.execute(
      UserCommandCreator.makeDeleteCommand(user),
      user,
    );
  }

  public async updateUserRole(
    user: User,
    role: UserRole,
    changedBy: User,
  ): Promise<void> {
    await this.userRoleService.changeUserRole(user, role, changedBy);
  }

  public async toggleBanned(user: User, toggledBy: User): Promise<void> {
    await this.bannedService.toggle(user, toggledBy);
  }

  public async setPassword(user: User, password: string): Promise<void> {
    await this.identityService.setPassword(user.getIdentity(), password);
  }

  public async updateProfile(user: User, identity: Identity): Promise<void> {
    await this.identityService.update(identity);
    user.setIdentity(identity);
  }

  public async login(user: User): Promise<void> {
    await this.repository.execute(
      UserCommandCreator.makeLoginCommand(user),
      user,
    );
  }

  public async logout(user: User): Promise<void> {
    await this.repository.execute(
      UserCommandCreator.makeLogoutCommand(user),
      user,
    );
  }
}
