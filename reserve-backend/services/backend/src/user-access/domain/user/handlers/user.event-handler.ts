/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { AggregateEventHandler, EventHandler } from '@rsaas/commons/lib/events';
import { UserRole } from '@rsaas/util/lib/user-role.type';
import {
  UserEvent,
  UserEventType,
  UserCreatedEvent,
  UserLoggedInEvent,
  UserRoleAssignedEvent,
} from '../user.event';
import { User } from '../user.entity';
//#endregion

@Injectable()
export class UserEventHandler extends AggregateEventHandler<User, UserEvent> {
  @EventHandler(UserEventType.created)
  public handleUserCreated(aggregate: User, event: UserCreatedEvent): User {
    aggregate.setId(event.aggregateId);
    aggregate.setRole(UserRole.user);
    aggregate.setWorkspaceId(event.payload.workspaceId);
    return aggregate;
  }

  @EventHandler(UserEventType.ownershipClaimed)
  public handleUserOwnershipClaimed(aggregate: User): User {
    aggregate.claimOwnership();
    return aggregate;
  }

  @EventHandler(UserEventType.ownershipGaveUp)
  public handleUserOwnershipGaveUp(aggregate: User): User {
    aggregate.giveupOwnership();
    return aggregate;
  }

  @EventHandler(UserEventType.loggedOut)
  public handleUserLoggedOut(aggregate: User): User {
    return aggregate;
  }

  @EventHandler(UserEventType.deleted)
  public handleUserDeleted(aggregate: User): User {
    aggregate.delete();
    return aggregate;
  }

  @EventHandler(UserEventType.loggedIn)
  public handleUserLoggedIn(aggregate: User, event: UserLoggedInEvent): User {
    aggregate.setLastLogin(event.timestamp);
    return aggregate;
  }

  @EventHandler(UserEventType.banLifted)
  public handleUserBanLifted(aggregate: User): User {
    aggregate.liftBan();
    return aggregate;
  }

  @EventHandler(UserEventType.banPlaced)
  public handleUserBanPlaced(aggregate: User): User {
    aggregate.placeBan();
    return aggregate;
  }

  @EventHandler(UserEventType.roleAssigned)
  public handleUserRoleAssigned(
    aggregate: User,
    event: UserRoleAssignedEvent,
  ): User {
    aggregate.setRole(event.payload.role);
    return aggregate;
  }
}
