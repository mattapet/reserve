/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import {
  AggregateCommandHandler,
  CommandHandler,
} from '@rsaas/commons/lib/events';
import {
  UserCommandType,
  UserCommand,
  UserCreateCommand,
  UserCreateOwnerCommand,
  UserClaimOwnershipCommand,
  UserGiveUpOwnershipCommand,
  UserDeleteCommand,
  UserAssignRoleCommand,
  UserToggleBanCommand,
  UserLoginCommand,
  UserLogoutCommand,
} from '../user.command';
import { UserEventType, UserEvent } from '../user.event';
import { User } from '../user.entity';
//#endregion

@Injectable()
export class UserCommandHandler extends AggregateCommandHandler<
  User,
  UserEvent,
  UserCommand
> {
  @CommandHandler(UserCommandType.create)
  public executeCreate(
    aggregate: User,
    { rowId, aggregateId, payload }: UserCreateCommand,
  ): UserEvent[] {
    return [
      {
        type: UserEventType.created,
        rowId,
        aggregateId,
        payload,
        timestamp: new Date(Date.now()),
      },
    ];
  }

  @CommandHandler(UserCommandType.createOwner)
  public executeCreateOwner(
    aggregate: User,
    { rowId, aggregateId, payload }: UserCreateOwnerCommand,
  ): UserEvent[] {
    return [
      {
        type: UserEventType.created,
        rowId,
        aggregateId,
        payload,
        timestamp: new Date(Date.now()),
      },
      {
        type: UserEventType.ownershipClaimed,
        rowId,
        aggregateId,
        payload: {},
        timestamp: new Date(Date.now()),
      },
    ];
  }

  @CommandHandler(UserCommandType.claimOwnership)
  public executeClaimOwnership(
    aggregate: User,
    { rowId, aggregateId }: UserClaimOwnershipCommand,
  ): UserEvent[] {
    return [
      {
        type: UserEventType.ownershipClaimed,
        rowId,
        aggregateId,
        payload: {},
        timestamp: new Date(Date.now()),
      },
    ];
  }

  @CommandHandler(UserCommandType.giveUpOwnership)
  public executeGiveUpOwnership(
    aggregate: User,
    { rowId, aggregateId }: UserGiveUpOwnershipCommand,
  ): UserEvent[] {
    return [
      {
        type: UserEventType.ownershipGaveUp,
        rowId,
        aggregateId,
        payload: {},
        timestamp: new Date(Date.now()),
      },
    ];
  }

  @CommandHandler(UserCommandType.delete)
  public executeDelete(
    aggregate: User,
    { rowId, aggregateId, payload }: UserDeleteCommand,
  ): UserEvent[] {
    if (aggregate.isDeleted()) {
      return [];
    }
    return [
      {
        type: UserEventType.deleted,
        rowId,
        aggregateId,
        payload,
        timestamp: new Date(Date.now()),
      },
    ];
  }

  @CommandHandler(UserCommandType.assignRole)
  public executeAssignRole(
    aggregate: User,
    { rowId, aggregateId, payload }: UserAssignRoleCommand,
  ): UserEvent[] {
    if (aggregate.getRole() === payload.role) {
      return [];
    }
    return [
      {
        type: UserEventType.roleAssigned,
        rowId,
        aggregateId,
        payload,
        timestamp: new Date(Date.now()),
      },
    ];
  }

  @CommandHandler(UserCommandType.toggleBan)
  public executeToggleBan(
    aggregate: User,
    { rowId, aggregateId, payload }: UserToggleBanCommand,
  ): UserEvent[] {
    if (aggregate.isBanned()) {
      return [
        {
          type: UserEventType.banLifted,
          rowId,
          aggregateId,
          payload: {
            liftedBy: payload.toggledBy,
          },
          timestamp: new Date(Date.now()),
        },
      ];
    } else {
      return [
        {
          type: UserEventType.banPlaced,
          rowId,
          aggregateId,
          payload: {
            placedBy: payload.toggledBy,
          },
          timestamp: new Date(Date.now()),
        },
      ];
    }
  }

  @CommandHandler(UserCommandType.login)
  public executeLogin(
    aggregate: User,
    { rowId, aggregateId, payload }: UserLoginCommand,
  ): UserEvent[] {
    return [
      {
        type: UserEventType.loggedIn,
        rowId,
        aggregateId,
        payload,
        timestamp: new Date(Date.now()),
      },
    ];
  }

  @CommandHandler(UserCommandType.logout)
  public executeLogout(
    aggregate: User,
    { rowId, aggregateId, payload }: UserLogoutCommand,
  ): UserEvent[] {
    return [
      {
        type: UserEventType.loggedOut,
        rowId,
        aggregateId,
        payload,
        timestamp: new Date(Date.now()),
      },
    ];
  }
}
