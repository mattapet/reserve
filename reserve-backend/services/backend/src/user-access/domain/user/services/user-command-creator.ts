/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Identity } from '../../identity';
import {
  UserCreateCommand,
  UserCreateOwnerCommand,
  UserDeleteCommand,
  UserClaimOwnershipCommand,
  UserGiveUpOwnershipCommand,
  UserLoginCommand,
  UserCommandType,
  UserLogoutCommand,
} from '../user.command';
import { User } from '../user.entity';
//#endregion

export class UserCommandCreator {
  private constructor() {}

  public static makeCreateCommand(identity: Identity): UserCreateCommand {
    return {
      type: UserCommandType.create,
      rowId: UserCommandCreator.getRowId(identity.getWorkspaceId()),
      aggregateId: identity.getUserId(),
      payload: {
        workspaceId: identity.getWorkspaceId(),
      },
    };
  }

  public static makeCreateOwnerCommand(
    identity: Identity,
  ): UserCreateOwnerCommand {
    return {
      type: UserCommandType.createOwner,
      rowId: UserCommandCreator.getRowId(identity.getWorkspaceId()),
      aggregateId: identity.getUserId(),
      payload: {
        workspaceId: identity.getWorkspaceId(),
      },
    };
  }

  public static makeDeleteCommand(user: User): UserDeleteCommand {
    return {
      type: UserCommandType.delete,
      rowId: UserCommandCreator.getRowId(user.getWorkspaceId()),
      aggregateId: user.getId(),
      payload: {},
    };
  }

  public static makeClaimOwnershipCommand(
    newOwner: User,
  ): UserClaimOwnershipCommand {
    return {
      type: UserCommandType.claimOwnership,
      rowId: UserCommandCreator.getRowId(newOwner.getWorkspaceId()),
      aggregateId: newOwner.getId(),
      payload: {},
    };
  }

  public static makeGiveUpOwnershipCommand(
    newOwner: User,
  ): UserGiveUpOwnershipCommand {
    return {
      type: UserCommandType.giveUpOwnership,
      rowId: UserCommandCreator.getRowId(newOwner.getWorkspaceId()),
      aggregateId: newOwner.getId(),
      payload: {},
    };
  }

  public static makeLoginCommand(user: User): UserLoginCommand {
    return {
      type: UserCommandType.login,
      rowId: UserCommandCreator.getRowId(user.getWorkspaceId()),
      aggregateId: user.getId(),
      payload: {},
    };
  }

  public static makeLogoutCommand(user: User): UserLogoutCommand {
    return {
      type: UserCommandType.logout,
      rowId: UserCommandCreator.getRowId(user.getWorkspaceId()),
      aggregateId: user.getId(),
      payload: {},
    };
  }

  private static getRowId(workspaceId: string): string {
    return `UserAggregate:${workspaceId}`;
  }
}
