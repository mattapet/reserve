/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { IllegalOperationError, InsufficientRightsError } from '../user.errors';
import { User } from '../user.entity';
import { UserRepository } from '../../../application/user/user.repository';
import { UserCommandType, UserCommand } from '../user.command';
//#endregion

export class BannedService {
  public constructor(private readonly repository: UserRepository) {}

  public get aggregateName(): string {
    return 'UserAggregate';
  }

  // - MARK: Commands

  public async toggle(user: User, toggledBy: User): Promise<void> {
    if (user.isDeleted()) {
      throw new IllegalOperationError('Cannot toggle ban of deleted user');
    }

    if (toggledBy.isUser()) {
      throw new InsufficientRightsError(
        'Only admins or Maintainers can ban people.',
      );
    }

    if (user.equals(toggledBy)) {
      throw new IllegalOperationError('Cannot change your own ban.');
    }

    if (user.isAdmin() && !toggledBy.isAdmin()) {
      throw new InsufficientRightsError(
        `Don't have privileges to change user's ban.`,
      );
    }

    await this.repository.execute(
      this.makeToggleBanCommand(user, toggledBy),
      user,
    );
  }

  private makeToggleBanCommand(user: User, toggledBy: User): UserCommand {
    return {
      type: UserCommandType.toggleBan,
      aggregateId: user.getId(),
      rowId: `${this.aggregateName}:${user.getWorkspaceId()}`,
      payload: { toggledBy: toggledBy.getId() },
    };
  }
}
