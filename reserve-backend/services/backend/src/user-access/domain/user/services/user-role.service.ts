/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserRole } from '@rsaas/util/lib/user-role.type';
import { IllegalOperationError, InsufficientRightsError } from '../user.errors';

import { User } from '../user.entity';
import { UserRepository } from '../../../application/user/user.repository';
import { UserCommandType, UserCommand } from '../user.command';
//#endregion

export class UserRoleService {
  public constructor(private readonly repository: UserRepository) {}

  public get aggregateName(): string {
    return 'UserAggregate';
  }

  public async changeUserRole(
    user: User,
    role: UserRole,
    changedBy: User,
  ): Promise<void> {
    if (user.isDeleted()) {
      throw new IllegalOperationError('Cannot change role of deleted user');
    }

    if (changedBy.isUser()) {
      throw new InsufficientRightsError(
        'Only Maintainers or admins can change user roles.',
      );
    }

    if (user.equals(changedBy)) {
      throw new IllegalOperationError(
        'You cannot change your own permissions.',
      );
    }

    if (user.isAdmin() && !changedBy.isAdmin()) {
      throw new InsufficientRightsError(
        'Cannot change privileges of users with higher than your privileges.',
      );
    }

    if (role === UserRole.admin && !changedBy.isAdmin()) {
      throw new InsufficientRightsError(
        'Cannot grant privileges of higher than yours.',
      );
    }

    if (user.isOwner()) {
      throw new InsufficientRightsError('Cannot demote owner of the workspace');
    }

    await this.repository.execute(
      this.makeAssignRoleCommand(user, role, changedBy),
      user,
    );
  }

  private makeAssignRoleCommand(
    user: User,
    role: UserRole,
    assignedBy: User,
  ): UserCommand {
    return {
      type: UserCommandType.assignRole,
      aggregateId: user.getId(),
      rowId: `${this.aggregateName}:${user.getWorkspaceId()}`,
      payload: { role, assignedBy: assignedBy.getId() },
    };
  }
}
