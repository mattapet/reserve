/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Event } from '@rsaas/commons/lib/events/event.entity';
import { UserRole } from '@rsaas/util/lib/user-role.type';
import { UserId } from './user.entity';
//#endregion

export enum UserEventType {
  created = 'user.created',
  deleted = 'user.deleted',
  ownershipClaimed = 'ownership_claimed',
  ownershipGaveUp = 'ownership_gave_up',
  loggedIn = 'user.logged_in',
  loggedOut = 'user.logged_out',
  banPlaced = 'user.ban.placed',
  banLifted = 'user.ban.lifted',
  roleAssigned = 'user.role_assigned',
}

export interface UserCreatedEvent extends Event {
  readonly type: UserEventType.created;
  readonly payload: {
    readonly workspaceId: string;
  };
}

export interface UserOwnershipClaimedEvent extends Event {
  readonly type: UserEventType.ownershipClaimed;
}

export interface UserOwnershipGaveUpEvent extends Event {
  readonly type: UserEventType.ownershipGaveUp;
}
export interface UserDeletedEvent extends Event {
  readonly type: UserEventType.deleted;
}

export interface UserLoggedInEvent extends Event {
  readonly type: UserEventType.loggedIn;
}

export interface UserLoggedOutEvent extends Event {
  readonly type: UserEventType.loggedOut;
}

export interface UserBanPlacedEvent extends Event {
  readonly type: UserEventType.banPlaced;
  readonly payload: {
    readonly placedBy: UserId;
  };
}

export interface UserBanLiftedEvent extends Event {
  readonly type: UserEventType.banLifted;
  readonly payload: {
    readonly liftedBy: UserId;
  };
}

export interface UserRoleAssignedEvent extends Event {
  readonly type: UserEventType.roleAssigned;
  readonly payload: {
    readonly role: UserRole;
    readonly assignedBy: UserId;
  };
}

export type UserEvent =
  | UserCreatedEvent
  | UserDeletedEvent
  | UserOwnershipClaimedEvent
  | UserOwnershipGaveUpEvent
  | UserLoggedInEvent
  | UserLoggedOutEvent
  | UserBanPlacedEvent
  | UserBanLiftedEvent
  | UserRoleAssignedEvent;
