/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable, Inject } from '@nestjs/common';

import { Identity } from './identity.entity';
import { IdentityRepository } from './identity.repository';

import { IDENTITY_REPOSITORY } from './constants';
//#endregion

@Injectable()
export class IdentityService {
  public constructor(
    @Inject(IDENTITY_REPOSITORY)
    private readonly repository: IdentityRepository,
  ) {}

  // - MARK: Queries

  public async getAll(workspaceId: string): Promise<Identity[]> {
    return this.repository.getAll(workspaceId);
  }

  public async getByUserId(
    workspaceId: string,
    userId: string,
  ): Promise<Identity> {
    return this.repository.getByUserId(workspaceId, userId);
  }

  public async getByEmail(
    workspaceId: string,
    email: string,
  ): Promise<Identity> {
    return this.repository.getByEmail(workspaceId, email);
  }

  public async findByEmail(
    workspaceId: string,
    email: string,
  ): Promise<Identity | undefined> {
    return this.repository.findByEmail(workspaceId, email);
  }

  // - MARK: Commands

  public async create(identity: Identity): Promise<Identity> {
    return this.repository.save(identity);
  }

  public async update(identity: Identity): Promise<Identity> {
    return this.repository.save(identity);
  }

  public async setPassword(
    identity: Identity,
    password: string,
  ): Promise<void> {
    identity.setPassword(password);
    await this.repository.save(identity);
  }

  public async anonymize(identity: Identity): Promise<void> {
    identity.anonymize();
    await this.repository.save(identity);
  }

  public async anonymizeByWorkspaceId(workspaceId: string): Promise<void> {
    await this.repository.mapByWorkspaceId(workspaceId, this.anonymizeIdentity);
  }

  private async anonymizeIdentity(identity: Identity): Promise<Identity> {
    identity.anonymize();
    return identity;
  }
}
