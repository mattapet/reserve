/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Entity, PrimaryColumn, Index, Column, AfterLoad } from 'typeorm';
import { notNull } from '@rsaas/util';

import { generateAnonymizationKey } from './services/generate-anonymization-key';
import { encrypt } from './services/encrypt';
import { UserId } from '../user';
//#endregion

@Entity({ name: 'identity' })
export class Identity {
  @PrimaryColumn({ name: 'user_id', charset: 'utf8' })
  private userId: UserId;

  @PrimaryColumn({ name: 'workspace_id', charset: 'utf8' })
  private workspaceId: string;

  @Index()
  @Column({ charset: 'utf8' })
  private email: string;

  @Column({ name: 'first_name', charset: 'utf8' })
  private firstName: string;

  @Column({ name: 'last_name', charset: 'utf8' })
  private lastName: string;

  @Column()
  private anonymized: boolean;

  @Column({ nullable: true, charset: 'utf8' })
  private phone?: string;

  @Column({ nullable: true, charset: 'utf8' })
  private password?: string;

  public constructor(
    userId: string,
    workspaceId: string,
    email: string,
    firstName: string = '',
    lastName: string = '',
    phone?: string,
    password?: string,
    anonymized: boolean = false,
  ) {
    this.userId = userId;
    this.workspaceId = workspaceId;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phone = phone;
    this.password = password;
    this.anonymized = anonymized;
  }

  @AfterLoad()
  // @ts-ignore
  private normalize() {
    this.anonymized = !!this.anonymized;
  }

  public getUserId(): UserId {
    return this.userId;
  }

  public hasId(id: UserId): boolean {
    return this.userId === id;
  }

  public getWorkspaceId(): string {
    return this.workspaceId;
  }

  public getEmail(): string {
    return this.email;
  }

  public hasEmail(email: string): boolean {
    return this.email === email;
  }

  public getFirstName(): string {
    return this.firstName;
  }

  public getLastName(): string {
    return this.lastName;
  }

  public getFullName(): string {
    return `${this.firstName} ${this.lastName}`;
  }

  public isAnonymized(): boolean {
    return this.anonymized;
  }

  public isNotAnonymized(): boolean {
    return !this.anonymized;
  }

  public getPhone(): string | undefined {
    return this.phone;
  }

  public hasPasswordSet(): boolean {
    return this.password != null;
  }

  public getPassword(): string {
    return notNull(this.password, 'Password is not set');
  }

  public setUserId(userId: UserId): void {
    this.userId = userId;
  }

  public setWorkspaceId(workspaceId: string): void {
    this.workspaceId = workspaceId;
  }

  public setEmail(email: string): void {
    this.email = email;
  }

  public setFirstName(firstName: string): void {
    this.firstName = firstName;
  }

  public setLastName(lastName: string): void {
    this.lastName = lastName;
  }

  public anonymize(): void {
    if (this.isAnonymized()) {
      return;
    }

    const key = generateAnonymizationKey();
    this.email = encrypt(this.email, key).toString('hex');
    this.firstName = encrypt(this.firstName, key).toString('hex');
    this.lastName = encrypt(this.lastName, key).toString('hex');
    if (this.phone) {
      this.phone = encrypt(this.phone, key).toString('hex');
    }
    if (this.password) {
      this.password = encrypt(this.password, key).toString('hex');
    }
    this.anonymized = true;
  }

  public setPhone(phone?: string): void {
    this.phone = phone;
  }

  public setPassword(password?: string): void {
    this.password = password;
  }
}
