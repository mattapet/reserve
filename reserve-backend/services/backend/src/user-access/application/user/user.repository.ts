/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { EventRepository } from '@rsaas/commons/lib/events/event.repository';
import {
  InjectEventRepository,
  InjectSnapshotRepository,
} from '@rsaas/commons/lib/events';
import { SnapshotRepository } from '@rsaas/commons/lib/events/snapshot.repository';

import { UserCommand } from '../../domain/user/user.command';
import { UserEvent } from '../../domain/user/user.event';
import { User } from '../../domain/user/user.entity';
import { AggregateRepository } from '@rsaas/commons/lib/events/repositories/aggregate-repository';
import {
  UserCommandHandler,
  UserEventHandler,
} from '../../domain/user/handlers';
//#endregion

@Injectable()
export class UserRepository extends AggregateRepository<
  UserCommand,
  UserEvent,
  User
> {
  public constructor(
    @InjectEventRepository()
    eventRepository: EventRepository<UserEvent>,
    @InjectSnapshotRepository()
    snapshotRepository: SnapshotRepository,
    commandHandler: UserCommandHandler,
    eventHandler: UserEventHandler,
  ) {
    super(
      eventRepository,
      snapshotRepository,
      commandHandler,
      eventHandler,
      User,
    );
  }
}
