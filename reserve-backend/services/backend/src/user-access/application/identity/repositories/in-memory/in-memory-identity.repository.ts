/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { UserNotFoundError } from '../../../../domain/user/user.errors';

import { IdentityRepository } from '../../../../domain/identity/identity.repository';
import { Identity } from '../../../../domain/identity/identity.entity';
//#endregion

interface Storage {
  [workspaceId: string]: {
    [userId: string]: Identity;
  };
}
export class InMemoryIdenittyRepository implements IdentityRepository {
  public constructor(private readonly storage: Storage = {}) {}

  public async getAll(workspaceId: string): Promise<Identity[]> {
    const identities = this.storage[workspaceId] ?? {};
    return Object.values(identities);
  }

  public async findByEmail(
    workspaceId: string,
    email: string,
  ): Promise<Identity | undefined> {
    const identities = await this.getAll(workspaceId);
    return identities.find((identity) => identity.hasEmail(email));
  }

  public async getByEmail(
    workspaceId: string,
    email: string,
  ): Promise<Identity> {
    const identity = await this.findByEmail(workspaceId, email);
    if (!identity) {
      throw new UserNotFoundError('Email not found');
    }
    return identity;
  }

  public async getByUserId(
    workspaceId: string,
    userId: string,
  ): Promise<Identity> {
    const identities = await this.getAll(workspaceId);
    const identity = identities.find((id) => id.hasId(userId));
    if (!identity) {
      throw new UserNotFoundError(`UserId ${userId} not found`);
    }
    return identity;
  }

  public async mapByWorkspaceId(
    workspaceId: string,
    transformer: (Identity: Identity) => Promise<Identity>,
  ): Promise<void> {
    const identities = await this.getAll(workspaceId);
    const transformedIdentities = await Promise.all(
      identities.map(transformer),
    );
    await this.saveAll(transformedIdentities);
  }

  public async save(identity: Identity): Promise<Identity> {
    const identities = this.storage[identity.getWorkspaceId()] || {};
    identities[identity.getUserId()] = identity;
    this.storage[identity.getWorkspaceId()] = identities;
    return identities[identity.getUserId()];
  }

  public async saveAll(identities: Identity[]): Promise<Identity[]> {
    return Promise.all(identities.map((idenity) => this.save(idenity)));
  }
}
