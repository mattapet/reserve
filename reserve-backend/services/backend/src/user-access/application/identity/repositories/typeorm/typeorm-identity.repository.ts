/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { EntityRepository, Repository } from 'typeorm';

import { Identity } from '../../../../domain/identity/identity.entity';
import { IdentityRepository } from '../../../../domain/identity/identity.repository';
import { UserNotFoundError } from '../../../../domain/user/user.errors';
//#endregion

@EntityRepository(Identity)
export class TypeormIdentityRepository
  extends Repository<Identity>
  implements IdentityRepository {
  public async getAll(workspaceId: string): Promise<Identity[]> {
    return this.find({ workspaceId } as any);
  }

  public async getByUserId(
    workspaceId: string,
    userId: string,
  ): Promise<Identity> {
    const item = await this.findOne({ workspaceId, userId } as any);
    if (!item) {
      throw new UserNotFoundError(`UserId ${userId} not found`);
    }
    return item;
  }

  public async getByEmail(
    workspaceId: string,
    email: string,
  ): Promise<Identity> {
    const item = await this.findOne({ workspaceId, email } as any);
    if (!item) {
      throw new UserNotFoundError('Email not found');
    }
    return item;
  }

  public async findByEmail(
    workspaceId: string,
    email: string,
  ): Promise<Identity | undefined> {
    return this.findOne({ workspaceId, email } as any);
  }

  public async mapByWorkspaceId(
    workspaceId: string,
    transformer: (Identity: Identity) => Promise<Identity>,
  ): Promise<void> {
    await this.manager.transaction(async (trx) => {
      const identities = await trx.find(Identity, { workspaceId } as any);
      const transformedIdentities = await Promise.all(
        identities
          .filter((identity) => identity.isNotAnonymized())
          .map(transformer),
      );
      await trx.save(transformedIdentities);
    });
  }
}
