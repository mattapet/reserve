/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { EventPattern, EventConsumer } from '@rsaas/commons/lib/event-broker';
import { Transactional } from '@rsaas/commons/lib/typeorm';

import {
  WorkspaceEventType,
  WorkspaceDeletedEvent,
} from '../../../workspace-management/domain/workspace/workspace.event';

import { IdentityService } from '../../domain/identity';
import {
  WorkspaceRequestEventType,
  WorkspaceRequestDeclinedEvent,
} from '../../../workspace-management/domain/workspace-request';
//#endregion

@Injectable()
@EventConsumer()
export class AnonymizationService {
  public constructor(private readonly identityService: IdentityService) {}

  @Transactional()
  @EventPattern(WorkspaceEventType.deleted)
  public async consumeWorkspaceDeleted(
    event: WorkspaceDeletedEvent,
  ): Promise<void> {
    await this.identityService.anonymizeByWorkspaceId(event.aggregateId);
  }

  @Transactional()
  @EventPattern(WorkspaceRequestEventType.declined)
  public async consumeWorkspaceRequestDeclined(
    event: WorkspaceRequestDeclinedEvent,
  ): Promise<void> {
    await this.identityService.anonymizeByWorkspaceId(event.aggregateId);
  }
}
