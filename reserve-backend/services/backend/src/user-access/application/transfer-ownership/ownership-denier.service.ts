/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { EventConsumer, EventPattern } from '@rsaas/commons/lib/event-broker';
import { Transactional } from '@rsaas/commons/lib/typeorm';

import { UserService } from '../../domain/user/user.service';
import {
  WorkspaceOwnershipTransferredEvent,
  WorkspaceEventType,
} from '../../../workspace-management/domain/workspace/workspace.event';
//#endregion

@Injectable()
@EventConsumer()
export class OwnershipDenier {
  public constructor(private readonly userService: UserService) {}

  @Transactional()
  @EventPattern(WorkspaceEventType.ownershipTransferred)
  public async consumeOwnershipTransfered(
    event: WorkspaceOwnershipTransferredEvent,
  ): Promise<void> {
    const newOwner = await this.userService.getById(
      event.aggregateId,
      event.payload.transferredFrom,
    );

    await this.userService.giveUpOwnership(newOwner);
  }
}
