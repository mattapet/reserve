/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { HttpService } from '@rsaas/commons/lib/http';
import { GrantType } from '@rsaas/util/lib/oauth2/grant-type.type';

import { Authority } from '../../../../domain/authority/authority.entity';
import { AuthorizedUser } from '../../../../domain/authority/authorized-user.interface';

import { SiliconHillAuthorizedUser } from './silicon-hill-user.interface';
//#endregion

const CLIENT_ID = process.env.SILICON_HILL_CLIENT_ID;
const CLIENT_SECRET = process.env.SILICON_HILL_CLIENT_SECRET;
const REDIRECT_URI = process.env.SILICON_HILL_REDIRECT_URI;
const SILICON_HILL_OAUTH_URL = 'https://is.sh.cvut.cz/oauth';
const SILICON_HILL_API_URL = 'https://api.is.sh.cvut.cz/v1';

@Injectable()
export class SiliconHillAuthority implements Authority {
  public constructor(private readonly httpService: HttpService) {}

  public getName(): string {
    return 'silicon_hill';
  }

  public getClientId(): string {
    return CLIENT_ID!;
  }

  public getClientSecret(): string {
    return CLIENT_SECRET!;
  }

  public getRedirectUri(): string {
    return REDIRECT_URI!;
  }

  public getScope(): string {
    return '';
  }

  public getAuthorizationUrl(): string {
    return `${SILICON_HILL_OAUTH_URL}/authorize`;
  }

  public async exchangeAuthorizationCode(
    authorizationCode: string,
  ): Promise<string> {
    const body = {
      client_id: this.getClientId(),
      client_secret: this.getClientSecret(),
      grant_type: GrantType.authorizationCode,
      redirect_uri: this.getRedirectUri(),
      code: authorizationCode,
    };

    const response = await this.httpService
      .post(`${SILICON_HILL_OAUTH_URL}/token`)
      .set('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8')
      .set('Accept', 'application/json')
      .send(body);

    const { access_token: accessToken } = await response.json();
    return accessToken;
  }

  public async fetchAuthorizedUser(
    accessToken: string,
  ): Promise<AuthorizedUser> {
    const shUser = await this.fetchSiliconHillAuthorizedUser(accessToken);
    return this.mapToAuthorizedUser(shUser);
  }

  private async fetchSiliconHillAuthorizedUser(
    accessToken: string,
  ): Promise<SiliconHillAuthorizedUser> {
    const response = await this.httpService
      .get(`${SILICON_HILL_API_URL}/users/me`)
      .set('Authorization', `Bearer ${accessToken}`)
      .set('Accept', 'application/json')
      .send();

    return response.json();
  }

  private mapToAuthorizedUser(user: SiliconHillAuthorizedUser): AuthorizedUser {
    const { first_name: firstName, email, surname: lastName } = user;
    return { firstName, lastName, email };
  }
}
