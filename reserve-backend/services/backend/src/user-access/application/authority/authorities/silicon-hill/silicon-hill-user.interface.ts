/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

export interface SiliconHillAuthorizedUser {
  readonly country: string;
  readonly created_at: string; // '2009-10-05T00:00:00+02:00'
  readonly email: string;
  readonly first_name: string;
  readonly id: number;
  readonly im: string | null;
  readonly note: string | null;
  readonly organization: {
    readonly name: string;
    readonly note: string;
  } | null;
  readonly phone: string | null;
  readonly phone_vpn: string | null;
  readonly photo_file: string | null; // url
  readonly photo_file_small: string | null; // url
  readonly state: 'active' /* vždy active, jiný účet se nepřihlásí */;
  readonly surname: string;
  readonly ui_language: 'en' | 'cs' | null;
  readonly ui_skin: 'light' | 'dark';
  readonly username: string;
  readonly usertype: 'individual' | 'legal_entity' | 'system';
}
