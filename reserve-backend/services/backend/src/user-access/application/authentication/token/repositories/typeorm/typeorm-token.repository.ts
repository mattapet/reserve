/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { Injectable } from '@nestjs/common';
import { Transactional } from '@rsaas/commons/lib/typeorm';

import {
  TokenRepository,
  Token,
} from '../../../../../domain/authentication/token/token.repository';
import { RefreshToken } from '../../../../../domain/authentication/token/refresh-token.entity';
import { AccessToken } from '../../../../../domain/authentication/token/access-token.entity';
import { AccessTokenRepository } from './access-token.repository';
import { RefreshTokenRepository } from './refresh-token.repository';
//#endregion

@Injectable()
export class TypeormTokenRepository implements TokenRepository {
  public constructor(
    private readonly atRepository: AccessTokenRepository,
    private readonly rtRepository: RefreshTokenRepository,
  ) {}

  @Transactional()
  public async clear(): Promise<void> {
    await this.atRepository.clear();
    await this.rtRepository.clear();
  }

  public async findAccessToken(
    value: string,
  ): Promise<AccessToken | undefined> {
    return this.atRepository.findOne(value);
  }

  public async findRefreshToken(
    value: string,
  ): Promise<RefreshToken | undefined> {
    return this.rtRepository.findOne(value);
  }

  @Transactional()
  public async save(...tokens: Token[]): Promise<void> {
    await Promise.all(tokens.map((token) => this.saveToken(token)));
  }

  private async saveToken(token: Token): Promise<void> {
    if (token instanceof AccessToken) {
      await this.atRepository.save(token);
    } else {
      await this.rtRepository.save(token);
    }
  }
}
