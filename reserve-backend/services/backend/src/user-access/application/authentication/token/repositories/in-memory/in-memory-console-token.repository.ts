/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import {
  TokenRepository,
  Token,
} from '../../../../../domain/authentication/token/token.repository';
import { AccessToken } from '../../../../../domain/authentication/token/access-token.entity';
import { RefreshToken } from '../../../../../domain/authentication/token/refresh-token.entity';
//#endregion

export class InMemoryTokenRepository implements TokenRepository {
  public constructor(
    private atStorage: { [token: string]: AccessToken } = {},
    private rtStorage: { [token: string]: RefreshToken } = {},
  ) {}

  public clear(): void {
    this.atStorage = {};
    this.rtStorage = {};
  }

  public async findAccessToken(
    token: string,
  ): Promise<AccessToken | undefined> {
    return this.atStorage[token];
  }

  public async findRefreshToken(
    token: string,
  ): Promise<RefreshToken | undefined> {
    return this.rtStorage[token];
  }

  public async save(...tokens: Token[]): Promise<void> {
    await Promise.all(tokens.map((token) => this.saveToken(token)));
  }

  private async saveToken(token: Token): Promise<void> {
    if (token instanceof AccessToken) {
      await this.saveAccessToken(token);
    } else {
      await this.saveRefreshToken(token);
    }
  }

  private async saveAccessToken(at: AccessToken): Promise<void> {
    at.created = at.created ?? new Date(Date.now());
    this.atStorage[at.value] = at;
  }

  private async saveRefreshToken(rt: RefreshToken): Promise<void> {
    rt.created = rt.created ?? new Date(Date.now());
    this.rtStorage[rt.value] = rt;
  }
}
