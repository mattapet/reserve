/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { uuid } from '@rsaas/util';

import { ClientRepository } from '../../../../../../domain/authentication/oauth2/client/client.repository';
import { ClientNotFoundError } from '../../../../../../domain/authentication/oauth2/client/client.errors';
import { Client } from '../../../../../../domain/authentication/oauth2/client/client.entity';
//#endregion

export class InMemoryClientRepository implements ClientRepository {
  public constructor(
    private readonly storage: { [clientId: string]: Client } = {},
  ) {}

  public async getById(clientId: string): Promise<Client> {
    const item = this.storage[clientId];
    if (!item) {
      throw new ClientNotFoundError(`Client with id ${clientId} was not found`);
    }
    return item;
  }

  public async save(client: Client): Promise<Client> {
    if (!client.clientId) {
      client.clientId = uuid();
    }
    return (this.storage[client.clientId] = client);
  }

  public async remove(client: Client): Promise<Client> {
    delete this.storage[client.clientId];
    return client;
  }
}
