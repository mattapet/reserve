/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// #region imports
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';
import { CommonsModule, COMMONS_MODELS } from '@rsaas/commons';

import { EdgeModule } from './edge';
import { ConsoleModule } from './console';
import { WorkspaceManagementModule } from './workspace-management';
import { UserAccessModule } from './user-access';
import { ResourceReservationModule } from './resource-reservation';
// #endregion

const DB_TYPE = process.env.DB_TYPE;
const DB_HOST = process.env.DB_HOST;
const DB_PORT = parseInt(process.env.DB_PORT ?? '3306', 10);
const DB_USERNAME = process.env.DB_USERNAME;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_DATABASE = process.env.DB_DATABASE;

@Module({
  imports: [
    ScheduleModule.forRoot(),
    TypeOrmModule.forRoot({
      type: DB_TYPE as 'mysql',
      host: DB_HOST,
      port: DB_PORT,
      username: DB_USERNAME,
      password: DB_PASSWORD,
      database: DB_DATABASE,
      autoLoadEntities: true,
      entities: [...COMMONS_MODELS, `${__dirname}/**/*.entity{.ts,.js}`],
      charset: 'utf8',
      synchronize: true,
      extra: { connectionLimit: 50 },
    }),
    CommonsModule,
    EdgeModule,
    ConsoleModule,
    ResourceReservationModule,
    UserAccessModule,
    WorkspaceManagementModule,
  ],
})
export class RootModule {}
