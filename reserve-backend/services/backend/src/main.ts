/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// tslint:disable:no-var-requires
require('./load-dotenv');

//#region imports
import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import morgan from 'morgan';
import helmet from 'helmet';

import { initializeEventBroker } from '@rsaas/commons/lib/event-broker';
import { initializeTransactionalContext } from '@rsaas/commons/lib/typeorm';
import { RootModule } from './root.module';
import { setupDocs } from './setup-docs';
//#endregion

const PORT = process.env.PORT ? parseInt(process.env.PORT, 10) : undefined;
const LOG_FORMAT = process.env.LOG_FORMAT;

initializeTransactionalContext();
async function bootstrap() {
  const app = await NestFactory.create(RootModule);
  initializeEventBroker(app);
  app.enableCors();
  app.use(helmet());
  app.use(morgan(LOG_FORMAT ?? 'dev'));
  app.useGlobalPipes(new ValidationPipe());

  setupDocs(app);
  try {
    await app.listen(PORT ?? 3000);
  } catch (err) {
    // tslint:disable:no-console
    console.error(err);
    process.exit(1);
  }
}
bootstrap();
