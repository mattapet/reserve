/*
 * Copyright (c) 2018-present Peter Matta, Czech Technical University in Prague
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

//#region imports
import { INestApplication } from '@nestjs/common';
import { SwaggerModule } from '@nestjs/swagger';

import { ApiDocsModule } from './edge/api';
//#endregion

export function setupDocs(app: INestApplication): void {
  SwaggerModule.setup(
    'api/docs/workspace',
    app,
    ApiDocsModule.createWorkspaceDocument(app),
  );
  SwaggerModule.setup(
    'api/docs/setup',
    app,
    ApiDocsModule.createSetupDocument(app),
  );
  SwaggerModule.setup(
    'api/docs/console',
    app,
    ApiDocsModule.createConsoleDocument(app),
  );
}
